#!/usr/bin/env python3


import xml.etree.ElementTree as ET
from pathlib import PosixPath
from tinycss2 import parse_stylesheet, serialize
from hashlib import md5


head = """
import {Option, some, none} from 'fp-ts/lib/Option';
import { createElement } from 'react';
import { DIV } from 'sdi/components/elements';

export type PreserveAspectRatio = 
 | 'none'
 | 'xMinYMin'
 | 'xMidYMin'
 | 'xMaxYMin'
 | 'xMinYMid'
 | 'xMidYMid'
 | 'xMaxYMid'
 | 'xMinYMax'
 | 'xMidYMax'
 | 'xMaxYMax' ;


const pictos = {
"""

tail = """
};

type Picto = typeof pictos;
type PictoKey = keyof Picto;

const isPictoKey = (name: string): name is PictoKey => name in pictos;

export const keyFromString = (
    name: string,
    prefix = 'pic-'
): Option<PictoKey> => {
    const key = `${prefix}${name}`;
    if (isPictoKey(key)) {
        return some(key);
    }
    return none;
};

export const renderPicto = (key: PictoKey, preserveAspectRatio = "xMidYMid" as PreserveAspectRatio) => DIV({className: `picto-infiltration ${key}`, key}, pictos[key](preserveAspectRatio))

export default renderPicto;
"""


def simple_name(n):
    try:
        return n.split("}")[1]
    except IndexError:
        return n


def transform_style(css, prefix):
    rules = parse_stylesheet(css)
    for rule in rules:
        if rule.type == "qualified-rule":
            for tok in rule.prelude:
                if tok.type == "ident":
                    tok.value = f"p{prefix}-{tok.value}"
    return serialize(rules)


def transform_attr(key, value, prefix):
    if key == "id" or key == "class":
        return f"p{prefix}-{value}"
    return value


def get_id(e):
    for name, value in e.attrib.items():
        if simple_name(name) == "id":
            return value

    bs = bytes(str(e), "utf-8") + bytes(str(e.attrib), "utf-8")
    return "id_" + md5(bs).hexdigest()


def clean_prefix(prefix):
    return prefix.replace(' ', '')

class Transform:
    def __init__(self, root, prefix):
        self._root = root
        self._prefix = clean_prefix(prefix)
        self._styles = []

    def run(self):
        self.collect_styles(self._root)
        return self.to_typescript(self._root)

    def collect_styles(self, e):
        for name, value in e.attrib.items():
            if name == "style":
                self._styles.append(
                    (
                        get_id(e),
                        value,
                    )
                )
        [self.collect_styles(c) for c in e]

    def ser_styles(self):
        rules = []
        for k, t in self._styles:
            rules.append(f"#{k}{{ {t} }}")
        return " ".join(rules)

    def to_typescript(self, e):
        attr_list = []
        forced_id = None
        for name, value in e.attrib.items():
            if name == "style":
                forced_id = get_id(e)

        attr_names = []
        for name, value in e.attrib.items():
            s_name = simple_name(name)
            if s_name in attr_names:
                continue
            if s_name == "style":
                attr_list.append(f"id: '{get_id(e)}'")
            elif s_name == "id" and forced_id is not None:
                continue
            elif simple_name(e.tag) == "svg" and s_name == "preserveAspectRatio":
                continue
            elif s_name == "class":
                attr_list.append(
                    f'"className": "{transform_attr(s_name, value, self._prefix)}"'
                )
            else:
                attr_list.append(
                    f'"{s_name}": "{transform_attr(s_name, value, self._prefix)}"'
                )
            attr_names.append(s_name)

        if simple_name(e.tag) == "svg":
            attrs = attrs = "{" + ",".join(attr_list) + ", preserveAspectRatio }"
        else:
            attrs = "{" + ",".join(attr_list) + "}"

        childs = [self.to_typescript(c) for c in e]
        if len(childs) > 0:
            cs = ",\n".join(childs)
            return f'createElement("{simple_name(e.tag)}", {attrs}, {cs})'
        if e.text is not None:
            if simple_name(e.tag) == "style":
                css = (
                    transform_style(e.text, self._prefix) + self.ser_styles()
                ).replace("\n", "")
                return f'createElement("{simple_name(e.tag)}", {attrs}, "{css}")'
            text = e.text.replace("\n", "\\n")
            return f'createElement("{simple_name(e.tag)}", {attrs}, "{text}")'
        return f'createElement("{simple_name(e.tag)}", {attrs})'


# def to_typescript(e, prefix):
#     attrs = (
#         "{"
#         + ",".join(
#             [
#                 f'"{name}":"{transform_attr(name, value, prefix)}"'
#                 for name, value in e.attrib.items()
#             ]
#         )
#         + "}"
#     )
#     childs = [to_typescript(c, prefix) for c in e]
#     if len(childs) > 0:
#         cs = ",\n".join(childs)
#         return f'createElement("{tag_name(e.tag)}", {attrs}, {cs})'
#     if e.text is not None:
#         if tag_name(e.tag) == "style":
#             css = transform_style(e.text, prefix)
#             return f'createElement("{tag_name(e.tag)}", {attrs}, "{css}")'
#         return f'createElement("{tag_name(e.tag)}", {attrs}, "{e.text}")'
#     return f'createElement("{tag_name(e.tag)}", {attrs})'


def main():
    with open("../../../src/components/pictos.ts", "w") as file:
        file.write(head)

        for path in PosixPath().glob("*.svg"):
            tree = ET.parse(str(path))
            svg_name = f"pic-{path.stem}"
            t = Transform(tree.getroot(), svg_name)
            file.write(
                f'"{svg_name}" : (preserveAspectRatio: PreserveAspectRatio) => {t.run()},\n'
            )

        file.write(tail)


if __name__ == "__main__":
    main()
