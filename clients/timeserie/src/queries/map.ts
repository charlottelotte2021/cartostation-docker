import { right } from 'fp-ts/lib/Either';
import { fromNullable, Option } from 'fp-ts/lib/Option';
import {
    IMapInfo,
    IMapBaseLayer,
    Inspire,
    ILayerInfo,
    MessageRecord,
    StyleConfig,
    Feature,
    FeatureCollection,
} from 'sdi/source';
import { iife } from 'sdi/lib';
import { queryK, query } from 'sdi/shape';
import { nameToCode } from 'sdi/components/button/names';
import { SyntheticLayerInfo } from 'sdi/app';
import { FetchData } from 'sdi/map';
import {
    waterMapId,
    ParameterType,
    VisibleGroundLayer,
    StationSelection,
} from '../types';
import { isSelectedStation } from '../events/timeserie';
import {
    GROUND_WATERBODIES,
    otherSurfaceWaterbodies,
    getSurfcaeWaterbodyColor,
    findSurfaceWaterBodyByCode,
    getSelectedKind,
    getSelectedStationList,
    withDataStation,
    getSelectedParameter,
    getFeatureCollectionSurface,
    stationCountFunction,
    getFeatureCollectionGQuality,
    getFeatureCollectionGQuantity,
} from './timeserie';

export const getBaseLayer = (): IMapBaseLayer => ({
    codename: 'urbis_gray',
    name: {
        fr: 'Urbis Gray',
        nl: 'Urbis Gray',
    },
    srs: 'EPSG:31370',
    url: '/webservice/wmsproxy/urbis.irisnet.be',
    params: {
        VERSION: '1.1.1',
        LAYERS: {
            fr: 'urbisFRGray',
            nl: 'urbisNLGray',
        },
    },
});

export const getView = queryK('port/map/view');

const NOW = new Date().toISOString();

const LABEL_RES_LIMIT = 6.3;

export const markerColor = (kind: ParameterType) => {
    switch (kind) {
        case 'surface':
            return '#6e8cff';
        case 'ground-quality':
            return '#c68700';
        case 'ground-quantity':
            return '#00729a';
    }
};

export const metadataTemplate = (kind: ParameterType): Inspire => ({
    id: kind,
    geometryType: 'Point',
    resourceTitle: { fr: kind, nl: kind },
    resourceAbstract: { fr: kind, nl: kind },
    uniqueResourceIdentifier: kind,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: NOW, revision: NOW },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: NOW,
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
});

const groundStyle = (
    codePoint: number,
    valuePropName: string,
    labelPropName: MessageRecord,
    position: 'top' | 'bottom'
): StyleConfig => ({
    kind: 'point-discrete',
    propName: valuePropName, // 'ref_gwb',
    label: {
        size: 12,
        yOffset: position === 'top' ? -12 : 24,
        baseline: 'bottom',
        propName: labelPropName, // { fr: 'no_code', nl: 'no_code' },
        align: 'center',
        resLimit: LABEL_RES_LIMIT,
        color: 'hsl(238, 37.9%, 28.4%)',
    },
    groups: GROUND_WATERBODIES.map(({ id, color }) => ({
        marker: {
            color,
            size: 16,
            codePoint,
        },
        values: [id.toString()],
        label: {},
    })),
});

const surfaceStyle = (): StyleConfig => ({
    kind: 'point-discrete',
    propName: 'id_waterbody',
    label: {
        size: 12,
        align: 'center',
        color: '#000000',
        yOffset: -12,
        baseline: 'bottom',
        propName: {
            en: 'gid_site',
            fr: 'gid_site',
            nl: 'gid_site',
        },
        resLimit: LABEL_RES_LIMIT,
    },
    groups: ['KAN', 'WOL', 'ZEN']
        .map(code => ({
            marker: {
                color: getSurfcaeWaterbodyColor(code),
                size: 16,
                codePoint: nameToCode('circle'),
            },
            values: [
                findSurfaceWaterBodyByCode(code)
                    .map(wb => wb.id.toString())
                    .getOrElse(''),
            ],
            label: {},
        }))
        .concat([
            {
                marker: {
                    color: getSurfcaeWaterbodyColor('other'),
                    size: 16,
                    codePoint: nameToCode('circle'),
                },
                values: otherSurfaceWaterbodies().map(({ id }) =>
                    id.toString()
                ),
                label: {},
            },
        ]),
});

export const layerTemplate = (
    kind: ParameterType,
    style: StyleConfig,
    visible: boolean
): ILayerInfo => ({
    id: kind,
    legend: null,
    group: null,
    metadataId: kind,
    featureViewOptions: { type: 'default' },
    layerInfoExtra: null,
    visible,
    style,
    visibleLegend: true,
    opacitySelector: false,
});

export const getMapInfo = (): IMapInfo => ({
    baseLayer: 'urbis.irisnet.be/urbis_gray',
    id: waterMapId,
    url: `/none`,
    lastModified: Date.now(),
    status: 'published',
    title: { fr: 'Water', nl: 'Water' },
    description: { fr: 'Water', nl: 'Water' },
    categories: [],
    attachments: [],
    layers: query('data/layer-infos')
        .map(info => ({ ...info, visible: isHydroLayerVisible(info.id) }))
        .concat([
            layerTemplate('surface', surfaceStyle(), true),
            layerTemplate(
                'ground-quality',
                groundStyle(
                    nameToCode('circle'),
                    'ref_gwb',
                    { fr: 'no_code', nl: 'no_code' },
                    'top'
                ),
                isGroundVisible('quality')
            ),
            layerTemplate(
                'ground-quantity',
                groundStyle(
                    nameToCode('dot-circle'),
                    'ref_gwb',
                    { fr: 'nom', nl: 'nom' },
                    'bottom'
                ),
                isGroundVisible('quantity')
            ),
        ]),
});

export const layerInfo = (kind: ParameterType): SyntheticLayerInfo => ({
    name: { fr: 'o', nl: 'o', en: 'o' },
    info: (() => {
        switch (kind) {
            case 'surface':
                return layerTemplate('surface', surfaceStyle(), true);
            case 'ground-quality':
                return layerTemplate(
                    'ground-quality',
                    groundStyle(
                        nameToCode('circle'),
                        'ref_gwb',
                        { fr: 'no_code', nl: 'no_code' },
                        'top'
                    ),
                    isGroundVisible('quality')
                );
            case 'ground-quantity':
                return layerTemplate(
                    'ground-quantity',
                    groundStyle(
                        nameToCode('dot-circle'),
                        'ref_gwb',
                        { fr: 'nom', nl: 'nom' },
                        'bottom'
                    ),
                    isGroundVisible('quantity')
                );
        }
    })(),
    metadata: metadataTemplate(kind),
});

const getFilteredSurfaceLayer = () =>
    getFeatureCollectionSurface().chain(fs =>
        getSelectedParameter().map(p => ({
            ...fs,
            features: fs.features
                .map(stationCountFunction(p.id))
                .filter(withDataStation('surface')),
        }))
    );

export const getFilteredGQuality = () =>
    getFeatureCollectionGQuality().chain(fs =>
        getSelectedParameter().map(p => ({
            ...fs,
            features: fs.features
                .map(stationCountFunction(p.id))
                .filter(withDataStation('ground-quality')),
        }))
    );

export const fetchData =
    (kind: ParameterType, filtered: boolean): FetchData =>
    () => {
        if (filtered) {
            const data = iife(() => {
                switch (kind) {
                    case 'ground-quality':
                        return getFilteredGQuality();
                    case 'ground-quantity':
                        return getFeatureCollectionGQuantity();
                    case 'surface':
                        return getFilteredSurfaceLayer();
                }
            });
            return right(data as Option<FeatureCollection>);
        }
        const data = iife(() => {
            switch (kind) {
                case 'ground-quality':
                    return getFeatureCollectionGQuality();
                case 'ground-quantity':
                    return getFeatureCollectionGQuantity();
                case 'surface':
                    return getFeatureCollectionSurface();
            }
        });
        return right(data as Option<FeatureCollection>);
        // const data = (() => {
        //     switch (kind) {
        //         case 'ground-quality':
        //             return fromNullable(query('data/layer/ground/quality'));
        //         case 'ground-quantity':
        //             return fromNullable(query('data/layer/ground/quantity'));
        //         case 'surface':
        //             return fromNullable(query('data/layer/surface'));
        //     }
        // })();
        // return right(data);
    };

const getVisibleGroundLayer = queryK('timeserie/visible/ground-layer');

export const isGroundVisible = (gl: VisibleGroundLayer) =>
    getVisibleGroundLayer().indexOf(gl) >= 0;

export const getGeocoderInput = queryK('port/geocoder/input');

export const getGeocoderResponse = () =>
    fromNullable(query('port/geocoder/response'));

export const getHydroLayerInfos = (): ILayerInfo[] =>
    query('data/layer-infos').concat();

export const findLayerInfo = (layerId: string) =>
    fromNullable(getHydroLayerInfos().find(l => l.id === layerId));

export const findMetadata = (metadataId: string) =>
    fromNullable(query('data/metadatas').find(m => m.id === metadataId));

export const findLayerData = (metadataId: string) =>
    fromNullable(query('data/layers')[metadataId]);

export const isHydroLayerVisible = (layerId: string) =>
    fromNullable(query('water/hydro-layer/visible')[layerId]).getOrElse(false);

export const isSelectedFeature = (f: Feature) =>
    getSelectedKind()
        .map(kind =>
            isSelectedStation(
                { id: f.id, kind },
                getSelectedStationList() as StationSelection[]
            )
        )
        .getOrElse(false);
