import * as debug from 'debug';
import {
    TableDataRow,
    TableSource,
    TableWindow,
    TableData,
    filterRows,
    sortRows,
    sliceWindow,
} from 'sdi/components/table2';
import { queryK, query } from 'sdi/shape';
import {
    getSelectedParameterList,
    getSelectedParameterRaw,
    getSelectedStation,
    getSelectedStationList,
    getStationName,
    getDisplayMode,
    getWindowEnd,
    getWindowStart,
    getParamUnit,
    getSelectedParameter,
} from './timeserie';
import tr, { fromRecord } from 'sdi/locale';
import {
    remoteToOption,
    RemoteResource,
    remoteNone,
    StreamingField,
} from 'sdi/source';
import { flatten } from 'fp-ts/lib/Array';
import { Option, none, some, fromNullable } from 'fp-ts/lib/Option';
import { TimeserieData, tsTime, tsValue, tsFactor } from '../remote/timeserie';
import { Collection } from 'sdi/util';
import { timeserieKey } from './timeserie';
import { ordNumber } from 'fp-ts/lib/Ord';

const logger = debug('sdi:queries/table');

// const getKeys = () => {
//     const tableMode = getTableMode();
//     switch (tableMode) {
//         case 'per-parameter': return [
//             'time',
//             ...(getSelectedStationList()
//                 .map(s => getStationName(s)))
//         ];

//         case 'per-station': return [
//             'time',
//             ...(getSelectedParameterList()
//                 .map(p => `${fromRecord(p.name)} \n (${p.unit})`))
//         ];
//     }
// };

// const getTypes = (): TableDataType[] => {
//     const tableMode = getTableMode();
//     switch (tableMode) {
//         case 'per-parameter': return [
//             'datetime',
//             ...(getSelectedStationList()
//                 .map<TableDataType>(_ => 'number'))
//         ];
//         case 'per-station': return [
//             'datetime',
//             ...(getSelectedParameterList()
//                 .map<TableDataType>(_ => 'number'))
//         ];
//     }
// };

const getFields = (): StreamingField[] => {
    const tableMode = getDisplayMode();
    switch (tableMode) {
        case 'per-parameter':
            return [
                [tr.ts('timestamp'), 'datetime'],
                ...getSelectedStationList().map<StreamingField>(s => [
                    getStationName(s),
                    'number',
                ]),
            ];
        case 'per-station':
            return [
                [tr.ts('timestamp'), 'datetime'],
                ...getSelectedParameterList().map<StreamingField>(p => [
                    `${fromRecord(p.name)} \n (${getParamUnit(p).getOrElse(
                        ''
                    )})`,
                    'number',
                ]),
            ];
    }
};

const tsIter = (timeserie: Option<TimeserieData>) => {
    let index = 0;

    const eq = (a: number, b: number) => Math.abs(a - b) < 1000; // Number.EPSILON

    const incr = (ts: TimeserieData, t: number) => {
        index += 1;
        if (index < ts.length) {
            const nextRow = ts[index];
            const nextRowTime = tsTime(nextRow);
            if (eq(nextRowTime, t)) {
                incr(ts, t);
            }
        }
    };

    const next = (t: number) =>
        timeserie.chain(ts => {
            if (index < ts.length) {
                const row = ts[index];
                const rowTime = tsTime(row);
                if (eq(rowTime, t)) {
                    incr(ts, t);
                    return tsFactor(row) < 1
                        ? some(`< ${tsValue(row)}`)
                        : some(`${tsValue(row) * tsFactor(row)}`);
                }
            }
            return none;
        });

    return next;
};

const getRows = (timeseries: Option<TimeserieData>[]): TableDataRow[] => {
    // gather dates
    const filterMin = getWindowStart()
        .map(({ date }) => {
            const minTime = date.getTime();
            return (t: number) => t >= minTime;
        })
        .getOrElse(() => true);

    const filterMax = getWindowEnd()
        .map(({ date }) => {
            const maxTime = date.getTime();
            return (t: number) => t <= maxTime;
        })
        .getOrElse(() => true);

    const filterWindow = (n: number) => filterMin(n) && filterMax(n);

    const timestamps: number[] = flatten(
        timeseries.map(opt =>
            opt.map(ts => ts.map(row => tsTime(row))).getOrElse([])
        )
    )
        .filter(filterWindow)
        .sort(ordNumber.compare);

    // make rows
    let currentTime = Number.MIN_SAFE_INTEGER;
    const rows: TableDataRow[] = [];
    const iters = timeseries
        .map(opt => opt.map(ts => ts.filter(row => filterWindow(tsTime(row)))))
        .map(tsIter);
    timestamps.forEach(from => {
        if (from === currentTime) {
            return;
        }
        currentTime = from;
        const cells = [from, ...iters.map(i => i(from).getOrElse(''))];
        // const cells = [
        //     // (new Date(from)).toUTCString(),
        //     datetimeBEFormated(new Date(from)),
        //     ...(iters.map(i => i(from).map(formatNumber).getOrElse(''))),
        // ];

        rows.push({ from, cells });
    });

    return rows;
};

const getDataForStation = (
    tsCollection: Collection<RemoteResource<TimeserieData>>
) => {
    const getTimeserie = (pid: number) =>
        getSelectedStation()
            .map(s => timeserieKey(s.kind, s.id, pid))
            .chain(key => fromNullable(tsCollection[key]))
            .getOrElse(remoteNone);

    const timeseries = getSelectedParameterRaw().map(({ id }) =>
        remoteToOption(getTimeserie(id))
    );

    return getRows(timeseries);
};

const getDataForParameter = (
    tsCollection: Collection<RemoteResource<TimeserieData>>
) => {
    const getTimeserie = (sid: string | number) =>
        getSelectedParameter()
            .map(p => timeserieKey(p.kind, sid, p.id))
            .chain(key => fromNullable(tsCollection[key]))
            .getOrElse(remoteNone);

    const timeseries = getSelectedStationList().map(({ id }) =>
        remoteToOption(getTimeserie(id))
    );

    return getRows(timeseries);
};

const getDataImpl = (
    tsCollection: Collection<RemoteResource<TimeserieData>>,
    w: TableWindow
): TableData => {
    const rawRows = (() => {
        const tableMode = getDisplayMode();
        switch (tableMode) {
            case 'per-parameter':
                return getDataForParameter(tsCollection);
            case 'per-station':
                return getDataForStation(tsCollection);
        }
    })();

    const { filters, sort } = query('component/table');

    const sorter = sort
        .map(s => sortRows(s.col, s.direction, getFields()))
        .getOrElse((data: TableDataRow[]) => data);
    const filter = filterRows(filters);
    const slicer = sliceWindow(w);

    const filteredRows = filter(rawRows);
    const sortedRows = sorter(filteredRows);
    const windowedRows = slicer(sortedRows);
    return { rows: windowedRows, total: filteredRows.length };
};

// const getData = subscribe(
//     'data/timeseries', getDataImpl,
//     'timeserie/display/mode',
//     'timeserie/station/select/index',
//     'timeserie/station/select',
//     'timeserie/parameter/select/index',
//     'timeserie/parameter/select',
//     'timeserie/dates/start',
//     'timeserie/dates/end',
// );

const getData = (w: TableWindow) => getDataImpl(query('data/timeseries'), w);

export const getSource = (): TableSource => ({
    data: getData,
    fields: getFields(),
});

export const tableTimeserieQuery = queryK('component/table');

logger('loaded');
