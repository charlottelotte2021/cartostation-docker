import { query, queryK } from 'sdi/shape';
import {
    fromNullable,
    some,
    none,
    Option,
    isSome,
    fromPredicate,
} from 'fp-ts/lib/Option';
import {
    NormSelection,
    ParameterSelection,
    ParameterType,
    StationSelection,
    TimeserieDataStatus,
} from '../types';
import { index, catOptions, flatten } from 'fp-ts/lib/Array';
import {
    remoteNone,
    RemoteResource,
    foldRemote,
    remoteLoading,
    remoteError,
    remoteSuccess,
    remoteToOption,
    FeatureCollection,
    Feature,
} from 'sdi/source';
import {
    PlotPoint,
    PlotDataElement,
    MAXLENGTH,
    PlotNorm,
    PlotData,
} from '../components/plot/types';
import {
    TimeserieData,
    tsTime,
    tsValue,
    tsFactor,
    Norm,
    getNormOption,
    Parameter,
    GroupData,
    ParameterList,
    WaterBody,
} from '../remote/timeserie';
import {
    mapCollection,
    getFeaturePropOption,
    uniqIdentified,
    tryNumber,
    getCollectionItem,
    memo,
} from 'sdi/util';
import * as debug from 'debug';
import { scopeOption, pickSortL } from 'sdi/lib';
import tr, { fromRecord } from 'sdi/locale';
import { Level } from '../events/route';
import { getLang } from 'sdi/app';
import { isSelectedFeature } from './map';

const logger = debug('sdi:queries/timeserie');

export const timeserieKey = (
    t: ParameterType,
    station: number | string,
    parameter: number
) => `${t}/${station}/${parameter}`;

const parameterSort = pickSortL((p: Parameter) =>
    fromRecord(p.name).toLowerCase()
);
const groupSort = pickSortL((g: GroupData) => g.sort);

export const filterSelectedStations =
    (kind: ParameterType) => (fc: FeatureCollection) =>
        fc.features
            .filter(f => !isSelectedFeature(f))
            .filter(f => {
                const normalized = getStationName({
                    id: f.id,
                    kind,
                }).toLocaleLowerCase();
                const normalized_search =
                    getStationSearch().toLocaleLowerCase();
                return (
                    normalized.includes(normalized_search) ||
                    normalized_search === ''
                );
            });

export const getFeatureCollectionSurface = () =>
    fromNullable(query('data/layer/surface'));

export const getFeatureCollectionGQuality = () =>
    fromNullable(query('data/layer/ground/quality'));

export const getFeatureCollectionGQuantity = () =>
    fromNullable(query('data/layer/ground/quantity'));

export const withDataStation = (kind: ParameterType) => (station: Feature) => {
    if (getMode() === 'station' || kind === 'ground-quantity') {
        return true;
    }
    const s = station as CountedStation;
    return s.count !== undefined && s.count !== NO_DATA && s.count !== 0;
};

export const getFilteredSurface = () =>
    getFeatureCollectionSurface()
        .map(filterSelectedStations('surface'))
        .chain(fs =>
            getSelectedParameter().map(p =>
                fs
                    .map(stationCountFunction(p.id))
                    .filter(withDataStation('surface'))
            )
        );

export const getFilteredGQuality = () =>
    getFeatureCollectionGQuality()
        .map(filterSelectedStations('ground-quality'))
        .chain(fs =>
            getSelectedParameter().map(p =>
                fs
                    .map(stationCountFunction(p.id))
                    .filter(withDataStation('ground-quality'))
            )
        );

export const getFilteredGQuantity = () =>
    getFeatureCollectionGQuantity().map(
        filterSelectedStations('ground-quantity')
    );

export const getDisplayMode = queryK('timeserie/display/mode');

export const getMode = () => query('timeserie/mode');

export const getLevel = () => fromNullable(query('timeserie/level'));

export const getLevelFromKind = (kind: ParameterType): Level =>
    kind === 'surface' ? 'surface' : 'ground';

export const getFeature = (
    id: string | number,
    fcOpt: Option<FeatureCollection>
): Option<Feature> => {
    const strId = typeof id === 'string' ? id : id.toString();
    return fcOpt.chain(fc =>
        fromNullable(
            fc.features.find(f => {
                const strFeatureId =
                    typeof f.id === 'string' ? f.id : f.id.toString();
                return strFeatureId === strId;
            })
        )
    );
};

export const getCurrentFeature = () => {
    const fcOpt = getSelectedKind().chain(getFeatureCollection);
    return getSelectedId().chain(id => getFeature(id, fcOpt));
};

export const getFeatureCollection = (
    k: ParameterType
): Option<FeatureCollection> => {
    switch (k) {
        case 'surface':
            return getFeatureCollectionSurface();
        case 'ground-quality':
            return getFeatureCollectionGQuality();
        case 'ground-quantity':
            return getFeatureCollectionGQuantity();
    }
};

// export const getAllFeaturePaths = () => {
//     const featurePaths: FeaturePath[] = [];
//     getSelectedKind().chain(kind => getFeatureCollection(kind).map(f => f.features.map(feature => featurePaths.push({ layerId: kind, featureId: feature.id }))));
//     return featurePaths;
// }

export const getFeatureFromSelection = ({ kind, id }: StationSelection) =>
    getFeature(id, getFeatureCollection(kind));

export const getParameterCounts = () =>
    scopeOption()
        .let('id', getSelectedId())
        // .let('level', getLevel())
        .let('counts', ({ id }) =>
            getCollectionItem(`${id}`, query('timeserie/parameter/count'))
        )
        .pick('counts');

export const getStationCounts = (paramId: number) =>
    getCollectionItem(`${paramId}`, query('timeserie/station/count'));

export type CountedParameter = Parameter & {
    count: number;
};

export type CountedStation = Feature & {
    count: number;
};

export type ParameterWithKind = Parameter & {
    kind: ParameterType;
};

export const NO_DATA = -1;

export const getSurfaceParameterRawList = () =>
    query('data/parameters/surface').concat();

export const getSurfaceParameterListWithKind = () =>
    parameterSort(getSurfaceParameterRawList()).map(param => ({
        param,
        kind: 'surface',
    }));

export const getGroundQualParameterListWithKind = (): ParameterWithKind[] =>
    parameterSort(getGroundQualityParameterList()).map(param => ({
        ...param,
        kind: 'ground-quality',
    }));

export const getGroundQuantParameterListWithKind = (): ParameterWithKind[] =>
    parameterSort(getGroundQuantityParameterList()).map(param => ({
        ...param,
        kind: 'ground-quantity',
    }));

const paramCountFunction = () =>
    getParameterCounts().fold(
        (p: Parameter) => ({
            ...p,
            count: NO_DATA,
        }),
        counts => (p: Parameter) => ({
            ...p,
            count: counts[p.id] ?? NO_DATA,
        })
    );

export const stationCountFunction = (paramId: number) =>
    getStationCounts(paramId).fold(
        (f: Feature) => ({
            ...f,
            count: NO_DATA,
        }),
        counts => (f: Feature) => ({
            ...f,
            count: counts[f.id] ?? NO_DATA,
        })
    );

export const getSurfaceParameterList = (): CountedParameter[] => {
    return parameterSort(getSurfaceParameterRawList()).map(
        paramCountFunction()
    );
};

export const getGroundQualityParameterList = (): CountedParameter[] =>
    parameterSort(query('data/parameters/ground/quality').concat()).map(
        paramCountFunction()
    );

export const getGroundQuantityParameterList = (): ParameterList =>
    parameterSort(query('data/parameters/ground/quantity').concat());

export const getDataTimeseries = queryK('data/timeseries');

export const getNormName = (norm: string) => {
    const name = query('data/norms')[norm];
    if (name === undefined) {
        return norm;
    }
    return fromRecord(name);
};

export const getSelectedStationList = () =>
    query('timeserie/station/select').concat();

export const findStation = (id: number) =>
    fromNullable(getSelectedStationList().find(ss => ss.id === id));

export const findStationName = (id: number) =>
    fromNullable(getSelectedStationList().find(s => s.id === id))
        .map(s => getStationName(s))
        .getOrElse('');

export const getSelectedStationListId = (): number[] =>
    getSelectedStationList()
        .map(({ id }) => tryNumber(id))
        .filter(opt => isSome(opt))
        .map(opt => opt.getOrElse(-1));

export const getSelectedStationIndex = () => {
    const len = getSelectedStationList().length;
    const index = query('timeserie/station/select/index');
    if (index < len) {
        return index;
    }
    return 0;
};

export const getHighlightedStation = () =>
    fromNullable(query('timeserie/station/select/highlighted'));

export const getHighlightedStationId = () =>
    getHighlightedStation().chain(station => tryNumber(station.id));

export const isHighlightedStation = (station: StationSelection) =>
    getHighlightedStation()
        .map(ss => ss.id === station.id && ss.kind === station.kind)
        .getOrElse(false);

export const isHighlightedStationOpt = fromPredicate((ss: StationSelection) =>
    isHighlightedStation(ss)
);

const getInfoStation = () =>
    fromNullable(query('timeserie/station/select/info'));

export const hasStationInfo = (ss: StationSelection) =>
    getInfoStation()
        .map(infost => infost.id === ss.id && infost.kind === ss.kind)
        .getOrElse(false);

export const hasStationInfoOpt = fromPredicate((s: StationSelection) =>
    hasStationInfo(s)
);

export const getSelectedStation = () =>
    index(getSelectedStationIndex(), getSelectedStationList());

export const getSelectedParameterRaw = queryK('timeserie/parameter/select');

const getInfoParam = () =>
    fromNullable(query('timeserie/parameter/select/info'));

export const hasParamInfo = (ps: ParameterSelection) =>
    getInfoParam()
        .map(infop => infop.id === ps.id && infop.kind === ps.kind)
        .getOrElse(false);

export const hasParamInfoOpt = fromPredicate((p: ParameterSelection) =>
    hasParamInfo(p)
);

export const getPlotData = () => {
    switch (getDisplayMode()) {
        case 'per-station':
            return getSelectedParameterRaw().reduce<PlotData>(
                (acc, { id }) =>
                    remoteToOption(getTimeserieAsPlotData(id)).fold(acc, ts =>
                        acc.concat(ts)
                    ),
                []
            );
        case 'per-parameter':
            return getSelectedStationListId().reduce<PlotData>(
                (acc, id) =>
                    remoteToOption(getTimeserieAsPlotData(id)).fold(acc, ts =>
                        acc.concat(ts)
                    ),
                []
            );
    }
};

export const getSelectedParameterList = () =>
    catOptions(
        getSelectedParameterRaw().map(({ kind, id }) => findParameter(kind, id))
    );

export const getSelectedParameterIndex = () => {
    const len = getSelectedParameterList().length;
    const index = fromNullable(query('timeserie/parameter/select/index'));
    return index.map(i => (i < len ? i : 0));
};

export const getSelectedParameter = () =>
    getSelectedParameterIndex().chain(pid =>
        index(pid, getSelectedParameterList())
    );

export const isSelectedParam = (paramId: number) =>
    getSelectedParameter()
        .map(p => p.id === paramId)
        .getOrElse(false);

export const getHighligthTimeserieData = () => {
    switch (getDisplayMode()) {
        case 'per-parameter':
            return getHighlightedStationId().chain(id =>
                getTimeserieBetweenDates(id)
            );
        case 'per-station':
            return getHighlightedParam().chain(({ id }) =>
                getTimeserieBetweenDates(id)
            );
        // return getSelectedParameter().chain(({ id }) =>
        //     getTimeserieBetweenDates(id)
        // );
    }
};

export const getHighlightedParam = () =>
    fromNullable(query('timeserie/parameter/select/highlighted'));

export const isHighlightedParam = (param: ParameterSelection) =>
    getHighlightedParam()
        .map(hp => hp.id === param.id && hp.kind === param.kind)
        .getOrElse(false);

export const getHighlightedElements = ():
    | Option<StationSelection>
    | Option<ParameterSelection> => {
    switch (getDisplayMode()) {
        case 'per-parameter':
            return getHighlightedStation();
        case 'per-station':
            return getHighlightedParam();
    }
};

/**
 * Those 2 functions replace Math.{min, max} because
 * we need them to process many more values than what
 * can be given as arguments.
 *
 */
const getMin = (values: number[]) =>
    values.reduce((acc, val) => (val < acc ? val : acc));
const getMax = (values: number[]) =>
    values.reduce((acc, val) => (val > acc ? val : acc));

export const getMinInDataElem = (dataElem: PlotDataElement) =>
    dataElem.ts.length > 0
        ? some(getMin(dataElem.ts.map(p => p.value * p.factor)))
        : none;

export const getMaxInDataElem = (dataElem: PlotDataElement) =>
    dataElem.ts.length > 0
        ? some(getMax(dataElem.ts.map(p => p.value * p.factor)))
        : none;

export const getMedian = (dataElem: PlotDataElement) => {
    const values = dataElem.ts.map(p => p.value * p.factor);
    if (values.length > 0) {
        const sorted = values.sort((a, b) => a - b);
        const len = sorted.length;
        const med =
            len % 2 !== 0
                ? values[Math.floor(len / 2)]
                : (values[len / 2 - 1] + values[len / 2]) / 2;
        return some(med.toPrecision(2));
    } else {
        return none;
    }
};

export const getAverage = (dataElem: PlotDataElement) => {
    const values = dataElem.ts.map(p => p.value * p.factor);
    if (values.length > 0) {
        const avg = values.reduce((acc, val) => acc + val) / values.length;
        return some(avg.toPrecision(2));
    } else {
        return none;
    }
};
export const getPercentile = (dataElem: PlotDataElement, percent: number) => {
    const values = dataElem.ts
        .map(dE => dE.value * dE.factor)
        .sort((a, b) => a - b);
    if (values.length > 0) {
        const k = (values.length - 1) * percent;
        const f = Math.floor(k);
        const c = Math.ceil(k);
        if (f == c) {
            return some(values[k].toString());
        }
        const d0 = values[f] * (c - k);
        const d1 = values[c] * (k - f);
        return some((d0 + d1).toPrecision(2));
    } else {
        return none;
    }
};

export const getSelectedKind = () =>
    getSelectedStation().fold(
        getSelectedParameter().map(p => p.kind),
        ss => some(ss.kind)
    );
// export const getSelectedKind = () =>
//     notEmpty(getSelectedStationList()).map(sl => sl[0].kind);

export const getSelectedId = () => getSelectedStation().map(({ id }) => id);

export const getWindowStart = () =>
    fromNullable(query('timeserie/dates/start'));
export const getWindowEnd = () => fromNullable(query('timeserie/dates/end'));

export const getSelectedPointInfo = () =>
    fromNullable(query('timeserie/point/select'));

interface PlotPointInfo {
    dataId: number;
    tag: 'point';
    point: PlotPoint;
    color: string;
}
interface ClusterInfo {
    dataId: number;
    tag: 'cluster';
    start: Date;
    end: Date;
    average: string;
    color: string;
}
export type PointOrCluster = PlotPointInfo | ClusterInfo;

/**
 * return an Option of a PointOrCluster
 */
export const getSelectedPlotPoint = () =>
    getSelectedPointInfo().chain(({ id, start, end, color }) =>
        remoteToOption(getTimeserieAsPlotData(id)).chain<PointOrCluster>(
            dataElem => {
                if (dataElem.ts.length > MAXLENGTH) {
                    const c = dataElem.ts.filter(
                        point => point.date >= start && point.date < end
                    );
                    const len = c.length;
                    switch (len) {
                        case 0:
                            return none;
                        case 1:
                            return some({
                                dataId: dataElem.identifier,
                                tag: 'point',
                                point: c[0],
                                color,
                            });
                        default: {
                            const avg =
                                c.reduce(
                                    (acc, point) =>
                                        acc + point.value * point.factor,
                                    0
                                ) / c.length;
                            return some({
                                dataId: dataElem.identifier,
                                tag: 'cluster',
                                start: new Date(start),
                                end: new Date(end),
                                average: avg.toPrecision(2),
                                color,
                            });
                        }
                    }
                }
                return fromNullable(
                    dataElem.ts.find(
                        point => Math.abs(point.date - start) < 60000
                    )
                ).map(point => ({
                    dataId: dataElem.identifier,
                    tag: 'point',
                    point,
                    color,
                }));
            }
        )
    );

export const getZoomWindow = (poc: ClusterInfo) => {
    const timeserie = getTimeserieBetweenDates(poc.dataId).map(
        dataElem => dataElem.ts
    );
    const nb_elem = timeserie.fold(0, ts => ts.length);
    const nb_left = timeserie.fold(
        0,
        ts => ts.filter(ts => ts.date < poc.start.getTime()).length
    );
    const nb_in_cluster =
        timeserie.fold(
            0,
            ts => ts.filter(ts => ts.date <= poc.end.getTime() + 1).length
        ) - nb_left;
    const s = timeserie.map(ts => {
        const testIndex =
            Math.round(nb_left - (MAXLENGTH - nb_in_cluster) / 2) + 1;
        const index = testIndex > 0 ? testIndex : 0;
        return ts[index].date;
    });
    const e = timeserie.map(ts => {
        const testIndex =
            Math.round(
                nb_left + nb_in_cluster + (MAXLENGTH - nb_in_cluster) / 2
            ) - 1;
        const index = testIndex < nb_elem - 1 ? testIndex : nb_elem - 1;
        return ts[index].date;
    });
    return { s, e };
};

const COLORS = [
    '#335496',
    '#42dd73',
    '#ce3943',
    '#df37c3',
    '#ff9c3a',
    '#373e46',
    '#c8e6cf',
    '#191760',
    '#b3e5b5',
    '#2ba6ff',
];
export const getColor = (i: number) => {
    const colorIndex = i % COLORS.length;
    return COLORS[colorIndex];
};

const foldParameterType =
    <T>(surface: () => T, gquant: () => T, gqual: () => T) =>
    (kind: ParameterType) => {
        switch (kind) {
            case 'surface':
                return surface();
            case 'ground-quantity':
                return gquant();
            case 'ground-quality':
                return gqual();
        }
    };

const parameterListGetter = foldParameterType(
    getSurfaceParameterList,
    getGroundQuantityParameterList,
    getGroundQualityParameterList
);

export const findParameter = memo((kind: ParameterType, id: number) => {
    const ps = parameterListGetter(kind);
    return fromNullable(ps.find(p => p.id === id)).map(p => ({
        ...p,
        kind,
    }));
});

// export const findParameterSurface = (id: number) => {
//     const ps = getSurfaceParameterList();
//     return fromNullable(ps.find(p => p.id === id)).map(p => {
//         p;
//     });
// };

export const getParameterSearch = () =>
    fromNullable(query('timeserie/parameter/search')).getOrElse('');
export const getStationSearch = () =>
    fromNullable(query('timeserie/station/search')).getOrElse('');

const withDataParam = (kind: ParameterType) => (param: Parameter) => {
    if (getMode() === 'param' || kind === 'ground-quantity') {
        return true;
    }
    const p = param as CountedParameter;
    return p.count !== undefined && p.count !== NO_DATA && p.count !== 0;
};

export const getParameterList = (kind: ParameterType) => ({
    kind,
    params: parameterListGetter(kind)
        .filter(withDataParam(kind))
        .filter(p => {
            const normalized = fromRecord(p.name).toLocaleLowerCase();
            const normalized_search = getParameterSearch().toLocaleLowerCase();
            return (
                normalized.includes(normalized_search) ||
                normalized_search === ''
            );
        }),
});

export const getParameterFilteredList = (kind: ParameterType) =>
    getSelectedGroupOpt().fold(getParameterList(kind), group => {
        const pl = getParameterList(kind);
        return {
            kind,
            params: pl.params.filter(
                p => p.groups.findIndex(g => g.id === group.id) >= 0
            ),
        };
    });

export const getGroupListSurface = () => getGroupList('surface');

export const getGroupListGround = () => getGroupList('ground-quality');
// [
//     {
//         id: -1,
//         name: { fr: 'piezométrie', nl: 'nltodo' } as MessageRecord,
//         sort: 0,
//     },
// ].concat(getGroupList('ground-quality'));

export const getGroupList = (kind: ParameterType) =>
    groupSort(
        uniqIdentified(
            flatten(getParameterList(kind).params.map(p => p.groups))
        )
        // uniqIdentified(flatten(parameterListGetter(kind).map(p => p.groups)))
    );
export const getSelectedGroup = () => query('timeserie/group/select');

export const getSelectedGroupOpt = () => fromNullable(getSelectedGroup());

/**
 *
 * @param id Parameter ID
 */
export const getTimeserie = (id: number) =>
    getSelectedStation()
        .map(s => timeserieKey(s.kind, s.id, id))
        .chain(key => fromNullable(getDataTimeseries()[key]))
        .getOrElse(remoteNone);

/**
 *
 * @param id Station ID
 */
export const getTimeserieForStation = (id: number | string) =>
    getSelectedParameter()
        .map(p => timeserieKey(p.kind, id, p.id))
        .chain(key => fromNullable(getDataTimeseries()[key]))
        .getOrElse(remoteNone);

// export const getTimeserie = (id: number) => {
//     const [begin, end] = getDateWindow().getOrElse([new Date, new Date]);
//     const timeserie = getSelectedStation()
//         .map(([kind, station]) => timeserieKey(kind, station, id))
//         .map(key => getDataTimeseries()[key]);
//     return timeserie
//         .chain(ts =>
//             remoteToOption(ts)
//                 .map(points => points
//                     .filter(p =>
//                         tsTime(p) >= begin && tsTime(p) < end)
//                 )).getOrElse([]);
// }

const getOptionWaterbody = getNormOption<number>('waterbody');

const selectNormsSurface = (norms: Norm[], feature: Feature): Norm[] =>
    getFeaturePropOption<number>(feature, 'id_waterbody')
        .map(featureWid => {
            const filter = (n: Norm) =>
                getOptionWaterbody(n)
                    .map(wid => wid === featureWid)
                    .getOrElse(false);
            return norms.filter(filter);
        })
        .getOrElse([]);

const selectNormsGround = (norms: Norm[], feature: Feature): Norm[] =>
    getFeaturePropOption<number>(feature, 'ref_gwb')
        .map(featureWid => {
            const filter = (n: Norm) =>
                getOptionWaterbody(n)
                    .map(wid => wid === featureWid)
                    .getOrElse(false);
            return norms.filter(filter);
        })
        .getOrElse([]);

const selectNormsPiezo = (_norms: Norm[], _feature: Feature): Norm[] => {
    return [];
};

const normToPlotNorm = (norm: Norm): PlotNorm[] =>
    mapCollection((name, value) => ({ name, value }), norm.values);

const getNormsFromFeature = (param: ParameterWithKind, feature: Feature) => {
    const { norms } = param;
    switch (param.kind) {
        case 'surface':
            return selectNormsSurface(norms, feature);
        case 'ground-quality':
            return selectNormsGround(norms, feature);
        case 'ground-quantity':
            return selectNormsPiezo(norms, feature);
    }
};

export const getPlotNorms = (pid: number) =>
    scopeOption()
        .let('station', getSelectedStation())
        .let('param', ({ station }) => findParameter(station.kind, pid))
        .let('feature', ({ station }) => getFeatureFromSelection(station))
        .map(({ param, feature }) => getNormsFromFeature(param, feature))
        .map(norms => flatten(norms.map(normToPlotNorm)))
        .getOrElse([]);

export const getPlotNormsForStation = (sid: number) =>
    scopeOption()
        .let('station', findStation(sid))
        .let('param', ({ station }) =>
            getSelectedParameter().chain(p => findParameter(station.kind, p.id))
        )
        .let('feature', ({ station }) => getFeatureFromSelection(station))
        .map(({ param, feature }) => getNormsFromFeature(param, feature))
        .map(norms => flatten(norms.map(normToPlotNorm)))
        .getOrElse([]);

export const tsToData = (pid: number, ts: TimeserieData): PlotDataElement => {
    const identifier = pid;
    const points: PlotPoint[] = ts.map(t => ({
        date: tsTime(t),
        value: tsValue(t),
        factor: tsFactor(t),
    }));
    const norms = getPlotNorms(pid);

    return { identifier, ts: points, norms, clusterized: false };
};

export const tsToDataForStation = (
    sid: number,
    ts: TimeserieData
): PlotDataElement => {
    const identifier = sid;
    const points: PlotPoint[] = ts.map(t => ({
        date: tsTime(t),
        value: tsValue(t),
        factor: tsFactor(t),
    }));
    const norms = getPlotNormsForStation(sid);

    return { identifier, ts: points, norms, clusterized: false };
};

export const getNormSelection = () =>
    fromNullable(query('timeserie/norm/select'));

export const getSelectedNorms = (norms: PlotNorm[]) =>
    norms.filter(n =>
        getNormSelection()
            .map(
                selectedNorms =>
                    selectedNorms.find(
                        sn =>
                            sn.norm.name == n.name && sn.norm.value === n.value
                    ) !== undefined
            )
            .getOrElse(false)
    );

const isSameNorm = (normSelect1: NormSelection, normSelect2: NormSelection) =>
    normSelect1.paramId === normSelect1.paramId &&
    normSelect1.norm.name === normSelect2.norm.name &&
    normSelect1.norm.value === normSelect2.norm.value;

export const isSelectedNorm = (normSelection: NormSelection) =>
    getNormSelection()
        .map(norms => norms.findIndex(ns => isSameNorm(ns, normSelection)) >= 0)
        .getOrElse(false);

export const getTimeserieAsPlotData = (id: number) => {
    const remote = (
        toData: (id: number, d: TimeserieData) => PlotDataElement
    ) =>
        foldRemote<TimeserieData, string, RemoteResource<PlotDataElement>>(
            () => remoteNone,
            () => remoteLoading,
            err => remoteError(err),
            data => remoteSuccess(toData(id, data))
        );
    switch (getDisplayMode()) {
        case 'per-parameter':
            return remote(tsToDataForStation)(getTimeserieForStation(id)); // id is a station id
        case 'per-station':
            return remote(tsToData)(getTimeserie(id)); // id is a param id
    }
};

export interface BetweenDates {
    identifier: number;
    ts: PlotPoint[];
    norms: PlotNorm[];
    clusterized: boolean;
}

export const getTimeserieBetweenDates = (id: number) => {
    const timeserieOpt = remoteToOption(getTimeserieAsPlotData(id));
    const defaultBegin = timeserieOpt
        .map(dataElem => dataElem.ts)
        .filter(ts => ts.length > 0)
        .map(ts => ts[0].date)
        .getOrElse(0);
    const defaultEnd = timeserieOpt
        .map(dataElem => dataElem.ts)
        .filter(ts => ts.length > 0)
        .map(ts => ts[ts.length - 1].date)
        .getOrElse(new Date().getTime());
    const begin = getWindowStart()
        .map(({ date }) => date.getTime())
        .getOrElse(defaultBegin);
    const end = getWindowEnd()
        .map(({ date }) => date.getTime())
        .getOrElse(defaultEnd);

    return timeserieOpt.map<BetweenDates>(dataElem => ({
        identifier: dataElem.identifier,
        ts: dataElem.ts.filter(
            point => point.date >= begin && point.date <= end
        ),
        norms: dataElem.norms,
        clusterized: false,
    }));
};

/**
 * Returns an array filled with 'undefined'. This way the array can be filled
 * with a map or reduce
 * @param size
 */
export const mappableEmptyArray = (size: number): undefined[] =>
    Array(size).fill(undefined);

const clusterizeTS = (timeserie: PlotPoint[], len: number) => {
    const baseSampleSize = Math.floor(len / MAXLENGTH);
    const restChunk = (len - baseSampleSize * MAXLENGTH) / MAXLENGTH;
    let rest = 0;
    let k = 0;
    return mappableEmptyArray(MAXLENGTH).map(() => {
        rest += restChunk;
        let sampleSize = baseSampleSize;

        if (rest >= 1) {
            sampleSize = baseSampleSize + 1;
            rest -= 1;
        }

        let sum = 0;
        let samples = 0;
        const limit = Math.min(k + sampleSize, len);

        for (let i = k; i < limit; i += 1) {
            const val = timeserie[i].value;
            if (val !== null) {
                sum += val * timeserie[i].factor;
                samples += 1;
            }
        }

        const average = samples > 0 ? sum / samples : 0;
        const refDate = timeserie[k].date;
        k += sampleSize;

        return { date: refDate, value: average, factor: 1 };
    });
};

export const getSimplifiedTimeserie = (id: number) =>
    getTimeserieBetweenDates(id).map(dataElem => {
        const len = dataElem.ts.length;
        let timeserie = dataElem.ts;
        let clusterized = false;
        if (len > MAXLENGTH) {
            timeserie = clusterizeTS(timeserie, len);
            clusterized = true;
        }
        return {
            identifier: dataElem.identifier,
            ts: timeserie,
            norms: dataElem.norms,
            clusterized,
        };
    });

// export const getAllSelectedData = () =>
//     getSelectedParameter().reduce<PlotData>(
//         (acc, { id }) =>
//             remoteToOption(getTimeserieAsPlotData(id)).fold(acc, ts =>
//                 acc.concat(ts)
//             ),
//         []
//     );

export const getTimeserieDataStatus = (id: number): TimeserieDataStatus =>
    scopeOption()
        .let('ts', remoteToOption(getTimeserie(id)))
        .let('wts', getTimeserieBetweenDates(id))
        .map(({ ts, wts }) => {
            if (ts.length === 0) {
                return 'no-data';
            } else if (wts.ts.length === 0) {
                return 'not-in-window';
            } else {
                return 'has-data';
            }
        })
        .getOrElse('no-data');

export const getTimeserieDataStatusForStation = (
    id: number | string
): TimeserieDataStatus =>
    scopeOption()
        .let('idnumber', tryNumber(id))
        .let('ts', ({ idnumber }) =>
            remoteToOption(getTimeserieForStation(idnumber))
        )
        .let('wts', ({ idnumber }) => getTimeserieBetweenDates(idnumber))
        .map(({ ts, wts }) => {
            if (ts.length === 0) {
                return 'no-data';
            } else if (wts.ts.length === 0) {
                return 'not-in-window';
            } else {
                return 'has-data';
            }
        })
        .getOrElse('no-data');

export const getStationName = (s: StationSelection) => {
    switch (s.kind) {
        case 'ground-quality':
            return getFeatureFromSelection(s)
                .chain<string>(f => getFeaturePropOption(f, 'no_code'))
                .getOrElse('na');
        case 'ground-quantity':
            return getFeatureFromSelection(s)
                .chain<string>(f => getFeaturePropOption(f, 'nom'))
                .getOrElse('na');
        case 'surface':
            return getFeatureFromSelection(s)
                .chain<string>(f => getFeaturePropOption(f, 'gid_site'))
                .getOrElse('na');
    }
};

export const getSelectedStationName = () =>
    getSelectedStation().map(getStationName);

export const GROUND_WATERBODIES = [
    { id: 1, color: '#94d050' },
    { id: 2, color: '#868686' },
    { id: 3, color: '#ff6600' },
    { id: 4, color: '#ff99ff' },
    { id: 5, color: '#ffff00' },
    { id: 6, color: '#465096' },
    { id: 7, color: '#465096' },
];

export const getSelectedWaterBody = (kind: ParameterType) =>
    fromNullable(query('timeserie/waterbody/select')).chain(swb =>
        swb.pType === kind ? some(swb.wb) : none
    );

export const getGroundWaterBodies = () =>
    query('data/waterbodies').filter(
        wb =>
            wb.kind === 'ground' && GROUND_WATERBODIES.find(b => b.id === wb.id)
    );
// const blue = (n: number) => `hsl(${160 + (7 * n)}, ${100 - (n * 5)}%, ${20 + (n * 4)}%)`;

// const SURFACE_MARKER_ICON: IconName[] = [
//     'circle',
//     'square',
//     'dot-circle'
// ];
// export const surfaceMarkerIcon = (id: number) => {
//     const idx = id % SURFACE_MARKER_ICON.length;
//     return SURFACE_MARKER_ICON[idx];
// };

/*
 *
 * 1;"GEL";"Geleytsbeek";"Geleytsbeek";"Geleytsbeek";""
 * 2;"HOL";"Hollebeek";"Hollebeek";"Hollebeek";""
 * >> 3;"KAN";"Canal";"Kanaal";"Canal";""
 * 4;"LEI";"Leibeek";"Leibeek";"Leibeek";""
 * 5;"LIN";"Linkebeek";"Linkebeek";"Linkebeek";""
 * 6;"MOL";"Molenbeek";"Molenbeek";"Molenbeek";""
 * 7;"ND";"No_data";"No_data";"No_data";""
 * 8;"NEE";"Neerpedebeek";"Neerpedebeek";"Neerpedebeek";""
 * 9;"ROO";"Roodkloosterbeek";"Roodkloosterbeek";"Roodkloosterbeek";""
 * 10;"VOG";"Vogelzangbeek";"Vogelzangbeek";"Vogelzangbeek";""
 * >> 11;"WOL";"Woluwe";"Woluwe";"Woluwe";""
 * >> 12;"ZEN";"Senne";"Zenne";"Zenne";""
 * 13;"ETA";"étangs";"vijvers";"ponds";""
 * 14;"REJ";"rejets";"lozingen";"discharges";""
 *
 */

export const getSurfcaeWaterbodyColor = (code: string) => {
    switch (code) {
        case 'KAN':
            return 'rgb(60, 220, 175)';
        case 'WOL':
            return 'rgb(215, 185, 90)';
        case 'ZEN':
            return 'rgb(150, 90, 220)';
        default:
            return 'rgb(200, 115, 60)';
    }
};

export const mainSurfaceWaterbodies = () =>
    query('data/waterbodies').filter(
        ({ kind, code }) =>
            kind === 'surface' &&
            (code === 'KAN' || code === 'WOL' || code === 'ZEN')
    );

export const otherSurfaceWaterbodies = () =>
    query('data/waterbodies').filter(
        ({ kind, code }) =>
            kind === 'surface' &&
            code !== 'KAN' &&
            code !== 'WOL' &&
            code !== 'ZEN'
    );

export const findGroundWaterBody = (id: number) =>
    fromNullable(
        query('data/waterbodies').find(
            wb => wb.kind === 'ground' && wb.id === id
        )
    );

export const findSurfaceWaterBody = (id: number) =>
    fromNullable(
        query('data/waterbodies').find(
            wb => wb.kind === 'surface' && wb.id === id
        )
    );

export const findSurfaceWaterBodyByCode = (code: string) =>
    fromNullable(
        query('data/waterbodies').find(
            wb => wb.kind === 'surface' && wb.code === code
        )
    );

const getSurfaceWaterBody = (feature: Feature) =>
    getFeaturePropOption(feature, 'id_waterbody').chain(wb => tryNumber(wb));

const getGroundWaterBody = (feature: Feature) =>
    getFeaturePropOption(
        feature,
        getLang() === 'nl' ? 'naam_gwb_nl' : 'nom_gwb_fr'
    );

export const isInSurfaceWaterbody = (waterbody: WaterBody, feature: Feature) =>
    getSurfaceWaterBody(feature).fold(
        false,
        wb => wb === waterbody.id && waterbody.kind === 'surface'
    );

export const isInGroundWaterbody = (waterbody: WaterBody, feature: Feature) =>
    getGroundWaterBody(feature).fold(
        false,
        wb =>
            (getLang() === 'fr'
                ? wb === waterbody.name.fr
                : wb === waterbody.name.nl) && waterbody.kind === 'ground'
    );

export const getWaterbodyFeatures = (
    waterbodies: WaterBody[],
    kind: ParameterType
): Feature[] => {
    const features: Feature[] = [];
    switch (kind) {
        case 'ground-quality':
            return flatten(
                waterbodies.map(wb =>
                    features.concat(
                        getFeatureCollectionGQuality().fold([], fc =>
                            fc.features.filter(f => isInGroundWaterbody(wb, f))
                        )
                    )
                )
            );

        case 'ground-quantity':
            return flatten(
                waterbodies.map(wb =>
                    features.concat(
                        getFeatureCollectionGQuantity().fold([], fc =>
                            fc.features.filter(f => isInGroundWaterbody(wb, f))
                        )
                    )
                )
            );
        case 'surface':
            return flatten(
                waterbodies.map(wb =>
                    features.concat(
                        getFeatureCollectionSurface().fold([], fc =>
                            fc.features.filter(f => isInSurfaceWaterbody(wb, f))
                        )
                    )
                )
            );
    }
};

/**
 * We need to special case on absolute piezo because it's bilingual and we didnt design for it
 *
 * @param param
 */
export const getParamUnit = (param: Parameter) =>
    param.id === 2 && param.code === 'abs'
        ? some(tr.ts('piezoAbsoluteUnit'))
        : fromNullable(param.unit);

logger('loaded');
