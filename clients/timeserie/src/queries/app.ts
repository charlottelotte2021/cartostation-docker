import { query } from 'sdi/shape';
import { fromNullable } from 'fp-ts/lib/Option';

export const getLayout = () => query('app/layout');

export const getUsername = () =>
    fromNullable(query('data/user')).map(u => u.name);
