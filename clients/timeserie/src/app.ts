import * as debug from 'debug';
import { loop } from 'sdi/app';
import { DIV } from 'sdi/components/elements';
import header from 'sdi/components/header';
import { loadAllServices } from 'sdi/geocoder/events';

import { getLayout } from './queries/app';
import { loadRoute } from './events/route';
import {
    loadAllParameters,
    loadNormNames,
    loadWaterBodies,
} from './events/timeserie';
import { loadLayers, loadHydro } from './events/map';

import footer from './components/footer';
import modal from './components/modal';
import indexInit from './components/view/index-init';
import index from './components/view';
import config from './components/view/config';
import table from './components/view/table';
import plot from './components/view/plot';
import splash from './components/view/splash';

const logger = debug('sdi:events/map');

// const devTool =
//     () => DIV({},
//         makeLabel('navigate', 3, () => tr.ts('home'))(navigateIndex),
//         makeLabel('navigate', 3, () => tr.ts('config'))(navigateConfig),
//         makeLabel('navigate', 3, () => tr.ts('table'))(navigateTable),
//         makeLabel('navigate', 3, () => tr.ts('plot'))(navigatePlot),
//     );

const wrappedMain = (
    name: string,
    // eslint-disable-next-line @typescript-eslint/ban-types
    ...elements: React.DOMElement<{}, Element>[]
) =>
    DIV(
        { className: 'timeserie-inner' },
        modal(),
        header('timeserie'),
        DIV({ className: `main ${name}` }, ...elements),
        footer()
    );

const renderSplash = () => wrappedMain('splash', splash());

const renderIndexInit = () => wrappedMain('index-init', indexInit());

const renderIndex = () => wrappedMain('index', index());

const renderConfig = () => wrappedMain('config', config());

const renderTable = () => wrappedMain('table', table());

const renderPlot = () => wrappedMain('plot', plot());

const render = () => {
    const layout = getLayout();
    switch (layout) {
        case 'splash':
            return renderSplash();
        case 'indexInit':
            return renderIndexInit();
        case 'index':
            return renderIndex();
        case 'config':
            return renderConfig();
        case 'table':
            return renderTable();
        case 'plot':
            return renderPlot();
    }
};

const effects = (initialRoute: string[]) => () => {
    loadAllServices();
    loadAllParameters()
        .then(() => loadRoute(initialRoute))
        .then(loadLayers)
        .then(loadNormNames)
        .then(loadWaterBodies)
        .then(loadHydro);
};

export const app = (initialRoute: string[]) =>
    loop('timeserie', render, effects(initialRoute));

logger('loaded');
