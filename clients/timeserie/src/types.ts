import { PlotNorm } from './components/plot/types';

export type ParameterType = 'surface' | 'ground-quality' | 'ground-quantity';
export interface ParameterSelection {
    kind: ParameterType;
    id: number;
}

export interface ParameterSelectionRaw {
    id: number;
}

export interface StationSelection {
    kind: ParameterType;
    id: number | string;
}

export interface PointSelection {
    id: number;
    start: number;
    end: number;
    x: number;
    y: number;
    color: string;
}

export interface NormSelection {
    paramId: number;
    norm: PlotNorm;
}

export interface DateSelection {
    date: Date;
    isAutomatic: boolean;
}

export const waterMapId = 'water-map';

export type TableMode = 'per-station' | 'per-parameter';

export type TimeserieDataStatus = 'no-data' | 'not-in-window' | 'has-data';

export type VisibleGroundLayer = 'quality' | 'quantity';
