import {
    IMapViewData,
    Interaction,
    defaultInteraction,
    IViewEvent,
} from 'sdi/map';
import { Inspire, ILayerInfo, FeatureCollection } from 'sdi/source';
import { Collection } from 'sdi/util';

declare module 'sdi/shape' {
    export interface IShape {
        'port/map/view': IMapViewData;
        'port/map/interaction': Interaction;

        'data/layer-infos': ILayerInfo[];
        'data/metadatas': Inspire[];
        'data/layers': Collection<FeatureCollection>;

        'water/hydro-layer/visible': Collection<boolean>;
    }
}

export const defaultMapView = (): IMapViewData => ({
    dirty: 'geo',
    srs: 'EPSG:31370',
    center: [148885, 170690],
    rotation: 0,
    zoom: 6,
    feature: null,
    extent: null,
});

export const defaultMapEvent = (): IViewEvent => ({
    dirty: 'geo',
    center: [148885, 170690],
    rotation: 0,
    zoom: 6,
});

export const defaultMapState = () => ({
    'port/map/view': defaultMapView(),
    'port/map/interaction': defaultInteraction(),
    'data/layer-infos': [],
    'data/metadatas': [],
    'data/layers': {},
    'water/hydro-layer/visible': {},
});
