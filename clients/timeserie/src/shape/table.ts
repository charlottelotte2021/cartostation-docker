import { TableState, defaultTableState } from 'sdi/components/table2';

declare module 'sdi/shape' {
    export interface IShape {
        'component/table': TableState;
    }
}

export const defaultTableShape = () => ({
    'component/table': defaultTableState(),
});
