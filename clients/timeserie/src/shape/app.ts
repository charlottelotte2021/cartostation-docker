import { Nullable } from 'sdi/util';
import { IUser } from 'sdi/source';
import { Layout } from '../events/route';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'app/layout': Layout;
        'data/user': Nullable<IUser>;
    }
}

export const defaultAppShape = () => ({
    'app/layout': 'splash' as Layout,
    'data/user': null,
});
