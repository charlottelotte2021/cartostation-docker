import * as debug from 'debug';
import { index } from 'fp-ts/lib/Array';
import { Router, Path } from 'sdi/router';
import { activityLogger, setLayout } from './app';
import {
    selectParameter,
    selectWindowStart,
    selectWindowEnd,
    lidToParameterType,
    setLevel,
    selectStations,
    selectNorm,
    clearParameterSelection,
    clearStationSelection,
    clearSelectedGroup,
    clearNormSelection,
    clearPointSelection,
    clearWindow,
    clearSelectedParameter,
    loadParameterCount,
    setMode,
    loadStationCount,
    defaultSelectedParameter,
} from './timeserie';
import * as io from 'io-ts';
import { scopeOption } from 'sdi/lib';
import {
    Option,
    fromEither,
    none,
    fromNullable,
    some,
    fromPredicate,
} from 'fp-ts/lib/Option';
import {
    getSelectedKind,
    getSelectedParameterList,
    getWindowStart,
    getWindowEnd,
    getSelectedStationListId,
    getPlotNorms,
    getCurrentFeature,
    getSelectedStationList,
    getMode,
} from '../queries/timeserie';
import { ParameterType } from '../types';
import { getLayout } from '../queries/app';
import { FeaturePath } from 'sdi/map';
import {
    mountGroundLayers,
    mountSurfaceLayers,
    unmountGroundLayer,
    unmountSurfaceLayer,
    centerOn,
    updateLayers,
    removeVisibleGroundLayer,
} from './map';
import { Coordinate } from 'ol/coordinate';
import { selectWaterLevelAction } from 'sdi/activity';

const logger = debug('sdi:route');

// tslint:disable-next-line: variable-name
const RouteIO = io.union([
    io.literal('indexInit'),
    io.literal('index'),
    io.literal('config'),
    io.literal('table'),
    io.literal('plot'),
]);
export type Route = io.TypeOf<typeof RouteIO>;

export const { home, route, navigate } = Router<Route>('timeserie');

export type Layout = Route | 'splash';

// tslint:disable-next-line:variable-name
export const ConfigIO = io.interface(
    {
        k: io.union([io.null, io.string]), // kind
        p: io.array(io.Integer), // parameters
        s: io.array(io.Integer), // stations
        w: io.union([
            io.null,
            io.tuple([io.Integer, io.boolean, io.Integer, io.boolean]),
        ]), // window
    },
    'Config'
);
export type Config = io.TypeOf<typeof ConfigIO>;

export const encodeConfig = (c: Config) =>
    encodeURIComponent(btoa(JSON.stringify(c)));
export const decodeConfig = (s: string) => {
    try {
        const ds = atob(decodeURIComponent(s));
        const d = JSON.parse(ds);
        return fromEither(ConfigIO.decode(d));
    } catch (_e) {
        return none;
    }
};

// tslint:disable-next-line:variable-name
export const LevelIO = io.union(
    [io.literal('ground'), io.literal('surface')],
    'LevelIO'
);

export type Level = io.TypeOf<typeof LevelIO>;

export const ModeIO = io.union(
    [io.literal('station'), io.literal('param')],
    'ModeIO'
);

export type Mode = io.TypeOf<typeof ModeIO>;

export const getConfig = (): Option<Config> =>
    some({
        k: getSelectedKind().toNullable(),
        p: getSelectedParameterList().map(p => p.id),
        s: getSelectedStationListId(),
        w: getWindowStart()
            .chain(s =>
                getWindowEnd().map<[number, boolean, number, boolean]>(e => [
                    s.date.getTime(),
                    s.isAutomatic,
                    e.date.getTime(),
                    e.isAutomatic,
                ])
            )
            .toNullable(),
    });

export const configSetKind = (kind: ParameterType) =>
    getConfig().map(config => ({
        k: kind,
        p: config.p,
        s: config.s,
        w: config.w,
    }));

export const configAddStations = (ids: number[], kind?: ParameterType) => {
    if (kind) {
        return getConfig().map(config => ({
            ...config,
            k: kind,
            s: ids,
        }));
    } else {
        return getConfig().map(config => ({
            ...config,
            s: ids,
        }));
    }
};

export const configRemoveStation = (rid: number) =>
    getConfig().map(config => ({
        ...config,
        s: config.s.filter(id => id !== rid),
    }));

export const configAddParameter = (id: number, kind: ParameterType) =>
    getConfig().map(config => ({
        ...config,
        k: kind, // needed when we didn't had a kind before
        p: config.p.concat(id),
    }));

export const configRemoveParameter = (rid: number) =>
    getConfig().map(config => ({
        ...config,
        p: config.p.filter(id => id !== rid),
    }));

export const configAddWindow = (
    start: number,
    startIsAuto: boolean,
    end: number,
    endIsAuto: boolean
): Option<Config> =>
    getConfig().map(config => ({
        ...config,
        w: [start, startIsAuto, end, endIsAuto],
    }));

const levelRouteParserReq = (p: Path) => {
    return scopeOption()
        .let(
            'level',
            index(0, p).chain(l => fromEither(LevelIO.decode(l)))
        )
        .let(
            'mode',
            index(1, p).chain(m => fromEither(ModeIO.decode(m)))
        );
};

const configRouterParserOpt = (p: Path) =>
    levelRouteParserReq(p).let('config', index(2, p).map(decodeConfig));

const configRouterParserReq = (p: Path) =>
    levelRouteParserReq(p).let('config', index(2, p).chain(decodeConfig));

const mountLayers = (level: Level) => {
    if (level === 'surface') {
        mountSurfaceLayers();
        unmountGroundLayer();
    } else if (level === 'ground') {
        mountGroundLayers();
        unmountSurfaceLayer();
    }
};

/// << route handlers

home(
    'index',
    route => {
        route.fold(setLayout('indexInit'), ({ level, mode }) => {
            setLevel(level);
            setMode(mode);
            setLayout('index');
            mountLayers(level);
            onlyForParamModeOpt(mode).map(() => {
                removeVisibleGroundLayer('quantity');
                // updateLayers(level);
            });
        });
    },
    levelRouteParserReq
);

export const onlyForStationModeOpt = fromPredicate(
    (m: Mode) => m === 'station'
);
export const onlyForParamModeOpt = fromPredicate((m: Mode) => m === 'param');

const setKindForParamMode = (level: Level) =>
    onlyForParamModeOpt(getMode()).chain(() => {
        switch (level) {
            case 'surface':
                return configSetKind('surface');
            case 'ground':
                return configSetKind('ground-quality');
        }
    });

route(
    'config',
    route => {
        route.map(({ level, config, mode }) => {
            const selectedStations: FeaturePath[] = [];
            clearParameterSelection();
            clearStationSelection();
            setLevel(level);
            mountLayers(level);
            setMode(mode);
            config.map(({ k, p, s, w }) => {
                p.map(param => {
                    fromNullable(k)
                        .chain(lidToParameterType)
                        .map(k => selectParameter(k, param));
                    loadStationCount(level, param);
                });
                s.map(station => {
                    selectedStations.push({ layerId: k, featureId: station });
                    loadParameterCount(level, station);
                });
                fromNullable(w).map(w => {
                    selectWindowStart(new Date(w[0]), w[1]);
                    selectWindowEnd(new Date(w[2]), w[3]);
                });
            });
            setKindForParamMode(level);
            selectStations(selectedStations);
            onlyForParamModeOpt(mode).map(() => {
                removeVisibleGroundLayer('quantity');
                updateLayers(level);
            });

            setLayout('config');

            if (getSelectedStationList().length === 1) {
                window.setTimeout(
                    () =>
                        getCurrentFeature().map(({ geometry }) => {
                            const { type, coordinates } = geometry;
                            if (type === 'Point') {
                                centerOn(coordinates as Coordinate);
                            }
                        }),
                    800
                );
            }
            defaultSelectedParameter();
        });
    },
    configRouterParserOpt
);

route(
    'table',
    route => {
        route.map(({ level, config }) => {
            const selectedStations: FeaturePath[] = [];
            clearParameterSelection();
            clearStationSelection();
            setLevel(level);
            config.p.map(param =>
                fromNullable(config.k).chain(kind =>
                    lidToParameterType(kind).map(k => {
                        selectParameter(k, param);
                        getPlotNorms(param).map(n => selectNorm(param, n));
                    })
                )
            );
            config.s.map(station =>
                fromNullable(config.k).chain(kind =>
                    lidToParameterType(kind).map(k =>
                        selectedStations.push({
                            layerId: k,
                            featureId: station,
                        })
                    )
                )
            );
            fromNullable(config.w).map(w => {
                selectWindowStart(new Date(w[0]), w[1]);
                selectWindowEnd(new Date(w[2]), w[3]);
            });
            selectStations(selectedStations);
            setLayout('table');
            defaultSelectedParameter();
        });
    },
    configRouterParserReq
);

route(
    'plot',
    route => {
        route.map(({ level, config }) => {
            const selectedStations: FeaturePath[] = [];
            clearParameterSelection();
            clearStationSelection();
            setLevel(level);
            config.p.map(param =>
                fromNullable(config.k).chain(kind =>
                    lidToParameterType(kind).map(k => {
                        selectParameter(k, param);
                        getPlotNorms(param).map(n => selectNorm(param, n));
                    })
                )
            );
            config.s.map(station =>
                fromNullable(config.k).chain(kind =>
                    lidToParameterType(kind).map(k =>
                        selectedStations.push({
                            layerId: k,
                            featureId: station,
                        })
                    )
                )
            );
            fromNullable(config.w).map(w => {
                selectWindowStart(new Date(w[0]), w[1]);
                selectWindowEnd(new Date(w[2]), w[3]);
            });
            defaultSelectedParameter();
            selectStations(selectedStations);
            setLayout('plot');
        });
    },
    configRouterParserReq
);

/// route handlers >>

const defaultRoute = () => navigate('index', []);

export const loadRoute = (initial: string[]) =>
    index(0, initial).foldL(defaultRoute, prefix =>
        RouteIO.decode(prefix).fold(defaultRoute, c =>
            navigate(c, initial.slice(1))
        )
    );

export const navigateIndex = (level: Level, mode = 'station' as Mode) => {
    activityLogger(selectWaterLevelAction(level));
    clearParameterSelection();
    clearStationSelection();
    clearSelectedGroup();
    clearSelectedParameter();
    clearNormSelection();
    clearPointSelection();
    clearWindow();

    navigate('index', [level, mode]);
};

export const navigateTable = (level: Level, config: Config, mode: Mode) =>
    navigate('table', [level, mode, encodeConfig(config)]);

export const navigatePlot = (level: Level, config: Config, mode: Mode) =>
    navigate('plot', [level, mode, encodeConfig(config)]);

// export const navigateConfigInit = (
//     level: Level,
// ) => navigate('config', [level, 'IMUNPARSABLE']);

export const navigateConfig = (level: Level, config: Config, mode: Mode) => {
    if (mode === 'param' && config.k === null) {
        const configKind =
            level === 'ground'
                ? configSetKind('ground-quality')
                : configSetKind('surface');
        configKind.map(c => navigate('config', [level, mode, encodeConfig(c)]));
    } else {
        navigate('config', [level, mode, encodeConfig(config)]);
    }
};

export const navigateInPlace = (level: Level, config: Config, mode: Mode) => {
    switch (getLayout()) {
        case 'index':
        case 'config':
            navigateConfig(level, config, mode);
            break;
        case 'plot':
            navigatePlot(level, config, mode);
            break;
        case 'table':
            navigateTable(level, config, mode);
            break;
    }
};

logger('loaded');
