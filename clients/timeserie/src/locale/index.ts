import { MessageStore, fromRecord } from 'sdi/locale';

const messages = {
    // appName: {
    //     fr: 'Séries temporelles',
    //     nl: 'Tijdreeksen',
    // },

    appName: {
        fr: 'BruWater',
        nl: 'BruWater',
    },

    canTakeTime: {
        fr: `Ceci peut prendre quelques minutes lors du chargement initial,  
        merci de patienter.`,
        nl: `Het herladen kan enkele minuten duren, gelieve even te wachten.`, // done
    },

    home: {
        fr: 'accueil',
        nl: 'homepage', // done
    },

    backHome: {
        fr: "retour à l'accueil",
        nl: 'terug naar homepage', // done
    },

    resetDate: {
        fr: 'Ré-initialiser',
        nl: 'Reset', // done
    },

    displayFrom: {
        fr: 'Du',
        nl: 'Van', // done
    },

    displayTo: {
        fr: 'au',
        nl: 'tot', // done
    },

    dataNotInWindowAvailableFrom: {
        fr: `Il n'y a pas de données pour l'intervalle temporel choisi. Les données couvrent la période du`,
        nl: 'Er zijn geen gegevens voor het geselecteerde tijdsinterval. De gegevens hebben betrekking op de periode vanaf ', // done
    },

    dataAvailableFrom: {
        fr: 'Jeu de données du ',
        nl: 'Gegevensset van de', // done
    },

    dataAvailableTo: {
        fr: 'au ',
        nl: 'tot', // nltodo
    },

    noDataForStation: {
        fr: `Il n'y a pas de données pour le site sélectionné.`,
        nl: 'Er zijn geen gegevens voor de geselecteerde meetpunt.', // done
    },

    noStationForParam: {
        fr: `Il n'y a pas de sites pour le paramètre sélectionné.`,
        nl: `Er zijn geen meetpunten voor de geselecteerde parameter`, //todo
    },
    selectedPointDateLabel: {
        fr: 'le',
        nl: 'op', // done
    },
    selectedPointHelptext: {
        fr: `Sélectionnez un point du graphique pour afficher ses infos.`,
        nl: 'Selecteer een punt in de grafiek om de informatie weer te geven.', // done
    },

    config: {
        fr: 'config',
        nl: 'config', // done
    },

    table: {
        fr: 'tableau',
        nl: 'tabel', // done
    },

    plot: {
        fr: 'plot',
        nl: 'plot', // done
    },

    chart: {
        fr: 'graphique',
        nl: 'grafiek', // done
    },

    exportCSV: {
        fr: 'exporter les données (.csv) et stats.',
        nl: 'exporteren van de gegevens (.csv) en stat.', // done
    },

    parameterToDisplay: {
        fr: 'Choix du paramètre à afficher',
        nl: 'Keuze van de parameter om weer te geven ', // done
    },

    parameter: {
        fr: 'Paramètre(s)',
        nl: 'Parameter(s)', // done
    },

    tableParameterLabel: {
        fr: 'Paramètre',
        nl: 'Parameter', // done
    },

    tableStationLabel: {
        fr: 'Site',
        nl: 'Meetpunt', // done
    },

    datesInterval: {
        fr: 'intervalle temporel',
        nl: 'tijdsinterval', // done
    },

    pointInfo: {
        fr: 'données de la sélection',
        nl: 'gegevens van de selectie', // done
    },

    norm: {
        fr: 'norme(s)',
        nl: 'norm(en)', // done
    },

    chooseParams: {
        fr: 'choix des paramètres',
        nl: 'keuze van de parameters', // done
    },
    goToChooseParams: {
        fr: 'Aller au choix des paramètres',
        nl: 'Ga naar de keuze van de parameters', // done
    },

    valueMinShort: {
        fr: 'Min.',
        nl: 'Min.', // done
    },
    valueMaxShort: {
        fr: 'Max.',
        nl: 'Max.', // done
    },
    valueMedShort: {
        fr: 'Méd.',
        nl: 'Med.', // done
    },
    valueAvgShort: {
        fr: 'Moy.',
        nl: 'Gem.', // gem. is short for 'gemiddeld'
    },
    valueP10: {
        fr: 'P10',
        nl: 'P10',
    },
    valueP90: {
        fr: 'P90',
        nl: 'P90',
    },
    valueMin: {
        fr: 'Minimum',
        nl: 'Minimum', // done
    },
    valueMax: {
        fr: 'Maximum',
        nl: 'Maximum', // done
    },
    valueMed: {
        fr: 'Médiane',
        nl: 'Mediaan', // done
    },
    valueAvg: {
        fr: 'Moyenne',
        nl: 'Gemiddeld', // done
    },
    valueCount: {
        fr: `Nb de valeurs`,
        nl: 'Aantal waarden', // done
    },

    valueCountDNQ: {
        fr: `Nb de valeurs inférieures à LOQ`,
        nl: 'Aantal waarden onder de kwantificatielimiet', // done
    },

    clusterHelptext1: {
        fr: `Étant donné le grand nombre d'enregistrements, la sélection est une moyenne des mesures effectuées sur la période du `,
        nl: `Door het grote aantal registraties is de selectie een gemiddelde van de verrichte metingen, over de periode van `, // done
    },
    clusterHelptext2: {
        fr: `au `,
        nl: `tot `, // done
    },
    clusterHelptext3: {
        fr: `Veuillez zoomer pour voir les valeurs mesurées.`,
        nl: 'Gelieve in te zoomen om de gemeten waarden te zien.', // done
    },
    clusterToolTip: {
        fr: 'Sélectionnez et zoomez pour voir les valeurs mesurées. \n Moyenne en ce point:',
        nl: 'Selecteer en zoom om de gemeten waarden te zien. \n Gemiddelde op dit punt:', // done
    },
    clusterStart: {
        fr: `Date de la première valeur`,
        nl: 'Datum van de eerste waarde', // done
    },
    clusterEnd: {
        fr: `Date de la dernière valeur`,
        nl: 'Datum van laatste waarde', // done
    },
    clusterAvg: {
        fr: `Moyenne sur la période`,
        nl: 'Gemiddelde over de periode', // done
    },
    clusterZoom: {
        fr: `Zoomer autour de la sélection`,
        nl: 'Zoom in op de selectie', // done
    },

    legend: {
        fr: `Légende`,
        nl: 'Legende', // done
    },

    selectAllStation: {
        fr: `sélectionner tous les sites`,
        nl: 'selecteer alle meetpunten', // done
    },

    deSelectAllStation: {
        fr: `Dé-sélectionner les sites`,
        nl: 'Deselecteer de meetpunten', // done
    },
    deselectParameter: {
        fr: `Dé-sélectionner le paramètre`,
        nl: 'Deselecteer de parameter', // done
    },
    deselectStation: {
        fr: `Dé-sélectionner le site`,
        nl: 'Deselecteer het meetpunt', // done
    },

    unquantified: {
        fr: `Non quantifié (limite de quantification divisée par 2)`,
        nl: 'Ongekwantificeerd (kwantificatielimiet gedeeld door 2)', // done
    },

    perParameter: {
        fr: `Afficher par paramètres`,
        nl: 'Weergave op basis van parameter(s)', // done
    },
    perStation: {
        fr: `Afficher par site(s)`,
        nl: 'Weergave op basis van meetpunt(en)', // done
    },

    stationMultiselectHelptext: {
        fr: 'Pour sélectionner plusieurs sites, maintenir la touche `SHIFT` enfoncée tout en cliquant sur les sites souhaités.',
        nl: 'Om meerdere meetpunten te selecteren, houdt u de `SHIFT` toets ingedrukt terwijl u op de gewenste meetpunten klikt.', // done
    },

    StationInfos: {
        fr: `Informations sur le site`,
        nl: 'Informatie op de meetpunt', // done
    },

    selectedStations: {
        fr: `Site(s)`,
        nl: 'Meetpunt(en)', // done
    },

    StationId: {
        fr: `Code`,
        nl: 'Code', // done
    },

    // piezzo
    Adress: {
        fr: 'Adresse',
        nl: 'Adres', // done
    },

    Coordinates: {
        fr: 'Coordonnées X et Y',
        nl: 'X- en Y-coördinaten', // done
    },

    ZCoord: {
        fr: 'Altitude (m DNG)',
        nl: 'Hoogte (m TAW)', // done
    },
    NomGWRef: {
        fr: 'Unité hydrogéologique',
        nl: 'Hydrogeologische eenheid', // done
    },
    NomGWBFr: {
        fr: "Masse d'eau souterraine",
        nl: 'Grondwaterlichaam', // done
    },
    surfaceWaterBody: {
        fr: "Masse d'eau de surface",
        nl: 'Oppervlaktewaterlichaam', // done (elise beke)
    },
    CodeGWB: {
        fr: "Code de la masse d'eau",
        nl: 'Code voor de watermassa', // done
    },
    RefGWRef: {
        fr: "Code de référence de la masse d'eau souterraine",
        nl: 'Referentiecode grondwaterlichaam', // done
    },
    NoEuCode: {
        fr: 'Code EU du site',
        nl: 'EU-code van de meetpunt', // done,
    },
    ProfRelM: {
        fr: 'Altitude - topographie (m DNG)',
        nl: 'Hoogte topografie (m TAW)', // done (elise beke)
    },
    TopCrepineRelM: {
        fr: 'Profondeur relative (m) du sommet de la crépine',
        nl: 'Relatieve diepte (m) van de top van de filter', // crépine is filter or zeef?
    },
    ProfCrepineRelM: {
        fr: 'Profondeur relative (m) de la base de la crépine',
        nl: 'Relatieve diepte (m) van de basis van de filter', // crépine is filter or zeef?
    },
    Monitoring: {
        fr: 'Date de début de surveillance - date de fin',
        nl: 'Begindatum van de controle - einddatum', // done
    },
    Ongoing: {
        fr: 'en cours',
        nl: 'lopend', // done
    },
    MonFreq: {
        fr: 'Fréquence de mesure',
        nl: 'Frequentie van de meting', // done
    },

    // ESO°Qual
    TypeOuvrage: {
        fr: "Type d'ouvrage",
        nl: 'Soort kunstwerk', // or werk
    },
    RefOuvrage: {
        fr: 'Référence ouvrage auto',
        nl: 'Referentie kunstwerk', // auto??
    },
    NCode: {
        fr: 'Code',
        nl: 'Code', // done,
    },
    Comment: {
        fr: 'Commentaire',
        nl: 'Opmerking', // done,
    },

    StationName: {
        fr: 'Nom du site',
        nl: 'Naam van de meetpunt', // done
    },

    station: {
        fr: 'Site(s)',
        nl: 'meetpunt(en)', // done
    },

    selectStation: {
        fr: 'Chercher un site',
        nl: 'Zoek een meetpunt', // done
    },

    legendGroundQuantity: {
        fr: `Sites de surveillance quantitatif (piézométrie) des masses d'eau souterraine`,
        nl: 'Meetpunten van de kwantitatieve toestand (piëzometrie) van de grondwaterlichamen', // nldone i would say -  grondwatermassa ipv grondwaterlichamen
    },

    legendGroundQuality: {
        fr: `Sites de surveillance qualitative des masses d'eau souterraine`,
        nl: 'Meetpunten voor de kwalitatieve toestand van de grondwaterlichamen', // nldone i would say - grondwatermassa ipv grondwaterlichamen
    },
    legendSurface: {
        fr: `Sites de surveillance qualitative des masses d'eau de surface`,
        nl: 'Meetpunten voor de kwalitatieve toestand van de oppervlaktewaterlichamen', // nldone
    },
    welcomeMessage: {
        fr: `BruWater - Réseaux de surveillance des eaux de surface et souterraines.`,
        nl: 'BruWater - Monitoringnetwerken van oppervlakte- en grondwater.', // done
    },
    welcomeMessageGround: {
        fr: `Données des eaux souterraines en RBC.`,
        nl: 'Gegevens van grondwater in BHG.', // done
    },
    welcomeMessageSurface: {
        fr: `Données des eaux de surface en RBC.`,
        nl: 'Gegevens van oppervlaktewater in BHG.', // done
    },
    surfaceWater: {
        fr: `Eaux de surface`,
        nl: 'Oppervlaktewater', // done
    },
    groundWater: {
        fr: `Eaux souterraines`,
        nl: 'Grondwater', // done
    },

    perparam: {
        fr: 'par paramètres',
        nl: 'per parameter', //nltodo
    },

    perstation: {
        fr: 'par sites',
        nl: 'per meetpunt', //nltodo
    },

    seeSurfaceWater: {
        fr: `Voir les eaux de surface`,
        nl: 'Zie oppervlaktewater', // done
    },
    seeGroundWater: {
        fr: `Voir les eaux souterraines`,
        nl: 'Zie grondwater', // done
    },

    searchByParam: {
        fr: 'Chercher par paramètres',
        nl: 'Zoeken op parameter', // nltodo
    },

    searchByStation: {
        fr: 'Chercher par site',
        nl: 'Zoeken op meetpunt', //nltodo
    },

    paramPickerMainTitle: {
        fr: `Paramètres disponibles`,
        nl: 'Te bekijken parameters', // done
    },
    stationPickerMainTitle: {
        fr: `3 - Choix des sites`,
        nl: '3 - Meetpunt keuze', // nltodo
    },

    groupPickerHelpText: {
        fr: `Choisir un groupe physico-chimique pour préciser la liste des paramètres.`,
        nl: 'Kies een fysisch-chemische groep om de lijst met parameters te specificeren.', // done
    },

    groupPicker: {
        fr: `1 - Filtre par groupe`,
        nl: '1 - Filter op groep', // done
    },

    paramPicker: {
        fr: `2 - Sélection des paramètres`,
        nl: '2 - Selectie van parameters', // done
    },

    selectedGroup: {
        fr: `Sélection`,
        nl: 'Selectie', // done
    },

    paramsToDisplay: {
        fr: `Paramètres sélectionnés`,
        nl: 'Geselecteerde parameters', // done
    },
    stationsToDisplay: {
        fr: `Sites sélectionnés`,
        nl: 'Geselecteerde meetpunten', // done
    },

    filter: {
        fr: `filtrez dans la liste`,
        nl: 'filter de lijst', // done
    },

    noDataTime: {
        fr: `Il n'y a pas de valeurs dans cette fenêtre temporelle. Veuillez ré-initialiser les dates`,
        nl: 'Er zijn geen waarden in dit tijdsvenster. Gelieve de data te resetten', // done
    },

    noDataParams: {
        fr: `Il n'y a pas de valeurs pour ces paramètres`,
        nl: 'Er zijn geen waarden voor deze parameters.', // done
    },

    'helptext:init1': {
        fr: `Bienvenue sur le portail de visualisation et distribution des données relatives aux eaux souterraines et de surface de la région de Bruxelles-Capitale.`,
        nl: `Welkom op de portaalsite voor de visualisatie en verspreiding van oppervlakte- en grondwatergegevens van het Brussels Hoofdstedelijk Gewest.`, // done
    },

    'helptext:init2': {
        fr: `Veuillez sélectionner un jeu de données à explorer.`,
        nl: `Gelieve een set gegevens te selecteren om te bekijken.`, // done
    },

    'helptext:paramsPicker': {
        fr: `Veuillez sélectionner les paramètres à visualiser. Il est possible de sélectionner plusieurs paramètres pour les visualiser simultanément. `,
        nl: 'Gelieve de parameters te selecteren die u wilt bekijken. Het is mogelijk om meerdere parameters te selecteren om ze tegelijkertijd te bekijken.', // done
    },
    'helptext:stats': {
        fr: `Pour visualiser les statistiques d'un paramètre, veuillez le sélectionner dans la légende du graphique.`,
        nl: 'Om de statistieken van een parameter te bekijken, gelieve deze in de legende van de grafiek te selecteren.', // done
    },
    creditsBE: {
        fr: `Fond de plan : © Urbis - Licence des données distribuées : [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.fr)`,
        nl: 'Achtergrond : © Urbis - Licentie van de gedistribueerde gegevens : CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.nl)', // done
    },

    legendGroundQuantityLabel: {
        fr: `Sélectionner les sites de surveillance quantitatif (piézométrie) par masse d'eau souterraine`,
        nl: `Selecteer de meetpunten van de kwantitatieve toestand (piëzometrie) per grondwaterlichaam`, // nl done
    },

    legendGroundQualityLabelSelect: {
        fr: `Sélectionner les sites de surveillance qualitative par masse d'eau souterraine`,
        nl: `Selecteer de meetpunten voor de kwalitatieve toestand per grondwaterlichaam`, // nl done
    },
    legendGroundQualityLabel: {
        fr: `Sites de surveillance qualitative des masses d'eau souterraine`,
        nl: `Meetpunten voor de kwalitatieve toestand van de grondwaterlichamen`, // nl done
    },

    legendSurfaceQualityLabel: {
        fr: `Sites de surveillance qualitative des masses d'eau de surface`,
        nl: `Meetpunten voor de kwalitatieve toestand van de oppervlaktewaterlichamen`, // nl to check
    },
    legendSurfaceQualityLabelSelect: {
        fr: `Sélectionner les sites de surveillance qualitative par masse d'eau de surface`,
        nl: `Selecteer de meetpunten voor de kwalitatieve toestand per oppervlaktewaterlichaam`, // nl to check
    },

    legendBR01: {
        fr: `(BR01) Socle et Crétacé `,
        nl: `(BR01) Sokkel en Krijt `, // nl done
    },
    legendBR02: {
        fr: `(BR02) Socle, zone d'alimentation `,
        nl: `(BR02) Sokkel, voedingsebied `, // nl done
    },
    legendBR03: {
        fr: `(BR03) Sables du Landénien `,
        nl: `(BR03) Landeniaanzand `, // nl done
    },
    legendBR04: {
        fr: `(BR04) Yprésien région des Collines `,
        nl: `(BR04) Ieperiaan heuvelstreken `, // nl done
    },
    legendBR05: {
        fr: `(BR05) Sables du Bruxellien `,
        nl: `(BR05) Brusseliaanzand `, // nl done
    },
    legendBROther: {
        fr: `Autre`,
        nl: `Andere`, // nl done
    },
    hideLayer: {
        fr: `Masquer la couche de données`,
        nl: `Verberg de laag gegevens`, // done
    },
    showLayer: {
        fr: `Afficher la couche de données`,
        nl: `Toon de laag gegevens`, // done
    },

    watherbodyOther: {
        fr: 'Étangs et affluents',
        nl: 'Vijvers en zijrivieren',
    },

    exportAllParameters: {
        fr: 'Exporter tous les paramètres',
        nl: 'Alle parameters exporteren',
    },

    exportAllStations: {
        fr: 'Exporter tous les sites',
        nl: 'Exporteer alle meetpunten',
    },

    'helptext:home': {
        fr: `Explorer les données : `,
        nl: `Verken de gegevens:`, // to check
    },

    'BKPhelptext:home': {
        fr: `Découvrez toutes les données collectées pour les eaux de surface et eaux souterraines en Région Bruxelles-Capitale.  

Sélectionnez les sites et données à explorer : `,
        nl: `Ontdek alle verzamelde gegevens over het oppervlaktewater en het grondwater in het Brussels Hoofdstedelijk Gewest.  
        
Selecteer de sites en gegevens die u wilt verkennen : `, // to check
    },

    Hour: {
        fr: 'Heures',
        nl: 'Uren',
    },
    Day: {
        fr: 'Jours',
        nl: 'Dagen',
    },
    Month: {
        fr: 'Mois',
        nl: 'Maanden',
    },
    Year: {
        fr: 'Années',
        nl: 'Jaren',
    },

    timestamp: {
        en: 'Timestamp',
        fr: 'Horodatage',
        nl: 'Tijdstempel',
    },

    piezoAbsoluteUnit: {
        fr: 'm DNG',
        nl: 'm TAW', // done - elise beke
    },

    showNorm: {
        fr: 'Afficher la norme',
        nl: 'Norm weergeven',
    },

    yes: {
        fr: 'oui',
        nl: 'ja',
    },

    no: {
        fr: 'non',
        nl: 'nee',
    },

    tableHelptext: {
        fr: `Les valeurs inférieures à la limite de quantification sont précédées du signe '<'`,
        nl: `Waarden onder de kwantificatielimiet worden voorafgegaan door een '<' teken.`,
    },
};

type MDB = typeof messages;
export type TimeserieMessageKey = keyof MDB;

declare module 'sdi/locale' {
    export interface MessageStore {
        ts(k: TimeserieMessageKey): Translated;
    }
}

MessageStore.prototype.ts = (k: TimeserieMessageKey) => fromRecord(messages[k]);
