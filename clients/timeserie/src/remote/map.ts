import * as io from 'io-ts';
import {
    fetchIO,
    FeatureCollectionIO,
    InspireIO,
    ILayerInfoIO,
} from 'sdi/source';
import { getApiUrl } from 'sdi/app';

export const fetchLayerSurface = () =>
    fetchIO(FeatureCollectionIO, getApiUrl('geodata/water/surface/layer'));

export const fetchLayerGroundQuality = () =>
    fetchIO(
        FeatureCollectionIO,
        getApiUrl('geodata/water/ground/layer/quality')
    );

export const fetchLayerGroundQuantity = () =>
    fetchIO(
        FeatureCollectionIO,
        getApiUrl('geodata/water/ground/layer/quantity')
    );

export const fetchHydroInfo = () =>
    fetchIO(
        io.array(ILayerInfoIO),
        getApiUrl('geodata/water/hydro/layer-info')
    );

export const fetchLayer = (metadataId: string) =>
    fetchIO(
        FeatureCollectionIO,
        getApiUrl(`geodata/water/hydro/layer-data/${metadataId}`)
    );

export const fetchDatasetMetadata = (metadataId: string) =>
    fetchIO(InspireIO, getApiUrl(`metadatas/${metadataId}`));
