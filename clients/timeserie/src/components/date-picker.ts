import { DIV, SPAN } from 'sdi/components/elements';
import {
    getWindowStart,
    getWindowEnd,
    getLevel,
    getPlotData,
    getMode,
} from '../queries/timeserie';
import tr from 'sdi/locale';
import { renderButtonInitDates } from './button';
import { configAddWindow, navigateInPlace } from '../events/route';
import { clearWindow, clearPointSelection } from '../events/timeserie';
// import { none } from 'fp-ts/lib/Option';
// import { PlotData } from './plot/types';
import { inputDate } from 'sdi/components/input';
import { maxX, minX } from './plot/xaxis';

// const fmtDateNb = (n: number) => (n < 10 ? `0${n}` : `${n}`);
// const simpleDateString = (t: number) => {
//     const date = new Date(t);
//     return `${date.getFullYear()}-${fmtDateNb(date.getMonth() + 1)}-${fmtDateNb(
//         date.getDate()
//     )}`;
// };

// const initDates = (data: PlotData) => {
//     if (getWindowStart() === none) {
//         if (getWindowEnd() === none) {
//             configAddWindow(minX(data), true, maxX(data), true).map(config =>
//                 getLevel().map(level =>
//                     navigateInPlace(level, config, getMode())
//                 )
//             );
//         } else {
//             // selectWindowStart(minX(data), true);
//             getWindowEnd().map(({ date, isAutomatic }) =>
//                 configAddWindow(
//                     minX(data),
//                     true,
//                     date.getTime(),
//                     isAutomatic
//                 ).map(config =>
//                     getLevel().map(level =>
//                         navigateInPlace(level, config, getMode())
//                     )
//                 )
//             );
//         }
//     } else if (getWindowEnd() === none) {
//         // selectWindowEnd(maxX(data), true);
//         getWindowStart().map(({ date, isAutomatic }) =>
//             configAddWindow(date.getTime(), isAutomatic, maxX(data), true).map(
//                 config =>
//                     getLevel().map(level =>
//                         navigateInPlace(level, config, getMode())
//                     )
//             )
//         );
//     }
// };

// const withTimestamp = (s: string) => {
//     const ts = Date.parse(s);
//     if (Number.isNaN(ts) || ts < 0) {
//         return none;
//     }
//     return some(ts);
// };

const inputDateStart = inputDate('start-date');
const inputDateEnd = inputDate('start-end');

const renderDatesInputBox = () => {
    const data = getPlotData();
    // initDates(data);

    const min = new Date(minX(data));
    const max = new Date(maxX(data));
    const start = getWindowStart()
        .map(({ date }) => date)
        .getOrElse(min);
    const end = getWindowEnd()
        .map(({ date }) => date)
        .getOrElse(max);

    return DIV(
        { className: 'date-input' },
        // H2({}, tr.ts('datesInterval')),
        DIV(
            { className: 'date-input__wrapper' },
            SPAN({}, tr.ts('displayFrom')),

            inputDateStart(min, end, start, newStart =>
                getWindowEnd()
                    .chain(windowEnd =>
                        configAddWindow(
                            newStart.getTime(),
                            false,
                            windowEnd.date.getTime(),
                            windowEnd.isAutomatic
                        )
                    )
                    .map(config =>
                        getLevel().map(level =>
                            navigateInPlace(level, config, getMode())
                        )
                    )
            ),

            SPAN({}, tr.ts('displayTo')),

            inputDateEnd(start, max, end, newEnd =>
                getWindowStart()
                    .chain(windowStart =>
                        configAddWindow(
                            windowStart.date.getTime(),
                            windowStart.isAutomatic,
                            newEnd.getTime(),
                            false
                        )
                    )
                    .map(config =>
                        getLevel().map(level =>
                            navigateInPlace(level, config, getMode())
                        )
                    )
            ),

            renderButtonInitDates(() => {
                clearWindow();
                clearPointSelection();
            })
        )
    );
};

export default renderDatesInputBox;
