// import { DIV } from 'sdi/components/elements';
// import {
//     getWindowStart,
//     getWindowEnd,
//     getPlotData,
// } from '../queries/timeserie';
// import { PlotData, minX, maxX } from './plot';
// import { none, some } from 'fp-ts/lib/Option';
// import tr from 'sdi/locale';

// const noDataMessage = (data: PlotData, start: number, end: number) => {
//     if (data.reduce((acc, dataElem) => acc + dataElem.ts.length, 0) < 1) {
//         return some(
//             DIV({ className: 'data-message no-data' }, tr.ts('noDataParams'))
//         );
//     } else {
//         const dates = data.reduce(
//             (acc, { ts }) => ts.map((p) => p.date).concat(acc),
//             []
//         );
//         const firstDate = dates.reduce(
//             (acc, val) => (val < acc ? val : acc),
//             (new Date()).getTime()
//         );
//         const lastDate = dates.reduce(
//             (acc, val) => (val > acc ? val : acc),
//             0
//         );
//         if (
//             start > lastDate ||
//             end < firstDate
//         ) {
//             return some(
//                 DIV({ className: 'data-message no-data' }, tr.ts('noDataTime'))
//             );
//         }
//     }
//     return none;
// };

// const renderNoDataMessage = () => {
//     const data = getPlotData();
//     const start = getWindowStart()
//         .map((ds) => ds.date.getTime())
//         .getOrElse(minX(data));
//     const end = getWindowEnd()
//         .map((ds) => ds.date.getTime())
//         .getOrElse(maxX(data));

//     return noDataMessage(data, start, end);
// };

// export default renderNoDataMessage;
