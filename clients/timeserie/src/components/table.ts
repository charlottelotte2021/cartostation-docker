import { DIV } from 'sdi/components/elements';
import { tableTimeserieQuery, getSource } from 'timeserie/src/queries/table';
import { tableTimeserieDispatch } from 'timeserie/src/events/table';
import { table, TableDataRow } from 'sdi/components/table2';
import tr from 'sdi/locale';
// import { renderDisplaySwitch } from './button';

// const renderDisplaySwitch = () => {
//     const tableMode = getTableMode();

//     switch (tableMode) {
//         case 'per-station':
//             return [stationLabel(), btnPerParam()];
//         case 'per-parameter':
//             return [paramLabel(), btnPerStation()];
//     }
// };

const rowHandler = (_row: TableDataRow, _i: number) => {};

const toolbar = () => DIV('table-toolbar', tr.ts('tableHelptext'));

const renderTable = table(
    tableTimeserieDispatch,
    tableTimeserieQuery,
    rowHandler
);

const render = () => DIV('content__body', renderTable(getSource(), toolbar()));

export default render;
