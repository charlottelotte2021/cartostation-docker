import { createElement as s } from 'react';

import { uniqId } from 'sdi/util';

import { graphsize, padding } from './types';

type DOMProperties = { [key: string]: any };

type tickAlignment = 'start' | 'middle' | 'end';

export const svg = (els: React.ReactSVGElement[], properties?: {}) =>
    s(
        'svg',
        {
            viewBox: `-${padding.left} -${padding.top} ${
                graphsize.width + padding.left + padding.right
            } ${graphsize.height + padding.top + padding.bottom}`,
            key: uniqId(),
            ...properties,
        },
        ...els
    );

export const innerSvg = (els: React.ReactSVGElement[], properties?: {}) =>
    s(
        'svg',
        {
            viewBox: `-${padding.left} -${padding.top} ${
                graphsize.width + padding.left + padding.right
            } ${graphsize.height + padding.top + padding.bottom}`,
            key: uniqId(),
            ...properties,
        },
        ...els
    );

export const line = (
    x1: number,
    y1: number,
    x2: number,
    y2: number,
    properties?: DOMProperties
) => s('line', { x1, y1, x2, y2, key: uniqId(), ...properties });

export const rect = (
    x: number,
    y: number,
    width: number,
    height: number,
    properties?: DOMProperties
) => s('rect', { x, y, width, height, key: uniqId(), ...properties });

export const text = (
    x: number,
    y: number,
    text: string,
    textAnchor: tickAlignment = 'start',
    properties?: DOMProperties
) => s('text', { x, y, textAnchor, key: uniqId(), ...properties }, text);

export const circle = (
    cx: number,
    cy: number,
    r: number,
    properties?: DOMProperties
) => s('circle', { cx, cy, r, ...properties });

export const circleWithTitle = (
    cx: number,
    cy: number,
    r: number,
    title: string,
    properties?: DOMProperties
) => s('circle', { cx, cy, r, ...properties }, s('title', {}, title));

export const group = (elems: React.ReactSVGElement[], properties?: {}) =>
    s('g', { key: uniqId(), ...properties }, ...elems);
