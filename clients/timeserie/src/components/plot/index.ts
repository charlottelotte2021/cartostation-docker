import { ReactSVGElement } from 'react';
import * as debug from 'debug';
// import { some, none } from 'fp-ts/lib/Option';
import { datetimeBEFormated } from 'sdi/util';
import tr, { formatDate } from 'sdi/locale';
import {
    getColor,
    getSelectedKind,
    getSelectedPointInfo,
    getNormSelection,
    getSelectedParameterRaw,
    getSimplifiedTimeserie,
    getNormName,
    getSelectedStationListId,
    getDisplayMode,
    getHighlightedElements,
    getSelectedNorms,
} from '../../queries/timeserie';
import { selectPoint, clearPointSelection } from '../../events/timeserie';
import { some, none } from 'fp-ts/lib/Option';
import { renderParamUnit } from '../point-selector';
import {
    DataElementInfo,
    graphsize,
    innerPadding,
    LABELFONTSIZE,
    LinechartInfo,
    padding,
    PlotData,
    PlotDataElement,
    PlotInterval,
    PlotNorm,
    PlotPoint,
    YAXISDISTANCE,
} from './types';
import { circleWithTitle, group, line, svg, text } from './svg';
import {
    axisX,
    beginOfInterval,
    getIntervalTime,
    getXInPixel,
    maxX,
    minX,
    toMillisecondUnit,
    xMillisecondRange,
} from './xaxis';
import {
    simpleAxisY,
    getMax,
    getMin,
    getYInPixel,
    multiplesYAxis,
    subdivision,
} from './yaxis';
const logger = debug('sdi:plot/index');

// line chart if all data with the same Y axis
// const simpleLineChart = (min: number, dataElem: DataElement, interval: PlotInterval) => {
//     const color = getColorFor(dataElem.identifier).getOrElse("#000000");
//     const range: number = fromHours(interval);
//     const nbUnit = Math.round(xHoursRange(data) / range);
//     const xUnit = (graphsize.width - innerPadding.left - innerPadding.right) / nbUnit;
//     const yUnit = graphsize.height / yRange(data);
//     let x = innerPadding.left;

//     let y = (dataElem.ts[0].value * yUnit) - min * yUnit;
//     const offSetY = (min - Math.floor(min)) * yUnit;
//     let i = 0;
//     const lines = dataElem.ts.map(e => {
//         const l = line(x, y + offSetY, innerPadding.left + i * xUnit, e.value * yUnit - min * yUnit + offSetY, { stroke: color, strokeWidth: '0.5' });
//         x = innerPadding.left + i * xUnit;
//         i += 1;
//         y = e.value * yUnit - min * yUnit;
//         return l;
//     })
//     x = innerPadding.left;
//     const points = dataElem.ts.map(e => {
//         const point = circle(x, (e.value - min) * yUnit + offSetY, 2, { stroke: color, className: `point-${dataElem.identifier}-${e.date}` });
//         x = x + xUnit;
//         logger(`coord = ${e.date}, ${e.value}`);
//         return point;
//     })
//     const chart = lines.concat(points);
//     return group(chart, {
//         className: `timeserie-linechart-${dataElem.identifier}`,
//     });
// }

const date_str = (point: PlotPoint) =>
    getSelectedKind().fold('', k => {
        if (k === 'ground-quantity') {
            return `${datetimeBEFormated(new Date(point.date))}`;
        } else {
            return `${formatDate(new Date(point.date))}`;
        }
    });

const isSelectedPoint = (dataElemInfo: DataElementInfo, point: PlotPoint) =>
    getSelectedPointInfo()
        .map(({ id, start }) =>
            id === dataElemInfo.id && Math.abs(point.date - start) < 60000 // if firstX is the same date as start.
                ? true
                : false
        )
        .getOrElse(false);

const highlightedClass = (dataElemInfo: DataElementInfo) =>
    getHighlightedElements() // could be parameters or stations
        .map(ps => (ps.id === dataElemInfo.id ? 'highlighted' : 'shaded'))
        .getOrElse('highlighted');

const yInfo = (chartInfo: LinechartInfo, dataElemInfo: DataElementInfo) => {
    const yUnit = chartInfo.multiAxis
        ? dataElemInfo.yUnit
        : chartInfo.yBiggestUnit;
    const yMin = chartInfo.multiAxis
        ? dataElemInfo.yMin
        : chartInfo.ySmallestMin;
    return { yUnit, yMin };
};

const chartPoints = (
    chartInfo: LinechartInfo,
    dataElemInfo: DataElementInfo,
    withClusters: boolean
) => {
    const ts = dataElemInfo.timeserie;
    const len = ts.length;
    const { yUnit, yMin } = yInfo(chartInfo, dataElemInfo);

    const points = ts.map((e, index) => {
        let radius = 2;
        let fillColor = dataElemInfo.color;
        if (e.factor < 1) {
            fillColor = 'white'; // rond "vide" pour les données non quantifiées
        }

        const firstX = e.date;
        const secondY = index < len - 1 ? ts[index + 1].date : firstX;
        const x = getXInPixel(firstX, chartInfo.xUnit, chartInfo.xMin);
        const y = getYInPixel(e.value * e.factor, yUnit, yMin);

        const title =
            withClusters === true
                ? `${tr.ts('clusterToolTip')} ${e.value.toPrecision(
                      4
                  )} ${renderParamUnit(dataElemInfo.id)}`
                : `${(e.value * e.factor).toPrecision(4)} ${renderParamUnit(
                      dataElemInfo.id
                  )} \n${date_str(e)}`;

        if (isSelectedPoint(dataElemInfo, e)) {
            radius = 4;
            return circleWithTitle(x, y, radius, title, {
                stroke: dataElemInfo.color,
                fill: fillColor,
                className: `point-selected`,
                onClick: () => {
                    clearPointSelection();
                },
            });
        } else {
            if (len > 200) {
                radius = 0.5;
            }
            return circleWithTitle(x, y, radius, title, {
                stroke: dataElemInfo.color,
                fill: fillColor,
                className: `point-unselected`,
                onClick: () => {
                    clearPointSelection();
                    selectPoint(
                        dataElemInfo.id,
                        firstX,
                        secondY,
                        x,
                        y,
                        dataElemInfo.color
                    );
                },
            });
        }
    });

    return group(points, {
        className: `timeserie-points-${highlightedClass(dataElemInfo)}`,
    });
};

const chartLines = (
    chartInfo: LinechartInfo,
    dataElemInfo: DataElementInfo
) => {
    // don't draw lines for ground-quantity
    if (getSelectedKind().getOrElse('ground-quantity') === 'ground-quantity') {
        return [] as ReactSVGElement[];
    }

    const ts = dataElemInfo.timeserie;
    let lines: ReactSVGElement[] = [];
    const { yUnit, yMin } = yInfo(chartInfo, dataElemInfo);

    let x = getXInPixel(ts[0].date, chartInfo.xUnit, chartInfo.xMin);
    let y = getYInPixel(ts[0].value * ts[0].factor, yUnit, yMin);

    lines = ts.map(e => {
        const l = line(
            x,
            y,
            getXInPixel(e.date, chartInfo.xUnit, chartInfo.xMin),
            getYInPixel(e.value * e.factor, yUnit, yMin),
            {
                className: `line`,
                stroke: dataElemInfo.color,
                strokeWidth: '0.5',
                strokeOpacity: '0.5',
            }
        );
        x = getXInPixel(e.date, chartInfo.xUnit, chartInfo.xMin);
        y = getYInPixel(e.value * e.factor, yUnit, yMin);
        return l;
    });

    return group(lines, {
        className: `timeserie-linechart-${highlightedClass(dataElemInfo)}`,
    });
};

const chartNorms = (
    chartInfo: LinechartInfo,
    dataElemInfo: DataElementInfo,
    norms: PlotNorm[],
    xWidth: number
) => {
    const { yUnit, yMin } = yInfo(chartInfo, dataElemInfo);
    const normsDrawing = getSelectedNorms(norms).map(n =>
        group([
            line(
                0,
                getYInPixel(n.value, yUnit, yMin),
                xWidth,
                getYInPixel(n.value, yUnit, yMin),
                {
                    stroke: dataElemInfo.color,
                    strokeDasharray: '4',
                    strokeWidth: '0.5',
                    className: `norm-${n.name}`,
                }
            ),
            text(
                xWidth,
                getYInPixel(n.value, yUnit, yMin),
                `${getNormName(n.name)}: ${n.value}`,
                'end',
                {
                    fill: `${dataElemInfo.color}`,
                    fontSize: LABELFONTSIZE,
                }
            ),
        ])
    );

    return group(normsDrawing, {
        className: `timeserie-linechart-${highlightedClass(dataElemInfo)}`,
    });
};

// all info needed for multiple axis representation (linechart)
// const getChartInfo = (
//     xUnit: number,
//     minX: number,
//     interval: PlotInterval,
//     dataElem: PlotDataElement
// ) => {
//     const timeserie = dataElem.ts;
//     const selectedNorms = getNormSelection()
//         .getOrElse([])
//         .filter(sn => sn.paramId === dataElem.identifier);
//     const yValues = timeserie
//         .map(point => point.value * point.factor)
//         .concat(selectedNorms.map(n => n.norm.value));
//     const minY = getMin(yValues);
//     const maxY = getMax(yValues);
//     const [divider] = subdivision(maxY - minY);
//     const minYFloor = Math.floor(minY / divider) * divider;
//     const maxYCeil = Math.ceil(maxY / divider) * divider;
//     const range = maxYCeil - minYFloor > 0 ? maxYCeil - minYFloor : 1;
//     const yUnit =
//         (graphsize.height - innerPadding.bottom - innerPadding.top) / range;

//     return {
//         timeserie,
//         xUnit: toMillisecondUnit(xUnit, interval),
//         xMin: beginOfInterval(minX, interval),
//         yBiggestUnit: yUnit,
//         ySmallestMin: minYFloor,
//         multiAxis: true,
//     };
// };

// generic info of the chart
const getChartInfo =
    (xUnit: number, minX: number, interval: PlotInterval, data: PlotData) =>
    (multiAxis: boolean) => {
        const timeserie = data.reduce(
            (acc, val) => acc.concat(val.ts),
            [] as PlotPoint[]
        );
        const selectedNorms =
            getDisplayMode() === 'per-station'
                ? getNormSelection()
                      .getOrElse([])
                      .filter(
                          sn =>
                              data.filter(d => d.identifier === sn.paramId)
                                  .length > 0
                      )
                : getNormSelection().getOrElse([]);
        const yValues = timeserie
            .map(point => point.value * point.factor)
            .concat(selectedNorms.map(n => n.norm.value));
        const minY = getMin(yValues);
        const maxY = getMax(yValues);
        const [divider] = subdivision(maxY - minY);
        const minYFloor = Math.floor(minY / divider) * divider;
        const maxYCeil = Math.ceil(maxY / divider) * divider;
        const range = maxYCeil - minYFloor > 0 ? maxYCeil - minYFloor : 1;
        const yBiggestUnit =
            (graphsize.height - innerPadding.bottom - innerPadding.top) / range;

        return {
            xUnit: toMillisecondUnit(xUnit, interval),
            xMin: beginOfInterval(minX, interval),
            yBiggestUnit,
            ySmallestMin: minYFloor,
            yBiggestMax: maxYCeil,
            multiAxis,
        };
    };

// info to draw each timeserie
const getDataElementInfo = (dataElem: PlotDataElement, index: number) => {
    const selectedNorms = getNormSelection()
        .getOrElse([])
        .filter(sn => dataElem.identifier === sn.paramId);
    const yValues = dataElem.ts
        .map(point => point.value * point.factor)
        .concat(selectedNorms.map(n => n.norm.value));
    const minY = getMin(yValues);
    const maxY = getMax(yValues);
    const [divider] = subdivision(maxY - minY);
    const minYFloor = Math.floor(minY / divider) * divider;
    const maxYCeil = Math.ceil(maxY / divider) * divider;
    const range = maxYCeil - minYFloor > 0 ? maxYCeil - minYFloor : 1;
    const yUnit =
        (graphsize.height - innerPadding.bottom - innerPadding.top) / range;
    return {
        timeserie: dataElem.ts,
        id: dataElem.identifier,
        color: getColor(index),
        yUnit,
        yMin: minYFloor,
    };
};

const getPlotInterval = (data: PlotData): PlotInterval => {
    const lastDate = maxX(data);
    const firstDate = minX(data);
    const diff = lastDate - firstDate;
    const nbYears =
        new Date(lastDate).getFullYear() - new Date(firstDate).getFullYear(); // ??
    const nbMonths = diff / getIntervalTime('Month');
    const nbDays = diff / getIntervalTime('Day');

    if (nbYears > 3) {
        return 'Year';
    } else if (nbMonths > 2) {
        return 'Month';
    } else if (nbDays > 2) {
        return 'Day';
    }
    return 'Hour';
};

const svgPlot = (
    drawing: ReactSVGElement[],
    data: PlotData,
    simpleAxis: boolean
) =>
    some(
        svg(drawing, {
            viewBox: `
            -${getXOffset(data, simpleAxis)} 
            -${padding.top} 
            ${graphsize.width + padding.left + padding.right} 
            ${graphsize.height + padding.top + padding.bottom}`,
        })
    );

const getXOffset = (data: PlotData, simpleAxis: boolean) => {
    const nbAxis = simpleAxis ? 1 : data.filter(d => d.ts.length > 0).length;
    return simpleAxis ? YAXISDISTANCE : YAXISDISTANCE * nbAxis;
};
export const getXWidth = (data: PlotData, simpleAxis: boolean) =>
    graphsize.width - getXOffset(data, simpleAxis) - 20;

const drawPlot = (data: PlotData, simpleAxis = false) => {
    if (data.length <= 0) {
        return none;
    }

    const interval: PlotInterval = getPlotInterval(data);

    const xWidth = getXWidth(data, simpleAxis);
    const xRange: number = getIntervalTime(interval);
    const nbUnit = Math.ceil(xMillisecondRange(data, interval) / xRange);

    let points: ReactSVGElement[] = [];
    let lines: ReactSVGElement[] = [];
    let norms: ReactSVGElement[] = [];
    let highlightedLinechart: ReactSVGElement[] = [];
    const highlightedId = getHighlightedElements()
        .map(ps => ps.id)
        .getOrElse(-1);

    const xUnit = (xWidth - innerPadding.left - innerPadding.right) / nbUnit;
    const xAxis = axisX(xWidth, data, interval, simpleAxis);
    const chartInfoFn = getChartInfo(xUnit, minX(data), interval, data);

    // if (simpleAxis) {
    //     const chartInfo = chartInfoFn(false);
    //     const yAxis = simpleAxisY(chartInfo);
    //     data.map((d, i) => {
    //         if (d.ts.length > 0) {
    //             const dataElemInfo = getDataElementInfo(d, i);
    //             points = points.concat(
    //                 chartPoints(chartInfo, dataElemInfo, d.clusterized)
    //             );
    //             lines = lines.concat(chartLines(chartInfo, dataElemInfo));
    //             norms = norms.concat(
    //                 chartNorms(chartInfo, dataElemInfo, d.norms, xWidth)
    //             );
    //             if (d.identifier === highlightedId) {
    //                 highlightedLinechart = highlightedLinechart
    //                     .concat(norms)
    //                     .concat(lines)
    //                     .concat(points);
    //             }
    //         }
    //     });

    //     const drawing = xAxis
    //         .concat(yAxis)
    //         .concat(norms)
    //         .concat(lines)
    //         .concat(points)
    //         .concat(highlightedLinechart);
    //     return svgPlot(drawing, 1);
    // }

    const chartInfo = simpleAxis ? chartInfoFn(false) : chartInfoFn(true);
    const yAxis = simpleAxis
        ? simpleAxisY(chartInfo, data)
        : multiplesYAxis(data);

    data.map((d, index) => {
        if (d.ts.length > 0) {
            const dataElemInfo = getDataElementInfo(d, index);
            if (d.identifier != highlightedId) {
                points = points.concat(
                    chartPoints(chartInfo, dataElemInfo, d.clusterized)
                );
                lines = lines.concat(chartLines(chartInfo, dataElemInfo));
                norms = norms.concat(
                    chartNorms(chartInfo, dataElemInfo, d.norms, xWidth)
                );
            } else {
                highlightedLinechart = highlightedLinechart
                    .concat(
                        chartNorms(chartInfo, dataElemInfo, d.norms, xWidth)
                    )
                    .concat(chartLines(chartInfo, dataElemInfo))
                    .concat(
                        chartPoints(chartInfo, dataElemInfo, d.clusterized)
                    );
            }
        }
    });

    const drawing = xAxis
        .concat(yAxis)
        .concat(norms)
        .concat(lines)
        .concat(points)
        .concat(highlightedLinechart);
    return svgPlot(drawing, data, simpleAxis);
};

// const nbDaysInMonth = (date: Date) => {
//     if (date.getMonth() in [0, 2, 4, 6, 7, 9, 11]) {
//         return 31;
//     }
//     else if (date.getMonth() in [3, 5, 8, 10]) {
//         return 30;
//     }
//     else {
//         return 28;
//     }
// }
export const getPlotDataForParameter = () =>
    getSelectedStationListId().reduce<PlotData>(
        (acc, id) =>
            getSimplifiedTimeserie(id).fold(acc, data => acc.concat(data)),
        []
    );

export const getPlotDataForStation = () =>
    getSelectedParameterRaw().reduce<PlotData>(
        (acc, { id }) =>
            getSimplifiedTimeserie(id).fold(acc, data => acc.concat(data)),
        []
    );

export const renderPlot = () => {
    switch (getDisplayMode()) {
        case 'per-parameter':
            return drawPlot(getPlotDataForParameter(), true);
        case 'per-station':
            return drawPlot(getPlotDataForStation());
    }
};
// const nearest = (value: number, tab: number[]) => {
//     return tab.reduce((acc, val) =>
//         Math.abs(value - val) < Math.abs(value - acc) ? val : acc
//     );
// };

// TODO: enlever la "grid" quand histogramme + DATE en trop sur l'axe x??
// const histogram = (
//     xUnit: number,
//     minX: number,
//     interval: PlotInterval,
//     dataElem: PlotDataElement,
//     index: number,
//     colorIndex: number,
// ) => {
//     const color = getColor(colorIndex);
//     const distances = [0];
//     for (let i = 0; i < graphsize.width / xUnit; i += 1) {
//         distances.push(i * xUnit + 3);
//     }

//     const yValues = dataElem.ts.map(point => point.value * point.factor);
//     const minY = getMin(yValues);
//     const maxY = getMax(yValues.concat(dataElem.norms.map(n => n.value)));
//     const yUnit =
//         (graphsize.height - innerPadding.bottom - innerPadding.top) /
//         (maxY - minY);
//     const offSetX = (minX - Math.floor(minX)) * xUnit;
//     const offSetY = (minY - Math.floor(minY)) * yUnit;
//     let y = getYInPixel(
//         dataElem.ts[0].value * dataElem.ts[0].factor,
//         yUnit,
//         minY,
//         offSetY
//     );

//     const xUnitMilli = toMillisecondUnit(xUnit, interval);
//     let x = getXInPixel(dataElem.ts[0].date.getTime(), xUnitMilli, minX, offSetX);

//     const lines = dataElem.ts.map(point => {
//         const l = line(x + index, 0, x + index, y, {
//             stroke: color,
//             strokeWidth: '2'
//         });
//         x = nearest(
//             getXInPixel(point.date.getTime(), xUnitMilli, minX, offSetY),
//             distances
//         );
//         y = getYInPixel(point.value, yUnit, minY, offSetY);
//         return l;
//     });
//     return group(lines, {
//         className: `timeserie-histogram-${dataElem.identifier}`
//     });
// };

// const plotByType = (
//     type: ParameterType,
//     data: PlotData,
//     xUnit: number,
//     interval: PlotInterval
// ): ReactSVGElement[] => {
//     switch (type) {
//         case 'ground-quality':
//             let i = 0;
//             return data.map((d, index) => {
//                 i += 5;
//                 return histogram(
//                     xUnit,
//                     minX(data).getTime(),
//                     interval,
//                     d,
//                     i - 5,
//                     index,
//                 );
//             });
//         case 'ground-quantity':
//             return data.map((d, index) =>
//                 lineChart(xUnit, minX(data).getTime(), interval, d, index)
//             );
//         case 'surface':
//             return data.map((d, index) =>
//                 lineChart(xUnit, minX(data).getTime(), interval, d, index)
//             );
//     }
// };

// const pointInfo = () =>
//     getSelectedPlotPoint().chain(poc =>
//         getSelectedPointInfo().map(
//             ({ x, y }) => {
//                 let dateLabel = '';
//                 let value = '';
//                 let className = '';
//                 switch (poc.tag) {
//                     case 'cluster':
//                         // return [];
//                         dateLabel = `Du ${datetime8601(poc.start)} au ${datetime8601(poc.end)}`;
//                         value = `Moy : ${poc.average}`;
//                         className = 'data-message point-info';
//                         break;
//                     case 'point':
//                         dateLabel = `${datetime8601(poc.point.date)}`;
//                         value = `${poc.point.value * poc.point.factor}`;
//                         className = 'cluster-info';
//                         break;
//                 }
//                 const dateText = text(
//                     x,
//                     y - 9,
//                     dateLabel,
//                     'middle',
//                     {
//                         className,
//                         transform: `scale(1,-1), translate(0,-${y * 2})`,
//                         fontSize: LABELFONTSIZE
//                     },
//                 );
//                 const valueText = text(
//                     x,
//                     y - 3,
//                     value,
//                     'middle',
//                     {
//                         transform: `scale(1,-1), translate(0,-${y * 2})`,
//                         fontSize: LABELFONTSIZE
//                     },
//                 )
//                 return [dateText, valueText];
//                 // const pointText = text(
//                 //     x,
//                 //     y - 9,
//                 //     `(${dateLabel}, ${value})`,
//                 //     'middle',
//                 //     {
//                 //         className,
//                 //         transform: `scale(1,-1), translate(0,-${y * 2})`,
//                 //         fontSize: LABELFONTSIZE
//                 //     },
//                 // );
//                 // return [pointText];
//             })
//     )
//         .getOrElse([]);

export default renderPlot;

logger('loaded');
