import { DIV } from 'sdi/components/elements';
import { TimeserieMessageKey } from '../locale';
import tr, { formatDate } from 'sdi/locale';
import {
    getMinInDataElem,
    getMaxInDataElem,
    getAverage,
    getMedian,
    getTimeserieBetweenDates,
    getSelectedKind,
    getPercentile,
    getWindowStart,
    getWindowEnd,
    BetweenDates,
    getTimeserieAsPlotData,
} from '../queries/timeserie';
import { Option, fromNullable, some, none } from 'fp-ts/lib/Option';
import { Parameter } from '../remote/timeserie';
import { remoteToOption } from 'sdi/source';
import { PlotDataElement } from './plot/types';

const renderParamDates = (start: Date, end: Date) =>
    DIV(
        'stat__item stat__dates',
        DIV('stat__label ', `${tr.ts('dataAvailableFrom')}`),
        DIV(
            'stat__value ',
            `${formatDate(start)} ${tr.ts('dataAvailableTo')} ${formatDate(
                end
            )}`
        )
    );

const renderParamDatesOutofWindow = (start: Date, end: Date) =>
    DIV(
        'stat__label',
        DIV(
            'stat__dates',
            `${tr.ts('dataNotInWindowAvailableFrom')} ${formatDate(
                start
            )} ${tr.ts('dataAvailableTo')} ${formatDate(end)}`
        )
    );

const renderValue = (
    key: TimeserieMessageKey,
    value: Option<number | string>
) =>
    value.map(v =>
        DIV('stat__item', DIV('stat__label', tr.ts(key)), DIV('stat__value', v))
    );

const renderDatedValue = (
    key: TimeserieMessageKey,
    value: Option<number>,
    date: Option<Date>
) =>
    value.map(v =>
        DIV(
            'stat__item',
            DIV('stat__label', tr.ts(key)),
            DIV('stat__value', v),
            DIV(
                'stat__date',
                date.map(d => `(${formatDate(d)})`)
            )
        )
    );

const getDateOfValue = (param: Parameter, val: number) => {
    const elem = getTimeserieBetweenDates(param.id).map(dataElem =>
        dataElem.ts.find(e => e.value * e.factor === val)
    );
    return elem.chain(e => fromNullable(e).map(p => p.date));
};

const getMinDateInDataElem = (dataElem: PlotDataElement) => {
    if (dataElem.ts.length > 0) {
        return some(
            dataElem.ts
                .map(point => point.date)
                .reduce((acc, val) => (val < acc ? val : acc))
        );
    } else {
        return none;
    }
};

const getMaxDateInDataElem = (dataElem: PlotDataElement) => {
    if (dataElem.ts.length > 0) {
        return some(
            dataElem.ts
                .map(point => point.date)
                .reduce((acc, val) => (val > acc ? val : acc))
        );
    } else {
        return none;
    }
};

const valueWindow = (param: Parameter) => {
    const s = getWindowStart().fold(
        remoteToOption(getTimeserieAsPlotData(param.id))
            .chain(dataElem => getMinDateInDataElem(dataElem))
            .map(n => new Date(n)),
        s => some(s.date)
    );

    const e = getWindowEnd().fold(
        remoteToOption(getTimeserieAsPlotData(param.id))
            .chain(dataElem => getMaxDateInDataElem(dataElem))
            .map(n => new Date(n)),
        e => some(e.date)
    );
    return s.chain(start => e.map(end => renderParamDates(start, end)));
};

const valueOutOfWindow = (param: Parameter) => {
    const s = remoteToOption(getTimeserieAsPlotData(param.id)).chain(dataElem =>
        getMinDateInDataElem(dataElem)
    );
    const e = remoteToOption(getTimeserieAsPlotData(param.id)).chain(dataElem =>
        getMaxDateInDataElem(dataElem)
    );
    return s.chain(start =>
        e.map(end =>
            renderParamDatesOutofWindow(new Date(start), new Date(end))
        )
    );
};

// const renderUnit = (param: Parameter) =>
//     DIV(
//         { className: 'stat__item' },
//         DIV({ className: 'stat__label' }, `en ${param.unit}`) // TODO Translations
//     );

const valueMin = (param: Parameter, dataElem: BetweenDates) => {
    const minOpt = getMinInDataElem(dataElem);
    return renderDatedValue(
        'valueMinShort',
        minOpt,
        minOpt.chain(min => getDateOfValue(param, min)).map(t => new Date(t))
    );
};

const valueMax = (param: Parameter, dataElem: BetweenDates) => {
    const maxOpt = getMaxInDataElem(dataElem);
    return renderDatedValue(
        'valueMaxShort',
        maxOpt,
        maxOpt.chain(max => getDateOfValue(param, max)).map(t => new Date(t))
    );
};
const valueMed = (dataElem: BetweenDates) => {
    const k = getSelectedKind().getOrElse('surface');
    if (k === 'ground-quantity') {
        return DIV({});
    }
    return renderValue('valueMedShort', getMedian(dataElem));
};
const valueAvg = (dataElem: BetweenDates) =>
    renderValue('valueAvgShort', getAverage(dataElem));

const valueP10 = (dataElem: BetweenDates) =>
    renderValue('valueP10', getPercentile(dataElem, 0.1));
const valueP90 = (dataElem: BetweenDates) =>
    renderValue('valueP90', getPercentile(dataElem, 0.9));
const valueCount = (dataElem: BetweenDates) =>
    renderValue('valueCount', some(dataElem.ts.length));

const valueCountDNQ = (dataElem: BetweenDates) => {
    const ndqLength = dataElem.ts.filter(t => t.factor < 1).length;

    return renderValue('valueCountDNQ', some(ndqLength));
};
// getTimeserieBetweenDates(param.id)
const render = (param: Parameter) =>
    getTimeserieBetweenDates(param.id).map(dataElem =>
        DIV(
            'legend-item__stat',
            // renderUnit(param),
            valueWindow(param),
            valueCount(dataElem),
            valueMin(param, dataElem),
            valueP10(dataElem),
            valueMed(dataElem),
            valueAvg(dataElem),
            valueP90(dataElem),
            valueMax(param, dataElem),
            getSelectedKind().chain(kind =>
                kind !== 'ground-quantity' ? valueCountDNQ(dataElem) : none
            )
        )
    );

export const renderDataNotInWindow = (param: Parameter) =>
    DIV('legend-item__stat', valueOutOfWindow(param));

export const renderDataNotOnStation = () =>
    DIV(
        'legend-item__stat',
        DIV('stat__label', DIV('stat__dates', tr.ts('noDataForStation')))
    );

export default render;
