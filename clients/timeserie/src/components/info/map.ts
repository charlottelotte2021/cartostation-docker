import { Option, none, some } from 'fp-ts/lib/Option';
import { DIV, SPAN, H1, A } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { markdown } from 'sdi/ports/marked';
import { getLang, getRoot } from 'sdi/app';
import { Feature } from 'sdi/source';
import { getFeaturePropOption } from 'sdi/util';
import { nameToString } from 'sdi/components/button/names';
import {
    getCurrentFeature,
    getSelectedKind,
    findSurfaceWaterBody,
} from '../../queries/timeserie';
import { ParameterType } from '../../types';
import mapLegend from '../legend/map';
import { levelSwitch, modeSwitch } from '../button';
import renderStationLegend from '../legend/station-legend';

// const kindLabel = (kind: ParameterType) => {
//     switch (kind) {
//         case 'ground-quantity':
//             return tr.ts('legendGroundQuantity');
//         case 'ground-quality':
//             return tr.ts('legendGroundQuality');
//         case 'surface':
//             return tr.ts('legendSurface');
//     }
// };

// const renderKind = () =>
//     H2(
//         {},
//         getSelectedKind().map(kind => kindLabel(kind))
//     );

// CLEAN ME
const renderKindMono = () => H1({}, tr.ts('welcomeMessage'));
const renderHelptextHome = () => DIV('pitch', markdown(tr.ts('helptext:home')));

const renderName = (f: Feature) =>
    getFeaturePropOption(f, 'nom').map(prop =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('StationName')}: `),
            SPAN('station-info-item__value', `${prop}`)
        )
    );

const renderCoord = (f: Feature) =>
    DIV(
        'station-info-item',
        SPAN('station-info-item__label', `${tr.ts('Coordinates')}: `),

        SPAN(
            {},
            getFeaturePropOption(f, 'x_ref_m').map(prop =>
                SPAN({}, `(X: ${prop}, `)
            ), // ground quantity and quality
            getFeaturePropOption(f, 'x_lamb72').map(prop =>
                SPAN({}, `(X: ${prop}, `)
            ) // surface
        ),
        SPAN(
            {},
            getFeaturePropOption(f, 'y_ref_m').map(prop =>
                SPAN({}, `Y: ${prop})`)
            ), // ground quantity and quality
            getFeaturePropOption(f, 'y_lamb72').map(prop =>
                SPAN({}, `Y: ${prop})`)
            ) // surface
        )
    );

const renderZCoord = (f: Feature) =>
    getFeaturePropOption(f, 'z_ref_m').map(prop =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('ZCoord')}: `),
            SPAN('station-info-item__value', `${prop}`)
        )
    );

// ground param:
const renderRefOuvrage = (f: Feature) =>
    getFeaturePropOption(f, 'ref_ouvrage').map(prop =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('RefOuvrage')}: `),
            SPAN('station-info-item__value', `${prop}`)
        )
    );

const renderBECode = (f: Feature) =>
    getFeaturePropOption(f, 'mon_be_code').map(prop =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('NCode')}: `),
            SPAN('station-info-item__value', `${prop}`)
        )
    );

const renderCode = (f: Feature) =>
    getFeaturePropOption(f, 'no_code').map(prop =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('NCode')}: `),
            SPAN('station-info-item__value', `${prop}`)
        )
    );
const renderEUCode = (f: Feature) =>
    getFeaturePropOption(f, 'no_eu_code').map(prop =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('NoEuCode')}: `),
            SPAN('station-info-item__value', `${prop}`)
        )
    );
const renderMes = (f: Feature) =>
    getFeaturePropOption(f, 'station_mes').map(prop =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('StationName')}: `),
            SPAN('station-info-item__value', `${prop}`)
        )
    );

const renderIdOuvrage = (f: Feature) =>
    getFeaturePropOption(f, 'id_ouvrage').map(prop =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('RefOuvrage')}: `),
            SPAN('station-info-item__value', `${prop}`)
        )
    );

const renderTypeOuvrage = (f: Feature) =>
    getFeaturePropOption(
        f,
        getLang() === 'fr' ? 'type_ouvrage' : 'type_ouvrage_nl'
    ).map(prop =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('TypeOuvrage')}: `),
            SPAN('station-info-item__value', `${prop}`)
        )
    );

const renderMonitoring = (f: Feature) => {
    const end = getFeaturePropOption(f, 'end_mon').getOrElse(
        `${tr.ts('Ongoing')}`
    );
    return getFeaturePropOption(f, 'start_mon').map(start =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('Monitoring')}: `),
            SPAN('station-info-item__value', `${start} - ${end}`)
        )
    );
};

const renderHydroUnit = (f: Feature) =>
    unknownToStringOpt(
        getFeaturePropOption(
            f,
            getLang() === 'fr' ? 'nom_gw_ref' : 'naam_gw_ref'
        )
    ).map(name =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('NomGWRef')}: `),
            SPAN('station-info-item__value', name)
        )
    );

const renderAdress = (f: Feature) => {
    const cp = getFeaturePropOption(f, 'ref_cp').getOrElse('');
    return getFeaturePropOption(
        f,
        getLang() === 'nl' ? 'adres_1' : 'adresse_1'
    ).map(street =>
        DIV(
            'station-info-item',
            SPAN(
                { className: 'station-info-item__label' },
                `${tr.ts('Adress')}: `
            ),
            SPAN('station-info-item__value', `${street},  ${cp}`)
        )
    );
};

const renderProfRel = (f: Feature) =>
    getFeaturePropOption(f, 'z_lidar_m').map(prop =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('ProfRelM')}: `),
            SPAN('station-info-item__value', `${prop} m`)
        )
    );
const renderTopCrepineRel = (f: Feature) =>
    getFeaturePropOption(f, 'top_crepine_rel_m').map(prop =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('TopCrepineRelM')}: `),
            SPAN('station-info-item__value', `${prop} m`)
        )
    );
const renderProfCrepineRel = (f: Feature) =>
    getFeaturePropOption(f, 'prof_crepine_rel_m').map(prop =>
        DIV(
            'station-info-item',
            SPAN(
                { className: 'station-info-item__label' },
                `${tr.ts('ProfCrepineRelM')}: `
            ),
            SPAN('station-info-item__value', `${prop} m`)
        )
    );

const renderGroundWaterbody = (f: Feature) =>
    getFeaturePropOption(
        f,
        getLang() === 'nl' ? 'naam_gwb_nl' : 'nom_gwb_fr'
    ).map(prop =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('NomGWBFr')}: `),
            SPAN('station-info-item__value', `${prop}`)
        )
    );

const renderSurfaceWaterbody = (f: Feature) =>
    getFeaturePropOption(f, 'id_waterbody')
        .chain(findSurfaceWaterBody)
        .map(wb =>
            DIV(
                'station-info-item',
                SPAN(
                    'station-info-item__label',
                    `${tr.ts('surfaceWaterBody')}: `
                ),
                SPAN('station-info-item__value', fromRecord(wb.name))
            )
        );

const renderWaterBody = (f: Feature) =>
    getSelectedKind().map(kind => {
        switch (kind) {
            case 'surface':
                return renderSurfaceWaterbody(f);
            case 'ground-quality':
            case 'ground-quantity':
                return renderGroundWaterbody(f);
        }
    });

const renderMonFreq = (f: Feature) =>
    getFeaturePropOption(
        f,
        getLang() === 'nl' ? 'mon_freq_nl' : 'mon_freq'
    ).map(prop =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('MonFreq')}: `),
            SPAN('station-info-item__value', `${prop}`)
        )
    );

const renderGidSite = (f: Feature) =>
    getFeaturePropOption(f, 'gid_site').map(prop =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `${tr.ts('NCode')}: `),
            SPAN('station-info-item__value', `${prop}`)
        )
    );
const renderEUSite = (f: Feature) =>
    getFeaturePropOption(f, 'eu_site').map(prop =>
        DIV(
            'station-info-item',
            SPAN('station-info-item__label', `eu_site: `),
            SPAN('station-info-item__value', `${prop}`)
        )
    );

const unknownToStringOpt = (prop: Option<unknown>) =>
    prop.map(p => (typeof p === 'string' ? p : ''));

const renderSite = (f: Feature) => {
    const siteFR = unknownToStringOpt(getFeaturePropOption(f, 'site_fr'));
    const siteNL = unknownToStringOpt(getFeaturePropOption(f, 'site_nl'));

    return siteFR.fold(
        // default if no fr-name
        siteNL.fold(
            // defautl if no name at all
            DIV({}),
            // if nl-name exists:
            nl_rec =>
                DIV(
                    'station-info-item',
                    SPAN(
                        'station-info-item__label',
                        `${tr.ts('StationName')}: `
                    ),
                    SPAN(
                        'station-info-item__value',
                        fromRecord({ fr: '', nl: nl_rec })
                    )
                )
        ),
        // if there is an fr-name
        fr_rec =>
            DIV(
                'station-info-item',
                SPAN('station-info-item__label', `${tr.ts('StationName')}: `),
                SPAN(
                    'station-info-item__value',
                    fromRecord({ fr: fr_rec, nl: siteNL.getOrElse('') })
                )
            )
    );
};

const renderBrugeo = (f: Feature) =>
    getSelectedKind().chain(kind =>
        kind === 'surface'
            ? none
            : some(
                  DIV(
                      'station-info-item btn btn-3 brugeo label-and-icon',
                      A(
                          {
                              className: 'station-info-brugeo',
                              target: '_blank',
                              href: `${getRoot()}brugeotool/technical/${
                                  f.geometry.coordinates[0]
                              }/${f.geometry.coordinates[1]}`,
                          },
                          SPAN('icon', nameToString('link')),
                          'BrugeoTool'
                      )
                  )
              )
    );

const getMapId = () => '22713aac-e03d-4a18-b0eb-c71f3ca55db1';

const getLayerId = (k: ParameterType) =>
    k === 'ground-quality'
        ? '64f9cdbf-c637-4eeb-bbd3-bb31cb968d58'
        : 'edd6ba77-1788-4352-b520-dab4fc10e868';

const renderViewLink = (f: Feature) =>
    getSelectedKind().chain(kind =>
        kind === 'surface'
            ? none
            : getFeaturePropOption(f, 'id_ouvrage').map(id =>
                  DIV(
                      'station-info-item btn btn-3 ~view label-and-icon',

                      A(
                          {
                              className: 'station-info-view',
                              target: '_blank',
                              href: `${getRoot()}view/${getMapId()}/feature/${getLayerId(
                                  kind
                              )}/${id}`,
                          },
                          SPAN('icon', nameToString('link')),
                          tr.core('map')
                      )
                  )
              )
    );

export const renderCurrentFeatureInfo = () =>
    getCurrentFeature().map(f =>
        DIV(
            'map-info__items__box',
            DIV(
                'map-info__items',
                renderCode(f),
                renderGidSite(f),
                renderBECode(f),
                renderName(f),
                renderSite(f),
                renderMes(f),
                renderAdress(f),
                renderCoord(f),
                renderZCoord(f),
                renderHydroUnit(f),
                renderWaterBody(f),
                renderEUCode(f),
                renderTypeOuvrage(f),
                renderIdOuvrage(f),
                renderRefOuvrage(f),
                renderProfRel(f),
                renderTopCrepineRel(f),
                renderProfCrepineRel(f),
                renderEUSite(f),
                renderMonFreq(f),
                renderMonitoring(f),
                renderViewLink(f),
                renderBrugeo(f)
            )
        )
    );

export const renderSelectedStationList = () =>
    DIV(
        'table-item-list',
        // H3({}, tr.ts('selectedStations')),
        // renderMultiselectHelp(),
        // getSelectedStationList().map(renderStation)
        renderStationLegend()
    );
// const renderMultiselectHelp = () =>
//     DIV(
//         {
//             className: 'helptext',
//             'aria-hidden': true,
//         },
//         markdown(tr.ts('stationMultiselectHelptext'))
//     );

const render = () =>
    DIV(
        'map-info',
        //   renderKind(),
        renderKindMono(),
        levelSwitch(),
        renderHelptextHome(),
        //   renderChangeLevelBtn(),
        //   getLevel().map(l =>
        //       getConfig().map(c => renderChangeModeButton(l, c, getMode()))
        //   ),
        modeSwitch(),
        mapLegend()
    );

export default render;
