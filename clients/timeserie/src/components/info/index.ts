import mapInfo from './map';
import featureInfo from './feature';

const mapOrFeature = (): 'map' | 'feature' => 'map';

export const info = () => {
    const cond = mapOrFeature();
    switch (cond) {
        case 'map':
            return mapInfo();
        case 'feature':
            return featureInfo();
    }
};
