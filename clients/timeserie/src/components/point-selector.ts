import { DIV, SPAN } from 'sdi/components/elements';
import tr, { formatDate, fromRecord, Translated } from 'sdi/locale';
import {
    getSelectedPlotPoint,
    PointOrCluster,
    getLevel,
    getSelectedKind,
    findParameter,
    getZoomWindow,
    getParamUnit,
    getDisplayMode,
    getSelectedParameter,
    getMode,
} from '../queries/timeserie';

import { renderButtonZoomCluster } from './button';
import { configAddWindow, navigateInPlace } from '../events/route';
import { clearPointSelection } from '../events/timeserie';
import { datetimeBEFormated } from 'sdi/util';

const renderParamName = (id: number) => {
    switch (getDisplayMode()) {
        case 'per-station':
            return getSelectedKind()
                .chain(k => findParameter(k, id).map(p => fromRecord(p.name)))
                .getOrElse('no name' as Translated);
        case 'per-parameter':
            return getSelectedParameter()
                .chain(p =>
                    findParameter(p.kind, p.id).map(p => fromRecord(p.name))
                )
                .getOrElse('no name' as Translated);
    }
};

export const renderParamUnit = (id: number) => {
    switch (getDisplayMode()) {
        case 'per-parameter':
            return getSelectedParameter()
                .chain(p => findParameter(p.kind, p.id).chain(getParamUnit))
                .getOrElse('');
        case 'per-station':
            return getSelectedKind()
                .chain(k => findParameter(k, id).chain(getParamUnit))
                .getOrElse('');
    }
};

const renderSelectedPointInfo = (poc: PointOrCluster) => {
    switch (poc.tag) {
        case 'point':
            return getSelectedKind().map(k => {
                let date = formatDate(new Date(poc.point.date));
                if (k === 'ground-quantity') {
                    date = datetimeBEFormated(
                        new Date(poc.point.date)
                    ) as Translated;
                }
                return DIV(
                    'point-info__wrapper',
                    SPAN({
                        className: 'point-info__picto',
                        style: {
                            backgroundColor: poc.color,
                        },
                    }),
                    SPAN(
                        'point-info__value',
                        `${renderParamName(poc.dataId)} :  
                ${poc.point.value * poc.point.factor} ${renderParamUnit(
                            poc.dataId
                        )}, 
                ${tr.ts('selectedPointDateLabel')} ${date}`
                    )
                );
            });
        // return DIV({});
        case 'cluster':
            return DIV(
                { className: 'cluster-info' },
                DIV(
                    {},
                    DIV(
                        {},
                        `${tr.ts('clusterHelptext1')} ${formatDate(
                            poc.start
                        )} ${tr.ts('clusterHelptext2')} ${formatDate(
                            poc.end
                        )}. ${tr.ts('clusterHelptext3')}`
                    )
                    // DIV({}, `${tr.ts('clusterAvg')} : ${poc.average}.`)
                ),
                DIV(
                    { className: 'action' },
                    renderButtonZoomCluster(() => {
                        const { s, e } = getZoomWindow(poc);

                        // clearWindowStart();
                        // clearWindowEnd();
                        clearPointSelection();
                        configAddWindow(
                            // poc.start.getTime(),
                            s.getOrElse(0),
                            false,
                            // poc.end.getTime(),
                            e.getOrElse(0),
                            false
                        ).map(config =>
                            getLevel().map(level =>
                                navigateInPlace(level, config, getMode())
                            )
                        );
                    })
                )
            );
    }
};

const renderSelection = () =>
    DIV(
        { className: 'data-message point-selection-wrapper' },
        // H3({}, tr.ts('pointInfo')),

        getSelectedPlotPoint().fold(
            DIV(
                { className: 'point-info no-selected-point' },
                tr.ts('selectedPointHelptext')
            ),
            p => renderSelectedPointInfo(p)
        )
    );

export default renderSelection;
