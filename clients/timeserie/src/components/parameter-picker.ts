import * as debug from 'debug';
import { some, Option } from 'fp-ts/lib/Option';
import {
    BUTTON,
    DIV,
    H2,
    H4,
    LI,
    NodeOrOptional,
    UL,
} from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { inputText, renderRadioIn } from 'sdi/components/input';
import {
    getSelectedGroupOpt,
    getParameterFilteredList,
    getSelectedParameterRaw,
    getLevel,
    findParameter,
    getParameterSearch,
    getSelectedGroup,
    getMode,
    getGroupListSurface,
    getGroupListGround,
    ParameterWithKind,
    getSelectedKind,
    isSelectedParam,
} from '../queries/timeserie';
import { Parameter, GroupData } from '../remote/timeserie';
import { ParameterSelection, ParameterType } from '../types';
import {
    setSelectedGroup,
    clearSelectedGroup,
    setParameterSearch,
    selectParameterIndex,
} from '../events/timeserie';
import {
    configAddParameter,
    navigateInPlace,
    configRemoveParameter,
    Level,
    navigateConfig,
    onlyForStationModeOpt,
} from '../events/route';
import { deselectParamButton, deselectBigButton } from './button';
import { renderContentSideFooter } from './view/config';

const logger = debug('sdi:param-picker');

const renderHelptextGroupWrapper = () =>
    DIV('helptext', tr.ts('groupPickerHelpText'));

const renderHelptextGroup = () =>
    getSelectedGroupOpt().foldL(
        () => renderHelptextGroupWrapper(),
        () => DIV({})
    );

const renderGroupHeader = () =>
    getSelectedGroupOpt().map(group =>
        DIV(
            'group__item group--selected',
            H4(
                {
                    id: `group-${group.id}`,
                    className: 'group__label',
                },
                `${tr.ts('selectedGroup')} : ${fromRecord(group.name)}`
            ),
            deselectBigButton(clearSelectedGroup)
        )
    );

const renderGroup = (group: GroupData) =>
    DIV(
        {
            className: `group__item`,
            key: `group-${group.id}`,
        },
        fromRecord(group.name)
    );

const renderGroupPicker = () => {
    return DIV(
        'picker-params',
        H2('subtitle', tr.ts('groupPicker')),
        renderGroupHeader(),
        renderHelptextGroup(),
        getLevel().map(l => {
            if (l === 'ground') {
                return DIV(
                    'group__list',
                    renderRadioIn(
                        'group-list',
                        renderGroup,
                        setSelectedGroup,
                        'radio'
                    )(getGroupListGround(), getSelectedGroup())
                );
            } else {
                return DIV(
                    'group__list',
                    renderRadioIn(
                        'group-list',
                        renderGroup,
                        setSelectedGroup,
                        'radio'
                    )(getGroupListSurface(), getSelectedGroup())
                );
            }
        })
    );
};

// const paramClass = (param: Parameter) => {
//     const p = param as CountedParameter;
//     if (p.count === undefined || p.count === NO_DATA) {
//         return 'parameter__item no-count';
//     } else if (p.count === 0) {
//         return 'parameter__item no-data';
//     }
//     return 'parameter__item';
// };

const renderParameter =
    (kind: ParameterType, level: Level) => (param: Parameter) =>
        LI(
            {
                className: 'parameter__item',
                // className: paramClass(param),
                key: `rp-${kind}-${param.id}`,
                onClick: () => {
                    configAddParameter(param.id, kind).map(config => {
                        navigateConfig(level, config, getMode());
                    });
                },
            },
            BUTTON({}, fromRecord(param.name))
        );

const notSelectedParam = (kind: ParameterType) => {
    const selection = getSelectedParameterRaw();
    return (p: Parameter) =>
        selection.findIndex(s => s.id === p.id && s.kind === kind) === -1;
};

// const renderHelpText = () =>
//     DIV({ className: 'helptext' }, tr.ts('helptext:paramsPicker'));

const unselectButton = (id: number) =>
    deselectParamButton(() =>
        getLevel().chain(level =>
            configRemoveParameter(id).map(config =>
                navigateInPlace(level, config, getMode())
            )
        )
    );

const highligthedParamClass = (param: ParameterSelection) =>
    isSelectedParam(param.id) && getMode() === 'param' ? 'active' : 'inactive';

const renderSelectedParam = ({ kind, id }: ParameterWithKind) =>
    DIV(
        {
            className: `tag param ${highligthedParamClass({
                kind,
                id,
            })}`,
            key: `rp-${kind}-${id}`,
            onClick: () => selectParameterIndex(kind, id),
        },
        unselectButton(id),
        DIV(
            'param__label',
            findParameter(kind, id).map(p => fromRecord(p.name))
        )
    );

const checkNoParams = (paramList: NodeOrOptional[]) =>
    paramList.length > 0
        ? paramList
        : DIV('no-params', tr.ts('noDataForStation'));

const paramListForKind = (kind: ParameterType, level: Level) =>
    checkNoParams(
        getParameterFilteredList(kind)
            .params.filter(notSelectedParam(kind))
            .map(renderParameter(kind, level))
    );

const getKind = (level: Level): Option<ParameterType> => {
    if (getMode() === 'param') {
        switch (level) {
            case 'surface':
                return some('surface');
            case 'ground':
                return some('ground-quality');
        }
    }
    return getSelectedKind();
};

const renderParamList = () =>
    getLevel().chain(l =>
        getKind(l).map(k => UL('parameter__list', paramListForKind(k, l)))
    );

const paramPicker = () =>
    DIV(
        'picker-params',
        DIV(
            'selector-params',
            H2('subtitle', tr.ts('paramPicker')),
            inputText({
                key: 'parameter-search',
                get: getParameterSearch,
                set: setParameterSearch,
                attrs: {
                    id: 'parameter-search',
                    placeholder: tr.ts('filter'),
                },
                monitor: e => setParameterSearch(e),
            }),
            renderParamList()
        ),
        DIV(
            'selected-params',
            H2('subtitle', tr.ts('paramsToDisplay')),
            // renderHelpText(),
            DIV(
                'tag__list parameter__list',
                getSelectedParameterRaw().map(renderSelectedParam)
            ),
            onlyForStationModeOpt(getMode()).map(() =>
                renderContentSideFooter()
            )
        )
    );

const render = () =>
    DIV('pickers__wrapper', renderGroupPicker(), paramPicker());

export default render;

logger('loaded');
