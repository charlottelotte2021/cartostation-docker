import { Option, none, some, fromNullable } from 'fp-ts/lib/Option';

import {
    create,
    defaultInteraction,
    multiSelectOptions,
    FeaturePath,
} from 'sdi/map';
import { DIV } from 'sdi/components/elements';

import { getBaseLayer, getView, getMapInfo } from 'timeserie/src/queries/map';
import {
    updateView,
    setScaleLine,
    addVisibleGroundLayer,
    removeVisibleGroundLayer,
    setView,
    setInteraction,
} from 'timeserie/src/events/map';
import { waterMapId, ParameterType } from '../types';
import {
    getSelectedStationList,
    getSelectedKind,
    getLevel,
    getWindowStart,
    getWindowEnd,
    getMode,
} from 'timeserie/src/queries/timeserie';
import {
    configAddStations,
    navigateInPlace,
    navigateIndex,
} from '../events/route';

import * as debug from 'debug';
import {
    lidToParameterType,
    clearWindowStart,
    clearWindowEnd,
    clearWaterBodySelection,
} from '../events/timeserie';
import { getLayout } from '../queries/app';
// import geocoder from './geocoder';
import geocoder from 'sdi/components/geocoder';
import { mapRightToolsWithRegio } from 'sdi/map/controls';
import { dispatchK } from 'sdi/shape';
import tr from 'sdi/locale';
import { noop } from 'sdi/util';

const logger = debug('sdi:route');

let mapUpdate: Option<() => void> = none;
let mapSetTarget: Option<(e: HTMLElement) => void> = none;

const {
    setLocalSelectionType,
    clearLocalSelectionType,
    checkLocalSelectionType,
} = (() => {
    let ty: Option<ParameterType> = none;

    const setLocalSelectionType = (pt: ParameterType) =>
        ty.foldL(() => {
            ty = some(pt);
        }, noop);

    const clearLocalSelectionType = () => (ty = none);

    const checkLocalSelectionType = (pt: ParameterType) =>
        ty.map(t => pt === t).getOrElse(true);

    return {
        setLocalSelectionType,
        clearLocalSelectionType,
        checkLocalSelectionType,
    };
})();

const selectStations = (fps: FeaturePath[]) => {
    if (fps.length > 0) {
        clearLocalSelectionType();
        clearWaterBodySelection();

        getWindowStart().map(s => {
            if (s.isAutomatic) {
                clearWindowStart();
            }
        });
        getWindowEnd().map(e => {
            if (e.isAutomatic) {
                clearWindowEnd();
            }
        });
        const stations: number[] = [];
        fps.map(fp => fp.featureId).forEach(elem => {
            // logger(`featureId: ${elem}, type: ${typeof elem}`);
            if (typeof elem === 'number') {
                stations.push(elem);
            } else if (typeof elem === 'string') {
                stations.push(parseInt(elem, 10));
            }
        });
        fromNullable(fps[0].layerId)
            .chain(k => lidToParameterType(k))
            .map(k => {
                configAddStations(stations, k).map(config => {
                    getLevel().map(level => {
                        if (getLayout() === 'index') {
                            if (k === 'ground-quality') {
                                addVisibleGroundLayer('quality');
                                removeVisibleGroundLayer('quantity');
                            } else if (k === 'ground-quantity') {
                                addVisibleGroundLayer('quantity');
                                removeVisibleGroundLayer('quality');
                            }
                        }
                        navigateInPlace(level, config, getMode());
                    });
                });
            });
    } else {
        getLevel().map(navigateIndex);
    }
};

const selectOptions = multiSelectOptions({
    selectFeatures: selectStations,
    getSelected: () =>
        getSelectedStationList().map(({ kind, id }) => ({
            layerId: kind,
            featureId: id,
        })),
    filter: (_feature, layer) =>
        lidToParameterType(layer.get('id')).fold(false, lt => {
            // if ((lt === 'ground-quality' && !isGroundVisible('quality'))
            //     || (lt === 'ground-quantity' && !isGroundVisible('quantity'))) {
            //     return false;
            // }
            return getSelectedKind().foldL(
                () => {
                    const result = checkLocalSelectionType(lt);
                    if (result) {
                        setLocalSelectionType(lt);
                    }
                    return result;
                },
                kind => kind === lt
            );
        }),
});

const attachMap = (element: HTMLElement | null) => {
    mapUpdate = mapUpdate.foldL(
        () => {
            const { update, setTarget, selectable } = create(waterMapId, {
                getBaseLayer,
                getView,
                getMapInfo,

                updateView,
                setScaleLine,

                element,
            });

            selectable(selectOptions, defaultInteraction);

            mapSetTarget = some(setTarget);
            update();
            return some(update);
        },
        update => some(update)
    );

    if (element) {
        mapSetTarget.map(f => f(element));
    }
};

const render = () => {
    mapUpdate.map(f => f());
    return DIV(
        {
            className: 'map-wrapper',
            'aria-label': `${tr.core('map')}`,
        },
        DIV({
            id: waterMapId,
            key: waterMapId,
            className: 'map',
            ref: attachMap,
        }),
        geocoder(setView, setInteraction),
        mapRightToolsWithRegio(dispatchK('port/map/view'), getView)
    );
};

export default render;

logger('loaded');
