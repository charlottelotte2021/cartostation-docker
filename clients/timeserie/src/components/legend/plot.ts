import * as debug from 'debug';
import { DIV } from 'sdi/components/elements';
import { getColor, getDisplayMode } from 'timeserie/src/queries/timeserie';
import { none, some } from 'fp-ts/lib/Option';
import { TableMode } from 'timeserie/src/types';

const logger = debug('sdi:events/timeserie');

export const renderStyleLegendItem = (i: number, displayMode: TableMode) =>
    getDisplayMode() === displayMode
        ? some(
              DIV(
                  { className: 'item-style' },
                  DIV({
                      className: 'item-style--element',
                      style: {
                          background: getColor(i),
                      },
                  })
              )
          )
        : none;

logger('loaded');
