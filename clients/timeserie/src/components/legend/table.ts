// import { DIV, DIV, H3 } from 'sdi/components/elements';
// import {
//     getDisplayMode,
//     getSelectedParameterList,
//     // getSelectedStationList,
// } from 'timeserie/src/queries/timeserie';
// import tr from 'sdi/locale';
// import { renderLegendItem } from './param-legend';
// import { renderSelectedStationList } from '../info/map';

// const renderParameterList = () =>
//     DIV(
//         { className: 'table-item-list' },
//         H3({}, tr.ts('parameterToDisplay')),
//         getSelectedParameterList().map(renderLegendItem)
//     );

// const renderParameters = () => DIV({}, renderParameterList());

// const renderSelectHeader = (station: StationSelection) => {
//     const csvBtn = tryNumber(station.id).map(renderButtonCSVAllParameters);
//     return DIV({ className: 'name' }, getStationName(station), csvBtn);
// };

// export const renderStation = (station: StationSelection, index: number) => {
//     const isSelected = getSelectedStationIndex() === index;
//     const key = `station ${station.id}`;
//     if (isSelected) {
//         return DIV(
//             `station-item ${station.id} selected`,
//             renderCollapsibleWrapper(
//                 key,
//                 renderSelectHeader(station),
//                 renderCurrentFeatureInfo()
//             )
//         );
//     } else {
//         return DIV(
//             'station-item',
//             BUTTON(
//                 {
//                     className: 'station-btn',
//                     onClick: () => setSelectedStationIndex(index),
//                 },
//                 renderSelectHeader(station)
//             )
//         );
//     }
// };

// const renderPerStationsActions = () => DIV({}, renderButtonCSVAllStations());

// const renderBody = () => {
//     switch (getDisplayMode()) {
//         case 'per-station':
//             return DIV(
//                 'per-station',
//                 renderSelectedStationList(),
//                 renderParameters()
//             );
//         case 'per-parameter':
//             return DIV(
//                 'per-parameter',
//                 renderParameters(),
//                 renderSelectedStationList()
//             );
//     }
// };

// const render = () => DIV({}, renderBody());

// export default render;
