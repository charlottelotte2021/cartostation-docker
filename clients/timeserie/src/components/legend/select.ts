import { renderSelectFilter } from 'sdi/components/input';
import { Feature } from 'sdi/source';
import { DIV, LI } from 'sdi/components/elements';
import { some } from 'fp-ts/lib/Option';
import { getLang } from 'sdi/app';
import { getFeaturePropOption } from 'sdi/util';
import tr from 'sdi/locale';
import { sort } from 'fp-ts/lib/Array';
import { scopeOption } from 'sdi/lib';
import { unsafeCompare } from 'fp-ts/lib/Ord';
import { ParameterType } from 'timeserie/src/types';
import { selectFeature } from 'timeserie/src/events/timeserie';

const renderPseudoSelected = () =>
    DIV(
        {
            className: 'select-filter__title',
        },
        tr.ts('selectStation')
    );

const PSEUDO_SELECTED_ID = -1;

export const pseudoSelected = some<Feature>({
    type: 'Feature',
    id: PSEUDO_SELECTED_ID,
    geometry: {
        type: 'Point',
        coordinates: [0, 0],
    },
    properties: {},
});

const setoidFeature = {
    equals: (a: Feature, b: Feature) => {
        if (typeof a.id === typeof b.id) {
            return a.id === b.id;
        }
        return false;
    },
};

const renderItem = (keys: LangKeys) => (feature: Feature) =>
    feature.id === PSEUDO_SELECTED_ID
        ? renderPseudoSelected()
        : DIV('', featureToString(keys)(feature));

interface LangKeys {
    fr: string;
    nl: string;
}

export const featureToString = (keys: LangKeys) => (feature: Feature) => {
    switch (getLang()) {
        case 'fr':
            return getFeaturePropOption<string>(feature, keys.fr).getOrElse(
                feature.id.toString()
            );
        case 'nl':
            return getFeaturePropOption<string>(feature, keys.nl).getOrElse(
                feature.id.toString()
            );
        default:
            return feature.id.toString();
    }
};

const selectFeatureInput = (keys: LangKeys, kind: ParameterType) =>
    renderSelectFilter<Feature>(
        setoidFeature,
        renderItem(keys),
        selectFeature(kind),
        featureToString(keys)
    );

const renderStationListItem =
    (keys: LangKeys) => (feature: Feature, kind: ParameterType) =>
        LI(
            {
                className: 'station__item',
                key: `rs-${feature.id}`,
                onClick: () => selectFeature(kind)(feature),
            },
            featureToString(keys)(feature)
        );

export const renderFeatureSurface = renderStationListItem({
    fr: 'gid_site',
    nl: 'gid_site',
});

export const renderFeatureGroundQual = renderStationListItem({
    fr: 'no_code',
    nl: 'no_code',
});

export const renderFeatureGroundQuan = renderStationListItem({
    fr: 'nom',
    nl: 'nom',
});

export const selectFeatureSurface = selectFeatureInput(
    {
        fr: 'gid_site',
        nl: 'gid_site',
    },
    'surface'
);

export const selectFeatureGroundQual = selectFeatureInput(
    {
        fr: 'no_code',
        nl: 'no_code',
    },
    'ground-quality'
);

export const selectFeatureGroundQuant = selectFeatureInput(
    {
        fr: 'nom',
        nl: 'nom',
    },
    'ground-quantity'
);

const sortFeatures = (key: string) =>
    sort({
        equals: (x: Feature, y: Feature) =>
            scopeOption()
                .let('a', getFeaturePropOption<string>(x, key))
                .let('b', getFeaturePropOption<string>(y, key))
                .map(({ a, b }) => a === b)
                .getOrElse(false),
        compare: (x: Feature, y: Feature) =>
            scopeOption()
                .let('a', getFeaturePropOption<string>(x, key))
                .let('b', getFeaturePropOption<string>(y, key))
                .map(({ a, b }) => unsafeCompare(a, b))
                .getOrElse(-1),
    });

export const sortSurfaceFeatures = sortFeatures('gid_site');
export const sortGroundQualityFeatures = sortFeatures('no_code');
export const sortGroundQuantityFeatures = sortFeatures('nom');
