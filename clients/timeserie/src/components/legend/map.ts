import { DIV, H3, SPAN } from 'sdi/components/elements';
import tr, { Translated, fromRecord } from 'sdi/locale';
import { legendRenderer, OpacitySelector } from 'sdi/components/legend';
import { nameToString, IconName } from 'sdi/components/button/names';
import {
    findGroundWaterBody,
    getFeatureCollectionGQuality,
    getFeatureCollectionGQuantity,
    getFeatureCollectionSurface,
    getLevel,
    getLevelFromKind,
    getMode,
    getSelectedStationList,
    getSelectedWaterBody,
    getStationName,
    getSurfcaeWaterbodyColor as getSurfaceWaterbodyColor,
    GROUND_WATERBODIES,
    mainSurfaceWaterbodies,
    otherSurfaceWaterbodies,
} from 'timeserie/src/queries/timeserie';
import {
    getConfig,
    Layout,
    Level,
    Mode,
    navigateConfig,
} from 'timeserie/src/events/route';
import {
    removeVisibleGroundLayer,
    addVisibleGroundLayer,
    setHydroVisibility,
} from 'timeserie/src/events/map';
import { makeIcon, makeLabel, makeLabelAndIcon } from 'sdi/components/button';
import {
    isGroundVisible,
    findMetadata,
    getHydroLayerInfos,
    isHydroLayerVisible,
    getView,
    isSelectedFeature,
} from 'timeserie/src/queries/map';
import {
    selectFeatureSurface,
    selectFeatureGroundQual,
    selectFeatureGroundQuant,
    pseudoSelected,
    sortGroundQualityFeatures,
    sortSurfaceFeatures,
    sortGroundQuantityFeatures,
} from './select';
import { getLayout } from 'timeserie/src/queries/app';
import { fromNullable, fromPredicate } from 'fp-ts/lib/Option';
import { setZoom } from 'timeserie/src/events/map';
import { notEmpty } from 'sdi/util';
import { ParameterType, StationSelection } from 'timeserie/src/types';
import {
    clearStationSelection,
    clearWaterBodySelection,
    deselectStation,
    selectWaterBody,
} from 'timeserie/src/events/timeserie';
import { WaterBody } from 'timeserie/src/remote/timeserie';
import { renderPicto, keyFromString } from '../pictos';

const visibleButtonRight = makeIcon('toggle-off', 2, 'eye', {
    position: 'top-right',
    text: () => tr.ts('hideLayer'),
});
const notVisibleButtonRight = makeIcon('toggle-on', 2, 'eye-slash', {
    position: 'top-right',
    text: () => tr.ts('showLayer'),
});
const visibleButtonLeft = makeIcon('toggle-off', 2, 'eye', {
    position: 'top-left',
    text: () => tr.ts('hideLayer'),
});
const notVisibleButtonLeft = makeIcon('toggle-on', 2, 'eye-slash', {
    position: 'top-left',
    text: () => tr.ts('showLayer'),
});

const renderVisibleGroundQualityBtn = () =>
    visibleButtonRight(() => removeVisibleGroundLayer('quality'));

const renderInvisibleGroundQualityBtn = () =>
    notVisibleButtonRight(() => addVisibleGroundLayer('quality'));

const renderVisibleGroundQuantityBtn = () =>
    visibleButtonRight(() => removeVisibleGroundLayer('quantity'));

const renderInvisibleGroundQuantityBtn = () =>
    notVisibleButtonRight(() => addVisibleGroundLayer('quantity'));

const isSelected = (legendLevel: Level) =>
    getLevel().fold('selected', level =>
        level === legendLevel ? 'selected' : 'unselected'
    );

// const waterbodyBtn = makeIcon('select', 3, 'mouse-pointer', {
//     position: 'top-left',
//     text: () => tr.ts('selectAllStation'),
// });
const isWaterbodySelected = (wb: WaterBody, parameterType: ParameterType) =>
    getSelectedWaterBody(parameterType)
        .map(wbs => wbs.indexOf(wb) > -1)
        .getOrElse(false);

const waterbodySelectedClass = (
    waterbodies: WaterBody[],
    parameterType: ParameterType
) => {
    const selected = getSelectedWaterBody(parameterType)
        .map(wbs => waterbodies.every(waterbody => wbs.indexOf(waterbody) > -1))
        .getOrElse(false);
    return selected ? 'selected' : 'unselected';
};

const switchWaterbodySelection = (
    wb: WaterBody[],
    parameterType: ParameterType
) =>
    wb.find(w => isWaterbodySelected(w, parameterType)) != undefined
        ? clearWaterBodySelection()
        : selectWaterBody(parameterType)(wb);

const renderLegendItem = (
    color: string,
    picto: IconName,
    label: Translated,
    labelCode: string,
    waterbodies: WaterBody[], // some legend item groups differents waterbodies
    parameterType: ParameterType
) =>
    DIV(
        {
            className: `legend-item selectable ${labelCode} ${waterbodySelectedClass(
                waterbodies,
                parameterType
            )}`,
            'aria-hidden': true,
            onClick: () =>
                onlyForStationModeOpt(getMode()).map(() =>
                    switchWaterbodySelection(waterbodies, parameterType)
                ),
        },

        DIV(
            {
                className: 'item-style',
                style: {
                    color,
                    fontSize: '12pt',
                },
            },
            nameToString(picto)
        ),
        onlyForStationModeOpt(getMode()).map(() =>
            DIV(
                {
                    className: 'picto',
                },
                keyFromString(labelCode).map(renderPicto)
            )
        ),
        DIV('item-label', label)
    );

const renderSelectSurface = () =>
    getFeatureCollectionSurface().map(fs =>
        selectFeatureSurface(
            sortSurfaceFeatures(fs.features.filter(f => !isSelectedFeature(f))),
            pseudoSelected
        )
    );

const onlyOnIndex = fromPredicate((l: Layout) => l === 'index');

const renderStationSelectorSurface = () =>
    onlyForStationModeOpt(getMode()).map(() =>
        DIV(
            'station-selectors',
            renderSelectSurface(),
            DIV(
                'tag__list',
                ...getSelectedStationList().map(renderSelectedStation)
            ),
            DIV(
                'actions',
                renderConfirmStations('surface'),
                renderCancelSelection()
            )
        )
    );

const renderSelectSurfaceWrapper = () =>
    onlyOnIndex(getLayout()).map(() =>
        DIV('select-station__wrapper', renderStationSelectorSurface())
    );

const confirmStationsBtn = makeLabelAndIcon(
    'navigate',
    1,
    'chevron-right',
    () => tr.ts('goToChooseParams')
);
// makeIcon('navigate', 2, 'play-circle', {
//     position: 'right',
//     text: () => tr.ts('chooseParams'),
// });
const renderConfirmStations = (kind: ParameterType) =>
    notEmpty(getSelectedStationList()).chain(sl =>
        fromNullable(sl.find(s => s.kind === kind)).map(() =>
            getConfig().map(c =>
                confirmStationsBtn(() => {
                    kind === 'ground-quality'
                        ? removeVisibleGroundLayer('quantity')
                        : removeVisibleGroundLayer('quality');
                    navigateConfig(getLevelFromKind(kind), c, getMode());
                })
            )
        )
    );

const cancelSelectionBtn = makeLabel('clear', 2, () => tr.core('cancel'));
const renderCancelSelection = () =>
    notEmpty(getSelectedStationList()).map(() =>
        cancelSelectionBtn(() => {
            clearStationSelection();
            clearWaterBodySelection();
        })
    );

const deleteValueButton = (station: string) =>
    makeIcon('clear', 3, 'times', {
        position: 'top-right',
        text: () => `${tr.core('remove')}: ${station}` as Translated,
    });

const renderSelectedStation = (s: StationSelection) => {
    const stationName: string = getStationName(s);
    return DIV(
        'tag',
        deleteValueButton(stationName)(() => deselectStation(s)),
        SPAN('tag__value', stationName)
    );
};

const renderSurfaceTitle = () => {
    switch (getMode()) {
        case 'param':
            return H3(
                'legend-title__label',
                tr.ts('legendSurfaceQualityLabel')
            );
        case 'station':
            return H3(
                'legend-title__label',
                tr.ts('legendSurfaceQualityLabelSelect')
            );
    }
};

const renderSurface = () =>
    DIV(
        {
            key: 'legendSurface',
            className: `legend-group legend-group--${isSelected('surface')}`,
        },
        DIV('legend-title', renderSurfaceTitle()),
        ...mainSurfaceWaterbodies().map(wb =>
            renderLegendItem(
                getSurfaceWaterbodyColor(wb.code),
                'circle',
                fromRecord(wb.name),
                wb.code,
                [wb],
                'surface'
            )
        ),
        renderLegendItem(
            getSurfaceWaterbodyColor('other'),
            'circle',
            tr.ts('watherbodyOther'),
            'surface-other',
            otherSurfaceWaterbodies(),
            'surface'
        ),
        renderSelectSurfaceWrapper()
    );

const renderSelectGroundQuality = () =>
    getFeatureCollectionGQuality().map(fs =>
        selectFeatureGroundQual(
            sortGroundQualityFeatures(
                fs.features.filter(f => !isSelectedFeature(f))
            ),
            pseudoSelected
        )
    );

const renderStationSelectorsGQuality = () =>
    onlyForStationModeOpt(getMode()).map(() =>
        DIV(
            'station-selectors',
            renderSelectGroundQuality(),
            DIV(
                'tag__list',
                ...getSelectedStationList()
                    .filter(s => s.kind === 'ground-quality')
                    .map(renderSelectedStation)
            ),
            DIV(
                'actions',
                renderConfirmStations('ground-quality'),
                renderCancelSelection()
            )
        )
    );

const renderSelectGroundQualityWrapper = () =>
    onlyOnIndex(getLayout()).map(() =>
        DIV('select-station__wrapper', renderStationSelectorsGQuality())
    );

const renderGroundQuality = () =>
    DIV(
        {
            key: 'legendGroundQuality',
            className: `legend-group legend-group--${isSelected('ground')}`,
        },
        DIV(
            'legend-title',
            renderVisibleGroundQualityBtn(),
            renderGroundQualTitle()
        ),
        ...GROUND_WATERBODIES.map(({ id, color }) =>
            findGroundWaterBody(id).map(wb =>
                renderLegendItem(
                    color,
                    'circle',
                    fromRecord(wb.name),
                    wb.code,
                    [wb],
                    'ground-quality'
                )
            )
        ),
        renderSelectGroundQualityWrapper()
    );

const renderSelectGroundQuantity = () =>
    getFeatureCollectionGQuantity().map(fs =>
        selectFeatureGroundQuant(
            sortGroundQuantityFeatures(
                fs.features.filter(f => !isSelectedFeature(f))
            ),
            pseudoSelected
        )
    );

const onlyForStationModeOpt = fromPredicate((m: Mode) => m === 'station');

const renderStationSelectorsGQuantity = () =>
    onlyForStationModeOpt(getMode()).map(() =>
        DIV(
            'station-selectors',
            renderSelectGroundQuantity(),
            DIV(
                'tag__list',
                ...getSelectedStationList()
                    .filter(s => s.kind === 'ground-quantity')
                    .map(renderSelectedStation)
            ),
            DIV(
                'actions',
                renderConfirmStations('ground-quantity'),
                renderCancelSelection()
            )
        )
    );

const renderSelectGroundQuantityWrapper = () =>
    onlyOnIndex(getLayout()).map(() =>
        DIV('select-station__wrapper', renderStationSelectorsGQuantity())
    );

const renderGroundQuantity = () =>
    DIV(
        {
            key: 'legendGroundQuantity',
            className: `legend-group legend-group--${isSelected('ground')}`,
        },
        DIV(
            'legend-title',
            renderVisibleGroundQuantityBtn(),
            H3('legend-title__label', tr.ts('legendGroundQuantityLabel'))
        ),
        ...GROUND_WATERBODIES.map(({ id, color }) =>
            findGroundWaterBody(id).map(wb =>
                renderLegendItem(
                    color,
                    'dot-circle',
                    fromRecord(wb.name),
                    wb.code,
                    [wb],
                    'ground-quantity'
                )
            )
        ),
        renderSelectGroundQuantityWrapper()
    );

const renderGroundQualTitle = () => {
    switch (getMode()) {
        case 'param':
            return H3('legend-title__label', tr.ts('legendGroundQualityLabel'));
        case 'station':
            return H3(
                'legend-title__label',
                tr.ts('legendGroundQualityLabelSelect')
            );
    }
};

const renderGroundQualityHidden = () =>
    DIV(
        {
            key: 'legendGroundQuality-hidden',
            className: `legend-group legend-group--${isSelected('ground')}`,
        },
        DIV(
            'legend-title',
            renderInvisibleGroundQualityBtn(),
            renderGroundQualTitle()
        )
    );

const renderGroundQuantityHidden = () =>
    DIV(
        {
            key: 'legendGroundQuantity-hidden',
            className: `legend-group legend-group--${isSelected('ground')}`,
        },
        DIV(
            'legend-title',
            renderInvisibleGroundQuantityBtn(),
            H3('legend-title__label', tr.ts('legendGroundQuantityLabel'))
        )
    );

const opacitySelector: OpacitySelector = {
    isVisible: false,
    saveStyle: () => void 0,
};

const renderHydroLegend = legendRenderer(
    findMetadata,
    getView,
    setZoom,
    info => {
        switch (isHydroLayerVisible(info.id)) {
            case true:
                return visibleButtonLeft(() =>
                    setHydroVisibility(info.id, false)
                );
            case false:
                return notVisibleButtonLeft(() =>
                    setHydroVisibility(info.id, true)
                );
        }
    },
    opacitySelector
);

const levelLegendSwitch = () =>
    getLevel().map(level => {
        if (level === 'ground') {
            switch (getMode()) {
                case 'station':
                    return [
                        isGroundVisible('quality')
                            ? renderGroundQuality()
                            : renderGroundQualityHidden(),
                        isGroundVisible('quantity')
                            ? renderGroundQuantity()
                            : renderGroundQuantityHidden(),
                    ];
                case 'param':
                    return [
                        isGroundVisible('quality')
                            ? renderGroundQuality()
                            : renderGroundQualityHidden(),
                    ];
            }
        }
        return [renderSurface()];
    });

const render = () =>
    DIV(
        'map-legend',
        // H4({}, tr.ts('legend')),
        levelLegendSwitch(),
        onlyOnIndex(getLayout()).map(() =>
            renderHydroLegend(getHydroLayerInfos())
        )
    );

export default render;
