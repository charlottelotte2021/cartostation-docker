import { DIV, H1, NodeOrOptional } from 'sdi/components/elements';
import { markdown } from 'sdi/ports/marked';
import tr from 'sdi/locale';
import map from '../map';
import legend from '../legend/map';
import { getMode } from '../../queries/timeserie';
import { levelSwitch, modeSwitch } from '../button';
import { renderMainPerParam } from './per-param';

const renderWelcomeMessage = () => H1({}, tr.ts('welcomeMessage'));

const renderHelptextHome = () => DIV('pitch', markdown(tr.ts('helptext:home')));

const renderSidebar = (): NodeOrOptional =>
    DIV(
        'content__side',
        renderWelcomeMessage(),
        levelSwitch(),
        renderHelptextHome(),
        modeSwitch(),
        legend()
    );

const renderMain = () => DIV('content__main', map());

const render = () => {
    const mode = getMode();
    switch (mode) {
        case 'station':
            return DIV(
                'content content--index station',
                renderSidebar(),
                renderMain()
            );
        case 'param':
            return DIV('content content--index param', renderMainPerParam());
    }
};

export default render;
