import { DIV, H2, SPAN } from 'sdi/components/elements';
import map from '../map';
// import legend from '../legend/map';

import { info } from '../info';

import paramPicker from '../parameter-picker';

import {
    deselectBigButton,
    // levelSwitch,
    // modeSwitch,
    renderBackHomeBtn,
    renderButtonPlot,
    renderButtonTable,
} from '../button';
import {
    getMode,
    getSelectedKind,
    getSelectedStationName,
} from 'timeserie/src/queries/timeserie';

import tr from 'sdi/locale';
import { renderMainPerParam } from './per-param';
import {
    clearParameterSelection,
    clearSelectedGroup,
    clearStationSelection,
} from 'timeserie/src/events/timeserie';
import renderStationLegend from '../legend/station-legend';
// import { markdown } from 'sdi/ports/marked';

const renderSidebar = () =>
    DIV({ className: 'content__side' }, info(), renderStationLegend());

// const renderWelcomeMessage = () => H1({}, tr.ts('welcomeMessage'));
// const renderHelptextHome = () => DIV('pitch', markdown(tr.ts('helptext:home')));

// const renderSidebar = (): NodeOrOptional =>
//     DIV(
//         'content__side',
//         renderWelcomeMessage(),
//         levelSwitch(),
//         renderHelptextHome(),
//         modeSwitch(),
//         legend()
//     );

const renderMain = () => {
    const isPiezzo =
        getSelectedKind().getOrElse('surface') === 'ground-quantity'
            ? 'piezzo'
            : '';
    return DIV(
        'content__main',
        DIV(
            `content__params ${isPiezzo}`,
            H2(
                'main-title',
                tr.ts('paramPickerMainTitle'),
                getSelectedStationName().map(name =>
                    SPAN('station-name', ' (', name, ')')
                )
            ),

            paramPicker()
        ),
        DIV('content__map', map(), renderBackHomeBtn())
    );
};

const clearParamAndGroups = () => {
    clearParameterSelection();
    clearSelectedGroup();
};

export const renderContentSideFooter = () =>
    DIV(
        'params__actions',
        renderButtonPlot(),
        renderButtonTable(),
        deselectBigButton(() => {
            getMode() === 'station'
                ? clearParamAndGroups()
                : clearStationSelection();
        })
    );

// const renderViewPerParam = () => {
//     const isPiezzo =
//         getSelectedKind().getOrElse('surface') === 'ground-quantity'
//             ? 'piezzo'
//             : '';

// }

export const render = () => {
    switch (getMode()) {
        case 'param':
            return DIV('content content--config param', renderMainPerParam());
        case 'station':
            return DIV(
                { className: 'content content--config station' },
                renderSidebar(),
                renderMain()
            );
    }
};

export default render;
