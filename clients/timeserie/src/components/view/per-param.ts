import { DIV, H1, H2, NodeOrOptional, SPAN } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { markdown } from 'sdi/ports/marked';

import { getSelectedParameter } from '../../queries/timeserie';
import paramPicker from '../parameter-picker';
import { levelSwitch, modeSwitch } from '../button';
import { stationPicker } from '../station-picker';
import map from '../map';
import legend from '../legend/map';
import { renderContentSideFooter } from './config';

const renderWelcomeMessage = () => H1({}, tr.ts('welcomeMessage'));

const renderHelptextHome = () => DIV('pitch', markdown(tr.ts('helptext:home')));

const renderMapbar = (): NodeOrOptional =>
    DIV(
        'content__up',
        DIV(
            'per-params-infos',
            renderWelcomeMessage(),
            levelSwitch(),
            renderHelptextHome(),
            modeSwitch()
        ),
        map(),
        legend()
    );

const renderParamPicker = () =>
    DIV(
        `content__bottom`,
        paramPicker(),
        DIV(
            'station__picker',
            H2(
                'subtitle',
                tr.ts('stationPickerMainTitle'),
                getSelectedParameter().map(p =>
                    SPAN('parameter-name', ` (${fromRecord(p.name)})`)
                )
            ),
            stationPicker(),
            renderContentSideFooter()
        )
    );

export const renderMainPerParam = () =>
    DIV('content__main', renderMapbar(), renderParamPicker());
