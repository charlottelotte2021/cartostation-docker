import { DIV } from 'sdi/components/elements';
import map from '../map';
import table from '../table';

// import legend from '../legend/table';
import legend from '../legend/index';

import {
    renderButtonParams,
    renderBackHomeBtn,
    renderButtonPlot,
    renderButtonCSV,
    renderDisplaySwitch,
    // renderButtonTableModeStations,
    // renderButtonTableModeParameters,
} from '../button';
import { navigateConfig, getConfig } from 'timeserie/src/events/route';
import renderDatesInputBox from '../date-picker';
import {
    getLevel,
    getMode,
    // getTableMode,
} from 'timeserie/src/queries/timeserie';
import { info } from '../info';
// import { setTableMode } from 'timeserie/src/events/timeserie';

// const perStation = () =>  renderButtonTableModeStations(() => setTableMode('per-station'));
// const perParam = () => renderButtonTableModeParameters(() => setTableMode('per-parameter'));

// const renderDisplaySwitch = () => {
//     const tableMode = getTableMode();

//     switch (tableMode) {
//         case 'per-station':
//             return perParam();
//         case 'per-parameter':
//             return perStation() ;
//     }
// };

const renderContentHeader = () =>
    DIV(
        { className: 'content__header' },
        renderDisplaySwitch(),
        renderDatesInputBox()
        // renderDisplaySwitch(),
    );

const renderContentFooter = () =>
    DIV(
        { className: 'content__footer' },
        renderButtonParams(() =>
            getLevel().map(l =>
                getConfig().map(c => navigateConfig(l, c, getMode()))
            )
        ),
        renderButtonPlot(),
        renderButtonCSV()
    );
const renderSidebar = () =>
    DIV({ className: 'content__side' }, info(), legend());

const renderMain = () =>
    DIV(
        { className: 'content__main' },
        DIV(
            { className: 'content__table' },
            renderContentHeader(),
            table(),
            renderContentFooter()
        ),
        DIV({ className: 'content__map' }, map(), renderBackHomeBtn())
    );

export const render = () =>
    DIV(
        { className: 'content content--config' },
        renderSidebar(),
        renderMain()
    );

export default render;
