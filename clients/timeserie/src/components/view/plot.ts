import { DIV, NodeOrOptional, NODISPLAY } from 'sdi/components/elements';
import map from '../map';

import plotLegend from '../legend/index';
import { info } from '../info';
import plot from '../plot/index';
import {
    renderBackHomeBtn,
    renderButtonTable,
    renderDisplaySwitch,
    renderButtonParams,
    renderButtonCSV,
} from '../button';
import { navigateConfig, getConfig } from 'timeserie/src/events/route';
// import renderNoDataMessage from '../nodata-message';
import renderSelection from '../point-selector';
import {
    getLevel,
    getMode,
    getSelectedParameterRaw,
    getTimeserie,
} from 'timeserie/src/queries/timeserie';
import { foldRemote } from 'sdi/source';
import { TimeserieData } from 'timeserie/src/remote/timeserie';
import tr from 'sdi/locale';
import renderDatesInputBox from '../date-picker';

const renderContentHeader = () =>
    DIV(
        { className: 'content__header' },
        renderDisplaySwitch(),
        renderDatesInputBox(),
        renderSelection()
    );

const renderContentFooter = () =>
    DIV(
        { className: 'content__footer' },
        renderButtonParams(() =>
            getLevel().map(l =>
                getConfig().map(c => navigateConfig(l, c, getMode()))
            )
        ),

        renderButtonTable(),
        renderButtonCSV()
    );

const renderSidebar = () =>
    DIV({ className: 'content__side' }, info(), plotLegend());

const renderLoader = () =>
    DIV(
        {},
        getSelectedParameterRaw().map(({ id }) => {
            const loader = foldRemote<TimeserieData, string, NodeOrOptional>(
                NODISPLAY,
                () =>
                    DIV(
                        { className: 'data-loader__plot ' },
                        DIV(
                            { className: 'loader-label' },
                            `${tr.core('loadingData')} id:${id}`
                        ),
                        DIV({ className: 'loader-spinner' })
                    ),
                _err => DIV({}, `~Error loading ${id}`),
                () => NODISPLAY()
            );
            return loader(getTimeserie(id));
        })
    );

const renderMain = () =>
    DIV(
        { className: 'content__main' },
        DIV(
            { className: 'content__plot' },
            renderContentHeader(),
            DIV({ className: 'content__plot--plot' }, renderLoader(), plot()),
            // renderNoDataMessage(),
            renderContentFooter()
        ),
        DIV({ className: 'content__map' }, map(), renderBackHomeBtn())
    );

const render = () =>
    DIV({ className: 'content content--plot' }, renderSidebar(), renderMain());

export default render;
