import {
    fetchIO, IUserListIO, IUserList, postUnrelatedIO, postIO, deleteIO,
} from 'sdi/source';
import {
    ContactListIO,
    FundingOrgListIO,
    LocalityListIO,
    SiteListIO,
    TeamListIO,
    TeamMemberListIO,
    NovaIO,
    FundingOrgIO,
    FundingOrg,
    SiteIO,
    Site,
    ContactIO,
    Contact,
    TagListIO,
    TeamIO,
    TeamPartialTeamMembers,
    AudienceListIO,
    Reading,
    ReadingIO,
    SchoolSiteIO,
    // ReadingIO,
} from 'angled-core/ref';



export const fetchLocalityList =
    (url: string) => fetchIO(LocalityListIO, url);

export const fetchContactList =
    (url: string) => fetchIO(ContactListIO, url);

export const fetchFundingOrgList =
    (url: string) => fetchIO(FundingOrgListIO, url);

export const fetchSiteList =
    (url: string) => fetchIO(SiteListIO, url);

export const fetchTeamMemberList =
    (url: string) => fetchIO(TeamMemberListIO, url);

export const fetchTeamList =
    (url: string) => fetchIO(TeamListIO, url);

export const fetchUserList =
    (url: string): Promise<IUserList> =>
        fetchIO(IUserListIO, url);

export const fetchTagList =
    (url: string) => fetchIO(TagListIO, url);

export const fetchAudienceList =
    (url: string) => fetchIO(AudienceListIO, url);

export const fetchNova =
    (url: string) => fetchIO(NovaIO, url);

export const postFundingOrg =
    (url: string, data: Partial<FundingOrg>) => postUnrelatedIO(FundingOrgIO, url, data);

export const postSite =
    (url: string, data: Partial<Site>) => postUnrelatedIO(SiteIO, url, data);

export const postContact =
    (url: string, data: Partial<Contact>) => postUnrelatedIO(ContactIO, url, data);

export const postTeam =
    (url: string, data: Partial<TeamPartialTeamMembers>) => postUnrelatedIO(TeamIO, url, data);

export const postReading =
    (url: string, data: Partial<Reading>) => postIO(ReadingIO, url, data);

export const deleteReading =
    (url: string) => deleteIO(url);

// export const postTeam =
//     (url: string, data: Partial<Team>) => postUnrelatedIO(TeamIO, url, data);

export const fetchSchoolSite =
    (url: string) => fetchIO(SchoolSiteIO, url);

