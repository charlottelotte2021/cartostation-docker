
import {
    fetchIO,
    InspireIO,
    fetchPaginatedIO,
    FeatureCollection,
    fetchWithoutValidationIO,
    IServiceBaseLayersIO,
} from 'sdi/source';

export const fetchBaseLayerAll =
    (url: string) => fetchIO(IServiceBaseLayersIO, url);

export const fetchAllDatasetMetadata =
    (url: string) => fetchPaginatedIO(InspireIO, url);


export const fetchLayer =
    (url: string): Promise<FeatureCollection> =>
        fetchWithoutValidationIO(url);
