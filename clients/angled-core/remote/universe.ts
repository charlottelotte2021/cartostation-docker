import {
    putIO,
    fetchIO,
    postIO,
} from 'sdi/source';

import { DomainListIO, TermListIO, Domain, DomainIO, Term, TermIO, DomainMappingListIO } from 'angled-core/ref';

export const fetchDomainList =
    (url: string) => fetchIO(DomainListIO, url);

export const fetchDomainMappingList =
    (url: string) => fetchIO(DomainMappingListIO, url);

export const fetchTermList =
    (url: string) => fetchIO(TermListIO, url);


export const postDomain =
    (url: string, data: Partial<Domain>): Promise<Domain> => postIO(DomainIO, url, data);

export const putDomain =
    (url: string, data: Domain): Promise<Domain> => putIO(DomainIO, url, data);

export const postTerm =
    (url: string, data: Partial<Term>): Promise<Term> => postIO(TermIO, url, data);

export const putTerm =
    (url: string, data: Term): Promise<Term> => putIO(TermIO, url, data);
