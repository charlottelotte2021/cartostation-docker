export default {
    panelNext: {
        fr: 'Panneau suivant',
        en: 'Next panel',
        nl: 'Volgend paneel', // nldone
    },

    panelPrev: {
        fr: 'Panneau précédent',
        en: 'Previous panel ',
        nl: 'Vorig paneel', // nldone
    },
    trueUnit: {
        fr: 'oui',
        en: 'yes',
        nl: 'ja', // nldone
    },
    falseUnit: {
        fr: 'non',
        en: 'no',
        nl: 'nee', //nldone
    },
};
