export default {
    universe: {
        fr: 'Termes',
        en: 'Terms',
        nl: 'Termen', //nldone
    },

    domains: {
        fr: 'Domaines',
        en: 'Domains',
        nl: 'Domeinen', //nldone
    },

    terms: {
        fr: 'Termes',
        en: 'Terms',
        nl: 'Termen', //nldone
    },

    projects: {
        fr: 'Projets',
        nl: 'Projecten', //nldone
        en: 'Projects',
    },

    cancel: {
        fr: 'Annuler',
        nl: 'Annuleren', //nldone
        en: 'Cancel',
    },

    close: {
        fr: 'Fermer',
        nl: 'Sluiten', //nldone
        en: 'Close',
    },

    loadingData: {
        fr: 'Chargement des données',
        nl: 'Data worden geladen', //nldone
        en: 'Loading datas',
    },

    drawMode: {
        fr: 'Dessiner une géométrie',
        nl: 'Teken een geometrische vorm', //nldone
        en: 'Draw geometry',
    },

    selectMode: {
        fr: 'Sélectionner une géométrie',
        nl: 'Selecteer een geometrische vorm', //nldone
        en: 'Select geometry',
    },

    quitAudienceManager: {
        fr: 'Masquer', // 'Arrêter la gestion des audiences',
        nl: 'Verbergen', //'Stop het beheer van publieken', //nldone
        en: 'Stop audience management',
    },
    helptextAudienceManager: {
        fr: `Visualisez et ajoutez des audiences aux unités d'information pour ajuster la visibilité des données`,
        nl: 'Bekijk en voeg doelgroepen toe aan informatie-eenheden om de zichtbaarheid van gegevens aan te passen', //nltodo
    },
    noEditAudienceManager: {
        fr: `Vous ne pouvez changer les audiences des informations que lorsque vous avez le droit de les voir.`,
        nl: 'U kunt de doelgroepen voor informatie alleen veranderen als u het recht heeft om ze te zien.', //nltodo
    },

    removeAudienceBtnLabel: {
        fr: `Enlever l'audience`,
        nl: 'Het publiek verwijderen', //nltodo
    },
    addAudienceBtnLabel: {
        fr: `Ajouter l'audience`,
        nl: 'Het publiek toevoegen', //nltodo
    },
    addAudienceToAllBtnLabel: {
        fr: `Ajouter à toutes les unités visibles`,
        nl: 'Toevoegen aan alle zichtbare eenheden', //nltodo
    },
};
