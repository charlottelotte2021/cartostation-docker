export default {

    noDescription: {
        fr: 'Pas de description disponible',
        en: 'No Description Available',
        nl: 'Geen beschrijving beschikbaar', // nldone
    },

    addDomain: {
        fr: 'Ajouter un domaine',
        en: 'Add a domain',
        nl: 'Domeinnaam toevoegen', // nldone
    },

    addTerm: {
        fr: 'Ajouter un terme',
        en: 'Add a term',
        nl: 'Voeg een term toe', // nldone
    },

    'appTextDomain': {
        fr: `Cette application permet aux opérateurs de Perspective de manipuler les Termes et Domaines.
        Ceux-ci sont utilisés parmi les unités d'informations dans l'application Projets.
        Un domaine est l\'équivalent d\'une catégorie, tandis que les termes d'un domaine constituent les qualificatifs possibles au sein de cette catégorie particulière.`,
        nl: `Deze applicatie stelt de operatoren van Perspective in staat om termen en domeinen te veranderen.
        Deze worden gebruikt door de informatie units van de applicatie Projecten.
        Een domein is het equivalent van een categorie, terwijl de termen binnen een domein de mogelijke specificaties binnen die bepaalde categorie zijn.`, // nldone
        en: `This application allows Perspective operators to manipulate Terms and Domains.
        These are used among the information units in the Urban Plan and Project Monitoring platform.
        A domain is the equivalent of a category, while the terms of a domain are the possible qualifiers within that particular category.`,
    },


    saveDomain: {
        fr: 'Sauvegarder le domaine',
        nl: 'Domein opslaan', // nldone
        en: 'Save domain',
    },

    saveTerm: {
        fr: 'Sauvegarder le terme',
        nl: 'Term opslaan', // nldone
        en: 'Save term',
    },


    editDomain: {
        fr: 'Modifier le domaine',
        nl: 'Domein Wijzigen', // nldone
        en: 'Edit domain',
    },

    editTerm: {
        fr: 'Modifier le terme',
        nl: 'Term wijzigen', // nldone
        en: 'Edit term',
    },


    nameFR: { fr: 'Nom en fançais', nl: 'Naam in het Frans' }, // nldone
    nameNL: { fr: 'Nom en néerlandais', nl: 'Naam in het Nederlands' }, // nldone

    descriptionFR: { fr: 'Description en fançais', nl: 'Beschrijving in het Frans' }, // nldone
    descriptionNL: { fr: 'Description en néerlandais', nl: 'Beschrijving in het Nederlands' }, // nldone

    newDomain: { fr: 'Nouveau domaine', nl: 'Nieuw domein' }, // nldone
    newTerm: { fr: 'Nouveau terme', nl: 'Nieuwe term' }, // nldone

    editPrefix: { fr: 'Modification de ', nl: 'Wijziging van ' }, // nldone
};