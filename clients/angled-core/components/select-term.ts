import { BUTTON, DIV, INPUT } from 'sdi/components/elements';
import { Setoid } from 'fp-ts/lib/Setoid';
import { ReactNode, Component, createElement } from 'react';
import { Option } from 'fp-ts/lib/Option';
import { makeIcon } from 'sdi/components/button';
import { Tooltip } from 'sdi/components/tooltip';
import tr from 'sdi/locale';

const renderSelectItem = <T>(
    renderItem: (a: T) => ReactNode,
    select: (a: T) => void
) => (v: T) =>
        BUTTON({
            className: 'interactive',
            onClick: () => select(v),
        }, renderItem(v));

const renderSelected = <T>(
    renderItem: (a: T) => ReactNode,
    selected: Option<T>
) =>
    selected.fold(DIV({ className: 'selected none' }, '----------------'), s =>
        DIV({ className: 'selected' }, renderItem(s))
    );

export const renderSelect = <T>(
    S: Setoid<T>,
    renderItem: (a: T) => ReactNode,
    select: (a: T) => void
) => (list: T[] | Readonly<T[]>, selected: Option<T>) => {
    const mkItem = renderSelectItem(renderItem, select);
    const tail = list
        .filter(i => selected.fold(true, s => S.equals(i, s) !== true))
        .map(mkItem);

    return DIV(
        {
            className: 'select__wrapper',
            onClick: e => e.currentTarget.classList.toggle('active')
        },
        BUTTON(
            {
                className: 'select'
                // FIXME this is a temporary hack, we should be VDOMy
            },
            renderSelected(renderItem, selected),
            DIV({ className: 'tail' }, ...tail)
        )
    );
};

interface FilterState {
    input: string;
    active: boolean;
}

interface FilterProps<T> {
    list: T[] | Readonly<T[]>;
    renderItem: (a: T) => ReactNode;
    select: (a: T) => void;
    selected: Option<T>;
    S: Setoid<T>;
    toString: (a: T) => string;
}

class SelectFilter<T> extends Component<FilterProps<T>, FilterState> {
    constructor(props: FilterProps<T>) {
        super(props);
    }

    render() {
        const { renderItem, select, list, selected, S, toString } = this.props;
        const inputValue = this.state?.input ?? '';
        const active = this.state?.active ?? false;
        const pat = new RegExp(`${inputValue}.*`, 'i');
        const mkItem = renderSelectItem(renderItem, select);
        const tail = list
            .filter(
                i =>
                    pat.test(toString(i)) &&
                    selected.fold(true, s => S.equals(i, s) !== true)
            )
            .map(mkItem);

        const input = INPUT({
            type: 'text',
            placeholder: tr.core('filter'),
            onChange: el =>
                this.setState({ active, input: el.currentTarget.value })
        });

        const resetState = () => this.setState({ input: '', active: false });

        if (active) {
            const closeTooltip: Tooltip = { text: () => tr.core('cancel'), position: 'bottom' }
            const closeButton = makeIcon('close', 3, 'times', closeTooltip);
            return DIV(
                {
                    className: `select__wrapper active`
                },
                DIV(
                    { className: 'select' },
                    DIV(
                        { className: 'head select--filter' },
                        input,
                        closeButton(resetState)
                    ),
                    DIV(
                        {
                            className: 'tail',
                            onClick: resetState
                        },
                        ...tail
                    )
                )
            );
        }
        return DIV(
            {
                className: `select__wrapper`,
                onClick: () =>
                    this.setState({ input: inputValue, active: true })
            },
            DIV({ className: 'select' }, renderSelected(renderItem, selected))
        );
    }
}

export const renderSelectFilter = <T>(
    S: Setoid<T>,
    renderItem: (a: T) => ReactNode,
    select: (a: T) => void,
    toString: (a: T) => string
) => (list: T[] | Readonly<T[]>, selected: Option<T>) => {
    const props: FilterProps<T> = {
        S,
        renderItem,
        select,
        toString,
        list,
        selected
    };
    return createElement(SelectFilter, props);
};
