import * as debug from 'debug';
import { ReactNode } from 'react';
import { fromNullable } from 'fp-ts/lib/Option';

import { DIV, H2 } from 'sdi/components/elements';
import { renderSelect } from 'sdi/components/input/select';
import { IMapOptions, create, singleSelectOptions } from 'sdi/map';
import { setoidIdentified } from 'sdi/util';
import {
    getCurrentBaseLayer,
    getView,
    getWriteMapInfo,
    getInteraction,
    getMetadataList,
    getSelectedMetadata,
    unitToGeometryType,
    getWriteLayerId,
    getFeatureFromInput,
} from 'angled-core/queries/map';
import {
    viewEvents,
    scalelineEvents,
    startEditGeometry,
    selectMetadataForGeometryWrite,
    startSelectGeometry,
    startMark,
    endMark,
    removeWriteLayer,
    addWriteLayer,
} from 'angled-core/events/map';
import {
    GeometryType,
    Inspire,
    getMessageRecord,
    Feature,
} from 'sdi/source';
import tr, { fromRecord } from 'sdi/locale';

import { InformationUnitName } from 'angled-core/ui';
import { selectFeatureForUnit, addFeatureForUnit } from 'angled-core/events/ui';
import { getSelectedFeaturePath, getWriteUnit, getFormInput } from 'angled-core/queries/ui';
import { makeLabel } from 'angled-core/components/buttons';
import { closeModalGeometry } from 'angled-core/events/modal';
import { ModalRender } from 'angled-core/../sdi/components/modal';
import { geocoder } from '../geocoder';

const logger = debug('sdi:write/geometry');

const closeModalButton = makeLabel('close', 2, () => tr.core('close'));
const confirmSelectButton = makeLabel('close', 1, () => tr.core('confirm'));
const switchModeDrawButton = makeLabel('select', 2, () => tr.angled('drawMode'));
const switchModeSelectButton = makeLabel('select', 2, () => tr.angled('selectMode'));

const helptextCreateGeom =
    () => DIV({ className: 'helptext' }, tr.angled('helptext:closeGeom'));

const helptextSelectGeom =
    () => DIV({ className: 'helptext' }, tr.angled('helptext:selectGeom'));


const options: IMapOptions = {
    getView,
    element: null,
    getBaseLayer: getCurrentBaseLayer,
    getMapInfo: getWriteMapInfo,
    updateView: viewEvents.updateMapView,
    setScaleLine: scalelineEvents.setScaleLine,
};




const renderMD =
    (md: Inspire) => fromRecord(getMessageRecord(md.resourceTitle));



const { mapSetTarget, registerTarget } = (function () {
    const targeters: { [k: string]: (e: HTMLElement | null) => void } = {};

    const registerTarget =
        (name: string, t: (e: HTMLElement | null) => void) => {
            targeters[name] = t;
        };

    const mapSetTarget =
        (name: string, e: HTMLElement | null) =>
            fromNullable(targeters[name]).map(f => f(e));

    return { mapSetTarget, registerTarget };
})();

const { mapUpdate, mapInsert } = (function () {
    const updaters: { [k: string]: () => void } = {};

    const mapInsert = (name: string, u: () => () => void) => {
        if (!(name in updaters)) {
            updaters[name] = u();
        }
    };
    const mapUpdate = (name: string) => fromNullable(updaters[name]).map(f => f());
    return { mapInsert, mapUpdate };
})();


const mergeCoords = (
    f0: Feature,
    f1: Feature,
): Feature => {
    const g0 = f0.geometry;
    const g1 = f1.geometry;
    if (g0.type === 'MultiPoint' && g1.type === 'MultiPoint') {
        return { ...f0, geometry: { ...g0, coordinates: [...g0.coordinates, ...g1.coordinates] } }
    }
    else if (g0.type === 'MultiLineString' && g1.type === 'MultiLineString') {
        return { ...f0, geometry: { ...g0, coordinates: [...g0.coordinates, ...g1.coordinates] } }
    }
    else if (g0.type === 'MultiPolygon' && g1.type === 'MultiPolygon') {
        return { ...f0, geometry: { ...g0, coordinates: [...g0.coordinates, ...g1.coordinates] } }
    }
    return f0
}

const addFeature = (
    unit: InformationUnitName,
    fieldName: string,
    newFeature: Feature,
) => getFeatureFromInput(unit, fieldName)
    .foldL(
        () => addFeatureForUnit(unit, fieldName, updateWriteLayer(unit, fieldName))(newFeature),
        (feature) => {
            const add = addFeatureForUnit(unit, fieldName, updateWriteLayer(unit, fieldName))
            add(mergeCoords(newFeature, feature))
        }
    )

const updateWriteLayer = (
    unit: InformationUnitName,
    fieldName: string,
) => () => {
    removeWriteLayer(unit);
    addWriteLayer(unit, fieldName);
    // unitToGeometryType(unit).map(startModifyGeometry)
}


const attachMap =
    (unit: InformationUnitName, fieldName: string) =>
        (element: HTMLElement | null) => {
            mapInsert(unit, () => {
                const { update, setTarget, editable, selectable, markable } = create(unit, {
                    ...options,
                    element,
                });
                markable({ startMark, endMark }, getInteraction);
                selectable(singleSelectOptions({
                    selectFeature: selectFeatureForUnit(unit, fieldName), // , closeModal),
                    clearSelection: () => void 0,
                    getSelected: () => getSelectedFeaturePath().getOrElse({ layerId: null, featureId: null }),
                }), getInteraction);

                editable({
                    addFeature: (f) => addFeature(unit, fieldName, f),
                    setGeometry: () => void 0,
                    getCurrentLayerId: () => getWriteLayerId(unit),
                    getGeometryType: () => unitToGeometryType(unit).getOrElse('MultiPoint'),
                }, getInteraction);

                registerTarget(unit, setTarget);
                return update;
            });

            mapSetTarget(unit, element);
        };




const geometryTypeFilter =
    (gt: GeometryType) => (md: Inspire) => {
        const mdgt = md.geometryType;
        if (gt === 'Point' || gt === 'MultiPoint') {
            return (mdgt === 'Point' || mdgt === 'MultiPoint');
        }
        else if (gt === 'LineString' || gt === 'MultiLineString') {
            return (mdgt === 'LineString' || mdgt === 'MultiLineString');
        }
        else {
            return (mdgt === 'Polygon' || mdgt === 'MultiPolygon');
        }
    };

const renderSwitchMode =
    (unit: InformationUnitName) => {
        const interaction = getInteraction();
        const elems: ReactNode[] = [];
        if (interaction.label === 'select') {
            const gtf = geometryTypeFilter(unitToGeometryType(unit).getOrElse('Point')); // hackish
            const rs = renderSelect('select-layer', renderMD, selectMetadataForGeometryWrite(unit), setoidIdentified());
            const mdList = getMetadataList().filter(gtf);
            const selected = getSelectedMetadata();

            elems.push(rs(mdList, selected));
            elems.push(switchModeDrawButton(() => unitToGeometryType(unit).map(startEditGeometry)), helptextSelectGeom());
        }
        else if (interaction.label === 'create') {
            elems.push(switchModeSelectButton(startSelectGeometry), helptextCreateGeom());
        }

        return elems;
    };

const renderInput =
    (unit: InformationUnitName, fieldName: string) =>
        DIV({ className: 'map-wrapper-modal-content' },
            ...(renderSwitchMode(unit)),
            DIV({ className: 'map-wrapper field__value field--write' },
                DIV({
                    id: `angled-map-${unit}-${fieldName}`,
                    className: 'map',
                    key: `map-wrapper-field--write-geometry-${fieldName}`,
                    ref: attachMap(unit, fieldName),
                }),
                geocoder()),
        );



const renderHeaderModal =
    () => H2({}, tr.angled('startGeomDrawing'));


// const renderFooterModal =
//     () => DIV({},
//         getInteraction().label === 'select' ?
//             getSelectedFeaturePath()
//                 .map(
//                     () => confirmSelectButton(closeModalGeometry)) :
//             NODISPLAY(),
//         closeModalButton(closeModalGeometry));


const makeConfirm =
    () => {
        const it = getInteraction();
        if (it.label === 'select') {
            return getSelectedFeaturePath().map(() => confirmSelectButton(closeModalGeometry));
        }

        return getWriteUnit()
            .chain(([unit, fieldName]) => getFormInput(unit, fieldName))
            .map(_fi => confirmSelectButton(closeModalGeometry));
    };

const renderFooterModal =
    () => DIV({},
        // getSelectedFeaturePath().map(() => confirmSelectButton(closeModalGeometry)),
        makeConfirm(),
        closeModalButton(closeModalGeometry));



export const render: ModalRender = {
    header: renderHeaderModal,
    footer: renderFooterModal,
    body: () => getWriteUnit().map(
        ([unit, fieldName]) => {
            mapUpdate(unit);
            return renderInput(unit, fieldName);
        }),
};


logger('loaded');
