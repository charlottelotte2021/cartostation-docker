import * as debug from 'debug';
import { init } from 'sdi/components/modal';

const logger = debug('sdi:core/modal');

export const { render, register } = init();
export default render;

export { render as renderFK } from './fk';
export { render as renderGeometry } from './geometry';
export { render as renderTeam } from './team';


logger('loaded');
