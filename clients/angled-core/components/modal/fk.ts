import * as debug from 'debug';
import { fromNullable } from 'fp-ts/lib/Option';
import { ReactNode } from 'react';

import { DIV, NODISPLAY, H2 } from 'sdi/components/elements';
import { attrOptions, inputText } from 'sdi/components/input';
import tr from 'sdi/locale';
import { emptyModal, ModalRenderObject } from 'sdi/components/modal';

import { InformationUnitName } from 'angled-core/ui';

import {
    getNewFundingOrgFormInput,
    getNewSiteFormInput,
    getNewContactFormInput,
    getWriteUnit,
} from 'angled-core/queries/ui';
import {
    setNewFundingOrgFormInput,
    setNewSiteFormInput,
    setNewContactFormInput,
} from 'angled-core/events/ui';
import {} from 'angled-core/ui/ui';
import { makeLabelAndIcon, makeLabel } from 'angled-core/components/buttons';
import {
    createFundingOrg,
    createSite,
    createContact,
} from 'angled-core/events/ref';
import { AngledMessageKey } from 'angled-core/locale';
import { closeModalFK } from 'angled-core/events/modal';

const logger = debug('app/sdi:angled-core/ui/write/fk');

const addButtonModal = makeLabelAndIcon('add', 1, 'check', () =>
    tr.core('add')
);

const closeModalButton = makeLabel('close', 2, () => tr.core('close'));

const addButton = (fn: () => void) =>
    DIV(
        { className: 'modal__footer__inner' },
        closeModalButton(closeModalFK),
        addButtonModal(() => {
            closeModalFK();
            fn();
        })
    );

const addDataFunding = () =>
    fromNullable(getNewFundingOrgFormInput()['name']).map(r =>
        createFundingOrg(r)
    );

const addDataSite = () =>
    fromNullable(getNewSiteFormInput()['name']).map(r => createSite(r));

const addDataContact = (unit: InformationUnitName, fieldName: string) => () =>
    fromNullable(getNewContactFormInput()['name']).map(r =>
        createContact(r, unit, fieldName)
    );

// BUILDING A MODAL

const renderInnerModal = (
    title: AngledMessageKey,
    help: AngledMessageKey,
    body: () => ReactNode,
    addFn: () => void
): ModalRenderObject => ({
    header: () => renderHeaderModal(title),
    body: () => renderContentModal(help, body),
    footer: () => renderFooterModal(addFn),
});

const renderHeaderModal = (title: AngledMessageKey) => H2({}, tr.angled(title));

const renderContentModal = (help: AngledMessageKey, body: () => ReactNode) => [
    DIV({}, tr.angled(help)),
    body(),
];

const renderFooterModal = (addFn: () => void) => addButton(addFn);

const renderNewFunding = () =>
    DIV(
        { className: 'new-data new-data--funding', key: 'funding' },
        inputText(
            attrOptions(
                `input-new-funding`,
                () =>
                    fromNullable(getNewFundingOrgFormInput()['name']).getOrElse(
                        ''
                    ),
                i => setNewFundingOrgFormInput({ name: i }),
                { placeholder: tr.angled('fundingOrganisation') }
            )
        )
    );

const renderNewSite = () =>
    DIV(
        { className: 'new-data new-data--site', key: 'site' },
        DIV({ className: 'input-label' }, 'Français'),
        inputText(
            attrOptions(
                `input-new-site-fr`,
                () =>
                    fromNullable(
                        fromNullable(getNewSiteFormInput()['name']).getOrElse({
                            fr: '',
                            nl: '',
                        })['fr']
                    ).getOrElse(''),
                i =>
                    setNewSiteFormInput({
                        name: {
                            fr: i,
                            nl: fromNullable(
                                fromNullable(
                                    getNewSiteFormInput()['name']
                                ).getOrElse({ fr: '', nl: '' })['nl']
                            ).getOrElse(''),
                        },
                    }),
                { placeholder: tr.angled('site') }
            )
        ),
        DIV({ className: 'input-label' }, 'Nederlands'),
        inputText(
            attrOptions(
                `input-new-site-nl`,
                () =>
                    fromNullable(
                        fromNullable(getNewSiteFormInput()['name']).getOrElse({
                            fr: '',
                            nl: '',
                        })['nl']
                    ).getOrElse(''),
                i =>
                    setNewSiteFormInput({
                        name: {
                            nl: i,
                            fr: fromNullable(
                                fromNullable(
                                    getNewSiteFormInput()['name']
                                ).getOrElse({ fr: '', nl: '' })['fr']
                            ).getOrElse(''),
                        },
                    }),
                { placeholder: tr.angled('site') }
            )
        )
    );

const renderNewContact = () =>
    DIV(
        { className: 'new-data new-data--contact', key: 'contact' },
        inputText(
            attrOptions(
                `input-new-contact`,
                () =>
                    fromNullable(getNewContactFormInput()['name']).getOrElse(
                        ''
                    ),
                i => setNewContactFormInput({ name: i }),
                { placeholder: tr.angled('contact') }
            )
        )
    );

const renderNewFkList = (
    unit: InformationUnitName,
    fieldName: string
): ModalRenderObject => {
    switch (unit) {
        case 'funding':
            return renderInnerModal(
                'fundingOrganisation',
                'helptext:newFkEntry',
                renderNewFunding,
                addDataFunding
            );
        case 'site':
            return renderInnerModal(
                'site',
                'helptext:newFkEntry',
                renderNewSite,
                addDataSite
            );
        case 'tender_winner':
        case 'bidders':
            return renderInnerModal(
                'teamName',
                'helptext:newContactEntry',
                renderNewContact,
                addDataContact(unit, fieldName)
            );
        case 'actor':
            return renderInnerModal(
                'contact',
                'helptext:newFkEntry',
                renderNewContact,
                addDataContact(unit, fieldName)
            );
        default:
            return { header: NODISPLAY, footer: NODISPLAY, body: () => [] };
    }
};

export const render = () =>
    getWriteUnit().foldL(emptyModal, ([unit, fieldName]) =>
        renderNewFkList(unit, fieldName)
    );

logger('loaded');
