import { i, nullable, TypeOf, a, MessageRecordIO } from 'sdi/source/io/io';
import * as io from 'io-ts';
import { fromNullable } from 'fp-ts/lib/Option';
import { InformationUnitNameIO } from 'angled-core/ui';
import { PointIO } from 'sdi/source';

// tslint:disable-next-line:variable-name
export const KindIO = i(
    {
        id: io.number,
        name: MessageRecordIO,
    },
    'KindIO'
);

// tslint:disable-next-line:variable-name
export const DomainIO = i(
    {
        id: io.number,
        name: MessageRecordIO,
        kind: nullable(io.number),
        description: nullable(MessageRecordIO),
    },
    'DomainIO'
);

// tslint:disable-next-line:variable-name
export const DomainListIO = a(DomainIO);

// tslint:disable-next-line:variable-name
export const DomainMappingIO = i(
    {
        id: io.number,
        domain: io.Integer,
        unit: InformationUnitNameIO,
        field: io.string,
    },
    'DomainMappingIO'
);

// tslint:disable-next-line:variable-name
export const DomainMappingListIO = a(DomainMappingIO);

// tslint:disable-next-line:variable-name
export const TermIO = i({
    id: io.number,
    name: MessageRecordIO,
    description: nullable(MessageRecordIO),
    domain: io.number,
});

// tslint:disable-next-line:variable-name
export const TermListIO = a(TermIO);

export type Kind = TypeOf<typeof KindIO>;
export type Domain = TypeOf<typeof DomainIO>;
export type DomainList = TypeOf<typeof DomainListIO>;
export type DomainMapping = TypeOf<typeof DomainMappingIO>;
export type DomainMappingList = TypeOf<typeof DomainMappingListIO>;
export type Term = TypeOf<typeof TermIO>;
export type TermList = TypeOf<typeof TermListIO>;

// utils

export const termDescription = (t: Term) => fromNullable(t.description);
export const domainDescription = (d: Domain) => fromNullable(d.description);

// tslint:disable-next-line: variable-name
export const LocalityIO = io.interface(
    {
        id: io.Integer,
        city: MessageRecordIO,
        postcode: io.string,
        ins: io.string,
    },
    'LocalityIO'
);

// tslint:disable-next-line:variable-name
export const ContactIO = io.interface(
    {
        id: io.Integer,
        name: io.string,
        info: io.string,
    },
    'ContactIO'
);

// tslint:disable-next-line:variable-name
export const FundingOrgIO = io.interface(
    {
        id: io.Integer,
        name: io.string,
    },
    'FundingOrgIO'
);

// tslint:disable-next-line:variable-name
export const SiteIO = io.interface(
    {
        id: io.Integer,
        name: MessageRecordIO,
    },
    'SiteIO'
);

// tslint:disable-next-line: variable-name
export const TeamMemberRoleIO = io.union(
    [io.literal('MAIN'), io.literal('REGU')],
    'TeamMemberRoleIO'
);

// tslint:disable-next-line: variable-name
export const TeamMemberIO = io.interface(
    {
        id: io.Integer,
        member: io.Integer,
        role: TeamMemberRoleIO,
    },
    'TeamMemberIO'
);

// tslint:disable-next-line:variable-name
export const TeamIO = io.interface(
    {
        id: io.Integer,
        members: io.array(TeamMemberIO),
    },
    'TeamIO'
);

export const NovaDataIO = io.interface(
    {
        zipcode: nullable(io.string),
        catdossier: nullable(io.string),
        s_idaddress: nullable(io.number),
        s_iddossier: nullable(io.number),
        typedossier: nullable(io.string),
        coordinate_x: nullable(io.number),
        coordinate_y: nullable(io.number),
        streetnamefr: nullable(io.string),
        streetnamenl: nullable(io.string),
        municipalityownerfr: nullable(io.string),
        municipalityownernl: nullable(io.string),
    },
    'NovaDataIO'
);

// tslint:disable-next-line: variable-name
export const NovaIO = io.interface(
    {
        ref: io.string,
        time_of_fetch: io.number,
        data: NovaDataIO,
    },
    'NovaIO'
);

export const NovaLoaderIO = io.interface(
    {
        ref: nullable(io.string),
        status: io.union([
            io.literal('LOADED'),
            io.literal('LOADING'),
            io.literal('UNLOADED'), // at init
            io.literal('ERROR'),
        ]),
        data: nullable(NovaIO),
    },
    'NovaLoaderIO'
);

// tslint:disable-next-line: variable-name
export const TagIO = io.interface(
    {
        id: io.Integer,
        label: MessageRecordIO,
    },
    'TagIO'
);

// tslint:disable-next-line: variable-name
export const TaggedIO = io.interface(
    {
        id: io.Integer,
        unit: io.Integer,
        tag: io.Integer,
    },
    'TaggedIO'
);

// tslint:disable-next-line: variable-name
export const AudienceIO = io.interface(
    {
        id: io.Integer,
        name: MessageRecordIO,
        description: MessageRecordIO,
        group: io.Integer,
    },
    'AudienceIO'
);

// tslint:disable-next-line: variable-name
export const ReadingIO = io.interface(
    {
        id: io.Integer,
        audience: io.Integer,
        unit: io.Integer,
    },
    'ReadingIO'
);

export type Locality = TypeOf<typeof LocalityIO>;
export type Contact = TypeOf<typeof ContactIO>;
export type FundingOrg = TypeOf<typeof FundingOrgIO>;
export type Site = TypeOf<typeof SiteIO>;
export type TeamMemberRole = TypeOf<typeof TeamMemberRoleIO>;
export type TeamMember = TypeOf<typeof TeamMemberIO>;
export type Team = TypeOf<typeof TeamIO>;
export type NovaData = TypeOf<typeof NovaDataIO>;
export type Nova = TypeOf<typeof NovaIO>;
export type NovaLoader = TypeOf<typeof NovaLoaderIO>;
export type Tag = TypeOf<typeof TagIO>;
export type Tagged = TypeOf<typeof TaggedIO>;
export type Audience = TypeOf<typeof AudienceIO>;
export type Reading = TypeOf<typeof ReadingIO>;

export type TeamPartialTeamMembers = {
    id: number;
    members: Omit<TeamMember, 'id'>[];
};

// tslint:disable-next-line: variable-name
export const LocalityListIO = a(LocalityIO);

// tslint:disable-next-line: variable-name
export const ContactListIO = a(ContactIO);

// tslint:disable-next-line: variable-name
export const FundingOrgListIO = a(FundingOrgIO);

// tslint:disable-next-line: variable-name
export const SiteListIO = a(SiteIO);

// tslint:disable-next-line: variable-name
export const TeamMemberListIO = a(TeamMemberIO);

// tslint:disable-next-line: variable-name
export const TeamListIO = a(TeamIO);

// tslint:disable-next-line: variable-name
export const NovaListIO = a(NovaIO);

// tslint:disable-next-line: variable-name
export const TagListIO = a(TagIO);

// tslint:disable-next-line: variable-name
export const TaggedListIO = a(TaggedIO);

// tslint:disable-next-line: variable-name
export const AudienceListIO = a(AudienceIO);

export type LocalityList = TypeOf<typeof LocalityListIO>;
export type ContactList = TypeOf<typeof ContactListIO>;
export type FundingOrgList = TypeOf<typeof FundingOrgListIO>;
export type SiteList = TypeOf<typeof SiteListIO>;
export type TeamMemberList = TypeOf<typeof TeamMemberListIO>;
export type TeamList = TypeOf<typeof TeamListIO>;
export type NovaList = TypeOf<typeof NovaListIO>;
export type TagList = TypeOf<typeof TagListIO>;
export type TaggedList = TypeOf<typeof TaggedListIO>;
export type AudienceList = TypeOf<typeof AudienceListIO>;

// tslint:disable-next-line: variable-name
export const SchoolSiteIO = io.interface(
    {
        gid: io.Integer,
        name: nullable(io.string),
        lang: nullable(io.string),
        street: nullable(io.string),
        kind: nullable(io.string),
        level: nullable(io.string),
        organizer: nullable(io.string),
        reseau: nullable(io.string),
        general: nullable(io.number),
        technique: nullable(io.number),
        profession: nullable(io.number),
        artistique: nullable(io.number),
        aso: nullable(io.number),
        tso: nullable(io.number),
        bso: nullable(io.number),
        kso: nullable(io.number),
        type1: nullable(io.number),
        type2: nullable(io.number),
        type3: nullable(io.number),
        type4: nullable(io.number),
        type5: nullable(io.number),
        type6: nullable(io.number),
        type7: nullable(io.number),
        type8: nullable(io.number),
        type9: nullable(io.number),
        form1: nullable(io.number),
        form2: nullable(io.number),
        form3: nullable(io.number),
        form4: nullable(io.number),
        clw: nullable(io.number),
        cefa: nullable(io.number),
        cta: nullable(io.string),
        rue: nullable(io.string),
        locality: nullable(io.string),
        postcode: nullable(io.string),
        point: nullable(PointIO),
    },
    'SchoolSiteIO'
);

export type SchoolSite = TypeOf<typeof SchoolSiteIO>;
