import { FormEvent } from 'react';
import { DIV, INPUT, IMG } from 'sdi/components/elements';

import { InformationUnitName } from 'angled-core/ui';
// import { toDataURL } from '../../../compose/src/events/app'; // This function was copied here but we'd better import it from sdi/components

import { getCurrentImage } from 'angled-core/queries/ui';
import { setFormInput, setImageFile, setImage } from 'angled-core/events/ui';
import { tr } from 'sdi/locale';
import { fieldDisplayName } from 'angled-core/queries/app';


const toDataURL = (f: File) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => {
            const result = reader.result;
            if (result) {
                resolve(result);
            }
            else {
                reject();
            }
        };
        reader.onerror = reject;
        reader.onabort = reject;
        reader.readAsDataURL(f);
    });
};

const setThumbnailImg =
    (file: File) => {
        toDataURL(file)
            .then(
                (r: string) => {
                    setImage(r);
                },
            )
            .catch(
                (r: string) => {
                    console.log(`Error transposing File to Data url ${r}`);
                },
            );
    };

export const renderThumbnail =
    () =>
        DIV({ className: 'field--image__thumbnail' },
            IMG({ className: 'field--image__thumbnail__img', src: getCurrentImage() }),
        );

const inputImage =
    (unit: InformationUnitName, fieldName: string) =>
        INPUT({
            type: 'file',
            name: 'field_input_image',
            className: 'field--image__input',
            onChange: (e: FormEvent<HTMLInputElement>) => {
                if (e && e.currentTarget.files && e.currentTarget.files.length > 0) {
                    const url = '/documents/images/';
                    setFormInput(unit, fieldName)(url);
                    setImageFile(e.currentTarget.files[0]);
                    setThumbnailImg(e.currentTarget.files[0]);
                }
                else {
                    setFormInput(unit, fieldName)('');
                }
            },
        });


const renderInput =
    (unit: InformationUnitName, fieldName: string) =>
        DIV({ className: 'field__value field--write' },
            inputImage(unit, fieldName),
        );


const renderName =
    (unitName: InformationUnitName, fieldName: string) =>
        DIV({ className: 'field__key field--write' }, fieldDisplayName(unitName, fieldName));

const disclaimer =
    () =>
        DIV({ className: 'helptext' }, tr.core('imageRightDisclaimer'));


export const render =
    (unit: InformationUnitName, fieldName: string) =>
        DIV({ className: `field field--${fieldName} field--write` },
            renderName(unit, fieldName),
            renderThumbnail(),
            renderInput(unit, fieldName),
            disclaimer(),
        );

export default render;
