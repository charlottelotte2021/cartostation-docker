import * as debug from 'debug';
import { Setoid } from 'fp-ts/lib/Setoid';
import { ReactNode } from 'react';
import { Option } from 'fp-ts/lib/Option';

import { DIV, NODISPLAY } from 'sdi/components/elements';
import { renderSelect, renderSelectFilter } from 'sdi/components/input';
import tr, { fromRecord } from 'sdi/locale';

import { Contact, Site, FundingOrg, Team } from 'angled-core/ref';
import { IUser } from 'sdi/source';
import {
    InformationUnitName,
    Project,
    makeValueMapper,
    getUnitbyName,
} from 'angled-core/ui';
import {
    getContactList,
    getSiteList,
    getFundingOrgList,
    getUserList,
    getTeamList,
    findContact,
    findSite,
    findFundingOrg,
    findUser,
    findTeam,
} from 'angled-core/queries/ref';
import { getFormInput, getFormInputTeamMembers } from 'angled-core/queries/ui';
import { setFormInput, setWriteUnit } from 'angled-core/events/ui';
import { makeLabelAndIcon, makeLabel } from 'angled-core/components/buttons';
import { fieldDisplayName } from 'angled-core/queries/app';
import { openModalFK, openModalTeam } from 'angled-core/events/modal';
import { catOptions } from 'fp-ts/lib/Array';

const logger = debug('app/sdi:angled-core/ui/write/fk');

const addEntry = makeLabelAndIcon('add', 2, 'plus', () =>
    tr.angled('addFkEntry')
);

const renderName = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__key field--write' },
        fieldDisplayName(unit, fieldName)
    );

const fieldMapper = makeValueMapper('fk', (a: number) => a, -1);

// const fieldMapperString = makeValueMapper('fk', (a: string) => parseInt(a, 10), -1);

const getCurrentFk = (unit: InformationUnitName, fieldName: string) =>
    getFormInput(unit, fieldName).map(fieldMapper);

// const getCurrentFkString =
//     (unit: InformationUnitName, fieldName: string) =>
//         getFormInput(unit, fieldName)
//             .map(fieldMapperString);

// const getCurrentMultiFk =
//     (_unit: InformationUnitName, _fieldName: string) => {
//         const currentTeamMembers: Contact[] = [];
//         getFormInputTeamMembers().map(tm => currentTeamMembers.push(tm));
//         return currentTeamMembers;
//     };

const setoidContact: Setoid<Contact> = {
    equals: (a: Contact, b: Contact) => a.id === b.id,
};

const setoidTeam: Setoid<Team> = {
    equals: (a: Team, b: Team) => a.id === b.id,
};

const setoidSite: Setoid<Site> = {
    equals: (a: Site, b: Site) => a.id === b.id,
};

const setoidFunding: Setoid<FundingOrg> = {
    equals: (a: FundingOrg, b: FundingOrg) => a.id === b.id,
};

const setoidUser: Setoid<IUser> = {
    equals: (a: IUser, b: IUser) => a.id === b.id,
};

const renderContact = (c: Contact) =>
    DIV(
        {
            className: '_fk__item',
            key: `contact-input-${c.id}`,
        },
        c.name
    );

const renderSite = (s: Site) => fromRecord(s.name);
// DIV({
//     className: '_fk__item',
//     key: `site-input-${s.id}`,
// }, fromRecord(s.name),
// );

const renderTeam = (t: Team) =>
    t.members.map(m => findContact(m.member).fold('', c => c.name)).join(', ');
// DIV({
//     className: '_fk__item',
//     key: `team-input-${t.id}`,
// }, t.members.map(m => findContact(m.member).fold('', c => c.name)).join(', '),
// );

// const renderFunding =
//     (f: FundingOrg) =>
//         DIV({
//             className: '_fk__item',
//             key: `funding-input-${f.id}`,
//         }, f.name,
//         );

const renderUser = (u: IUser) =>
    DIV(
        {
            className: '_fk__item',
            key: `user-input-${u.id}`,
        },
        u.name
    );

const selectContactRenderer = (unit: InformationUnitName, fieldName: string) =>
    renderSelectFilter(
        setoidContact,
        renderContact,
        d => {
            setFormInput(unit, fieldName)(d.id);
            console.log(
                `test select: UNIT: ${unit}, FIELDN: ${fieldName}, ID:${d.id}`
            );
        },
        c => c.name
    );

const selectSiteRenderer = (unit: InformationUnitName, fieldName: string) =>
    renderSelect(
        'select-site',
        renderSite,
        d => setFormInput(unit, fieldName)(d.id),
        setoidSite
    );

const selectFundingRenderer = (unit: InformationUnitName, fieldName: string) =>
    renderSelect(
        'select-funding',
        f => f.name,
        d => setFormInput(unit, fieldName)(d.id),
        setoidFunding
    );

const getBiddersList = (p: Project): Team[] => {
    const bidder_unit = getUnitbyName('bidders', p).getOrElse([]);

    if (Array.isArray(bidder_unit)) {
        return catOptions(
            bidder_unit
                .filter(t => 'bidders_team' in t)
                .map<number>(t => (t as any).bidders_team) // I don't like it either - pm
                .map(tid => findTeam(tid))
        );
    } else {
        return 'bidders_team' in bidder_unit
            ? findTeam(bidder_unit.bidders_team).fold([], t => [t])
            : [];
    }
};

const selectTeamRenderer = (unit: InformationUnitName, fieldName: string) => {
    return renderSelect<Team>(
        'select-team',
        renderTeam,
        d => setFormInput(unit, fieldName)(d.id),
        setoidTeam
    );
};

const selectUserRenderer = (unit: InformationUnitName, fieldName: string) =>
    renderSelectFilter(
        setoidUser,
        renderUser,
        d => setFormInput(unit, fieldName)(parseInt(d.id, 10)),
        u => u.name
    );

const renderInputTeamCurrent = () =>
    getFormInputTeamMembers().map(([contact, _role]) =>
        DIV(
            {
                key: `input-contact-${contact.id}`,
            },
            contact.name
        )
    );

const renderFkList = (
    unit: InformationUnitName,
    fieldName: string,
    project: Option<Project>
) => {
    switch (unit) {
        case 'funding':
            return selectFundingRenderer(unit, fieldName)(
                getFundingOrgList(),
                getCurrentFk(unit, fieldName).chain(findFundingOrg)
            );
        case 'site':
            return selectSiteRenderer(unit, fieldName)(
                getSiteList(),
                getCurrentFk(unit, fieldName).chain(findSite)
            );
        case 'bidders':
            return renderInputTeamCurrent();
        case 'tender_winner':
            return selectTeamRenderer(unit, fieldName)(
                project.fold(getTeamList(), p => getBiddersList(p)),
                getCurrentFk(unit, fieldName).chain(findTeam)
            );
        case 'actor':
            return selectContactRenderer(unit, fieldName)(
                getContactList(),
                getCurrentFk(unit, fieldName).chain(findContact)
            );
        case 'manager':
            return selectUserRenderer(unit, fieldName)(
                getUserList(),
                // parseInt(getCurrentFk(unit, fieldName).fold(0, n => n),10).chain(findUser),
                getCurrentFk(unit, fieldName).chain(findUser)
            );
        default:
            return NODISPLAY();
    }
};

const wrapFKSelect = (
    select: ReactNode,
    unit: InformationUnitName,
    fieldName: string
) => {
    const children: ReactNode[] = [];

    switch (unit) {
        case 'bidders':
            if (getFormInputTeamMembers().length > 0) {
                children.push(
                    makeLabel('edit', 2, () => tr.angled('reconfigureTeam'))(
                        openModalTeam
                    )
                );
            } else {
                children.push(
                    makeLabel('add', 2, () => tr.angled('createTeam'))(
                        openModalTeam
                    )
                );
            }
            break;
        case 'tender_winner':
            break;
        case 'manager':
            break;
        default:
            children.push(
                addEntry(() => {
                    setWriteUnit(unit, fieldName);
                    openModalFK();
                })
            );
    }

    return DIV({}, select, ...children);
};

export const render = (
    unit: InformationUnitName,
    fieldName: string,
    projOpt: Option<Project>
) => {
    return DIV(
        { className: `field field--${fieldName} field--write` },
        renderName(unit, fieldName),
        wrapFKSelect(renderFkList(unit, fieldName, projOpt), unit, fieldName)
    );
};

export default render;

logger('Loaded !');
