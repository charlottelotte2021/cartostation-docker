import { InformationUnitName } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { inputText } from 'sdi/components/input';
import { getNovaRefFormInput, getNovaDataFormInput } from 'angled-core/queries/ui'; // todo renommer NewNova en NovaRef ?
import { setNovaRefFormInput, setFormInput, setNovaDataFormInput } from 'angled-core/events/ui';
import tr from 'sdi/locale';
import { makeLabel } from 'angled-core/components/buttons';
import { fromNullable } from 'fp-ts/lib/Option';
import { loadNovaAndExecuteCallback } from 'angled-core/events/ref';
import debug = require('debug');
import { renderDataNova } from 'angled-core/ui/read/nova';

const logger = debug('sdi:write/nova');

const loadButton = makeLabel('load', 2, () => tr.core('load')); // to do rename button

// type Self = string;

// const fieldMapper = makeValueMapper('varchar', (a: Self) => a, '');

const renderName =
    (fieldName: string) =>
        DIV({ className: 'field__key field--write' }, fieldName);

const renderInput =
    () =>
        DIV({ className: 'field__value field--write' },
            inputText(
                () => fromNullable(getNovaRefFormInput()).getOrElse(''), // passer param
                (v: string) => setNovaRefFormInput(v),
            ));

const renderNovaDataFromInput =
    () =>
        renderDataNova(fromNullable(getNovaDataFormInput()));


export const render =
    (_unit: InformationUnitName, fieldName: string) => {
        return DIV({ className: `field field--${fieldName} field--write` },
            renderName(fieldName),
            renderInput(),
            loadButton(() => {
                const ref_nova = fromNullable(getNovaRefFormInput()).getOrElse('');
                loadNovaAndExecuteCallback(
                    ref_nova,
                    (nova => {
                        setNovaDataFormInput({ 'ref': ref_nova, status: 'LOADED', data: nova });
                        setFormInput('nova', 'nova')(nova.ref)
                    }),
                    () => {
                        setNovaDataFormInput({ 'ref': ref_nova, status: 'ERROR', data: null });
                    });
            }), // todo check if not empty fromNullable -> fold
            renderNovaDataFromInput() // display if there is data in component/ui/form/nova-data
        );
    };

export default render;

logger('LOADED');