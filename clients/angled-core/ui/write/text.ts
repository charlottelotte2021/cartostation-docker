import { InformationUnitName, makeValueMapper } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { inputLongText } from 'sdi/components/input';
import { getFormInput } from 'angled-core/queries/ui';
import { setFormInput } from 'angled-core/events/ui';
import { MessageRecord, MessageRecordLang } from 'sdi/source';
import { rec, EmptyString } from 'sdi/locale';
import { fieldDisplayName } from 'angled-core/queries/app';

type Self = MessageRecord;

const fieldMapper = makeValueMapper('text', (a: Self) => a, { fr: '', nl: '' });

const renderName = (unitName: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__key field--write' },
        fieldDisplayName(unitName, fieldName)
    );

const getRecord = (unit: InformationUnitName, fieldName: string) => // todo fixme meme fonction que label.ts -> a mettre ailleurs ?
    getFormInput(unit, fieldName).map(fieldMapper);

const getValue = (
    unit: InformationUnitName,
    fieldName: string,
    l: MessageRecordLang
) => getRecord(unit, fieldName).fold(EmptyString, record => rec(record, l, ''));

const renderInput = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__value field--write' },
        DIV({ className: 'input-label' }, 'Français'),
        inputLongText(
            () => getValue(unit, fieldName, 'fr'),
            i =>
                setFormInput(
                    unit,
                    fieldName
                )({ fr: i, nl: getValue(unit, fieldName, 'nl') }),
            {}
        ),
        DIV({ className: 'input-label' }, 'Nederlands'),
        inputLongText(
            () => getValue(unit, fieldName, 'nl'),
            i =>
                setFormInput(
                    unit,
                    fieldName
                )({ nl: i, fr: getValue(unit, fieldName, 'fr') }),
            {}
        )
    );

export const render = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: `field field--${fieldName} field--write` },
        renderName(unit, fieldName),
        renderInput(unit, fieldName)
    );

export default render;
