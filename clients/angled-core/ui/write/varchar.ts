import { InformationUnitName, makeValueMapper } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { inputText, options } from 'sdi/components/input';
import { getFormInput } from 'angled-core/queries/ui';
import { setFormInput } from 'angled-core/events/ui';
import renderImage from './image';
import renderDocument from './document';
// import renderNova from './nova';
import { fieldDisplayName } from 'angled-core/queries/app';

type Self = string;

const fieldMapper = makeValueMapper('varchar', (a: Self) => a, '');

const renderName = (unitName: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__key field--write' },
        fieldDisplayName(unitName, fieldName)
    );

const renderInput = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__value field--write' },
        inputText(
            options(
                `input-varchar-${unit}-${fieldName}`,
                () => getFormInput(unit, fieldName).fold('', fieldMapper),
                v => setFormInput(unit, fieldName)(v)
            )
        )
    );

export const render = (unit: InformationUnitName, fieldName: string) => {
    switch (unit) {
        case 'image':
            return renderImage(unit, fieldName);
        case 'document':
            return renderDocument(unit, fieldName);
        // case 'nova': return renderNova(unit, fieldName);
        default:
            return DIV(
                { className: `field field--${fieldName} field--write` },
                renderName(unit, fieldName),
                renderInput(unit, fieldName)
            );
    }
};

export default render;
