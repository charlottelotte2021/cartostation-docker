import { InformationUnitName, makeValueMapper } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { inputYear } from 'sdi/components/input';
import { getFormInput } from 'angled-core/queries/ui';
import { setFormInput } from 'angled-core/events/ui';
import { fieldDisplayName } from 'angled-core/queries/app';

type Self = number;

const fieldMapper = makeValueMapper('year', (y: Self) => y, NaN);

const renderName = (unitName: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__key field--write' },
        fieldDisplayName(unitName, fieldName)
    );

const setYearInput =
    (unit: InformationUnitName, fieldName: string) => (y: string) => {
        const y_int = parseInt(y, 10);

        if (y_int > 0 && y_int < 10000) {
            setFormInput(unit, fieldName)(y_int);
        }
    };

const renderInput = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__value field--write' },
        inputYear({
            key: `input-year-${unit}-${fieldName}`,
            get: () =>
                getFormInput(unit, fieldName).fold(NaN, fieldMapper).toString(),
            set: setYearInput(unit, fieldName),
            attrs: {
                placeholder: 'YYYY',
            },
            monitor: setYearInput(unit, fieldName),
        })
    );

export const render = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: `field field--${fieldName} field--write` },
        renderName(unit, fieldName),
        renderInput(unit, fieldName)
    );

export default render;
