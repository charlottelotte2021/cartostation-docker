import * as debug from 'debug';

import { InformationUnitName, makeValueMapper } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { attrOptions, inputText } from 'sdi/components/input';
import { getFormInput } from 'angled-core/queries/ui';
import { setFormInput } from 'angled-core/events/ui';
import { fieldDisplayName } from 'angled-core/queries/app';

const logger = debug('sdi:angled/components/write/date');

type Self = string;

const fieldMapper = makeValueMapper(
    'date',
    (a: Self) => new Date(a).toISOString().split('T')[0], // date en 'yyyy-mm-dd' pour le input field
    new Date().toISOString().split('T')[0]
);

const renderName = (unitName: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__key field--write' },
        fieldDisplayName(unitName, fieldName)
    );

// const tmsp2yyymmddDate =
//     // Date in format yyyy-mm-dd from a timestamp
//     (timestamp: number) => new Date(timestamp).toISOString().split('T')[0];

// const yyyymmddDate2Tmsp =
//     (date: string) => isNaN(Date.parse(date)) ? Date.now() : Date.parse(date)

const renderInput = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__value field--write' },
        inputText(
            attrOptions(
                `input-date-${unit}-${fieldName}`,
                () =>
                    getFormInput(unit, fieldName)
                        .map(fieldMapper)
                        .getOrElse(new Date().toISOString().split('T')[0]),
                d => setFormInput(unit, fieldName)(d),
                { type: 'date' }
            )
        )
    );

// const renderInput =
//     (unit: InformationUnitName, fieldName: string) =>
//         DIV({ className: 'field__value field--write' },
//             inputText(
//                 () => getFormInput(unit, fieldName)
//                     .fold(tmsp2yyymmddDate(Date.now()), fieldMapper),
//                 (d) => setFormInput(unit, fieldName)(yyyymmddDate2Tmsp(d)),
//                 { type: 'date' },
//             ));

export const render = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: `field field--${fieldName} field--write` },
        renderName(unit, fieldName),
        renderInput(unit, fieldName)
    );

export default render;

logger('Loaded!');
