import { InformationUnitName, makeValueMapper } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { inputLongText } from 'sdi/components/input';
import { getFormInput } from 'angled-core/queries/ui';
import { setFormInput } from 'angled-core/events/ui';
import { fieldDisplayName } from 'angled-core/queries/app';

type Self = string;

const fieldMapper = makeValueMapper('raw_text', (a: Self) => a, '');

const renderName =
    (unitName: InformationUnitName, fieldName: string) =>
        DIV({ className: 'field__key field--write' }, fieldDisplayName(unitName, fieldName));

const renderInput =
    (unit: InformationUnitName, fieldName: string) =>
        DIV({ className: 'field__value field--write' },
            inputLongText(
                () => getFormInput(unit, fieldName)
                    .fold('', fieldMapper),
                setFormInput(unit, fieldName),
                { rows: 5 },
            ));

export const render =
    (unit: InformationUnitName, fieldName: string) =>
        DIV({ className: `field field--${fieldName} field--write` },
            renderName(unit, fieldName),
            renderInput(unit, fieldName),
        );

export default render;
