import * as debug from 'debug';
import { Option, none, some } from 'fp-ts/lib/Option';

import { DIV } from 'sdi/components/elements';

import { viewEvents, startEditGeometry, clearMetadataForGeometryWrite } from 'angled-core/events/map';
import {
    GeometryType,
} from 'sdi/source';
import tr from 'sdi/locale';

import { InformationUnitName } from 'angled-core/ui';
import { clearSelectedGeometry, ensureMultiGeometry, setWriteUnit, resetFormInput } from 'angled-core/events/ui';
import { getSelectedFeature } from 'angled-core/queries/ui';
import { makeLabel } from 'angled-core/components/buttons';
import { defaultMapEvent } from 'angled-core/shape/map';
import { fieldDisplayName } from 'angled-core/queries/app';
import { renderValue } from 'angled-core/ui/read/geometry';
import { openModalGeometry } from 'angled-core/events/modal';

const logger = debug('sdi:write/geometry');

const openModalButton = makeLabel('add', 2, () => tr.angled('startGeomDrawing'));



const unitToGeometryType =
    (u: InformationUnitName): Option<GeometryType> => {
        switch (u) {
            case 'point': return some('MultiPoint');
            case 'line': return some('MultiLineString');
            case 'polygon': return some('MultiPolygon');
            default: return none;
        }
    };


const renderName =
    (unitName: InformationUnitName, fieldName: string) =>
        DIV({ className: 'field__key field--write' }, fieldDisplayName(unitName, fieldName));



export const render =
    (unit: InformationUnitName, fieldName: string) => {

        const button = openModalButton(() => {
            setWriteUnit(unit, fieldName);
            resetFormInput();
            openModalGeometry();
            unitToGeometryType(unit).map(startEditGeometry);
            clearSelectedGeometry();
            clearMetadataForGeometryWrite(unit);
            viewEvents.updateMapView(defaultMapEvent());
        });

        const modalTrigger = getSelectedFeature().fold(
            button,
            feature => DIV({},
                renderValue(fieldName, ensureMultiGeometry(feature.geometry)),
                button));



        return (
            DIV({ className: `field field--${fieldName} field--write` },
                renderName(unit, fieldName),
                DIV({ className: 'new-data new-data--geom' }, modalTrigger),
            ));
    };

export default render;

logger('loaded');
