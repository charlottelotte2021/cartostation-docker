import { UnitFieldProcessor, fields, UnitField, InformationUnitName, Unit, Project, typeNameToName, isMulti } from './ui';
import {
    MessageRecord,
    MultiPoint,
    MultiLineString,
    MultiPolygon,
    MultiPointIO,
    MultiLineStringIO,
    MultiPolygonIO,
    GeometryType,
} from 'sdi/source';
import { fromNullable } from 'fp-ts/lib/Option';
import * as io from 'io-ts';
import { MessageRecordIO } from 'angled-core/../sdi/source/io/io';

export * from './ui';


export type GeometryUnit = 'point' | 'line' | 'polygon';

export const geometryTypeForUnit =
    (gu: GeometryUnit): GeometryType => {
        switch (gu) {
            case 'point': return 'MultiPoint';
            case 'line': return 'MultiLineString';
            case 'polygon': return 'MultiPolygon';
        }
    };


export type FieldType = keyof UnitFieldProcessor<never>;
export type FieldProcessor<T> = (fieldType: FieldType, name: string, extra?: (string | string[] | number)) => T;

const fieldsGenerator =
    <T>(fieldProcessor: FieldProcessor<T>): UnitFieldProcessor<T> => ({
        fk: (n, e) => fieldProcessor('fk', n, e),
        term: (n, e) => fieldProcessor('term', n, e),
        label: n => fieldProcessor('label', n),
        text: n => fieldProcessor('text', n),
        raw_text: n => fieldProcessor('raw_text', n),
        number: n => fieldProcessor('number', n),
        decimal: n => fieldProcessor('decimal', n),
        month: n => fieldProcessor('month', n),
        year: n => fieldProcessor('year', n),
        date: n => fieldProcessor('date', n),
        boolean: n => fieldProcessor('boolean', n),
        varchar: (n, e) => fieldProcessor('varchar', n, e),
        geometry: n => fieldProcessor('geometry', n),
    });

export const mapFields =
    <T>(fieldProcessor: FieldProcessor<T>) =>
        fields(fieldsGenerator(fieldProcessor));

// tslint:disable-next-line: variable-name
export const FieldValueTypeIO = io.union([
    io.number,
    io.string,
    io.boolean,
    io.array(io.number),
    MessageRecordIO,
    MultiPointIO,
    MultiLineStringIO,
    MultiPolygonIO,
]);

export type FieldValueType = io.TypeOf<typeof FieldValueTypeIO>;

export interface FieldMapper<T> {
    text(a: MessageRecord): T;
    label(a: MessageRecord): T;
    raw_text(a: string): T;
    boolean(a: boolean): T;
    number(a: number): T;
    term(a: number): T;
    decimal(a: number): T;
    year(a: number): T;
    date(a: string): T;
    geometry(a: MultiPoint | MultiLineString | MultiPolygon): T;
    month(a: string): T;
    fk(a: number): T;
    varchar(a: string): T;
}

export const mapValueField =
    <T>(m: FieldMapper<T>) => (field: UnitField) => {
        switch (field.kind) {
            case 'boolean': return m.boolean(field.value);
            case 'date': return m.date(field.value);
            case 'decimal': return m.decimal(field.value);
            case 'fk': return m.fk(field.value);
            case 'geometry': return m.geometry(field.value);
            case 'label': return m.label(field.value);
            case 'month': return m.month(field.value);
            case 'number': return m.number(field.value);
            case 'raw_text': return m.raw_text(field.value);
            case 'term': return m.term(field.value);
            case 'text': return m.text(field.value);
            case 'varchar': return m.varchar(field.value);
            case 'year': return m.year(field.value);
        }
    };

export const makeValueMapper =
    <T, K extends keyof FieldMapper<T>>(k: K, f: Pick<FieldMapper<T>, K>[K], dflt: T) => {
        return mapValueField<T>(Object.assign({
            text: (_a: MessageRecord) => dflt,
            label: (_a: MessageRecord) => dflt,
            raw_text: (_a: string) => dflt,
            boolean: (_a: boolean) => dflt,
            number: (_a: number) => dflt,
            term: (_a: number) => dflt,
            decimal: (_a: number) => dflt,
            year: (_a: number) => dflt,
            date: (_a: string) => dflt,
            geometry: (_a: MultiPoint | MultiLineString | MultiPolygon) => dflt,
            month: (_a: string) => dflt,
            fk: (_a: number) => dflt,
            varchar: (_a: string) => dflt,
        }, { [k]: f }));
    };



export type MappedField = [FieldType, string, string | number | string[] | undefined];
const namedFields = mapFields<MappedField>((fieldType, name, extra) => [fieldType, name, extra]);
export const mapFieldType = (f: MappedField) => f[0];
export const mapFieldName = (f: MappedField) => f[1];
export const mapFieldExtra = (f: MappedField) => f[2];

export const lookupNamedField =
    (name: InformationUnitName) =>
        fromNullable(namedFields.find(f => f.name === name));

export const lookupNamedFieldType =
    (name: InformationUnitName, fieldName: string) =>
        lookupNamedField(name)
            .chain(({ fields }) =>
                fromNullable(fields.find(f =>
                    fieldName === mapFieldName(f))))
            .map(mapFieldType);


const fieldUnits: [InformationUnitName, string, string][] = [
    ['cost', 'cost', ' €'],
    ['funding', 'amount', ' €'],
    ['area', 'value', ' m²'],
];

export const getFieldUnit =
    (unit: InformationUnitName, fieldName: string) =>
        fromNullable(fieldUnits.find(([uname, fname]) => unit === uname && fieldName === fname))
            .map(fu => fu[2]);



export const updateProject =
    (unitData: Unit) => (project: Project): Project => {
        const key = typeNameToName[unitData.unit];
        const multi = isMulti(key);
        if (multi) {
            let value = project[key] as (typeof unitData)[] | undefined;
            if (value !== undefined) {
                value = value.filter(u => u.id !== unitData.id).concat(unitData);
            }
            else {
                value = [unitData];
            }
            const np = {
                ...project,
                [key]: value,
            };
            return np;
        }
        const np = {
            ...project,
            [key]: unitData,
        };
        return np;
    };
