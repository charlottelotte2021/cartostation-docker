import { UnitData, typeNameToName } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { MessageRecord } from 'sdi/source';
import { fromRecord } from 'sdi/locale';
import { fieldDisplayName } from 'angled-core/queries/app';
import { WidgetFilterOpt } from 'angled-core/profile';

type Self = MessageRecord;


const renderValue =
    (value: Self) =>
        DIV({ className: 'field__value field--read' }, fromRecord(value));

export const render = (
    u: UnitData,
    fieldName: string,
    value: Self,
    _filter: WidgetFilterOpt
) =>
    DIV({
        className: `field field--${fieldName} field--read`,
        title: fieldDisplayName(typeNameToName[u.unit], fieldName)
    },
        renderValue(value),
    );

export default render;
