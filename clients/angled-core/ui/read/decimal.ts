import { InformationUnitName, UnitData, getFieldUnit, typeNameToName } from 'angled-core/ui';
import { DIV, SPAN } from 'sdi/components/elements';
import { formatNumber } from 'sdi/locale';
import { fieldDisplayName } from 'angled-core/queries/app';
import { WidgetFilterOpt } from 'angled-core/profile';

type Self = number;


const renderValue =
    (unit: InformationUnitName, fieldName: string, value: Self) =>
        DIV({ className: 'field__value field--read' },
            formatNumber(value),
            getFieldUnit(unit, fieldName).map(u => SPAN({ className: '~unit' }, u)),
        );


// const renderUnit =
//     (unit: InformationUnitName, fieldName: string) => {
//         if (unit === 'area'
//             || unit === 'cost'
//             || unit === 'funding') {

//         }
//     }

export const render = (
    u: UnitData,
    fieldName: string,
    value: Self,
    _filter: WidgetFilterOpt
) =>
    DIV({
        className: `field field--${fieldName} field--read`,
        title: fieldDisplayName(typeNameToName[u.unit], fieldName),
    },
        renderValue(typeNameToName[u.unit], fieldName, value),
    );

export default render;
