import * as debug from 'debug';

import { UnitData, typeNameToName } from 'angled-core/ui';
import { DIV, IMG, NODISPLAY } from 'sdi/components/elements';
import {
    MultiPolygon,
    MultiLineString,
    MultiPoint,
    Position,
} from 'sdi/source';
import { getMiniMap } from 'angled-core/queries/map';
import miniMap, {
    miniMapLoading,
    miniMapLoaded,
    miniMapFailed,
} from 'sdi/map/mini';
import { setMiniMap } from 'angled-core/events/map';
import { fieldDisplayName } from 'angled-core/queries/app';
import { WidgetFilterOpt } from 'angled-core/profile';

const logger = debug('sdi:angled/read/geometry');

const map = miniMap(
    'https://geoservices-urbis.irisnet.be/geoserver/ows/urbis.irisnet.be',
    {
        LAYERS: 'urbisFRGray',
    }
);

type Self = MultiPoint | MultiLineString | MultiPolygon;

const hashCoordinate = (p: Position) => `${p[0]},${p[1]}`;

const hashGeom = (geom: Self) => {
    switch (geom.type) {
        case 'MultiPoint':
            return geom.coordinates.map(hashCoordinate).join('~');
        case 'MultiLineString':
            return geom.coordinates
                .map(multi => multi.map(hashCoordinate).join('+'))
                .join('~');
        case 'MultiPolygon':
            return geom.coordinates
                .map(multi =>
                    multi
                        .map(inner => inner.map(hashCoordinate).join('+'))
                        .join('~')
                )
                .join('/');
    }
};

export const renderValue = (_fieldName: string, value: Self) =>
    DIV(
        { className: 'field__value field--read' },
        getMiniMap(hashGeom(value)).foldL(
            () => {
                logger(`starting minimap ${hashGeom(value).substr(0, 16)}`);
                // we're in render phase, store is likely locked
                window.setTimeout(
                    () => setMiniMap(hashGeom(value), miniMapLoading()),
                    1
                );
                map(value)
                    .then(opt =>
                        opt.map(urlString =>
                            setMiniMap(
                                hashGeom(value),
                                miniMapLoaded(urlString)
                            )
                        )
                    )
                    .catch(() => setMiniMap(hashGeom(value), miniMapFailed()));
                return NODISPLAY();
            },
            step => {
                logger(
                    `stepping minimap ${hashGeom(value).substr(0, 16)} -> ${
                        step.step
                    }`
                );
                switch (step.step) {
                    case 'loading':
                        return DIV({ className: '_loader' }, '~loading');
                    case 'failed':
                        return DIV({ className: '_failed' }, '~failed');
                    case 'loaded':
                        return IMG({ src: step.data });
                }
            }
        )
    );

export const render = (
    u: UnitData,
    fieldName: string,
    value: Self,
    _filter: WidgetFilterOpt
) =>
    DIV(
        {
            className: `field field--${fieldName} field--read`,
            title: fieldDisplayName(typeNameToName[u.unit], fieldName),
        },
        renderValue(fieldName, value)
    );

export default render;

logger('loaded');
