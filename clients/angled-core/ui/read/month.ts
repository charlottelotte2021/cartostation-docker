import { UnitData, typeNameToName } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { fieldDisplayName } from 'angled-core/queries/app';
import { WidgetFilterOpt } from 'angled-core/profile';

type Self = string;



const renderValue =
    (value: Self) =>
        DIV({ className: 'field__value field--read' }, `${value}`);

export const render = (
    u: UnitData,
    fieldName: string,
    value: Self,
    _filter: WidgetFilterOpt
) =>
    DIV({
        className: `field field--${fieldName} field--read`,
        title: fieldDisplayName(typeNameToName[u.unit], fieldName),
    },
        renderValue(value),
    );

export default render;
