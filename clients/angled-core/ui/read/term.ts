import { findTerm, findDomain } from 'angled-core/queries/universe';
import { DIV, SPAN } from 'sdi/components/elements';
import { fromRecord } from 'sdi/locale';
import { scopeOption } from 'sdi/lib';
import { UnitData, typeNameToName } from '..';
import { fieldDisplayName } from 'angled-core/queries/app';
import { WidgetFilterOpt } from 'angled-core/profile';

type Self = number;

const renderValue =
    (value: Self) =>
        scopeOption()
            .let('term', findTerm(value))
            .let('domain', ({ term }) => findDomain(term.domain))
            .map(
                ({ term }) =>
                    DIV({ className: 'field__value field--read' },
                        // SPAN({ className: '_domain-name' }, fromRecord(domain.name)),
                        SPAN({ className: '_term-name' }, fromRecord(term.name))),
            );

export const formatTermValue =
    (value: Self) =>
        scopeOption()
            .let('term', findTerm(value))
            .fold(
                '',
                ({ term }) =>
                    `${fromRecord(term.name)}`
            );


const filteredClass = (
    fieldName: string,
    value: Self,
    filter: WidgetFilterOpt,
) => filter
    .map(({ term }) =>
        term.id === value ?
            `field field--${fieldName} field--read hidden` :
            `field field--${fieldName} field--read`
    )
    .getOrElse(`field field--${fieldName} field--read`);


export const render = (
    u: UnitData,
    fieldName: string,
    value: Self,
    filter: WidgetFilterOpt,
) =>
    DIV({
        className: filteredClass(fieldName, value, filter),
        title: fieldDisplayName(typeNameToName[u.unit], fieldName),
    },
        renderValue(value),
    );

export default render;
