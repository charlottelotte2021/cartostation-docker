import { UnitData, typeNameToName } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { fromNullable, Option } from 'fp-ts/lib/Option';
import { loadNovaAndExecuteCallback, setNovaAsLoading, setNovaAsLoadingError, setNovaAsLoaded } from 'angled-core/events/ref';
import { findNova } from 'angled-core/queries/ref';
import { Nova, NovaLoader } from 'angled-core/ref';
import { fieldDisplayName } from 'angled-core/queries/app';
import { WidgetFilterOpt } from 'angled-core/profile';

type Self = string;

const renderKV =
    <T>(k: string, v: T) =>
        DIV({ className: 'kv' },
            DIV({ className: 'k' }, k),
            DIV({ className: 'v' }, v),
        );

export const renderDataNova = // todo fixme mettre ça dans une vue / view
    (nova: Option<NovaLoader>) => nova.foldL(
        () => DIV({ className: 'field field--nova field--preview invalid' }, 'No DATA'),
        (nova) => {
            switch (nova.status) {
                case 'UNLOADED': return DIV({ className: 'field field--nova field--preview invalid' }, 'UNLOAD');
                case 'LOADING': return DIV({ className: 'field field--nova field--preview invalid' }, 'LOADING');
                case 'ERROR': return DIV({ className: 'field field--nova field--preview invalid' }, 'ERROR');
                default:
                    return fromNullable(nova.data).map(
                        // .foldL(
                        // () => NODISPLAY(),
                        (nova_data: Nova) =>
                            DIV({ className: 'field field--nova field--preview' },  // todo mettre field-read ?
                                renderKV('time_of_fetch', nova_data.time_of_fetch),
                                renderKV('zipcode', nova_data.data.zipcode),
                                renderKV('catdossier', nova_data.data.catdossier),
                                renderKV('s_idaddress', nova_data.data.s_idaddress),
                                renderKV('s_iddossier', nova_data.data.s_iddossier),
                                renderKV('typedossie', nova_data.data.typedossier),
                                renderKV('coordinate_x', nova_data.data.coordinate_x),
                                renderKV('coordinate_y', nova_data.data.coordinate_y),
                                renderKV('streetnamefr', nova_data.data.streetnamefr),
                                renderKV('streetnamenl', nova_data.data.streetnamenl),
                                renderKV('municipalityownerfr', nova_data.data.municipalityownerfr),
                                renderKV('municipalityownernl', nova_data.data.municipalityownernl),
                            )
                    );
            }
        }
    );



const renderRefNova = // render the ref
    (value: Self) =>
        DIV({ className: 'field__value field--read' }, value);

export const renderDataNovaWhenLoaded =
    (refNova: Self, _errorMessage: string) =>
        renderDataNova(findNova(refNova));

const doLoadNova =
    (refNova: string) => {
        setNovaAsLoading(refNova);
        loadNovaAndExecuteCallback(
            refNova,
            (novaData) => setNovaAsLoaded(refNova, novaData),
            () => setNovaAsLoadingError(refNova)
        );
    };

const tryLoadDataNova =
    (refNova: string) => {
        findNova(refNova).foldL(
            () => doLoadNova(refNova),
            (nova) => {
                if (nova.status != 'LOADING' && nova.status != 'LOADED' && nova.status != 'ERROR') {
                    doLoadNova(refNova);
                } // todo fix me : print a log ?
            }
        );
    };

export const render = (
    u: UnitData,
    fieldName: string,
    value: Self,
    _filter: WidgetFilterOpt
) => {
    tryLoadDataNova(value);
    return DIV({
        className: `field field--${fieldName} field--read`,
        title: fieldDisplayName(typeNameToName[u.unit], fieldName),
    },
        renderRefNova(value),
        renderDataNovaWhenLoaded(value, 'Nova pas chargé'),
    );
};

export default render;
