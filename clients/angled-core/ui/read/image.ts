import { UnitData } from 'angled-core/ui';
import { DIV, IMG, NODISPLAY } from 'sdi/components/elements';
import { WidgetFilterOpt } from 'angled-core/profile';

type Self = string;

const renderValue =
    (value: Self) => {
        if (value === '/documents/images/') {  // FIXME: could be better for handling where there is no image uploaded (yet)
            return NODISPLAY();
        }
        else {
            return DIV({ className: 'field__value field--read' },
                IMG({ className: 'field__value--image', src: value }));
        }
    };

export const render = (
    _u: UnitData,
    fieldName: string,
    value: Self,
    _filter: WidgetFilterOpt
) =>
    DIV({ className: `field field--${fieldName} field--read` },
        renderValue(value),
    );

export default render;
