import { UnitData, typeNameToName } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import {
    findContact,
    findSite,
    findFundingOrg,
    findTeam,
    findUser,
} from 'angled-core/queries/ref';
import { Contact, Site, FundingOrg, Team } from 'angled-core/ref';
import { IUser } from 'sdi/source';
import { fromRecord } from 'sdi/locale';
import { none } from 'fp-ts/lib/Option';
import { fieldDisplayName } from 'angled-core/queries/app';
import { WidgetFilterOpt } from 'angled-core/profile';
import { catOptions } from 'fp-ts/lib/Array';

type Self = number;

const fkMap = {
    operator: findContact,
    site: findSite,
    occupant_contact: findContact,
    poc: findContact,
    contact: findContact,
    org: findFundingOrg,
    client_contact: findContact,
    expert_contact: findContact,
    bidders_team: findTeam,
    winner_team: findTeam,
    manager_user: findUser,
};

const formatContact =
    (contact: Contact) => contact.name;

const formatSite =
    (site: Site) => fromRecord(site.name);

const formatFundingOrg =
    (f: FundingOrg) => f.name;

// const joinSpans = (
//     nodes: NodeOrOptional[],
//     sep: string,
// ) => flatten(nodes.map((node, index) => {
//     if (index < nodes.length - 1) {
//         return [node, SPAN('separator', sep)]
//     }
//     return [node]
// }))
// Got to comment this out atm as angled-query relies on this format outputing a string
// const formatTeamNode =
//     (team: Team) => {
//         const mains = team.members
//             .filter(m => m.role === 'MAIN')
//             .map(m => findContact(m.member).map(contact => SPAN('team-member-main', contact.name)))
//         const regulars = team.members
//             .filter(m => m.role === 'REGU')
//             .map(m => findContact(m.member).map(contact => SPAN('team-member-regular', contact.name)))

//         return joinSpans(mains.concat(regulars), ', ')
//     };

const formatTeam =
    (team: Team) => {
        const mains = team.members
            .filter(m => m.role === 'MAIN')
            .map(m => findContact(m.member).map(contact => `${contact.name}*`))
        const regulars = team.members
            .filter(m => m.role === 'REGU')
            .map(m => findContact(m.member).map(contact => contact.name))

        return catOptions(mains.concat(regulars)).join(', ');
    };

const formatManager =
    (user: IUser) => user.name;

const renderContact =
    (contact: Contact) =>
        DIV({ className: 'field__value field--read' }, formatContact(contact));

const renderSite =
    (site: Site) =>
        DIV({ className: 'field__value field--read' }, formatSite(site));

const renderFundingOrg =
    (f: FundingOrg) =>
        DIV({ className: 'field__value field--read' }, formatFundingOrg(f));

const renderTeam =
    (team: Team) =>
        DIV({ className: 'field__value field--read' }, ...formatTeam(team));

const renderManager =
    (user: IUser) =>
        DIV({ className: 'field__value field--read' }, formatManager(user));


export const renderValue =
    (fieldName: string, value: Self) => {
        switch (fieldName) {
            case 'operator': return fkMap[fieldName](value).map(renderContact);
            case 'site': return fkMap[fieldName](value).map(renderSite);
            case 'occupant_contact': return fkMap[fieldName](value).map(renderContact);
            case 'poc': return fkMap[fieldName](value).map(renderContact);
            case 'contact': return fkMap[fieldName](value).map(renderContact);
            case 'org': return fkMap[fieldName](value).map(renderFundingOrg);
            case 'client_contact': return fkMap[fieldName](value).map(renderContact);
            case 'expert_contact': return fkMap[fieldName](value).map(renderContact);
            case 'bidders_team': return fkMap[fieldName](value).map(renderTeam);
            case 'winner_team': return fkMap[fieldName](value).map(renderTeam);
            case 'manager_user': return fkMap[fieldName](value).map(renderManager);
        }
        return none;
    };

export const formatFkValue =
    (fieldName: string, value: Self) => {
        switch (fieldName) {
            case 'operator': return fkMap[fieldName](value).map(formatContact);
            case 'site': return fkMap[fieldName](value).map(formatSite);
            case 'occupant_contact': return fkMap[fieldName](value).map(formatContact);
            case 'poc': return fkMap[fieldName](value).map(formatContact);
            case 'contact': return fkMap[fieldName](value).map(formatContact);
            case 'org': return fkMap[fieldName](value).map(formatFundingOrg);
            case 'client_contact': return fkMap[fieldName](value).map(formatContact);
            case 'expert_contact': return fkMap[fieldName](value).map(formatContact);
            case 'bidders_team': return fkMap[fieldName](value).map(formatTeam);
            case 'winner_team': return fkMap[fieldName](value).map(formatTeam);
            case 'manager_user': return fkMap[fieldName](value).map(formatManager);
        }
        return none;
    };


export const render = (
    u: UnitData,
    fieldName: string,
    value: Self,
    _filter: WidgetFilterOpt
) =>
    DIV({
        className: `field field--${fieldName} field--read`,
        title: fieldDisplayName(typeNameToName[u.unit], fieldName),
    },
        renderValue(fieldName, value)
            .getOrElse(DIV({ className: 'field__value error' },
                `~Could not find ${fieldName}(${value})`)),
    );

export default render;
