import * as debug from 'debug';
import { assign, dispatch, query } from 'sdi/shape';
import {
    fetchSubscriptionList,
    postSubscribeProject,
    postUnsubscribeProject,
    postReSubscribeProject,
    fetchNotificationList,
    fetchTaggedUnit,
    markNotificationRead,
} from 'angled-core/remote/sub';
import {
    getSubscriptionForProject,
    notifications,
} from 'angled-core/queries/sub';
import { fromNullable } from 'fp-ts/lib/Option';
import { Notif } from 'angled-core/sub';
import {
    mapRemote,
    remoteLoading,
    remoteSuccess,
} from 'angled-core/../sdi/source';

const logger = debug('sdi:angled/sub/event');

export const loadSubscriptions = () => {
    assign('data/subscriptions', remoteLoading);
    fetchSubscriptionList()
        .then(xs => assign('data/subscriptions', remoteSuccess(xs)))
        .catch(err => assign('data/subscriptions', err));
};

export const subscribeProject = (pid: number) =>
    postSubscribeProject(pid)
        .then(s =>
            dispatch('data/subscriptions', r =>
                mapRemote(r, xs => xs.concat(s))
            )
        )
        .catch(err => logger(`failed posting sub ${err}`));

export const reSubscribeProject = (pid: number) =>
    getSubscriptionForProject(pid).map(s =>
        postReSubscribeProject(s)
            .then(s =>
                dispatch('data/subscriptions', r =>
                    mapRemote(r, xs => xs.filter(x => x.id !== s.id).concat(s))
                )
            )
            .catch(err => logger(`failed re subscribing ${err}`))
    );

export const unsubscribeProject = (pid: number) =>
    getSubscriptionForProject(pid).map(s =>
        postUnsubscribeProject(s)
            .then(s =>
                dispatch('data/subscriptions', r =>
                    mapRemote(r, xs => xs.filter(x => x.id !== s.id).concat(s))
                )
            )
            .catch(err => logger(`failed unsubscibing ${err}`))
    );

const checkTagNotifications = () => {
    const tagged = query('data/tagged');
    const findTagged = (tid: number) =>
        fromNullable(tagged.find(t => t.id === tid));
    return notifications()
        .filter(n => n.action === 'tag')
        .map(n => {
            const tagged = findTagged(n.unit_id);
            if (tagged.isNone()) {
                fetchTaggedUnit(n.unit, n.unit_id)
                    .then(tagged =>
                        dispatch('data/tagged', ts => ts.concat(tagged))
                    )
                    .catch(err => logger(`Failed to fetch tagged unit ${err}`));
            }
        });
};

export const markRead = (n: Notif) =>
    markNotificationRead(n)
        .then(n =>
            dispatch('data/notifications', notifs =>
                mapRemote(notifs, ns =>
                    ns.filter(({ id }) => id !== n.id).concat(n)
                )
            )
        )
        .catch(err => logger('mark read failed', err));

export const startNotifications = () => {
    const interval = 10 * 60 * 1000;
    const fetcher = () => {
        assign('data/notifications', remoteLoading);
        fetchNotificationList()
            .then(ns => assign('data/notifications', remoteSuccess(ns)))
            .catch(err => assign('data/notifications', err))
            .then(checkTagNotifications);
    };
    window.setInterval(fetcher, interval);
    fetcher();
};

logger('loaded');
