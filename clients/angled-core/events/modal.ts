import { register, renderFK, renderGeometry, renderTeam } from '../components/modal';


export const [
    closeModalFK,
    openModalFK,
] = register('core/fk', renderFK);


export const [
    closeModalGeometry,
    openModalGeometry,
] = register('core/geometry', renderGeometry);


export const [
    closeModalTeam,
    openModalTeam,
] = register('core/team', renderTeam);
