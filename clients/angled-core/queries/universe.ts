import { pickSortL } from 'sdi/lib';
import { fromRecord } from 'sdi/locale';
import { Domain, Term, DomainList, termDescription, domainDescription } from 'angled-core/ref';
import { query } from 'sdi/shape';
import { fromNullable } from 'fp-ts/lib/Option';
import { InformationUnitName } from 'angled-core/ui';
import { mapOption } from 'fp-ts/lib/Array';




const domainSort = pickSortL((d: Domain) => fromRecord(d.name));
const termSort = pickSortL((t: Term) => fromRecord(t.name));


export const getDomainList =
    () => domainSort(query('data/domains').concat());

export const getTermList =
    () => termSort(query('data/terms').concat());


export const findDomain =
    (id: number) => fromNullable(getDomainList().find(d => d.id === id));

export const getDomainIdByName =
    (name: string) => getDomainList().find(d => d.name.fr === name);

export const findTerm =
    (id: number) =>
        fromNullable(getTermList().find(t => t.id === id));

export const findTermsByDomain =
    (id: number, domainId: number) =>
        fromNullable(termsInDomain(domainId).find(t => t.id === id));


export const termsInDomain =
    (domainId: number) => getTermList().filter(t => t.domain === domainId);


export const domainsForField =
    (unit: InformationUnitName, field: string): DomainList => {
        const mappings = query('data/domain-mappings').filter(m => m.unit === unit && m.field === field);
        if (0 === mappings.length) {
            return getDomainList();
        }
        return mapOption(mappings, m => findDomain(m.domain));
    };

export const getDescriptionFromTerm =
    (termId: number) =>
        findTerm(termId)
            .chain(termDescription)
            .fold<string>('', fromRecord)


export const getDescriptionFromDomain =
    (domainId: number) =>
        findDomain(domainId)
            .chain(domainDescription)
            .fold<string>('', fromRecord)