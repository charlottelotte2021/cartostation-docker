import { fromNullable } from 'fp-ts/lib/Option';
import { query } from 'sdi/shape';
import { pickSortL } from 'sdi/lib';
import { IUser } from 'sdi/source';
import { flatten, tryNumber, ensureArray } from 'sdi/util';
import { Contact } from 'angled-core/ref';
import { Unit } from 'angled-core/ui';
import { index } from 'fp-ts/lib/Array';

const contactSort = pickSortL((c: Contact) => c.name.toLocaleLowerCase());
const userSort = pickSortL((u: IUser) => u.name.toLocaleLowerCase());

export const getLocalityList = () => query('data/locality');

export const getContactList = () => contactSort(query('data/contact').concat());

export const getFundingOrgList = () => query('data/funding-org');

export const getSiteList = () => query('data/site');

export const getTeamMemberList = () => query('data/team-member');

export const getTeamList = () => query('data/team');

export const getUserList = () => userSort(query('data/users').concat());

export const getUserData = () => fromNullable(query('data/user'));

export const getAudienceList = () =>
    query('data/audiences')
        .concat()
        .sort((a, b) => a.id - b.id);

export const getDefaultAudience = () => index(0, getAudienceList());

export const getNovaList = () => query('data/nova');

export const findNova = (ref: string) =>
    fromNullable(getNovaList().find(nd => nd.ref === ref));

export const findLocality = (id: number) =>
    fromNullable(getLocalityList().find(i => i.id === id));

export const findContact = (id: number) =>
    fromNullable(getContactList().find(i => i.id === id));

export const findFundingOrg = (id: number) =>
    fromNullable(getFundingOrgList().find(i => i.id === id));

export const findSite = (id: number) =>
    fromNullable(getSiteList().find(i => i.id === id));

export const findTeamMember = (id: number) =>
    fromNullable(getTeamMemberList().find(i => i.id === id));

export const findTeam = (id: number) =>
    fromNullable(getTeamList().find(i => i.id === id));

export const findUser = (id: number) =>
    fromNullable(getUserList().find(u => parseInt(u.id, 10) === id)); // todo fixme etrange de faire le parse int ici

export const findAudience = (id: number) =>
    fromNullable(getAudienceList().find(a => a.id === id));

export const findAudiencesForRole = (id: number) =>
    getAudienceList().filter(({ group }) => group === id);

export const findSchoolSite = (id: number) =>
    fromNullable(query('data/school-sites').find(s => s.gid === id));

const currentUserCanSeeUnitImpl = ({ readings }: Unit) =>
    getUserData()
        .map(({ roles }) => {
            const inAudience = flatten(
                roles.map(({ id }) =>
                    tryNumber(id)
                        .map(id => findAudiencesForRole(id).map(a => a.id))
                        .getOrElse([])
                )
            );
            return readings.some(
                ({ audience }) => inAudience.indexOf(audience) >= 0
            );
        })
        .getOrElse(false);

export const currentUserCanSeeUnit = (unit: Unit | Unit[]) =>
    ensureArray(unit).some(currentUserCanSeeUnitImpl);
