import { query, queryK } from 'sdi/shape';
import { InformationUnitName, getFieldValue } from 'angled-core/ui';
import { fromNullable, none } from 'fp-ts/lib/Option';
import { getContactList } from './ref';
import { Audience, Contact } from 'angled-core/ref';

export const getUnitInEdition =
    () => query('component/ui/form/manip');

export const getFormInput =
    (unitName: InformationUnitName, fieldName: string) =>
        fromNullable(query('component/ui/form/manip'))
            .chain(unitData => fromNullable(getFieldValue(unitName, fieldName, unitData)));

export const getFormInputTeamMembers =
    () => query('component/ui/form/manip-team-member');

export const getNovaRefFormInput =
    () => query('component/ui/form/nova-ref');

export const getNovaDataFormInput =
    () => query('component/ui/form/nova-data');


export const getNewFundingOrgFormInput =
    () => query('component/ui/form/new-funding');

export const getNewSiteFormInput =
    () => query('component/ui/form/new-site');

export const getNewTeamFormInput =
    () => query('component/ui/form/new-team');

export const getNewContactFormInput =
    () => query('component/ui/form/new-contact');


export const getWriteDomain =
    () => query('component/ui/form/write/domain');


export const getCurrentImage =
    () =>
        fromNullable(query('component/ui/image'))
            .getOrElse('');

export const getCurrentImageFile =
    () =>
        fromNullable(query('component/ui/image-file'))
            .getOrElse(new File([''], 'filename'));

export const getCurrentDocumentFiles =
    () =>
        fromNullable(query('component/ui/document-file'))
            .getOrElse([new File([''], 'filename')]);

export const getSelectedFeaturePath = () => fromNullable(query('port/map/select'));

export const getSelectedFeature =
    () =>
        getSelectedFeaturePath()
            .chain(({ layerId, featureId }) => {
                if (layerId && featureId) {
                    const layer = query('data/layers')[layerId];
                    return fromNullable(layer.features.find(f => f.id === featureId));
                }
                return none;
            });


export const getFormModalStatus = queryK('component/ui/form/modal-status');

export const getWriteUnit =
    () => fromNullable(query('component/ui/write/modal/unit'));


export const getTeamInputFilter =
    () => query('component/ui/team/contact-pattern');


export const getTeamFilteredContact =
    () => {
        const selected = getFormInputTeamMembers().map(([c, _r]) => c.id);
        const pat = new RegExp(`.*${getTeamInputFilter()}.*`, 'i');
        const isNotSelected = (c: Contact) => selected.indexOf(c.id) < 0;
        return getContactList().filter(c => isNotSelected(c) && pat.test(c.name));
    };



export const getTeamFilteredContactExists =
    () => {
        const pat = getTeamInputFilter().toUpperCase();
        return getContactList().filter(c => c.name.toUpperCase() === pat).length > 0;
    };

export const getDisplayAudience = () =>
    fromNullable(query('component/ui/display-audience'));

export const audienceIsForDisplay = (a: Audience) =>
    getDisplayAudience()
        .map(({ id }) => id === a.id)
        .getOrElse(false);
