import { Nullable } from 'sdi/util';
import { UnitData, InformationUnitName } from 'angled-core/ui';
import { ButtonComponent } from 'sdi/components/button';
import { FundingOrg, Site, Team, Contact, NovaLoader, TeamMemberRole, Audience } from 'angled-core/ref';
import { ModalStatus } from 'sdi/components/modal';

export type writeDomain = { fieldName: string, id: number };

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'component/ui/form/manip': Nullable<Partial<UnitData>>; // etat de la modif en cours (niveau UI)
        'component/ui/form/write/domain': writeDomain[];
        'component/ui/form/new-funding': Partial<FundingOrg>;
        'component/ui/form/new-site': Partial<Site>;
        'component/ui/form/new-team': Partial<Team>;
        'component/ui/form/new-contact': Partial<Contact>;
        'component/ui/form/modal-status': ModalStatus;
        'component/ui/form/manip-team-member': [Contact, TeamMemberRole][];
        'component/ui/form/write/geometry/md': Nullable<string>;
        'component/button': ButtonComponent;

        'component/ui/image': Nullable<string>;
        'component/ui/image-file': Nullable<File>; // FIXME: state shall be serializable;
        'component/ui/document-file': File[]; // FIXME: state shall be serializable;


        // used for write action
        'component/ui/form/nova-ref': Nullable<string>;
        'component/ui/form/nova-data': NovaLoader;

        'component/ui/write/modal/unit': Nullable<[InformationUnitName, string]>;
        'component/ui/team/contact-pattern': string;

        'component/ui/display-audience': Nullable<Audience>;
    }
}

const initNovaData: NovaLoader = { ref: null, status: 'UNLOADED', data: null };

export const defaultUnitState =
    () => ({
        'component/ui/form/manip': null,
        'component/ui/form/write/domain': [],
        'component/ui/form/new-funding': {},
        'component/ui/form/new-site': {},
        'component/ui/form/new-team': {},
        'component/ui/form/new-contact': {},
        'component/ui/form/write/geometry/md': null,
        'component/button': {},
        'component/ui/form/modal-status': 'close' as ModalStatus,
        'component/ui/form/manip-team-member': [],

        'component/ui/image': null,
        'component/ui/image-file': null,
        'component/ui/document-file': [],

        'component/ui/form/nova-ref': null,
        'component/ui/form/nova-data': initNovaData,

        'component/ui/write/modal/unit': null,
        'component/ui/team/contact-pattern': '',

        'component/ui/display-audience': null,

    });
