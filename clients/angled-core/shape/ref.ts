import { Nullable } from 'sdi/util';
import {
    AudienceList,
    Contact,
    FundingOrg,
    Locality,
    Site,
    TagList,
    Team,
    TeamMember,
    SchoolSite,
    NovaLoader,
} from 'angled-core/ref';
import { IUser } from 'sdi/source';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'data/contact': Contact[];
        'data/funding-org': FundingOrg[];
        'data/site': Site[];
        'data/team': Team[];
        'data/team-member': TeamMember[];
        'data/locality': Locality[];
        'data/users': IUser[];
        'data/user': Nullable<IUser>;
        'data/tags': TagList;
        'data/audiences': AudienceList;
        'data/school-sites': SchoolSite[];

        // used for read action
        'data/nova': NovaLoader[];
    }
}

export const defaultRef = () => ({
    'data/contact': [],
    'data/funding-org': [],
    'data/site': [],
    'data/team': [],
    'data/team-member': [],
    'data/locality': [],
    'data/users': [],
    'data/user': null,
    'data/tags': [],
    'data/audiences': [],
    'data/school-sites': [],
    'data/nova': [],
});
