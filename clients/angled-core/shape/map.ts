/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { IMapInfo, MessageRecord, FeatureCollection, Inspire, IServiceBaseLayers } from 'sdi/source';
import { defaultInteraction, IMapViewData, Interaction, IMapScale, IViewEvent, FeaturePath } from 'sdi/map';
import { IToolGeocoder, defaultToolGeocoder } from 'sdi/ports/geocoder';

import { Collection, Nullable } from 'sdi/util';
import { MiniStep } from 'sdi/map/mini';


// State Augmentation

declare module 'sdi/shape' {
    export interface IShape {
        // 'component/geocoder/response': IUgWsResponse | null;
        // 'component/geocoder/input': string;

        'port/map/view': IMapViewData;
        'port/map/interaction': Interaction;
        'port/map/loading': MessageRecord[];
        'port/map/scale': IMapScale;
        'port/map/minimap': Collection<MiniStep>;
        'port/map/select': Nullable<FeaturePath>;
        'port/component/geocoder': IToolGeocoder;
        'port/component/geocoder/interaction/save': Nullable<Interaction>;


        'data/maps': IMapInfo[];
        'data/baselayers': IServiceBaseLayers;
        'data/layers': Collection<FeatureCollection>;
        'data/datasetMetadata': Inspire[];
    }
}

export const defaultMapView = (): IMapViewData => ({
    dirty: 'geo',
    srs: 'EPSG:31370',
    center: [148885, 170690],
    rotation: 0,
    zoom: 8,
    feature: null,
    extent: null,
});

export const defaultMapEvent = (): IViewEvent => ({
    dirty: 'geo',
    center: [148885, 170690],
    rotation: 0,
    zoom: 8,
});


export const defaultMapState = () => ({
    'port/map/scale': {
        count: 0,
        unit: '',
        width: 0,
    },

    'port/map/view': defaultMapView(),

    'port/map/interaction': defaultInteraction(),
    'port/map/loading': [],
    'port/map/minimap': {},
    'port/map/select': null,
    'port/component/geocoder': defaultToolGeocoder(),
    'port/component/geocoder/interaction/save': null,


    'data/baselayers': [],
    'data/maps': [],
    'data/layers': {},
    'data/datasetMetadata': [],
});
