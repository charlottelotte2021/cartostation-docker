import * as io from 'io-ts';
import { InformationUnitNameIO } from 'angled-core/ui';
import { fromNullable, Option } from 'fp-ts/lib/Option';
import { Term } from 'angled-core/ref';

// tslint:disable-next-line: variable-name
export const OptionValueIO = io.union([io.string, io.number], 'OptionValueIO');
// tslint:disable-next-line: variable-name
export const PanelOptionsIO = io.dictionary(io.string, OptionValueIO, 'PanelOptionsIO');
export type PanelOptions = io.TypeOf<typeof PanelOptionsIO>;

// todo fixme à générer
// tslint:disable-next-line: variable-name
export const UnitWidgetIO = io.interface({
  type: io.literal('UnitWidget'),
  name: InformationUnitNameIO,
  options: PanelOptionsIO,
}, 'UnitWidgetIO');
export type UnitWidget = io.TypeOf<typeof UnitWidgetIO>;



// tslint:disable-next-line: variable-name
export const ProjectViewIO = io.interface({
  type: io.literal('ProjectView'),
  name: io.string,
  options: PanelOptionsIO,
}, 'ProjectViewIO');
export type ProjectView = io.TypeOf<typeof ProjectViewIO>;


// tslint:disable-next-line: variable-name
export const DirectionIO = io.union([
  io.literal('Vertical'),
  io.literal('Horizontal'),
], 'DirectionIO');
export type Direction = io.TypeOf<typeof DirectionIO>;

export interface DirectedContainer {
  type: 'DirectedContainer';
  direction: Direction;
  children: Panel[];
  options: PanelOptions;
}

export type Panel = UnitWidget | DirectedContainer | ProjectView;

export const getPanelOption =
  (optionName: string) => (panel: Panel) =>
    fromNullable(panel.options[optionName]);


// tslint:disable-next-line: variable-name
const DirectedContainerIO: io.Type<DirectedContainer> = io.recursion('DirectedContainerIO', () =>
  io.interface({
    type: io.literal('DirectedContainer'),
    direction: DirectionIO,
    children: io.array(PanelIO),
    options: PanelOptionsIO,
  }),
);

// tslint:disable-next-line: variable-name
const PanelIO: io.Type<Panel> = io.recursion('PannelIO', () =>
  io.union([UnitWidgetIO, DirectedContainerIO, ProjectViewIO]),
);

// tslint:disable-next-line: variable-name


// tslint:disable-next-line:variable-name
export const PageIO = io.interface({
  type: io.literal('Page'),
  boxes: io.array(DirectedContainerIO),
}, 'PageIO');
export type Page = io.TypeOf<typeof PageIO>;

export const LayoutIO = io.array(PageIO);
export type Layout = io.TypeOf<typeof LayoutIO>;

// tslint:disable-next-line: variable-name
export const ProfileIO = io.interface({
  id: io.Integer,
  name: io.string,
  layout: LayoutIO,
});

export type Profile = io.TypeOf<typeof ProfileIO>;

// tslint:disable-next-line: variable-name
export const ProfileListIO = io.array(ProfileIO);

export type ProfileList = io.TypeOf<typeof ProfileListIO>;


export interface WidgetFilter {
  fieldName: string;
  term: Term;
}

export type WidgetFilterOpt = Option<WidgetFilter>;
