import * as io from 'io-ts';
import { PointIO } from 'sdi/source';
import { InformationUnitNameIO } from 'angled-core/ui';

// tslint:disable-next-line: variable-name
export const ProjectSubscriptionIO = io.interface(
    {
        tag: io.literal('sub/project'),
        id: io.Integer,
        created_at: io.string,
        active: io.boolean,
        project: io.Integer,
    },
    'ProjectSubscriptionIO',
);

// tslint:disable-next-line: variable-name
export const ProjectSubscriptionListIO = io.array(
    ProjectSubscriptionIO,
    'ProjectSubscriptionlistIO',
);

export type ProjectSubscription = io.TypeOf<typeof ProjectSubscriptionIO>;

export const makePojectSub = (pid: number): Partial<ProjectSubscription> => ({
    tag: 'sub/project',
    active: true,
    project: pid,
});

// tslint:disable-next-line: variable-name
export const CircleSubscriptionIO = io.interface(
    {
        tag: io.literal('sub/circle'),
        id: io.Integer,
        created_at: io.string,
        active: io.boolean,
        radius: io.Integer,
        geom: PointIO,
    },
    'CircleSubscriptionIO',
);

// tslint:disable-next-line: variable-name
export const CircleSubscriptionListIO = io.array(
    CircleSubscriptionIO,
    'CircleSubscriptionListIO',
);

export type CircleSubscription = io.TypeOf<typeof CircleSubscriptionIO>;

// tslint:disable-next-line: variable-name
export const SubscriptionIO = io.union(
    [ProjectSubscriptionIO, CircleSubscriptionIO],
    'SubscriptionIO',
);
// tslint:disable-next-line: variable-name
export const SubscriptionListIO = io.array(SubscriptionIO);

export type Subscription = ProjectSubscription | CircleSubscription;

// tslint:disable-next-line: variable-name
export const ProjectNotificationIO = io.interface(
    {
        tag: io.literal('notif/project'),
        id: io.Integer,
        created_at: io.string,
        mark_view: io.boolean,
        subscription: io.Integer,
        action: io.string,
        unit: InformationUnitNameIO,
        unit_id: io.Integer,
    },
    'ProjectNotificationIO',
); 



// tslint:disable-next-line: variable-name
export const ProjectNotificationListIO = io.array(
    ProjectNotificationIO,
    'ProjectNotificationListIO',
);

export type ProjectNotification = io.TypeOf<typeof ProjectNotificationIO>;

// tslint:disable-next-line: variable-name
export const CircleNotificationIO = io.interface(
    {
        tag: io.literal('notif/circle'),
        id: io.Integer,
        created_at: io.string,
        mark_view: io.boolean,
        subscription: io.Integer,
        action: io.string,
        unit: InformationUnitNameIO,
        unit_id: io.Integer,
    },
    'CircleNotificationIO',
);

// tslint:disable-next-line: variable-name
export const CircleNotificationListIO = io.array(
    CircleNotificationIO,
    'CircleNotificationListIO',
);

export type CircleNotification = io.TypeOf<typeof CircleNotificationIO>;

// tslint:disable-next-line: variable-name
export const NotifIO = io.union(
    [CircleNotificationIO, ProjectNotificationIO],
    'NotifIO',
);

// tslint:disable-next-line: variable-name
export const NotifListIO = io.array(NotifIO, 'NotifListIO');

export type Notif = io.TypeOf<typeof NotifIO>;


