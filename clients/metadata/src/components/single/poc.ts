import { BUTTON, DETAILS, DIV, SPAN, SUMMARY } from 'sdi/components/elements';
import {
    getPocList,
    getSelectedPocs,
    getPocSearch,
    isSelectedPoc,
} from '../../queries/metadata';
import { addPoc, setPocSearch } from '../../events/metadata';
import tr from 'sdi/locale';
import { inputText } from 'sdi/components/input';
import { isSelectOpen } from 'sdi/app';

const filter = () =>
    inputText({
        key: 'poc-search-input',
        get: getPocSearch,
        set: k => setPocSearch(k.toLocaleLowerCase()),
        attrs: {
            id: 'poc-search',
            placeholder: tr.core('filter'),
        },
        monitor: e => setPocSearch(e.toLocaleLowerCase()),
    });

// const removeBtn = makeIcon('cancel', 3, 'times', {
//     text: () => tr.core('clear'),
//     position: 'bottom-right',
// });

export const renderSelectedPocs = () =>
    DIV(
        'tag__list',
        ...getSelectedPocs().map(poc =>
            DIV(
                { className: 'tag' },
                SPAN({ className: 'tag__value' }, poc.contactName)
                // removeBtn(() => removePoc(kw.id))
            )
        )
    );

export const renderSelectPoc = () =>
    DIV(
        {},
        DETAILS(
            {
                className: `select-filter__wrapper`,
                open: isSelectOpen('select-metadata-poc'),
            },
            SUMMARY('', tr.meta('add')),
            DIV(
                'select-tail__wrapper',
                filter(),
                DIV(
                    'tail',
                    ...getPocList()
                        .filter(poc => !isSelectedPoc(poc.id))
                        .filter(poc =>
                            poc.contactName
                                .toLocaleLowerCase()
                                .includes(getPocSearch())
                        )
                        .map(poc =>
                            BUTTON(
                                {
                                    className: 'tail-item',
                                    key: poc.id,
                                    onClick: () => addPoc(poc.id),
                                    // isSelectedPoc(k.id)
                                    //     ? removePoc(k.id)
                                    //     : addPoc(k.id),
                                },
                                SPAN('item__control'),
                                DIV('item__label', poc.contactName)
                            )
                        )
                )
            )
        )
    );
