import { fromPredicate } from 'fp-ts/lib/Either';

import {
    DIV,
    H1,
    INPUT,
    TEXTAREA,
    SPAN,
    NODISPLAY,
    LABEL,
    H2,
} from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import {
    Inspire,
    MessageRecord,
    getMessageRecord,
    makeRecord,
    MessageRecordLang,
    MaintenanceFrequency,
    maintenanceFrequencyValues,
} from 'sdi/source';

import { getLayout, isMetadataAdmin } from '../../queries/app';
import { setLayout } from '../../events/app';
import {
    formIsSaving,
    getCurrentDatasetMetadata,
    getMdDescription,
    getMdTitle,
    getTemporalReference,
    getPersonOfContact,
    getMaintenanceFrequencyStr,
    getMdMaintenanceFrequency,
    getMdForm,
    // getTopics,
    // getTopicList,
    // isSelectedTopic,
} from '../../queries/metadata';

import {
    saveMdForm,
    setMdDescription,
    setMdTitle,
    setMdMaintenanceFreq,
    // removeTopic,
    // addTopic,
} from '../../events/metadata';

import { renderSelectedKeywords, renderSelectKeyword } from './keywords';
import { renderSelectPoc } from './poc';

import { makeLabelAndIcon } from '../button';
import { renderSelect } from 'sdi/components/input/select';
import { setoidString } from 'fp-ts/lib/Setoid';
import { fromNullable, some } from 'fp-ts/lib/Option';
import { renderRadioOut } from 'sdi/components/input';

export interface MdForm {
    title: MessageRecord;
    description: MessageRecord;
    topics: string[];
    keywords: string[];
    published: boolean;
    maintenance_freq: MaintenanceFrequency;
    saving: boolean;
    poc: number[];
}

const saveButton = makeLabelAndIcon('save', 1, 'save', () => tr.meta('save'));
// const removeButton = (key: string) =>
//     makeRemove(
//         `remove-${key}`,
//         2,
//         () => tr.meta('remove'),
//         () => tr.meta('remove')
//     );
// const addKeyword = makeLabelAndIcon('add', 2, 'plus', () => tr.meta('add'));

export const defaultMdFormState = (): MdForm => ({
    title: makeRecord(),
    description: makeRecord(),
    topics: [],
    keywords: [],
    published: false,
    maintenance_freq: 'unknown',
    saving: false,
    poc: [],
});

type TextGetter = () => string;
type TextSetter = (a: string) => void;

const isNotSaving = fromPredicate<void, React.ReactNode>(
    () => !formIsSaving(),
    () => {}
);

const renderInputText = (label: string, get: TextGetter, set: TextSetter) => {
    const defaultValue = get();
    const update = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newVal = e.currentTarget.value;
        set(newVal);
    };
    return INPUT({
        defaultValue,
        placeholder: label,
        type: 'text',
        onChange: update,
    });
};

const renderTextArea = (label: string, get: TextGetter, set: TextSetter) => {
    const defaultValue = get();
    const update = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        const newVal = e.currentTarget.value;
        set(newVal);
    };
    return TEXTAREA({
        defaultValue,
        placeholder: label,
        onChange: update,
        rows: 8,
    });
};

const renderEditLang = (
    l: MessageRecordLang,
    title: string,
    abstract: string,
    notice?: string
) =>
    DIV(
        { className: `app-col-wrapper meta-${l}` },
        H2('', `${l}${notice !== undefined ? ` (${notice})` : ''}`),
        DIV(
            { className: 'app-col-main' },
            LABEL(
                { className: 'label' },
                title,
                renderInputText(title, getMdTitle(l), setMdTitle(l))
            ),
            LABEL(
                { className: 'label' },
                abstract,
                renderTextArea(
                    abstract,
                    getMdDescription(l),
                    setMdDescription(l)
                )
            )
        )
    );

const renderEdit = (m: Inspire) =>
    DIV(
        { className: 'meta-edit' },
        renderInfo(m),
        DIV(
            'main__wrapper',
            DIV(
                'lang__wrapper',
                renderEditLang('fr', 'Titre', 'Résumé'),
                renderEditLang('nl', 'Titel', 'Overzicht')
                // renderEditLang('en', 'Title', 'Abstract')
            )
            // DIV(
            //     'keywords__wrapper',
            //     H2('', tr.meta('keywords')),
            //     renderSelectedKeywords()
            // )
        )
    );

const renderPoc = () =>
    // m.metadataPointOfContact.map(id =>
    //     getPersonOfContact(id).fold(NODISPLAY({ key: 'renderPoc-none' }), poc =>
    DIV(
        'point-of-contact-wrapper',
        getMdForm().poc.map(p =>
            getPersonOfContact(p).fold(
                NODISPLAY({ key: 'renderPoc-none' }),
                poc =>
                    DIV(
                        {
                            className: 'point-of-contact',
                            key: `renderPoc-${poc.id}`,
                        },
                        SPAN({ className: 'contact-name' }, poc.contactName),
                        SPAN({ className: 'contact-email' }, poc.email),
                        SPAN(
                            { className: 'contact-organisation' },
                            fromRecord(getMessageRecord(poc.organisation.name))
                        )
                    )
            )
        ),
        renderSelectPoc()
    );

// const closeBtn = makeIcon('close', 2, 'times-circle', {
//     text: () => tr.core('close'),
//     position: 'left',
// });

// const renderCommon = (_m: Inspire) =>
//     DIV(
//         { className: 'app-col-wrapper meta-common' },
//         H2(
//             '',
//             tr.meta('keywords'),
//             closeBtn(() => setLayout('Single'))
//         ),
//         DIV(
//             { className: 'app-col-main' },
//             // renderSelectTopic(),
//             renderSelectKeyword()
//         )
//     );

const labeledString = (l: string, v: string) =>
    DIV(
        { className: 'metadata-label' },
        DIV({ className: 'label' }, l),
        DIV({ className: 'value' }, v)
    );

const labeledNode = (l: string, v: React.ReactNode) =>
    DIV({ className: 'metadata-label' }, DIV({ className: 'label' }, l), v);

// const publishButton = makeLabelAndIcon('toggle-off', 2, 'toggle-off', 'toggle-off');
// const unpublishButton = makeLabelAndIcon('toggle-on', 2, 'toggle-on', 'toggle-on');

// const publishButton = makeLabel('publish', 2, () => tr.meta('toggle-on'));
// const unpublishButton = makeLabel('publish', 2, () => tr.meta('toggle-off'));

// const renderPublishState = ({ published }: Inspire) => {
//     if (published) {
//         return DIV(
//             { className: 'publication' },
//             tr.meta('metadata:isPublishedInternalandExternal'),
//             DIV(
//                 {},
//                 DIV(
//                     { className: 'label' },
//                     tr.meta('metadata:publicationToCatalogue')
//                 ),
//                 unpublishButton(mdDraft)
//             )
//         );
//     }
//     return DIV(
//         { className: 'publication' },
//         tr.meta('metadata:isPublishedInternal'),
//         DIV(
//             {},
//             DIV(
//                 { className: 'label' },
//                 tr.meta('metadata:publicationToCatalogue')
//             ),
//             publishButton(mdPublish)
//         )
//     );
// };

// // const renderPublishState =
// //     ({ published }: Inspire) => {
// //         if (published) {
// //             return DIV({ className: 'toggle' },
// //                 DIV({ className: 'no-active' }, tr.meta('internal')),
// //                 unpublishButton(mdDraft),
// //                 DIV({ className: 'active' }, tr.meta('inspireCompliant')));
// //         }
// //         return DIV({ className: 'toggle' },
// //             DIV({ className: 'active' }, tr.meta('internal')),
// //             publishButton(mdPublish),
// //             DIV({ className: 'no-active' }, tr.meta('inspireCompliant')));
// //     };

// export const renderKeywords = () => {
//     const list = getKeywords().map(kw => {
//         const id = kw.id;
//         return DIV(
//             { className: 'keyword' },
//             removeButton(id)(() => removeKeyword(id)),
//             SPAN({ className: 'value' }, fromRecord(kw.name))
//         );
//     });
//     return DIV({}, ...list);
// };

const selectMaintenanceFreq = renderSelect(
    'select-maintenance-frequentie',
    getMaintenanceFrequencyStr,
    setMdMaintenanceFreq,
    setoidString
);

const renderInfo = (m: Inspire) =>
    DIV(
        { className: 'app-col-wrapper metadata-info' },
        H2('', tr.meta('layerInfo')),
        DIV(
            { className: 'app-col-main' },
            labeledString(tr.meta('geometryType'), m.geometryType),
            labeledString(
                tr.meta('layerId'),
                fromNullable(m.resourceIdentifier).getOrElse(
                    m.uniqueResourceIdentifier
                )
            ),
            // labeledNode(tr.meta('publicationStatus'), renderPublishState(m)),
            labeledString(
                tr.meta('temporalReference'),
                getTemporalReference(m.temporalReference)
            ),
            labeledNode(tr.meta('pointOfContact'), renderPoc()),
            labeledNode(
                tr.meta('maintenanceFrequency'),
                selectMaintenanceFreq(
                    maintenanceFrequencyValues,
                    some(getMdMaintenanceFrequency())
                )
            ),
            // labeledNode(tr.meta('topics'), renderTopics()),
            H2('', tr.meta('keywords')),
            renderSelectedKeywords(),
            renderSelectKeyword()

            // labeledNode(
            //     tr.meta('keywords'),
            //     DIV(
            //         {},
            //         // renderKeywords(),
            //         // addKeyword(() => setLayout('SingleAndKeywords'))
            //         renderSelectKeyword()
            //     )
            // )
            // labeledString(tr.meta('identifier'), m.id),
        )
    );

type singleLayout = 'admin' | 'simple';
const selectAdminLayout = (layout: singleLayout) => {
    switch (layout) {
        case 'admin':
            setLayout('AdminSingle');
            break;
        case 'simple':
            setLayout('Single');
            break;
    }
};
const getAdminMetadata = () =>
    getLayout() === 'AdminSingle' ? 'admin' : 'simple';

const renderSwitchLabels = (layout: singleLayout) =>
    DIV('switch-label', tr.meta(layout));

const switchAdminMetadata = renderRadioOut(
    'switch-admin',
    renderSwitchLabels,
    selectAdminLayout
);
const renderSwitchAdmin = () =>
    isMetadataAdmin().map(() =>
        switchAdminMetadata(['admin', 'simple'], getAdminMetadata())
    );

const renderAction = (_m: Inspire) =>
    DIV(
        'meta-action',
        isNotSaving(saveButton(saveMdForm)).fold(
            () =>
                DIV(
                    { className: 'saving' },
                    SPAN({ className: 'loader-spinner' }),
                    SPAN({}, tr.meta('saving'))
                ),
            e => e
        )
    );

const renderMetadataHeader = (m: Inspire) =>
    DIV(
        'metadata__header',
        H1({}, tr.meta('metadataEditor')),
        renderSwitchAdmin(),
        renderAction(m)
    );

const renderEditor = (m: Inspire) =>
    DIV(
        'metadata-editor',
        renderMetadataHeader(m),
        DIV('meta-wrapper', renderEdit(m))
    );

const render = () =>
    getCurrentDatasetMetadata().fold(
        H1({}, `Loading Current Metadata Failed`),
        renderEditor
    );

export default render;
