import { DIV, H1 } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { getSplash } from '../queries/app';

const render = () =>
    DIV(
        {},
        H1({}, tr.meta('metadata')),
        DIV({}, `${tr.meta('loadingData')}: ${getSplash()}%`)
    );

export default render;
