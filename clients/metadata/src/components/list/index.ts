import { DIV, H1, H2 } from 'sdi/components/elements';
import tr from 'sdi/locale';
import metadata from '../table/metadata';
import { getSchemaList, getSelectedSchema } from '../../queries/metadata';
import { renderRadioIn } from 'sdi/components/input';
import { selectSchema } from 'metadata/src/events/metadata';
import { getAliasOption } from 'sdi/app';

const schemaElement = (s: string) => {
    if (s === 'all') {
        return DIV('schema', tr.meta('allSchema'));
    }
    return DIV('schema', s);
};

const schemaFilter = renderRadioIn(
    'schema-filter',
    (s: string) => schemaElement(s),
    selectSchema,
    'radio'
);

const render = () => {
    const list = ['all'].concat(getSchemaList());
    return DIV(
        'metadata-list',
        H1({}, tr.meta('sheetList')),
        DIV(
            'metadata__wrapper',
            DIV(
                'schema-list__wrapper',
                H2(
                    '',
                    getAliasOption('md/domain').getOrElse(tr.meta('md/domain'))
                ),
                schemaFilter(list, getSelectedSchema())
            ),
            metadata()
        )
    );
};

export default render;
