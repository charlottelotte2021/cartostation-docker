import * as debug from 'debug';
import { none, fromNullable, some } from 'fp-ts/lib/Option';

import { query, subscribe } from 'sdi/shape';
import {
    MessageRecordLang,
    TemporalReference,
    FreeText,
    isAnchor,
    isTemporalExtent,
    Inspire,
    MaintenanceFrequency,
} from 'sdi/source';
import tr, { fromRecord, formatDate, rec } from 'sdi/locale';
import { TableDataType, TableDataRow, TableSource } from 'sdi/components/table';
import { isNotNullNorUndefined } from 'sdi/util';
import { getLayout } from './app';
import { getAliasOption } from 'sdi/app';
import { MetadataMessageKey } from '../locale';

const logger = debug('sdi:queries/metadata');

const layerKeys: MetadataMessageKey[] = [
    'title',
    'geometryType',
    // tr.meta('layerId'),
    'md/domain',
    'md/path',
    // tr.meta('publicationStatus'),
    'temporalReference',
    'md/module',
    // tr.meta('pointOfContact'),
    // tr.meta('responsibleOrganisation'),
];

// metadata list
const loadLayerListKeys = () =>
    layerKeys.map(k => getAliasOption(k).getOrElse(tr.meta(k)));

const loadLayerListTypes = (): TableDataType[] => [
    'string',
    'string',
    'string',
    'string',
    'string',
    'string',
    // 'string',
];

export const getTemporalReference = (t: TemporalReference) => {
    if (isTemporalExtent(t)) {
        return formatDate(new Date(Date.parse(t.end)));
    }
    return formatDate(new Date(Date.parse(t.revision)));
};

export const getMaintenanceFrequencyStr = (freq: MaintenanceFrequency) => {
    switch (freq) {
        case 'continual':
            return tr.meta('continual');
        case 'daily':
            return tr.meta('daily');
        case 'weekly':
            return tr.meta('weekly');
        case 'fortnightly':
            return tr.meta('fortnightly');
        case 'monthly':
            return tr.meta('monthly');
        case 'quarterly':
            return tr.meta('quarterly');
        case 'biannually':
            return tr.meta('biannually');
        case 'annually':
            return tr.meta('annually');
        case 'asNeeded':
            return tr.meta('asNeeded');
        case 'irregular':
            return tr.meta('irregular');
        case 'notPlanned':
            return tr.meta('notPlanned');
        case 'unknown':
            return tr.meta('unknown');
    }
};

const splitURL = (rid: string) => {
    const splitStr = rid.split('://', 2);
    if (splitStr.length === 2) {
        const domAndPath = splitStr[1].split('/', 2);
        if (domAndPath.length === 2) {
            return some({
                module: splitStr[0],
                dom: domAndPath[0],
                path: domAndPath[1],
            });
        }
    }
    return none;
};
const getModule = (rid: string) =>
    splitURL(rid).fold('', domAndPath => domAndPath.module);
export const getDomain = (rid: string) =>
    splitURL(rid).fold('', domAndPath => domAndPath.dom);
const getPath = (rid: string) =>
    splitURL(rid).fold('', domAndPath => domAndPath.path);

const getLayerListData = (mds: Readonly<Inspire[]>): TableDataRow[] => {
    const getFreeText = (ft: FreeText) => {
        if (isAnchor(ft)) {
            return fromRecord(ft.text);
        }

        return fromRecord(ft);
    };

    return mds
        .filter(({ resourceIdentifier }) => {
            return (
                isNotNullNorUndefined(resourceIdentifier) &&
                (getSelectedSchema() === null ||
                    getSelectedSchema() === 'all' ||
                    getSelectedSchema() === getDomain(resourceIdentifier))
            );
        })
        .map(md => {
            const rid = md.resourceIdentifier!;
            const cells = [
                getFreeText(md.resourceTitle),
                md.geometryType,
                getDomain(rid),
                getPath(rid),
                // md.published ? tr.meta('published') : tr.meta('draft'),
                getTemporalReference(md.temporalReference),
                getModule(rid),
                // md.metadataPointOfContact.reduce((acc, poc, idx) => {
                //     const sep = idx === 0 ? '' : ', ';
                //     return `${acc}${sep}${poc.contactName}`;
                // }, ''),
                // md.responsibleOrganisation.reduce((acc, ri, idx) => {
                //     const sep = idx === 0 ? '' : '; ';
                //     return acc + sep + getFreeText(ri.organisationName);
                // }, ''),
            ];
            return { from: md.id, cells };
        });
};

export const getTableSource = subscribe(
    'data/datasetMetadata',
    state =>
        ({
            kind: 'local',
            data: getLayerListData(state),
            keys: loadLayerListKeys(),
            types: loadLayerListTypes(),
        } as TableSource),
    'app/lang',
    'data/md/schema'
);

// Keywords

export const getKeywordSearch = () =>
    fromNullable(query('metadata/keyword/search')).getOrElse('');

// const keywordsKeys = () => [tr.meta('label'), tr.meta('thesaurus')];

// const keywordsTypes = (): TableDataType[] => ['string', 'string'];

// const getKeywordsData = (kws: Readonly<Keyword[]>): TableDataRow[] => {
//     const filtered = isMetadataAdmin()
//         ? kws
//         : kws.filter(kw => !kw.thesaurus.admin_only);
//     return filtered.map(kw => {
//         const cells = [fromRecord(kw.name), fromRecord(kw.thesaurus.name)];
//         return { from: kw.id, cells };
//     });
// };

// export const getKeywordsSource = subscribe(
//     'data/keywords',
//     state =>
//         ({
//             kind: 'local',
//             data: getKeywordsData(state),
//             keys: keywordsKeys(),
//             types: keywordsTypes(),
//         } as TableSource),
//     'app/lang'
// );

export const getPersonOfContact = (id: number) =>
    fromNullable(query('data/poc').find(poc => poc.id === id));

export const getResponsibleOrg = (id: number) =>
    fromNullable(query('data/md/org').find(org => org.id === id));

export const getMdForm = () => query('component/single');

export const getMdTitle = (l: MessageRecordLang) => () =>
    rec(getMdForm().title, l);

export const getMdDescription = (l: MessageRecordLang) => () =>
    rec(getMdForm().description, l);

export const getMdMaintenanceFrequency = () => getMdForm().maintenance_freq;

export const formIsSaving = () => getMdForm().saving;

export const getMetadataId = () => query('app/current-metadata');

export const getMetadataList = () => query('data/datasetMetadata');

export const getDatasetMetadata = (id: string) =>
    fromNullable(getMetadataList().find(md => md.id === id));

export const getCurrentDatasetMetadata = () => {
    const id = getMetadataId();
    if (id) {
        return getDatasetMetadata(id);
    }
    return none;
};

const withoutNull = <T>(ts: (T | null)[]) => {
    const r: T[] = [];
    ts.forEach(t => (!!t ? r.push(t) : null));
    return r;
};

export const getSchemaList = () => {
    const uniques: string[] = [];
    getMetadataList().map(md => {
        const schema = getDomain(md.resourceIdentifier!);
        if (uniques.indexOf(schema) === -1) {
            uniques.push(schema);
        }
    });
    return uniques;
};

export const getSelectedSchema = () => query('data/md/schema');

export const getKeywordList = () =>
    getLayout() === 'AdminSingle'
        ? query('data/keywords')
        : query('data/keywords').filter(kw => !kw.thesaurus.admin_only);

export const getKeywordDataOpt = (id: string) =>
    fromNullable(query('data/keywords').find(k => k.id === id));

export const getKeywords = () =>
    withoutNull(
        getMdForm()
            .keywords.map(getKeywordDataOpt)
            .map(o => o.fold(null, k => k))
    );

export const isSelectedKeyword = (id: string) =>
    getMdForm().keywords.indexOf(id) >= 0;

export const getTopicList = () => query('data/topics');

export const getTopicDataOpt = (id: string) =>
    fromNullable(query('data/topics').find(k => k.id === id));

export const getTopics = () =>
    withoutNull(
        getMdForm()
            .topics.map(getTopicDataOpt)
            .map(o => o.fold(null, k => k))
    );

export const isSelectedTopic = (id: string) =>
    getMdForm().topics.indexOf(id) >= 0;

export const getPocList = () => query('data/poc');
export const getSelectedPocs = () => query('data/md/poc');
export const getPocSearch = () =>
    fromNullable(query('metadata/poc/search')).getOrElse('');
export const isSelectedPoc = (id: number) => getMdForm().poc.indexOf(id) >= 0;

logger('loaded');
