import * as debug from 'debug';

import { fromNullable } from 'fp-ts/lib/Option';

import { getApiUrl } from 'sdi/app';
import {
    getMessageRecord,
    Inspire,
    MaintenanceFrequency,
    MdPointOfContact,
    MessageRecordLang,
    ResponsibleOrganisation,
} from 'sdi/source';
import { scopeOption } from 'sdi/lib';
import { dispatchK, dispatch, assign } from 'sdi/shape';

import {
    getDatasetMetadata,
    getMdForm,
    getMetadataId,
} from '../queries/metadata';
import {
    putMetadata,
    fetchPointOfContact,
    fetchOrganisation,
    fetchPointOfContactList,
} from '../remote';

const logger = debug('sdi:events/metadata');
const single = dispatchK('component/single');
const apiUrl = (s: string) => getApiUrl(s);

export const setMdTitle = (l: MessageRecordLang) => (t: string) =>
    single(s => ({ ...s, title: { ...s.title, [l]: t } }));

export const setMdDescription = (l: MessageRecordLang) => (t: string) =>
    single(s => ({ ...s, description: { ...s.description, [l]: t } }));

export const setMdMaintenanceFreq = (freq: MaintenanceFrequency) =>
    single(s => ({ ...s, maintenance_freq: freq }));

export const selectMetadata = (id: string) => {
    dispatch('app/current-metadata', () => id);
    getDatasetMetadata(id)
        .map(md => {
            md.metadataPointOfContact.forEach(loadPersonOfContact);
            md.responsibleOrganisation.forEach(loadResponsibleOrganisation);
            return md;
        })
        .map(md =>
            dispatch('component/single', s => ({
                ...s,
                keywords: md.keywords,
                topics: md.topicCategory,
                title: getMessageRecord(md.resourceTitle),
                description: getMessageRecord(md.resourceAbstract),
                maintenance_freq: md.maintenanceFrequency,
                poc: md.metadataPointOfContact,
            }))
        );
};

export const selectSchema = (schema: string) =>
    assign('data/md/schema', schema);

export const selectMdPoc = (poc: MdPointOfContact) =>
    dispatch('data/md/poc', state => {
        return state.filter(p => p.id !== poc.id).concat(poc);
    });
export const selectMdOrg = (o: ResponsibleOrganisation) =>
    dispatch('data/md/org', state => {
        return state.filter(p => p.id !== o.id).concat(o);
    });

export const loadPersonOfContact = (id: number) =>
    fetchPointOfContact(getApiUrl(`md/poc/${id}`))
        .then(selectMdPoc)
        .catch(err => logger(`loadPersonOfContact error ${err}`));

export const loadPersonOfContactList = () =>
    fetchPointOfContactList(getApiUrl('md/poc')).then(pocList =>
        assign('data/poc', pocList)
    );

export const loadResponsibleOrganisation = (id: number) =>
    fetchOrganisation(getApiUrl(`md/org/${id}`))
        .then(org =>
            dispatch('data/md/org', state => {
                return state.filter(o => o.id !== id).concat(org);
            })
        )
        .catch(err => logger(`loadResponsible error ${err}`));

const updatedMd = (md: Inspire): Inspire => {
    const {
        title,
        description,
        keywords,
        topics,
        published,
        maintenance_freq,
        poc,
    } = getMdForm();
    const newMd = {
        ...md,
        keywords,
        published,
        topicCategory: topics,
        resourceTitle: title,
        resourceAbstract: description,
        maintenanceFrequency: maintenance_freq,
        metadataPointOfContact: poc,
    };
    return newMd;
};

const updateLocalSet = (collection: Inspire[], md: Inspire) =>
    collection.filter(i => i.id !== md.id).concat(md);

export const saveMdForm = () =>
    scopeOption()
        .let('id', fromNullable(getMetadataId()))
        .let('md', s => getDatasetMetadata(s.id))
        .map(({ id, md }) => {
            single(s => ({ ...s, saving: true }));
            putMetadata(apiUrl(`metadatas/${id}`), updatedMd(md))
                .then(newMd => {
                    single(s => ({ ...s, saving: false }));
                    dispatch('data/datasetMetadata', collection =>
                        updateLocalSet(collection, newMd)
                    );
                })
                .catch(() => single(s => ({ ...s, saving: false })));
        });

export const setKeywordSearch = (k: string) =>
    assign('metadata/keyword/search', k);

export const addKeyword = (id: string) =>
    single(s => ({ ...s, keywords: s.keywords.concat([id]) }));

export const removeKeyword = (id: string) =>
    single(s => ({ ...s, keywords: s.keywords.filter(k => k !== id) }));

export const addTopic = (id: string) =>
    single(s => ({ ...s, topics: s.topics.concat([id]) }));

export const removeTopic = (id: string) =>
    single(s => ({ ...s, topics: s.topics.filter(k => k !== id) }));

export const mdPublish = () => {
    single(s => ({ ...s, published: true }));
    saveMdForm();
};
export const mdDraft = () => {
    single(s => ({ ...s, published: false }));
    saveMdForm();
};

export const addPoc = (id: number) =>
    single(s => ({ ...s, poc: s.poc.concat([id]) }));
export const setPocSearch = (p: string) => assign('metadata/poc/search', p);

// observe('data/datasetMetadata',
//     () => dispatch('component/table',
//         ts => ({ ...ts, loaded: false })));

logger('loaded');
