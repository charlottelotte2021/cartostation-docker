/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { loop, getUserId, getApiUrl, loadAlias } from 'sdi/app';
import { BUTTON, DIV, SPAN } from 'sdi/components/elements';
import header from 'sdi/components/header';
import footer from 'sdi/components/footer';
import splash from 'sdi/components/splash';
import tr from 'sdi/locale';

import {
    loadAllTopic,
    loadAllKeyword,
    loadAllDatasetMetadata,
    loadUser,
    setLayout,
    loadIsMetadataAdmin,
} from './events/app';
import { getLayout } from './queries/app';
import list from './components/list';
import single from './components/single';
import mdSplash from './components/splash';
// import "./events/route"
import { navigateIndex, loadRoute } from './events/route';
import { loadPersonOfContactList } from './events/metadata';

const logger = debug('sdi:app');

// export type AppLayout = 'List' | 'Single' | 'SingleAndKeywords' | 'Splash';
export type AppLayout = 'List' | 'Single' | 'AdminSingle' | 'Splash';

const renderAppListingButton = () => {
    const l = getLayout();
    if (l !== 'List' && l !== 'Splash') {
        return BUTTON(
            {
                className: 'navigate app-listview',
                // onClick: () => events.setLayout('List'),
                onClick: () => navigateIndex(),
            },
            SPAN({ className: 'label' }, tr.meta('sheetList'))
        );
    }
    return DIV({});
};

const wrappedMain = (
    name: string,
    ...elements: React.DOMElement<{}, Element>[]
) =>
    DIV(
        { className: 'metadata-inner' },
        header('metadata', renderAppListingButton),
        DIV({ className: `main ${name}` }, ...elements),
        footer()
    );

const renderSplash = () => wrappedMain('splash', splash(mdSplash()));
const renderList = () => wrappedMain('list', list());
const renderSingle = () => wrappedMain('single', single());

const renderMain = () => {
    const layout = getLayout();
    switch (layout) {
        case 'Splash':
            return renderSplash();
        case 'List':
            return renderList();
        // case 'SingleAndKeywords':
        case 'AdminSingle':
            return renderSingle();
        case 'Single':
            return renderSingle();
    }
};

const effects = (initialRoute: string[]) => () => {
    getUserId().map(userId => loadUser(getApiUrl(`users/${userId}`)));
    loadAlias(getApiUrl(`alias`));
    loadAllTopic();
    loadAllKeyword();
    loadAllDatasetMetadata(() => {
        setLayout('List');
        loadRoute(initialRoute);
    });
    loadIsMetadataAdmin();
    loadPersonOfContactList();
    tr.init_edited();
};

const app = (initialRoute: string[]) =>
    loop('metadata', renderMain, effects(initialRoute));
export default app;

logger('loaded');
