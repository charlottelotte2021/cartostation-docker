import { MessageStore, formatMessage } from 'sdi/locale';

const messages = {
    layerId: {
        fr: 'Schéma / Table',
        nl: 'Schema / Tabel', // nldone
        en: 'Schema / Table',
    },

    schema: {
        fr: 'Schéma',
        nl: 'Schema', // nldone
        en: 'Schema',
    },

    publicationStatus: {
        fr: 'État de publication',
        nl: 'Status van publicatie', // nldone
        en: 'Publication state',
    },

    geometryType: {
        fr: 'Geométrie',
        nl: 'Geometrie', // nldone
        en: 'Geometry',
    },

    title: {
        fr: 'Titre',
        nl: 'Titel', // nldone
        en: 'Title',
    },

    temporalReference: {
        fr: 'Dernière mise à jour de la métadonnée',
        nl: 'Laatste update van de metagegevens', // nldone
        en: 'Last update of the metadata',
    },
    maintenanceFrequency: {
        fr: 'Fréquence de mise à jour des données',
        nl: 'Frequentie van gegevensupdates', // nl todo
        en: 'frequency of data update',
    },

    continual: {
        fr: 'En continu (plusieurs fois par jour)',
        nl: 'Voortdurend (verschillende keren per dag)',
        en: 'repeatedly and frequently',
    },
    daily: {
        fr: 'Quotidienne',
        nl: 'Dagelijks',
        en: 'daily',
    },
    weekly: {
        fr: 'Hebdomadaire',
        nl: 'Wekelijks',
        en: 'weekly',
    },
    fortnightly: {
        fr: 'Toutes les deux semaines',
        nl: 'Om de twee weken',
        en: 'fortnightly',
    },
    monthly: {
        fr: 'Mensuelle',
        nl: 'Maandelijks',
        en: 'monthly',
    },
    quarterly: {
        fr: 'Tous les 3 mois',
        nl: 'Driemaandelijks',
        en: 'quarterly',
    },
    biannually: {
        fr: 'Biannuelle',
        nl: 'Tweejaarlijks',
        en: 'biannually',
    },
    annually: {
        fr: 'Annuelle',
        nl: 'Jaarlijks',
        en: 'annually',
    },
    asNeeded: {
        fr: 'Si nécessaire',
        nl: 'Indien nodig',
        en: 'as needed',
    },
    irregular: {
        fr: 'Irrégulièrement',
        nl: 'Onregelmatig',
        en: 'irregular',
    },
    notPlanned: {
        fr: 'Non planifiée',
        nl: 'Niet gepland',
        en: 'not planned',
    },
    unknown: {
        fr: 'Inconnue',
        nl: 'Onbekend',
        en: 'unknown',
    },

    draft: {
        fr: 'Brouillon',
        nl: 'Ontwerp', // nldone
        en: 'Draft',
    },

    published: {
        fr: 'Publié',
        nl: 'Gepubliceerd', // nldone
        en: 'Published',
    },

    label: {
        fr: 'Label',
        nl: 'Label', // nldone
        en: 'Label',
    },

    thesaurus: {
        fr: 'Thesaurus',
        nl: 'Thesaurus', // nldone
        en: 'Thesaurus',
    },

    'metadata:isPublishedInternal': {
        fr: 'Les données sont publiées en interne',
        nl: 'De gegevens worden intern gepubliceerd', // nldone
        en: 'The data are published internally',
    },

    'metadata:isPublishedInternalandExternal': {
        fr: 'Les données sont publiées en interne et au Catalogue Régional',
        nl: 'De gegevens worden intern en in de regionale catalogus gepubliceerd', // nldone
        en: 'The data are published internally and in the Regional Catalogue',
    },

    'metadata:publicationToCatalogue': {
        fr: 'Publication au Catalogue Régional',
        nl: 'Publicatie in de regionale catalogus', // nldone
        en: 'Publication in the Regional Catalogue',
    },

    metadata: {
        fr: 'Métadonnées',
        nl: 'Metadata', // nldone
        en: 'Metadatas',
    },

    loadingData: {
        fr: 'Chargement des données',
        nl: 'Data worden geladen', // nldone
        en: 'Loading datas',
    },

    save: {
        fr: 'Sauvegarder',
        nl: 'Opslaan', // nldone
        en: 'Save',
    },

    remove: {
        fr: 'Supprimer',
        nl: 'Verwijderen', // nldone
        en: 'Remove',
    },

    add: {
        fr: 'Ajouter',
        nl: 'Toevoegen', // nldone
        en: 'Add',
    },

    keywords: {
        fr: 'Mots clés',
        nl: 'Trefwoorden', // nldone
        en: 'Keyword',
    },

    saving: {
        fr: 'Mise à jour des données',
        nl: 'Gegevens bijwerken', // nldone
        en: 'Updating datas',
    },

    'toggle-off': {
        fr: 'Désactiver',
        nl: 'Deactiveren', // nldone
        en: 'Toggle off',
    },

    'toggle-on': {
        fr: 'Activer',
        nl: 'Activeren', // nldone
        en: 'Toggle on',
    },

    layerInfo: {
        fr: 'Informations',
        nl: 'Informatie', // nldone
        en: 'Informations',
    },

    pointOfContact: {
        fr: 'Point de contact',
        nl: 'Contactpunt', // nldone
        en: 'Point of contact',
    },

    identifier: {
        fr: 'Identifiant',
        nl: 'Identificatie', // nldone
        en: 'Identifier',
    },

    metadataEditor: {
        fr: 'Édition des métadonnées',
        nl: 'Metadata bewerken', // nldone
        en: 'Metadatas editor',
    },

    sheetList: {
        fr: 'Liste des fiches',
        nl: 'Lijst van de fiches', // nldone
        en: 'Sheet list',
    },

    metaCommon: {
        fr: 'Métadonnées bilingues',
        nl: 'Tweetalige metadata', // nldone
        en: 'Métadonées bilingues',
    },

    metaFrench: {
        fr: 'Métadonnées françaises',
        nl: 'Franse metadata', // nldone
        en: 'French metadata',
    },

    metaDutch: {
        fr: 'Métadonnées Néerlandaises',
        nl: 'Nederlandse metadata', // nldone
        en: 'Dutch metadata',
    },

    rmvMsgKeyword: {
        fr: 'Souhaitez-vous supprimer le mot clé ?',
        nl: 'Wilt u het trefwoord verwijderen ?', // nldone
        en: 'Would you like to delete the keyword?',
    },
    'md/module': {
        fr: 'Module',
        nl: 'Module', //nltodo
        en: 'Module',
    },
    'md/domain': {
        fr: 'Domaine',
        nl: 'Domain', //nltodo
        en: 'Domain',
    },
    'md/path': {
        fr: 'Chemin',
        nl: 'Pad', //nltodo
        en: 'Path',
    },
    allSchema: {
        fr: 'Tous les schémas',
        nl: 'Alle schemas', //nltodo
    },
    admin: {
        fr: 'Avancé',
        nl: 'Admin', //nltodo
    },
    simple: {
        fr: 'Simplifié',
        nl: 'Vereenvoudigd', //nltodo
    },
};

type MDB = typeof messages;
export type MetadataMessageKey = keyof MDB;

declare module 'sdi/locale' {
    export interface MessageStore {
        meta(k: MetadataMessageKey): Translated;
    }
}

export const isMessageKey = (text: string) => text in messages;

MessageStore.prototype.meta = function (k: MetadataMessageKey) {
    return this.getEdited('metadata', k, () => formatMessage(messages[k]));
};
