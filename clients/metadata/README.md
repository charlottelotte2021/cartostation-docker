![illustration](../assets/image/metadata.png)

The _metadata_ application is used to edit the metadata associated with _cartostation_ data layers.

A part of this metadata can be generated automatically (see Admin documentation), while another part, more qualitative, must be encoded manually by the persons in charge of this data.

NB: This application was developed primarily to meet the INSPIRE directive (https://inspire.ec.europa.eu/) in the context of Bruxelles Environnement administration. Its purpose could/should be more generic, and should be the subject of a specific project.

## User guide

The complete user guide is available here : https://cartostation.com/documentation
