/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// import { ChangeEvent, KeyboardEvent } from 'react';

// import { BUTTON, DIV, INPUT, SPAN } from 'sdi/components/elements';
// import { tr } from 'sdi/locale';
// import { isENTER } from 'sdi/components/keycodes';
// import { getLang } from 'sdi/app';

// import { updateGeocoderTerm } from '../../events/legend';
// import queries from '../../queries/legend';
// import { viewEvents, putMark } from '../../events/map';
// import { queryService } from 'sdi/geocoder/index';
// import { makeIcon } from '../button';
// import { ServiceRequest, ServiceResult } from 'sdi/geocoder/io';
// import { getAllServices, getAllServicesCodes, getSelectedService, getServicePlaceholder, getServiceResponse } from 'sdi/geocoder/queries';
// import { clearServiceResponse, selectService } from 'sdi/geocoder/events';
// import { getServiceName } from 'sdi/geocoder/queries';
// import { renderSelect } from 'sdi/components/input';

// const updateAddress = (e: ChangeEvent<HTMLInputElement>) => {
//     updateGeocoderTerm(e.target.value);
// };

// const searchAddress = (service_code: string) => {
//     const state = queries.toolsGeocoder();
//     const lang = getLang();
//     const serviceRequest: ServiceRequest = { service: service_code, input: state.address, lang }
//     queryService(serviceRequest)
// };

// const renderResults = (results: ServiceResult[]) => {
//     return results.map((result, key) => {
//         const coords: [number, number] = [result.coord.x, result.coord.y];
//         return DIV(
//             {
//                 className: 'result',
//                 key,
//                 onClick: () => {
//                     clearServiceResponse();
//                     viewEvents.updateMapView({
//                         dirty: 'geo',
//                         center: coords,
//                         zoom: 12,
//                     });
//                     putMark(coords);
//                 },
//             },
//             BUTTON({
//                 className: 'select-icon',
//                 "aria-labelledby": `adress-${key}`
//             }),
//             SPAN({
//                 id: `adress-${key}`,
//             },
//                 result.message));
//     });
// };

// const btnSearch = makeIcon('search', 2, 'search', { position: 'right', text: () => tr.core('search') });

// const renderService = (placeholder: string, service_code: string) =>
//     DIV('search-widget',
//         DIV('search',
//             INPUT({
//                 type: 'text',
//                 name: 'adress',
//                 placeholder: `${placeholder}`,
//                 onChange: updateAddress,
//                 onKeyPress: (e: KeyboardEvent<HTMLInputElement>) => {
//                     if (isENTER(e)) {
//                         searchAddress(service_code);
//                     }
//                 },
//             }),
//             btnSearch(() => searchAddress(service_code)),
//         ),
//         getServiceResponse().map(r => DIV('search-results', renderResults(r.results)))
//     )

// const renderServiceList = renderSelect(`geocoder-service`, c => getServiceName(c), selectService)
// const renderServiceOrList = () => {
//     const services = getAllServices();
//     return services.length == 1 ? DIV('', getServiceName(services[0].code)) : renderServiceList(getAllServicesCodes(), getSelectedService());
// }

// const renderGeocoder = () =>
//     DIV('',
//         getSelectedService().map(s => renderService(getServicePlaceholder(s), s))
//     )

// const render = () =>
//     DIV(
//         'geocoder-wrapper',
//         renderServiceOrList(),
//         renderGeocoder()
//     )

// export default render;
