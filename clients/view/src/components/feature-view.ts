import { ILayerInfo, Feature, getMessageRecord } from 'sdi/source';
import { DIV, H1 } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { renderConfig } from 'sdi/components/feature-view';
import timeserie from 'sdi/components/timeserie';

import {
    getCurrentFeature,
    getCurrentLayer,
    getDatasetMetadataOption,
    getMapInfoOption,
    getCurrentInfo,
} from '../queries/app';
import { dispatchTimeserie, loadData } from '../events/timeserie';
import { getData, queryTimeserie } from '../queries/timeserie';
import { viewEvents } from '../events/map';
import { makeLabelAndIcon } from './button';
import { getView } from '../queries/map';
import { setLayerVisibility } from '../events/app';
import debug = require('debug');

const logger = debug('view');

const zoomToFeature = () => getCurrentFeature().map(renderZoom);

const zoomBtn = makeLabelAndIcon('zoomOnFeature', 2, 'search', () =>
    tr.view('zoomOnFeature')
);
const activateAndZoomBtn = makeLabelAndIcon('zoomOnFeature', 2, 'search', () =>
    tr.view('activateLayerAndZoomOnFeature')
);

const renderZoom = (feature: Feature) => {
    logger(`CENTER ZOOM FEATURE: ${getView().center}`);
    return getCurrentInfo().map(li =>
        li.visible
            ? zoomBtn(() =>
                  viewEvents.updateMapView({
                      dirty: 'geo/feature',
                      feature,
                  })
              )
            : activateAndZoomBtn(() => {
                  setLayerVisibility(li.id, true);
                  viewEvents.updateMapView({
                      dirty: 'geo/feature',
                      feature,
                  });
              })
    );
};

// const renderZoomMultiFeature = (feature: Feature, index ) => {
//     console.log(`CENTER ZOOM FEATURE: ${getView().center}`);
//     return zoomBtn(() =>
//         viewEvents.updateMapView({
//             dirty: 'geo/feature',
//             feature,
//         })
//     );
// };

// export const switcher =
//     () =>
//         DIV({ className: 'switcher infos' },
//             DIV({
//                 className: `switch-legend`,
//                 title: tr.view('mapLegend'),
//                 onClick: () => {
//                     appEvents.setLayout(AppLayout.MapFS);
//                     appEvents.unsetCurrentFeature();
//                     legendEvents.setPage('legend');
//                 },
//             }),
//             zoomToFeature());

const tsPlotter = timeserie(
    queryTimeserie,
    getData,
    getCurrentLayer,
    dispatchTimeserie,
    loadData
);

const noView = () => DIV('sidebar-main feature-view no');

const featureHeader = (info: ILayerInfo) =>
    getDatasetMetadataOption(info.metadataId).map(md =>
        DIV(
            'sidebar-header feature-header',
            H1(
                {},
                getMapInfoOption().fold(
                    '',
                    mapInfo => fromRecord(mapInfo.title) as string
                )
            ),
            DIV('layer-name', fromRecord(getMessageRecord(md.resourceTitle)))
        )
    );

// const renderFeatureInfoSingle = (
//     info: ILayerInfo,
//     feature: Feature | Readonly<Feature>
// ) =>
//     DIV(
//         'feature-info-wrapper',
//         renderZoom(feature),
//         renderConfig(info.featureViewOptions, feature, tsPlotter)
//     );

// const renderFeatureInfoMulti = (
//     info: ILayerInfo,
//     features: Feature[] | Readonly<Feature[]>
// ) =>
//     DIV(
//         'multifeatures',
//         H2(
//             'feature-info-amount',
//             tr.view('selectedAmount'),
//             `: ${features.length}`
//         ),
//         ...features.map((feature, index) =>
//             DIV(
//                 'feature-info-wrapper',
//                 // DIV('selection-index', index + 1),
//                 makeLabelAndIcon('zoomOnFeature', 2, 'search', () =>
//                     concat(tr.view('zoomOnFeature'), formatNumber(index + 1))
//                 )(() =>
//                     viewEvents.updateMapView({
//                         dirty: 'geo/feature',
//                         feature,
//                     })
//                 ),

//                 // renderZoomMultiFeature(feature, index),
//                 renderConfig(info.featureViewOptions, feature, tsPlotter)
//             )
//         )
//     );
// const renderFeatureInfo = (
//     info: ILayerInfo,
//     features: Feature[] | Readonly<Feature[]>
// ) =>
//     DIV(
//         { className: 'sidebar-right feature-info-page' },
//         featureHeader(info),
//         features.length > 1
//             ? renderFeatureInfoMulti(info, features)
//             : renderFeatureInfoSingle(info, features[0])
//     );
// const withInfo = (info: ILayerInfo) =>
//     DIV(
//         'feature-list',
//         getSelectedFeatures().map(f => renderFeatureInfo(info, f))
//     );

const withInfo = (info: ILayerInfo) =>
    getCurrentFeature().fold(noView(), feature =>
        DIV(
            'sidebar__wrapper sidebar-right feature-info-page',
            featureHeader(info),
            zoomToFeature(),
            renderConfig(info.featureViewOptions, feature, tsPlotter)
        )
    );

const render = () => getCurrentInfo().map(withInfo);

export default render;

logger('loaded');
