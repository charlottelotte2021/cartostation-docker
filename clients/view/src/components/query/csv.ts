// import { findTerm, getAlias } from 'sdi/app';
// import { TableDataCell } from 'sdi/components/table2';
// import tr, {
//     formatNumber,
//     formatDate,
//     fromRecord,
//     Translated,
// } from 'sdi/locale';
// import { getFields, PropertyTypeDescriptor, streamFieldName } from 'sdi/source';
// import {
//     ensureArray,
//     tryBoolean,
//     tryNumber,
//     tryString,
//     datetime8601,
//     getLayerFields,
// } from 'sdi/util';
// import { HarvestedLayerLoaded } from '.';
// import { getConfigLayerKeys, getRessourceTitle } from './results';

// const formatCell = (data: TableDataCell, dataType: PropertyTypeDescriptor) => {
//     return ensureArray(data)
//         .map(data => {
//             const defaultReturn = `${data}` as Translated;
//             switch (dataType) {
//                 case 'boolean':
//                     return tryBoolean(data)
//                         .map<string>(b =>
//                             b ? tr.core('true') : tr.core('false')
//                         )
//                         .getOrElse(defaultReturn);
//                 case 'number':
//                     return tryNumber(data)
//                         .map(formatNumber)
//                         .getOrElse(defaultReturn);
//                 case 'string':
//                     return tryString(data).getOrElse(defaultReturn);
//                 case 'date':
//                     return tryString(data)
//                         .chain(s => tryNumber(Date.parse(s)))
//                         .map(n => formatDate(new Date(n)))
//                         .getOrElse(defaultReturn);
//                 case 'datetime': {
//                     return tryNumber(data).foldL(
//                         () =>
//                             tryString(data)
//                                 .chain(s => tryNumber(Date.parse(s)))
//                                 .map(n => datetime8601(new Date(n)))
//                                 .getOrElse(defaultReturn),
//                         n => datetime8601(new Date(n))
//                     );
//                 }
//                 case 'term':
//                     return tryNumber(data)
//                         .chain(findTerm)
//                         .map<string>(t => fromRecord(t.name))
//                         .getOrElse(defaultReturn);
//             }
//         })
//         .join('; ');
// };

// const getTyper = (hLayer: HarvestedLayerLoaded) => {
//     const fd = getFields(hLayer.data)
//         .map(fds => {
//             const result: { [k: string]: PropertyTypeDescriptor } = {};
//             fds.forEach(([f, t]) => (result[f] = t));
//             return result;
//         })
//         .getOrElseL(() => {
//             const keys = getConfigLayerKeys(hLayer);
//             const firstRow = hLayer.data.features[0].properties!;
//             const result: { [k: string]: PropertyTypeDescriptor } = {};
//             keys.forEach(k => {
//                 const val = firstRow[k];

//                 switch (typeof val) {
//                     case 'string':
//                         result[k] = 'string';
//                         break;
//                     case 'number':
//                         result[k] = 'number';
//                         break;
//                     case 'boolean':
//                         result[k] = 'boolean';
//                         break;
//                     default:
//                         result[k] = 'string';
//                 }
//             });
//             return result;
//         });

//     type FD = typeof fd;

//     return <K extends keyof FD>(key: K): PropertyTypeDescriptor => fd[key];
// };

// export const extractAsCSV = (hLayer: HarvestedLayerLoaded) => {
//     const separator = ';';
//     const filename = getRessourceTitle(hLayer.metadataId) + '.csv';
//     let csvContent = '';

//     const keys = getConfigLayerKeys(hLayer);
//     const fieldNames = getLayerFields(hLayer.data)
//         .map(list => list.map(streamFieldName))
//         .getOrElse([]);
//     const configFields = keys.filter(k => fieldNames.indexOf(k) >= 0);
//     const typer = getTyper(hLayer);

//     // Append keys as CSV header
//     csvContent += configFields.map(getAlias).join(separator) + '\r\n';

//     // Append values
//     hLayer.data.features.forEach(f => {
//         const rowData = f.properties || {};
//         const row = configFields
//             .map(k => formatCell(rowData[k], typer(k)))
//             .join(separator);
//         csvContent += row + '\r\n';
//     });

//     // Create the blob
//     const bom = '\uFEFF'; //to force Excel to use utf8 encoding
//     const blob = new Blob([bom, csvContent], {
//         type: 'text/csv;charset=utf-8',
//     });
//     const url = window.URL.createObjectURL(blob);

//     // Download
//     if ((navigator as any).msSaveBlob) {
//         (navigator as any).msSaveBlob(blob, filename);
//     } else {
//         const a = document.createElement('a');
//         a.href = url;
//         a.download = filename;
//         document.body.appendChild(a);
//         a.click();
//         document.body.removeChild(a);
//     }
//     window.URL.revokeObjectURL(url);
// };
