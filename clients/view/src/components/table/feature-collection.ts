/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { none, fromNullable } from 'fp-ts/lib/Option';

import tr from 'sdi/locale';
import { DIV, SPAN, A } from 'sdi/components/elements';
import {
    SelectRowHandler,
    TableDataRow,
    baseTable,
} from 'sdi/components/table';
import { scopeOption } from 'sdi/lib';

import { layerTableQueries } from '../../queries/table';
import { layerTableEvents } from '../../events/table';
import {
    getCurrentLayerInfo,
    getLayerData,
    getCurrentName,
    getCurrentMetadata,
} from '../../queries/app';
import {
    unsetCurrentFeature,
    setLayout,
    setCurrentFeature,
} from '../../events/app';
import { AppLayout } from '../../shape/types';
import { makeIcon } from '../button';
import { startExtract, stopExtract } from '../../events/map';
import { withExtract } from '../../queries/map';
import { exportLinks } from 'sdi/components/export';

const logger = debug('sdi:table/feature-collection');

const closeButton = makeIcon('close', 3, 'times');
const extractButton = makeIcon('toggle-off', 3, 'toggle-off');
const noExtractButton = makeIcon('toggle-on', 3, 'toggle-on');

const renderExtract = () =>
    withExtract().fold(
        DIV(
            { className: 'toggle' },
            DIV({ className: 'active' }, tr.view('extractOff')),
            extractButton(startExtract),
            DIV({ className: 'no-active' }, tr.view('extractOn'))
        ),
        () =>
            DIV(
                { className: 'toggle' },
                DIV({ className: 'no-active' }, tr.view('extractOff')),
                noExtractButton(stopExtract),
                DIV({ className: 'active' }, tr.view('extractOn'))
            )
    );

const renderExportCSV = () =>
    getCurrentMetadata().map(({ uniqueResourceIdentifier }) =>
        DIV(
            'btn export-csv',
            A(
                {
                    className: 'table-download',
                    target: '_blank',
                    href: withQueryString(uniqueResourceIdentifier, {
                        form: 'csv',
                    }),
                },
                SPAN(
                    {
                        className: 'dl-item',
                        'aria-label': `${tr.core(
                            'exportCSV'
                        )} ${getCurrentName().getOrElse('')}`,
                    },
                    'csv'
                )
            )
        )
    );
const renderExportXLSX = () =>
    getCurrentMetadata().map(({ uniqueResourceIdentifier }) =>
        DIV(
            'btn export-xlsx',
            A(
                {
                    className: 'table-download',
                    target: '_blank',
                    href: withQueryString(uniqueResourceIdentifier, {
                        form: 'xlsx',
                    }),
                },
                SPAN(
                    {
                        className: 'dl-item',
                        'aria-label': `${tr.core(
                            'exportXLSX'
                        )} ${getCurrentName().getOrElse('')}`,
                    },
                    'xlsx'
                )
            )
        )
    );

const toolbar = () =>
    DIV(
        { className: 'table-toolbar', key: 'table-toolbar' },
        DIV({ className: 'table-title' }, getCurrentName().getOrElse('...')),
        DIV({ className: 'table-download' }, renderExtract()),
        DIV(
            { className: 'table-download' },
            renderExportCSV(),
            renderExportXLSX()
        ),
        closeButton(() => {
            unsetCurrentFeature();
            setLayout(AppLayout.MapFS);
        })
    );

const onRowSelect: SelectRowHandler = (row: TableDataRow) =>
    scopeOption()
        .let('info', getCurrentLayerInfo())
        .let('layer', ({ info }) =>
            getLayerData(info.metadata.uniqueResourceIdentifier).getOrElse(none)
        )
        .let('feature', ({ layer }) =>
            fromNullable(layer.features.find(f => f.id === row.from))
        )
        .map(({ feature }) => {
            setCurrentFeature(feature);
            setLayout(AppLayout.MapAndTableAndFeature);
        });

const base = baseTable(layerTableQueries, layerTableEvents);

const render = base({
    className: 'attr-select-wrapper',
    toolbar,
    onRowSelect,
});

export default render;

logger('loaded');
