/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { fromNullable, fromPredicate } from 'fp-ts/lib/Option';
import { Extent } from 'ol/extent';

import { dispatch, observe, dispatchK, query, assign } from 'sdi/shape';
import {
    Feature,
    IMapInfo,
    MessageRecord,
    ILayerInfo,
    getMessageRecord,
    fetchIO,
    StreamingMetaIO,
    Inspire,
    FieldsDescriptorList,
} from 'sdi/source';
import { removeLayerAll, addLayer, addFeaturesToLayer } from 'sdi/map';
import { getApiUrl, getRootUrl, getUserId } from 'sdi/app';
import { iife, scopeOption } from 'sdi/lib';
import { activity } from 'sdi/activity';
import {
    addStream,
    clearStreams,
    mapStream,
    pushStreamExtent,
} from 'sdi/geodata-stream';

import { AppLayout } from '../shape/types';
import {
    fetchLayer,
    fetchAlias,
    fetchAllMaps,
    fetchCategories,
    fetchDatasetMetadata,
    fetchMap,
    fetchAttachment,
    fetchBaseLayer,
    fetchBaseLayerAll,
    fetchLinks,
    fetchUser,
} from '../remote';
// import { addAppIdToFeature } from '../util/app';
import {
    getSyntheticLayerInfo,
    getLayerData,
    mapReady,
    getMapInfo,
    getCurrentMap,
    getLayerDataFromInfo,
    getDatasetMetadataOption,
    getMapOption,
    getLayout,
} from '../queries/app';
import { addBookmarksToMap } from './bookmark';
import { mapName } from '../components/map';
import { getMapExtent, getView } from '../queries/map';
import { isNotNullNorUndefined } from 'sdi/util';
import { updateLoadingAddOne, updateLoadingRemoveOne, viewEvents } from './map';
import { navigateMapAction } from 'sdi/activity';
import { withoutBookmarks } from '../queries/bookmark';
import { addHarvestToMap } from './harvest';

const logger = debug('sdi:events/app');
export const activityLogger = activity('view');

// lets keep it around for next time we have an issue
// that doesn't show when dev tools are opened
// const notice = (() => {
//     const box = document.createElement('div');
//     box.id = 'notice';
//     box.setAttribute(
//         'style',
//         'position:absolute;z-index:999999;width:500px;height:500px;border:1px solid green;right:0;bottom:30px;overflow-y: auto;font-family: monospace; font-size: 10px;'
//     );
//     const attach = () => {
//         if (document.body) {
//             document.body.appendChild(box);
//         } else {
//             window.setTimeout(attach, 300);
//         }
//     };
//     attach();

//     return (msg: string) => {
//         const e = document.createElement('div');
//         e.setAttribute('style', 'border-bottom:1px solid grey;padding:4px');
//         e.appendChild(document.createTextNode(msg));
//         box.appendChild(e);
//     };
// })();

observe('data/maps', () =>
    fromNullable(getMapInfo()).map(info =>
        info.attachments.forEach(aid =>
            fetchAttachment(getApiUrl(`attachments/${aid}`)).then(a =>
                attachments(s => s.concat([a]))
            )
        )
    )
);

observe('port/map/view', view => {
    fromNullable(view.extent).map(e => {
        mapStream(e, ({ uri, lid }, extent) => {
            loadLayerDataExtent(lid, uri, extent);
        });
    });
});

const layerInRange = (info: ILayerInfo) => {
    const low = fromNullable(info.minZoom).getOrElse(0);
    const high = fromNullable(info.maxZoom).getOrElse(30);
    const zoom = getView().zoom;
    return info.visible && zoom > low && zoom < high;
};

const whenInRange = fromPredicate(layerInRange);

export const loadUser = () =>
    getUserId().map(userId =>
        fetchUser(getApiUrl(`users/${userId}`)).then(user =>
            assign('data/user', user)
        )
    );

const { markVisibilityPending, delVisibilityPending, isVisibilityPending } =
    iife(() => {
        const register: Set<string> = new Set();

        const hash = (a: string, b: string) => `${a}/${b}`;
        const markVisibilityPending = (mid: string, lid: string) =>
            register.add(hash(mid, lid));

        const delVisibilityPending = (mid: string, lid: string) =>
            register.delete(hash(mid, lid));

        const isVisibilityPending = (mid: string, lid: string) =>
            register.has(hash(mid, lid));

        return {
            markVisibilityPending,
            delVisibilityPending,
            isVisibilityPending,
        };
    });

const prepareStreamedLayer = (
    mapInfo: IMapInfo,
    layerInfo: ILayerInfo,
    md: Inspire,
    fields: FieldsDescriptorList
) => {
    dispatch('data/layers', state => {
        if (!(md.uniqueResourceIdentifier in state)) {
            state[md.uniqueResourceIdentifier] = {
                type: 'FeatureCollection',
                fields,
                features: [],
            };
        }
        return state;
    });
    if (layerInfo.visible && layerInRange(layerInfo)) {
        fromNullable(getView().extent).map(e => {
            loadLayerDataExtent(layerInfo.id, md.uniqueResourceIdentifier, e);
        });
    } else {
        markVisibilityPending(mapInfo.id, layerInfo.id);
    }
};

const loadLayer = (mapInfo: IMapInfo) => (layerInfo: ILayerInfo) =>
    fetchDatasetMetadata(getApiUrl(`metadatas/${layerInfo.metadataId}`))
        .then(md => {
            dispatch('data/datasetMetadata', state => {
                state[md.id] = md;
                return state;
            });

            addLayer(
                mapName,
                () => getSyntheticLayerInfo(layerInfo.id),
                () => getLayerData(md.uniqueResourceIdentifier)
            );

            if (isNotNullNorUndefined(md.dataStreamUrl)) {
                addStream({
                    uri: md.uniqueResourceIdentifier,
                    lid: layerInfo.id,
                });
                return fetchIO(StreamingMetaIO, md.dataStreamUrl)
                    .then(({ fields }) => {
                        prepareStreamedLayer(mapInfo, layerInfo, md, fields);
                    })
                    .catch(() => {
                        logger(
                            `[ERROR] failed to get fields for ${md.uniqueResourceIdentifier}`
                        );
                        prepareStreamedLayer(mapInfo, layerInfo, md, []);
                    });
            } else {
                return loadLayerData(md.uniqueResourceIdentifier);
            }
        })
        .catch(err =>
            logger(`Failed to load MD ${layerInfo.metadataId}: ${err}`)
        );

const loadLayerDataExtent = (layerId: string, url: string, bbox: Extent) =>
    getSyntheticLayerInfo(layerId).map(({ info }) => {
        whenInRange(info).map(info => {
            const title = getDatasetMetadataOption(info.metadataId).map(md =>
                getMessageRecord(md.resourceTitle)
            );
            pushStreamExtent(bbox, { lid: layerId, uri: url });
            title.map(updateLoadingAddOne);
            fetchLayer(
                `${url}?bbox=${bbox[0]},${bbox[1]},${bbox[2]},${bbox[3]}`
            )
                .then(layer => {
                    if (layer.features !== null) {
                        logger(
                            'addFeaturesToLayer',
                            title.getOrElse({ fr: info.metadataId }).fr,
                            layer.features.length
                        );

                        addFeaturesToLayer(mapName, info, layer.features);
                        dispatch('data/layers', state => {
                            if (url in state) {
                                state[url].features = state[
                                    url
                                ].features.concat(layer.features);
                            } else {
                                state[url] = layer;
                            }
                            return state;
                        });
                    }
                    title.map(updateLoadingRemoveOne);
                })
                .catch(err => {
                    title.map(updateLoadingRemoveOne);
                    logger(`Failed to load features at ${url} due to ${err}`);
                    dispatch('remote/errors', state => ({
                        ...state,
                        [url]: `${err}`,
                    }));
                });
        });
    });

const loadLayerData = (url: string) => {
    logger(`loadLayerData(${url})`);
    return fetchLayer(url)
        .then(layer => {
            dispatch('data/layers', state => {
                logger(`Put layer ${url} on state`);
                state[url] = layer;
                return state;
            });
        })
        .catch(err => {
            logger(`Failed to load layer at ${url} due to ${err}`);
            dispatch('remote/errors', state => ({ ...state, [url]: `${err}` }));
        });
};

const loadMapFromInfo = (info: IMapInfo, delay?: number) => {
    const mapIsReady = mapReady();
    logger(`loadMap ${info.id} ${mapIsReady} ${delay}`);
    activityLogger(navigateMapAction(info.id, info.title));
    if (mapIsReady) {
        removeLayerAll(mapName);
        const load = loadLayer(info);
        // withoutBookmarks(info.layers).reverse().map(load);
        withoutBookmarks(info.layers).map(load);
        // .reduce(
        //     (acc, layer) => acc.then(() => load(layer)),
        //     Promise.resolve([])
        // );
        addBookmarksToMap();
        addHarvestToMap();
    } else {
        const d = delay !== undefined ? 2 * delay : 2;
        setTimeout(() => loadMapFromInfo(info, d), d);
    }
};

const findMap = (mid: string) => getMapOption(mid);

const attachments = dispatchK('data/attachments');

const loadLinks = (mid: string) =>
    fetchLinks(getApiUrl(`map/links?mid=${mid}`)).then(links => {
        dispatch('data/links', data => ({ ...data, [mid]: links }));
    });

export const setLayout = (l: AppLayout) => {
    logger(`setLayout ${l}`);
    // dispatch('app/layout', state => state.concat([l]));
    assign('app/layout', l);
};

export const signalReadyMap = () => dispatch('app/map-ready', () => true);

export const loadMap = () =>
    fromNullable(getCurrentMap()).map(mid =>
        findMap(mid).foldL(
            () => {
                fetchMap(getApiUrl(`maps/${mid}`))
                    .then(info => {
                        dispatch('data/maps', maps => maps.concat([info]));
                        loadMapFromInfo(info);
                    })
                    .then(() => loadLinks(mid))
                    .catch(response => {
                        const user = query('app/user');

                        if (response.status === 403 && !user) {
                            window.location.assign(
                                getRootUrl(`login/view/${mid}`)
                            );
                            return;
                        }
                    });
            },
            info => {
                loadLinks(mid);
                loadMapFromInfo(info);
            }
        )
    );

export const loadBaseLayer = (id: string, url: string) => {
    fetchBaseLayer(url).then(bl => {
        dispatch('data/baselayers', state => ({ ...state, [id]: bl }));
    });
};

export const loadAllBaseLayers = (url: string) => {
    fetchBaseLayerAll(url).then(blc => {
        dispatch('data/baselayers', () => blc);
    });
};

export const loadAllMaps = () =>
    fetchAllMaps(getApiUrl(`maps`)).then(maps => {
        assign('data/maps', maps);
        if (getLayout() === AppLayout.Splash) {
            assign('app/layout', AppLayout.MapNavigatorFS);
        }
    });

export const loadAlias = (url: string) => {
    fetchAlias(url).then(alias => {
        dispatch('data/alias', () => alias);
    });
};

export const loadCategories = (url: string) => {
    fetchCategories(url).then(categories => {
        dispatch('data/categories', () => categories);
    });
};

export const updateCurrentMapInPlace = (f: (m: IMapInfo) => void) =>
    fromNullable(getCurrentMap()).map(mid =>
        dispatch('data/maps', maps => {
            fromNullable(maps.find(m => m.id === mid)).map(f);
            return maps;
        })
    );

export const setLayerVisibility = (id: string, visible: boolean) =>
    updateCurrentMapInPlace(mapInfo => {
        mapInfo.layers.forEach(l => {
            if (l.id === id) {
                l.visible = visible;
                if (isVisibilityPending(mapInfo.id, l.id)) {
                    getMapExtent().map(e =>
                        mapStream(e, ({ uri, lid }, extent) =>
                            loadLayerDataExtent(lid, uri, extent)
                        )
                    );
                    delVisibilityPending(mapInfo.id, l.id);
                } else if (!visible) {
                    markVisibilityPending(mapInfo.id, l.id);
                }
            }
        });
    });

export const saveStyle = (layerInfo: ILayerInfo) => {
    const mid = getCurrentMap();
    dispatch('data/maps', maps => {
        const idx = maps.findIndex(m => m.id === mid);
        if (idx !== -1) {
            const m = maps[idx];
            const layerIndex = m.layers.findIndex(
                l => l.id === layerInfo.metadataId
            );
            if (layerIndex >= 0) {
                m.layers[layerIndex].style = layerInfo.style;
            }
            viewEvents.updateMapView({ dirty: 'style' });
        }
        return maps;
    });
};

export const setMapBaseLayer = (id: string) => {
    const mid = getCurrentMap();
    dispatch('data/maps', maps => {
        const idx = maps.findIndex(m => m.id === mid);

        if (idx !== -1) {
            const m = maps[idx];
            m.baseLayer = id;
        }

        return maps;
    });
};

export const setCurrentLayer = (id: string) => {
    dispatch('app/current-layer', () => id);
    unsetCurrentFeature();
    dispatch('component/table', state => ({
        ...state,
        selected: -1,
        loaded: 'none',
    }));
};

export const selectFeature = (feature: Feature) =>
    assign('app/current-feature', feature);

export const unsetCurrentFeature = () =>
    dispatch('app/current-feature', () => null);

const {
    clearCurrentFeatureById,
    retryCurrentFeatureById,
    checkCurrentFeatureById,
} = iife(() => {
    const maxTry = 1000;
    let timeout: number | null = null;
    let lid: string | null = null;
    let fid: string | number | null = null;
    const clearCurrentFeatureById = () => {
        if (timeout !== null) {
            clearTimeout(timeout);
            timeout = null;
        }
    };
    const retryCurrentFeatureById = (
        layerInfoId: string,
        id: string | number,
        tries: number
    ) => {
        clearCurrentFeatureById();
        lid = layerInfoId;
        fid = id;
        timeout = window.setTimeout(() => {
            setCurrentFeatureById(layerInfoId, id, tries);
        }, 100);
    };
    const checkCurrentFeatureById = (
        layerInfoId: string,
        id: string | number,
        tries: number
    ) =>
        timeout === null ||
        (layerInfoId === lid && id === fid && tries < maxTry);

    return {
        clearCurrentFeatureById,
        retryCurrentFeatureById,
        checkCurrentFeatureById,
    };
});

export const setCurrentFeatureById = (
    layerInfoId: string,
    id: string | number,
    tries = 0
) => {
    if (checkCurrentFeatureById(layerInfoId, id, tries)) {
        scopeOption()
            .let('data', getLayerDataFromInfo(layerInfoId))
            .let('feature', s =>
                fromNullable(s.data.features.find(f => f.id === id))
            )
            .map(({ feature }) => {
                clearCurrentFeatureById();
                dispatch('app/current-layer', () => layerInfoId);
                dispatch('app/current-feature', () => feature);
            })
            .getOrElseL(() =>
                retryCurrentFeatureById(layerInfoId, id, tries + 1)
            );
    } else {
        clearCurrentFeatureById();
    }
};

// export const selectFeatureList = (data: Feature[]) =>
//     assign('app/selected-features', data);
// export const clearFeatures = () => {
//     clearSelectedRow();
//     assign('app/selected-feature', []);
// };

export const pushFeaturePathToSelection = (
    layerInfoId: string,
    id: string | number
) =>
    scopeOption()
        .let('data', getLayerDataFromInfo(layerInfoId))
        .let('feature', s =>
            fromNullable(s.data.features.find(f => f.id === id))
        )
        .map(({ feature }) => {
            assign('app/current-layer', layerInfoId);
            selectFeature(feature);
        });

// export const pushFeaturePathToSelection = (
//     layerInfoId: string,
//     id: string | number
// ) =>
//     scopeOption()
//         .let('data', getLayerDataFromInfo(layerInfoId))
//         .let('feature', s =>
//             fromNullable(s.data.features.find(f => f.id === id))
//         )
//         .map(({ feature }) => {
//             assign('app/current-layer', layerInfoId);
//             dispatch('app/selected-features', fs =>
//                 fs
//                     .filter(f => f.feature.id !== feature.id)
//                     .concat({ feature, layerId: layerInfoId })
//             );
//         });

export const clearMap = () => {
    assign('app/current-map', null);
    assign('app/current-layer', null);
    unsetCurrentFeature();
    dispatch('component/table', state => {
        state.selected = -1;
        return state;
    });
    clearStreams();
};

export const setPrintTitle = (customTitle: MessageRecord) =>
    dispatch('component/print', s => ({ ...s, customTitle }));

export const resetPrintTitle = () =>
    dispatch('component/print', s => ({ ...s, customTitle: null }));

export const loadMetadata = (mid: string) =>
    fetchDatasetMetadata(getApiUrl(`metadatas/${mid}`))
        .then(md =>
            dispatch('data/datasetMetadata', state => {
                state[md.id] = md;
                return state;
            })
        )
        .catch(err => logger(`Failed to load MD ${mid}: ${err}`));

export const selectTableView = () => assign('app/table/selected', true);
export const deselectTableView = () => assign('app/table/selected', false);

logger('loaded');
