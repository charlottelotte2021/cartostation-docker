/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {
    fromNullable,
    Option,
    fromPredicate,
    none,
    some,
} from 'fp-ts/lib/Option';
import { left, right, Either } from 'fp-ts/lib/Either';

import { query } from 'sdi/shape';
import {
    getMessageRecord,
    IMapInfo,
    FeatureCollection,
    ILayerInfo,
    Feature,
} from 'sdi/source';
import { SyntheticLayerInfo } from 'sdi/app';
import { scopeOption } from 'sdi/lib';
import { fromRecord } from 'sdi/locale';
import { withoutBookmarks } from './bookmark';

export const mapReady = () => {
    return query('app/map-ready');
};

export const getLayout = () => query('app/layout');
// {
//     const ll = query('app/layout');
//     if (ll.length === 0) {
//         throw new Error('PoppingEmptyLayoutList');
//     }
//     return ll[ll.length - 1];
// };

export const getLayerData = (
    ressourceId: string
): Either<string, Option<FeatureCollection>> => {
    const errors = query('remote/errors');
    if (ressourceId in errors) {
        return left(errors[ressourceId]);
    }
    return right(getLayerDataFromUri(ressourceId));
};

export const getLayerDataFromInfo = (
    layerId: string
): Option<FeatureCollection> =>
    scopeOption()
        .let('mid', fromNullable(query('app/current-map')))
        .let('mapInfo', ({ mid }) =>
            fromNullable(query('data/maps').find(m => m.id === mid))
        )
        .let('layerInfo', ({ mapInfo }) =>
            fromNullable(mapInfo.layers.find(l => l.id === layerId))
        )
        .let('metadata', ({ layerInfo }) =>
            getDatasetMetadataOption(layerInfo.metadataId)
        )
        .let('data', ({ metadata }) =>
            getLayerDataFromUri(metadata.uniqueResourceIdentifier)
        )
        .pick('data');

export const getLayerDataFromUri = (uri: string) =>
    fromNullable(query('data/layers')[uri]);

export const getMapOption = (mid: string) => {
    const maps = query('data/maps');
    return fromNullable(maps.find(m => m.id === mid));
};

export const getDatasetMetadata = (id: string) => {
    const collection = query('data/datasetMetadata');
    if (id in collection) {
        return collection[id];
    }
    return null;
};

export const getSyntheticLayerInfoOption = (
    layerId: string
): Option<SyntheticLayerInfo> =>
    scopeOption()
        .let('mid', fromNullable(query('app/current-map')))
        .let('info', ({ mid }) =>
            fromNullable(query('data/maps').find(m => m.id === mid))
        )
        .let('layerInfo', ({ info }) =>
            fromNullable(info.layers.find(l => l.id === layerId))
        )
        .let('metadata', ({ layerInfo }) =>
            getDatasetMetadataOption(layerInfo.metadataId)
        )
        .map(({ layerInfo, metadata }) => ({
            name: getMessageRecord(metadata.resourceTitle),
            info: layerInfo,
            metadata,
        }));

export const getSyntheticLayerInfo = getSyntheticLayerInfoOption;

export const getCurrentMap = () => query('app/current-map');

export const getCurrentLayer = () => query('app/current-layer');

export const getCurrentLayerOpt = () => fromNullable(getCurrentLayer());

export const getCurrentLayerInfoOption = () =>
    fromNullable(query('app/current-layer')).chain(getSyntheticLayerInfoOption);

export const getCurrentLayerInfo = getCurrentLayerInfoOption;

export const getFirstLayer = () =>
    getMapInfoOption()
        .map(mapInfo =>
            withoutBookmarks(
                mapInfo.layers.filter(layer => layer.visibleLegend)
            )
        )
        .chain(l => (l.length > 0 ? some(l[l.length - 1]) : none));

export const getCurrentMetadata = () =>
    getCurrentLayerInfo().map(i => i.metadata);
export const getCurrentInfo = () =>
    getCurrentLayerInfo().map<ILayerInfo>(i => i.info);
export const getCurrentName = () =>
    getCurrentLayerInfo().map<string>(i => fromRecord(i.name));

// const withFeatures = fromPredicate<Feature[] | Readonly<Feature[]>>(features =>
//     features.length > 0 ? true : false
// );
// export const getSelectedFeatures = (): Option<
// Readonly<Feature[]> | Feature[]
// > => withFeatures(query('app/selected-features'));

export const getCurrentFeature = (): Option<Feature> =>
    fromNullable(query('app/current-feature'));

// export const isSelectedFeature = (f: Feature) =>
//     getSelectedFeatures()
//         .map(list => list.indexOf(f) > -1)
//         .getOrElse(false);

// export const noSelectedFeatures = () =>
//     getSelectedFeatures().getOrElse([]).length < 1;

export const getCurrentBaseLayerName = () => {
    const mid = query('app/current-map');
    const map = query('data/maps').find(m => m.id === mid);
    if (map) {
        return map.baseLayer;
    }
    return null;
};

export const gteBaseLayer = (id: string | null) => {
    const [serviceId, layerName] = fromNullable(id)
        .map(i => i.split('/'))
        .getOrElse(['', '']);
    const service = query('data/baselayers').find(s => s.id === serviceId);
    if (service) {
        return service.layers.find(l => l.codename === layerName) ?? null;
    }
    return null;
};

export const getCurrentBaseLayer = () => {
    const name = getCurrentBaseLayerName();
    return gteBaseLayer(name);
};

export const getBaseLayerServices = () =>
    query('data/baselayers').map(s => s.id);

export const getBaseLayersForService = (serviceId: string) =>
    fromNullable(query('data/baselayers').find(s => s.id === serviceId))
        .map(s => s.layers)
        .getOrElse([]);

export const getMapInfo = () => {
    const mid = query('app/current-map');
    const info = query('data/maps').find(m => m.id === mid);
    return info !== undefined ? info : null;
};

export const getMapInfoOption = () => fromNullable(getMapInfo());

export const getDatasetMetadataOption = (id: string) =>
    fromNullable(getDatasetMetadata(id));

export const hasPrintTitle = () =>
    null === query('component/print').customTitle;

export const getPrintTitle = (info: IMapInfo) =>
    fromNullable(query('component/print').customTitle).fold(info.title, s => s);

export const isTableSelected = () =>
    fromPredicate<boolean>(v => v)(query('app/table/selected'));
