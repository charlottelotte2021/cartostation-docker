/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Coordinate } from 'ol/coordinate';

import { IMapBaseLayer } from 'sdi/source';
import { Foldable } from 'sdi/util';

export enum AppLayout {
    Splash,
    MapFS,
    MapAndInfo,
    MapAndFeature,
    MapAndExtract,
    MapNavigatorFS,
    MapAndTracker,
    MapAndMeasure,
    Print,
    Query,
    QueryPrint,
}

export enum SortDirection {
    ascending,
    descending,
}

export type TableDataKey = string;

export interface IMapNavigator {
    query: string;
}

export type IMenuData = Foldable;

export interface IToolWebServices extends Foldable {
    url: string;
    layers: IMapBaseLayer[];
}

export type LegendPage =
    | 'base-map'
    | 'data'
    | 'info'
    | 'locate'
    | 'measure'
    | 'print'
    | 'share'
    | 'spatial-filter';

export interface ILegend {
    currentPage: LegendPage;
}

export interface TrackerCoordinate {
    coord: Coordinate;
    accuracy: number;
}

export interface IGeoTracker {
    track: TrackerCoordinate[];
    active: boolean;
}

export interface IPositioner {
    point: {
        latitude: number;
        longitude: number;
    };
}

export interface IGeoMeasure {
    active: boolean;
    geometryType: 'Polygon' | 'LineString';
    coordinates: Coordinate[];
}

export interface IShare {
    withView: boolean;
}
