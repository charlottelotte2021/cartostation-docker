/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {
    Attachment,
    Category,
    Feature,
    FeatureCollection,
    IMapInfo,
    Inspire,
    MapLink,
    MessageRecord,
    GeometryObject,
    GeometryType,
    StreamingState,
    IUser,
    IServiceBaseLayers,
} from 'sdi/source';
import { TableState } from 'sdi/components/table2';
import { IDataTable } from 'sdi/components/table';
import {
    ITimeserieInteractive,
    ITimeserieCollection,
} from 'sdi/components/timeserie';
import { ButtonComponent } from 'sdi/components/button';
import {
    IMapViewData,
    IMapScale,
    Interaction,
    PrintRequest,
    PrintResponse,
} from 'sdi/map';
import { Collection, Nullable } from 'sdi/util';

import {
    AppLayout,
    ILegend,
    IMenuData,
    IMapNavigator,
    IToolWebServices,
    IPositioner,
    IShare,
} from './types';
import { PrintProps, PrintState } from '../components/print';
import { HarvestedLayer } from 'view/src/components/query';
import { MiniStep } from 'sdi/map/mini';
import { IToolGeocoder } from 'sdi/ports/geocoder';

// export interface FeatureSelection {
//     feature: Feature;
//     layerId: string;
// }

declare module 'sdi/shape' {
    export interface IShape {
        'app/layout': AppLayout;
        'app/current-map': string | null;
        'app/current-layer': string | null;
        'app/current-feature': Feature | null;
        'app/table/selected': boolean;
        // 'app/selected-features': FeatureSelection[];
        'app/map-ready': boolean;
        'app/route': string[];

        'component/legend': ILegend;
        'component/menu': IMenuData;
        'component/table': TableState;
        'component/table/extract': IDataTable;
        'component/mapnavigator': IMapNavigator;
        'component/timeserie': Collection<ITimeserieInteractive>;
        'component/legend/expand-image': boolean;
        'component/legend/show-wms-legend': boolean;
        'component/legend/webservices': IToolWebServices;
        'component/legend/geocoder': IToolGeocoder;
        'component/legend/positioner': IPositioner;
        'component/legend/share': IShare;
        'component/button': ButtonComponent;
        'component/print': PrintState;
        'component/bookmark/current-index': Nullable<number>;

        'component/harvest/geometry': Nullable<GeometryObject>;
        'component/harvest/geometry-type': Nullable<GeometryType>;
        'component/harvest/results': HarvestedLayer[];
        'component/harvest/minimap': Collection<MiniStep>;

        'port/map/view': IMapViewData;
        'port/map/scale': IMapScale;
        'port/map/interaction': Interaction;
        'port/map/loading': MessageRecord[];
        'port/map/printRequest': PrintRequest<PrintProps | null>;
        'port/map/printResponse': PrintResponse<PrintProps | null>;

        'data/layers': Collection<FeatureCollection>;
        'data/maps': IMapInfo[];
        'data/timeseries': ITimeserieCollection;
        'data/categories': Category[];
        'data/datasetMetadata': Collection<Inspire>;
        'data/attachments': Attachment[];
        'data/baselayers': IServiceBaseLayers;
        'data/links': Collection<MapLink[]>;
        'remote/errors': Collection<string>;

        'data/user': IUser | null;
        'data/features/stream': StreamingState;
    }
}
