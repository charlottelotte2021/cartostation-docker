_view_ presents the maps created in the [_compose_](compose) application.

_view_ is commonly called _atlas_.

In addition to the classic functions of map navigation, access to the map legend and editorial content, a series of tools are available to refine the consultation of the data.

## User guide

The complete user guide is available here : https://cartostation.com/documentation
