import { deselectAppList, getAppListStatus, getApps, selectAppList } from '../app';
import { appName, appDisplayName, appUrl } from '../source';
import tr, { fromRecord } from '../locale';
import { DIV, SPAN, A, BUTTON, NODISPLAY } from './elements';
// import { nameToString } from './button/names';


export const renderAppSelect = (
    current: string,
) => {
    const apps = getApps();
    const tail = apps
        .filter(a => appName(a) !== current)
        .map(a =>
            appDisplayName(a).map(name =>
                A(
                    { className: `app-item ${appName(a)}`, href: appUrl(a)  },
                    DIV(
                        { className: 'app-name', },
                        SPAN({ className: 'app-picto' }),
                        SPAN({className: 'app-name',}, fromRecord(name)),
                    )
                )
            )
        );

    const head = DIV(
        { className: 'selected head' },
        BUTTON(
            {
                className: 'head-label',
                "aria-haspopup": "listbox",
                "aria-expanded": getAppListStatus(),
                onClick: () => getAppListStatus() ? deselectAppList() : selectAppList(),
            },
            tr.core('applications')
        )
    );

    return DIV(
        {
            className: 'app-select__wrapper ',
            onClick: e => e.currentTarget.classList.toggle('active')
        },
        DIV(
            { className: 'app-select' },
            head,
            getAppListStatus() ? DIV({ className: 'tail' }, ...tail) : NODISPLAY()
        )
    );
};
