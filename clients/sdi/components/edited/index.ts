
import { DIV } from '../elements';
import { query } from '../../shape';
import { fromNullable } from 'fp-ts/lib/Option';
import { fromRecord, Translated } from '../../locale';
import { nameToString } from '../button/names';

export interface EditedModalOptions { }

const defaultModalOptions: EditedModalOptions = {};



export const renderEditedModal =
    (_defaultText: Translated, _options = defaultModalOptions) =>
        DIV({
            style: {
                fontFamily: 'ForkAwesome',
            },
        }, nameToString('info'));




const getEdited =
    (name: string) =>
        fromNullable(query('data/component/edited')[name]);


export const renderEdited =
    (name: string, defaultText: Translated) =>
        DIV({ className: 'edited-box' },
            getEdited(name).fold(defaultText, fromRecord),
        );

export default renderEdited;





