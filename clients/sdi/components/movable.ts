import tr from '../locale';
import { makeIcon } from './button';
import { DIV, NodeOrOptional, NODISPLAY } from './elements';

const moveUpButton = makeIcon('move-up', 3, 'arrow-up', {
    position: 'left',
    text: () => tr.core('move-up'),
});
const moveDownButton = makeIcon('move-down', 3, 'arrow-down', {
    position: 'left',
    text: () => tr.core('move-down'),
});

const moveUpButtonDisabled = () => moveUpButton(() => {}, 'disabled');
const moveDownButtonDisabled = () => moveDownButton(() => {}, 'disabled');

const wrap = (...nodes: NodeOrOptional[]) => DIV('actions order-box', ...nodes);

export const renderMovable =
    (moveUp: (n: number) => void, moveDown: (n: number) => void) =>
    (index: number, length: number) => {
        if (length === 1) {
            return NODISPLAY();
        } else if (index === 0) {
            return wrap(
                moveUpButtonDisabled(),
                moveDownButton(() => moveDown(index))
            );
        } else if (index === length - 1) {
            return wrap(
                moveUpButton(() => moveUp(index)),
                moveDownButtonDisabled()
            );
        } else {
            return wrap(
                moveUpButton(() => moveUp(index)),
                moveDownButton(() => moveDown(index))
            );
        }
    };

export default renderMovable;
