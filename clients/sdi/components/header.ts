/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { DIV, A, NodeOrOptional, HEADER } from './elements';
import langSwitch from './lang-switch';
import {
    getUserId,
    getRoot,
    getLang,
    getAppManifest,
    getRelativeRoot,
} from '../app';
import { tr, fromRecord } from '../locale';
import { getPathElements } from '../util';
import { renderAppSelect } from './app-select';
import { appDisplayName } from '../source';
import { none } from 'fp-ts/lib/Option';

const logger = debug('sdi:header');

export const loginURL = () => {
    const path = getPathElements(document.location.pathname);
    const root = getPathElements(getRelativeRoot());
    const next = path
        .filter((p, i) => (i < root.length ? p !== root[i] : true))
        .join('/');
    return `${getRoot()}login/${next}`;
};

// const dashboardButton =
//     () => DIV({
//         className: 'navigate dashboard',
//         onClick: () => navigateRoot(),
//     }, SPAN({ className: 'label' }, tr.core('dashboard')));

const rootButton = (current: string) =>
    getUserId().fold(
        DIV(
            { className: 'navigate login' },
            A({ href: loginURL() }, tr.core('login'))
        ),
        () => renderAppSelect(current)
    );

const documentationLink = () =>
    getUserId().map(() =>
        DIV(
            { className: 'navigate documentation' },
            A(
                {
                    href: `https://cartostation.com/documentation/${getLang()}/index`,
                    target: '_blank',
                },
                tr.core('help')
            )
        )
    );

const renderTitle = (appCodename: string) =>
    getAppManifest(appCodename)
        .chain(appDisplayName)
        .map<string>(n => fromRecord(n))
        .getOrElse(appCodename);

const appElementDefault = (): NodeOrOptional => none;

export const header = (appCodename: string, appElement = appElementDefault) =>
    HEADER(
        { className: 'header' },
        A(
            {
                className: 'logo-link',
                href: getRoot(),
                'aria-label': renderTitle('dashboard'),
            },
            DIV({ className: 'brand-logo' }, DIV({ className: 'brand-name' }))
        ),
        DIV({ className: 'app-title' }, renderTitle(appCodename)),
        DIV({ className: 'app-listwrapper' }, appElement()),
        DIV(
            { className: 'header-toolbar' },
            rootButton(appCodename),
            documentationLink(),
            langSwitch(appCodename)
        )
    );

export default header;

logger('loaded');
