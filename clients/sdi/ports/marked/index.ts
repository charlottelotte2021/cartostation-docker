import { Marked } from './marked';
import { DIV } from '../../components/elements';


export const markdown =
    (source: string, className = 'md') =>
        DIV({ className }, ...(Marked.parse(source)))
