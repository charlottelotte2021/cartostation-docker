import * as io from 'io-ts';
import { i } from '../io';
import { BaseActivityIO } from './base';

// save-units action
export const SaveUnitsDataIO = i({
    action: io.literal('save-units'),
    parameter: i({
        projectId: io.number,
        numberOfEdits: io.number,
    }),
});
export type SaveUnitsData = io.TypeOf<typeof SaveUnitsDataIO>;

export const SaveUnitsIO = io.intersection(
    [BaseActivityIO, SaveUnitsDataIO],
    'SaveUnitsIO'
);

export type SaveUnits = io.TypeOf<typeof SaveUnitsIO>;
