import { u, i, a, TypeOf } from '../io';
import {
    CheckboxDataIO,
    ClickedCheckboxIO,
    ExportActionIO,
    ExportDataIO,
    LangDataIO,
    LinkDataIO,
    LinkIO,
} from './base';
import {
    NavigateMapDataIO,
    NavigateMapIO,
    PrintMapDataIO,
    PrintMapIO,
} from './action-map';
import {
    NavigateSolarContactDataIO,
    NavigateSolarContactIO,
    PrintReportDataIO,
    PrintReportIO,
} from './action-solar';
import * as io from 'io-ts';
import {
    LangActionIO,
    NavigateCapakeyDataIO,
    NavigateCapakeyIO,
    NavigateDetailDataIO,
    NavigateDetailIO,
    VisitDataIO,
    VisitIO,
} from '.';
import { NavigateQueryDataIO, NavigateQueryIO } from './action-query';
import { SaveUnitsDataIO, SaveUnitsIO } from './action-project';
import { SelectWaterLevelDataIO, SelectWaterLevelIO } from './action-timeserie';

export const ActivityActionDataIO = u([
    VisitDataIO,
    CheckboxDataIO,
    NavigateMapDataIO,
    PrintMapDataIO,
    LinkDataIO,
    PrintReportDataIO,
    NavigateDetailDataIO,
    NavigateSolarContactDataIO,
    NavigateCapakeyDataIO,
    NavigateQueryDataIO,
    LangDataIO,
    SaveUnitsDataIO,
    SelectWaterLevelDataIO,
    ExportDataIO,
]);
export type ActivityActionData = io.TypeOf<typeof ActivityActionDataIO>;

export type ActivityActionName = ActivityActionData['action'];
export const ActivityActionNameIO = io.union([
    io.literal('visit'),
    io.literal('link'),
    io.literal('navigate-capakey'),
    io.literal('navigate-detail'),
    io.literal('navigate-solar-contact'),
    io.literal('navigate-map'),
    io.literal('print-map'),
    io.literal('print-report'),
    io.literal('navigate-query'),
    io.literal('lang-choice'),
    io.literal('save-units'),
    io.literal('select-water-level'),
    io.literal('export'),
    io.literal('checked'),
]); // TODO: get this better

export const ActivityIO = u([
    VisitIO,
    NavigateMapIO,
    PrintMapIO,
    LinkIO,
    PrintReportIO,
    NavigateDetailIO,
    NavigateSolarContactIO,
    NavigateCapakeyIO,
    NavigateQueryIO,
    LangActionIO,
    SaveUnitsIO,
    SelectWaterLevelIO,
    ExportActionIO,
    ClickedCheckboxIO,
]);
export type Activity = io.TypeOf<typeof ActivityIO>;

export const ActivityResultIO = i({
    count: io.number,
    data: a(ActivityIO),
});
export type ActivityResult = TypeOf<typeof ActivityResultIO>;
