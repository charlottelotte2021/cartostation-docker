# Layer style definition

A layer style is a rather complex data structure covering a matrix of geometry types and interactions with features attributes. It drives the display of geographic features on map as well as the layer representation in the legend.


## `StyleConfig`

The root type is a union of interfaces covering the product of `Point`, `LineString` and `Polygon` geometry types with `SimpleteStyle`, `DiscreteStyle` and `ContinuousStyle` legend types.

Simple configurations are those which do not interact with features' attributes, displaying all features with the same style. 

Discrete configurations apply configured styles to features meeting an exact match of the property value with at least one of reference values.

Continuous configurations apply configured styles to features for which the property value is in their numerical range.

```typescript
export type StyleConfig =
    | PointStyleConfigSimple
    | PointStyleConfigDiscrete
    | PointStyleConfigContinuous 

    | LineStyleConfigSimple
    | LineStyleConfigDiscrete
    | LineStyleConfigContinuous
    
    | PolygonStyleConfigSimple
    | PolygonStyleConfigDiscrete
    | PolygonStyleConfigContinuous
```


### `LabelStyle`

`LabelStyle` is common to all types of styles. 
Except for `propName`, which selects a column, properties of this object
is a subset of OpenLayers [style.Text](https://openlayers.org/en/latest/apidoc/module-ol_style_Text-Text.html). Note that the color is the fill color, the stroke being constantly white to help readability.


```typescript
type LabelStyle = {
    propName: MessageRecord,
    align: 'left' | 'right' | 'center' | 'end' | 'start',
    baseline:  'alphabetic' | 'bottom' | 'top' | 'middle',
    resLimit: number,
    color: string,
    size: number,

    xOffset?: number,
    yOffset?: number,
}
```


## `PointStyleConfigSimple`

The `Marker` type holds most of the configuration here. `codePoint` is a unicode codepoint mapping a glyph in the marker font. A list of available symbols and their codepoints is present in the [names module](../../components/button/names.ts). The `color` is a CSS color string, and `size` is fixed font size. 

Even though both `marker` and `label` are optionals, it's expected to at least find one to be configured.

```typescript
type Marker = {
    codePoint: number;
    color: string;
    size: number;
}

export interface PointStyleConfigSimple {
    kind: 'point-simple';
    marker?: Marker;
    label?: LabelStyle;
} 
```


Example:
```json
{
    "kind": "point-simple",
    "label": {
        "size": 12,
        "align": "center",
        "color": "#000000",
        "yOffset": -12,
        "baseline": "bottom",
        "propName": {
            "fr": "title_fr",
            "en": "title_en",
            "nl": "title_nl"
        },
        "resLimit": 9
    },
    "marker": {
        "size": 10,
        "color": "hsl(247.20000000000005, 100%, 51.2%)",
        "codePoint": 61713
    }
}
```


## `PointStyleConfigDiscrete`

In this configuration, different styles are defined in `PointDiscreteGroup`s where `values` is a list of strings to match against, `label` is used for the legend entry and `marker` defined the marker style.

The input value comes from the feature's attribute pointed by `PointStyleConfigDiscrete.propName`.

```typescript
type PointDiscreteGroup = {
    values: string[];
    label: MessageRecord;
    marker: Marker;
}

export interface PointStyleConfigDiscrete {
    kind: 'point-discrete';
    label?: LabelStyle;
    propName: string;
    groups: PointDiscreteGroup[];
}
```


## `PointStyleConfigContinuousIO`

A continuous configuration where styles are grouped by intervals. A `PointInterval` object has `low` and `high` keys to filter input value. Filtering results from `(n) => n >= low && n < high`.

```typescript
type PointInterval = {
    low: number;
    high: number; 
    label: MessageRecord;
    marker: Marker;
}

export interface PointStyleConfigContinuous {
    kind: 'point-continuous';
    label?: LabelStyle;
    propName: string;
    intervals: PointInterval[];
}
```



## `LineStyleConfigSimple`

A simple configuration for `LineString` geometries.


```typescript
export interface LineStyleConfigSimple {
    kind: 'line-simple';
    label?: LabelStyle;
    strokeColor: string;
    strokeWidth: number;
    dash: number[];
} 
```

`strikeColor` is a CSS color string and `strokeWidth` is a pixel size for the width of the stroke. The  `dash` array is passed directly to *OpenLayers* [style.Stroke](https://openlayers.org/en/latest/apidoc/module-ol_style_Stroke-Stroke.html) which in turn passes it to [CanvasRenderingContext2D.setLineDash](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/setLineDash), where the documentation states:

> An Array of numbers that specify distances to alternately draw a line and a gap (in coordinate space units). If the number of elements in the array is odd, the elements of the array get copied and concatenated. For example, [5, 15, 25] will become [5, 15, 25, 5, 15, 25]. If the array is empty, the line dash list is cleared and line strokes return to being solid.

## `LineStyleConfigDiscrete`

A discrete style for lines. The selection process is identical to `PointStyleConfigDiscrete`, it's encoded in a `LineDiscreteGroup` objects along with `strokeColor`, `strokeWidth` and `dash`.

```typescript
type LineDiscreteGroup = {
    values: string[];
    label: MessageRecord;
    strokeColor: string;
    strokeWidth: number;
    dash: number[];
}

export interface LineStyleConfigDiscrete {
    kind: 'line-discrete';
    label?: LabelStyle;
    propName: string;
    groups: LineDiscreteGroup[];
}
```


## `LineStyleConfigContinuous`

A continuous style for lines. The selection is the same as for `PointStyleConfigContinuous`, encoded in a `LineInterval` object. For reasons, `LineInterval` *does not* have a `dash` key.

```typescript
type LineInterval = {
    low: number;
    high: number; 
    label: MessageRecord;
    strokeColor: string;
    strokeWidth: number;
}

export interface LineStyleConfigDiscrete {
    kind: 'line-continuous';
    label?: LabelStyle;
    propName: string;
    intervals: LineInterval[];
}
```


## `PolygonStyleConfigSimple`

A simple configuration for polygons.

```typescript
export interface PolygonStyleConfigSimple{
    kind: "polygon-simple";
    strokeColor: string;
    fillColor: string;
    strokeWidth: number;
    pattern: boolean;
    patternAngle: 0 | 45 | 90 | 135;
    patternColor?: string;
}
```

Depending of `pattern` being set, the interpretation is slightly different. Essentially re-using the `strokeWidth` and `fillColor` to draw a simple hatch pattern at angle `patternAngle`, and *not* drawing a stroke. The optional `patternColor` is a CSS color string to fill a hatched polygon with a backround color.


## `PolygonStyleConfigDiscrete` 

A discrete style for polygons. The selection process is identical to `PointStyleConfigDiscrete`, it's encoded in a `PolygonDiscreteGroup` objects along with polygon style definition.

```typescript
type PolygonDiscreteGroup = {
    values: string[];
    label: MessageRecord;
    strokeColor: string;
    fillColor: string;
    strokeWidth: number;
    pattern: boolean;
    patternAngle: 0 | 45 | 90 | 135;
    patternColor?: string;
}

export interface PolygonStyleConfigDiscrete {
    kind: 'polygon-discrete';
    label?: LabelStyle;
    propName: string;
    groups: PolygonDiscreteGroup[];
}
```


## `PolygonStyleConfigContinuous`


A continuous style for plygons. The selection is the same as for `PointStyleConfigContinuous`, encoded in a `PolygonInterval` object  along with polygon style definition.

```typescript
type PolygonInterval = {
    low: number;
    high: number; 
    label: MessageRecord;
    strokeColor: string;
    fillColor: string;
    strokeWidth: number;
    pattern: boolean;
    patternAngle: 0 | 45 | 90 | 135;
    patternColor?: string;
}

export interface LineStyleConfigDiscrete {
    kind: 'polygon-continuous';
    label?: LabelStyle;
    propName: string;
    intervals: PolygonInterval[];
}
```