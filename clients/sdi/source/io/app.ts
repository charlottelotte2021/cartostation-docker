/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import { MessageRecordIO, nullable, TypeOf } from './io';
import * as io from 'io-ts';
import { fromNullable } from 'fp-ts/lib/Option';


// tslint:disable-next-line: variable-name
export const AppManifestIO = io.tuple([
    (io.string), // codename
    io.union([MessageRecordIO, io.null]), // display name
    io.string, // route name
    io.string, // url
], 'AppManifestIO');
export type AppManifest = TypeOf<typeof AppManifestIO>;


export const appName = (a: AppManifest) => a[0];
export const appDisplayName = (a: AppManifest) => fromNullable(a[1]);
export const appRoute = (a: AppManifest) => a[2];
export const appUrl = (a: AppManifest) => a[3];



// tslint:disable-next-line: variable-name
export const AppUrlIO = io.interface({
    name: io.string,
    label: MessageRecordIO,
    url: MessageRecordIO,
}, 'AppUrlIO')

export type AppUrl = io.TypeOf<typeof AppUrlIO>;


// tslint:disable-next-line: variable-name
export const AppConfigIO = io.interface({
    user: nullable(io.string),
    args: io.array(io.string),
    api: io.string,
    csrf: io.string,
    root: io.string,
    apps: io.array(AppManifestIO),
    urls: io.array(AppUrlIO),
    admin_query: nullable(io.number),
}, 'AppConfigIO');
export type AppConfig = TypeOf<typeof AppConfigIO>;
