import { query, queryK } from '../shape';
import { fromEither, fromNullable, none, some } from 'fp-ts/lib/Option';
import { IAliasCollection, appName } from '../source';
import { fromRecord } from '../locale';
import { identity } from 'fp-ts/lib/function';
import { MessageRecordLangIO } from '../source/io/message';
import { checkURLScheme } from '../util';

export const getCurrentAppCodename = queryK('app/codename');

export const getUserId = () => fromNullable(query('app/user'));

export const getUserIdAstNumber = () =>
    getUserId().chain(id =>
        Number.isNaN(parseInt(id, 10)) ? none : some(parseInt(id, 10))
    );

export const isUserLogged = () => getUserId().isSome();

export const getApiUrl = (path: string) =>
    checkURLScheme(`${query('app/api-root')}${path}`);

export const getLang = () => query('app/lang');

export const getCSRF = () => fromNullable(query('app/csrf'));

export const getRoute = () =>
    query('app/route')
        .map(frag => frag.trim())
        .filter(frag => frag.length > 0);

export const getRoot = () => checkURLScheme(query('app/root'));

export const getRelativeRoot = () => {
    const parsed = new URL(getRoot());
    return parsed.pathname;
};

export const getRootUrl = (path: string) => getRoot() + path;

export const getActivityToken = () => fromNullable(query('app/activityToken'));

const blackList = ['dashboard', 'login', 'solar'];

export const getApps = () =>
    query('app/apps').filter(a => blackList.indexOf(appName(a)) === -1);

export const getAppManifest = (codename: string) =>
    fromNullable(query('app/apps').find(a => appName(a) === codename));

export const getAppUrl = (urlName: string) =>
    fromNullable(query('app/urls').find(({ name }) => urlName === name));

const getAliasInDictOption = (dict: Readonly<IAliasCollection>, k: string) =>
    fromNullable(dict.find(alias => alias.select === k)).map(alias =>
        fromRecord(alias.replace)
    );

export const getAliasRaw = (k: string) =>
    fromNullable(query('data/alias')).chain(dict =>
        fromNullable(dict.find(alias => alias.select === k))
    );

export const getAliasOption = (k: string) =>
    fromNullable(query('data/alias')).chain(dict =>
        getAliasInDictOption(dict, k)
    );

export const getAlias = (k: string) => getAliasOption(k).fold(k, identity);

export const getTermList = queryK('data/terms');

export const findTerm = (id: number) =>
    fromNullable(getTermList().find(t => t.id === id));

export const getAppListStatus = () => query('app/appselect/visible');

export const getFocusId = () => query('app/focus-id');

export const getCollapsible = (name: string) =>
    fromNullable(query('app/collapsible')[name]).getOrElse(false);

export const getParameterLang = () => {
    try {
        const loc = document.location;
        const params = new URL(loc.href).searchParams;
        return fromEither(MessageRecordLangIO.decode(params.get('lang')));
    } catch (e) {
        return none;
    }
};

export const isSelectOpen = (id: string) =>
    fromNullable(query('app/select/open/id'))
        .map(i => i === id)
        .getOrElse(false);

export const getReadMore = (name: string) =>
    fromNullable(query('app/collapsible')[name]).getOrElse(false);