#!/bin/sh

export NODE_OPTIONS=--openssl-legacy-provider
echo "Watching $1"

webpack --config $1/webpack.config.js --watch  --mode development  ${WEBPACK_OPTIONS}