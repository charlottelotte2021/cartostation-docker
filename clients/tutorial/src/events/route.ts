import * as debug from 'debug';
import { index } from 'fp-ts/lib/Array';
import { TypeOf, union, literal } from 'io-ts';
import { Router } from 'sdi/router';
import { setLayout, setAppName } from './app';
import { fetchAllUsersEvents } from './table';

const logger = debug('sdi:route');

// tslint:disable-next-line: variable-name
const RouteIO = union([
    literal('home'),
    literal('map'),
    literal('table'),
    literal('input'),
    literal('interface'),
]);
export type Route = TypeOf<typeof RouteIO>;

export const { home, route, navigate } = Router<Route>('tutorial');

export type Layout = Route;

home('home', () => {
    setLayout('home');
});

const simpleRoute = (r: Route) =>
    route(r, () => {
        setLayout(r);
        setAppName(r);
    });

// Declare route handlers

route('map', () => {
    setLayout('map');
});

route('table', () => {
    fetchAllUsersEvents();
    setLayout('table');
});

route('input', () => {
    fetchAllUsersEvents();
    setLayout('input');
});

simpleRoute('interface');

// end of route handlers

export const loadRoute = (initial: string[]) =>
    index(0, initial).map(prefix =>
        RouteIO.decode(prefix).map(c => navigate(c, initial.slice(1)))
    );

export const navigateMap = () => navigate('map', []);
export const navigateTable = () => navigate('table', []);
export const navigateInput = () => navigate('input', []);

logger('loaded');
