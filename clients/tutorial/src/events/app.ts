import { assign } from 'sdi/shape';
import { IUser } from 'sdi/source';
import { Layout } from './route';

export const setLayout = (l: Layout) => assign('app/layout', l);

export const setUserData = (u: IUser) => assign('data/user', u);

export const setAppName = (n: string) => assign('app/name', n);
