import { register, renderNavigate } from '../components/modal';

export const [closeNavigate, openNavigate] = register(
    'tutorial/navigate',
    renderNavigate
);
