import { create } from 'sdi/map';
import { getBaseLayer, getView, getMapInfo } from '../queries/map';
import { Option, none, some } from 'fp-ts/lib/Option';
import { updateView, setScaleLine } from '../events/map';
import { DIV, SPAN } from 'sdi/components/elements';
import { fromRecord } from 'sdi/locale';
import { MessageRecord } from 'sdi/source';
import { Route, navigate } from '../events/route';
import { getLinks } from '../queries/navigate';

let mapUpdate: Option<() => void> = none;
let mapSetTarget: Option<(e: HTMLElement) => void> = none;

const attachMap = (element: HTMLElement | null) => {
    mapUpdate = mapUpdate.foldL(
        () => {
            const { update, setTarget } = create('tutorial-map', {
                getBaseLayer,
                getView,
                getMapInfo,

                updateView,
                setScaleLine,

                element,
            });
            mapSetTarget = some(setTarget);
            update();
            return some(update);
        },
        update => some(update)
    );

    if (element) {
        mapSetTarget.map(f => f(element));
    }
};

const renderLink = (name: MessageRecord, onClick: () => void) =>
    DIV({ onClick, className: 'tuto-links' }, SPAN({}, fromRecord(name)));

const goTo = (r: Route) => () => navigate(r, []);

const render = () =>
    DIV(
        { className: 'main-app interface' },
        ...getLinks().map(([n, r]) => renderLink(n, goTo(r))),
        mapUpdate.map(f => f()),
        DIV(
            { className: 'map-wrapper' },
            DIV({
                id: 'tutorial-map',
                className: 'map',
                ref: attachMap,
            })
        )
    );

export default render;
