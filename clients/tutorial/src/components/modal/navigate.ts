import { DIV, H1, SPAN } from 'sdi/components/elements';
import { makeLabel } from '../buttons';
import { setNext } from '../../events/navigate';
import { getNext, getLinks } from '../../queries/navigate';
import tr, { fromRecord } from 'sdi/locale';
import { navigate, Route } from 'tutorial/src/events/route';
import { MessageRecord } from 'sdi/source';

const cancelButton = makeLabel('cancel', 2, () => tr.core('cancel'));
const goButton = makeLabel('navigate', 2, () => tr.core('go'));

const renderLink = (name: MessageRecord, r: Route) =>
    DIV(
        {
            onClick: () => setNext(r),
        },
        SPAN({}),
        fromRecord(name),
        getNext().fold('', next => (next === r ? ' *' : ''))
    );

const header = () => H1({}, 'Navigate');

const goTo = () => getNext().map(r => navigate(r, []));

const footer = (close: () => void) =>
    DIV(
        {},
        goButton(() => {
            goTo();
            close();
        }),
        cancelButton(close)
    );

const body = () => DIV({}, ...getLinks().map(([m, r]) => renderLink(m, r)));

export const render = { header, body, footer };
