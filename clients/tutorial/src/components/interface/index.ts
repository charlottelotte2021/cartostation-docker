import { none, some, Option } from 'fp-ts/lib/Option';
import {
    DIV,
    H2,
    NodeOrOptional,
    SECTION,
    SPAN,
} from 'sdi/components/elements';
import {
    renderSelect,
    renderRadioIn,
    renderRadioOut,
    renderSelectFilter,
} from 'sdi/components/input';
import { fromRecord, Translated } from 'sdi/locale';
import { MessageRecord } from 'sdi/source';
import { Route, navigate } from '../../events/route';
import { getLinks } from '../../queries/navigate';
import { renderCheckbox } from 'sdi/components/input/checkbox';
import text from './text';
import { btn1, btn2, btn3 } from './buttons';
import {
    renderCollapsibleWrapper,
    renderTooledCollapsibleWrapper,
} from 'sdi/components/collapsible-wrapper';
import { __forceRefreshState } from 'sdi/app';
import { makeIcon } from 'sdi/components/button';
import { tooltip } from './tooltip';
import {
    renderInputDate,
    renderInputDate2,
    renderInputNumber,
    renderInputText,
    renderTextLong,
} from './input';
import { setoidNumber } from 'fp-ts/lib/Setoid';

const noop = () => {};

const wrapper = (name: string, ...nodes: NodeOrOptional[]) =>
    SECTION(
        // name.toLowerCase().replace(/\s+/g, '-'),
        'component__wrapper',
        H2('component__title', name),
        DIV('component', ...nodes)
    );

// drop-down list
const testList = [1001, 223, 555, 444, 29000, 1];
const numToString = (d: number) => d.toString();
const select = () =>
    wrapper(
        'Drop-down list',
        renderSelect(
            'test-list',
            numToString,
            noop,
            setoidNumber
        )(testList, none)
    );

// Filtered Drop-Down list
//const testStationList = ['bruwater', 'bru', 'bruxelles', 'brugeotool']

const selectandinput = () =>
    wrapper(
        'Select and filter',
        renderSelectFilter(
            setoidNumber,
            numToString,
            noop,
            toString
        )(testList, none)
    );

// checkbox
const checkbox = () =>
    wrapper(
        'Checkbox',
        renderCheckbox(
            'checkbox-test',
            () => 'Check this:' as Translated,
            noop
        )(true, 'tutorial-checkbox')
    );

// radio / switch buttons

const { setValue, getValue } = (() => {
    let value = 'this';
    const setValue = (key: string) => {
        value = key;
    };
    const getValue = () => value;
    return { setValue, getValue };
})();
const elements = ['this', 'that', 'those', 'other'];
const switchSelect = (s: string) => {
    setValue(s);
    __forceRefreshState();
};
const renderElement = (s: string) => SPAN('element-in', s);

// switch buttons with text in the buttons

const switchButton = () => {
    return wrapper(
        'Switch label inside',
        renderRadioIn(
            'tutorial-switch',
            renderElement,
            switchSelect,
            'switch'
        )(elements, getValue())
    );
};
// radio buttons with text out

const radioButton = () => {
    return wrapper(
        'Radio buttons',
        renderRadioIn(
            'tutorial-radio',
            renderElement,
            switchSelect,
            'radio'
        )(elements, getValue())
    );
};

//radio button with text out, and only two choices
const { setValueOut, getValueOut } = (() => {
    let value = 'two';
    const setValueOut = (key: string) => {
        value = key;
    };
    const getValueOut = () => value;
    return { setValueOut, getValueOut };
})();
const elementsOut: [string, string] = ['one', 'two'];
const switchSelectOut = (s: string) => {
    setValueOut(s);
    __forceRefreshState();
};
const renderElementOut = (s: string) => SPAN('element-out', s);
const switchButtonOut = () => {
    return wrapper(
        'Toggle switch',
        renderRadioOut(
            'tutorial-switch-boolean',
            renderElementOut,
            switchSelectOut
        )(elementsOut, getValueOut())
    );
};

// collapsible wrapper default
const head = DIV('wrapper-head', 'click to expand/collapse!');
const body = DIV(
    'body',
    'Asperiores illo similique ipsam architecto ut facere. Dicta et voluptatum aliquam porro voluptatem. Dolorum ut officiis temporibus totam eum alias eum. Quo in atque qui. Enim autem aperiam adipisci enim doloribus qui nihil provident.'
);
const collapsibleWrapper = () =>
    wrapper(
        'Collapsible-default',
        renderCollapsibleWrapper('collapsible', head, body)
    );

// collapsible wrapper extra
const headExtraActions = DIV(
    { onClick: e => e.stopPropagation },
    makeIcon('reset', 3, 'bath', {
        position: 'top-left',
        text: 'Bath' as Translated,
    })(() => ''),
    makeIcon('reset', 3, 'sun', {
        position: 'top-right',
        text: 'Sun' as Translated,
    })(() => '')
);
const headExtra = DIV('wrapper-head', 'A header that contains more ');
const bodyExtra = DIV(
    'body',
    'Asperiores illo similique ipsam architecto ut facere. Dicta et voluptatum aliquam porro voluptatem. Dolorum ut officiis temporibus totam eum alias eum. Quo in atque qui. Enim autem aperiam adipisci enim doloribus qui nihil provident.'
);

const collapsibleWrapperExtra = () =>
    wrapper(
        'Collapsible-extra',
        renderTooledCollapsibleWrapper(
            'collapsible',
            headExtra,
            headExtraActions,
            bodyExtra
        )
    );

const renderLink = (name: MessageRecord, onClick: () => void) =>
    DIV({ onClick, className: 'tuto-links' }, SPAN({}, fromRecord(name)));

const goTo = (r: Route) => () => navigate(r, []);

const render = () =>
    DIV(
        { className: 'main-app interface' },
        ...getLinks().map(([n, r]) => renderLink(n, goTo(r))),
        DIV(
            'components__list',
            btn1(),
            btn2(),
            btn3(),
            tooltip(),
            select(),
            selectandinput(),
            checkbox(),
            radioButton(),
            switchButton(),
            switchButtonOut(),
            collapsibleWrapper(),
            collapsibleWrapperExtra(),
            renderInputText(),
            renderTextLong(),
            renderInputDate(),
            renderInputDate2(),
            renderInputNumber(),
            text()
        )
    );

export default render;
