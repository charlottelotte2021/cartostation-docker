import {
    DIV,
    H2,
    INPUT,
    NodeOrOptional,
    SECTION,
} from 'sdi/components/elements';
import {
    attrOptions,
    inputDate,
    inputLongText,
    inputNumber,
    inputText,
    options,
} from 'sdi/components/input';
import { setInputDate, setInputNumber, setInputText } from '../../events/input';
import {
    getInputDate,
    getInputNumber,
    getInputText,
} from '../../queries/input';

//  const noop = () => { }

const wrapper = (name: string, ...nodes: NodeOrOptional[]) =>
    SECTION(
        // name.toLowerCase().replace(/\s+/g, '-'),
        'component__wrapper',
        H2('component__title', name),
        DIV('component', ...nodes)
    );

export const renderInputText = () =>
    wrapper(
        'Input-text',
        inputText(
            attrOptions(
                `'interface-input-text-example`,
                getInputText,
                setInputText,
                {
                    placeholder: 'put your text here',
                }
            )
        )
    );

export const renderInputDate = () =>
    wrapper('Input-date', INPUT({ type: 'date' }));

export const renderInputDate2 = () =>
    wrapper(
        'Input-date 2',
        inputDate(getInputDate().toString())(
            new Date(0),
            new Date(),
            new Date(),
            setInputDate
        )
    );

export const renderInputNumber = () =>
    wrapper(
        'Input-number',
        inputNumber(
            options(`input-number-example`, getInputNumber, setInputNumber)
        )
    );

export const renderTextLong = () =>
    wrapper(
        'Input-textarea',
        inputLongText(getInputText, setInputText, {
            placeholder: 'put your text here',
            rows: 5,
        })
    );
