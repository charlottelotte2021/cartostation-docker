import { nameToString } from 'sdi/components/button/names';
import { DIV, H2, NodeOrOptional, SECTION } from 'sdi/components/elements';
import {
    buttonTooltipBottom,
    buttonTooltipBottomLeft,
    buttonTooltipBottomRight,
    buttonTooltipLeft,
    buttonTooltipRight,
    buttonTooltipTop,
    buttonTooltipTopLeft,
    buttonTooltipTopRight,
} from 'sdi/components/tooltip';
import { Translated } from 'sdi/locale';

const wrapper = (name: string, ...nodes: NodeOrOptional[]) =>
    SECTION(
        // name.toLowerCase().replace(/\s+/g, '-'),
        'component__wrapper',
        H2('component__title', name),
        DIV('component', ...nodes)
    );

export const tooltip = () =>
    wrapper(
        'Tooltips',
        buttonTooltipLeft(
            'éditer' as Translated,
            { className: 'btn btn-2 icon-only component-tooltip' },
            nameToString('pencil-alt')
        ),
        buttonTooltipTopLeft(
            'éditer' as Translated,
            { className: 'btn btn-2 icon-only component-tooltip' },
            nameToString('pencil-alt')
        ),
        buttonTooltipTop(
            'éditer' as Translated,
            { className: 'btn btn-2 icon-only component-tooltip' },
            nameToString('pencil-alt')
        ),
        buttonTooltipTopRight(
            'éditer' as Translated,
            { className: 'btn btn-2 icon-only component-tooltip' },
            nameToString('pencil-alt')
        ),
        buttonTooltipRight(
            'éditer' as Translated,
            { className: 'btn btn-2 icon-only component-tooltip' },
            nameToString('pencil-alt')
        ),
        buttonTooltipBottomRight(
            'éditer' as Translated,
            { className: 'btn btn-2 icon-only component-tooltip' },
            nameToString('pencil-alt')
        ),
        buttonTooltipBottom(
            'éditer' as Translated,
            { className: 'btn btn-2 icon-only component-tooltip' },
            nameToString('pencil-alt')
        ),
        buttonTooltipBottomLeft(
            'éditer' as Translated,
            { className: 'btn btn-2 icon-only component-tooltip' },
            nameToString('pencil-alt')
        )
    );
