import { DIV, H2 } from 'sdi/components/elements';
import { Item } from './index';
import tr from 'sdi/locale';
import { getCurrentItem } from 'tutorial/src/queries/app';
import { navigateList } from 'tutorial/src/events/route';

const renderItem = (i: Item) =>
    DIV(
        { className: 'ietm-item', key: i.name },
        H2({}, i.name),
        DIV({}, i.content)
    );

const renderNoItem = () => DIV({ className: 'error' }, tr.tuto('noItemError'));

export const render = () =>
    DIV(
        { className: 'item-list' },
        DIV({ onClick: navigateList }, 'Return to list'),
        getCurrentItem().foldL(renderNoItem, renderItem)
    );

export default render;
