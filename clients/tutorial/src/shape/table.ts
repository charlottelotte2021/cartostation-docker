import { IUserList } from 'sdi/source';
import { IDataTable, initialTableState } from 'sdi/components/table';

declare module 'sdi/shape' {
    export interface IShape {
        'data/users': IUserList;
        'component/table': IDataTable;
    }
}

export const defaultTableShape = () => ({
    'data/users': [],
    'component/table': initialTableState(),
});
