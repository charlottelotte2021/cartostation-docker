import { loop } from 'sdi/app';
import { DIV } from 'sdi/components/elements';
import header from 'sdi/components/header';
import footer from 'sdi/components/footer';

import { loadRoute } from './events/route';
import { getLayout } from './queries/app';

import home from './components/home';
import map from './components/map';
import modal from './components/modal';
import table from './components/table';
import input from './components/input';
import csInterface from './components/interface';

const wrappedMain = (
    name: string,
    ...elements: React.DOMElement<{}, Element>[]
) =>
    DIV(
        { className: 'project' },
        modal(),
        header('tutorial'),
        DIV({ className: `main ${name}` }, ...elements),
        footer()
    );

const renderHome = () => wrappedMain('home', home());
const renderMap = () => wrappedMain('map', map());
const renderTable = () => wrappedMain('table', table());
const renderInput = () => wrappedMain('input', input());
const renderInterface = () => wrappedMain('interface', csInterface());

const render = () => {
    const layout = getLayout();
    switch (layout) {
        case 'home':
            return renderHome();
        case 'map':
            return renderMap();
        case 'table':
            return renderTable();
        case 'input':
            return renderInput();
        case 'interface':
            return renderInterface();
    }
};

const effects = (initialRoute: string[]) => () => {
    loadRoute(initialRoute);
};

export const app = (initialRoute: string[]) =>
    loop('tutorial', render, effects(initialRoute));
