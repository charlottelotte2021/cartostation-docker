import { query } from 'sdi/shape';
import { fromNullable } from 'fp-ts/lib/Option';
import { Item } from 'tutorial/src/components/item';

export const getLayout = () => query('app/layout');

const findItem = (name: string, xs: Item[] | readonly Item[]) =>
    xs.find(x => x.name === name);

export const getCurrentItem = () =>
    fromNullable(query('app/selectd-item')).chain(name =>
        fromNullable(findItem(name, query('component/items')))
    );

export const getUsername = () =>
    fromNullable(query('data/user')).map(u => u.name);

export const getAppName = () => query('app/name');
