import {
    tableQueries,
    TableSourceGetter,
    TableDataRow,
    TableDataType,
} from 'sdi/components/table';
import { queryK, query } from 'sdi/shape';

export const getUsers = () => query('data/users');

const getData = (): TableDataRow[] =>
    getUsers().map(user => ({
        from: user.id,
        cells: [user.id, user.name, user.maps.join(', ')],
    }));

const getKeys = (): string[] => ['id', 'name', 'maps'];

const getTypes = (): TableDataType[] => ['number', 'string', 'string'];

export const getSource: TableSourceGetter = () => ({
    kind: 'local',
    data: getData(),
    keys: getKeys(),
    types: getTypes(),
});

export const tableUserQueries = tableQueries(
    queryK('component/table'),
    getSource
);
