import { MessageStore, formatMessage } from 'sdi/locale';
import * as texts from './marked-texts';

const messages = {
    logout: {
        fr: 'Se déconnecter',
        nl: 'Uitloggen', // nldone
        en: 'Logout',
    },

    login: {
        fr: 'Se connecter',
        nl: 'Inloggen', // nldone
        en: 'Sign in',
    },

    'dashboard.tagline': {
        fr: 'Applications',
        nl: 'Applicaties', // nldone
        en: 'Applications',
    },

    yourAccount: {
        fr: 'Votre compte',
        nl: 'Uw account', // nldone
        en: 'Your account',
    },

    notifications: {
        fr: 'Notifications',
        nl: 'Berichtgevingen', // c'est comme ça que c'est traduit dans des téléphones. mais 'aankondigingen' serait bien aussi
        en: 'Notifications',
    },

    'dashboard.infos': {
        fr: texts.homeTextFR,
        nl: texts.homeTextNL,
        en: ``,
    },

    'dashboard.disclaimer': {
        fr: texts.homeDisclaimerFR,
        nl: texts.homeDisclaimerNL,
        en: ``,
    },
};

type MDB = typeof messages;
export type DashboardMessageKey = keyof MDB;

declare module 'sdi/locale' {
    export interface MessageStore {
        dashboard(k: DashboardMessageKey): Translated;
    }
}

MessageStore.prototype.dashboard = function (k: DashboardMessageKey) {
    return this.getEdited('dashboard', k, () => formatMessage(messages[k]));
};
