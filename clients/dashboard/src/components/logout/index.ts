import { fromNullable } from 'fp-ts/lib/Option';

import { DIV, BUTTON, A, H2 } from 'sdi/components/elements';
import { IUser } from 'sdi/source';
import tr from 'sdi/locale';

import { tryLogout } from '../../events/login';
import { getUserData } from '../../queries/app';

import { makeLabelAndIcon } from '../button';
import { markdown } from 'sdi/ports/marked';

const logoutButton = makeLabelAndIcon('logout', 2, 'sign-out-alt', () =>
    tr.dashboard('logout')
);

const username = (user: IUser) => {
    const ut = user.name.trim();
    if (ut.length === 0) {
        return `User ${user.id}`;
    }
    return ut;
};

const disclaimer = () =>
    DIV(
        { className: 'user__disclaimer' },
        markdown(tr.dashboard('dashboard.disclaimer'), 'md')
    );

const renderLoggedIn = (user: IUser) =>
    DIV(
        { className: 'login-widget' },
        H2({}, tr.dashboard('yourAccount')),
        DIV({ className: 'logout-username' }, username(user)),
        logoutButton(tryLogout),
        disclaimer()
    );

const renderAnonymous = () =>
    DIV(
        {},
        BUTTON(
            { className: 'login-widget btn btn-1 btn-navigate' },

            A({ className: '', href: '/client/login/' }, tr.dashboard('login')),
            disclaimer()
        )
    );

const render = () =>
    fromNullable(getUserData()).foldL(renderAnonymous, renderLoggedIn);

export default render;
