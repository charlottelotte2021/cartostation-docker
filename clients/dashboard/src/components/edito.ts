/*
 *  Copyright (C) 2019 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { DIV } from 'sdi/components/elements';
import tr from 'sdi/locale';
// import { stringToParagraphs } from 'sdi/util';
import { markdown } from 'sdi/ports/marked';

const render = () =>
    DIV(
        { className: 'edito' },

        DIV({ className: 'edito__title' }, tr.core('__empty__')),
        DIV(
            { className: 'illu__wrapper' },
            DIV({ className: 'illu__img' }),
            DIV({ className: 'illu__caption' }, tr.core('__empty__'))
        ),
        DIV(
            { className: 'edito__text' },
            markdown(tr.dashboard('dashboard.infos'), 'md')
        )
    );

export default render;
