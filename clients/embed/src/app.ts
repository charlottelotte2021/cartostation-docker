import * as debug from 'debug';

import { DIV, NodeOrOptional } from 'sdi/components/elements';
import { loop, getApiUrl, getLang } from 'sdi/app';

import {
    initMap,
    loadBaseLayer,
    loadAlias,
    activityLogger,
} from './events/app';

import map from './components/map';
import legend from './components/legend';
import feature from './components/feature-view';
import { getLayout } from './queries/app';
import tr from 'sdi/locale';
import { visitAction, langAction } from 'sdi/activity';

const logger = debug('sdi:app');

const logoBe = () =>
    DIV({ className: 'brand-logo' }, DIV({ className: 'brand-name' }));

const wrappedMain = (name: string, ...elements: NodeOrOptional[]) =>
    DIV(
        { className: 'embed' },
        DIV({ className: `main ${name}` }, ...elements),
        logoBe()
    );

const render = () => {
    const layout = getLayout();
    switch (layout) {
        case 'map':
            return wrappedMain('main', map(), legend());
        case 'map-and-feature':
            return wrappedMain('main', map(), feature());
    }
};

const baseLayers = [
    'urbis.irisnet.be/urbis_gray',
    'urbis.irisnet.be/ortho_2016',
];

const effects = () => {
    baseLayers.forEach(id => loadBaseLayer(id, getApiUrl(`wmsconfig/${id}`)));
    loadAlias(getApiUrl('alias'));
    initMap();
    tr.init_edited();
    activityLogger(visitAction());
    activityLogger(langAction(getLang()));
};

const app = loop('embed-app', render, effects);
export default app;

logger('loaded');
