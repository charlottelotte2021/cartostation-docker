import { MessageStore, formatMessage } from 'sdi/locale';

const messages = {
    zoomOnFeature: {
        fr: 'Zoomer sur l’entité',
        nl: 'Zoom in op de entiteit',
        en: 'Zoom to item',
    },

    mapLegend: {
        fr: 'Légende',
        nl: 'Legende',
        en: 'Legend',
    },

    atlasLinkLabel: {
        fr: 'Voir la carte sur Cartostation',
        nl: 'Zie de kaart op Cartostation', // nltocheck
        en: 'See the map on Cartostation',
    },
};

type MDB = typeof messages;
export type EmbedMessageKey = keyof MDB;

declare module 'sdi/locale' {
    export interface MessageStore {
        embed(k: EmbedMessageKey): Translated;
    }
}

MessageStore.prototype.embed = function (k: EmbedMessageKey) {
    return this.getEdited('embed', k, () => formatMessage(messages[k]));
};
