// imports from sdi
import { IUser } from 'sdi/source';
import { Collection, Nullable } from 'sdi/util';

export type AppLayout =
    | 'splash'
    | 'domain-list'
    | 'domain-select'
    | 'domain-form'
    | 'term-select'
    | 'term-form';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'app/layout': AppLayout;
        'app/route': string[];

        'data/user': Nullable<IUser>;
        'component/foldable': Collection<boolean>;
    }
}
