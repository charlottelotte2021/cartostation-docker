import { fromNullable, some, none, Option } from 'fp-ts/lib/Option';
import { query } from 'sdi/shape';
import { Editor, PartialForm, Form } from '../components/edit';
import { optRec } from 'sdi/locale';
import { MessageRecordLang } from 'sdi/source';
import { left, right, Either } from 'fp-ts/lib/Either';
import { findDomain, findTerm } from 'angled-core/queries/universe';

export const getCurrentDomain = () =>
    fromNullable(query('universe/domain-id')).chain(findDomain);

export const getCurrentTerm = () =>
    fromNullable(query('universe/term-id')).chain(findTerm);

export const getForm = (e: Editor) =>
    fromNullable(query('universe/form')).chain(f =>
        f.kind === e ? some(f) : none
    );

export const getNewForm = (e: Editor) =>
    fromNullable(query('universe/form-new')).chain(f =>
        f.kind === e ? some(f) : none
    );

export const getEitherForm = (e: Editor): Either<Option<PartialForm>, Form> =>
    getForm(e).foldL(
        () => left(getNewForm(e)),
        f => right(f)
    );

export const getNewDomainName =
    (ln: MessageRecordLang) =>
    (dflt = '') =>
        getNewForm('domain')
            .chain(f => fromNullable(f.data.name))
            .fold(dflt, name => optRec(name)(ln).getOrElse(dflt));

export const getNewTermName =
    (ln: MessageRecordLang) =>
    (dflt = '') =>
        getNewForm('term')
            .chain(f => fromNullable(f.data.name))
            .fold(dflt, name => optRec(name)(ln).getOrElse(dflt));
