import { query } from 'sdi/shape';
import { fromNullable } from 'fp-ts/lib/Option';
import { getAppManifest } from 'sdi/app';
import { appDisplayName } from 'sdi/source';
import tr, { fromRecord } from 'sdi/locale';

export const getUserData = () => query('data/user');

export const getLayout = () => query('app/layout');

export const getAppName = () =>
    getAppManifest('angled-universe')
        .chain(appDisplayName)
        .map(fromRecord)
        .getOrElse(tr.core('angled:universe'));

export const getFolded = (name: string) =>
    fromNullable(query('component/foldable')[name]).fold(true, v => v);
