import { DIV, NODISPLAY, H2, SPAN } from 'sdi/components/elements';
import { MessageRecordLang } from 'sdi/source';
import {
    inputText,
    inputLongText,
    attrOptions,
    options,
} from 'sdi/components/input';
import tr from 'sdi/locale';

import {
    saveDomain,
    updateNewDomainName,
    saveNewDomain,
    updateDomainFormName,
    updateDomainFormDescription,
} from 'angled-universe/src/events/universe';
import {
    getNewDomainName,
    getEitherForm,
} from 'angled-universe/src/queries/universe';
import { nameGetter, Form, PartialForm, descriptionGetter } from '../edit';
import { makeLabelAndIcon } from 'sdi/components/button';
import { navigateHome, navigateDomain } from 'angled-universe/src/events/route';
import { getLang } from 'sdi/app';

const saveButton = makeLabelAndIcon('save', 1, 'check', () =>
    tr.angled('saveDomain')
);
const cancelButton = makeLabelAndIcon('cancel', 3, 'times', () =>
    tr.angled('cancel')
);

const newDomainName = (ln: MessageRecordLang) =>
    inputText(
        attrOptions(
            'form-new-domain-name',
            getNewDomainName(ln),
            updateNewDomainName(ln),
            {}
        )
    );

const renderNewDomainForm = (_pf: PartialForm) =>
    DIV(
        { className: 'inner-form' },
        H2({}, tr.angled('newDomain')),
        DIV(
            { className: 'input-wrapper' },
            DIV({ className: 'input-label' }, tr.angled('nameFR')),
            newDomainName('fr')
        ),
        DIV(
            { className: 'input-wrapper' },
            DIV({ className: 'input-label' }, tr.angled('nameNL')),
            newDomainName('nl')
        ),
        cancelButton(() => navigateHome()),
        saveButton(() =>
            saveNewDomain().map(p => p.then(d => navigateDomain(d.id)))
        )
    );

const domainName = (ln: MessageRecordLang) =>
    inputText(
        options(
            'form-domain-name',
            () => nameGetter('domain')(ln).getOrElse(''),
            updateDomainFormName(ln)
        )
    );

const domainDescription = (ln: MessageRecordLang) =>
    inputLongText(
        () => descriptionGetter('domain')(ln).getOrElse(''),
        updateDomainFormDescription(ln)
    );

const renderFormDomain = (f: Form) =>
    DIV(
        { className: 'inner-form' },
        H2(
            {},
            tr.angled('editPrefix'),
            SPAN(
                { className: 'edit-name' },
                nameGetter('domain')(getLang()).getOrElse('')
            )
        ),
        DIV(
            { className: 'input-wrapper' },
            DIV({ className: 'input-label' }, tr.angled('nameFR')),
            domainName('fr'),
            DIV({ className: 'input-label' }, tr.angled('descriptionFR')),
            domainDescription('fr')
        ),
        DIV(
            { className: 'input-wrapper' },
            DIV({ className: 'input-label' }, tr.angled('nameNL')),
            domainName('nl'),
            DIV({ className: 'input-label' }, tr.angled('descriptionNL')),
            domainDescription('nl')
        ),
        cancelButton(() => navigateDomain(f.data.id)),
        saveButton(() => {
            saveDomain();
            navigateDomain(f.data.id);
        })
    );

// tslint:disable-next-line: variable-name
export const DomainForm = () =>
    DIV(
        { className: 'panel form' },
        getEitherForm('domain').fold(
            optF => optF.fold(NODISPLAY(), renderNewDomainForm),
            f => renderFormDomain(f)
        )
    );
