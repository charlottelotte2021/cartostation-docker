import { fromNullable } from 'fp-ts/lib/Option';

import { DIV, H1 } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { isUserLogged } from 'sdi/app/queries';
import { stringToParagraphs } from 'sdi/util';
import { makeLabelAndIcon, makeIcon } from 'sdi/components/button';

import {
    navigateHome,
    navigateDomainForm,
} from 'angled-universe/src/events/route';
import { getCurrentDomain } from 'angled-universe/src/queries/universe';
import { Domain } from 'angled-core/ref';

import { isActiveButton, goToLoginPage } from '../buttons';

const editButton = makeLabelAndIcon('edit', 1, 'pencil-alt', () =>
    tr.angled('editDomain')
);
const closeButton = makeIcon('close', 3, 'times', {
    position: 'top-right',
    text: () => tr.core('close'),
});

const onPushButton = (id: number) => {
    if (isUserLogged()) {
        navigateDomainForm(id);
    } else {
        goToLoginPage();
    }
};

const renderEditButton = (d: Domain) =>
    editButton(() => onPushButton(d.id), isActiveButton());

const renderDomain = (d: Domain) =>
    DIV(
        {},
        closeButton(() => navigateHome()),
        H1({}, fromRecord(d.name)),
        fromNullable(d.description).fold(
            DIV({ className: 'description' }, tr.angled('noDescription')),
            desc =>
                DIV(
                    { className: 'description' },
                    stringToParagraphs(fromRecord(desc))
                )
        ),
        renderEditButton(d)
    );

// tslint:disable-next-line: variable-name
export const DomainDetail = () =>
    DIV({ className: 'detail domain' }, getCurrentDomain().map(renderDomain));
