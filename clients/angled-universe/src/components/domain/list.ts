import { fromNullable } from 'fp-ts/lib/Option';

import { DIV, H2, SPAN } from 'sdi/components/elements';
import tr, { fromRecord, Translated } from 'sdi/locale';
import { isUserLogged } from 'sdi/app/queries';
import { makeLabelAndIcon } from 'sdi/components/button';

import { Domain } from 'angled-core/ref';
import { getCurrentDomain } from 'angled-universe/src/queries/universe';
import {
    navigateDomain,
    navigateDomainNew,
} from 'angled-universe/src/events/route';
import { clearForm } from 'angled-universe/src/events/universe';
import { isActiveButton, goToLoginPage } from '../buttons';
import { DomainHelp } from './help';
import { getDomainList } from 'angled-core/queries/universe';

const createButton = makeLabelAndIcon('add', 1, 'plus', () =>
    tr.angled('addDomain')
);

const onPushButton = () => {
    if (isUserLogged()) {
        clearForm();
        navigateDomainNew();
    } else {
        goToLoginPage();
    }
};

const renderCreateButton = () =>
    createButton(() => onPushButton(), isActiveButton());

const isActive = (d: Domain) =>
    getCurrentDomain().fold(false, c => d.id === c.id);

const domainClass = (d: Domain) =>
    isActive(d) ? 'list-item domain active' : 'list-item domain';

const renderDomain = (d: Domain) =>
    DIV(
        {
            className: domainClass(d),
            onClick: () => navigateDomain(d.id),
        },
        SPAN(
            {
                className: 'list-item-label interactive',
            },
            fromRecord(d.name)
        ),
        SPAN(
            {
                className: 'description',
            },
            fromNullable(d.description).fold('' as Translated, fromRecord)
        )
    );

// tslint:disable-next-line: variable-name
export const DomainList = () =>
    DIV(
        { className: 'panel' },
        DomainHelp(),
        H2({}, tr.angled('domains')),
        renderCreateButton(),
        DIV(
            { className: 'list domain-list' },
            ...getDomainList().map(renderDomain)
        )
    );
