import { fromNullable } from 'fp-ts/lib/Option';

import { DIV, H2, SPAN, STRONG, NODISPLAY } from 'sdi/components/elements';
import tr, { fromRecord, Translated } from 'sdi/locale';
import { isUserLogged } from 'sdi/app/queries';
import { makeLabelAndIcon } from 'sdi/components/button';

import {
    getCurrentDomain,
    getCurrentTerm,
} from 'angled-universe/src/queries/universe';
import {
    navigateTerm,
    navigateTermNew,
} from 'angled-universe/src/events/route';
import { clearForm } from 'angled-universe/src/events/universe';
import { termsInDomain } from 'angled-core/queries/universe';
import { Term, Domain } from 'angled-core/ref';

import { isActiveButton, goToLoginPage } from '../buttons';
import { DomainDetail } from '../domain';

const createButton = makeLabelAndIcon('add', 1, 'plus', () =>
    tr.angled('addTerm')
);

const onPushButton = () => {
    if (isUserLogged()) {
        clearForm();
        navigateTermNew();
    } else {
        goToLoginPage();
    }
};

const renderCreateButton = () =>
    createButton(() => onPushButton(), isActiveButton());

const isActive = (t: Term) => getCurrentTerm().fold(false, c => t.id === c.id);

const termClass = (t: Term) =>
    isActive(t) ? 'list-item term active' : 'list-item  term';

const renderTerm = (t: Term) =>
    DIV(
        {
            className: termClass(t),
            onClick: () => navigateTerm(t.id),
        },
        STRONG({}, `${t.id}  `),
        SPAN(
            {
                className: 'list-item-label interactive',
            },
            fromRecord(t.name)
        ),
        SPAN(
            {
                className: 'description',
            },
            fromNullable(t.description).fold('' as Translated, fromRecord)
        )
    );

const renderTermList = (domain: Domain) =>
    DIV(
        { className: 'list term-list' },
        ...termsInDomain(domain.id).map(renderTerm)
    );

// tslint:disable-next-line: variable-name
export const TermList = () =>
    getCurrentDomain().fold(NODISPLAY(), domain =>
        DIV(
            { className: 'panel' },
            DomainDetail(),
            H2({}, tr.angled('terms')),
            renderCreateButton(),
            renderTermList(domain)
        )
    );
