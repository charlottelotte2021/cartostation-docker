import { DIV, NODISPLAY, H2, SPAN } from 'sdi/components/elements';
import { MessageRecordLang } from 'sdi/source';
import tr from 'sdi/locale';

import { inputText, inputLongText, options } from 'sdi/components/input';
import {
    nameGetter,
    Form,
    asTermForm,
    PartialForm,
    descriptionGetter,
} from '../edit';
import {
    saveTerm,
    updateNewTermName,
    saveNewTerm,
    updateTermFormName,
    updateTermFormDescription,
} from 'angled-universe/src/events/universe';
import {
    getEitherForm,
    getNewTermName,
    getCurrentDomain,
} from 'angled-universe/src/queries/universe';
import { makeLabelAndIcon } from 'sdi/components/button';
import { navigateTerm, navigateDomain } from 'angled-universe/src/events/route';
import { getLang } from 'sdi/app';

const saveButton = makeLabelAndIcon('save', 1, 'check', () =>
    tr.angled('saveTerm')
);
const cancelButton = makeLabelAndIcon('cancel', 3, 'times', () =>
    tr.angled('cancel')
);

const newTermName = (ln: MessageRecordLang) =>
    inputText(
        options('form-new-term-name', getNewTermName(ln), updateNewTermName(ln))
    );

const renderNewTermForm = (_pf: PartialForm) =>
    DIV(
        { className: 'inner-form' },
        H2({}, tr.angled('newTerm')),
        DIV(
            { className: 'input-wrapper' },
            DIV({ className: 'input-label' }, tr.angled('nameFR')),
            newTermName('fr')
        ),
        DIV(
            { className: 'input-wrapper' },
            DIV({ className: 'input-label' }, tr.angled('nameNL')),
            newTermName('nl')
        ),
        cancelButton(() => getCurrentDomain().map(d => navigateDomain(d.id))),
        saveButton(() =>
            saveNewTerm().map(p => p.then(d => navigateTerm(d.id)))
        )
    );

const termName = (ln: MessageRecordLang) =>
    inputText(
        options(
            'form-term-name',
            () => nameGetter('term')(ln).getOrElse(''),
            updateTermFormName(ln)
        )
    );

const termDescription = (ln: MessageRecordLang) =>
    inputLongText(
        () => descriptionGetter('term')(ln).getOrElse(''),
        updateTermFormDescription(ln)
    );

const renderFormTerm = (f: Form) =>
    asTermForm(f).fold(NODISPLAY(), tf =>
        DIV(
            { className: 'inner-form' },
            H2(
                {},
                tr.angled('editPrefix'),
                SPAN(
                    { className: 'edit-name' },
                    nameGetter('term')(getLang()).getOrElse('')
                )
            ),
            DIV(
                { className: 'input-wrapper' },
                DIV({ className: 'input-label' }, tr.angled('nameFR')),
                termName('fr'),
                DIV({ className: 'input-label' }, tr.angled('descriptionFR')),
                termDescription('fr')
            ),
            DIV(
                { className: 'input-wrapper' },
                DIV({ className: 'input-label' }, tr.angled('nameNL')),
                termName('nl'),
                DIV({ className: 'input-label' }, tr.angled('descriptionNL')),
                termDescription('nl')
            ),
            cancelButton(() => navigateTerm(tf.data.id)),
            saveButton(() => {
                saveTerm();
                navigateTerm(tf.data.id);
            })
        )
    );

// tslint:disable-next-line: variable-name
export const TermForm = () =>
    DIV(
        { className: 'panel form' },
        getEitherForm('term').fold(
            optF => optF.fold(NODISPLAY(), renderNewTermForm),
            f => renderFormTerm(f)
        )
    );
