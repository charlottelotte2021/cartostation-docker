import { DIV } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { stringToParagraphs } from 'sdi/util';

// tslint:disable-next-line: variable-name
export const TermHelp = () =>
    DIV(
        { className: 'app-infos' },
        stringToParagraphs(tr.angled('appTextDomain'))
    );
