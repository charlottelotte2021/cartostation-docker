import { fromNullable } from 'fp-ts/lib/Option';
import { compose } from 'fp-ts/lib/function';

import { DIV, H1, P, NODISPLAY } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { stringToParagraphs } from 'sdi/util';
import { isUserLogged } from 'sdi/app/queries';
import { makeLabelAndIcon, makeIcon } from 'sdi/components/button';

import {
    getCurrentTerm,
    getCurrentDomain,
} from 'angled-universe/src/queries/universe';
import {
    navigateTermForm,
    navigateDomain,
} from 'angled-universe/src/events/route';
import { Term } from 'angled-core/ref';

import { isActiveButton, goToLoginPage } from '../buttons';

const editButton = makeLabelAndIcon('edit', 1, 'pencil-alt', () =>
    tr.angled('editTerm')
);
const closeButton = makeIcon('close', 3, 'times', {
    position: 'top-right',
    text: () => tr.core('close'),
});

const onPushButton = (id: number) => {
    if (isUserLogged()) {
        navigateTermForm(id);
    } else {
        goToLoginPage();
    }
};

const renderEditButton = (t: Term) =>
    editButton(() => onPushButton(t.id), isActiveButton());

const renderName = (term: Term) =>
    DIV({ className: 'name' }, fromRecord(term.name));

const renderDescription = (term: Term) =>
    DIV(
        { className: 'description' },
        fromNullable(term.description).fold(
            [P({ key: 'renderDescription-none' }, tr.angled('noDescription'))],
            compose(stringToParagraphs, fromRecord)
        )
    );

const renderEdit = (term: Term) =>
    DIV({ className: 'edit' }, renderEditButton(term));

// tslint:disable-next-line: variable-name
export const TermDetail = () =>
    getCurrentTerm().fold(NODISPLAY(), term =>
        DIV(
            { className: 'panel' },
            DIV(
                { className: 'detail term' },
                closeButton(() =>
                    getCurrentDomain().map(d => navigateDomain(d.id))
                ),
                H1({}, renderName(term)),
                renderDescription(term),
                renderEdit(term)
            )
        )
    );
