import * as debug from 'debug';

import { assign, dispatch } from 'sdi/shape';
import { getApiUrl } from 'sdi/app';
import { Lens, LensFromPath } from 'monocle-ts';

import { Domain, Term } from 'angled-core/ref';
import {
    putDomain,
    putTerm,
    postDomain,
    postTerm,
} from 'angled-core/remote/universe';
import { getForm, getNewForm } from '../queries/universe';
import {
    asDomainForm,
    Form,
    DomainForm,
    Editor,
    asTermForm,
    TermForm,
    asPartialDomainForm,
    asPartialTermForm,
} from '../components/edit';
import { fromNullable, fromPredicate, Option } from 'fp-ts/lib/Option';
import { MessageRecordLang } from 'sdi/source';
import { scopeOption } from 'sdi/lib';
import { findDomain, findTerm } from 'angled-core/queries/universe';

const logger = debug('sdi:events/universe');

export const setDomain = (id: number) =>
    findDomain(id).map(({ id }) => {
        assign('universe/domain-id', id);
        assign('universe/term-id', null);
    });

export const setTerm = (id: number) =>
    findTerm(id).foldL(
        () => logger(`ERROR setTerm ${id}`),
        ({ id, domain }) => {
            logger(`setTerm ${id}`);
            assign('universe/domain-id', domain);
            assign('universe/term-id', id);
        }
    );

export const createDomainForm = () =>
    assign('universe/form-new', {
        kind: 'domain',
        isDirty: false,
        isNew: true,
        data: {},
    });

export const createTermForm = (domainId: number) =>
    assign('universe/form-new', {
        kind: 'term',
        isDirty: false,
        isNew: true,
        data: { domain: domainId },
    });

const updateNewDomainForm = (
    u: (
        l: LensFromPath<Partial<Domain>>
    ) => (d: Partial<Domain>) => Partial<Domain>
) =>
    dispatch('universe/form-new', f =>
        fromNullable(f)
            .chain(f => asPartialDomainForm(f))
            .foldL(
                () => {
                    return f;
                },
                f => ({
                    ...f,
                    isDirty: true,
                    data: u(Lens.fromPath<Domain>())(f.data),
                })
            )
    );

export const updateNewDomainName = (ln: MessageRecordLang) => (s: string) =>
    updateNewDomainForm(lens =>
        lens(['name'])
            .asOptional()
            .modify(r => ({ ...r, [ln]: s }))
    );

export const saveNewDomain = () =>
    getNewForm('domain')
        .chain(f => fromNullable(f.data.name))
        .map(name =>
            postDomain(getApiUrl('geodata/angled/r/domain'), { name }).then(
                d => {
                    dispatch('data/domains', ds => ds.concat([d]));
                    assign('universe/form-new', null);
                    return d;
                }
            )
        );

const updateNewTermForm = (
    u: (l: LensFromPath<Partial<Term>>) => (d: Partial<Term>) => Partial<Term>
) =>
    dispatch('universe/form-new', f => {
        const nf = fromNullable(f)
            .chain(f => asPartialTermForm(f))
            .foldL(
                () => {
                    return f;
                },
                f => ({
                    ...f,
                    isDirty: true,
                    data: u(Lens.fromPath<Term>())(f.data),
                })
            );
        logger(f, nf);
        return nf;
    });

export const updateNewTermName = (ln: MessageRecordLang) => (s: string) =>
    updateNewTermForm(lens =>
        lens(['name'])
            .asOptional()
            .modify(r => ({ ...r, [ln]: s }))
    );

export const saveNewTerm = () =>
    scopeOption()
        .let('form', getNewForm('term').chain(asPartialTermForm))
        .let('name', ({ form }) => fromNullable(form.data.name))
        .let('domain', ({ form }) => fromNullable(form.data.domain))
        .map(({ domain, name }) =>
            postTerm(getApiUrl('geodata/angled/r/term'), { name, domain }).then(
                t => {
                    dispatch('data/terms', ts => ts.concat([t]));
                    assign('universe/form-new', null);
                    return t;
                }
            )
        );

export const setForm = (kind: Editor) => (id: number) => {
    switch (kind) {
        case 'domain':
            findDomain(id).map(d =>
                assign('universe/form', {
                    kind,
                    isDirty: false,
                    isNew: false,
                    data: d,
                })
            );
            break;
        case 'term':
            findTerm(id).map(d =>
                assign('universe/form', {
                    kind,
                    isDirty: false,
                    isNew: false,
                    data: d,
                })
            );
            break;
    }
};

const updateDomainForm = (
    u: (l: LensFromPath<Domain>) => (d: Domain) => Domain
) =>
    dispatch('universe/form', f =>
        fromNullable(f)
            .chain(f => asDomainForm(f))
            .fold(f, f => ({
                ...f,
                isDirty: true,
                data: u(Lens.fromPath<Domain>())(f.data),
            }))
    );

export const updateDomainFormName = (l: MessageRecordLang) => (s: string) =>
    updateDomainForm(lens => lens(['name', l]).set(s));

export const updateDomainFormDescription =
    (l: MessageRecordLang) => (s: string) =>
        updateDomainForm(lens =>
            lens(['description'])
                .asOptional()
                .modify(record => ({
                    ...record,
                    [l]: s,
                }))
        );

const dirtyForm = <F extends Form>() =>
    fromPredicate<Form>(f => f.isDirty) as () => Option<F>;

const updateDomain = (du: Domain) =>
    dispatch('data/domains', ds => ds.map(d => (d.id !== du.id ? d : du)));

export const clearForm = () => {
    logger('clearForm');
    assign('universe/form', null);
};

export const saveDomain = () =>
    getForm('domain')
        .chain(dirtyForm<DomainForm>())
        .map(f =>
            putDomain(
                getApiUrl(`geodata/angled/r/domain/${f.data.id}`),
                f.data
            ).then((data: Domain) => {
                clearForm();
                // assign('universe/form', null);
                // assign('universe/form', { ...f, isDirty: false, isNew: false, data });
                updateDomain(data);
            })
        );

const updateTermForm = (u: (l: LensFromPath<Term>) => (t: Term) => Term) =>
    dispatch('universe/form', f =>
        fromNullable(f)
            .chain(f => asTermForm(f))
            .fold(f, f => ({
                ...f,
                isDirty: true,
                data: u(Lens.fromPath<Term>())(f.data),
            }))
    );

export const updateTermFormName = (l: MessageRecordLang) => (s: string) =>
    updateTermForm(lens => lens(['name', l]).set(s));

export const updateTermFormDescription =
    (l: MessageRecordLang) => (s: string) =>
        updateTermForm(lens =>
            lens(['description'])
                .asOptional()
                .modify(record => ({
                    ...record,
                    [l]: s,
                }))
        );

const updateTerm = (tu: Term) =>
    dispatch('data/terms', ds => ds.filter(t => t.id !== tu.id).concat([tu]));

export const saveTerm = () =>
    getForm('term')
        .chain(dirtyForm<TermForm>())
        .map(f =>
            putTerm(
                getApiUrl(`geodata/angled/r/term/${f.data.id}`),
                f.data
            ).then((data: Term) => {
                clearForm();
                updateTerm(data);
            })
        );

logger('loaded');
