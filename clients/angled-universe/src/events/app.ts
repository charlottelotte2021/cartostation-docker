import { assign, dispatch } from 'sdi/shape';

import { fetchUser } from '../remote';
import { AppLayout } from '../shape';

export const loadUser = (url: string) =>
    fetchUser(url).then(user => {
        assign('data/user', user);
    });

export const setLayout = (l: AppLayout) => assign('app/layout', l);

export const unfold = (name: string) =>
    dispatch('component/foldable', folds => ({ ...folds, [name]: false }));

export const fold = (name: string) =>
    dispatch('component/foldable', folds => ({ ...folds, [name]: true }));
