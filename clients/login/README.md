![illustration](../assets/image/login.png)

Access to some _cartostation_ features may require a user account with the corresponding access rights.

This simple application allows authentication.

## Connection LDAP server

The _cartostation_ authentication system can be connected to the LDAP server of the organizations that use it in order to be synchronized with accounts that are otherwise internally activated.

## User guide

The complete user guide is available here : https://cartostation.com/documentation
