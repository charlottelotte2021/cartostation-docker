import { loop, rect, loadAlias, getApiUrl } from 'sdi/app';
import { DIV, NodeOrOptional } from 'sdi/components/elements';
import footer from './components/footer';
import header from 'sdi/components/header';

import { loadRoute } from './events/route';
import { getLayout } from './queries/app';

import home from './components/home';
import {
    general,
    license,
    geology,
    finance,
    technical,
} from './components/geothermie';
import cssPrint from './components/css-print';
import modal from './components/modal';
import {
    loadSelectionOfMaps,
    loadBaseLayers,
    setMainRect,
    loadInfoBulles,
} from './events/app';
import { loadInfoAll } from './events/geothermie';
import tr from 'sdi/locale';
import { loadAllServices } from 'sdi/geocoder/events';

const mainRect = rect(({ width, height }) => setMainRect(width, height));

const wrappedMain = (name: string, ...elements: NodeOrOptional[]) =>
    DIV(
        {
            className: 'geo-inner',
            ref: mainRect,
        },
        modal(),
        header('brugeotool'),
        DIV({ className: `main ${name}` }, ...elements),
        footer()
    );

const renderHome = () => wrappedMain('home', home());

const renderGeneral = () => wrappedMain('general', general());
const renderLicense = () => wrappedMain('license', license());
const renderGeology = () => wrappedMain('geology', geology());
const renderFinance = () => wrappedMain('finance', finance());
const renderTechnical = () => wrappedMain('technical', technical());

const render = () => {
    const layout = getLayout();
    switch (layout) {
        case 'home':
            return renderHome();
        case 'general':
            return renderGeneral();
        case 'license':
            return renderLicense();
        case 'geology':
            return renderGeology();
        case 'finance':
            return renderFinance();
        case 'technical':
            return renderTechnical();
        case 'print':
            return cssPrint();
    }
};

const effects = (initialRoute: string[]) => () => {
    loadAllServices();
    loadRoute(initialRoute);
    loadSelectionOfMaps();
    loadInfoBulles();
    loadBaseLayers();
    loadInfoAll();
    loadAlias(getApiUrl('alias'));
    tr.init_edited();
};

export const app = (initialRoute: string[]) =>
    loop('brugeotool', render, effects(initialRoute));
