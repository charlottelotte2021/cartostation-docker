import * as debug from 'debug';

import { assign, observe, query, dispatch } from 'sdi/shape';
import { IUgWsAddress } from 'sdi/ports/geocoder';
import { MessageRecordLang } from 'sdi/source/io/message';

import {
    System,
    DisplayMapsSeparator,
    SearchType,
    InfoPanel,
} from '../components/geothermie';
import {
    DepthPoint,
    ConstraintPoint,
    DepthTile,
    DepthArray,
} from '../remote/io';
import { snap } from './route';
import {
    fetchTile,
    fetchCapakeyCapakey,
    fetchCapakeyPosition,
    fetchSlice,
} from '../remote';
import { updateCollection, uniqId, center, Nullable } from 'sdi/util';
import { fromNullable, fromPredicate, some } from 'fp-ts/lib/Option';
import {
    CakeMode,
    cakeInZoomRange,
    CakeRes,
    CakeAlpha,
} from '../components/cake';
import {
    getMoreTextHidden,
    getSearchCapakey,
    getCakeMode,
    getShowPartTableGeol,
    getShowPartTableHydro,
    getGraphXy,
    getGraphSnapXy,
    getShowPartTableFinanceThermicNeeds,
    getInfoPanel,
    getCakeRes,
    getDrySatConductivity,
    getDrySatLayersDepths,
    computeCumulDrySatConductivity,
    getArchiExtent,
    BottomRender,
    foldBottomRender,
    getSliceResolution,
} from '../queries/geothermie';
import { MoreTextHidden, Depth } from '../components/table/format';
import {
    ConnectName,
    ConnectData,
    defaultConnect,
    setoidConectData,
} from '../components/table/connect';
import {
    remoteLoading,
    remoteSuccess,
    remoteError,
    remoteNone,
    ILayerInfo,
    Inspire,
    getMessageRecord,
    Feature,
    GeometryObject,
    RemoteResource,
    FeatureCollection,
    DirectGeometryObject,
} from 'sdi/source';
import { index } from 'fp-ts/lib/Array';
import { fitFeature, addPositionMarker, setInteractionSlice } from './map';
import { removeLayer, addLayer, FetchData } from 'sdi/map';
import { getLayout } from '../queries/app';
import {
    getTile,
    getTopView,
    getTopZoom,
    sliceSyntheticInfo,
    SLICE_LAYER_NAME,
    topMapName,
} from '../queries/map';
import { right } from 'fp-ts/lib/Either';
import { SyntheticLayerInfo } from 'sdi/app';
import { Extent } from 'ol/extent';
import { scopeOption } from 'sdi/lib';
import { xToValue, depthToY, snapXy, GraphXY } from '../components/table/graph';
import { GeoMessageKey, updateMessageDB } from '../locale';
import {
    fetchInfoStrati,
    fetchInfoClosed,
    fetchInfoHydroGeol,
    fetchInfoOpen,
    fetchFinanialTableData,
} from '../remote/info';
import { updateValues } from '../components/settings';
import { SliceData } from '../components/slice';
import { DisplayPiezo } from '../components/slice/legend';

const logger = debug('sdi:events/geo');

export const setAddress = (lang: MessageRecordLang, a: IUgWsAddress) =>
    dispatch('geo/address', ga =>
        fromNullable(ga).fold({ [lang]: a }, as =>
            updateCollection(as, lang, a)
        )
    );

export const resetAddress = () => assign('geo/address', null);

export const setSystem = (s: System) => assign('geo/system', s);

export const resetSystem = () => assign('geo/system', null);

export const setDisplayMapsSeparator = (m: DisplayMapsSeparator) =>
    assign('geo/display/maps/separator', m);

export const mapsSeparatorMoveDown = () =>
    dispatch('geo/display/maps/separator', sep => {
        switch (sep) {
            case 'middle':
                return 'bottom-hidden';
            case 'top-hidden':
                return 'middle';
        }
        return sep;
    });

export const mapsSeparatorMoveUp = () =>
    dispatch('geo/display/maps/separator', sep => {
        switch (sep) {
            case 'middle':
                return 'top-hidden';
            case 'bottom-hidden':
                return 'middle';
        }
        return sep;
    });

export const setXY = (x: number, y: number) => assign('geo/xy', { x, y });

export const setDataPoint = (d: RemoteResource<DepthPoint>) => {
    assign('geo/data/point', d);
};

export const setDataConstraint = (c: RemoteResource<ConstraintPoint>) =>
    assign('geo/data/constraint', c);

export const setDisplayData = (b: boolean) =>
    dispatch('geo/display/dataswitch', ps => {
        if (b) {
            return ps.concat('map-data');
        }
        return ps.concat('presentation');
    });

export const toggleDataswitcher = () => {
    if (getInfoPanel() === 'presentation') {
        setDisplayData(true);
    } else {
        setDisplayData(false);
    }
};

export const setInfoPanel = (info: InfoPanel) =>
    dispatch('geo/display/dataswitch', ps => ps.concat(info));

export const popInfoPanel = () =>
    dispatch('geo/display/dataswitch', ps => ps.slice(0, ps.length - 1));

export const tileKeyFromExtent = ([minx, miny, maxx, maxy]: Extent) =>
    `${snap(minx, 10)}/${snap(miny, 10)}/${snap(maxx, 1)}/${snap(
        maxy,
        1
    )}/${getCakeRes()}`;

const { putInFlight, getInflight, clearInflight } = (() => {
    let inFlightTiles: string[] = [];

    const putInFlight = (key: string) => inFlightTiles.push(key);

    const getInflight = (key: string) => inFlightTiles.indexOf(key) >= 0;

    const clearInflight = (key: string) =>
        (inFlightTiles = inFlightTiles.filter(k => k !== key));

    return { putInFlight, getInflight, clearInflight };
})();

export const getAdjustedExtent = ([minx, miny, maxx, maxy]: Extent) => {
    /*
     *     ___trapezeBase___
     *    |\               /|
     *    | \      .      / | trapezeHeight
     *    |  \           /  |
     *    |   \_________/   |
     *
     *
     * A trapeze with the 3 small edges being of same length
     * - oh no, the new deal is a trapeze with projections of the 3 small edges are the  same length
     * with · being 'center', and the small angle of diagonal edges being alpha
     *
     */

    // const ALPHA = 0.483; /* angle in radian */
    // const center = [(minx + maxx) / 2, (miny + maxy) / 2];
    // const edge = Math.min(maxx - minx, maxy - miny);
    // const REDUCING_FACTOR = 1.5;
    // // const REDUCING_FACTOR = 1.1;

    // const trapezeBase = edge * REDUCING_FACTOR;
    // const trapezeHeight =
    //     trapezeBase / (2 * Math.tan(ALPHA) + 1 / Math.cos(ALPHA));
    // // const trapezeHeight = (trapezeBase / (3 * Math.tan(ALPHA))) * 1.1; //no idea why we need the 1.1. to check (nw)

    // return [
    //     center[0] - trapezeBase / 2,
    //     center[1] - trapezeHeight / 2,
    //     center[0] + trapezeBase / 2,
    //     center[1] + trapezeHeight / 2,
    // ];

    const extentWidth = maxx - minx;
    const extentHeight = maxy - miny;
    let width = extentWidth * 0.7;
    let height = width * 0.66;
    if (height > extentHeight) {
        height = extentHeight * 0.9;
        width = height / 0.66;
    }
    const offsetX = (extentWidth - width) / 2;
    const offsetY = (extentHeight - height) / 2;
    return [
        minx + offsetX,
        miny + offsetY,
        minx + offsetX + width,
        miny + offsetY + height,
    ];
};

export const loadTile = (key: string, extent: Extent) => {
    const res = getCakeRes();
    if (!getInflight(key)) {
        putInFlight(key);
        const [minx, miny, maxx, maxy] = getAdjustedExtent(extent);
        const cons = (t: DepthTile) =>
            dispatch('geo/tiles', ts => updateCollection(ts, key, t));

        fetchTile(
            res,
            snap(minx, 10),
            snap(miny, 10),
            snap(maxx, 10),
            snap(maxy, 10)
        )
            .then(cons)
            .then(() => clearInflight(key))
            .catch(err => logger(`Fetch tile error: ${err}`));
    }
};

export const updateTiles = (extent: Nullable<Extent>) => {
    logger(`updateTiles ${extent}`);
    if (getLayout() === 'home' && foldBottomRender('cake', true, false)) {
        fromNullable(extent).map(extent => {
            const key = tileKeyFromExtent(extent);
            const tiles = query('geo/tiles');
            if (!(key in tiles)) {
                loadTile(key, extent);
            }
        });
        updateCakeOnMap();
    }
};

observe('port/map/top/view', ({ extent }) => updateTiles(extent));
observe('geo/cake/mode', () => updateCakeOnMap());
observe('geo/bottom-render', state =>
    fromNullable(state)
        .chain(fromPredicate(n => n === 'cake'))
        .map(updateCakeOnMap)
);
observe('app/layout', () => updateCakeOnMap());

export const setGraphXy = (xy: GraphXY | null) => assign('geo/graph/xy', xy);

export const setGraphSnapXy = (xy: GraphXY | null) =>
    assign('geo/graph/snap-xy', xy);

export const resetGraphXY = () => {
    setGraphXy(null);
    setGraphSnapXy(null);
};

export const setShowPartTableGeol = (b: boolean) =>
    assign('geo/table/show/geol', b);

export const setShowPartTableHydro = (b: boolean) =>
    assign('geo/table/show/hydro', b);

export const changeShowPartTableGeol = () => {
    const b = getShowPartTableGeol();
    assign('geo/table/show/geol', !b);
};

export const changeShowPartTableHydro = () => {
    const b = getShowPartTableHydro();
    assign('geo/table/show/hydro', !b);
};

export const toggleMoreTextHidden = (
    key: keyof MoreTextHidden,
    index: number
) => {
    const mth = getMoreTextHidden();
    const arr = mth[key];
    arr[index] === true ? (arr[index] = false) : (arr[index] = true);
    const newMth = {} as MoreTextHidden;

    const keys = Object.keys(mth) as (keyof MoreTextHidden)[];
    keys.map(k => (k === key ? (newMth[k] = arr) : (newMth[k] = mth[k])));
    assign('geo/table/text-hidden', newMth);
};

export const setMoreTextHiddenColumWise = (
    key: keyof MoreTextHidden,
    hidden: boolean
) => {
    const mth = getMoreTextHidden();
    let arr = mth[key];
    arr = arr.map(() => hidden);
    const newMth = {} as MoreTextHidden;
    const keys = Object.keys(mth) as (keyof MoreTextHidden)[];
    keys.map(k => (k === key ? (newMth[k] = arr) : (newMth[k] = mth[k])));
    assign('geo/table/text-hidden', newMth);
};

export const toggleShowPartTableFinanceThermicNeeds = () => {
    const b = getShowPartTableFinanceThermicNeeds();
    assign('geo/finance/show/thermic/needs', !b);
};

export const setBottomRender = (b: BottomRender) => {
    assign('geo/bottom-render', b);
    window.setTimeout(() => getArchiExtent().map(updateTiles), 64 * 2);
};

export const clearBottomRender = () => {
    assign('geo/bottom-render', null);
    assign('geo/display/maps/separator', 'bottom-hidden');
};

export const setCakeMode = (m: CakeMode) => assign('geo/cake/mode', m);

export const setCakeQuatAlpha = (a: CakeAlpha) =>
    assign('geo/cake/quat-alpha', a);

export const setCakeRes = (r: CakeRes) => {
    assign('geo/cake/res', r);
    getArchiExtent().map(updateTiles);
};

export const setSliceData = (a: SliceData) => assign('geo/slice/data', a);

export const updateConnect = (
    name: ConnectName,
    updater: (d: ConnectData) => ConnectData
) => {
    const oldData = fromNullable(query('geo/connect')[name]).getOrElse(
        defaultConnect()
    );
    const newData = updater(oldData);
    if (!setoidConectData.equals(oldData, newData)) {
        dispatch('geo/connect', data => updateCollection(data, name, newData));
    }
};

export const setDiagramHeight = (n: number) => {
    // if (getLayout() === 'print' && n > 800) {
    //     assign('geo/diagram/height', 800);
    // }
    // else if (n < 2000) {
    //     dispatch('geo/diagram/height', old => Math.abs(n - old) < 12 ? old : n);
    // }
    assign('geo/diagram/height', n);
};

export const setSearchType = (st: SearchType) => assign('geo/search/type', st);

export const searchCapakeyInit = () => {
    assign('geo/search/capakey/result', remoteNone);
    setSearchCapakey('');
};

export const setSearchCapakey = (s: string) => assign('geo/search/capakey', s);

export const setSearchCoordinates = (s: string) =>
    assign('geo/search/coordinates', s);

export const searchCapakey = () =>
    getSearchCapakey().map(ck => {
        assign('geo/search/capakey/result', remoteLoading);
        fetchCapakeyCapakey(ck)
            .then(fs => {
                assign('geo/search/capakey/result', remoteSuccess(fs));
                index(0, fs.features).map(fitFeature);
                index(0, fs.features).map(f =>
                    addPositionMarker(center(f.geometry))
                );
            })
            .catch(err =>
                assign('geo/search/capakey/result', remoteError(`${err}`))
            );
    });

// export const searchCoordinates = () =>
//     parseSearchCoordinates().map(([x, y]) => {
//         addPositionMarker([x, y]);
//         centerMapOn(x, y);
//     });

export const setPinnable = (p: boolean) => assign('geo/map/interaction/pin', p);

export const setMeasureBox = (b: boolean) => assign('geo/map/measure/box', b);

export const loadCurrentCapakey = (x: number, y: number) => {
    assign('geo/capakey/current', remoteLoading);
    fetchCapakeyPosition(x, y)
        .then(fs => {
            assign('geo/capakey/current', remoteSuccess(fs));
            // index(0, fs.features).map(fitFeature);
        })
        .catch(err => assign('geo/capakey/current', remoteError(`${err}`)));
};

export const CAKE_ON_MAP_LAYER = '__CAKE_ON_MAP__';
const cakeOnMapMetadata: Inspire = {
    id: CAKE_ON_MAP_LAYER,
    geometryType: 'LineString',
    resourceTitle: { fr: 'cake', nl: 'cake', en: 'cake' },
    resourceAbstract: { fr: 'cake', nl: 'cake', en: 'cake' },
    uniqueResourceIdentifier: CAKE_ON_MAP_LAYER,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: {
        creation: '2018-04-12T14:51:27.335376Z',
        revision: '2018-04-12T14:51:27.335030Z',
    },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: '2018-04-12T14:51:27.335030Z',
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
};
export const cakeOnMapLayer: ILayerInfo = {
    id: CAKE_ON_MAP_LAYER,
    legend: null,
    group: null,
    metadataId: CAKE_ON_MAP_LAYER,
    visible: true,
    featureViewOptions: { type: 'default' },
    style: {
        kind: 'line-simple',
        strokeColor: 'black',
        dash: [],
        strokeWidth: 1,
    },
    layerInfoExtra: null,
    visibleLegend: true,
    opacitySelector: false,
};

// const rowWidth = (row: DepthRow) => row[row.length - 1].x - row[0].x;

const tileToFeature = (tile: DepthTile, extent: Extent): Feature => {
    const borderBottom = extent[1];
    const borderTop = extent[3];
    const getStep = (n: number) => Math.ceil(n / 2);
    const len = tile.data[0].length;

    const isInside = (ty: number) => ty <= borderTop && ty >= borderBottom;
    const geometry = (rows: DepthArray): GeometryObject => {
        const rf = rows[0];
        const rl = rows[rows.length - 1];
        const colfLeftTop = rf[0];
        const colfLeftBottom = rl[0];
        const colRightTop = rf[rf.length - 1];
        const colRightBottom = rl[rl.length - 1];

        return {
            type: 'LineString',
            coordinates: [
                [colfLeftTop.x, colfLeftTop.y],
                [colRightTop.x, colRightTop.y],
                [colRightBottom.x, colRightBottom.y],
                [colfLeftBottom.x, colfLeftBottom.y],
                [colfLeftTop.x, colfLeftTop.y],
            ],
        };
    };

    const feature = (rows: DepthArray): Feature => ({
        type: 'Feature',
        properties: {},
        id: uniqId(),
        geometry: geometry(rows),
    });

    const cm = getCakeMode();

    switch (cm) {
        case 'left-cut':
            return feature(
                tile.data
                    .map((row, i) => row.slice(getStep(i)))
                    .filter(row => row.length > 0 && isInside(row[0].y))
            );
        case 'right-cut':
            return feature(
                tile.data
                    .map((row, i) =>
                        row.slice(
                            0,
                            Math.floor(row.length / 2) + getStep(len - i)
                        )
                    )
                    .filter(row => row.length > 0 && isInside(row[0].y))
            );
        case 'double-cut':
            return feature(
                tile.data
                    .map((row, i) =>
                        row.slice(
                            getStep(i),
                            Math.floor(len / 2) + getStep(len - i)
                        )
                    )
                    .filter(row => row.length > 0 && isInside(row[0].y))
            );
        case 'uncut':
            return feature(
                tile.data.filter(row => row.length > 0 && isInside(row[0].y))
            );
    }
};

export const updateCakeOnMap = () => {
    removeLayer(topMapName, CAKE_ON_MAP_LAYER);
    if (
        getLayout() === 'home' &&
        foldBottomRender('cake', true, false) &&
        cakeInZoomRange() === 'in'
    ) {
        scopeOption()
            .let('tile', getTile())
            .let('extent', getArchiExtent())
            .foldL(
                () => {
                    window.setTimeout(updateCakeOnMap, 500);
                },
                ({ tile, extent }) => {
                    const info: SyntheticLayerInfo = {
                        name: getMessageRecord(cakeOnMapMetadata.resourceTitle),
                        info: cakeOnMapLayer,
                        metadata: cakeOnMapMetadata,
                    };
                    addLayer(
                        topMapName,
                        () => some(info),
                        () =>
                            right(
                                some({
                                    type: 'FeatureCollection',
                                    features: [tileToFeature(tile, extent)],
                                })
                            )
                    );
                }
            );
    }
};

export const updateDepth = (
    depth: number,
    depths: number[],
    values: number[],
    cumulValues: number[]
) => {
    setGraphXy({ x: getGraphXy().fold(0, xy => xy.x), y: depthToY(depth) });
    snapXy(depths, values, cumulValues);
    assign(
        'geo/form/conductivity',
        xToValue(getGraphSnapXy().fold(0, xy => xy.x))
    );
    assign('geo/form/depth', depth);
};

export const setInputDepth = (depth: number | null) => {
    assign('geo/form/depth', depth);
    if (depth !== null) {
        updateDepth(
            depth,
            getDrySatLayersDepths(),
            getDrySatConductivity(),
            computeCumulDrySatConductivity()
        );
    }
};

export const setInputUH = (uh: GeoMessageKey) => assign('geo/form/uh', uh);

export const setDepthMode = (d: Depth) => assign('geo/depth/mode', d);

export const loadInfoAll = () =>
    Promise.all([
        fetchInfoStrati(),
        fetchInfoClosed(),
        fetchInfoHydroGeol(),
        fetchInfoOpen(),
        fetchFinanialTableData(),
    ])
        .then(([strati, closed, hgeol, open, fintabledata]) => {
            assign('geo/info/strati', strati);
            assign('geo/info/closed', closed);
            assign('geo/info/hgeol', hgeol);
            assign('geo/info/open', open);
            assign('geo/finance/table/data', fintabledata);

            updateValues(strati, closed, hgeol, open);
            updateMessageDB(strati, open, closed);
        })
        .catch(err => logger(`Failed to load infos: ${err}`));

export const setWMSLegendVisible = (b: boolean) =>
    assign('geo/legend/wms-visible', b);

export const initSlicing = () =>
    fromNullable(getTopView().extent).map(([minx, miny, maxx, maxy]) => {
        const y = miny + (maxy - miny) / 2;
        const sliceLength = (maxx - minx) * (getTopZoom() > 8 ? 0.1 : 0.3);
        const x0 = minx + sliceLength;
        const x1 = maxx - sliceLength;

        const geometry: DirectGeometryObject = {
            type: 'LineString',
            coordinates: [
                [x0, y],
                [x1, y],
            ],
        };
        const featureCollection: FeatureCollection = {
            type: 'FeatureCollection',
            features: [
                {
                    type: 'Feature',
                    id: 1,
                    geometry,
                    properties: {},
                },
            ],
        };
        const fetchData: FetchData = () => right(some(featureCollection));
        addLayer(topMapName, sliceSyntheticInfo, fetchData);
        setInteractionSlice();
        makeSlice(geometry);
    });

export const endSlicing = () => {
    removeLayer(topMapName, SLICE_LAYER_NAME);
    clearSlice();
};

export const endCake = () => {
    removeLayer(topMapName, CAKE_ON_MAP_LAYER);
};

export const makeSlice = (geometry: DirectGeometryObject) => {
    assign('geo/slice', remoteNone);
    if (geometry.type === 'LineString') {
        if (geometry.coordinates.length > 1) {
            assign('geo/slice/geometry', geometry);
            assign('geo/slice', remoteLoading);
            fetchSlice(getSliceResolution(), geometry)
                .then(data => assign('geo/slice', remoteSuccess(data)))
                .catch(err => assign('geo/slice', remoteError(err.toString())));
        }
    }
};

export const clearSlice = () => assign('geo/slice/geometry', null);

export const setDepthScaling = (s: number) => assign('geo/depth-scaling', s);

export const setDisplayPiezo = (key: keyof DisplayPiezo, b: boolean) => {
    const piezoDisplay = query('geo/slice/piezo');
    const newPiezoDisplay = {} as DisplayPiezo;
    Object.assign(newPiezoDisplay, piezoDisplay);
    newPiezoDisplay[key] = b;
    assign('geo/slice/piezo', newPiezoDisplay);
};

export const setDisplayPhreatic = (b: boolean) =>
    assign('geo/slice/phreatic', b);

export const setMapButtonHelpText = (b: boolean) =>
    assign('geo/map/button/helptext', b);

logger('loaded');
