import { assign, dispatch, query, observe, dispatchK } from 'sdi/shape';
import { IUgWsResponse } from 'sdi/ports/geocoder';
import {
    IMapInfo,
    ILayerInfo,
    getMessageRecord,
    Feature,
    Inspire,
    MessageRecord,
} from 'sdi/source';
import { updateCollection, isNotNullNorUndefined, uniqId } from 'sdi/util';
import {
    removeLayerAll,
    addLayer,
    viewEventsFactory,
    scaleEventsFactory,
    IViewEvent,
    defaultInteraction,
    singleClickInteraction,
    PrintResponse,
    PrintRequest,
    measureEventsFactory,
    removeLayer,
    ViewDirt,
} from 'sdi/map';

import {
    getCurrentMapId,
    findMap,
    findMetadata,
    getInteraction,
    getLayerData,
    bottomMapName,
    topMapName,
    MapName,
} from '../queries/map';
import { fetchMap, fetchMetadata, fetchLayer } from '../remote';
import { some, fromNullable } from 'fp-ts/lib/Option';
import { right } from 'fp-ts/lib/Either';
import { setDisplayData, setPinnable, cakeOnMapLayer } from './geothermie';
import { flatten } from 'fp-ts/lib/Array';
import { Coordinate } from 'ol/coordinate';
import { nameToCode } from 'sdi/components/button/names';

export const setGeocoderInput = (i: string) =>
    assign('component/geocoder/input', i);

export const setGeocoderResponse = (r: IUgWsResponse | null) =>
    assign('component/geocoder/response', r);

export const loadLayerData = (url: string) =>
    fromNullable(query('data/layers')[url]).foldL(
        () =>
            fetchLayer(url)
                .then(layer => {
                    dispatch('data/layers', state =>
                        updateCollection(state, url, layer)
                    );
                })
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                .catch(_err => {
                    // TODO
                }),
        () => Promise.resolve()
    );

export const setInteraction = dispatchK('port/map/interaction');

export const { startMeasureLength, startMeasureArea, stopMeasure } =
    measureEventsFactory(setInteraction, getInteraction);

export const setInteractionSelect = () => {
    assign('port/map/interaction', defaultInteraction());
    setPinnable(false);
};

export const setInteractionClick = () => {
    assign('port/map/interaction', singleClickInteraction());
    setPinnable(false);
};

export const setInteractionPrint = () =>
    assign('port/map/interaction', {
        label: 'print',
        state: null,
    });

export const setInteractionSlice = () =>
    assign('port/map/interaction', {
        label: 'modify',
        state: {
            selected: 1,
            geometryType: 'LineString',
            centerOnSelected: false,
        },
    });

export const startMark = () =>
    dispatch('port/map/interaction', s => {
        if (s.label === 'mark') {
            return { ...s, state: { ...s.state, started: true } };
        }
        return s;
    });

export const endMark = setInteractionSelect;

// export const putMark = (coordinates: Coordinate) =>
//     assign('port/map/interaction', {
//         label: 'mark',
//         state: {
//             started: false,
//             endTime: Date.now() + 200000,
//             coordinates,
//         },
//     });

// const loadedLayers: string[] = [];

const loadLayer = (name: MapName) => (info: ILayerInfo) => {
    // if (loadedLayers.indexOf(info.id) < 0) {
    //     loadedLayers.push(info.id);
    findMetadata(info.metadataId).foldL(
        () => {
            fetchMetadata(info.metadataId).then(md => {
                dispatch('data/metadatas', mds => mds.concat(md));
                loadLayer(name)(info);
            });
        },
        metadata => {
            addLayer(
                name,
                () =>
                    some({
                        name: getMessageRecord(metadata.resourceTitle),
                        info,
                        metadata,
                    }),
                () => getLayerData(metadata.uniqueResourceIdentifier)
            );
            loadLayerData(metadata.uniqueResourceIdentifier).then(() => void 0);
        }
    );
    // }
};

const loadMapFromInfo = (name: MapName, info: IMapInfo) => {
    removeLayerAll(name);
    clearGroupVisibility();
    clearGroupFolding();
    info.layers.filter(l => l.visible).forEach(loadLayer(name));
    info.layers
        .filter(l => !l.visible)
        .forEach(layerInfo =>
            fetchMetadata(layerInfo.metadataId).then(md =>
                dispatch('data/metadatas', mds => mds.concat(md))
            )
        );
};

const loadMapWithMid = (name: MapName) => (mid: string) =>
    findMap(mid).foldL(
        () => {
            fetchMap(mid)
                .then(info => {
                    info.layers.push(cakeOnMapLayer);
                    dispatch('data/maps', maps => maps.concat(info));
                    loadMapFromInfo(name, info);
                })
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                .catch(_response => {
                    // TODO
                });
        },
        info => {
            loadMapFromInfo(name, info);
        }
    );

export const loadMap = (name: MapName) =>
    getCurrentMapId(name).map(loadMapWithMid(name));

observe('geo/map/top', mid => {
    if (isNotNullNorUndefined(mid)) {
        loadMap(topMapName);
    }
});

observe('geo/map/bottom', mid => {
    if (isNotNullNorUndefined(mid)) {
        loadMap(bottomMapName);
    }
});

export const setTopMap = (mid: string) => assign('geo/map/top', mid);

export const setBottomMap = (mid: string) => assign('geo/map/bottom', mid);

export const dispatchView = dispatchK('port/map/top/view');

export const updateMapView = (name: MapName) => {
    const top = viewEventsFactory(dispatchView);
    const bottom = viewEventsFactory(dispatchK('port/map/bottom/view'));
    switch (name) {
        case topMapName:
            return (data: IViewEvent) => {
                top.updateMapView(data);
            };
        case bottomMapName:
            return (data: IViewEvent) => {
                bottom.updateMapView(data);
            };
    }
};

export const centerMapOn = (x: number, y: number) =>
    dispatch('port/map/top/view', v => ({
        ...v,
        dirty: 'geo' as ViewDirt,
        center: [x, y] as Coordinate,
    }));

export const zoomIn = () =>
    dispatch('port/map/top/view', v => ({
        ...v,
        dirty: 'geo' as ViewDirt,
        zoom: v.zoom + 1,
    }));

export const zoomOut = () =>
    dispatch('port/map/top/view', v => ({
        ...v,
        dirty: 'geo' as ViewDirt,
        zoom: v.zoom - 1,
    }));

export const resetRotate = () => {
    dispatch('port/map/top/view', v => ({
        ...v,
        dirty: 'geo' as ViewDirt,
        rotation: 0,
    }));
    // we know it's a bit dirty, but it has to be quick here
    window.setTimeout(
        () =>
            dispatch('port/map/top/view', v => ({
                ...v,
                dirty: 'geo' as ViewDirt,
                rotation: 0,
            })),
        1500
    );
};
export const fitFeature = (feature: Feature) =>
    dispatch('port/map/top/view', v => ({
        ...v,
        dirty: 'geo/feature' as ViewDirt,
        feature,
    }));

export const { setScaleLine } = scaleEventsFactory(dispatchK('port/map/scale'));

export const setSelectedFeature = (lid: string, id: string | number) => {
    setDisplayData(true);
    assign('port/map/select', {
        layerId: lid,
        featureId: id,
    });
};

export const clearSelectedFeature = (then: null | (() => void)) => {
    // popInfoPanel();
    assign('port/map/select', null);
    if (then !== null) {
        then();
    }
};

export const updateLayer = (lid: string, f: (l: ILayerInfo) => ILayerInfo) =>
    dispatch('data/maps', maps => {
        for (let i = 0; i < maps.length; i += 1) {
            for (let j = 0; j < maps[i].layers.length; j += 1) {
                if (maps[i].layers[j].id === lid) {
                    maps[i].layers[j] = f(maps[i].layers[j]);
                }
            }
        }
        return maps;
    });

export const setLayerVisibility = (lid: string, visible: boolean) => {
    const simpleUpdate = (layer: ILayerInfo) => ({ ...layer, visible });
    const updateAndLoad = (layer: ILayerInfo) => {
        if (!layer.visible && visible) {
            loadLayer(topMapName)(layer);
        }
        return { ...layer, visible };
    };
    const updater = visible ? updateAndLoad : simpleUpdate;

    updateLayer(lid, updater);
};

export const setGroupVisibility = (gid: string, visible: boolean) => {
    flatten(
        query('data/maps').map(({ layers }) =>
            layers
                .filter(l => l.group !== null && l.group.id === gid)
                .map(({ id }) => id)
        )
    ).forEach(lid => setLayerVisibility(lid, visible));
    dispatch('geo/legend/group/visibility', s =>
        updateCollection(s, gid, visible)
    );
};

export const clearGroupVisibility = () =>
    assign('geo/legend/group/visibility', {});

export const setGroupFolding = (gid: string, folded: boolean) =>
    dispatch('geo/legend/group/folding', s => updateCollection(s, gid, folded));

export const clearGroupBulleVisibility = () =>
    assign('geo/legend/group/bulle', {});

export const setGroupBulleVisibility = (gid: string, v: boolean) =>
    dispatch('geo/legend/group/bulle', s => updateCollection(s, gid, v));

export const clearGroupFolding = () => assign('geo/legend/group/folding', {});

export const setLayerInfo = (lid: string, info: boolean) =>
    dispatch('geo/legend/layer/info', s => updateCollection(s, lid, info));

export const clearLayerInfo = () => assign('geo/legend/layer/info', {});

export const setBaseLayer = (mapName: MapName) => (name: string) =>
    getCurrentMapId(mapName).map(mid =>
        dispatch('data/maps', maps => {
            for (let i = 0; i < maps.length; i += 1) {
                if (maps[i].id === mid) {
                    maps[i].baseLayer = name;
                    break;
                }
            }
            return maps;
        })
    );

export const setPrintRequest = (r: PrintRequest<Record<string, unknown>>) =>
    assign('port/map/printRequest', r);
export const setPrintResponse = (r: PrintResponse<Record<string, unknown>>) =>
    assign('port/map/printResponse', r);

export const POSITION_MARKER_LAYER = '__POSITION_MARKER_LAYER __';

export const layerPositionMarker: ILayerInfo = {
    id: POSITION_MARKER_LAYER,
    legend: null,
    group: null,
    metadataId: POSITION_MARKER_LAYER,
    visible: true,
    featureViewOptions: { type: 'default' },
    style: {
        kind: 'point-discrete',
        propName: 'type',
        groups: [
            {
                values: ['white'],
                marker: {
                    codePoint: nameToCode('circle'),
                    size: 20,
                    color: 'white',
                },
                label: {},
            },
            {
                values: ['black-out'],
                marker: {
                    codePoint: nameToCode('circle'),
                    size: 20,
                    color: 'black',
                },
                label: {},
            },
            {
                values: ['black-in'],
                marker: {
                    codePoint: nameToCode('circle'),
                    size: 6,
                    color: 'black',
                },
                label: {},
            },
        ],
    },
    layerInfoExtra: null,
    visibleLegend: true,
    opacitySelector: false,
};

const layerPositionMetadata: Inspire = {
    id: POSITION_MARKER_LAYER,
    geometryType: 'Point',
    resourceTitle: { fr: '-' },
    resourceAbstract: { fr: '-' },
    uniqueResourceIdentifier: POSITION_MARKER_LAYER,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: Date(), revision: Date() },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: Date(),
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
};

export const addPositionMarker = (coordinates: Coordinate) => {
    removePositionMarker();
    assign('geo/map/marker', coordinates);
    const feature = (type: string): Feature => ({
        id: uniqId(),
        type: 'Feature',
        geometry: {
            type: 'Point',
            coordinates,
        },
        properties: { type },
    });
    addLayer(
        topMapName,
        () =>
            some({
                name: { fr: '-' },
                info: layerPositionMarker,
                metadata: layerPositionMetadata,
            }),
        () =>
            right(
                some({
                    type: 'FeatureCollection',
                    features: [
                        feature('white'),
                        feature('black-out'),
                        feature('black-in'),
                    ],
                })
            )
    );
};

export const resetPositionMarker = () => assign('geo/map/marker', null);

export const removePositionMarker = () => {
    resetPositionMarker();
    removeLayer(topMapName, POSITION_MARKER_LAYER);
};

export const updateLoading = (ms: MessageRecord[]) => {
    console.log(`>> updateLoading ${ms.length}`);
    dispatch('port/map/loading', () => ms.concat());
};
