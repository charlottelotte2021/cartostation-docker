import * as debug from 'debug';
import { index } from 'fp-ts/lib/Array';
import { TypeOf, union, literal } from 'io-ts';
import { Router, Path } from 'sdi/router';
import { activityLogger, setLayout } from './app';
import { getNumber } from 'sdi/util';
import { scopeOption } from 'sdi/lib';
import { SystemIO, System } from '../components/geothermie';
import { fromEither } from 'fp-ts/lib/Option';
import { getSystem, getAddress, getXY } from '../queries/geothermie';
import {
    setXY,
    setAddress,
    setDataPoint,
    setDataConstraint,
    resetGraphXY,
    setSystem,
    resetSystem,
    loadCurrentCapakey,
    setShowPartTableGeol,
    setShowPartTableHydro,
    setMoreTextHiddenColumWise,
    setDepthMode,
} from './geothermie';
import { fetchPoint, fetchConstraint } from '../remote';
import { queryReverseGeocoder } from 'sdi/ports/geocoder';
import { setInteractionClick, centerMapOn, setInteractionSelect } from './map';
import { assign } from 'sdi/shape';
import {
    remoteNone,
    remoteLoading,
    remoteSuccess,
    remoteError,
} from 'sdi/source';
import { linkAction, printReportAction } from 'sdi/activity';
import tr from 'sdi/locale';

const logger = debug('sdi:route');

/**
 * We use io-ts here to be able to validate route name
 * on user input URL
 */
// tslint:disable-next-line: variable-name
const RouteIO = union([
    literal('home'),
    literal('general'),
    literal('license'),
    literal('geology'),
    literal('finance'),
    literal('technical'),
    literal('print'),
]);
export type Route = TypeOf<typeof RouteIO>;

// At least each route maps to specific layout
export type Layout = Route;

export const { home, route, navigate } = Router<Route>('brugeotool');

export const snap = (n: number, on = 1) => Math.round(n / on) * on;

const baseRouteParser = (p: Path) =>
    scopeOption()
        .let('x', index(0, p).chain(getNumber))
        .let('y', index(1, p).chain(getNumber))
        .map(({ x, y }) => ({
            x: snap(x),
            y: snap(y),
            system: index(2, p).chain(s => fromEither(SystemIO.decode(s))),
        }));

const initXY = (x: number, y: number) => {
    setXY(x, y);
    assign('geo/capakey/current', remoteNone);
    getAddress()
        .foldL(
            () =>
                Promise.all([
                    queryReverseGeocoder(x, y, 'fr'),
                    queryReverseGeocoder(x, y, 'nl'),
                ]).then(([fr, nl]) => {
                    setAddress('fr', fr.result.address);
                    setAddress('nl', nl.result.address);
                    return Promise.resolve(true);
                }),
            ca => {
                const hasBoth = 'fr' in ca && 'nl' in ca;
                const hasFr = 'fr' in ca;
                // const hasNl = 'nl' in ca;
                if (hasBoth) {
                    return Promise.resolve(true);
                } else if (hasFr) {
                    return queryReverseGeocoder(x, y, 'nl')
                        .then(nl => setAddress('nl', nl.result.address))
                        .then(() => true);
                } else {
                    return queryReverseGeocoder(x, y, 'fr')
                        .then(fr => setAddress('fr', fr.result.address))
                        .then(() => true);
                }
            }
        )
        .catch(err => logger(`Address Error: ${err}`));

    setInteractionClick();
    centerMapOn(x, y);

    setDataPoint(remoteLoading);
    fetchPoint(x, y)
        .then(r => setDataPoint(remoteSuccess(r)))
        .catch(err => setDataPoint(remoteError(`error loading point: ${err}`)));

    setDataConstraint(remoteLoading);
    fetchConstraint(x, y)
        .then(r => setDataConstraint(remoteSuccess(r)))
        .catch(err =>
            setDataConstraint(remoteError(`error loading constraints: ${err}`))
        );

    loadCurrentCapakey(x, y);

    resetGraphXY();
};

// Route handlers

/**
 * The `home` special route for index
 */
home('home', () => {
    setLayout('home');
    setInteractionSelect();
});

route(
    'general',
    route => {
        route.map(({ x, y, system }) => {
            initXY(x, y);
            system.foldL(resetSystem, setSystem);
            setDepthMode('relative');
            activityLogger(linkAction('tab-1-general'));
        });
        setLayout('general');
    },
    baseRouteParser
);

route(
    'license',
    route => {
        route.map(({ x, y, system }) => {
            initXY(x, y);
            system.foldL(resetSystem, setSystem);
            activityLogger(linkAction('tab-2-steps-legal'));
        });
        setLayout('license');
    },
    baseRouteParser
);

route(
    'geology',
    route => {
        route.map(({ x, y, system }) => {
            initXY(x, y);
            system.foldL(resetSystem, setSystem);
            activityLogger(linkAction('tab-3-geology-data'));
        });
        setLayout('geology');
    },
    baseRouteParser
);

route(
    'finance',
    route => {
        route.map(({ x, y, system }) => {
            initXY(x, y);
            system.foldL(resetSystem, setSystem);
            activityLogger(linkAction('tab-4-dimensioning'));
        });
        setLayout('finance');
    },
    baseRouteParser
);

route(
    'technical',
    route => {
        route.map(({ x, y, system }) => {
            initXY(x, y);
            system.foldL(resetSystem, setSystem);
            activityLogger(linkAction('geo-drill'));
        });
        setLayout('technical');
    },
    baseRouteParser
);

route(
    'print',
    route => {
        route.map(({ x, y, system }) => {
            initXY(x, y);
            system.foldL(resetSystem, setSystem);
        });
        setLayout('print');
    },
    baseRouteParser
);

// end of route handlers

/**
 * Initial routing, should be called on loop effects
 * @param initial
 */
export const loadRoute = (initial: string[]) =>
    index(0, initial).map(prefix =>
        RouteIO.decode(prefix).map(c => navigate(c, initial.slice(1)))
    );

// navigation functions

export const navigateHome = () => navigate('home', []);

export const navigateGeneral = (x: number, y: number) =>
    navigate('general', [snap(x), snap(y), getSystem().getOrElse('close')]);

export const navigateLicense = (x: number, y: number) =>
    navigate('license', [snap(x), snap(y), getSystem().getOrElse('close')]);

export const navigateGeology = (x: number, y: number) =>
    navigate('geology', [snap(x), snap(y), getSystem().getOrElse('close')]);

export const navigateFinance = (x: number, y: number) =>
    navigate('finance', [snap(x), snap(y), getSystem().getOrElse('close')]);

export const navigateTechnical = (x: number, y: number) =>
    navigate('technical', [snap(x), snap(y)]);

export const navigateSystem = (s: System, l: Layout) =>
    getXY().map(({ x, y }) => {
        setSystem(s);
        switch (l) {
            case 'general':
                return navigateGeneral(x, y);
            case 'license':
                return navigateLicense(x, y);
            case 'geology':
                return navigateGeology(x, y);
            case 'finance':
                return navigateFinance(x, y);
            case 'technical':
                return navigateTechnical(x, y);
        }
    });

export const navigatePrint = () =>
    getXY().map(({ x, y }) => {
        setShowPartTableGeol(true);
        setShowPartTableHydro(true);
        setMoreTextHiddenColumWise('detailledLithology', false);
        getSystem().foldL(
            () => navigate('print', [snap(x), snap(y)]),
            s => navigate('print', [snap(x), snap(y), s])
        );
        activityLogger(
            printReportAction(`${tr.geo('printTitle')}-report.pdf`)
        );
    });

logger('loaded');
