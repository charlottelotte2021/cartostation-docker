import * as debug from 'debug';

import {
    register,
    renderPrintMap,
    renderSmartGeotherm,
} from '../components/modal';

const logger = debug('sdi:events/modal');

export const [closePrintMap, openPrintMap] = register(
    'geo/print-map',
    renderPrintMap
);

export const [, openSmartGeotherm] = register(
    'geo/smart-geotherm',
    renderSmartGeotherm
);

logger('loaded');
