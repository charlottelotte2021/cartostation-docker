import { assign, dispatch, assignK } from 'sdi/shape';
import { Layout } from './route';
import {
    fetchMapSelection,
    fetchMap,
    fetchBaseLayerAll,
    fetchAttachment,
} from '../remote';
import { MapSelection } from '../remote/io';
import { getSelectedMaps, getOrientation } from '../queries/app';
import { index } from 'fp-ts/lib/Array';
import { updateTiles, cakeOnMapLayer } from './geothermie';
import { IMapInfo } from 'sdi/source';
import {
    getSystem,
    getDiagramHeight,
    getArchiExtent,
} from '../queries/geothermie';
import { adjustCarrotConnect } from '../components/carrot/engine';
import { layerPositionMarker } from './map';
import { fetchInfoBulles } from '../remote/info';
import { activity } from 'sdi/activity';
import { sliceInfo } from '../queries/map';

export const activityLogger = activity('brugeotool');

export const setLayout = (l: Layout) => {
    let currentLayout: null | Layout = null;
    dispatch('app/layout', current => {
        currentLayout = current;
        return l;
    });
    if (l === 'home') {
        getArchiExtent().map(updateTiles);
    } else if (l === 'geology') {
        assign('app/layout/persistent', l);
        getSystem().map(sys => {
            if (sys === 'open') {
                adjustCarrotConnect(getDiagramHeight());
            }
        });
    } else if (l === 'print' && currentLayout) {
        if ('print' !== currentLayout) {
            assign('app/layout/persistent', currentLayout);
        }
    } else {
        assign('app/layout/persistent', l);
    }
};

const loadAttachments = (info: IMapInfo) =>
    info.attachments
        .map(fetchAttachment)
        .map(prm =>
            prm
                .then(a => dispatch('data/attachments', ats => ats.concat(a)))
                .catch(err => console.log(`Error fetching attachment ${err}`))
        );

const preselectTopMap = () =>
    index(0, getSelectedMaps('top')).map(s => assign('geo/map/top', s.map));

const preloadSelected = (s: MapSelection) =>
    fetchMap(s.map)
        .then(m => {
            m.layers.push(cakeOnMapLayer);
            m.layers.push(layerPositionMarker);
            m.layers.push(sliceInfo);
            dispatch('data/maps', maps => maps.concat(m));
            loadAttachments(m);
        })
        .then(preselectTopMap)
        .catch(() => {
            /* TODO */
        });

export const loadSelectionOfMaps = () =>
    fetchMapSelection()
        .then(ms => {
            assign('data/selected-maps', ms);
            ms.forEach(preloadSelected);
        })
        .catch(() => {
            /* TODO */
        });

export const loadBaseLayers = () =>
    fetchBaseLayerAll().then(assignK('data/baselayers'));

export const setMainRect = (width: number, height: number) => {
    const orientation = getOrientation();
    const newOrientation = height > width ? 'vertical' : 'horizontal';
    if (newOrientation !== orientation) {
        assign('app/main/rect', [width, height]);
    }
};

export const loadInfoBulles = () =>
    fetchInfoBulles()
        .then(bs => {
            assign('geo/bulles', bs);
        })
        .catch(() => {
            /* TODO */
        });
