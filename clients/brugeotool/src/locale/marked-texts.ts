import { MessageRecord } from 'sdi/source';
import { encodeUrlQuery } from 'sdi/util';
import { getSmartGOThermBaseUrl } from '../components/settings';

export const pagGeneralFR = `
Informations
générales
`;

export const pagGeneralNL = `
Algemene
informatie
`;

export const pagLicenseFR = `
Étapes et
démarches
`;

export const pagLicenseNL = `
Fasen en
stappen
`;

export const pagGeologyFR = `
Données
du sous-sol
`;
export const pagGeologyNL = `
Gegevens
ondergrond
`;
export const pagFinanceFR = `
Dimensionnements
types
`;
export const pagFinanceNL = `
Typische
dimensioneringen
`;

export const finPageTitleFR = `
Dimensionnements types d’installations géothermiques
`;
export const finPageTitleNL = `
Typische dimensioneringen van geothermische installaties
`;

export const finPageRemarkCloseFR = `
* d’autres configurations impliquant une profondeur inférieure et un nombre de sondes supérieur sont possibles.
`;

export const finPageRemarkCloseNL = `
* andere configuraties die minder diep zijn en meer sondes bevatten zijn mogelijk.
`;

export const finPageRemarkOpenFR = `
* un doublet unique bien que suffisant en terme de débit peut poser des problèmes d’approvisionnement dans le cas de l’arrêt d’une pompe.
`;

export const finPageRemarkOpenNL = `
* een enkel doublet met voldoende debiet kan bevoorradingsproblemen opleveren bij het stilvallen van een pomp.
`;

export const finPageTextFR = (openSys: boolean) => `
Ce tableau présente des **dimensionnements types** d’installations géothermiques faible profondeur pour **10 classes de bâtiment** représentatives de la segmentation du bâtiment Bruxellois caractérisée majoritairement par du logement et du bureau.

Les calculs ont été effectués **indépendamment des spécificités géothermiques locales** du lieu sélectionné sur la carte et pour des **classes de bâtiment théoriques** caractérisées par des besoins thermiques de référence.

Les valeurs proposées sont des **valeurs repères représentatives d'un ordre de grandeur** et doivent nécessairement être adaptées aux caractéristiques techniques du projet dans le cadre du [**dimensionnement**](${
    openSys
        ? 'https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/mon-projet-etape-par-etape/systeme-ouvert#dimensionnement'
        : 'https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/mon-projet-etape-par-etape/systeme-ferme#dimensionnement'
}).

[**Plus d’information sur ces valeurs ?**](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/quel-systeme-choisir#exemple)
`;

export const finPageTextNL = (openSys: boolean) => `
Deze tabel bevat **typische dimensioneringen** van ondiepe geothermische installaties voor **10 klassen van gebouwen** die representatief zijn voor de segmentatie van de Brusselse gebouwen die voornamelijk gekenmerkt worden door woningen en kantoren.

De berekeningen werden **onafhankelijk van de lokale geothermische specifieke kenmerken** van de geselecteerde plaats op de kaart uitgevoerd en voor **theoretische klassen van gebouwen** die gekenmerkt worden door thermische referentiebehoeften.

De voorgestelde waarden zijn **richtwaarden die representatief zijn voor een orde van grootte** en die moeten worden aangepast aan de technische eigenschappen van het project in het kader van de [**dimensionering**](${
    openSys
        ? 'https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/mijn-project-stap-voor-stap/open-systeem#dimensionering'
        : 'https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/mijn-project-stap-voor-stap/gesloten-systeem#dimensionering'
}).

[**Meer informatie over deze waarden?**](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/voor-welk-systeem-kiezen#voorbeelden)
`;

export const finPageText = (openSys: boolean): MessageRecord => ({
    fr: finPageTextFR(openSys),
    nl: finPageTextNL(openSys),
});

// export const homeTextFR = `
// **BrugeoTool**
// l’application sous-sol et géothermie bruxelloise.

// Consulter les données
// **géologiques**,
// **hydrogéologiques**
// et **géothermiques**.

// Planifier et maitriser
// les **étapes d’un**
// **projet** de géothermie

// **Pré-dimensionner**
// une installation
// géothermique.
// `;

export const paginatorSidebarFR = `
Il existe deux types de systèmes géothermiques. [Lequel est adapté à mon projet](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/quel-systeme-choisir) ?
`;

export const paginatorSidebarNL = `
Er bestaan twee types van geothermische systemen. [Welke is het meest aangepast aan mijn project?](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/voor-welk-systeem-kiezen) ?
`;

// export const paginatorDisclaimer = `
// Les données et informations fournies sur cette application n’ont qu’une portée indicative.

// Nous vous invitons à consulter les [conditions d’utilisation]().
// `;

const generalInfoOpenOkBeforeFR = (numberAquifere: number) => `

*Sous réserve de l’obtention des autorisations mentionnées dans la section « 2. Etapes et démarches ».

Ce type de système géothermique exploite l’eau souterraine (ou nappe) issue d’un [aquifère](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/eaux-souterraines/hydrogeologie)  pour chauffer et/ou refroidir un bâtiment. [Plus d’information ?](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/la-geothermie-bruxelles)

A cette adresse, ${
    numberAquifere === 1
        ? 'une nappe aquifère est potentiellement exploitable'
        : numberAquifere + ' nappes aquifères sont potentiellement exploitables'
} pour un système ouvert :
`;

const generalInfoOpenOkBeforeNL = (numberAquifere: number) => `

* Onder voorbehoud van het verkrijgen van de vergunningen vermeld in deel « 2. Fasen en stappen».

Dit soort geothermisch systeem exploiteert grondwater (of grondwaterlaag) afkomstig van een waterhoudende laag om een gebouw op te warmen en/of af te koelen. [Meer informatie ?](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/geothermie-brussel)

Op dit adres ${
    numberAquifere === 1
        ? 'is een grondwaterhoudende laag potentieel exploiteerbaar'
        : 'zijn ' +
          numberAquifere +
          '  grondwaterhoudende lagen potentieel exploiteerbaar'
} voor een open systeem:


`;

export const generalInfoOpenOkBefore = (
    numberAquifere: number
): MessageRecord => ({
    fr: generalInfoOpenOkBeforeFR(numberAquifere),
    nl: generalInfoOpenOkBeforeNL(numberAquifere),
});

const aquifere1FR = (depth: number) => `
- [nappe libre](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/eaux-souterraines/hydrogeologie/types-de-nappe-aquifere) (ou phréatique) des sables du *Bruxellien* atteignable à une profondeur d’environ ${depth.toFixed(
    1
)} mètres.
`;

const aquifere1NL = (depth: number) => `
- [vrije grondwaterlaag](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/grondwater/hydrogeologie/soorten-grondwaterlagen) (of freatische grondwaterlaag) van Brusseliaanzand bereikbaar op een diepte van ongeveer  ${depth.toFixed(
    1
)} meter`;

export const aquifere1 = (depth: number): MessageRecord => ({
    fr: aquifere1FR(depth),
    nl: aquifere1NL(depth),
});

const aquifere2FR = (depth: number) => `
- [nappe captive](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/eaux-souterraines/hydrogeologie/types-de-nappe-aquifere) des sables du *Landénien* atteignable à une profondeur d’environ ${depth.toFixed(
    1
)} mètres.
`;

const aquifere2NL = (depth: number) => `
- [artesische grondwaterlaag](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/grondwater/hydrogeologie/soorten-grondwaterlagen) van Landeniaanzand bereikbaar op een diepte van ongeveer ${depth.toFixed(
    1
)} meter.
`;

export const aquifere2 = (depth: number): MessageRecord => ({
    fr: aquifere2FR(depth),
    nl: aquifere2NL(depth),
});

const aquifere3FR = (depth: number) => `
- [nappe captive](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/eaux-souterraines/hydrogeologie/types-de-nappe-aquifere) au sein du socle rocheux, composé de schistes et/ou grès (voire quartzites), atteignable à une profondeur d’environ ${depth.toFixed(
    1
)} mètres.
`;

const aquifere3NL = (depth: number) => `
- [artesische grondwaterlaag](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/grondwater/hydrogeologie/soorten-grondwaterlagen) in de bedrock, samengesteld uit schiefer en/of zandsteen (zelfs kwartsiet), bereikbaar op een diepte van ongeveer ${depth.toFixed(
    1
)} meter.`;

export const aquifere3 = (depth: number): MessageRecord => ({
    fr: aquifere3FR(depth),
    nl: aquifere3NL(depth),
});

const aquifere3US91FR = (depth: number) => `
- [nappe captive](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/eaux-souterraines/hydrogeologie/types-de-nappe-aquifere) au sein du socle rocheux, composé de schistes et/ou grès (voire quartzites) surmontés d’une couche de craies, atteignable à une profondeur d’environ ${depth.toFixed(
    1
)} mètres.
`;

const aquifere3US91NL = (depth: number) => `
- [artesische grondwaterlaag](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/grondwater/hydrogeologie/soorten-grondwaterlagen) in de bedrock, samengesteld uit schiefer en/of zandsteen (zelfs kwartsiet) met daarboven een krijtlaag, bereikbaar op een diepte van ongeveer ${depth.toFixed(
    1
)} meter.`;

export const aquifere3US91 = (depth: number): MessageRecord => ({
    fr: aquifere3US91FR(depth),
    nl: aquifere3US91NL(depth),
});

const generalInfoOpenOKAfterFR = () =>
    `Une [étude de faisabilité hydrogéologique](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/mon-projet-etape-par-etape/systeme-ouvert#etude_faisabilite) doit nécessairement être réalisée pour confirmer
et quantifier le potentiel de la nappe ciblée en vue du [dimensionnement](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/mon-projet-etape-par-etape/systeme-ouvert#dimensionnement) du système géothermique.

Pour faire les bons choix techniques et optimiser le dimensionnement de l’installation,
faites-vous conseiller par un bureau d’étude spécialisé en **hydrogéologie et géothermie**.
`;

const generalInfoOpenOKAfterNL = () => `
Een [hydrogeologische haalbaarheidsstudie](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/mijn-project-stap-voor-stap/open-systeem#haalbaarheidsstudie) moet worden uitgevoerd om het potentieel van de
beoogde waterlaag te bevestigen en te kwantificeren met het oog op de [dimensionering](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/mijn-project-stap-voor-stap/open-systeem#dimensionering) van
het geothermisch systeem.

Om de juiste technische keuzes te maken en de afmeting van de installatie te optimaliseren, kan u zich laten adviseren door een studiebureau dat gespecialiseerd is in **hydrogeologie en geothermie**.
`;

export const generalInfoOpenOkAfter = (): MessageRecord => ({
    fr: generalInfoOpenOKAfterFR(),
    nl: generalInfoOpenOKAfterNL(),
});

const generalInfoClosedOkFR = (depthBedrock: number) => `

*Sous réserve de l’obtention des autorisations mentionnées dans la section « 2. Etapes et démarches ».

Ce type de système exploite l’énergie thermique du sous-sol pour chauffer et/ou refroidir un bâtiment. [Plus d’information ?](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/mon-projet-etape-par-etape/systeme-ferme#besoins_thermiques)

A cette adresse, le sous-sol se compose :
- d’une alternance de sables, de silts et d’argiles (ou sédiments tertiaires) jusqu’à ${depthBedrock.toFixed(
    1
)} mètres de profondeur,
- puis du socle rocheux, composé de schistes et/ou grès (voire quartzites).
Les sondes géothermiques peuvent être forées uniquement à travers les sédiments tertiaires ou être prolongées au sein du socle rocheux. Théoriquement, les roches de ce dernier présentent un meilleur potentiel géothermique. En contrepartie, leur forage peut générer des contraintes opérationnelles additionnelles.

Pour faire les bons choix techniques et optimiser le dimensionnement de l’installation, faites-vous conseiller par un bureau d’étude spécialisé en **géologie et géothermie**.
`;

const generalInfoClosedOkNL = (depthBedrock: number) => `

* Onder voorbehoud van het verkrijgen van de vergunningen vermeld in deel « 2. Fasen en stappen».

Dit soort van systeem maakt gebruik van ondergrondse thermische energie om een gebouw op te warmen en/of af te koelen. [Meer informatie ?](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/geothermie-brussel)

Op dit adres bestaat de ondergrond uit:
- een afwisseling van zand, leem en klei (of tertiaire afzettingen) tot op ${depthBedrock.toFixed(
    1
)} meter diepte,
- dan de bedrock, samengesteld uit schiefer en/of zandsteen (zelfs kwartsiet).
De geothermische sondes kunnen enkel worden geboord door de tertiaire sedimenten of worden verlengd binnen de bedrock. Theoretisch hebben de rotsen van deze laatste een beter geothermisch potentieel. Hun boring kan daarentegen bijkomende operationele beperkingen met zich meebrengen.

Om de juiste technische keuzes te maken en de dimensionering van de installatie te optimaliseren, kan u zich laten adviseren door een studiebureau dat gespecialiseerd is in **geologie en geothermie**.
`;

export const generalInfoClosedOk = (depthBedrock: number): MessageRecord => ({
    fr: generalInfoClosedOkFR(depthBedrock),
    nl: generalInfoClosedOkNL(depthBedrock),
});

const generalInfoClosedOkUS91FR = (depthBedrock: number) => `

*Sous réserve de l’obtention des autorisations mentionnées dans la section « 2. Etapes et démarches ».

Ce type de système exploite l’énergie thermique du sous-sol pour chauffer et/ou refroidir un bâtiment. [Plus d’information ?](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/mon-projet-etape-par-etape/systeme-ferme#besoins_thermiques)

A cette adresse, le sous-sol se compose :
- d’une alternance de sables, de silts et d’argiles (ou sédiments tertiaires) jusqu’à ${depthBedrock.toFixed(
    1
)} mètres de profondeur,
- puis du socle rocheux, composé de schistes et/ou grès (voire quartzites) surmontés d’une couche de craies.
Les sondes géothermiques peuvent être forées uniquement à travers les sédiments tertiaires ou être prolongées au sein du socle rocheux. Théoriquement, les roches de ce dernier présentent un meilleur potentiel géothermique. En contrepartie, leur forage peut générer des contraintes opérationnelles additionnelles.

Pour faire les bons choix techniques et optimiser le dimensionnement de l’installation, faites-vous conseiller par un bureau d’étude spécialisé en **géologie et géothermie**.
`;

const generalInfoClosedOkUS91NL = (depthBedrock: number) => `

* Onder voorbehoud van het verkrijgen van de vergunningen vermeld in deel « 2. Fasen en stappen».

Dit soort van systeem maakt gebruik van ondergrondse thermische energie om een gebouw op te warmen en/of af te koelen. [Meer informatie ?](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/geothermie-brussel)

Op dit adres bestaat de ondergrond uit:
- een afwisseling van zand, leem en klei (of tertiaire afzettingen) tot op ${depthBedrock.toFixed(
    1
)} meter diepte,
- dan de bedrock, samengesteld uit schiefer en/of zandsteen (zelfs kwartsiet) met daarboven een krijtlaag.
De geothermische sondes kunnen enkel worden geboord door de tertiaire sedimenten of worden verlengd binnen de bedrock. Theoretisch hebben de rotsen van deze laatste een beter geothermisch potentieel. Hun boring kan daarentegen bijkomende operationele beperkingen met zich meebrengen.

Om de juiste technische keuzes te maken en de dimensionering van de installatie te optimaliseren, kan u zich laten adviseren door een studiebureau dat gespecialiseerd is in **geologie en geothermie**.
`;

export const generalInfoClosedOkUS91 = (
    depthBedrock: number
): MessageRecord => ({
    fr: generalInfoClosedOkUS91FR(depthBedrock),
    nl: generalInfoClosedOkUS91NL(depthBedrock),
});

export const licenseCloseFR = `
**Étapes et démarches nécessaires à l'installation d'un système géothermique fermé au lieu sélectionné.**

___

### Étude des besoins thermiques
Estimer les besoins énergétiques du bâtiment (en chaud et en froid).

[Plus d’information](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/mon-projet-etape-par-etape/systeme-ferme#besoins_thermiques).
___

### Pré-dimensionnement
Procéder au pré-dimensionnement de l’installation sur la base des données du sous-sol disponibles sur BrugeoTool.

[Plus d’information](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/mon-projet-etape-par-etape/systeme-ferme#predimensionnement).
___

### Demandes de permis et déclarations

>  Un système géothermique fermé est une installation classée de classe 1C, soumise à déclaration*.

Vous trouverez toutes les informations relatives aux déclarations et aux demandes de permis d'environnement sur le [guide pratique du permis d'environnement](https://environnement.brussels/le-permis-denvironnement/le-guide-pratique-du-permis-denvironnement).

Vous trouverez également ici les [formulaires de déclarations et de demandes de permis d'environnement](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/formulaires-de-demande-de-systemes-geothermiques).

*Si votre activité ou votre projet comprend plusieurs installations de classes différentes, vous devez demander le permis qui correspond à [l’installation de la classe la plus élevée](https://environnement.brussels/le-permis-denvironnement/le-guide-pratique-du-permis-denvironnement/comment-preparer-votre-demande-0).

>  Un système géothermique fermé est aussi soumis à une demande de [permis d'urbanisme](https://urbanisme.irisnet.be/lepermisdurbanisme).

___

### Dimensionnement
Forer une sonde test et dimensionner l’installation sur la base des propriétés locales du sous-sol.

[Plus d’information](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/mon-projet-etape-par-etape/systeme-ferme#dimensionnement).
___

### Mise en oeuvre

Installer les sondes, le réseau hydraulique et le dispositif de chauffage.

[Plus d’information](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/mon-projet-etape-par-etape/systeme-ferme#mise_en_oeuvre).
`;

export const licenseCloseNL = `
**Fasen en stappen die nodig zijn om een gesloten geothermisch systeem op de geselecteerde locatie te installeren.**

___

### Studie van thermische noden
De energienoden van het gebouw (qua verwarming en koeling) inschatten.

[Meer informatie](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/mijn-project-stap-voor-stap/gesloten-systeem#behoeften).
___

### Pre-dimensionering
Overgaan tot de pre-dimensionering van de installatie op basis van de gegevens van de ondergrond die beschikbaar zijn via de BrugeoTool.

[Meer informatie](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/mijn-project-stap-voor-stap/gesloten-systeem#predimensionering).
___

### Vergunningsaanvragen en aangiften

>  Een gesloten geothermisch systeem is een installatie geklasseerd onder klasse 1C, die onderworpen is aan een aangifte*.

U vindt alle informatie over aangiften en milieuvergunningsaanvragen in de [praktische gids voor milieuvergunningen](https://leefmilieu.brussels/de-milieuvergunning/praktische-gids-voor-milieuvergunningen).

U vindt hier ook de [formulieren voor milieuvergunningsaanvragen en aangiften](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/formulieren-voor-geothermische-systemen).

*Indien uw activiteit of project **meerdere inrichtingen van verschillende klassen** bevat, moet u de vergunning aanvragen die overeenkomt met de [inrichting van de hoogste klasse](https://leefmilieu.brussels/de-milieuvergunning/praktische-gids-voor-milieuvergunningen/hoe-uw-milieuvergunningsaanvraag-0).

>  Een gesloten geothermisch systeem is ook onderworpen aan een [stedenbouwkundige vergunningsaanvraag](https://stedenbouw.irisnet.be/vergunning?set_language=nl).

___

### Dimensionering
Een testsonde boren en de installatie dimensioneren op basis van de lokale eigenschappen van de ondergrond.

[Meer informatie](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/mijn-project-stap-voor-stap/gesloten-systeem#dimensionering).
___

### Uitvoering

De sondes, het hydraulisch netwerk en het verwarmingssysteem installeren.

[Meer informatie](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/mijn-project-stap-voor-stap/gesloten-systeem#uitvoering).
`;

export const licenseOpenFrCommonHeader = `
**Étapes et démarches nécessaires à l'installation d'un système géothermique ouvert au lieu sélectionné.**

___
`;

export const licenseOpenNlCommonHeader = `
**Fasen en stappen die nodig zijn om een open geothermisch systeem op de geselecteerde locatie te installeren.**

___
`;

export const licenseOpenSoilFrCheck0or01or02 = `
### Reconnaissance de l’état du sol

Le système géothermique est implanté sur une parcelle potentiellement polluée au sens de l’Ordonnance Sol.

Si les travaux ont lieu sur plus de 20m2 de contact avec le sol, une
[reconnaissance de l’état](https://environnement.brussels/thematiques/sols/les-etudes-de-la-pollution-du-sol/les-differentes-etudes/reconnaissance-de-letat-du)
du sol doit-être effectuée en amont du projet et ce avant toute autre démarche.

Si une pollution du sol est découverte (la parcelle passe en catégorie 3, 4, 0+3 ou 0+4), vous devrez alors
vous assurer de la faisabilité de votre projet, en obtenant un avis favorable de la sous-division sol de Bruxelles Environnement.

>  Dans ce cas, il faudra faire établir un rapport par un [expert en pollution du sol agréé](http://app.bruxellesenvironnement.be/listes/?nr_list=EPS0001). Celui-ci le transmettra à Bruxelles Environnement via la [plateforme BRUSOIL](http://brusoil.environnement.brussels/home.html), qui remettra ensuite son avis.

___
`;

export const licenseOpenSoilNlCheck0or01or02 = `
### Verkennend bodemonderzoek

Het geothermisch systeem wordt geplaatst op een mogelijk verontreinigd perceel volgens de Bodemordonnantie.

Indien de werken plaatsvinden op meer dan 20m² in contact met de bodem, is [een verkennend bodemonderzoek](https://leefmilieu.brussels/themas/bodem/de-bodemonderzoeken/de-verschillende-bodemonderzoeken/verkennend-bodemonderzoek) vereist vóór elke andere stap.

Indien een bodemverontreiniging wordt ontdekt (het perceel gaat naar categorie 3, 4, 0+3 of 0+4), moet u zich vergewissen van de haalbaarheid van uw project door een gunstig advies te bekomen van de onderafdeling Bodem van Leefmilieu Brussel.

>  In dit geval moet u een verslag laten opstellen door een [erkend bodemverontreinigingsdeskundige](https://app.leefmilieubrussel.be/lijsten/?nr_list=EPS0001). Die moet het verslag bezorgen aan Leefmilieu Brussel via het [BRUSOIL-platform](http://brusoil.environnement.brussels/nl/home.html), die vervolgens zijn advies zal uitbrengen.

___
`;

export const licenseOpenSoilFrCheck03or04 = `
### Reconnaissance de l’état du sol

Le système géothermique est implanté sur une parcelle polluée au sens de l’Ordonnance Sol et présente encore une suspicion de pollution supplémentaire.

Si les travaux ont lieu sur plus de 20m2 de contact avec le sol, une
[reconnaissance de l’état](https://environnement.brussels/thematiques/sols/les-etudes-de-la-pollution-du-sol/les-differentes-etudes/reconnaissance-de-letat-du)
du sol doit-être effectuée en amont du projet.

Assurez-vous par ailleurs de la faisabilité de votre projet, et ce avant toute autre démarche.

Comment ? En obtenant un avis favorable de la sous-division sol de Bruxelles Environnement.

>  Dans ce cas, il faudra faire établir un rapport par un [expert en pollution du sol agréé](http://app.bruxellesenvironnement.be/listes/?nr_list=EPS0001). Celui-ci le transmettra à Bruxelles Environnement via la [plateforme BRUSOIL](http://brusoil.environnement.brussels/home.html), qui remettra ensuite son avis.

___
`;

export const licenseOpenSoilNlCheck03or04 = `
### Onderzoek naar de staat van de bodem

Het geothermisch systeem wordt geplaatst op een verontreinigd perceel volgens de Bodemordonnantie en er is een vermoeden van bijkomende verontreiniging.

Indien de werken plaatsvinden op meer dan 20m² in contact met de bodem, is [een verkennend bodemonderzoek](https://leefmilieu.brussels/themas/bodem/de-bodemonderzoeken/de-verschillende-bodemonderzoeken/verkennend-bodemonderzoek) vereist in een vroeg stadium van het project.

Verzeker u bovendien van de haalbaarheid van uw project, en dit vóór elke andere stap.

Hoe? Door een gunstig advies te bekomen van de onderafdeling Bodem van Leefmilieu Brussel.

>  Hiervoor moet u een verslag laten opstellen door een [erkend bodemverontreinigingsdeskundige](https://app.leefmilieubrussel.be/lijsten/?nr_list=EPS0001). Die moet het verslag bezorgen aan Leefmilieu Brussel via het [BRUSOIL-platform](http://brusoil.environnement.brussels/nl/home.html), die vervolgens zijn advies zal uitbrengen.
___
`;

export const licenseOpenSoilFrCheck3or4 = `
### Demande d'avis préalable sol

Le système géothermique est implanté sur une parcelle polluée au sens de l’Ordonnance Sol.

Assurez-vous de la faisabilité de votre projet, et ce avant toute autre démarche.

Comment ? En obtenant un avis favorable de la sous-division sol de Bruxelles Environnement.

>  Pour cela, faites établir un rapport par un [expert en pollution du sol agréé](http://app.bruxellesenvironnement.be/listes/?nr_list=EPS0001). Celui-ci le transmettra à Bruxelles Environnement via la [plateforme BRUSOIL](http://brusoil.environnement.brussels/home.html), qui remettra ensuite son avis.
___

`;

export const licenseOpenSoilNlCheck3or4 = `
### Vraag om een voorafgaand bodemadvies

Het geothermisch systeem wordt geplaatst op een verontreinigd perceel volgens de Bodemordonnantie.

Verzeker u van de haalbaarheid van uw project, en dit vóór elke andere stap.

Hoe? Door een gunstig advies te bekomen van de onderafdeling Bodem van Leefmilieu Brussel.

>  Hiervoor moet u een verslag laten opstellen door een [erkend bodemverontreinigingsdeskundige](https://app.leefmilieubrussel.be/lijsten/?nr_list=EPS0001). Die moet het verslag bezorgen aan Leefmilieu Brussel via het [BRUSOIL-platform](http://brusoil.environnement.brussels/nl/home.html), die vervolgens zijn advies zal uitbrengen.
___

`;

export const licenseOpenFrCommonBody = `

### Étude des besoins thermiques
Estimer les besoins énergétiques du bâtiment (en chaud et en froid).

[Plus d’information](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/mon-projet-etape-par-etape/systeme-ouvert#etude_besoins).
___

### Étude de faisabilité hydrogéologique
La mise en place d’un système géothermique ouvert nécessite de réaliser une étude de faisabilité hydrogéologique en amont de la demande de permis d’environnement. Une analyse de la faisabilité en première approche (pré-faisabilité) peut être réalisée sur BrugeoTool.

>  Dans le cadre de cette étude, un ou plusieurs pompages d'essai sont nécessaires. Ceux-ci nécessitent d’effectuer une [demande de prise d’eau souterraine pour un pompage d'essai](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/eaux-souterraines/captages/formulaires-de-demande-de-prises).

[Plus d’information](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/mon-projet-etape-par-etape/systeme-ouvert#etude_faisabilite).

___

### Pré-dimensionnement
Procéder au pré-dimensionnement de l’installation sur la base des données
hydrogéologiques locales connues (étude de faisabilité hydrogéologique)
complétées par les données du sous-sol disponibles sur BrugeoTool.

[Plus d’information](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/mon-projet-etape-par-etape/systeme-ouvert#predimensionnement).
___

### Demandes de permis
>  Un système géothermique ouvert est une installation classée de classe 1B, soumise à permis d'environnement.

Vous trouverez toutes les informations relatives aux demandes de permis d'environnement
sur le [guide pratique du permis d'environnement](https://environnement.brussels/le-permis-denvironnement/le-guide-pratique-du-permis-denvironnement).

Vous trouverez également ici les [formulaires de demandes de permis d'environnement](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/formulaires-de-demande-de-systemes-geothermiques).

>  Un système géothermique ouvert est aussi soumis à une demande de [permis d'urbanisme](https://urbanisme.irisnet.be/lepermisdurbanisme).

___

### Dimensionnement
Forer un ou plusieurs puit(s) test(s) et dimensionner l’installation sur la base des propriétés locales du sous-sol.

[Plus d’information](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/mon-projet-etape-par-etape/systeme-ouvert#dimensionnement).
___

### Mise en oeuvre
Installer les doublets géothermiques, le réseau hydraulique et le dispositif de chauffage.

[Plus d’information](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geothermie/mon-projet-etape-par-etape/systeme-ouvert#mise_en_oeuvre).
`;

export const licenseOpenNlCommonBody = `

### Studie van thermische noden
De energienoden van het gebouw (qua verwarming en koeling) inschatten.

[Meer informatie](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/mijn-project-stap-voor-stap/open-systeem#behoeften).
___

### Hydrogeologische haalbaarheidsstudie
De plaatsing van een open geothermisch systeem vereist de uitvoering van een hydrogeologische haalbaarheidsstudie in een vroeg stadium van de milieuvergunningsaanvraag. Een eerste benadering van haalbaarheidsanalyse (pre-haalbaarheid) kan worden uitgevoerd via BrugeoTool.

>  In het kader van deze studie zijn een of meerdere pompproeven nodig. Deze vereisen de uitvoering van een [aanvraag voor een pompproefvergunning](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/grondwater/waterwinningen/formulieren-voor-de-aanvraag-van).

[Meer informatie](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/mijn-project-stap-voor-stap/open-systeem#haalbaarheidsstudie).

___

### Pre-dimensionering
Overgaan tot de predimensionering van de installatie op basis van de gekende lokale hydrogeologische gegevens (hydrogeologische haalbaarheidsstudie) aangevuld met de gegevens van de ondergrond die beschikbaar zijn via de BrugeoTool.

[Meer informatie](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/mijn-project-stap-voor-stap/open-systeem#predimensionering).
___

### Vergunningsaanvraag
>  Een open geothermisch systeem is een installatie geklasseerd onder klasse 1B, die onderworpen is aan een milieuvergunning.

U vindt alle informatie over milieuvergunningsaanvragen in de [praktische gids voor milieuvergunningen](https://leefmilieu.brussels/de-milieuvergunning/praktische-gids-voor-milieuvergunningen).

U vindt hier ook de [milieuvergunningsaanvraagformulieren](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/formulieren-voor-geothermische-systemen).

>  Een open geothermisch systeem is ook onderworpen aan een [stedenbouwkundige vergunningsaanvraag](https://stedenbouw.irisnet.be/vergunning?set_language=nl).

___

### Dimensionering
Een of meerdere testputten boren en de installatie dimensioneren op basis van de lokale eigenschappen van de ondergrond.

[Meer informatie](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/mijn-project-stap-voor-stap/open-systeem#dimensionering).
___

### Uitvoering
De geothermische doubletten, het hydraulisch netwerk en het verwarmingssysteem installeren.

[Meer informatie](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geothermie/mijn-project-stap-voor-stap/open-systeem#uitvoering).
`;

export const licenseOpenSoilFR = `
**Étapes et démarches nécessaires à l'installation d'un système géothermique ouvert au lieu sélectionné.**

___

### Demande d'avis préalable
Le système géothermique est implanté sur une parcelle polluée au sens de l’Ordonnance Sol. Assurez-vous de la faisabilité de votre projet, et ce avant toute autre démarche.
Comment ? En obtenant un avis favorable de la sous-division sol de Bruxelles Environnement.
Pour cela, faites établir un rapport par un [expert en pollution du sol agréé](url?). Celui-ci le transmettra à Bruxelles Environnement via la plateforme [BRUSOIL](url?), qui remettra ensuite son avis.

___

### Étude des besoins thermiques

___

### Étude de faisabilité technique
La mise en place d’un système géothermique ouvert nécessite de réaliser une étude de faisabilité en amont de la demande de permis.
Dans le cadre de cette étude, un ou plusieurs pompages d'essai sont nécessaires. Ceux-ci nécessitent d’effectuer une [demande de prise d’eau souterraine pour un pompage d'essai](url?).

___

### Pré-dimensionnement

___

### Demandes de permis
Un système géothermique ouvert est une installation classée de classe 1B, soumise à permis d'environnement.
Vous trouverez toutes les informations relatives aux demandes de permis d'environnement sur le [guide pratique du permis d'environnement](?url).
Vous trouverez également ici les [formulaires de demandes de permis d'environnement](url?).
Un système géothermique ouvert est aussi soumis à une demande de [permis d'urbanisme](url?).

___

### Dimensionnement

___

### Mise en oeuvre

`;

export const outOfRegionFR = `
Vous avez sélectionné un endroit hors de la région Bruxelloise pour lequel les données géologiques sont indisponibles. Nous vous renvoyons vers l’outil
`;

export const outOfRegionNL = `
U hebt een locatie buiten het Brusselse gewest geselecteerd waarvoor geen geologische gegevens beschikbaar zijn. We verwijzen u door naar
`;

export const outOfRegionFRTechnical = `
Vous avez sélectionné un endroit hors de la région Bruxelloise pour lequel les données géologiques sont indisponibles. Nous vous renvoyons vers l’outil de la région flamande
`;

export const outOfRegionNLTechnical = `
U hebt een locatie buiten het Brusselse gewest geselecteerd waarvoor geen geologische gegevens beschikbaar zijn. We verwijzen u naar de tool van het Vlaams Gewest
`;

export const outOfRegionLicenseFR =
    'Vous avez sélectionné un endroit hors de la région Bruxelloise. Les démarches à entreprendre sont indépendantes de Bruxelles-Environnement.';

export const outOfRegionLicenseNL =
    'U hebt een locatie buiten het Brusselse gewest geselecteerd. De te ondernemen stappen zijn onafhankelijk van Leefmilieu-Brussel.';

export const outOfRegionGeneralButInBufferFR = (x: number, y: number) => `
Le lieu sélectionné est en dehors de la Région de Bruxelles-Capitale. Pour plus d\'information sur la géothermie, nous vous invitons à consulter l’application [SmartGeoTherm](${getSmartGOThermBaseUrl(
    'extern'
)}?${encodeUrlQuery({
    z: 9,
    use: 1,
    lang: 'fr',
    x,
    y,
})}).

Nos données géologiques et hydrogéologiques sont cependant disponibles en ce point. Vous pouvez les consulter dans la section « 3. Données du sous-sol » et dans le « forage virtuel ».
`;

export const outOfRegionGeneralButInBufferNL = (x: number, y: number) => `
De geselecteerde plaats ligt buiten het Brussels Hoofdstedelijk Gewest. Voor meer informatie over geothermische energie nodigen wij u uit om de applicatie [SmartGeoTherm](${getSmartGOThermBaseUrl(
    'extern'
)}?${encodeUrlQuery({
    z: 9,
    use: 1,
    lang: 'nl',
    x,
    y,
})}) te raadplegen.

Onze geologische en hydrogeologische gegevens zijn op dit punt desondanks beschikbaar. U kunt ze raadplegen in de rubriek " 3. Gegevens
ondergrond" en in de "Virtuele boring".

`;

export const disclaimerFR = `
Les données et informations fournies par l’application BrugeoTool sont des estimations. Bruxelles Environnement ne pourra être tenu responsable de l’utilisation qui en sera faite. Nous vous invitons à consulter les [conditions d’utilisation](https://environnement.brussels/content/brugeotool-conditions-legales-dutilisation) pour plus d’informations.
`;

export const disclaimerNL = `
De gegevens en informatie die door de BrugeoTool applicatie worden verstrekt zijn schattingen. Leefmilieu Brussel kan niet verantwoordelijk worden gesteld voor het gebruik ervan. Wij nodigen u uit om de [gebruiksvoorwaarden te raadplegen](https://leefmilieu.brussels/content/brugeotool-wettelijke-gebruiksvoorwaarden) voor meer informatie.
`;

export const disclaimerPrintFR = `
Les données et informations fournies par l’application BrugeoTool sont des estimations. Bruxelles Environnement ne pourra être tenu responsable de l’utilisation qui en sera faite. Nous vous invitons à consulter les conditions d’utilisation de l’application BrugeoTool disponibles à l’URL suivante pour plus d’informations : https://environnement.brussels/content/brugeotool-conditions-legales-dutilisation .
`;

export const disclaimerPrintNL = `
De gegevens en informatie die door de BrugeoTool applicatie worden verstrekt zijn schattingen. Leefmilieu Brussel kan niet verantwoordelijk worden gesteld voor het gebruik ervan. Wij nodigen u uit om de gebruiksvoorwaarden van de BrugeoTool applicatie te raadplegen, beschikbaar op de volgende URL voor meer informatie:  https://leefmilieu.brussels/content/brugeotool-wettelijke-gebruiksvoorwaarden .
`;

export const creditsFR = `
Fond de plan & géocodeur © UrbIS. Données du sous-sol fournies par Bruxelles Environnement.
Conception et réalisation : [A.-C.](https://atelier-cartographique.be), [C.-L.](https://champs-libres.coop) & [E.-C.](https://energiecommune.be/)
`;

export const creditsNL = `
Achtergrond & geocoder © UrbIS. Ondergrondgegevens verstrekt door Leefmilieu Brussel.
Ontwerp en productie : [A.-C.](https://atelier-cartographique.be), [C.-L.](https://champs-libres.coop) & [E.-C.](https://energiecommune.be/)
`;

export const constraintHeaderNaturaTrueFr = `
** Le projet est situé en [zone d'incidence sur Natura 2000 ou réserve naturelle ou réserve forestière](https://environnement.brussels/thematiques/espaces-verts-et-biodiversite/action-de-la-region/natura-2000/travaux-et-amenagements).**

Une évaluation appropriée des incidences (EAI) vous sera demandée dans le cadre de la demande de permis d'environnement.",
`;

export const constraintHeaderNaturaTrueNl = `
** Het project bevindt zich in een [zone met gevolgen voor Natura 2000 of natuur- of bosreservaat](https://leefmilieu.brussels/themas/groene-ruimten-en-biodiversiteit/acties-van-het-gewest/natura-2000/werken-en-inrichtingen).
**

Een impactbeoordeling (PEB) zou u kunnen worden gevraagd in het kader van de aangifte of milieuvergunning.",
`;
