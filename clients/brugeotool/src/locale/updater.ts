import { GeoMessageKey, GeoMessageDB } from '.';
import { InfoOpen, InfoStrati, InfoClosed } from '../remote/info';
import { scopeOption } from 'sdi/lib';
import { fromNullable } from 'fp-ts/lib/Option';

const makeKey = (prefix: string, center: string, sufix: string) =>
    `${prefix}${center}${sufix}` as GeoMessageKey;

/**
 * Update records in place with values coming from the server
 *
 *
 * @param strati
 * @param closed
 * @param hgeol
 * @param opened
 * @param db
 */
export const updateRecords = (
    strati: InfoStrati[],
    opened: InfoOpen[],
    closed: InfoClosed[],
    db: GeoMessageDB
) => {
    const en = '';
    closed.forEach(s => {
        db[makeKey('US/RBC_', s.code_us, '')] = {
            nl: s.se_bhg,
            fr: s.us_rbc,
            en,
        };
        db[makeKey('US/RBC ', s.code_us, '')] = {
            nl: s.se_bhg,
            fr: s.us_rbc,
            en,
        };
        scopeOption()
            .let('nl', fromNullable(s.remark_nl))
            .let('fr', fromNullable(s.remark_fr))
            .map(({ fr, nl }) => {
                db[makeKey('US/RBC_', s.code_us, '-remark')] = { nl, fr, en };
            });
    });

    strati.forEach(s => {
        // db[makeKey('US/RBC_', s.code_us, '')] = { nl: s.se_bhg, fr: s.us_rbc }
        db[makeKey('US/RBC_', s.code_us, '-litho')] = {
            nl: s.descrnl,
            fr: s.descrfr,
            en,
        };
        db[makeKey('geoSerie', s.ref_serie.toString(), '')] = {
            nl: s.serie_nl,
            fr: s.serie_fr,
            en,
        };
        db[makeKey('geoEtage', s.ref_etage.toString(), '')] = {
            nl: s.etage_nl,
            fr: s.etage_fr,
            en,
        };
    });

    opened.forEach(o => {
        db[makeKey('UH/RBC_', o.code_uh, '')] = {
            nl: o.he_bhg,
            fr: o.uh_rbc,
            en,
        };
        db[makeKey('UH/RBC_', o.code_uh, '-hydro_state')] = {
            nl: o.h_staat,
            fr: o.h_etat,
            en,
        };
        db[makeKey('UH/RBC_', o.code_uh, '-potential')] = {
            nl: o.poten_nl,
            fr: o.poten_fr,
            en,
        };
    });
};

// const KEYS: GeoMessageKey[] = [

//     // 'US/RBC_1-remark',
//     // 'US/RBC_92-remark',
//     // 'IV',
//     // 'III',
//     // 'II',
//     // 'I',

//     // 'phreatic',
//     // 'confined',

//     // 'Quaternary',
//     // 'Neogene',
//     // 'Paleogene',
//     // 'Cretacea',
//     // 'Cambria',
// ];
