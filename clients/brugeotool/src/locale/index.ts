import { MessageStore, formatMessage } from 'sdi/locale';
import * as texts from './marked-texts';
import { updateRecords } from './updater';
import { InfoStrati, InfoOpen, InfoClosed } from '../remote/info';

const messages = {
    // '-litho': {fr: '', nl:''},
    // '-hydro_state': {fr: '', nl:''},
    // '-potential': {fr: '', nl:''},
    appName: {
        fr: 'Carte géothermique de la Région de Bruxelles-Capitale',
        nl: 'Geothermische kaart van het Brussels Hoofdstedelijk Gewest',
    },

    backToBeHome: {
        fr: "Retour vers l'accueil BrugeoTool",
        nl: 'Terug naar de BrugeoTool homepage',
    },

    backToBeHomeLink: {
        fr: 'https://environnement.brussels/brugeotool',
        nl: 'https://leefmilieu.brussels/brugeotool',
    },

    projectSituation: {
        fr: 'Situation de votre projet',
        nl: 'Situering van uw project',
    },

    dataSource: {
        fr: 'source des données',
        nl: 'databron',
    },

    dataSourceLink: {
        fr: 'https://environnement.brussels/content/brugeotool-sources',
        nl: 'https://leefmilieu.brussels/content/brugeotool-bronnen',
    },

    infoAndLegend: {
        fr: 'Informations et légende',
        nl: 'Informatie en legende',
    },

    dataManagement: {
        fr: 'Affichage et gestion des données',
        nl: 'Weergave en beheer van gegevens',
    },

    coordinates: {
        fr: 'Coordonnées',
        nl: 'Coördinaten',
    },
    coordinatesLambert: {
        fr: 'Coordonnées Lambert 72',
        nl: 'Coördinaten Lambert 72',
    },

    capakey: {
        fr: 'Numéro de parcelle cadastral',
        nl: 'Kadastraal perceelnummer',
    },

    parcelle: {
        fr: 'Parcelle',
        nl: 'Perceel',
    },

    address: {
        fr: 'Adresse',
        nl: 'Adres',
    },

    leftSlice: {
        fr: 'Coupe à gauche',
        nl: 'Linkse snede',
    },

    rightSlice: {
        fr: 'Coupe à droite',
        nl: 'Rechtse snede',
    },

    doubleSlice: {
        fr: 'Double coupe',
        nl: 'Dubbele snede',
    },

    noSlice: {
        fr: 'Vue complète',
        nl: 'Compleet beeld',
    },

    searchByAdress: {
        fr: 'Chercher par adresse',
        nl: 'Zoeken op adres',
    },

    searchByCapakey: {
        fr: 'Chercher par identifiant de parcelle',
        nl: 'Zoeken op perceelidentificatie',
    },

    searchByCoordinates: {
        fr: 'Chercher par coordonnées (Lambert72 - EPSG:31370)',
        nl: 'Zoeken op coördinaten (Lambert72 - EPSG:31370)',
    },

    'helptext:measure': {
        fr: 'Pour finaliser une mesure, re-cliquer sur le dernier point créé. ',
        nl: 'Om een meting af te ronden, klikt u opnieuw op het laatst aangemaakte punt.',
    },
    'helptext:searchByCoordinates': {
        fr: 'Les coordonnées doivent être en Lambert72 / EPSG:31370 au format [X Y], le séparateur étant un espace.',
        nl: 'Coördinaten moeten in Lambert72 / EPSG:31370 in het formaat [X Y] zijn, waarbij het scheidingsteken een spatie is.',
    },

    'helptext:printToPdf': {
        fr: `
**TIP 1** : Pour enregistrer le document en .PDF, vous pouvez utiliser la fonctionnalité "*Print to PDF*" de votre menu d'impression.
Selon la quantité d'information, un ajustement de l'échelle d'impression et/ou du format de papier (A4 ou A3) peut être utile.`,
        nl: '**TIP 1**: Om het document als PDF-bestand op te slaan, kunt u de functie "*Print to PDF*" in uw afdrukmenu gebruiken. Afhankelijk van de hoeveelheid informatie kan een aanpassing van de afdrukschaal en/of het papierformaat (A4 of A3) nuttig zijn.',
    },

    printTitle: {
        fr: 'Rapport BrugeoTool',
        nl: 'Verslag BrugeoTool',
    },

    date: {
        fr: 'date du rapport',
        nl: 'rapporteringsdatum',
    },

    url: {
        fr: 'url du projet',
        nl: 'projecturl',
    },

    launchSearch: {
        fr: 'Lancer la recherche',
        nl: 'Start de zoekopdracht',
    },

    backToAnalysis: {
        fr: "retour à l'analyse",
        nl: 'terug naar de analyse',
    },

    modalSmartTitle: {
        fr: "Lien vers l'outil de pré-dimensionnement SmartGeotherm",
        nl: 'Link naar de SmartGeotherm pre-dimensioneringstool',
    },

    'helptext:modalSmart': {
        fr: "Vous êtes sur le point d'être re-dirigé vers l'outil SmartGeotherm vous offrant un pré-dimensionnement sur base des données de votre projet.",
        nl: 'U staat op het punt om doorgestuurd te worden naar de SmartGeotherm tool, die u een presizing biedt op basis van uw projectgegevens.',
    },

    'helptext:modalSmartClose': {
        fr: 'Vous pouvez indiquer ci-dessous la profondeur pour laquelle vous souhaitez un pré-dimensionnement (laissez ce champ vide si vous ne souhaitez pas utiliser de valeur par défaut) : ',
        nl: 'U kan hieronder de diepte aangeven waarvoor u een predimensionering wenst (laat dit veld leeg indien u geen standaardwaarde wil gebruiken) : ', // nldone
    },

    'helptext:modalSmartOpen': {
        fr: "Vous pouvez choisir ci-dessous l'unité hydrogéologique pour laquelle vous souhaitez un pré-dimensionnement. Seules les unités hydrogéologiques présentant un potentiel intéressant* et pour lesquelles des données sont disponibles sont proposées. Si vous souhaitez lancer une analyse sur une autre unité hydrogéologique, vous pouvez laisser ce champ vide ou sélectionner « Autre Unité Hydrogéologique (encodage manuel des paramètres)»",
        nl: 'U kan hieronder de hydrogeologische eenheid kiezen waarvoor u een predimensionering wenst. Enkel de hydrogeologische eenheden met een interessant* potentieel en waarvoor de gegevens beschikbaar zijn worden voorgesteld. Indien u een analyse van een andere hydrogeologische eenheid wil lanceren, kan u dit veld leeg laten of “andere hydrogeologische eenheid” (manuele invoering van de parameters) selecteren.', // nldone
    },
    'helptext:modalSmartOpen2': {
        fr: '*Le Système aquifère des craies du Crétacé et du socle Paléozoïque (UH/RBC_9 regroupant les UH/RBC_9a et UH/RBC_9b) n’est pas proposé dans la liste ci-dessus par manque de données. Celui-ci peut cependant présenter un potentiel intéressant. Dès lors, dans l’hypothèse où votre choix se porterait sur cette unité, nous vous invitons à encoder les paramètres manuellement.',
        nl: '* Het Krijt en sokkel aquifersysteem (HE/BHG_9 met inbegrip van HE/BHG 9a en HE/BHG 9b) wordt niet voorgesteld in de bovenstaande lijst door een gebrek aan gegevens. Het kan evenwel een interessant potentieel voorstellen. In de veronderstelling dat uw keuze deze eenheid betreft, vragen wij u de parameters manueel in te voeren.', // nldone
    },

    faq: {
        fr: 'FAQ',
        nl: 'FAQ',
    },

    faqLink: {
        fr: 'https://environnement.brussels/content/brugeotool-faq',
        nl: 'https://leefmilieu.brussels/content/brugeotool-faq',
    },

    labelBE: {
        fr: 'environnement.bruxelles',
        nl: 'leefmilieu.brussel',
    },

    printAnalysis: {
        fr: "Imprimer l'analyse",
        nl: 'De analyse afdrukken',
    },

    projectSteps: {
        fr: 'Étapes et démarches',
        nl: 'Fasen en processen',
    },

    projectGeology: {
        fr: 'Données du sous-sol',
        nl: 'Gegevens van de ondergrond',
    },

    dataSwitch: {
        fr: 'Exploration : Gestionnaire de données',
        nl: 'Exploratie : Data Manager',
    },

    mapSelect: {
        fr: `Exploration : Sélection d'éléments sur la carte`,
        nl: 'Exploratie : Selectie van items op de kaart',
    },

    backgroundUrbis: {
        fr: `Urbis`,
        nl: 'Urbis',
    },

    backgroundOrtho: {
        fr: `orthophoto`,
        nl: 'orthophoto',
    },

    expertEstim: {
        fr: `Outils de pré-dimensionnement`,
        nl: 'Pre-dimensionering tool',
    },

    metaInfos: {
        fr: `Métadonnées`,
        nl: 'Metadata',
    },

    geoAnalysis: {
        fr: `Analyse géothermique`,
        nl: `Geothermische analyse`,
    },

    tooltipGeoAnalysis: {
        fr: `Fonctionalité : Analyse géothermique`,
        nl: `Tool : Geothermische analyse`,
    },

    data: {
        fr: `Données`,
        nl: 'Gegevens',
    },

    legend: {
        fr: `Légende`,
        nl: 'Legende',
    },

    lithoLegend: {
        fr: `Légende lithologique`,
        nl: 'Lithologische legende ',
    },

    lithoStratiLegend: {
        fr: `Légende lithostratigraphique`,
        nl: 'Lithostratigrafische legende ',
    },

    hydroLegend: {
        fr: `Légende hydrogéologique`,
        nl: 'Hydrogeologische legende ',
    },

    tooltipPinLabel: {
        fr: `Exploration : Placer un pointeur`,
        nl: 'Exploratie : Plaats een markering',
    },

    tooltipMeasure: {
        fr: `Exploration : Mesurer une longueur ou une superficie`,
        nl: 'Exploratie : Een lengte of oppervlakte meten',
    },

    measureLength: {
        fr: `Mesurer une longueur`,
        nl: 'Een lengte meten',
    },
    measureArea: {
        fr: `Mesurer une superficie`,
        nl: 'Een oppervlakte meten',
    },

    fullscreenOn: {
        fr: `Afficher en entier`,
        nl: 'Volledige weergave',
    },

    fullscreenOff: {
        fr: `Réduire l'affichage`,
        nl: 'Verkleinen van de weergave',
    },

    tooltipGeoSynthesis: {
        fr: `Fonctionalité : Forage virtuel`,
        nl: 'Tool : Virtuele boring',
    },

    // biggerTopMap: {
    //     fr: `Agrandir la carte du haut`,
    //     nl: 'Vergroten van de kaart bovenaan'
    // },

    // biggerBottomMap: {
    //     fr: `Agrandir la carte du bas`,
    //     nl: 'Vergroten van de kaart onderaan'
    // },

    placeholderGeocode: {
        fr: 'rue, numéro...',
        nl: 'straat, nummer...',
        // en: 'street, number...'
    },

    placeholderGeocodeCapakey: {
        fr: 'Identifiant de parcelle',
        nl: 'Perceel ID',
    },

    placeholderGeocodeCoordinates: {
        fr: 'X Y',
        nl: 'X Y',
    },
    placeholderGeocodeLatitude: {
        fr: 'Latitude',
        nl: 'Breedtegraad',
    },
    researchGeocode: {
        fr: 'Chercher',
        nl: 'Zoeken',
        // en: 'Search'
    },

    displayPerformance: {
        fr: 'Performance',
        nl: 'Vertoningsperfomance',
        // en: 'Search'
    },

    displayQuality: {
        fr: 'Qualité',
        nl: 'Vertoningsprecisie',
        // en: 'Search'
    },

    searchGeocodeResult: {
        fr: 'Résultat de recherche',
        nl: 'Zoekresultaat',
        // en: 'Search results'
    },
    backToMap: {
        fr: 'Carte générale',
        nl: 'Algemene kaart',
    },
    close: {
        fr: 'fermé',
        nl: 'gesloten',
        // en: 'closed'
    },
    open: {
        fr: 'ouvert',
        nl: 'open',
        // en: 'open'
    },
    closeGeothermal: {
        fr: 'système en géothermie fermé',
        nl: 'gesloten geothermisch systeem',
        // en: 'closed geothermal system'
    },
    openGeothermal: {
        fr: 'système en géothermie ouverte',
        nl: 'open geothermisch systeem',
        // en: 'open geothermal system'
    },
    closeSystem: {
        fr: 'Système fermé',
        nl: 'Gesloten systeem',
        // en: 'Closed system'
    },
    openSystem: {
        fr: 'Système ouvert',
        nl: 'Open systeem',
        // en: 'Open system'
    },
    bothSystem: {
        fr: 'Forage virtuel',
        nl: 'Virtuele boring',
    },

    elevation: {
        fr: 'Topographie',
        nl: 'Topografie',
    },

    paginatorGeneral: {
        fr: texts.pagGeneralFR,
        nl: texts.pagGeneralNL,
    },
    paginatorLicense: {
        fr: texts.pagLicenseFR,
        nl: texts.pagLicenseNL,
    },
    paginatorGeology: {
        fr: texts.pagGeologyFR,
        nl: texts.pagGeologyNL,
    },
    paginatorFinance: {
        fr: texts.pagFinanceFR,
        nl: texts.pagFinanceNL,
    },
    sidebarText: {
        fr: texts.paginatorSidebarFR,
        nl: texts.paginatorSidebarNL,
    },

    licenseClose: {
        fr: texts.licenseCloseFR,
        nl: texts.licenseCloseNL,
    },

    constraintHeaderWaterTrue: {
        fr: '**Le projet est situé en [zone de protection de captage](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/eaux-souterraines/monitoring-des-eaux-souterraines/zones).**',
        nl: '**Het project bevindt zich in een [beschermingsgebied voor waterwinning](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/grondwater/monitoring-van-het-grondwater/beschermde-gebieden).** ',
    },

    constraintHeaderMetroTrue: {
        fr: '**Le projet est situé sur le [tracé du Métro Nord](https://geodata.environnement.brussels/client/view/c6954a8a-71e7-4095-a8fc-325ee2467d84).**',
        nl: '**Het project bevindt zich op de [Metro Noord traject](https://geodata.leefmilieu.brussels/client/view/c6954a8a-71e7-4095-a8fc-325ee2467d84).** ',
    },

    constraintHeaderWaterTrueResultClose: {
        fr: '**La géothermie système fermé est interdite.**',
        nl: '**Het gesloten geothermisch systeem is verboden.** ',
    },
    constraintHeaderWaterTrueResultOpen: {
        fr: '**La géothermie système ouvert est interdite.**',
        nl: '**Het open geothermisch systeem is verboden.**',
    },

    constraintHeaderWaterFalse: {
        fr: "**Le projet n'est pas situé en [zone de protection de captage](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/eaux-souterraines/monitoring-des-eaux-souterraines/zones).**",
        nl: '**Het project bevindt zich niet in een [beschermingsgebied voor waterwinning](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/grondwater/monitoring-van-het-grondwater/beschermde-gebieden).**',
    },

    constraintHeaderSoilCase0: {
        fr: 'Inventaire sol : parcelle potentiellement polluée ',
        nl: 'Bodeminventaris: mogelijk verontreinigd perceel  ',
    },
    constraintHeaderSoilCase1: {
        fr: 'Inventaire sol : parcelle non polluée ',
        nl: ' Bodeminventaris : niet-verontreinigd perceel',
    },
    constraintHeaderSoilCase2: {
        fr: 'Inventaire sol : parcelle légèrement polluée sans risques ',
        nl: 'Bodeminventaris: licht verontreinigd perceel zonder risico ',
    },
    constraintHeaderSoilCase3: {
        fr: 'Inventaire sol : parcelle polluée sans risques ',
        nl: 'Bodeminventaris: verontreinigd perceel zonder risico ',
    },
    constraintHeaderSoilCase4: {
        fr: "Inventaire sol : parcelle polluée en cours d'étude ou de traitement ",
        nl: 'Bodeminventaris: verontreinigd perceel in onderzoek of behandeling ',
    },
    constraintHeaderSoilCaseMixed: {
        fr: 'Inventaire sol : parcelle en ',
        nl: 'Bodeminventaris: perceel in ',
    },
    constraintHeaderSoilCaseNone: {
        fr: "Inventaire sol : cette parcelle n'est pas recensée à l'inventaire sol. ",
        nl: 'Bodeminventaris: dit perceel is niet opgenomen in de bodeminventaris.',
    },
    constraintHeaderSoilcondEnviClose: {
        fr: "Des conditions spécifiques à la gestion des sols pollués pourraient être imposées dans le cadre de la déclaration ou du permis d'environnement.",
        nl: ' Specifieke voorwaarden voor het beheer van verontreinigde bodems zouden kunnen worden opgelegd in het kader van de aangifte of milieuvergunning. ',
    },
    constraintHeaderSoilcondEnviOpen: {
        fr: 'Vous devez obtenir un avis favorable de la sous-division sol de Bruxelles Environnement avant toute autre démarche.',
        nl: 'U moet een gunstig advies bekomen van de onderafdeling Bodem van Leefmilieu Brussel vóór elke andere stap. ',
    },
    constraintHeaderSoilcondUrbaClose: {
        fr: "Si les travaux ont lieu sur plus de 20m² de contact avec le sol, une reconnaissance de l'état du sol est requise avant délivrance du permis d'urbanisme.",
        nl: 'Indien werken plaatsvinden met meer dan 20m² in contact met de bodem, is een verkennend bodemonderzoek vereist vóór de aflevering van de stedenbouwkundige vergunning. ',
    },
    constraintHeaderSoilcondUrbaOpen: {
        fr: "Si les travaux ont lieu sur plus de 20m² de contact avec le sol, une reconnaissance de l'état du sol est requise avant toute autre démarche.",
        nl: 'Indien werken plaatsvinden met meer dan 20m² in contact met de bodem, is een verkennend bodemonderzoek vereist vóór elke andere stap.',
    },

    constraintHeaderNaturaTrue: {
        fr: texts.constraintHeaderNaturaTrueFr,
        nl: texts.constraintHeaderNaturaTrueNl,
    },

    constraintHeaderNaturaFalse: {
        fr: "**Le projet n'est pas situé en [zone d'incidence sur Natura 2000 ou réserve naturelle ou réserve forestière](https://environnement.brussels/thematiques/espaces-verts-et-biodiversite/action-de-la-region/natura-2000/travaux-et-amenagements).**",
        nl: '**Het project bevindt zich niet in een [zone met gevolgen voor Natura 2000 of natuur- of bosreservaat](https://leefmilieu.brussels/themas/groene-ruimten-en-biodiversiteit/acties-van-het-gewest/natura-2000/werken-en-inrichtingen). ** ',
    },

    constraintHeaderMetroNordTrue: {
        fr: '**Le projet est situé sur le futur [tracé du Métro Nord](https://geodata.environnement.brussels/client/view/c6954a8a-71e7-4095-a8fc-325ee2467d84)**',
        nl: 'TODO **Het project bevindt in [Metro Noord traject](https://geodata.leefmilieu.brussels/client/view/c6954a8a-71e7-4095-a8fc-325ee2467d84)**',
    },

    constraintHeaderMetroNordFalse: {
        fr: "**Le projet n'est pas situé sur le futur [tracé du Métro Nord](https://geodata.environnement.brussels/client/view/c6954a8a-71e7-4095-a8fc-325ee2467d84)**",
        nl: '**Het project bevindt zich niet in [Metro Noord traject](https://geodata.leefmilieu.brussels/client/view/c6954a8a-71e7-4095-a8fc-325ee2467d84)**',
    },

    constraintHeaderUrban: {
        fr: 'Assurez-vous également qu’il n’y ait pas de [contraintes urbanistiques](https://urban.brussels/fr) qui pourraient empêcher la réalisation de votre projet',
        nl: 'Verzeker u er ook van dat er geen [stedenbouwkundige beperkingen](https://urban.brussels/nl) zijn die de uitvoering van uw project zouden kunnen verhinderen.',
    },

    polutionCategory: {
        fr: 'catégorie',
        nl: 'categorie',
        // en: 'category'
    },

    display: {
        fr: 'affichage ',
        nl: 'weergave',
        // en: 'display'
    },

    layerManager: {
        fr: 'Gestionnaire de données ',
        nl: 'Data Manager',
        // en: 'Data manager'
    },

    mapTop: {
        fr: 'Carte affichée en haut ',
        nl: 'Kaart bovenaan',
        // en: 'Top map'
    },
    mapBottom: {
        fr: 'Carte affichée en bas ',
        nl: 'Kaart onderaan',
        // en: 'Bottom map'
    },

    generalInfoOpenNotOk: {
        fr: "Il est interdit d'installer un système géothermique ouvert dans la zone sélectionnée.",
        nl: 'Het is verboden een open geothermisch systeem in de geselecteerde zone te installeren.',
    },

    generalInfoClosedNotOk: {
        fr: "Il est interdit d'installer un système géothermique fermé dans la zone sélectionnée.",
        nl: 'Het is verboden een gesloten geothermisch systeem in de geselecteerde zone te installeren.',
    },

    appTextLicense: {
        fr: 'Informations géologiques',
        nl: 'Geologische informatie',
        // en: 'Geological information'
    },

    incompleteAdress: {
        fr: 'Adresse incomplète',
        nl: 'Onvolledig adres',
        // en: 'Incomplete address'
    },

    tradVersion: {
        fr: 'version',
        nl: 'versie',
        // en: 'version'
    },

    closeTableColRemark: {
        fr: 'Remarque',
        nl: 'Opmerking',
    },
    closeTableColUS: {
        fr: '[Unité stratigraphique](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/geologie)',
        nl: '[Stratigrafische eenheid](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/geologie)',
    },
    closeGraphDepth: {
        fr: 'Profondeur: ',
        nl: 'Diepte:',
    },
    closeTableColDepth: {
        fr: '[Profondeur  \n (m)](https://environnement.brussels/content/brugeotool-sources#donnees_stratigraphiques)',
        nl: '[Diepte   \n (m)](https://leefmilieu.brussels/content/brugeotool-bronnen#stratigrafische)',
    },
    closeTableColDepthAbsolute: {
        fr: '[Altitude   \n (m-DNG)](https://environnement.brussels/content/brugeotool-sources#donnees_stratigraphiques)',
        nl: '[Hoogte  \n (m-TAW)](https://leefmilieu.brussels/content/brugeotool-bronnen#stratigrafische)',
    },
    closeTableColDepthBottom: {
        fr: "Profondeur de la base de l'US (m)",
        nl: 'Diepte van de basis van de SE (m)',
    },
    closeTableColCond: {
        fr: '[λ (W/(m.K))](https://environnement.brussels/content/brugeotool-sources#conductivites_thermiques)',
        nl: '[λ (W/(m.K))](https://leefmilieu.brussels/content/brugeotool-bronnen#thermische)',
    },
    condGraphTitle: {
        fr: '[λ apparent (W/(m.K))](https://environnement.brussels/content/brugeotool-sources#conductivites_thermiques) : ',
        nl: '[Schijnbare λ (W/(m.K))](https://leefmilieu.brussels/content/brugeotool-bronnen#thermische) :',
    },
    condGraphTitlePrint: {
        fr: 'λ apparent (W/(m.K))',
        nl: 'Schijnbare λ (W/(m.K))',
    },
    closeTableColCondDry: {
        fr: 'λ sèche (W/(m.K))',
        nl: 'λ droogte (W/(m.K))',
    },
    closeTableColCondSat: {
        fr: 'λ saturée (W/(m.K))',
        nl: 'λ verzadiging (W/(m.K))',
    },
    openTableColState: {
        fr: 'État',
        nl: 'Staat',
    },
    openTableColUH: {
        fr: '[Unité hydrogéologique](https://environnement.brussels/thematiques/geologie-et-hydrogeologie/eaux-souterraines/hydrogeologie/unites-hydrogeologiques-de-la)',
        nl: '[Hydrogeologische eenheid](https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/grondwater/hydrogeologie/hydrogeologische-eenheden-van-het-brussels)',
    },
    openTableColKmin: {
        fr: '[Kmin (m/s)](https://environnement.brussels/content/brugeotool-sources#conductivites_hydrauliques)',
        nl: '[Kmin (m/s)](https://leefmilieu.brussels/content/brugeotool-bronnen#hydraulische)',
    },
    openTableColKmean: {
        fr: '[Kmoy (m/s)](https://environnement.brussels/content/brugeotool-sources#conductivites_hydrauliques)',
        nl: '[Kgem (m/s)](https://leefmilieu.brussels/content/brugeotool-bronnen#hydraulische)',
    },
    openTableColKmax: {
        fr: '[Kmax (m/s)](https://environnement.brussels/content/brugeotool-sources#conductivites_hydrauliques)',
        nl: '[Kmax (m/s)](https://leefmilieu.brussels/content/brugeotool-bronnen#hydraulische)',
    },
    openTableColPotential: {
        fr: '[Potentiel](https://environnement.brussels/content/brugeotool-sources#potentiel_systeme_ouvert)',
        nl: '[Potentieel](https://leefmilieu.brussels/content/brugeotool-bronnen#open_systeempotentieel)',
    },
    openRelativePiezo: {
        fr: '[Piézométrie (mai 2013) (m)](https://environnement.brussels/content/brugeotool-sources#donnees_piezometriques)',
        nl: '[Piëzometrie (mei 2013) (m)](https://leefmilieu.brussels/content/brugeotool-bronnen#piezometrische)',
    },
    openRelativePiezoAbsolute: {
        fr: '[Piézométrie (mai 2013) (m-DNG)](https://environnement.brussels/content/brugeotool-sources#donnees_piezometriques)',
        nl: '[Piëzometrie (mei 2013) (m-TAW)](https://leefmilieu.brussels/content/brugeotool-bronnen#piezometrische)',
    },
    openTableCol3: {
        fr: 'Formation (membre)',
        nl: 'formatie (lid)',
    },
    eraCol: {
        fr: 'Ère',
        nl: 'Era',
    },
    systemCol: {
        fr: 'Système',
        nl: 'Systeem',
    },
    serieCol: {
        fr: 'Série',
        nl: 'Serie',
    },
    etageCol: {
        fr: 'Étage',
        nl: 'Etage',
    },
    lithoCol: {
        fr: 'Lithologie détaillée',
        nl: 'Lithologische beschrijving',
    },

    tooltipCakeMode: {
        fr: 'Fonctionalité : Modèle géologique 3D',
        nl: 'Tool : 3D-geologisch model',
    },
    cakeName: {
        fr: 'Modèle géologique 3D',
        nl: '3D-geologisch model',
    },

    tooltipSliceName: {
        fr: 'Fonctionalité : Coupe géologique',
        nl: 'Tool : Geologische sectie', // nl to check
    },
    sliceName: {
        fr: 'Coupe géologique',
        nl: 'Geologische sectie', // nl to check
    },

    sliceMessageNotVisible: {
        fr: 'Diminuez le zoom pour afficher la coupe géologique 2D',
        nl: 'Verlaag de zoom om de 2D geologische doorsnede weer te geven', // nl to check
    },

    'US/RBC_11': { fr: 'US/RBC_11 Remblais', nl: 'SE/BHG_11 Ophogingen' },
    'US/RBC_12': {
        fr: 'US/RBC_12 Couverture limoneuse',
        nl: 'SE/BHG_12 Leemachtige deklagen',
    },
    'US/RBC_13': {
        fr: 'US/RBC_13 Argiles alluviales',
        nl: 'SE/BHG_13 Alluviale klei',
    },
    'US/RBC_14': {
        fr: 'US/RBC_14 Limons, sables et graviers alluviaux',
        nl: 'SE/BHG_14 Alluviale leem, zand en grind',
    },
    'US/RBC_21': {
        fr: 'US/RBC_21 Sables de Diest',
        nl: 'SE/BHG_21 Zand van Diest',
    },
    'US/RBC_22': {
        fr: 'US/RBC_22 Sables de Bolderberg',
        nl: 'SE/BHG_22 Zand van Bolderberg',
    },
    'US/RBC_23': {
        fr: 'US/RBC_23 Sables et argiles de Sint-Huibrechts-Hern',
        nl: 'SE/BHG_23 Zand en klei van Sint-Huibrechts-Hern',
    },
    'US/RBC_25': {
        fr: 'US/RBC_25 Sables de Maldegem (membre de Onderdale)',
        nl: 'SE/BHG_25 Zand van Maldegem (Lid van Onderdale)',
    },
    'US/RBC_31': {
        fr: 'US/RBC_31 Argiles de Maldegem (membre de Ursel et Asse)',
        nl: 'SE/BHG_31 Klei van Maldegem (Lid van Ursel en Asse)',
    },
    'US/RBC_41': {
        fr: 'US/RBC_41 Sables de Maldegem (membre de Wemmel)',
        nl: 'SE/BHG_41 Zand van Maldegem (Lid van Wemmel)',
    },
    'US/RBC_42': {
        fr: 'US/RBC_42 Sables de Lede',
        nl: 'SE/BHG_42 Zand van Lede',
    },
    'US/RBC_43': {
        fr: 'US/RBC_43 Sables de Bruxelles',
        nl: 'SE/BHG_43 Zand van Brussel',
    },
    'US/RBC_44': {
        fr: 'US/RBC_44 Sables de Gent (membre de Vlierzele)',
        nl: 'SE/BHG_44 Zand van Gent (Lid van Vlierzele)',
    },
    'US/RBC_51': {
        fr: 'US/RBC_51 Argiles de Gent (membre de Merelbeke)',
        nl: 'SE/BHG_51 Klei van Gent (Lid van Merelbeke)',
    },
    'US/RBC_61': {
        fr: 'US/RBC_61 Sables et argiles de Tielt',
        nl: 'SE/BHG_61 Zand en klei van Tielt',
    },
    'US/RBC_71': {
        fr: "US/RBC_71 Argiles de Kortrijk (membre d'Aalbeke)",
        nl: 'SE/BHG_71 Klei van Kortrijk (Lid van Aalbeke)',
    },
    'US/RBC_72': {
        fr: 'US/RBC_72 Sables et argiles de Kortrijk (membre de Moen)',
        nl: 'SE/BHG_72 Zand en Klei van Kortrijk (Lid van Moen)',
    },
    'US/RBC_73': {
        fr: 'US/RBC_73 Argiles de Kortrijk (membre de Saint Maur)',
        nl: 'SE/BHG_73 Klei van Kortrijk (Lid van Saint Maur)',
    },
    'US/RBC_81': {
        fr: 'US/RBC_81 Sables de Hannut (Membre de Grandglise)',
        nl: 'SE/BHG_81 Zand van Hannut (Lid van Grandglise)',
    },
    'US/RBC_82': {
        fr: 'US/RBC_82 Argiles de Hannut (Membre de Lincent)',
        nl: 'SE/BHG_82 Klei van Hannut (Lid van Lincent)',
    },
    'US/RBC_91': {
        fr: 'US/RBC_91 Craies de Gulpen',
        nl: 'SE/BHG_91 Krijt van Gulpen',
    },
    'US/RBC_92': {
        fr: 'US/RBC_92 Socle Paleozoique',
        nl: 'SE/BHG_92 Paleozoische sokkel',
    },
    // 'UH/RBC_01a': {
    //     fr: 'UH/RBC_01a Système aquitard quaternaire superficiel',
    //     nl: 'HE/BHG_01a Ondiep quaternair aquitardsysteem'
    // },
    // 'UH/RBC_01b': {
    //     fr: 'UH/RBC_01b Aquifère des limons, sables et graviers alluviaux',
    //     nl: 'HE/BHG_01b Alluviale leem, zand en grind aquifer'
    // },
    // 'UH/RBC_02': {
    //     fr: 'UH/RBC_02 Systeme aquifere sableux perché',
    //     nl: 'HE/BHG_02 Hoog zanderig aquifersysteem'
    // },
    // 'UH/RBC_03': {
    //     fr: 'UH/RBC_03 Aquiclude des argiles de Ursel et Asse',
    //     nl: 'HE/BHG_03 Aquiclude van klei van Ursel en Asse'
    // },
    // 'UH/RBC_04': {
    //     fr:
    //         'UH/RBC_04 Systeme aquifere des sables de Wemmel, Lede, Bruxelles et Vlierzele',
    //     nl:
    //         'HE/BHG_04 Aquifersysteem van Zand van Wemmel, Lede, Brussel en Vlierzele'
    // },
    // 'UH/RBC_05': {
    //     fr: 'UH/RBC_05 Aquiclude des argiles de Merelbeke',
    //     nl: 'HE/BHG_05 Aquiclude van klei van Merelbeke'
    // },
    // 'UH/RBC_06': {
    //     fr: 'UH/RBC_06 Aquitard des sables et argiles de Tielt',
    //     nl: 'HE/BHG_06 Aquitard van zand en klei van Tielt'
    // },
    // 'UH/RBC_07a': {
    //     fr: 'UH/RBC_07a Aquiclude des argiles de Aalbeke',
    //     nl: 'HE/BHG_07a Aquiclude van klei van Aalbeke'
    // },
    // 'UH/RBC_07b': {
    //     fr: 'UH/RBC_07b Aquitard des sables et argiles de Moen',
    //     nl: 'HE/BHG_07b Aquitard van Zand en klei van Moen'
    // },
    // 'UH/RBC_07c': {
    //     fr: 'UH/RBC_07c Aquiclude des argiles de Saint-Maur',
    //     nl: 'HE/BHG_07c Aquiclude van klei van Saint-Maur'
    // },
    // 'UH/RBC_08a': {
    //     fr: 'UH/RBC_08a Aquifère des sables du Landénien',
    //     nl: 'HE/BHG_08a Aquifer van Landeniaans zand'
    // },
    // 'UH/RBC_08b': {
    //     fr: 'UH/RBC_08b Aquiclude des argiles du Landenien',
    //     nl: 'HE/BHG_08b Aquiclude van Landeniaanse klei'
    // },
    // 'UH/RBC_09a': {
    //     fr: 'UH/RBC_09a Aquifère des craies du Crétacé',
    //     nl: 'HE/BHG_09a Aquifer van krijt van het Krijt'
    // },
    // 'UH/RBC_09b': {
    //     fr: 'UH/RBC_09b Système aquifère du socle Paléozoïque',
    //     nl: 'HE/BHG_09b Aquifersysteem van de Paleozoische sokkel'
    // },
    'US/RBC_11-14': {
        fr: 'US/RBC_11-14 Formations quaternaires indifférenciées ',
        nl: 'SE/BHG_11-14 Ongedifferentieerde quartaire formaties',
    },

    'US/RBC 11-14': {
        fr: 'US/RBC_11-14 Formations quaternaires indifférenciées ',
        nl: 'SE/BHG_11-14 Ongedifferentieerde quartaire formaties',
    },

    'US/RBC_1': {
        fr: 'Système aquifère du quaternaire',
        nl: 'Quartair aquifersysteem',
    },

    'US/RBC_1-litho': {
        fr: 'Quaternaire.',
        nl: 'Quartaire',
    },

    'US/RBC_11-litho': {
        fr: 'Remblais.',
        nl: 'Ophogingen',
    },

    'US/RBC_12-litho': {
        fr: "Dépôt de loess, limons d'origine éolienne, fin, sableux, homogène sur les plateaux et stratifiés sur les versants, chargé de matières argileuses, calcareux / Diluvium caillouteux des plateaux",
        nl: 'Afzetting van löss, leem van oorsprong eolisch, zanderig, fijn, homogeen op de plateaus en gelaagd op de hellingen, aanwezigheid van kleiachtige, kalkrijke stoffen / Keiachtige diluvium van de plateaus',
    },

    'US/RBC_13-litho': {
        fr: "Dépôt succesif de tourbe et d'argiles. Origine fluviale. Limité aux vallées alluviales.",
        nl: 'Afzetting van opeenvolgend veen en klei. Van oorsprong fluviaal. Beperkt tot alluviale valleien.',
    },
    'US/RBC_14-litho': {
        fr: "Dépôt de sables grossiers, graviers et limons. Présences de couches fines d'argiles, de silex. Mouvements du sol dûs aux gels et dégels successifs. Limité aux vallées alluviales.",
        nl: 'Afzetting van grof zand, grind en leem. Aanwezigheid van fijne lagen klei en silex. Grondbewegingen door opeenvolgende cycli van dooi en bevriezing. Beperkt tot alluviale valleien.',
    },
    'US/RBC_21-litho': {
        fr: "Sable vert brun à rouille, mi-fin à grossier, très glauconieux, avec concrétions limoniteuses. Parfois, présence de stratifications entrecroisées abondantes et des terriers tubulaires d'animaux fouisseurs.",
        nl: 'Groenbruin tot roestkleurig, matig fijn tot grof zand met veel glauconiet en leemhoudende concreties. Soms aanwezigheid van overvloedige gekruiste gelaagdheden en buisvormige gangen van gravende dieren.',
    },
    'US/RBC_22-litho': {
        fr: 'Sable fin, jaune pâle, micacé("sable chamois"), A la base, présence d\'un gravier de petits silex',
        nl: 'Fijn, bleekgeel, micahoudend zand ("sable chamois"), aan de basis aanwezigheid van grind van kleine silex.',
    },
    'US/RBC_23-litho': {
        fr: "Sable fin, jaune, micacé, passant graduellement de haut en bas à une argile ou un silt jaune blanc à gris rose (\"l'argile saumon\"), légèrement sableux et glauconieux, suivi d'une argile grise à grise verte, sableuse et plus glauconieuse. A la base, présence d'un gravier de quartz, de silex tantôt plats noirs, tantôt de plus grandes tailles et ovales. Parfois, présence de galets de grès vert cambrien.",
        nl: 'Fijn, geel, micahoudend zand dat geleidelijk overgaat van boven naar onder naar een witgele tot grijs-roze klei of silt ("l\'argile saumon"), die licht zand- en glauconiethoudend is, gevolgd door een grijs tot grijsgroene zandhoudende klei met meer glauconiet. Aan de basis aanwezigheid van grind van kwarts, silex van soms platte zwarte en soms grote ovale taille. Soms aanwezigheid van groene Cambrium zandsteen kiezels.',
    },
    'US/RBC_25-litho': {
        fr: 'Sable fin moyen, gris foncé, silteux, glauconnieux et micacé.',
        nl: 'Matig fijn, donkergrijs, siltig zand, glauconiet- en micahoudend.',
    },
    'US/RBC_31-litho': {
        fr: 'Argile gris bleuâtre, homogène, passant graduellement à une argile glauconieuse. A la base, principalement sable grossier très glauconieux ("Bande noire").',
        nl: 'Homogene grijsblauwe klei die geleidelijk overgaat in glauconiethoudende klei. Aan de basis, vooral grof glauconietzand ("Bande noire").',
    },
    'US/RBC_41-litho': {
        fr: "Sable fin, gris, très glauconieux. La teneur en argile augmente vers le sommet. Ce membre est caractérisé par la présence de Nummulites wemmelensis et d'une couche de base bien développée, riche en nummulites remaniés et roulés, ainsi qu'en fragments de grès calcaire fossilifère. ",
        nl: 'Grijs glauconiethoudend fijn zand met een toename van het kleigehalte naar de top. Gekenmerkt door het voorkomen van Nummulites wemmelensis en een goed ontwikkelde basislaag rijk aan herwerkte en gerolde nummulieten evenals fossielhoudende kalkzandsteenbrokken.',
    },
    'US/RBC_42-litho': {
        fr: "Sable gris, fin, carbonaté et légèrement glauconieux. Le sable est caractérisé par quelques bancs de calcaire sableux et par la présence de Nummulites variolarius, quand le sable n'est pas altéré. Vers la base, présence de niveaux plus grossiers et tout à la base, d'un gravier bien marqué avec des éléments remaniés",
        nl: 'Grijs, fijn zand, kalk- en licht glauconiethoudend. Het zand wordt gekenmerkt door enkele banken zandige kalksteen en door de aanwezigheid van Nummulites variolarius, wanneer het niet verweerd is. Naar de basis toe, aanwezigheid van grovere niveaus en helemaal aan de basis een duidelijk gemarkeerde grindlaag met verwerkte elementen.',
    },
    'US/RBC_43-litho': {
        fr: 'Sables quartzeux, fins à grossiers, hétérogènes et très faiblement glauconieux, caractérisés par la présence de bancs durs (bancs de calcaire sableux "faciès de Gobertange - bancs de grès carbonaté, parfois silicifié) et/ou par des nodules de grès de formes très capricieuses. Quand le sédiment est calcaire, la présence de Nummulites laevigatus est typique. Structure érosive en chenaux à la base.',
        nl: 'Kwartshoudend, fijn tot grof, heterogeen en zeer licht glauconiethoudend zand, gekenmerkt door aanwezigheid van harde banken (zandige kalksteenbanken "Gobertange facies" - carbonaatzandstenen banken, soms verkiezeld) en/of zandsteenknollen met zeer grillige vormen. Indien het sediment kalkhoudend is, is de aanwezigheid van Nummulites laevigatus typerend. Geulvormige erosieve structuur aan de basis.',
    },
    'US/RBC_44-litho': {
        fr: 'Sable gris vert, très fin, glauconieux, plus argileux vers la base. ',
        nl: 'Grijsgroen, zeer fijn, glauconiethoudend zand, meer kleihoudend naar de basis toe.',
    },
    'US/RBC_51-litho': {
        fr: 'Argile gris vert avec des zones sableuses vertes. Parfois, présence de pyrite. ',
        nl: 'Grijsgroene klei met groene zandige zones. Soms aanwezigheid van pyriet.',
    },
    'US/RBC_61-litho': {
        fr: "Dépôt hétérogène de sable fin, glauconieux et micacé, alternant avec des couches d'argile. Localement, présence de fragments de grès. ",
        nl: 'Heterogene afzetting van fijn, glauconiet- en micahoudend zand, afgewisseld met kleilagen. Plaatselijk aanwezigheid van zandsteenfragmenten.',
    },
    'US/RBC_71-litho': {
        fr: 'Argile grise à gris brun, très finement silteuse. ',
        nl: 'Grijs tot grijsbruine zeer fijnsiltige klei.',
    },
    'US/RBC_72-litho': {
        fr: "Dépôt hétérogène de sable silteux à argileux, faiblement glauconieux, avec quelques couches d'argile. Présence de nummulites. ",
        nl: 'Heterogene afzetting van siltig tot kleihoudend en licht glauconiethoudend zand met enkele lagen klei. Aanwezigheid van nummuliet.',
    },
    'US/RBC_73-litho': {
        fr: "Argile très finement silteuse avec quelques minces intercalations d'argile grossièrement silteuse ou de silt très fin argileux. ",
        nl: 'Zeer fijnsiltige klei met enkele fijne intercalaties van grofsiltige of zeer fijnsiltige klei.',
    },
    'US/RBC_81-litho': {
        fr: 'Sable fin, glauconieux, avec intercalations argileuses minces',
        nl: 'Fijn glauconiethoudend zand met fijne kleiige intercalaties.',
    },
    'US/RBC_82-litho': {
        fr: "Argile gris vert, légèrement sableuse, localement cimentée par de l'opale. A la base, quelques galets de silex vert foncé",
        nl: 'Grijsgroene, licht zandhoudende klei plaatselijk gecementeerd met opaal. Aan de basis enkele donkergroene silexkeien.',
    },
    'US/RBC_91-litho': {
        fr: 'Craies blanches à grise avec silexs noirs, appartenant probablement à la formation de Gulpen.',
        nl: 'Wit tot grijs krijt met zwarte silex die vermoedelijk tot de formatie van Gulpen behoren.',
    },
    'US/RBC_92-litho': {
        fr: "Dominance de bancs de grès feldspathique, schiste et quartzites d'âge  supposé cambrien inférieur (ou éventuellement précambrien : l'absence de fossile rend leur datation difficile). Ces roches peuvent s'altérer sous la forme d'argile compacte.",
        nl: 'Overwegend veldspaathoudende zandsteenbanken, leisteen en kwartsiet verondersteld van jongere Cambrium tijdperk (of eventueel précambrien: de afwezigheid van fossielen maakt de datering moeilijk). Deze gesteenten kunnen veranderen in de vorm van compacte klei.',
    },

    'UH/RBC_1a': {
        fr: 'Système aquitard quaternaire superficiel',
        nl: 'Ondiep quartair aquitardsysteem',
    },
    'UH/RBC_1b': {
        fr: 'Aquifère des limons, sables et graviers alluviaux',
        nl: 'Aquifer van alluviale leem, zand en grind',
    },
    'UH/RBC_2': {
        fr: 'Système aquifère sableux perché',
        nl: 'Hoog zanderig aquifersysteem',
    },
    'UH/RBC_3': {
        fr: 'Aquiclude des argiles de Ursel et Asse',
        nl: 'Aquiclude van klei van Ursel en Asse',
    },
    'UH/RBC_4': {
        fr: 'Système aquifère des sables de Wemmel, Lede, Bruxelles et Vlierzele',
        nl: 'Aquifersysteem van zand van Wemmel, Lede, Brussel en Vlierzele',
    },
    'UH/RBC_5': {
        fr: 'Aquiclude des argiles de Gent',
        nl: 'Aquiclude van klei van Gent',
    },
    'UH/RBC_6': {
        fr: 'Aquitard des sables et argiles de Tielt',
        nl: 'Aquitard van zand en klei van Tielt',
    },
    'UH/RBC_7a': {
        fr: 'Aquiclude des argiles de Aalbeke',
        nl: 'Aquiclude van klei van Aalbeke',
    },
    'UH/RBC_7b': {
        fr: 'Aquitard des sables et argiles de Moen',
        nl: 'Aquitard van zand en klei van Moen',
    },
    'UH/RBC_7c': {
        fr: 'Aquiclude des argiles de Saint-Maur',
        nl: 'Aquiclude van klei van Saint-Maur',
    },
    'UH/RBC_8a': {
        fr: 'Aquifère des sables du Landénien',
        nl: 'Aquifer van Landeniaans zand',
    },
    'UH/RBC_8b': {
        fr: 'Aquiclude des argiles du Landénien',
        nl: 'Aquiclude van Landeniaanse klei',
    },
    'UH/RBC_9a': {
        fr: 'Aquifère des craies du Crétacé',
        nl: 'Aquifer van krijt van het Krijt',
    },
    'UH/RBC_9b': {
        fr: 'Système aquifère du socle Paléozoïque',
        nl: 'Aquifersysteem van Paleozoische sokkel',
    },

    'UH/RBC_1a-hydro_state': {
        fr: 'libre',
        nl: 'Vrij',
    },
    'UH/RBC_1b-hydro_state': {
        fr: 'semi-captif',
        nl: 'Semi-afgesloten',
    },
    'UH/RBC_2-hydro_state': {
        fr: 'libre',
        nl: 'Vrij',
    },
    'UH/RBC_3-hydro_state': {
        fr: '-',
        nl: '-',
    },
    'UH/RBC_4-hydro_state': {
        fr: 'libre (localement captif)',
        nl: 'vrij (plaatselijk afgesloten)',
    },
    'UH/RBC_5-hydro_state': {
        fr: '-',
        nl: '-',
    },
    'UH/RBC_6-hydro_state': {
        fr: 'libre (localement captif)',
        nl: 'vrij (plaatselijk afgesloten)',
    },
    'UH/RBC_7a-hydro_state': {
        fr: '-',
        nl: '-',
    },
    'UH/RBC_7b-hydro_state': {
        fr: 'libre (localement captif)',
        nl: 'vrij (plaatselijk afgesloten)',
    },
    'UH/RBC_7c-hydro_state': {
        fr: '-',
        nl: '-',
    },
    'UH/RBC_8a-hydro_state': {
        fr: 'captif',
        nl: 'afgelsoten',
    },

    'UH/RBC_8b-hydro_state': {
        fr: '-',
        nl: '-',
    },
    'UH/RBC_9a-hydro_state': {
        fr: 'captif',
        nl: 'afgelsoten',
    },
    'UH/RBC_9b-hydro_state': {
        fr: 'captif',
        nl: 'afgelsoten',
    },

    'UH/RBC_1a-potential': {
        fr: 'Aucun potentiel',
        nl: 'Geen potentieel',
    },
    'UH/RBC_1b-potential': {
        fr: "Potentiel limité (faible extension et aux risques d'affaissement)",
        nl: 'Beperkt potentieel (geringe uitbreiding en risico op verzakking)',
    },
    'UH/RBC_2-potential': {
        fr: 'Aucun potentiel (faible extension et faible épaisseur)',
        nl: 'Geen potentieel (geringe uitbreiding en geringe dikte)',
    },
    'UH/RBC_3-potential': {
        fr: 'Aucun potentiel',
        nl: 'Geen potentieel',
    },
    'UH/RBC_4-potential': {
        fr: 'Potentiellement intéressant',
        nl: 'Potentieel interessant',
    },
    'UH/RBC_5-potential': {
        fr: 'Aucun potentiel',
        nl: 'Geen potentieel',
    },
    'UH/RBC_6-potential': {
        fr: 'Potentiel limité',
        nl: 'Beperkt potentieel',
    },
    'UH/RBC_7a-potential': {
        fr: 'Aucun potentiel',
        nl: 'Geen potentieel',
    },
    'UH/RBC_7b-potential': {
        fr: 'Potentiel limité',
        nl: 'Beperkt potentieel',
    },
    'UH/RBC_7c-potential': {
        fr: 'Aucun potentiel',
        nl: 'Geen potentieel',
    },
    'UH/RBC_8a-potential': {
        fr: 'Potentiellement intéressant',
        nl: 'Potentieel interessant',
    },
    'UH/RBC_8b-potential': {
        fr: 'Aucun potentiel',
        nl: 'Geen potentieel',
    },
    'UH/RBC_9a-potential': {
        fr: 'Potentiellement intéressant',
        nl: 'Potentieel interessant',
    },
    'UH/RBC_9b-potential': {
        fr: 'Potentiellement intéressant (la conductivité hydraulique du socle est très hétérogène et depend de la lithologie et du degré de fracturation)',
        nl: 'Potentieel interessant (de hydraulische geleidbaarheid van de sokkel is zeer heterogeen en is afhankelijk van de lithologie en de mate van fracturing)',
    },
    'US/RBC_1-remark': {
        fr: "Le modèle géologique utilisé ne permet pas une discrétisation des différentes unités stratigraphiques quaternaires. La conductivité thermique indiquée correspond à la valeur apparente intégrée sur toute l'épaisseur du Quaternaire.",
        nl: 'Het gebruikte geologische model laat geen discretisatie toe van de verschillende kwartaire stratigrafische eenheden. Het aangegeven thermisch geleidingsvermogen komt overeen met de gegeven waarde van de gehele dikte van het Quartair.',
    },
    'US/RBC_11-14-remark': {
        fr: "Le modèle géologique utilisé ne permet pas une discrétisation des différentes unités stratigraphiques quaternaires. La conductivité thermique indiquée correspond à la valeur apparente intégrée sur toute l'épaisseur du Quaternaire.",
        nl: 'Het gebruikte geologische model laat geen discretisatie toe van de verschillende kwartaire stratigrafische eenheden. Het aangegeven thermisch geleidingsvermogen komt overeen met de gegeven waarde van de gehele dikte van het Quartair.',
    },
    'US/RBC_21-remark': { fr: '', nl: '' },
    'US/RBC_22-remark': { fr: '', nl: '' },
    'US/RBC_23-remark': { fr: '', nl: '' },
    'US/RBC_25-remark': { fr: '', nl: '' },
    'US/RBC_31-remark': { fr: '', nl: '' },
    'US/RBC_41-remark': { fr: '', nl: '' },
    'US/RBC_42-remark': { fr: '', nl: '' },
    'US/RBC_43-remark': { fr: '', nl: '' },
    'US/RBC_44-remark': { fr: '', nl: '' },
    'US/RBC_51-remark': { fr: '', nl: '' },
    'US/RBC_61-remark': { fr: '', nl: '' },
    'US/RBC_71-remark': { fr: '', nl: '' },
    'US/RBC_72-remark': { fr: '', nl: '' },
    'US/RBC_73-remark': { fr: '', nl: '' },
    'US/RBC_81-remark': { fr: '', nl: '' },
    'US/RBC_82-remark': { fr: '', nl: '' },
    'US/RBC_91-remark': { fr: '', nl: '' },
    'US/RBC_92-remark': {
        fr: 'Le modèle géologique utilisé ne permet pas une discrétisation du Socle Paléozoïque en unités lithologiques. La conductivité thermique indiquée correspond à une valeur apparente indépendante des variations lithologiques.',
        nl: 'Het gebruikte geologische model laat geen discretisatie toe van de Paleozoïsche sokkel in lithologische eenheden. Het aangegeven thermisch geleidingsvermogen komt overeen met een gegeven waarde die onafhankelijk is van lithologische variaties.',
    },

    IV: {
        fr: 'IV-Cénozoïque',
        nl: 'IV-Cenozoïcum',
    },
    III: {
        fr: 'III-Cénozoïque',
        nl: 'III-Cenozoïcum',
    },
    II: {
        fr: 'II-Mésozoïque',
        nl: 'II-Mesozoïcum',
    },
    I: {
        fr: 'I-Paléozoïque',
        nl: 'I-Paleozoïcum',
    },

    phreatic: {
        fr: 'Système phréatique',
        nl: 'Grondwatersysteem',
    },

    confined: {
        fr: 'Système confiné',
        nl: 'Beperkt systeem',
    },

    Quaternary: {
        fr: 'Quaternaire',
        nl: 'Quartair',
    },
    Neogene: {
        fr: 'Néogène',
        nl: 'Neogeen',
    },
    Paleogene: {
        fr: 'Paléogène',
        nl: 'Paleogeen',
    },
    Cretacea: {
        fr: 'Crétacé',
        nl: 'Krijt',
    },
    Cambria: {
        fr: 'Cambrien',
        nl: 'Cambrium',
    },

    geoSerie1: {
        fr: '-',
        nl: '-',
    },
    geoSerie2: {
        fr: 'Holocène / Pléistocène',
        nl: 'Holoceen / Pleistoceen',
    },
    geoSerie3: {
        fr: 'Holocène',
        nl: 'Holoceen',
    },
    geoSerie4: {
        fr: 'Pléistocène',
        nl: 'Pleistoceen',
    },
    geoSerie5: {
        fr: 'Miocène supérieur',
        nl: 'Boven-Mioceen',
    },
    geoSerie6: {
        fr: 'Miocène inférieur',
        nl: 'Onder-Mioceen',
    },
    geoSerie7: {
        fr: 'Eocène supérieur',
        nl: 'Boven-Eoceen',
    },
    geoSerie8: {
        fr: 'Eocène moyen',
        nl: 'Midden-Eoceen',
    },
    geoSerie9: {
        fr: 'Eocène inférieur',
        nl: 'Onder-Eoceen',
    },
    geoSerie10: {
        fr: 'Paléocène supérieur',
        nl: 'Boven-Paleoceen',
    },
    geoSerie11: {
        fr: 'Crétacé supérieur',
        nl: 'Boven-Krijt',
    },
    geoSerie12: {
        fr: 'Cambrien inférieur (?)',
        nl: 'Onder-Cambrium',
    },

    geoEtage1: {
        fr: '-',
        nl: '-',
    },
    geoEtage2: {
        fr: '-',
        nl: '-',
    },
    geoEtage3: {
        fr: '-',
        nl: '-',
    },
    geoEtage4: {
        fr: '-',
        nl: '-',
    },
    geoEtage5: {
        fr: 'Diestien',
        nl: 'Diestiaan',
    },
    geoEtage6: {
        fr: 'Bolderien',
        nl: 'Bolderiaan',
    },
    geoEtage7: {
        fr: 'Tongerien',
        nl: 'Tongeriaan',
    },
    geoEtage8: {
        fr: 'Complexe de Kallo',
        nl: 'Kallo Complex',
    },
    geoEtage9: {
        fr: 'Ledien',
        nl: 'Lediaan',
    },
    geoEtage10: {
        fr: 'Bruxellien',
        nl: 'Brusseliaan',
    },
    geoEtage11: {
        fr: 'Paniselien',
        nl: 'Paniseliaan',
    },
    geoEtage12: {
        fr: 'Yprésien',
        nl: 'Ieperiaan',
    },
    geoEtage13: {
        fr: 'Landénien',
        nl: 'Landeniaan',
    },
    geoEtage14: {
        fr: 'Maastrichtien (Sénonien)',
        nl: 'Maastrichtiaan (Senonian)',
    },
    geoEtage15: {
        fr: 'Dévillien (?)',
        nl: 'Deviliaan',
    },
    outOfRegion: {
        fr: texts.outOfRegionFR,
        nl: texts.outOfRegionNL,
    },
    outOfRegionTechnical: {
        fr: texts.outOfRegionFRTechnical,
        nl: texts.outOfRegionNLTechnical,
    },
    outOfRegionLicense: {
        fr: texts.outOfRegionLicenseFR,
        nl: texts.outOfRegionLicenseNL,
    },

    disclaimerSidebar: {
        fr: texts.disclaimerFR,
        nl: texts.disclaimerNL,
    },

    disclaimerPrint: {
        fr: texts.disclaimerPrintFR,
        nl: texts.disclaimerPrintNL,
    },

    disclaimerDataUrl: {
        fr: 'https://environnement.brussels/content/brugeotool-conditions-legales-dutilisation',
        nl: 'https://leefmilieu.brussels/content/brugeotool-wettelijke-gebruiksvoorwaarden',
    },

    creditsSidebar: {
        fr: texts.creditsFR,
        nl: texts.creditsNL,
    },

    cakeMessageBefore: {
        fr: 'Augmentez le zoom sur la carte pour afficher le modèle géologique 3D',
        nl: 'Zoom in op de kaart om het 3D geologisch model weer te geven. ',
    },

    cakeMessageAfter: {
        fr: 'Diminuez le zoom sur la carte pour afficher le modèle géologique 3D',
        nl: 'Verlaag de zoom op de kaart om het 3D geologisch model weer te geven.',
    },

    closeIsPossible: {
        fr: 'Il est possible* d’installer un système géothermique fermé dans la zone sélectionnée.',
        nl: 'Het is mogelijk* een gesloten geothermisch systeem in de geselecteerde zone te installeren. ',
    },

    openIsPossible: {
        fr: 'Il est possible* d’installer un système géothermique ouvert dans la zone sélectionnée.',
        nl: 'Het is mogelijk* een open geothermisch systeem in de geselecteerde zone te installeren.',
    },

    licenseOpenCommonHeader: {
        fr: texts.licenseOpenFrCommonHeader,
        nl: texts.licenseOpenNlCommonHeader, // nldone
    },

    licenseOpenCommonBody: {
        fr: texts.licenseOpenFrCommonBody,
        nl: texts.licenseOpenNlCommonBody, // nldone
    },

    licenseOpenSoilCheck0or01or02: {
        fr: texts.licenseOpenSoilFrCheck0or01or02,
        nl: texts.licenseOpenSoilNlCheck0or01or02, // nldone
    },

    licenseOpenSoilCheck03or04: {
        fr: texts.licenseOpenSoilFrCheck03or04,
        nl: texts.licenseOpenSoilNlCheck03or04, // nldone
    },

    licenseOpenSoilCheck3or4: {
        fr: texts.licenseOpenSoilFrCheck3or4,
        nl: texts.licenseOpenSoilNlCheck3or4, // nldone
    },

    links: {
        fr: 'Liens',
        nl: 'Links', // nldone
        // en: 'Links'
    },

    info: {
        fr: 'Informations',
        nl: 'Informatie',
    },

    conductivity: {
        fr: 'Conductivité thermique',
        nl: 'Thermische geleiding',
        // en: 'Thermal conductivity'
    },

    goToSmartGeotherm: {
        fr: 'Accéder à l’outil',
        nl: 'Toegang tot de tool ',
    },

    otherUH: {
        fr: 'Autre unité hydrogéologique (encodage manuel des paramètres)',
        nl: 'Andere hydrogeologische eenheid (manuele invoering van de parameters)',
    },

    buildingClass: {
        fr: 'Classe de bâtiment',
        nl: 'Klassen van gebouwen',
    },

    thermicNeeds: {
        fr: 'Besoins thermiques de référence',
        nl: 'Referentiewaarden thermische behoeften',
    },

    FinTable_class: {
        fr: 'Classe',
        nl: 'Klasse',
    },

    FinTable_buildingType: {
        fr: 'Type de bâtiment',
        nl: 'Gebouwtype',
    },

    FinTable_isolationType: {
        fr: "Type d'isolation",
        nl: 'Type isolatie',
    },

    FinTable_surface: {
        fr: 'Surface [m²]',
        nl: 'Oppervlakte (m²)',
    },

    FinTable_hotNeeds: {
        fr: 'Chaud [kWh /m² /an]',
        nl: 'Warmte [kWh /m² /jaar]',
    },

    FinTable_coldNeeds: {
        fr: 'Froid [kWh /m² /an]',
        nl: 'Koude [kWh /m² /jaar]',
    },

    FinTable_hotWaterNeeds: {
        fr: 'Eau chaude sanitaire [kWh /an]',
        nl: 'Sanitair warm water [kWh /jaar]',
    },

    FinTable_closeSysTubeLength: {
        fr: 'Longueur totale de sonde [m]',
        nl: 'Totale lengte van de sondes [m]',
    },

    FinTable_closeSysTubesNumber: {
        fr: 'Nombre de sondes*',
        nl: 'Aantal sondes*',
    },

    FinTable_closeSysDrillingDepth: {
        fr: 'Profondeur  de forage [m]*',
        nl: 'Boordiepte [m]*',
    },

    FinTable_openSysFlow: {
        fr: 'Débit nappe [m³ /h]',
        nl: 'Grondwaterdebiet [m³ /h]',
    },

    FinTable_openSysDoubletNumber: {
        fr: 'Nombre de doublet*',
        nl: 'Aantal doubletten*',
    },

    finPageTitle: {
        fr: texts.finPageTitleFR,
        nl: texts.finPageTitleNL,
    },

    finPageRemarkClose: {
        fr: texts.finPageRemarkCloseFR,
        nl: texts.finPageRemarkCloseNL,
    },

    finPageRemarkOpen: {
        fr: texts.finPageRemarkOpenFR,
        nl: texts.finPageRemarkOpenNL,
    },

    thermicNeedsSwitch: {
        fr: 'Besoins thermiques',
        nl: 'Thermische behoeften',
    },

    uniResidential: {
        fr: 'Résidentiel unifamilial',
        nl: 'Eengezinswoning',
    },

    multiResidential: {
        fr: 'Résidentiel collectif',
        nl: 'Collectief woongebouw',
    },

    smallOffice: {
        fr: 'Bureaux (petite taille)',
        nl: 'Kantoren (klein)',
    },

    mediumOffice: {
        fr: 'Bureaux (taille moyenne)',
        nl: 'Kantoren (middelgroot)',
    },

    hugeOffice: {
        fr: 'Bureaux (très grande taille)',
        nl: 'Kantoren (zeer groot)',
    },

    newIso: {
        fr: 'neuf',
        nl: 'nieuwbouw',
    },

    renewIso: {
        fr: 'rénové',
        nl: 'renovatie',
    },

    relative: {
        fr: 'Relatif (m)',
        nl: 'Relatieve (m)',
    },

    absolute: {
        fr: 'Absolu (m-DNG)',
        nl: 'Absoluut (m-TAW)',
    },

    wmsLegendDisplay: {
        fr: 'Montrer la légende du fond de carte',
        nl: 'De legende van de kaart op de achtergrond tonen',
    },

    wmsLegendHide: {
        fr: 'Masquer la légende du fond de carte',
        nl: 'De legende van de kaart op de achtergrond verbergen',
    },

    loadingData: {
        fr: 'Chargement des données',
        nl: 'Data worden geladen',
    },
    highPerformance: {
        fr: 'Haute performance',
        nl: 'Hoge vertoningsperfomance',
    },
    goodPerformance: {
        fr: 'Bonne performance',
        nl: 'Goeie vertoningsperfomance',
    },
    highQuality: {
        fr: 'Haute qualité',
        nl: 'Hoge vertoningsprecisie',
    },
    goodQuality: {
        fr: 'Bonne qualité',
        nl: 'Goeie vertoningsprecisie',
    },
    lowAlpha: {
        fr: 'Transparent',
        nl: 'Hoge vertoningsprecisie', //TODO
    },
    highAlpha: {
        fr: 'Opaque',
        nl: 'Goeie vertoningsprecisie', //TODO
    },
    scalingZlabel: {
        fr: 'Scaling Z',
        nl: 'Scaling Z',
    },
    lowDepthScaling: {
        fr: '+',
        nl: '+',
    },
    highDepthScaling: {
        fr: '-',
        nl: '-',
    },
    geology: {
        fr: 'Géologie', //TODO
        nl: 'Geology', //TODO
    },
    hydrogeology: {
        fr: 'Hydrogéologie', //TODO
        nl: 'Hydrogeology', //TODO
    },

    'legend/lithologic/619b': {
        en: 'Clay / Silt',
        fr: 'Argile / Silt',
        nl: 'Klei / Silt', // Nl to check
    },

    'legend/lithologic/602': {
        en: 'Gravel or conglomerate',
        fr: 'Sable et gravier',
        nl: 'Zand en grind', // Nl to check
    },
    'legend/lithologic/607': {
        en: 'Massive sand or sandstone',
        fr: 'Sable',
        nl: 'Zand',
    },
    'legend/lithologic/616': {
        en: 'Silt / siltstone, or shaly silt',
        fr: 'Silt / sable et argile',
        nl: 'Silt / zand en klei', // nl to check
    },
    // 'legend/lithologic/619': {
    //     en: 'Sandy or silty shale',
    //     fr: 'Argile alluviale hétérogène, tourbes localement',
    //     nl: 'Heterogene alluviale klei, plaatselijk veen', // nl to check
    // },
    'legend/lithologic/619': {
        fr: 'Quaternaire indifférencié',
        nl: 'Ongedifferentieerde quartaire', // nl to check
    },
    'legend/lithologic/620': {
        en: 'Clay or clay shale',
        fr: 'Argile',
        nl: 'Clay',
    },
    'legend/lithologic/626': {
        en: 'Chalk',
        fr: 'Craie',
        nl: 'Krijt',
    },
    'legend/lithologic/705': {
        en: 'Schist',
        fr: 'Schiste et grès, roche de socle',
        nl: 'Schalie en zandsteen, vast gesteente', // nl to check
    },
    phreaticLevel: {
        fr: 'Niveau phréatique',
        nl: 'Waterstand', // nl to check
    },
    piezoLevels: {
        fr: 'Niveaux piézométriques',
        nl: 'Piezometrische niveaus', // nl to check
    },
    slicePiezoLegendLabelLine: {
        fr: 'Piézométrie (m_DNG)',
        nl: 'Piëzometrie (m_TAW)', // nl to check
    },
    slicePiezoLegendLabelImage: {
        fr: 'Extension UH',
        nl: 'Uitbreidings HE', // nl to check
    },
    geothermicAnalysisHelptext: {
        fr: 'Effectuez un clic simple sur la carte pour sélectionner le lieu où vous souhaitez lancer l’analyse géothermique',
        nl: 'Klik op de kaart om de plaats te kiezen waar u de geothermische analyse wilt starten', //TODO
        en: 'Make a single click on the map to select the location where you want to start the geothermic analysis',
    },
    technicalAnalysisHelptext: {
        fr: 'Effectuez un clic simple sur la carte pour sélectionner le lieu où vous souhaitez lancer le forage virtuel',
        nl: 'Eén klik op de kaart om de plaats te selecteren waar u de virtuele boor wilt starten', //TODO
        en: 'Make a single click on the map to select the location where you want to start the virtual drill',
    },
    labelAquifere: {
        fr: 'Aquifère',
        nl: 'Aquifer',
    },
    labelAquitard: {
        fr: 'Aquitard',
        nl: 'Aquitard',
    },
    labelAquiclude: {
        fr: 'Aquiclude',
        nl: 'Aquiclude',
    },
};

export type GeoMessageDB = typeof messages;
export type GeoMessageKey = keyof GeoMessageDB;

declare module 'sdi/locale' {
    export interface MessageStore {
        geo(k: GeoMessageKey): Translated;
    }
}

MessageStore.prototype.geo = function (k: GeoMessageKey) {
    return this.getEdited('geo', k, () => formatMessage(messages[k]));
};

export const updateMessageDB = (
    strati: InfoStrati[],
    opened: InfoOpen[],
    closed: InfoClosed[]
) => updateRecords(strati, opened, closed, messages);
