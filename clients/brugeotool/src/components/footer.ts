import { DIV, A, SPAN } from 'sdi/components/elements';
import { markdown } from 'sdi/ports/marked';
import tr, { fromRecord } from 'sdi/locale';
import { getAppUrl } from 'sdi/app';

const renderGeoCredits = () =>
    SPAN(
        { className: 'content--footer' },
        markdown(tr.geo('creditsSidebar'), 'credits md')
    );

const renderUrl = (name: string) =>
    getAppUrl(name).map(url =>
        A(
            { className: 'footerLink', href: fromRecord(url.url) },
            fromRecord(url.label)
        )
    );

const csLink = SPAN(
    { className: 'cs-link', title: 'cartostation' },
    A({ href: 'http://cartostation.com', target: '_blank' }, `cartostation`)
);

const acLink = SPAN(
    { className: 'ac-link', title: 'atelier cartographique' },
    A(
        { href: 'http://atelier-cartographique.be', target: '_blank' },
        `atelier cartographique`
    )
);

const csacCredits = SPAN('csac-credits', csLink, '©', acLink);

const footerInfos = () =>
    SPAN('footer-infos', renderUrl('wcag'), renderUrl('contact'));

const render = () =>
    DIV('footer', footerInfos(), ' ', renderGeoCredits(), csacCredits);

export default render;
