import { DIV, H3, NODISPLAY, SPAN } from 'sdi/components/elements';
import {
    buttonTooltipLeft,
    buttonTooltipBottomRight,
} from 'sdi/components/tooltip';
import { renderTopMap, renderBottomMap } from './map';

import {
    toggleDataswitcher,
    resetSystem,
    setSystem,
    setPinnable,
    setDisplayData,
    setMeasureBox,
    clearBottomRender,
    setBottomRender,
    initSlicing,
    endSlicing,
    setDisplayMapsSeparator,
    endCake,
    setMapButtonHelpText,
} from '../events/geothermie';
import {
    getBottomRender,
    getDisplayMapsSeparator,
    getSystem,
    getPinnable,
    getInfoPanel,
    isMeasureBox,
    foldBottomRender,
    getDisplayDataswitch,
    getGeologicalGroupId,
    getMapButtonHelpText,
} from '../queries/geothermie';
import { renderCake, cakeInZoomRange } from './cake';
import { renderSlice } from './slice';
import { renderHomeSidebar } from './sidebar';
import {
    setInteractionClick,
    setInteractionSelect,
    zoomIn,
    zoomOut,
    resetRotate,
    startMeasureLength,
    startMeasureArea,
    stopMeasure,
    dispatchView,
    setInteraction,
    setGroupVisibility,
    clearGroupVisibility,
} from '../events/map';
import {
    getInteraction,
    getTopView,
    getMeasuredGeometryType,
    getInteractionMode,
    // getSelectedFeature,
} from '../queries/map';
import { renderSelectWMS } from './map/wms';
import tr from 'sdi/locale';
import { nameToString } from 'sdi/components/button/names';
import { measureQueryFactory } from 'sdi/map';
import { makeIcon, makeLabelAndIcon } from 'sdi/components/button';

import { clearSelectedFeature } from 'brugeotool/src/events/map';
import { setFocusId } from 'sdi/app';
import { geocoderWithCoords, makeActionSelect } from 'sdi/components/geocoder';
import { Coordinate } from 'ol/coordinate';
import { navigateGeneral } from '../events/route';
import { actionCenter } from './geocoder';
import { activityLogger } from '../events/app';
import { linkAction } from 'sdi/activity';
import { none, some } from 'fp-ts/lib/Option';

// if (getSelectedFeature()) {
//     toggleDataswitcher();
//     clearSelectedFeature();
// } else {
//     toggleDataswitcher();
// }

const resetLeftButtons = () => setInteractionSelect();

const buttonData = () =>
    buttonTooltipBottomRight(tr.geo('dataSwitch'), {
        className: `geo-btn geo-btn--icon geo-btn--2 geo-btn--data ${dataClass()}`,
        onClick: () => {
            toggleDataswitcher();
            resetLeftButtons();
            clearSelectedFeature(null);
            getDisplayDataswitch() === 'map-data'
                ? activityLogger(linkAction('show-data-manager'))
                : null;
        },
    });
const dataClass = () => (getInfoPanel() === 'presentation' ? '' : 'active');

const selectClass = () =>
    !getPinnable() && getInteraction().label === 'select' ? 'active' : '';

export const generalButtonClass = () =>
    !getPinnable() &&
    getInteraction().label === 'singleclick' &&
    getSystem().isSome()
        ? 'active'
        : '';

export const technicalButtonClass = () =>
    !getPinnable() &&
    getInteraction().label === 'singleclick' &&
    getSystem().isNone()
        ? 'active'
        : '';

const getRenderCake = () => foldBottomRender('cake', true, false);

const getRenderSlice = () => foldBottomRender('slice', true, false);

const cakeModeButtonClass = () => foldBottomRender('cake', 'active', '');

const sliceModeButtonClass = () => foldBottomRender('slice', 'active', '');

const pinModeButtonClass = () => (getPinnable() ? 'active' : '');

const buttonSelect = () =>
    buttonTooltipBottomRight(tr.geo('mapSelect'), {
        className: `geo-btn geo-btn--2 geo-btn--icon geo-btn--select ${selectClass()}`,
        onClick: () => {
            setInteractionSelect();
        },
    });

const endAllBottom = () => {
    if (getRenderCake()) {
        endCake();
        clearBottomRender();
        setGroupVisibility(getGeologicalGroupId(), false);
    } else if (getRenderSlice()) {
        endSlicing();
        clearBottomRender();
        setInteractionSelect();
        setGroupVisibility(getGeologicalGroupId(), false);
    }
};

const buttonGeneral = () =>
    buttonTooltipBottomRight(tr.geo('tooltipGeoAnalysis'), {
        className: `geo-btn geo-btn--2 geo-btn--icon geo-btn--open  ${generalButtonClass()}`,
        onClick: () => {
            setSystem(getSystem().getOrElse('close'));
            setInteractionClick();
            setDisplayData(false);
            clearSelectedFeature(null);
            endAllBottom();
            setMapButtonHelpText(true);
            activityLogger(linkAction('geo-analysis'));
        },
    });

const buttonTechnical = () =>
    buttonTooltipBottomRight(tr.geo('tooltipGeoSynthesis'), {
        className: `geo-btn geo-btn--2 geo-btn--icon geo-btn--drill   ${technicalButtonClass()}`,
        onClick: () => {
            resetSystem();
            setInteractionClick();
            setDisplayData(false);
            clearSelectedFeature(null);
            endAllBottom();
            setMapButtonHelpText(true);
            activityLogger(linkAction('geo-drill'));
        },
    });

const button3d = () =>
    buttonTooltipBottomRight(tr.geo('tooltipCakeMode'), {
        className: `geo-btn geo-btn--2 geo-btn--icon geo-btn--3d ${cakeModeButtonClass()} `,
        onClick: () => {
            if (getRenderCake()) {
                endCake();
                clearBottomRender();
                setGroupVisibility(getGeologicalGroupId(), false);
            } else {
                activityLogger(linkAction('show-3d'));
                setBottomRender('cake');
                setDisplayMapsSeparator('middle');
                endSlicing();
                setInteractionSelect();
                clearGroupVisibility();
                setGroupVisibility(getGeologicalGroupId(), true);
            }
        },
    });

const buttonSlice = () =>
    buttonTooltipBottomRight(tr.geo('tooltipSliceName'), {
        className: `geo-btn geo-btn--2 geo-btn--icon geo-btn--slice ${sliceModeButtonClass()} `,
        onClick: () => {
            if (getRenderSlice()) {
                endSlicing();
                clearBottomRender();
                setInteractionSelect();
                setGroupVisibility(getGeologicalGroupId(), false);
            } else {
                activityLogger(linkAction('show-slice'));
                setBottomRender('slice');
                setDisplayMapsSeparator('middle');
                initSlicing();
                endCake();
                setGroupVisibility(getGeologicalGroupId(), true);
            }
        },
    });

const buttonPin = () =>
    buttonTooltipBottomRight(tr.geo('tooltipPinLabel'), {
        className: `geo-btn geo-btn--2 geo-btn--icon geo-btn--pin ${pinModeButtonClass()} `,
        onClick: () => {
            // mind the order here. to not bother checking many places
            // setInteractionClick sets pinnable to false.
            setInteractionClick();
            setPinnable(true);
        },
    });

const buttonMeasure = () =>
    buttonTooltipBottomRight(tr.geo('tooltipMeasure'), {
        className: `geo-btn geo-btn--2 geo-btn--icon geo-btn--measure ${
            getInteractionMode() === 'measure' ? 'active' : ''
        }`,
        onClick: () => {
            setMeasureBox(!isMeasureBox());
            setFocusId('measure-box__body');
        },
    });

const buttonZoomIn = () =>
    buttonTooltipLeft(
        tr.core('zoomIn'),
        {
            className: 'geo-btn geo-btn--2 geo-btn--icon geo-btn--zoom-in',
            onClick: zoomIn,
        },
        DIV('btn-icon', nameToString('plus'))
    );

const buttonZoomOut = () =>
    buttonTooltipLeft(
        tr.core('zoomOut'),
        {
            className: 'geo-btn geo-btn--2 geo-btn--icon geo-btn--zoom-out',
            onClick: zoomOut,
        },
        DIV('btn-icon', nameToString('minus'))
    );

const buttonNorth = () =>
    buttonTooltipLeft(
        tr.core('north'),
        {
            className: 'geo-btn geo-btn--2 geo-btn--icon geo-btn--north',
            onClick: resetRotate,
        },
        DIV(
            'btn-icon',
            SPAN(
                {
                    style: {
                        transform: `rotate(${
                            getTopView().rotation - 90
                        }deg) scaleY(0.75)`,
                    },
                },
                nameToString('play')
            )
        )
    );

const closeHelpTextButton = makeIcon('close', 2, 'times', {
    text: () => tr.core('close'),
    position: 'right',
});

const renderGeneralButtonActive = () =>
    DIV(
        'map-helptext',
        tr.geo('geothermicAnalysisHelptext'),
        closeHelpTextButton(() => setMapButtonHelpText(false))
    );

const renderTechnicalButtonActive = () =>
    DIV(
        'map-helptext',
        tr.geo('technicalAnalysisHelptext'),
        closeHelpTextButton(() => setMapButtonHelpText(false))
    );

const leftTools = () =>
    DIV(
        'tools-left',
        DIV(
            'tools-actions',
            DIV(
                'btn-list big-tools-btn',
                button3d(),
                buttonSlice(),
                buttonTechnical(),
                buttonGeneral()
            ),
            DIV(
                'btn-list small-tools-btn',
                buttonData(),
                buttonSelect(),
                buttonPin(),
                buttonMeasure()
            )
        ),
        generalButtonClass() === 'active' && getMapButtonHelpText()
            ? renderGeneralButtonActive()
            : NODISPLAY(),
        technicalButtonClass() === 'active' && getMapButtonHelpText()
            ? renderTechnicalButtonActive()
            : NODISPLAY()
    );

const rightTools = () =>
    DIV(
        'tools-right',
        renderSelectWMS(),
        buttonZoomIn(),
        buttonZoomOut(),
        buttonNorth()
    );

export const contentClassName = () => {
    const dms = getDisplayMapsSeparator();
    if (dms === 'bottom-hidden' || cakeInZoomRange() === 'in') {
        return `content ${dms}`;
    } else {
        return `content middle`;
    }
};

const buttonStopMeasure = makeLabelAndIcon('close', 1, 'stop', () =>
    tr.core('stop')
);

const btnMeasureLength = makeLabelAndIcon('start', 2, 'arrows-alt-h', () =>
    tr.geo('measureLength')
);

const btnMeasureArea = makeLabelAndIcon('start', 2, 'arrows-alt', () =>
    tr.geo('measureArea')
);

const { getMeasured } = measureQueryFactory(getInteraction);

const renderMeasureTools = () =>
    DIV(
        {},
        getMeasuredGeometryType().map(t =>
            t === 'LineString'
                ? H3({}, tr.geo('measureLength'))
                : H3({}, tr.geo('measureArea'))
        ),
        DIV('helptext', tr.geo('helptext:measure')),
        DIV(
            'measure-box__body',
            DIV(
                'value',
                getMeasured().map(m => m)
            ),
            buttonStopMeasure(() => {
                stopMeasure();
                setMeasureBox(false);
            })
        )
    );

const renderMeasure = () =>
    isMeasureBox()
        ? DIV(
              'measure-box',
              DIV(
                  'tool measure',
                  DIV(
                      'tool-body',
                      btnMeasureLength(() => {
                          startMeasureLength();
                          setFocusId(`stop-measure`);
                      }),
                      btnMeasureArea(() => {
                          startMeasureArea();
                          setFocusId(`stop-measure`);
                      }),
                      getInteractionMode() === 'measure'
                          ? renderMeasureTools()
                          : NODISPLAY()
                  )
              )
          )
        : NODISPLAY();

const select = (coord: Coordinate) => {
    setSystem(getSystem().getOrElse('close'));
    navigateGeneral(coord[0], coord[1]);
};

const renderTop = () =>
    getDisplayMapsSeparator() === 'top-hidden'
        ? none
        : some(
              DIV(
                  'content-top',
                  geocoderWithCoords(
                      dispatchView,
                      setInteraction,
                      [
                          actionCenter,
                          makeActionSelect(tr.core('analyseAtAddress'), select),
                      ],
                      {
                          name: tr.core('analyseAtAddress'),
                          icon: 'play',
                          action: select,
                      }
                  ),
                  leftTools(),
                  // renderGeocoder(),
                  rightTools(),
                  renderTopMap(),
                  renderMeasure()
                  // maskLeft(),
                  // maskRight()
                  // buttonMoveUp()
              )
          );

const content = () =>
    DIV(
        { className: contentClassName() },
        renderTop(),
        getBottomRender()
            .map(which => {
                switch (which) {
                    case 'cake':
                        return renderCake();
                    case 'map':
                        return renderBottomMap();
                    case 'slice':
                        return renderSlice();
                }
            })
            .map(node => DIV('content-bottom', node))
    );

export const renderHome = () =>
    DIV('home-wrapper', renderHomeSidebar(), content());

export default renderHome;
