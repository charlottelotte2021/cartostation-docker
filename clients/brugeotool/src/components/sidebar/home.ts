import * as debug from 'debug';

import {
    A,
    DIV,
    H1,
    NodeOrOptional,
    // NODISPLAY,
    SPAN,
} from 'sdi/components/elements';
import { renderConfig } from 'sdi/components/feature-view';
import tr from 'sdi/locale';

import {
    renderBottomSwitch,
    renderMapData,
    renderMapLegend,
    renderButtonPrint,
} from './map-switch';
import { renderWmsLegend } from '../legend/legend-wms';
import {
    getBottomRender,
    getDisplayDataswitch,
    getDisplayMapsSeparator,
    getInfoPanel,
} from '../../queries/geothermie';
import {
    getCurrentMapId,
    findSelectedFeature,
    findMap,
    getSelectedFeatureOpt,
    topMapName,
} from 'brugeotool/src/queries/map';
import { Option, fromNullable } from 'fp-ts/lib/Option';
import { InfoPanel } from '../geothermie';
import mapInfo from './map-info';
import { setInfoPanel, popInfoPanel } from 'brugeotool/src/events/geothermie';
import {
    buttonTooltipBottomRight,
    divTooltipBottomLeft,
} from 'sdi/components/tooltip';

import { scopeOption } from 'sdi/lib';
import factory, { initialTimeserieState } from 'sdi/components/timeserie';
import { clearSelectedFeature } from 'brugeotool/src/events/map';
import { nameToString } from 'sdi/components/button/names';
import { cakeInZoomRange } from '../cake';

const logger = debug('sdi:geothermie');

export const contentClassName = () => {
    const dms = getDisplayMapsSeparator();
    if (dms === 'bottom-hidden' || cakeInZoomRange() === 'in') {
        return `content ${dms}`;
    } else {
        return `content middle`;
    }
};

const buttonInfo = (onClick: () => void, extraClas: string) =>
    buttonTooltipBottomRight(
        tr.geo('infoAndLegend'),
        {
            className: `geo-btn geo-btn--2  ${extraClas}`,
            onClick,
        },
        tr.geo('legend')
    );

const buttonData = (onClick: () => void, extraClas: string) =>
    buttonTooltipBottomRight(
        tr.geo('dataManagement'),
        {
            className: `geo-btn geo-btn--2 ${extraClas}`,
            onClick,
        },
        tr.geo('data')
    );

const buttonsDataWrapper = () =>
    DIV(
        'btn-wrapper',
        buttonData(
            () => setInfoPanel('map-data'),
            getInfoPanel() === 'map-data' ? 'active' : ''
        ),
        buttonInfo(
            () => setInfoPanel('map-legend'),
            getInfoPanel() === 'map-legend' ? 'active' : ''
        ),
        renderButtonPrint()
    );

const homeBeLink = () =>
    DIV(
        'geo-btn geo-btn--1 geo-btn--link backlink-bgtool',
        SPAN('btn__icon', nameToString('chevron-left')),
        SPAN(
            'btn__label',
            A(
                {
                    href: tr.geo('backToBeHomeLink'),
                    target: '_blank',
                },
                tr.geo('backToBeHome')
            )
        )
    );

const logos = () =>
    DIV('logo__wrapper', DIV('logo logo--brugeo'), DIV('logo logo--feder'));

const renderMapInfo = (mid: string) =>
    findMap(mid).map(mapInfo).getOrElse(DIV({}, 'ERROR: could not find map'));

// const renderGeneralButtonActive = () =>
//     DIV('map-helptext', tr.geo('geothermicAnalysisHelptext'));

// const renderTechnicalButtonActive = () =>
//     DIV('map-helptext', tr.geo('technicalAnalysisHelptext'));

const renderWelcome = () =>
    getBottomRender()
        .map(which =>
            DIV(
                'sidebar sidebar-welcome',
                DIV(
                    'content middle',
                    DIV(
                        'content-top',
                        H1({}, 'BrugeoTool'),
                        getCurrentMapId(topMapName).map(renderMapInfo),
                        // generalButtonClass() === 'active'
                        //     ? renderGeneralButtonActive()
                        //     : NODISPLAY(),
                        // technicalButtonClass() === 'active'
                        //     ? renderTechnicalButtonActive()
                        //     : NODISPLAY(),
                        homeBeLink(),
                        logos()
                    ),

                    DIV('content-bottom', renderBottomSwitch(which))
                )
            )
        )
        .getOrElseL(() =>
            DIV(
                'sidebar sidebar-welcome',
                DIV(
                    'content-top',
                    H1({}, 'BrugeoTool'),
                    getCurrentMapId(topMapName).map(renderMapInfo)
                    // generalButtonClass() === 'active'
                    //     ? renderGeneralButtonActive()
                    //     : NODISPLAY(),
                    // technicalButtonClass() === 'active'
                    //     ? renderTechnicalButtonActive()
                    //     : NODISPLAY()
                ),
                DIV('content-bottom', homeBeLink(), logos())
            )
        );

const renderFeatureInfo = (): Option<NodeOrOptional> =>
    scopeOption()
        .let('feature', findSelectedFeature())
        .let('mapId', getCurrentMapId(topMapName))
        .let('map', ({ mapId }) => findMap(mapId))
        .let('path', getSelectedFeatureOpt())
        .let('layerId', ({ path }) => fromNullable(path.layerId))
        .let('layerInfo', ({ map, layerId }) =>
            fromNullable(map.layers.find(l => l.id === layerId))
        )
        .map(({ layerInfo, feature }) => {
            const plotter = factory(
                () => initialTimeserieState(),
                () => null,
                () => null,
                () => void 0,
                () => void 0
            );
            return DIV(
                {},
                renderCloseFeatureView(),
                renderConfig(layerInfo.featureViewOptions, feature, plotter)
            );
        });

const wrapRenderMap = (nodes: NodeOrOptional[]) => DIV({}, ...nodes);

const renderInfoPanel = (mapId: string, infoPanel: InfoPanel) => {
    switch (infoPanel) {
        case 'map-legend':
            return wrapRenderMap([renderMapLegend(mapId), renderWmsLegend()]);
        case 'map-data':
            return wrapRenderMap([renderMapData(mapId)]);
        case 'presentation':
            return DIV({}, 'ERROR: Logical Error');
    }
};

const renderCloseFeatureView = () =>
    divTooltipBottomLeft(
        tr.core('close'),
        {
            className: 'geo-btn geo-btn--3 geo-btn--icon geo-btn--close',
            onClick: () => clearSelectedFeature(popInfoPanel),
        },
        DIV('btn__icon', nameToString('times'))
    );

const renderDataswitch = () =>
    DIV(
        'sidebar sidebar-dataswitch',
        DIV(
            { className: contentClassName() },
            DIV(
                'content-top',
                // renderCloseFeatureView(),
                renderFeatureInfo().getOrElse(
                    getCurrentMapId(topMapName).map(mid =>
                        DIV(
                            {},
                            DIV(
                                'data-header',
                                H1({}, tr.geo('layerManager')),
                                buttonsDataWrapper()
                            ),
                            renderInfoPanel(mid, getDisplayDataswitch())
                        )
                    )
                )
            ),
            getBottomRender().map(which =>
                DIV('content-bottom', renderBottomSwitch(which))
            )
        )
    );

export const renderHomeSidebar = () =>
    getDisplayDataswitch() === 'presentation'
        ? renderWelcome()
        : renderDataswitch();

logger('loaded');
