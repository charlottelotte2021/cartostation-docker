import { DIV } from 'sdi/components/elements';
import { getDatasetMetadata, getMapInfo } from 'brugeotool/src/queries/app';
import {
    setInteractionPrint,
    setPrintRequest,
} from 'brugeotool/src/events/map';
import tr from 'sdi/locale';
import { legendLegendRenderer, legendDataRenderer } from '../legend';
import {
    BottomRender,
    withoutToolingLayers,
} from 'brugeotool/src/queries/geothermie';
import renderCakeLegend from '../cake/legend';
import renderSliceLegend from '../slice/legend';
import { openPrintMap } from 'brugeotool/src/events/modal';
import { uniqId } from 'sdi/util';
import { makeIcon } from 'sdi/components/button';

const renderLegendLegend = legendLegendRenderer(getDatasetMetadata);
const renderLegendData = legendDataRenderer(getDatasetMetadata);

export const renderMapLegend = (mid: string) =>
    DIV(
        {
            className: 'legend-wrapper',
            key: `legend-${mid}`,
        },
        getMapInfo(mid).map(minfo =>
            DIV(
                { className: 'styles-wrapper' },
                ...renderLegendLegend(withoutToolingLayers(minfo.layers))
            )
        )
    );

export const renderMapData = (mid: string) =>
    DIV(
        {
            className: 'legend-wrapper',
            key: `legend-${mid}`,
        },
        getMapInfo(mid).map(minfo =>
            DIV(
                { className: 'styles-wrapper' },
                ...renderLegendData(withoutToolingLayers(minfo.layers))
            )
        )
    );

const printButton = makeIcon('info', 2, 'print', {
    text: () => tr.core('print'),
    position: 'bottom-left',
});
export const renderButtonPrint = () =>
    printButton(() => {
        setInteractionPrint();
        setPrintRequest({
            id: uniqId(),
            width: 180,
            height: 180,
            resolution: 200,
            props: {},
        });
        openPrintMap();
    });

const renderLegendTitle = (title: string) =>
    DIV(
        {
            key: `bottom-cake`,
            className: `radio__item map__title selected`,
        },
        DIV({ className: 'radio__label' }, title)
    );

const renderLegend = (which: BottomRender) => {
    switch (which) {
        case 'map':
            return [];
        case 'cake':
            return [renderLegendTitle(tr.geo('cakeName')), renderCakeLegend()];
        case 'slice':
            return [renderLegendTitle(tr.geo('sliceName')), renderSliceLegend()];
    }
};

export const renderBottomSwitch = (which: BottomRender) =>
    DIV({ className: 'map-list__wrapper' }, ...renderLegend(which));
