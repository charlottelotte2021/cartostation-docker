import { getOrientation } from 'brugeotool/src/queries/app';
import * as debug from 'debug';

import {
    DETAILS,
    DIV,
    H2,
    H3,
    NodeOrOptional,
    SPAN,
    SUMMARY,
} from 'sdi/components/elements';
import tr from 'sdi/locale';

import { navigateSystem } from '../../events/route';
import {
    renderLithology,
    renderHydroLegend,
} from '../legend/lithostratigraphy';
import { renderSidebarBottomContent, renderMinimapWrapper } from './geothermie';

const logger = debug('sdi:geothermie');

const buttonClose = () =>
    DIV(
        {
            className: 'geo-btn geo-btn--back-to-map geo-btn--2',
            onClick: () => navigateSystem('close', 'general'),
        },
        DIV('geo-btn__inner', SPAN('btn__label', tr.geo('geoAnalysis')))
    );

const legend = () =>
    DIV(
        'legend-log',

        DETAILS(
            {
                className: 'strati-legend-details',
                open: true,
            },
            SUMMARY('strati-legend-summary', H3('', tr.geo('lithoLegend'))),
            renderLithology()
        ),

        DETAILS(
            {
                className: 'hydro-legend-details',
                open: true,
            },
            SUMMARY('hydro-legend-summary', H3('', tr.geo('hydroLegend'))),
            renderHydroLegend()
        )
    );

const wrapTop = (...nodes: NodeOrOptional[]) => DIV('sidebar--top', ...nodes);

export const renderTechnicalSidebarTopContent = () =>
    getOrientation() === 'horizontal'
        ? wrapTop(
              DIV('sidebar-header', H2('', tr.geo('bothSystem'))),
              buttonClose(),
              legend(),
              renderMinimapWrapper()
          )
        : wrapTop(
              DIV('sidebar-header', H2('', tr.geo('bothSystem'))),
              buttonClose(),
              renderMinimapWrapper()
          );

export const renderTechnicalSidebar = () =>
    DIV(
        'sidebar sidebar-general',
        renderTechnicalSidebarTopContent(),
        renderSidebarBottomContent()
    );

logger('loaded');
