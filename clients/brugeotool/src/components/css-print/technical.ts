import { DIV, IMG, H1, BR, H3 } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { engine } from '../carrot/engine';
import { renderHydroGeolTable } from '../table';
import { renderSidebarTopContent } from '../sidebar/geothermie';

import { logoBE } from './logo';
import { printHelp, printDate, projectUrl } from './elements';
import { markdown } from 'sdi/ports/marked';
import { renderConnect } from '../table/connect';
import { getDiagramHeight } from 'brugeotool/src/queries/geothermie';
import {
    renderLithology,
    renderHydroLegend,
} from '../legend/lithostratigraphy';

const { getImage } = engine();

const header = () =>
    DIV(
        'print-header',
        H1({}, tr.geo('printTitle')),
        printDate(),
        projectUrl(),
        IMG({ className: 'logo', src: logoBE }),
        BR({}),
        markdown(tr.geo('disclaimerPrint'))
    );

const page1 = () =>
    DIV(
        'page',
        header(),
        renderSidebarTopContent(),
        H3({}, tr.geo('lithoLegend')),
        renderLithology(),
        H3({}, tr.geo('hydroLegend')),
        renderHydroLegend()
    );

const page2 = () =>
    DIV(
        'page landscape landscape--A3',
        DIV(
            'page__inner',
            H1({}, tr.geo('bothSystem')),
            DIV(
                { className: 'page__content', style: { display: 'flex' } },
                DIV('carrot ', IMG({ src: getImage(300, getDiagramHeight()) })),
                renderConnect('carrot', 'depth'),
                renderHydroGeolTable()
            )
        )
    );

export const render = () => DIV('css-print', printHelp(), page1(), page2());

export default render;
