import * as debug from 'debug';
import * as io from 'io-ts';
import { ReactNode } from 'react';
import { DIV, NodeOrOptional, A, SPAN } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { getLang, rect } from 'sdi/app';
import {
    getSystem,
    getDataConstraint,
    getDepthMode,
} from '../queries/geothermie';
import renderCarrot from './carrot';
import renderPiezo from './carrot/piezo';
import renderPaginator from './paginator';
import {
    renderGeoSidebar,
    renderTechnicalSidebar,
    renderSidebarTopContent,
    renderSidebarBottomContent,
    renderTechnicalSidebarTopContent,
} from './sidebar';
import {
    renderDepthTable,
    renderHydroGeolTable,
    renderCloseTable,
    renderGraphDiv,
    renderOpenTable,
    renderFinancialTable,
} from './table';
import renderText from './text';
import { setDiagramHeight } from '../events/geothermie';
import renderConnect from './table/connect';
import { getOrientation, getLayout } from '../queries/app';
import { none, some, fromNullable, Option } from 'fp-ts/lib/Option';
import { getCenterAsLambert2008 } from '../queries/map';
import { encodeUrlQuery } from 'sdi/util';
import { getSmartGOThermBaseUrl } from './settings';
import { scopeOption } from 'sdi/lib';
import { renderToggleDepthMode } from './button';
import { foldRemote } from 'sdi/source';
import { ConstraintPoint } from '../remote/io';
import { nameToString } from 'sdi/components/button/names';

const logger = debug('sdi:geo/geo');

// tslint:disable-next-line:variable-name
export const SystemIO = io.union(
    [io.literal('open'), io.literal('close')],
    'SystemIO'
);

export type System = io.TypeOf<typeof SystemIO>;

// tslint:disable-next-line: variable-name
export const DisplayMapsSeparatorIO = io.union(
    [
        io.literal('bottom-hidden'),
        io.literal('middle'),
        io.literal('top-hidden'),
    ],
    'DisplayMapsSeparatorIO'
);

export type DisplayMapsSeparator = io.TypeOf<typeof DisplayMapsSeparatorIO>;

export type SearchType = 'geocoder' | 'capakey' | 'coordinates';

export type InfoPanel = 'presentation' | 'map-legend' | 'map-data';

// const rectify = rect(r => { setDiagramHeight(r.height); });

const computeMaxHeight = (minHeight: number) => {
    const hs = [
        [600, 800],
        [800, 900],
        [1000, 1100],
        [1200, 1200],
        [1400, 1200],
        [1600, 1400],
        [1800, 1500],
        [2000, 1800],
        [2200, 2000],
        [2400, 2200],
        [2600, 2400],
        [2800, 2800],
        [3000, 3000],
        [100000000, 3000],
    ];
    const windowHeight = window.innerHeight;
    const h = fromNullable(hs.find(([p, _]) => p < windowHeight))
        .map(([_, h]) => h)
        .getOrElse(3000);
    return Math.max(minHeight, h);
};

const rectifyElement = (element: Element | null) => {
    if (element !== null) {
        element.querySelectorAll('.guide').forEach(guide => {
            rect((r, _e) => {
                scopeOption()
                    .let(
                        'contentBody',
                        fromNullable(document.querySelector('.content--body'))
                    )
                    .map(({ contentBody }) => {
                        const tables = guide.querySelectorAll('table');
                        const guideHasTableChild = tables.length > 0;
                        const cbcr = contentBody.getBoundingClientRect();
                        const minHeight = cbcr.height;
                        const maxHeight = computeMaxHeight(minHeight);
                        const guideHeight = guideHasTableChild
                            ? tables[0].getBoundingClientRect().height
                            : r.height;

                        // const currentHeight = getDiagramHeight();
                        const diff = maxHeight - minHeight;
                        if (guideHeight <= minHeight) {
                            setDiagramHeight(minHeight);
                        } else if (guideHeight >= maxHeight) {
                            setDiagramHeight(maxHeight);
                        } else if (guideHeight <= minHeight + 0.5 * diff) {
                            setDiagramHeight(minHeight + 0.5 * diff);
                        } else if (guideHeight <= minHeight + 0.8 * diff) {
                            setDiagramHeight(minHeight + 0.8 * diff);
                        } else {
                            setDiagramHeight(maxHeight);
                        }
                    });
            })(guide);
        });
    }
};

const renderOutRegionLink = () => {
    const [x, y] = getCenterAsLambert2008().map(Math.round);
    const q = encodeUrlQuery({
        use: 1,
        lang: getLang(),
        z: 9,
        x,
        y,
    });
    return getLayout() === 'technical'
        ? A(
              {
                  className: '',
                  href: 'https://www.dov.vlaanderen.be/portaal/?module=verkenner',
                  target: '_blank',
              },
              'Databank Ondergrond Vlaanderen (DOV-Verkenner)'
          )
        : A(
              {
                  className: '',
                  href: `${getSmartGOThermBaseUrl('extern')}?${q}`,
                  target: '_blank',
              },
              'SmartGeoTherm'
          );
};

const renderOutRegion = () =>
    DIV(
        'content',
        DIV(
            'content--body disclaimer',
            SPAN(
                {},
                getLayout() === 'technical'
                    ? tr.geo('outOfRegionTechnical')
                    : tr.geo('outOfRegion'),
                renderOutRegionLink(),
                '.'
            )
        )
    );

const withinRegion = (node: NodeOrOptional) =>
    foldRemote<ConstraintPoint, string, Option<NodeOrOptional>>(
        () => none,
        () =>
            some(
                DIV(
                    'info',
                    SPAN('icon fa fa-spin', nameToString('circle-notch')),
                    SPAN({}, ` ${tr.core('loadingData')}`)
                )
            ),
        err =>
            some(
                DIV(
                    'error',
                    SPAN('icon fa', nameToString('times')),
                    ` Error fetching data ${err}`
                )
            ),
        data => (data.constraints.region !== 'none' ? some(node) : none)
    )(getDataConstraint());

const renderGeothermie = (...nodes: ReactNode[]) => {
    const orientation = getOrientation();
    switch (orientation) {
        case 'horizontal':
            return renderHorizontal(...nodes);
        case 'vertical':
            return renderVertical(...nodes);
    }
};

const renderContentBody = (...nodes: ReactNode[]) =>
    DIV(
        {
            key: 'geo-content-body',
            className: 'content--body',
        },
        DIV(
            {
                className: 'content--body-inner',
                ref: rectifyElement,
                key: `content--body-inner-${getLayout()}`,
            },
            ...nodes
        )
    );

const renderContent = (...nodes: ReactNode[]) =>
    DIV({ className: 'content' }, ...nodes);

const renderHorizontal = (...nodes: ReactNode[]) =>
    DIV(
        { className: 'geo-output horizontal' },
        renderGeoSidebar(),
        withinRegion(
            renderContent(renderPaginator(), renderContentBody(...nodes))
        ).getOrElse(renderOutRegion())
    );

const renderVertical = (...nodes: ReactNode[]) =>
    DIV(
        { className: 'geo-output vertical' },
        renderSidebarTopContent(),
        withinRegion(renderPaginator()),
        withinRegion(renderContent(renderContentBody(...nodes))).getOrElse(
            renderOutRegion()
        ),
        renderSidebarBottomContent()
    );

const renderGeothermieTechnical = (...nodes: ReactNode[]) => {
    const orientation = getOrientation();
    switch (orientation) {
        case 'horizontal':
            return renderHorizontalTechnical(...nodes);
        case 'vertical':
            return renderVerticalTechnical(...nodes);
    }
};

const renderHorizontalTechnical = (...nodes: ReactNode[]) =>
    DIV(
        { className: 'geo-output horizontal' },
        renderTechnicalSidebar(),
        withinRegion(renderContent(renderContentBody(...nodes))).getOrElse(
            renderOutRegion()
        )
    );

const renderVerticalTechnical = (...nodes: ReactNode[]) =>
    DIV(
        { className: 'geo-output vertical' },
        renderTechnicalSidebarTopContent(),
        withinRegion(renderContent(renderContentBody(...nodes))).getOrElse(
            renderOutRegion()
        ),
        renderSidebarBottomContent()
    );

export const general = () => renderGeothermie(renderCarrot(), renderText());

export const license = () =>
    renderGeothermie(
        renderDepthTable('depth-licence'),
        renderCarrot(),
        renderText()
    );

export const geology = () =>
    getSystem().map(sys => {
        if (sys === 'close') {
            return renderGeothermie(
                renderToggleDepthMode(['absolute', 'relative'], getDepthMode()),
                renderCarrot(),
                renderGraphDiv(),
                renderConnect('conductivity', 'depth'),
                renderCloseTable()
            );
        } else {
            return renderGeothermie(
                renderToggleDepthMode(['absolute', 'relative'], getDepthMode()),
                renderCarrot(),
                renderPiezo(),
                renderConnect('carrot', 'depth'),
                renderOpenTable()
            );
        }
    });

export const finance = () => renderGeothermie(renderFinancialTable());

export const technical = () =>
    renderGeothermieTechnical(
        // renderGeoTable(),
        renderToggleDepthMode(['absolute', 'relative'], getDepthMode()),
        renderCarrot(),
        renderPiezo(),
        renderConnect('carrot', 'depth'),
        renderHydroGeolTable()
        // renderHydroTable()
    );

logger('loaded');
