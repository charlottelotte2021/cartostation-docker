import { DIV, SPAN, NODISPLAY, H2, A, P } from 'sdi/components/elements';
import tr from 'sdi/locale';

import {
    isConstraintNatura,
    isExcluded,
    isConstraintSoil,
    isStrictlyConstraintSoil,
    isConstraints,
    getConstraintSoil,
    getSystem,
    getConstraintRegion,
    isConstraintMetroNord,
    isConstraintWater,
} from '../../queries/geothermie';
import { markdown } from 'sdi/ports/marked';
import { nameToString } from 'sdi/components/button/names';
// import {
//     licenseOpenFrCommonHeader,
//     licenseOpenFrCommonBody,
//     licenseOpenSoilFrCheck0or01or02,
//     licenseOpenSoilFrCheck03or04,
//     licenseOpenSoilFrCheck3or4
// } from 'geothermie/src/locale/marked-texts';

const renderExclusionIcon = () =>
    SPAN({ className: 'icon forbidden' }, nameToString('times')); // TODO
const renderNoExclusionIcon = () =>
    SPAN({ className: 'icon allowed' }, nameToString('check')); // TODO
const renderConstraintIcon = () =>
    SPAN({ className: 'icon warning' }, nameToString('exclamation-triangle')); // TODO

const renderSoilCategory = () =>
    !isStrictlyConstraintSoil()
        ? NODISPLAY() // case when cat = "none"
        : SPAN(
              { className: 'soil-polution' },
              A(
                  {
                      href: 'https://environnement.brussels/thematiques/sols/linventaire-de-letat-du-sol/quelles-sont-les-5-categories-de-linventaire',
                      target: '_blank',
                  },
                  `${tr.geo('polutionCategory')} ${getConstraintSoil()}`
              )
          );

const renderConstraintHeaderTitle1 = () =>
    H2({ className: 'constraint__section-title' }, tr.geo('projectSituation'));

const renderConstraintHeaderTitle2 = () =>
    H2({ className: 'constraint__section-title' }, tr.geo('projectSteps'));

const renderSoilConstraint = () => {
    switch (getConstraintSoil()) {
        case '0':
            return DIV(
                {},
                P(
                    { className: 'constraints__header--soil__title' },
                    tr.geo('constraintHeaderSoilCase0'),
                    '(',
                    renderSoilCategory(),
                    ').'
                ),
                getSystem().map(s =>
                    s === 'close'
                        ? DIV(
                              { className: 'info' },
                              tr.geo('constraintHeaderSoilcondUrbaClose')
                          )
                        : DIV(
                              { className: 'info' },
                              tr.geo('constraintHeaderSoilcondUrbaOpen')
                          )
                ),
                getSystem().map(s =>
                    s === 'close'
                        ? DIV(
                              { className: 'info' },
                              tr.geo('constraintHeaderSoilcondEnviClose')
                          )
                        : NODISPLAY()
                )
            );
        case '1':
            return DIV(
                {},
                P(
                    { className: 'constraints__header--soil__title' },
                    tr.geo('constraintHeaderSoilCase1'),
                    '(',
                    renderSoilCategory(),
                    ').'
                )
            );
        case '2':
            return DIV(
                {},
                P(
                    { className: 'constraints__header--soil__title' },
                    tr.geo('constraintHeaderSoilCase2'),
                    '(',
                    renderSoilCategory(),
                    ').'
                )
            );
        case '3':
            return DIV(
                {},
                P(
                    { className: 'constraints__header--soil__title' },
                    tr.geo('constraintHeaderSoilCase3'),
                    '(',
                    renderSoilCategory(),
                    ').'
                ),
                getSystem().map(s =>
                    s === 'close'
                        ? DIV(
                              { className: 'info' },
                              tr.geo('constraintHeaderSoilcondEnviClose')
                          )
                        : DIV(
                              { className: 'info' },
                              tr.geo('constraintHeaderSoilcondEnviOpen')
                          )
                )
            );
        case '4':
            return DIV(
                {},
                P(
                    { className: 'constraints__header--soil__title' },
                    tr.geo('constraintHeaderSoilCase4'),
                    '(',
                    renderSoilCategory(),
                    ').'
                ),
                getSystem().map(s =>
                    s === 'close'
                        ? DIV(
                              { className: 'info' },
                              tr.geo('constraintHeaderSoilcondEnviClose')
                          )
                        : DIV(
                              { className: 'info' },
                              tr.geo('constraintHeaderSoilcondEnviOpen')
                          )
                )
            );
        case '0+1':
        case '0+2':
            return DIV(
                {},
                P(
                    { className: 'constraints__header--soil__title' },
                    tr.geo('constraintHeaderSoilCaseMixed'),
                    renderSoilCategory(),
                    '.'
                ),
                getSystem().map(s =>
                    s === 'close'
                        ? DIV(
                              { className: 'info' },
                              tr.geo('constraintHeaderSoilcondUrbaClose')
                          )
                        : DIV(
                              { className: 'info' },
                              tr.geo('constraintHeaderSoilcondUrbaOpen')
                          )
                ),
                getSystem().map(s =>
                    s === 'close'
                        ? DIV(
                              { className: 'info' },
                              tr.geo('constraintHeaderSoilcondEnviClose')
                          )
                        : NODISPLAY()
                )
            );
        case '0+3':
        case '0+4':
            return DIV(
                {},
                P(
                    { className: 'constraints__header--soil__title' },
                    tr.geo('constraintHeaderSoilCaseMixed'),
                    renderSoilCategory(),
                    '.'
                ),
                getSystem().map(s =>
                    s === 'close'
                        ? DIV(
                              { className: 'info' },
                              tr.geo('constraintHeaderSoilcondUrbaClose')
                          )
                        : DIV(
                              { className: 'info' },
                              tr.geo('constraintHeaderSoilcondUrbaOpen')
                          )
                ),
                getSystem().map(s =>
                    s === 'close'
                        ? DIV(
                              { className: 'info' },
                              tr.geo('constraintHeaderSoilcondEnviClose')
                          )
                        : DIV(
                              { className: 'info' },
                              tr.geo('constraintHeaderSoilcondEnviOpen')
                          )
                )
            );
        case 'none':
        default:
            return DIV(
                {},
                P(
                    { className: 'constraints__header--soil__title' },
                    tr.geo('constraintHeaderSoilCaseNone')
                )
            );
    }
};

const renderConstraintsHeader = () =>
    isExcluded()
        ? DIV(
              { className: 'constraints__header' },
              renderConstraintHeaderTitle1(),
              DIV(
                  { className: 'constraints__header--water forbidden' },
                  DIV(
                      {},
                      renderExclusionIcon(),
                      getSystem().map(s =>
                          s === 'open'
                              ? markdown(
                                    tr.geo(
                                        'constraintHeaderWaterTrueResultOpen'
                                    )
                                )
                              : markdown(
                                    tr.geo(
                                        'constraintHeaderWaterTrueResultClose'
                                    )
                                )
                      )
                  )
              ),
              DIV(
                  { className: '~constraints__header--exclusion' },
                  isConstraintWater()
                      ? markdown(tr.geo('constraintHeaderWaterTrue'))
                      : markdown(tr.geo('constraintHeaderMetroTrue'))
              )
          )
        : DIV(
            { className: 'constraints__header' },
            renderConstraintHeaderTitle1(),
            DIV(
                { className: 'constraints__header--water' },
                DIV(
                    {},
                    renderNoExclusionIcon(),
                    markdown(tr.geo('constraintHeaderWaterFalse'))
                )
            ),
            DIV(
                { className: 'constraints__header--metronord' },
                isConstraintMetroNord()
                    ? DIV(
                            {},
                            renderConstraintIcon(),
                            markdown(tr.geo('constraintHeaderMetroNordTrue'))
                        )
                    : DIV(
                            {},
                            renderNoExclusionIcon(),
                            markdown(tr.geo('constraintHeaderMetroNordFalse'))
                        )
            ),
            DIV(
                { className: 'constraints__header--soil' },
                DIV(
                    {},
                    isConstraintSoil()
                        ? renderConstraintIcon()
                        : renderNoExclusionIcon(),
                    renderSoilConstraint()
                )
            ),
            DIV(
                { className: 'constraints__header--natura' },
                isConstraintNatura()
                    ? DIV(
                        {},
                        renderConstraintIcon(),
                        markdown(tr.geo('constraintHeaderNaturaTrue'))
                    )
                    : DIV(
                        {},
                        renderNoExclusionIcon(),
                        markdown(tr.geo('constraintHeaderNaturaFalse'))
                    )
            ),
            DIV(
                { className: 'constraints__header--urban' },
                renderConstraintIcon(),
                markdown(tr.geo('constraintHeaderUrban'))
            ),

          );

const renderExclusion = () => DIV({}, '');

const renderCloseConstraint = () =>
    DIV(
        {
            className:
                'constraints__body constraints__body--constraints--close',
        },
        renderConstraintHeaderTitle2(),
        markdown(tr.geo('licenseClose'))
    );

const renderOpenNaturaConstraint = () =>
    DIV(
        {
            className:
                'constraints__body constraints__body--constraints--open--natura',
        },
        renderConstraintHeaderTitle2(),
        markdown(tr.geo('licenseOpenCommonHeader')),
        isConstraintSoil() ? renderOpenSoilConstraintSoilInfo() : NODISPLAY(),
        markdown(tr.geo('licenseOpenCommonBody'))
    );

const renderOpenSoilConstraintSoilInfo = () => {
    switch (getConstraintSoil()) {
        case '0':
        case '0+1':
        case '0+2':
            return markdown(tr.geo('licenseOpenSoilCheck0or01or02'));
        case '3':
        case '4':
            return markdown(tr.geo('licenseOpenSoilCheck3or4'));
        case '0+3':
        case '0+4':
            return markdown(tr.geo('licenseOpenSoilCheck03or04'));
        default:
            return '';
    }
};

const renderOpenSoilConstraint = () =>
    DIV(
        {
            className:
                'constraints__body constraints__body--constraints--open--soil',
        },
        renderConstraintHeaderTitle2(),
        // markdown(tr.geo('licenseOpenSoil')),
        markdown(tr.geo('licenseOpenCommonHeader')),
        renderOpenSoilConstraintSoilInfo(),
        markdown(tr.geo('licenseOpenCommonBody'))
    );

const renderNoConstraints = () =>
    getSystem().getOrElse('close') === 'close'
        ? DIV(
              {
                  className:
                      'constraints__body constraints__body--no-constraints--close',
              },
              renderConstraintHeaderTitle2(),
              markdown(tr.geo('licenseClose'))
          )
        : DIV(
              {
                  className:
                      'constraints__body constraints__body--no-constraints--open',
              },
              renderConstraintHeaderTitle2(),
              markdown(tr.geo('licenseOpenCommonHeader')),
              markdown(tr.geo('licenseOpenCommonBody'))
          );

const render = () =>
    getConstraintRegion() !== 'in'
        ? DIV(
              { className: 'disclaimer' },
              markdown(tr.geo('outOfRegionLicense'), 'md md--large')
          )
        : DIV(
              { className: 'paginator-text' },
              renderConstraintsHeader(),
              isExcluded()
                  ? DIV({}, renderExclusion())
                  : isConstraints() // any constraints ?
                  ? getSystem().getOrElse('close') === 'close'
                      ? renderCloseConstraint()
                      : isConstraintNatura()
                      ? renderOpenNaturaConstraint()
                      : renderOpenSoilConstraint()
                  : renderNoConstraints()
          );

export default render;
