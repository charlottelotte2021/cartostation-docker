/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { Option, fromNullable, none, some } from 'fp-ts/lib/Option';

import { ILayerInfo, LayerGroup, Inspire, getMessageRecord } from 'sdi/source';
import tr, { fromRecord } from 'sdi/locale';
import { DIV, SPAN } from 'sdi/components/elements';
import { makeIcon } from 'sdi/components/button';

import {
    groupVisibility,
    groupFolding,
    getLayerInfo,
    groupBulleVisibility,
} from 'brugeotool/src/queries/map';
import {
    // setGroupVisibility,
    setLayerVisibility,
    setGroupFolding,
    setLayerInfo,
    setGroupBulleVisibility,
} from 'brugeotool/src/events/map';
import { withoutToolingLayers } from 'brugeotool/src/queries/geothermie';
import { findInfoBulle } from 'brugeotool/src/queries/app';
import { markdown } from 'sdi/ports/marked';

const logger = debug('sdi:legend-item');

const withLabel =
    (layerInfo: ILayerInfo, md: Option<Inspire>) =>
    (nodes: React.ReactNode[]) => {
        const infoIsVisible = getLayerInfo(layerInfo.id).getOrElse(false);
        const legendOpt = fromNullable(layerInfo.legend)
            .map(fromRecord)
            .chain(r => (r.trim().length > 0 ? some(r) : none));

        const isVisible = layerInfo.visible;
        const vButton = isVisible
            ? makeIcon('toggle-off', 3, 'eye', {
                  position: 'top-left',
                  text: tr.core('viewLayer'),
              })
            : makeIcon('toggle-on', 3, 'eye-slash', {
                  position: 'top-left',
                  text: tr.core('hideLayer'),
              }); // TODO: check position & text
        const vClass = isVisible ? 'visible' : 'disabled';

        const labelButton = makeIcon('info', 3, 'info-circle', {
            position: 'bottom-left',
            text: tr.core('label'),
        }); // TODO: check position & text

        return [
            DIV(
                { className: `${vClass} legend-group__item` },
                DIV(
                    { className: 'layer-title' },
                    SPAN(
                        { className: 'layer-label' },
                        md.map(md =>
                            fromRecord(getMessageRecord(md.resourceTitle))
                        )
                    ),
                    legendOpt.map(() =>
                        labelButton(() =>
                            setLayerInfo(layerInfo.id, !infoIsVisible)
                        )
                    ),
                    vButton(() => setLayerVisibility(layerInfo.id, !isVisible))
                ),
                legendOpt
                    .chain(l => (infoIsVisible ? some(l) : none))
                    .map(label => DIV({ className: `legend-label` }, label)),
                ...nodes
            ),
        ];
    };

export const renderLegendItem = (
    layerInfo: ILayerInfo,
    md: Option<Inspire>
) => {
    const label = withLabel(layerInfo, md);
    return label([]);
};

export interface Group {
    g: LayerGroup | null;
    layers: ILayerInfo[];
}

export const groupItems = (layers: ILayerInfo[]) =>
    withoutToolingLayers(layers)
        .reverse()
        .reduce<Group[]>((acc, info) => {
            const ln = acc.length;
            if (ln === 0) {
                return [
                    {
                        g: info.group,
                        layers: [info],
                    },
                ];
            }
            const prevGroup = acc[ln - 1];
            const cg = info.group;
            const pg = prevGroup.g;
            // Cases:
            // info.group == null && prevGroup.g == null => append
            // info.group != null && prevGroup.g != null && info.group.id == prevGroup.id => append
            if (
                (cg === null && pg === null) ||
                (cg !== null && pg !== null && cg.id === pg.id)
            ) {
                prevGroup.layers.push(info);
                return acc;
            }
            // info.group == null && prevGroup.g != null => new
            // info.group != null && prevGroup.g == null => new
            // info.group != null && prevGroup.g != null && info.group.id != prevGroup.id => new

            return acc.concat({
                g: cg,
                layers: [info],
            });
        }, []);

export type MetadataGetter = (id: string) => Option<Inspire>;
export const renderGroups = (
    groups: Group[],
    getDatasetMetadata: MetadataGetter
) =>
    groups.map(group => {
        const visibleLayers =
            group.layers.filter(l => l.visible === true).length > 0;
        const isVisible = fromNullable(group.g?.id).fold(true, groupVisibility);
        const isFolded = fromNullable(group.g?.id).fold(
            !visibleLayers,
            (id: string) => groupFolding(id).getOrElse(!visibleLayers)
        );
        const isBulleVisible = fromNullable(group.g?.id)
            .chain(groupBulleVisibility)
            .getOrElse(false);

        const layers = group.layers.filter(_ => true);

        // const items = visibleLayers
        //     ? layers.map(layer =>
        //         renderLegendItem(layer, getDatasetMetadata(layer.metadataId))
        //     )
        //     : [];
        const groupGroup = group.g;
        if (groupGroup !== null) {
            const items = isFolded
                ? []
                : layers.map(layer =>
                      renderLegendItem(
                          layer,
                          getDatasetMetadata(layer.metadataId)
                      )
                  );
            const vizClass = isVisible ? ' open ' : ' close ';
            const foldClass = isFolded ? ' fold ' : ' unfold ';

            // const vButton = isVisible
            //     ? makeIcon('toggle-off', 3, 'eye')
            //     : makeIcon('toggle-on', 3, 'eye-slash');

            const fButton = isFolded
                ? makeIcon('collapsed', 3, 'caret-right', {
                      position: 'left',
                      text: tr.core('expand'),
                  }) // TODO: check position & label
                : makeIcon('expanded', 3, 'caret-down', {
                      position: 'left',
                      text: tr.core('collapse'),
                  }); // TODO: check position & label
            const bulleButton = makeIcon('info', 3, 'info-circle', {
                position: 'bottom',
                text: tr.geo('info'),
            }); // TODO: check position & label

            const foldFunc = () => setGroupFolding(groupGroup.id, !isFolded);

            return DIV(
                { className: `legend-group named ${vizClass} ${foldClass}` },
                DIV(
                    {
                        className: 'legend-group-title',
                    },
                    fButton(foldFunc),
                    SPAN(
                        {
                            className: 'group-name',
                            onClick: foldFunc,
                        },
                        fromRecord(groupGroup.name)
                    ),
                    findInfoBulle(groupGroup.id).map(() =>
                        bulleButton(() =>
                            setGroupBulleVisibility(
                                groupGroup.id,
                                !isBulleVisible
                            )
                        )
                    )
                ),
                DIV(
                    { className: 'legend-group-items' },
                    findInfoBulle(groupGroup.id)
                        .chain(b => (isBulleVisible ? some(b) : none))
                        .map(bulle =>
                            DIV(
                                {
                                    className: 'group-bulle legend-label',
                                },
                                markdown(fromRecord(bulle.content))
                            )
                        ),
                    items
                )
            );
        }
        const items = layers.map(layer =>
            renderLegendItem(layer, getDatasetMetadata(layer.metadataId))
        );
        return DIV({ className: 'legend-group anonymous' }, items);
    });

export const legendRenderer =
    (getDatasetMetadata: MetadataGetter) => (layers: ILayerInfo[]) =>
        renderGroups(groupItems(layers), getDatasetMetadata);

export const hasZoomConstraint = (layerInfo: ILayerInfo): boolean => {
    return layerInfo.minZoom !== 0 || layerInfo.maxZoom !== 30;
};

export const getItemZoom = (zoomConstraint: boolean) => {
    return zoomConstraint ? SPAN({ className: 'fa fa-search-plus' }) : null;
};

logger('loaded');
