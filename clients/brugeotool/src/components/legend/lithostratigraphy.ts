import { none, some } from 'fp-ts/lib/Option';
import { DIV, IMG } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { getPatternUrl } from '../patterns';
import { CANONICAL_PATTERN_UH_BASEROCK } from '../settings';

// const US_IDS = [602, 607, 616, 619, 620, 626, CANONICAL_PATTERN_UH_BASEROCK];
const US_IDS = [619, 620, 616, 607, 602, 626, CANONICAL_PATTERN_UH_BASEROCK];
const lito_IDS = [620, 619, 616, 607, 602, 626, CANONICAL_PATTERN_UH_BASEROCK];

const stratiLabel = (id: number) => {
    switch (id) {
        case 602:
            return some(tr.geo('legend/lithologic/602'));
        case 607:
            return some(tr.geo('legend/lithologic/607'));
        case 616:
            return some(tr.geo('legend/lithologic/616'));
        case 619:
            return some(tr.geo('legend/lithologic/619'));
        case 620:
            return some(tr.geo('legend/lithologic/620'));
        case 626:
            return some(tr.geo('legend/lithologic/626'));
        case 705:
            return some(tr.geo('legend/lithologic/705'));
        default:
            return none;
    }
};

const lithoLabel = (id: number) => {
    switch (id) {
        case 602:
            return some(tr.geo('legend/lithologic/602'));
        case 607:
            return some(tr.geo('legend/lithologic/607'));
        case 616:
            return some(tr.geo('legend/lithologic/616'));
        case 619:
            return some(tr.geo('legend/lithologic/619b'));
        case 620:
            return some(tr.geo('legend/lithologic/620'));
        case 626:
            return some(tr.geo('legend/lithologic/626'));
        case 705:
            return some(tr.geo('legend/lithologic/705'));
        default:
            return none;
    }
};

const renderStratiItem = (id: number) =>
    DIV(
        {
            className: `lithostrti-item strati-${id}`,
        },
        DIV('style', IMG({ src: getPatternUrl(id) })),
        DIV('label', stratiLabel(id))
    );

const renderLithoItem = (id: number) =>
    DIV(
        {
            className: `lithostrti-item strati-${id}`,
        },
        DIV('style', IMG({ src: getPatternUrl(id) })),
        DIV('label', lithoLabel(id))
    );

export const renderHydroLegend = () =>
    DIV(
        'legend--hydro',
        DIV(
            'hydro-item aquifere',
            DIV('style'),
            DIV('label', tr.geo('labelAquifere'))
        ),
        DIV(
            'hydro-item aquitard',
            DIV('style'),
            DIV('label', tr.geo('labelAquitard'))
        ),
        DIV(
            'hydro-item aquiclude',
            DIV('style'),
            DIV('label', tr.geo('labelAquiclude'))
        )
    );

export const renderLithostratigraphy = () =>
    DIV('legend--lithostrti', ...US_IDS.map(renderStratiItem));

export const renderLithology = () =>
    DIV('legend--lithology', ...lito_IDS.map(renderLithoItem));

export default renderLithostratigraphy;
