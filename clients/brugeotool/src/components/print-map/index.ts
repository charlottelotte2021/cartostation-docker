/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV, H2 } from 'sdi/components/elements';
import { IMapInfo } from 'sdi/source';
import tr from 'sdi/locale';

import {
    getPrintResponse,
    getCurrentMapInfo,
    getInteraction,
    topMapName,
} from '../../queries/map';
import { setPrintResponse, setInteractionSelect } from '../../events/map';
import { renderPDF } from './generate';
import { ReactNode } from 'react';
import { makeLabel } from 'sdi/components/button';

const logger = debug('sdi:view/print');

const wrap = (...children: ReactNode[]) =>
    DIV({ className: 'print-progress' }, ...children);

const wrapItem = (
    passed: boolean,
    current: boolean,
    ...children: ReactNode[]
) =>
    DIV(
        {
            className: `print-progress-item ${
                passed ? 'done' : current ? 'doing' : 'todo'
            }`,
        },
        ...children
    );

const renderInitial = (step: number) =>
    wrapItem(step > 0, step === 0, DIV({}, 'printNotStarted'));

const renderDownload = (step: number) =>
    wrapItem(step > 1, step === 1, DIV({}, 'printDownloadingBaseMap'));

const renderPrepare = (step: number) =>
    wrapItem(step > 2, step === 2, DIV({}, 'printPreparingPDF'));

const renderItems = (step: number) =>
    wrap(renderInitial(step), renderDownload(step), renderPrepare(step));

const renderError = () => wrap(DIV({}, 'Error'));

const renderPrintProgress = (close: () => void) => (mapInfo: IMapInfo) => {
    const iLabel = getInteraction().label;
    if (iLabel !== 'print') {
        window.setTimeout(finish(close), 3000);
        return DIV({}, 'ERROR: Not Printing');
    }
    const response = getPrintResponse();
    switch (response.status) {
        case 'error':
            return renderError();
        case 'none':
            return renderItems(0);
        case 'start':
            return renderItems(1);
        case 'end':
            window.setTimeout(() => renderPDF(mapInfo, response), 0);
            setPrintResponse({
                id: response.id,
                data: '',
                extent: [0, 0, 0, 0],
                status: 'done',
                props: response.props,
            });
            return renderItems(2);
    }
    return renderItems(3);
};

const closeModalButton = makeLabel('close', 2, () => tr.core('close'));

const header = () => H2({}, tr.core('print'));

const footer = (close: () => void) => DIV({}, closeModalButton(finish(close)));

const body = (close: () => void) =>
    getCurrentMapInfo(topMapName).foldL(() => {
        window.setTimeout(finish(close), 3000);
        return DIV({}, 'ERROR: NO MAP');
    }, renderPrintProgress(close));

export const finish = (close: () => void) => () => {
    setInteractionSelect();
    close();
};

export const render = { header, footer, body };

logger('loaded');
