import { fromNullable } from 'fp-ts/lib/Option';
import { makeSlice, resetAddress } from 'brugeotool/src/events/geothermie';
import {
    setScaleLine,
    updateMapView,
    clearSelectedFeature,
    setSelectedFeature,
    startMark,
    endMark,
    setPrintResponse,
    addPositionMarker,
    setInteraction,
    updateLoading,
} from 'brugeotool/src/events/map';
import {
    navigateFinance,
    navigateGeneral,
    navigateGeology,
    navigateLicense,
    navigateTechnical,
} from 'brugeotool/src/events/route';
import { getLayout } from 'brugeotool/src/queries/app';
import { getSystem, getPinnable } from 'brugeotool/src/queries/geothermie';
import {
    getBaseLayer,
    getCurrentMapInfo,
    getInteraction,
    getView,
    getSelectedFeature,
    getPrintRequest,
    getLoading,
    SLICE_LAYER_NAME,
    MapName,
    topMapName,
    bottomMapName,
    sliceInteractionStyle,
} from 'brugeotool/src/queries/map';
import { DIV, SPAN, H3 } from 'sdi/components/elements';
import {
    create,
    IMapOptions,
    singleSelectOptions,
    measureEventsFactory,
} from 'sdi/map';
import { renderScaleline } from './scaleline';
import { MessageRecord } from 'sdi/source';
import tr, { fromRecord } from 'sdi/locale';
import { Coordinate } from 'ol/coordinate';

const { mapSetTarget, registerTarget } = (function () {
    const targeters: Partial<{ [k in MapName]: (e: Element | null) => void }> =
        {};

    const registerTarget = (name: MapName, t: (e: Element | null) => void) => {
        targeters[name] = t;
    };

    const mapSetTarget = (name: MapName, e: Element | null) =>
        fromNullable(targeters[name]).map(f => f(e));

    return { mapSetTarget, registerTarget };
})();

const { mapUpdate, mapInsert } = (function () {
    const updaters: Partial<{ [k in MapName]: () => void }> = {};

    const mapInsert = (name: MapName, u: () => () => void) => {
        if (!(name in updaters)) {
            updaters[name] = u();
        }
    };
    const mapUpdate = (name: MapName) =>
        fromNullable(updaters[name]).map(f => f());
    return { mapInsert, mapUpdate };
})();

const options = (name: MapName): IMapOptions => ({
    element: null,
    getBaseLayer: () => getBaseLayer(name).toNullable(),
    getMapInfo: () => getCurrentMapInfo(name).toNullable(),
    updateView: updateMapView(name),
    getView: getView(name),
    setLoading: updateLoading,
    setScaleLine,
});

const { updateMeasureCoordinates, stopMeasure } = measureEventsFactory(
    setInteraction,
    getInteraction
);

const setPosition = ([x, y]: Coordinate, isActive: boolean) => {
    if (isActive || getPinnable()) {
        resetAddress();
        switch (getLayout()) {
            case 'home': {
                if (getPinnable()) {
                    addPositionMarker([x, y]);
                    return;
                } else {
                    return getSystem().foldL(
                        () => {
                            navigateTechnical(x, y);
                        },
                        () => {
                            navigateGeneral(x, y);
                        }
                    );
                }
            }
            case 'general':
                return navigateGeneral(x, y);
            case 'license':
                return navigateLicense(x, y);
            case 'geology':
                return navigateGeology(x, y);
            case 'finance':
                return navigateFinance(x, y);
            case 'technical':
                return navigateTechnical(x, y);
        }
    }
};

const MIN_ZOOM = 4;
const MAX_ZOOM = 15;

const attachMap = (name: MapName) => (element: HTMLElement | null) => {
    if (name === topMapName) {
        mapInsert(name, () => {
            const {
                update,
                setTarget,
                selectable,
                clickable,
                follow,
                andThen,
                markable,
                printable,
                measurable,
                editable,
            } = create(name, {
                ...options(name),
                element,
            });

            selectable(
                singleSelectOptions({
                    selectFeature: setSelectedFeature,
                    clearSelection: () => clearSelectedFeature(null),
                    getSelected: getSelectedFeature,
                }),
                getInteraction
            );

            clickable({ setPosition }, getInteraction);

            follow(name, updateMapView(bottomMapName));

            registerTarget(name, setTarget);

            markable({ startMark, endMark }, getInteraction);

            printable(
                {
                    getRequest: getPrintRequest,
                    setResponse: setPrintResponse,
                },
                getInteraction
            );

            measurable(
                {
                    updateMeasureCoordinates,
                    stopMeasuring: stopMeasure,
                },
                getInteraction
            );

            editable(
                {
                    getCurrentLayerId: () => SLICE_LAYER_NAME,
                    getGeometryType: () => 'LineString',
                    addFeature: () => void 0,
                    setGeometry: makeSlice,
                    getStyle: sliceInteractionStyle,
                },
                getInteraction
            );

            andThen(m => {
                const v = m.getView();
                v.setMinZoom(MIN_ZOOM);
                v.setMaxZoom(MAX_ZOOM);
            });

            return update;
        });
    } else if (name === bottomMapName) {
        mapInsert(name, () => {
            const { update, setTarget, andThen } = create(name, {
                ...options(name),
                element,
            });

            registerTarget(name, setTarget);

            andThen(m => {
                const v = m.getView();
                v.setMinZoom(MIN_ZOOM);
                v.setMaxZoom(MAX_ZOOM);
            });

            return update;
        });
    }

    mapSetTarget(name, element);
};

const renderLoading = (ms: Readonly<MessageRecord[]>) => {
    console.log(`<< renderLoading ${ms.length}`);
    return DIV(
        {
            className: `loading-layer-wrapper ${
                ms.length === 0 ? 'hidden' : ''
            }`,
        },
        H3({}, tr.geo('loadingData')),
        ms.map(r =>
            DIV(
                {
                    className: 'loading-layer',
                    key: fromRecord(r),
                },
                SPAN({ className: 'loader-spinner' }),
                fromRecord(r)
            )
        )
    );
};

const renderMap = (name: MapName) => () => {
    mapUpdate(name);
    return DIV(
        {
            className: 'map-wrapper unfocused',
            'aria-hidden': true,
        },
        DIV({
            id: name,
            key: name,
            className: `map interaction-${getInteraction().label}`,
            ref: attachMap(name),
        }),
        renderLoading(getLoading()),
        renderScaleline()
    );
};

export const renderTopMap = renderMap(topMapName);
export const renderBottomMap = renderMap(bottomMapName);
