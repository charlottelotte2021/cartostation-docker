import { getScaleLine } from 'brugeotool/src/queries/map';
import { DIV } from 'sdi/components/elements';

export const renderScaleline = () => {
    const sl = getScaleLine();
    return DIV(
        { className: 'map-scale' },
        DIV({ className: 'map-scale-label' }, `${sl.count} ${sl.unit}`),
        DIV(
            { className: 'map-scale-line', style: { width: `${sl.width}px` } },
            DIV({ className: 'quarter' }),
            DIV({ className: 'quarter' }),
            DIV({ className: 'half' })
        )
    );
};
