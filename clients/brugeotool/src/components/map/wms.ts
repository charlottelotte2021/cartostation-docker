import { renderSelect } from 'sdi/components/input';
import { translateMapBaseLayer } from 'sdi/util';

import { setBaseLayer } from 'brugeotool/src/events/map';
import {
    getCurrentBaseLayer,
    getBaseLayerNameList,
    findBaseLayer,
    topMapName,
} from 'brugeotool/src/queries/map';
import { setoidString } from 'fp-ts/lib/Setoid';

const renderBaseLayerName = (blName: string) =>
    findBaseLayer(blName).fold('', bl => translateMapBaseLayer(bl).name);

const renderItem = (name: string) => renderBaseLayerName(name);

// const render = renderSelect(
//     setoidString,
//     renderItem,
//     setBaseLayer(topMapName),
// );
const render = renderSelect(
    'geo-map-top-select',
    renderItem,
    setBaseLayer(topMapName),
    setoidString
);

export const renderSelectWMS = () =>
    render(getBaseLayerNameList(), getCurrentBaseLayer(topMapName));
