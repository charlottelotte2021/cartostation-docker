import * as debug from 'debug';
import {
    DepthTile,
    DepthRow,
    DepthPointThickness,
    DepthArray,
} from 'brugeotool/src/remote/io';
import {
    getCakeMode,
    getCakeRes,
    getArchiExtent,
    getCakeQuatAlpha,
    getDepthScaling,
} from 'brugeotool/src/queries/geothermie';
import { STRATIGRAPHY_COLORS } from '../settings';

import { Option } from 'fp-ts/lib/Option';
import { getPositionMarker } from 'brugeotool/src/queries/map';
import { Extent } from 'ol/extent';
import { Coordinate } from 'ol/coordinate';
import { CakeAlpha, cakeInZoomRange, CakeMode, dec2hex, hex2dec } from '.';
import { getAdjustedExtent } from 'brugeotool/src/events/geothermie';

const logger = debug('sdi:cake/engine');

const colors = STRATIGRAPHY_COLORS.slice().reverse();

export const VERTICAL_RATIO = 1.8;
const TRIANGLE_HEIGHT_RATIO = 1 / 4;

export function tileBBox(tile: DepthTile) {
    let minx = Number.MAX_VALUE;
    let miny = Number.MAX_VALUE;
    let minz = Number.MAX_VALUE;
    let maxx = 0;
    let maxy = 0;
    let maxz = 0;

    tile.data.forEach(row =>
        row.forEach(p => {
            minx = Math.min(minx, p.x);
            maxx = Math.max(maxx, p.x);
            miny = Math.min(miny, p.y);
            maxy = Math.max(maxy, p.y);
            minz = Math.min(minz, p.thickness[0]);
            maxz = Math.max(maxz, p.thickness[0]);
        })
    );
    return [minx, miny, minz, maxx + tile.size, maxy + tile.size, maxz];
}

function even(n: number) {
    return n % 2 === 0;
}

function getStep(n: number) {
    return Math.ceil(n / 2);
}

const TAG_ERROR = 0x1;
const TAG_NODATA = 0x2;
const TAG_OUTSIDE = 0x4;
const TAG_INSIDE = 0x8;
const TAG_BORDER = 0x10;

const { tagTile, getTag } = (() => {
    let data: DepthArray | null = null;
    let width = 0;
    let height = 0;

    const tagTile = (sizeH: number, sizeV: number, tile: DepthTile) => {
        data = tile.data;
        width = sizeH;
        height = sizeV;
    };

    const isOut = (x: number, y: number, mode: CakeMode) => {
        const notEnough = () => x < getStep(y);
        const tooMuch = () => x > width - getStep(y - 1);

        switch (mode) {
            case 'uncut':
                return x < 0 || x >= width || y < 0 || y >= height;
            case 'left-cut':
                return notEnough();
            case 'right-cut':
                return tooMuch();
            case 'double-cut':
                return notEnough() || tooMuch();
        }
    };

    const noData = (x: number, y: number) =>
        data !== null &&
        x >= 0 &&
        x < width &&
        y >= 0 &&
        y < height &&
        data[y][x].nodata;

    const testNeighbours = (x: number, y: number, mode: CakeMode) => {
        const indices = [
            [x - 1, y],
            [x - 1, y + 1],
            [x, y + 1],
            [x + 1, y + 1],
            [x + 1, y],
        ];

        for (let i = 0; i < indices.length; i += 1) {
            const [tx, ty] = indices[i];
            if (noData(tx, ty) || isOut(tx, ty, mode)) {
                return true;
            }
        }
        return false;
    };

    const getTag = (x: number, y: number) => {
        if (data === null || x < 0 || x >= width || y < 0 || y >= height) {
            return TAG_ERROR;
        }

        if (noData(x, y)) {
            return TAG_NODATA;
        } else if (isOut(x, y, getCakeMode())) {
            return TAG_OUTSIDE;
        } else if (testNeighbours(x, y, getCakeMode())) {
            return TAG_INSIDE | TAG_BORDER;
        }
        return TAG_INSIDE;
    };

    return { tagTile, getTag };
})();

const withContext = (ctx: CanvasRenderingContext2D) => {
    function drawPiece(
        x: number,
        y: number,
        height: number,
        blockWidth: number,
        isLastBand: boolean,
        isTopPiece: boolean,
        quatAlpha: CakeAlpha
    ) {
        const halfWidth = blockWidth / 2;
        const triangleHeight = blockWidth * TRIANGLE_HEIGHT_RATIO;

        if (isLastBand) {
            // base
            ctx.beginPath();

            ctx.moveTo(x, y + triangleHeight); // 0
            ctx.lineTo(x + halfWidth, y + triangleHeight); // 1
            ctx.lineTo(x + halfWidth, y + triangleHeight + height); // 2
            if (isTopPiece) {
                ctx.lineTo(x, y + triangleHeight * 2 + height); // 3
            }
            ctx.lineTo(x - halfWidth, y + triangleHeight + height);
            ctx.lineTo(x - halfWidth, y + triangleHeight);
            ctx.closePath();
            ctx.fill();

            // front shadow
            ctx.fillStyle = '#00000022';
            if (isTopPiece) {
                ctx.fillStyle = `#000000${dec2hex(hex2dec(quatAlpha) * 0.15)}`;
            }
            ctx.beginPath();
            ctx.moveTo(x - halfWidth, y); // 0
            ctx.lineTo(x + halfWidth, y); // 1
            ctx.lineTo(x + halfWidth, y + height); // 2
            ctx.lineTo(x - halfWidth, y + height);
            ctx.closePath();
            ctx.fill();
        } else {
            // base shape
            ctx.beginPath();
            ctx.moveTo(x, y); // 0
            ctx.lineTo(x + halfWidth, y + triangleHeight); // 1
            ctx.lineTo(x + halfWidth, y + triangleHeight + height); // 2
            ctx.lineTo(x, y + triangleHeight * 2 + height); // 3
            ctx.lineTo(x - halfWidth, y + triangleHeight + height);
            ctx.lineTo(x - halfWidth, y + triangleHeight);
            ctx.closePath();
            ctx.fill();

            // right shadow
            ctx.fillStyle = '#00000033';
            if (isTopPiece) {
                ctx.fillStyle =
                    quatAlpha === 'FC'
                        ? `#000000${dec2hex(hex2dec(quatAlpha) * 0.25)}`
                        : '#00000000';
            }
            ctx.beginPath();
            ctx.moveTo(x, y); // 0
            ctx.lineTo(x, y + height); // ?
            ctx.lineTo(x + halfWidth, y + triangleHeight + height); // 2
            ctx.lineTo(x + halfWidth, y + triangleHeight); // 1
            ctx.closePath();
            ctx.fill();

            // left shadow
            ctx.fillStyle = '#00000015';
            if (isTopPiece) {
                ctx.fillStyle =
                    quatAlpha === 'FC'
                        ? `#000000${dec2hex(hex2dec(quatAlpha) * 0.15)}`
                        : '#00000000';
            }
            ctx.beginPath();
            ctx.moveTo(x, y); // 0
            ctx.lineTo(x, y + height); // ?
            ctx.lineTo(x - halfWidth, y + triangleHeight + height); // 2
            ctx.lineTo(x - halfWidth, y + triangleHeight); // 1
            ctx.closePath();
            ctx.fill();
        }
    }

    function drawSelection(x: number, y: number, blockWidth: number) {
        // const halfWidth = blockWidth / 2;
        const triangleHeight = blockWidth * TRIANGLE_HEIGHT_RATIO;
        const radius = blockWidth / 2;
        const height = 12;
        const strokeWidth = blockWidth / 12;

        ctx.save();
        ctx.lineWidth = strokeWidth;
        ctx.lineCap = 'round';
        ctx.strokeStyle = 'black';
        ctx.fillStyle = 'black';
        ctx.beginPath();
        ctx.moveTo(x, y + height * triangleHeight); // 0
        ctx.lineTo(x, y);
        ctx.closePath();
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(x, y + height * triangleHeight, radius, 0, 2 * Math.PI);
        ctx.fill();
        ctx.restore();
    }

    function isQuaternaire(l: number) {
        return l === 18;
    }

    function offset(bandOffset: number, blockWidth: number) {
        // return (bandOffset * ((blockWidth) / 2));
        const th = blockWidth * TRIANGLE_HEIGHT_RATIO;
        const conv = blockWidth - th;
        return bandOffset * conv;
    }

    function realClose(a: number, b: number, threshold: number) {
        return Math.abs(a - b) < threshold;
    }

    type BlockCH = [number /* y */, string /* color */, number /* height */];

    function drawBlock(
        block: DepthPointThickness,
        blockWidth: number,
        scale: number,
        bandOffset: number,
        blockOffset: number,
        selected: Option<Coordinate>,
        isLastBand: boolean
    ) {
        const tag = getTag(blockOffset, bandOffset);
        // const vert = (v: number) => v; // (v * VERTICAL_RATIO) / scale;
        const vert = (v: number) => v / scale;
        const adaptVerticalScale = 0; // canvasRatio > 2 ? vert((20 * canvasRatio) ^ 2) : 0;
        if ((tag & TAG_INSIDE) !== 0) {
            const elev = vert(block.thickness[0]);
            const values = block.thickness.slice(1).reverse();
            const ground = elev - adaptVerticalScale; // elev - vert(100); // 100 as in "altitude 100"
            values[0] = values[0] * 100;
            let currentDepth =
                ground - values.reduce((acc, v) => acc + vert(v), 0);

            const xOffset = even(bandOffset) ? blockWidth / 2 : 0;
            const yOffset = offset(bandOffset, blockWidth);

            const x = block.x + xOffset;
            const baseY = block.y + yOffset;

            const visibles = values
                .map<BlockCH>((v, i) => {
                    const y = baseY + currentDepth;
                    /* quaternaire is the 18th layer */
                    const color = isQuaternaire(i)
                        ? `${colors[i]}${getCakeQuatAlpha()}`
                        : colors[i];
                    const height = vert(v);
                    currentDepth += height;
                    return [y, color, height];
                })
                .filter(([_y, _c, h]) => h > 0);

            if (isLastBand || !!(tag & TAG_BORDER)) {
                visibles.forEach(([y, color, height], i) => {
                    ctx.fillStyle = color;
                    drawPiece(
                        x,
                        y,
                        height,
                        blockWidth,
                        isLastBand,
                        i + 1 === visibles.length,
                        getCakeQuatAlpha()
                    );
                });
            } else {
                for (
                    let i = Math.max(0, visibles.length - 4);
                    i < visibles.length;
                    i += 1
                ) {
                    const [y, color, height] = visibles[i];
                    ctx.fillStyle = color;
                    drawPiece(
                        x,
                        y,
                        height,
                        blockWidth,
                        false,
                        i + 1 === visibles.length,
                        getCakeQuatAlpha()
                    );
                }
            }

            selected.map(([sx, sy]) => {
                if (
                    realClose(sx, block.x, blockWidth / 2) &&
                    realClose(sy, block.y, blockWidth / 2)
                ) {
                    drawSelection(x, baseY + currentDepth, blockWidth);
                }
            });
        }
        // else if (!!(tag & TAG_NODATA)) {
        //     logger(`drawBlock TAG_NODATA`);
        //     // drawSelection(
        //     //     block.x + (even(bandOffset) ? blockWidth / 2 : 0),
        //     //     block.y + offset(bandOffset, blockWidth, scale) + vert(block.thickness[0]),
        //     //     blockWidth);
        // }
        // else if (!!(tag & TAG_OUTSIDE)) {
        //     logger(`drawBlock TAG_OUTSIDE`);
        //     // drawSelection(
        //     //     block.x + (even(bandOffset) ? blockWidth / 2 : 0),
        //     //     block.y + offset(bandOffset, blockWidth, scale) + vert(block.thickness[0]),
        //     //     blockWidth);
        // }
    }

    function drawBand(
        band: DepthRow,
        blockWidth: number,
        scale: number,
        bandOffset: number,
        baseExtent: Extent,
        selected: Option<Coordinate>
    ) {
        if (band.length > 0) {
            const extent = getAdjustedExtent(baseExtent);
            const t = band[0];
            const ty = t.y;
            const borderBottom = extent[1];
            const borderTop = extent[3];
            const isInside = ty <= borderTop && ty >= borderBottom;
            const isLastBand =
                ty >= borderBottom && ty - blockWidth < borderBottom;
            // logger(`---------------------------------`);
            // logger(`drawBand len: ${band.length}`);
            // logger(`drawBand ty: ${ty}`);
            // logger(`drawBand borderBottom: ${borderBottom}`);
            // logger(`drawBand borderTop: ${borderTop}`);
            // logger(`drawBand isInside: ${isInside}`);
            // logger(`drawBand isLast: ${isLastBand}`);
            if (isInside) {
                band.forEach((block, i) => {
                    drawBlock(
                        block,
                        blockWidth,
                        scale,
                        bandOffset,
                        i,
                        selected,
                        isLastBand
                    );
                });
            }
        }
    }

    // function offsetX([minx, miny, maxx, maxy]: Extent, canvasWidth: number) {
    //     const extentWidth = maxx - minx;
    //     const extentHeight = maxy - miny;
    //     let width = extentWidth;
    //     let height = width * 0.66;
    //     if (height > extentHeight) {
    //         height = extentHeight * 0.9;
    //         width = height / 0.66;
    //     }
    //     const ox = (extentWidth - width) / 2;
    //     return (ox * canvasWidth) / extentWidth;
    // }

    function draw(
        width: number,
        height: number,
        tile: DepthTile,
        [x0, y0, x1, y1]: Extent,
        selected: Option<Coordinate>
    ) {
        ctx.save();
        ctx.clearRect(0, 0, width, height);

        const len = getCakeRes(); // firstRow.length;
        tagTile(len, tile.data.length, tile);
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const [minx, _miny, _minz, maxx, maxy, maxz] = tileBBox(tile);
        // const margin = offsetX([x0, y0, x1, y1], width);
        const margin = width * 0.1;
        const topMargin = margin * (height / width);
        const scaleRatio = (width - margin * 2) / (maxx - minx);
        const zScaling = scaleRatio / getDepthScaling() / VERTICAL_RATIO;
        const base = new DOMMatrix();
        const ty = maxy + (maxz / zScaling + topMargin / scaleRatio);
        const tx = minx - margin / scaleRatio;
        const vertInverter = base.scale(1, -1);
        const matrix = base.scale(scaleRatio, scaleRatio).translate(-tx, -ty);

        const { a, b, c, d, e, f } = vertInverter.multiply(matrix);
        ctx.setTransform(a, b, c, d, e, f);
        ctx.lineWidth = 0.1 / scaleRatio;
        tile.data.forEach((row, i) => {
            drawBand(row, tile.size, zScaling, i, [x0, y0, x1, y1], selected);
        });

        // iife(() => {
        //     const sw = width / scaleRatio;
        //     const sh = height / scaleRatio;
        //     ctx.save();
        //     ctx.beginPath();
        //     ctx.strokeStyle = 'black';
        //     ctx.moveTo(minx + sw / 2, miny);
        //     ctx.lineTo(minx + sw / 2, maxy);
        //     ctx.moveTo(minx, miny + sh / 2);
        //     ctx.lineTo(maxx, miny + sh / 2);
        //     ctx.stroke();
        //     ctx.restore();
        // });
        // iife(() => {
        //     ctx.save();
        //     ctx.beginPath();
        //     ctx.strokeStyle = 'green';
        //     ctx.rect(
        //         minx,
        //         maxy - len * (bboxHeight / len) * TRIANGLE_HEIGHT_RATIO,
        //         maxx - minx,
        //         len * (bboxHeight / len) * TRIANGLE_HEIGHT_RATIO
        //     );
        //     ctx.stroke();
        // });

        // iife(() => {
        //     ctx.beginPath();
        //     ctx.fillStyle = 'blue';
        //     ctx.strokeStyle = 'blue';
        //     const start = miny - 5000;
        //     const end = maxy + 5000;
        //     for (let y = start; y < end; y += 200) {
        //         ctx.moveTo(minx, y);
        //         ctx.lineTo(minx + 40 / scaleRatio, y);
        //     }
        //     ctx.stroke();
        //     ctx.font = `${10 / scaleRatio}px monospace`;
        //     for (let y = start; y < end; y += 200) {
        //         ctx.fillText(y.toString(10), minx + 50 / scaleRatio, y);
        //     }
        // });

        ctx.restore();
    }

    // const imageData = (width: number, height: number, tile: DepthTile) => {
    //     draw(width, height, tile, none);
    //     return ctx.getImageData(0, 0, width, height);
    // };

    return { draw };
};

export const engine = () => {
    // const canvas = document.createElement('canvas');
    // const ctx = canvas.getContext('2d')!;

    // const getImage = (width: number, height: number, data: DepthTile) => {
    //     canvas.width = width;
    //     canvas.height = height;

    //     withContext(ctx).draw(width, height, data, none);

    //     return canvas.toDataURL();
    // };

    const onCanvas = (canvasRef: HTMLCanvasElement) => {
        const ctxRef = canvasRef.getContext('2d')!;

        let it: number | null = null;
        let startTime = Date.now();
        const loaderFrameRate = 32;

        const drawLoader = (
            x: number,
            y: number,
            width: number,
            height: number
        ) => {
            logger(`drawLoader ${width}`);
            ctxRef.save();
            ctxRef.resetTransform();
            ctxRef.fillStyle = '#8db63c';
            ctxRef.fillRect(x, y, width, height);
            ctxRef.restore();
        };

        const update = (data: DepthTile) => {
            if (it) {
                window.clearInterval(it);
                it = null;
            }

            getArchiExtent().map(extent => {
                const { width, height } = canvasRef.getBoundingClientRect();
                const { draw } = withContext(ctxRef);
                const selected = getPositionMarker();

                canvasRef.width = width;
                canvasRef.height = height;
                draw(width, height, data, extent, selected);
            });
        };

        const load = () => {
            if (it === null && cakeInZoomRange()) {
                const { width, height } = canvasRef.getBoundingClientRect();
                canvasRef.width = width;
                canvasRef.height = height;
                startTime = Date.now();
                it = window.setInterval(() => {
                    const max =
                        ((Date.now() - startTime) / loaderFrameRate) * 1.9 + 50; // this 50 is horizontal scale (hardcoded in style)
                    logger(`max: ${max}; width: ${width}; height: ${height}`);
                    if (max < width) {
                        drawLoader(0, height - 6, max, 6);
                    } else {
                        window.clearInterval(it!);
                        it = null;
                    }
                }, loaderFrameRate);
            }
        };

        return { update, load };
    };

    return { onCanvas };
};

logger('loaded');
