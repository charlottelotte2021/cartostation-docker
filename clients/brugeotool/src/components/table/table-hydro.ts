import { DIV, TABLE, TBODY, THEAD, TH, TR, TD } from 'sdi/components/elements';
import tr from 'sdi/locale';

import {
    getHydroLayersThicknesses,
    getHydroSystemThicknesses,
    // getDiagramHeight,
    getKMin,
    getKMean,
    getKMax,
} from '../../queries/geothermie';
import {
    addSuffixToMessageKey,
    HYDROGEOLOGY_UNITS_NAME,
    HYDROLOGICAL_SYSTEM_NAME,
    UH_GREY_SHADE,
} from '../settings';

import {
    renderNoTableLine,
    // computeLineHeight,
    keyAndTrad,
    computeSmartOpenLineHeight,
} from './format';

const getUH = () => HYDROGEOLOGY_UNITS_NAME.map(u => keyAndTrad(u));

const getUHClassName = () => UH_GREY_SHADE;

const getHydroState = () =>
    addSuffixToMessageKey(HYDROGEOLOGY_UNITS_NAME, '-hydro_state').map(u =>
        tr.geo(u)
    );

const getHydroSystem = () => HYDROLOGICAL_SYSTEM_NAME.map(u => tr.geo(u));

// FIXTURES
// const getLayersThicknesses = () => [9.331809997558594, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.7692070007324219, 3.0461788177490234, 27.77418327331543, 45.48012161254883, 13.581069946289062, 19.563568115234375, 11.043914794921875] // for debug when no connection
// const getLayersSat = () => [0.3, 0, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.8, 1, 1, 1, 1, 1, 1, 1]; // (fixture) get real data from x,y
// const getQuaternaireThicknesses = () => [0.2, 0.3, 0.4, 0.1]; // (fixture)

// Render of tables

const renderHydroTableLine = (l: number, index: number) =>
    l === 0
        ? renderNoTableLine()
        : TR(
              {
                  className: '',
                  style: {
                      // height: computeLineHeight(l) + '%'
                      height: `${computeSmartOpenLineHeight(index)}px`,
                  },
                  key: index,
              },
              TD(
                  { className: `uh ${getUHClassName()[index]}` },
                  getUH()[index]
              ),
              TD({}, getKMin()[index]),
              TD({}, getKMean()[index]),
              TD({}, getKMax()[index]),
              TD({}, getHydroState()[index])
          );

export const renderHydroTable = () =>
    DIV(
        { className: 'table--hydro' },
        DIV(
            {},
            TABLE(
                {
                    style: {
                        // height: getDiagramHeight()
                    },
                },
                THEAD(
                    {},
                    TR(
                        {},
                        TH({}, tr.geo('openTableColUH')),
                        TH({}, tr.geo('openTableColKmin')),
                        TH({}, tr.geo('openTableColKmean')),
                        TH({}, tr.geo('openTableColKmax')),
                        TH({}, tr.geo('openTableColState'))
                    )
                ),
                TBODY(
                    {},
                    ...getHydroLayersThicknesses().map((l, i) =>
                        renderHydroTableLine(l, i)
                    )
                )
            )
        ),
        renderHydroSystemTable()
    );

const renderHydroSystemTableLine = (l: number, index: number) =>
    l === 0
        ? renderNoTableLine()
        : TR(
              {
                  style: {
                      // height: computeLineHeight(l) + '%'
                  },
                  key: index,
              },
              TD({}, DIV({}, getHydroSystem()[index]))
          );

const renderHydroSystemTable = () =>
    DIV(
        { className: 'table--hydro--system' },
        TABLE(
            {
                style: {
                    // height: getDiagramHeight()
                },
            },
            THEAD({}, TR({}, TH({}, tr.geo('systemCol')))),
            TBODY(
                {},
                ...getHydroSystemThicknesses().map((l, i) =>
                    renderHydroSystemTableLine(l, i)
                )
            )
        )
    );
