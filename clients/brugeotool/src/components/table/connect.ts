/**
 * Connect create a component that links 2 sets of offsets
 */
import * as debug from 'debug';
import { getConnect } from 'brugeotool/src/queries/geothermie';
import { DIV, CANVAS } from 'sdi/components/elements';
import { updateConnect } from 'brugeotool/src/events/geothermie';
import { scopeOption } from 'sdi/lib';
import { rect } from 'sdi/app';
import { fromNullable } from 'fp-ts/lib/Option';
import { Setoid, getArraySetoid, setoidNumber } from 'fp-ts/lib/Setoid';

const logger = debug('sdi:connect');

export type ConnectName = 'carrot' | 'depth' | 'conductivity';
// | 'close'
// | 'comment'
// | 'open'
// | 'geology'
// | 'hydrology'

export interface ConnectData {
    offset: number;
    heights: number[];
}

export const setoidConectData: Setoid<ConnectData> = {
    equals: (a, b) =>
        a.offset === b.offset &&
        getArraySetoid(setoidNumber).equals(a.heights, b.heights),
};

export const defaultConnect = (): ConnectData => ({
    offset: 99,
    heights: [],
});

export const rectifyConnect = (
    name: ConnectName,
    f: (d: ConnectData) => ConnectData
) => updateConnect(name, f);

export const rectifySizes = (name: ConnectName, sizes: number[]) =>
    rectifyConnect(name, data => ({ ...data, sizes }));

const zip = <A, B>(left: A[], right: B[]) => {
    const len = Math.min(left.length, right.length);
    // tslint:disable-next-line: prefer-array-literal
    const result: [A, B][] = new Array(len);
    for (let i = 0; i < len; i += 1) {
        result[i] = [left[i], right[i]];
    }
    return result;
};

export const line = (
    ctx: CanvasRenderingContext2D,
    x1: number,
    y1: number,
    x2: number,
    y2: number
) => {
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.bezierCurveTo(
        x1 + (x2 - x1) * 0.5,
        y1, // control point 1
        x1 + (x2 - x1) * 0.5,
        y2, // control point 2
        x2,
        y2 // end point
    );
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 0.8;
    ctx.stroke();
};

export const accumulate = (ns: number[], init = 0) =>
    ns.reduce(
        (acc, n) => {
            const last = acc[acc.length - 1];
            return acc.concat(last + n);
        },
        [init]
    );

const renderConnectData = (
    ctx: CanvasRenderingContext2D,
    left: ConnectData,
    right: ConnectData,
    width: number
) => {
    if (left.heights.length === 0 || right.heights.length === 0) {
        return;
    }
    ctx.lineWidth = 0.5;
    // ctx.strokeStyle = '#'
    const accLeft = accumulate(left.heights, left.offset);
    const accRight = accumulate(right.heights, right.offset);
    // const maxLeft = accLeft[accLeft.length - 1];
    // const maxRight = accRight[accRight.length - 1];
    // const scaleLeft = (y: number) => y * height / maxLeft;
    // const scaleRight = (y: number) => y * height / maxRight;

    const zipped = zip(accLeft, accRight);
    const previousPair = [0, 0];

    zipped
        .slice(0, zipped.length - 1) // Il y a aussi un connecteur qui semble persister tout en bas de la colonne (il pointe sur la base de la colonne, en système fermé, ouvert et synthèse) : serait-il possible de le faire disparaitre ?
        .map(([left, right]) => {
            if (left - previousPair[0] > 1 && right - previousPair[1] > 1) {
                line(ctx, 0, left, width, right);
            }
            previousPair[0] = left;
            previousPair[1] = right;
        });
};

const sum = (ns: number[]) => ns.reduce((acc, n) => acc + n, 0);

const onMount = (
    left: ConnectName,
    right: ConnectName,
    canvas: HTMLCanvasElement
) => {
    const withData = (f: (left: ConnectData, right: ConnectData) => void) =>
        scopeOption()
            .let('left', getConnect(left))
            .let('right', getConnect(right))
            .map(({ left, right }) => f(left, right));

    const updateSize = () => {
        let { width, height } = canvas.getBoundingClientRect();
        if (width > height) {
            const tmp = height;
            height = width;
            width = tmp;
        }

        withData((left, right) => {
            const neededHeight = Math.max(
                sum(left.heights) + left.offset,
                sum(right.heights) + right.offset
            );
            if (neededHeight > height) {
                height = neededHeight;
            }
        });
        canvas.width = width;
        canvas.height = height;
        return { width, height };
    };

    const draw = () => {
        const ctx = canvas.getContext('2d');
        if (ctx) {
            const { width } = updateSize();
            withData((left, right) =>
                renderConnectData(ctx, left, right, width)
            );
        }
    };

    rect(r => {
        let { width, height } = canvas.getBoundingClientRect();
        if (
            Math.floor(width) !== Math.floor(r.width) &&
            Math.floor(height) !== Math.floor(r.height)
        ) {
            // width = r.width;
            // height = r.height;
            // if (width > height) {
            //     const tmp = height;
            //     height = width;
            //     width = tmp;
            // }
            // canvas.width = width;
            // canvas.height = height;
            draw();
        }
    })(canvas);

    draw();
};

const onUnMount = (_left: ConnectName, _right: ConnectName) => {
    // TODO
};

const ref =
    (left: ConnectName, right: ConnectName) =>
    (canvas: HTMLCanvasElement | null) =>
        fromNullable(canvas).foldL(
            () => onUnMount(left, right),
            canvas => onMount(left, right, canvas)
        );

export const renderConnect = (left: ConnectName, right: ConnectName) =>
    DIV(
        {
            className: `connect ${left}-${right}`,
        },
        CANVAS({
            key: `connect-${left}-${right}`,
            ref: ref(left, right),
            style: {
                width: '100%',
                // height: '100%'
            },
        })
    );

export default renderConnect;

logger('loaded');
