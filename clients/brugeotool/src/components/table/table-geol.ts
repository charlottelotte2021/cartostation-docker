import {
    DIV,
    TABLE,
    TBODY,
    THEAD,
    TH,
    TR,
    TD,
    NODISPLAY,
} from 'sdi/components/elements';
import tr from 'sdi/locale';

import {
    getLayersThicknesses,
    getQuaternaireThicknesses,
    getGeologicalEtageThicknesses,
    getGeologicalSerieThicknesses,
    getGeologicalEraThicknesses,
    getGeologicalSystemThicknesses,
    getShowPartTableGeol,
    getDiagramHeight,
} from '../../queries/geothermie';
import {
    addSuffixToMessageKey,
    STRATIGRAPHY_UNITS_NAME,
    Q_STRATIGRAPHY_UNITS_NAME,
    STRATIGRAPHY_COLORS,
    GEOLOGICAL_ETAGE_LAYERS_NAME,
    GEOLOGICAL_SERIE_LAYERS_NAME,
    GEOLOGICAL_ERA_LAYERS_NAME,
    GEOLOGICAL_SYSTEM_LAYERS_NAME,
} from '../settings';

import {
    renderNoTableLine,
    //computeLineHeight,
    // computeQLineHeight,
    getLayersDepths,
    keyAndTrad,
    renderMoreText,
    formatDepth,
    computeSmartGeolLineHeight,
    computeSmartEtageLineHeight,
    computeSmartSerieLineHeight,
    computeSmartEraLineHeight,
    computeSmartSystemLineHeight,
    MIN_LINE_HEIGHT,
} from './format';
import { renderDepthTitle, renderDepthCol } from './table-depth';
import { changeShowPartTableGeol } from 'brugeotool/src/events/geothermie';
import { makeIcon } from 'sdi/components/button';

const getUS = () => STRATIGRAPHY_UNITS_NAME.map(u => keyAndTrad(u));

const getQuaternaireName = () =>
    Q_STRATIGRAPHY_UNITS_NAME.map(u => keyAndTrad(u));

const getDetailedLithology = () =>
    addSuffixToMessageKey(STRATIGRAPHY_UNITS_NAME, '-litho').map(u =>
        tr.geo(u)
    );

const getDetailedLithologyQuaternaire = () =>
    addSuffixToMessageKey(Q_STRATIGRAPHY_UNITS_NAME, '-litho').map(u =>
        tr.geo(u)
    );

const getGeologicalEra = () => GEOLOGICAL_ERA_LAYERS_NAME.map(u => tr.geo(u));

const getGeologicalSystem = () =>
    GEOLOGICAL_SYSTEM_LAYERS_NAME.map(u => tr.geo(u));

const getGeologicalEtage = () =>
    GEOLOGICAL_ETAGE_LAYERS_NAME.map(u => tr.geo(u));

const getGeologicalSerie = () =>
    GEOLOGICAL_SERIE_LAYERS_NAME.map(u => tr.geo(u));

// FIXTURES
// const getLayersThicknesses = () => [9.331809997558594, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.7692070007324219, 3.0461788177490234, 27.77418327331543, 45.48012161254883, 13.581069946289062, 19.563568115234375, 11.043914794921875] // for debug when no connection
// const getLayersSat = () => [0.3, 0, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.8, 1, 1, 1, 1, 1, 1, 1]; // (fixture) get real data from x,y
// const getQuaternaireThicknesses = () => [0.2, 0.3, 0.4, 0.1]; // (fixture)

// Render of tables

const renderGeoTableLine = (l: number, index: number) =>
    l === 0
        ? renderNoTableLine()
        : index === 0
        ? getQuaternaireThicknesses().map((ql, qindex) =>
              ql === 0
                  ? renderNoTableLine()
                  : TR(
                        {
                            className: '',
                            style: {
                                // height: `${(computeLineHeight(l) *
                                //     computeQLineHeight(ql)) /
                                //     100}%`
                                height: `${MIN_LINE_HEIGHT}px`,
                            },
                            key: `quaternaire-${qindex}`,
                        },
                        TD(
                            {},
                            renderMoreText(
                                getDetailedLithologyQuaternaire()[qindex],
                                'detailledLithology',
                                qindex,
                                30
                            )
                        ),
                        TD(
                            {
                                className: 'large',
                                style: {
                                    background: STRATIGRAPHY_COLORS[index],
                                },
                            },
                            getQuaternaireName()[qindex]
                        ),
                        TD(
                            { className: 'depth' },
                            qindex === getQuaternaireThicknesses().length - 1
                                ? formatDepth(getLayersDepths()[0])
                                : ''
                        )
                    )
          )
        : TR(
              {
                  className: '',
                  //   style: { height: computeLineHeight(l) + '%' },
                  style: {
                      height: `${computeSmartGeolLineHeight(index)}px`,
                  },
                  key: index,
              },
              TD(
                  {},
                  renderMoreText(
                      getDetailedLithology()[index],
                      'detailledLithology',
                      index + getQuaternaireThicknesses().length,
                      30
                  )
              ),
              TD(
                  {
                      className: 'large',
                      style: { background: STRATIGRAPHY_COLORS[index] },
                  },
                  getUS()[index]
              ),
              renderDepthCol(getLayersDepths(), index)
          );

export const renderGeoTable = () =>
    getLayersThicknesses().fold(NODISPLAY(), thicknesses =>
        DIV(
            { className: 'table--geol' },
            renderShowLeftButton(),
            renderGeoEraTable(),
            renderGeoSystemTable(),
            renderGeoSerieTable(),
            renderGeoEtageTable(),
            DIV(
                {},
                TABLE(
                    {
                        // style: { height: getDiagramHeight() }
                    },
                    THEAD(
                        {},
                        TR(
                            {},
                            TH({}, tr.geo('lithoCol')),
                            TH({}, tr.geo('closeTableColUS')),
                            renderDepthTitle()
                        )
                    ),
                    TBODY(
                        {},
                        ...thicknesses.map((l, i) => renderGeoTableLine(l, i))
                    )
                )
            )
        )
    );

const renderShowLeftButton = () =>
    DIV(
        {},
        getShowPartTableGeol()
            ? makeIcon('toggle-on', 2, 'chevron-left', {
                  position: 'left',
                  text: tr.core('expand'),
              })(changeShowPartTableGeol) // TODO: check position & label
            : makeIcon('toggle-off', 2, 'chevron-right', {
                  position: 'left',
                  text: tr.core('collapse'),
              })(changeShowPartTableGeol)
    );

const renderGeoEtageTableLine = (l: number, index: number) =>
    l === 0
        ? renderNoTableLine()
        : TR(
              {
                  className: '',
                  //   style: { height: computeLineHeight(l) + '%' },
                  style: { height: `${computeSmartEtageLineHeight(index)}px` },
                  key: index,
              },
              TD({}, getGeologicalEtage()[index])
          );

const renderGeoEtageTable = () =>
    getShowPartTableGeol()
        ? NODISPLAY()
        : DIV(
              {},
              TABLE(
                  {
                      style: { height: getDiagramHeight() },
                  },
                  THEAD({}, TR({}, TH({}, tr.geo('etageCol')))),
                  TBODY(
                      {},
                      ...getGeologicalEtageThicknesses().map((l, i) =>
                          renderGeoEtageTableLine(l, i)
                      )
                  )
              )
          );

const renderGeoSerieTableLine = (l: number, index: number) =>
    l === 0
        ? renderNoTableLine()
        : TR(
              {
                  className: '',
                  //   style: { height: computeLineHeight(l) + '%' },
                  style: { height: `${computeSmartSerieLineHeight(index)}px` },
                  key: index,
              },
              TD({}, getGeologicalSerie()[index])
          );

const renderGeoSerieTable = () =>
    getShowPartTableGeol()
        ? NODISPLAY()
        : DIV(
              {},
              TABLE(
                  {
                      style: { height: getDiagramHeight() },
                  },
                  THEAD({}, TR({}, TH({}, tr.geo('serieCol')))),
                  TBODY(
                      {},
                      ...getGeologicalSerieThicknesses().map((l, i) =>
                          renderGeoSerieTableLine(l, i)
                      )
                  )
              )
          );

const renderGeoSystemTableLine = (l: number, index: number) =>
    l === 0
        ? renderNoTableLine()
        : TR(
              {
                  className: '',
                  //   style: { height: computeLineHeight(l) + '%' },
                  style: { height: `${computeSmartSystemLineHeight(index)}px` },
                  key: index,
              },
              TD({}, getGeologicalSystem()[index])
          );

const renderGeoSystemTable = () =>
    getShowPartTableGeol()
        ? NODISPLAY()
        : DIV(
              {},
              TABLE(
                  {
                      style: { height: getDiagramHeight() },
                  },
                  THEAD({}, TR({}, TH({}, tr.geo('systemCol')))),
                  TBODY(
                      {},
                      ...getGeologicalSystemThicknesses().map((l, i) =>
                          renderGeoSystemTableLine(l, i)
                      )
                  )
              )
          );

const renderGeoEraTableLine = (l: number, index: number) =>
    l === 0
        ? renderNoTableLine()
        : TR(
              {
                  className: '',
                  //style: { height: computeLineHeight(l) + '%' },
                  style: { height: `${computeSmartEraLineHeight(index)}px` },
                  key: index,
              },
              TD({}, getGeologicalEra()[index])
          );

const renderGeoEraTable = () =>
    getShowPartTableGeol()
        ? NODISPLAY()
        : DIV(
              {},
              TABLE(
                  {
                      style: { height: getDiagramHeight() },
                  },
                  THEAD({}, TR({}, TH({}, tr.geo('eraCol')))),
                  TBODY(
                      {},
                      ...getGeologicalEraThicknesses().map((l, i) =>
                          renderGeoEraTableLine(l, i)
                      )
                  )
              )
          );
