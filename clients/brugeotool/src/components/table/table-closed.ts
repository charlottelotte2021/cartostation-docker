import * as debug from 'debug';

import {
    DIV,
    TABLE,
    TBODY,
    THEAD,
    TH,
    TR,
    TD,
    NODISPLAY,
} from 'sdi/components/elements';
import tr, { formatNumber } from 'sdi/locale';
import { fromNullable } from 'fp-ts/lib/Option';
import { rect } from 'sdi/app';
import { markdown } from 'sdi/ports/marked';

import {
    getLayersThicknesses,
    getLayersSat,
    getDrySatConductivity,
    getDryConductivity,
    getSatConductivity,
    getDrySatLayersDepths,
    computeCumulDrySatConductivity,
    getDiagramHeight,
    getUSName,
} from '../../queries/geothermie';
import { getLayout } from '../../queries/app';
import { updateConnect } from 'brugeotool/src/events/geothermie';
import {
    STRATIGRAPHY_UNITS_NAME,
    STRATIGRAPHY_COLORS,
    addSuffixToMessageKey,
} from '../settings';

import {
    renderNoTableLine,
    computeSmartClosedLineHeight,
    getLayersDepths,
    formatDepth,
    renderMoreText,
} from './format';
import { renderDepthTitle } from './table-depth';
import renderGraph from './graph';
import { GRAPH_TOP_MARGIN } from './engine';

const logger = debug('sdi:table-close');

const getUS = () =>
    STRATIGRAPHY_UNITS_NAME.map(
        u => getUSName(u)
    );

const getRemarks = () =>
    addSuffixToMessageKey(STRATIGRAPHY_UNITS_NAME, '-remark').map(u =>
        u === 'US/RBC_1-remark' ? tr.geo('US/RBC_11-14-remark') : tr.geo(u)
    );

// Render of tables

const rectifyConnect = (_r: any, el: Element) => {
    const offset = fromNullable(el.querySelector('thead'))
        .map(h => h.clientHeight)
        .getOrElse(0);
    fromNullable(el.querySelector('tbody')).map(tbody => {
        const heights = [...tbody.children]
            .map(c => c.clientHeight)
            .filter(h => h > 0);
        updateConnect('depth', () => ({ offset, heights }));
    });
    getLayersThicknesses().map(ts => {
        updateConnect('conductivity', () => {
            const cc = ts.filter(t => t > 0);
            const maxdepth = cc.reduce((acc, v) => acc + v);
            const height = getDiagramHeight() - GRAPH_TOP_MARGIN;
            const scale = height / maxdepth;
            logger(`height: ${height}, maxdepth: ${maxdepth}, scale: ${scale}`);
            return {
                offset: GRAPH_TOP_MARGIN,
                heights: cc.map(c => c * scale),
            };
        });
    });
};

const renderCloseTableLine = (l: number, index: number) =>
    l === 0
        ? renderNoTableLine()
        : TR(
            {
                className: 'closed-tr',
                // style: { height: computeLineHeight(l) + '%' },
                style: { height: `${computeSmartClosedLineHeight(index)}px` },
                key: index,
            },
            TD(
                {
                    className: `table--closed--body--conductivity
                        ${/*getLayersSat()[index] === 1 ? 'saturated' : ''*/''}
                        ${/*US_GREY_SHADE[index]*/''}
                    `,
                    style: {
                        // For FF only
                        height: `${computeSmartClosedLineHeight(index)}px`,
                    },
                },
                getLayersSat()[index] !== 1
                    ? DIV(
                        {
                            style: {
                                height:
                                    (1 - getLayersSat()[index]) * 100 + '%',
                                borderBottom:
                                    getLayersSat()[index] === 0
                                        ? 'none'
                                        : '1px solid black',
                            },
                        },
                        formatNumber(getDryConductivity()[index])
                    )
                    : NODISPLAY(),
                getLayersSat()[index] !== 0
                    ? DIV(
                        {
                            style: {
                                height: getLayersSat()[index] * 100 + '%',
                            },
                            className: `
                                ${
                                    /*getLayersSat()[index] === 1
                                        ? ''
                                        : 'saturated'*/''
                                }
                                ${/*US_GREY_SHADE[index]*/''}
                            `,
                        },
                        formatNumber(getSatConductivity()[index])
                    )
                    : NODISPLAY()
            ),
            TD(
                { className: 'cell-depth' },
                index === getLayersDepths().length - 1
                    ? ''
                    : formatDepth(getLayersDepths()[index])
            ),
            TD(
                {
                    className: 'large',
                    style: { background: STRATIGRAPHY_COLORS[index] },
                },
                getUS()[index]
            ),
            TD(
                {
                    className: `large ${
                        getLayout() === 'print' ? '' : 'remark'
                    }`,
                },
                getLayout() === 'print'
                    ? getRemarks()[index]
                    : renderMoreText(
                        getRemarks()[index],
                        'remarks',
                        index,
                        18
                    )
            )
        );

export const renderCloseTable = () =>
    getLayersThicknesses().map(thicknesses =>
        DIV(
            {
                className: 'table--closed guide',
                key: 'table--closed',
            },
            TABLE(
                {
                    ref: rect(rectifyConnect),
                    style: {
                        height: getDiagramHeight(),
                    },
                    key: 'render-close-table',
                },
                THEAD(
                    { className: 'conductivity-thead' },
                    TR(
                        {},
                        TH(
                            { style: { borderBottom: 'none' } },
                            markdown(tr.geo('closeTableColCond'))
                        ),
                        renderDepthTitle(),
                        TH({}, markdown(tr.geo('closeTableColUS'))),
                        TH({}, markdown(tr.geo('closeTableColRemark')))
                    )
                ),
                TBODY(
                    {
                        className: 'table--closed--body',
                    },
                    ...thicknesses.map((l, i) => renderCloseTableLine(l, i))
                )
            )
        )
    );

export const renderGraphDiv = () =>
    DIV(
        {},
        renderGraph(
            getDrySatLayersDepths(),
            getDrySatConductivity(),
            computeCumulDrySatConductivity()
        )
    );

logger('loaded');
