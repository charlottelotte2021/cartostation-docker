import * as io from 'io-ts';

import { DIV, IMG, INPUT, SPAN, LABEL } from 'sdi/components/elements';
import tr from 'sdi/locale';

import {
    getLayersTotalDepth,
    getGraphSnapXy,
    getGraphWidth,
    getDiagramHeight,
    getGraphXy,
    getBedrockDepth,
} from '../../queries/geothermie';
import {
    setGraphXy,
    setGraphSnapXy,
    updateDepth,
    setInputDepth,
} from '../../events/geothermie';

import {
    graph,
    GRAPH_LEFT_MARGIN,
    GRAPH_TOP_MARGIN,
    MAX_CONDUCTIVITY_VALUE,
} from './engine';
import { markdown } from 'sdi/ports/marked';

// tslint:disable-next-line: variable-name
export const GraphXYIO = io.interface(
    {
        x: io.Integer,
        y: io.Integer,
    },
    'XyIO'
);

export type GraphXY = io.TypeOf<typeof GraphXYIO>;

export const newGraphXY = (x: number, y: number): GraphXY => {
    return { x, y };
};

const getGraphHeight = getDiagramHeight;

const { getGraph } = graph();

export const xToValue = (x: number) =>
    /*
     * Convert a x pixel value (in px) on the graph canvas into a conductivity value (in W/(m.K))
     */
    ((x - GRAPH_LEFT_MARGIN) / (getGraphWidth() - GRAPH_LEFT_MARGIN)) *
    MAX_CONDUCTIVITY_VALUE;

export const yToDepth = (y: number) =>
    /*
     * Convert a y pixel value (in px) on the graph canvas into a depth value (in meter)
     */
    ((y - GRAPH_TOP_MARGIN) / (getGraphHeight() - GRAPH_TOP_MARGIN)) *
    getLayersTotalDepth();

const valueToX = (value: number) =>
    /*
     * Convert a conductivity value (in W/(m.K)) into a x pixel value (in px) on the graph canvas
     */
    (value / MAX_CONDUCTIVITY_VALUE) * (getGraphWidth() - GRAPH_LEFT_MARGIN) +
    GRAPH_LEFT_MARGIN;

export const depthToY = (depth: number) =>
    /*
     * Convert a depth value (in m) into a x pixel value (in px) on the graph canvas
     */
    (depth / getLayersTotalDepth()) * (getGraphHeight() - GRAPH_TOP_MARGIN) +
    GRAPH_TOP_MARGIN;

export const snapXy = (
    depths: number[],
    values: number[],
    cumulValues: number[]
) => {
    const y = getGraphXy().fold(0, xy => xy.y);

    const notNullDepths: number[] = [];
    const notNullCumulValues: number[] = [];
    depths.map((d, i) => {
        if (depths[i - 1] !== depths[i]) {
            notNullDepths.push(d);
            notNullCumulValues.push(cumulValues[i]);
        }
    });
    let xSnap: number;
    const depthValue = yToDepth(y);

    if (depthValue < notNullDepths[0]) {
        /*
         * we set the conductivity value (xToValue(xSnap)) equal to the first value (no interpolation)
         */
        setGraphSnapXy({ x: valueToX(notNullCumulValues[0]), y });
    } else {
        if (depthValue > notNullDepths[notNullDepths.length - 1]) {
            /*
             * we extrapole the conductivity value (xToValue(xSnap)) below the bedrock depth.
             * For depth (y) >>> bedrockDepth, xSnap ~= condBedrock
             */
            const cumulCondAtTopBedrock = valueToX(
                notNullCumulValues[notNullCumulValues.length - 1]
            );
            const condBedrock = valueToX(values[values.length - 1]);
            const bedrockDepthToY = depthToY(getBedrockDepth());

            xSnap =
                (cumulCondAtTopBedrock * bedrockDepthToY +
                    condBedrock * (y - bedrockDepthToY)) /
                y;

            setGraphSnapXy({ x: xSnap, y });
        } else {
            /*
             * we interpolate the conductivity value (xToValue(xSnap)) between each geology interface
             * by building a linear function between the bottom and the top of the layer.
             */
            let snapIndex: number;
            notNullDepths.map((d, i) => {
                if (depthValue >= d && depthValue <= notNullDepths[i + 1]) {
                    snapIndex = i;

                    const x1 = valueToX(notNullCumulValues[snapIndex]);
                    const y1 = depthToY(notNullDepths[snapIndex]);
                    const x2 = valueToX(notNullCumulValues[snapIndex + 1]);
                    const y2 = depthToY(notNullDepths[snapIndex + 1]);

                    const a = isNaN((y1 - y2) / (x1 - x2))
                        ? 0
                        : (y1 - y2) / (x1 - x2);
                    const b = y1 - a * x1;

                    xSnap = (y - b) / a;

                    setGraphSnapXy({ x: xSnap, y });
                }
            });
        }
    }
};

const onClickOnGraph = (
    e: React.MouseEvent<HTMLElement>,
    depths: number[],
    values: number[],
    cumulValues: number[]
) => {
    const rect = document.getElementById('graph')!.getBoundingClientRect();
    const x = e.clientX - rect.left;
    const y = e.clientY - rect.top;
    setGraphXy({ x, y });
    snapXy(depths, values, cumulValues);
    setInputDepth(Math.round(yToDepth(y)));
};

const render = (
    depthValues: number[],
    values: number[],
    cumulValues: number[]
) =>
    DIV(
        { className: 'graph' },
        IMG({
            id: 'graph',
            onClick: e => onClickOnGraph(e, depthValues, values, cumulValues),
            src: getGraph(
                getGraphWidth(),
                getGraphHeight(),
                depthValues,
                values,
                cumulValues
            ),
            alt: 'geologic carrot layers',
        }),
        DIV(
            { className: 'graph__reader' },
            DIV(
                { className: 'graph__reader__value' },
                markdown(tr.geo('condGraphTitle'), 'graph__reader__value__title'),
                xToValue(
                    getGraphSnapXy().fold(GRAPH_LEFT_MARGIN, xy => xy.x)
                ).toFixed(2),
                ' W/(m.K)'
            ),
            LABEL(
                { htmlFor: 'input-closeGraphDepth' },
                tr.geo('closeGraphDepth')
            ),
            INPUT({
                className: '',
                id: 'input-closeGraphDepth',
                type: 'number',
                onChange: e =>
                    updateDepth(
                        Number(e.currentTarget.value),
                        depthValues,
                        values,
                        cumulValues
                    ),
                value: Math.round(
                    yToDepth(
                        getGraphSnapXy().fold(GRAPH_TOP_MARGIN, xy => xy.y)
                    )
                ),
                min: 0,
            }),
            SPAN({}, 'm')
        )
    );

export default render;
