import { DIV, TABLE, TBODY, THEAD, TH, TR, TD } from 'sdi/components/elements';
import tr from 'sdi/locale';

import {
    getHydroLayersThicknesses,
    getDiagramHeight,
    getKMin,
    getKMean,
    getKMax,
    getUHPotentialLabel,
    getUHPotentialColor,
} from '../../queries/geothermie';
import {
    addSuffixToMessageKey,
    HYDROGEOLOGY_UNITS_NAME,
    POTENTIAL_CLASS,
    UH_GREY_SHADE,
    getUHCode,
} from '../settings';

import {
    renderNoTableLine,
    // computeLineHeight,
    keyAndTrad,
    getHydroLayersDepths,
    computeSmartOpenLineHeight,
} from './format';
import { renderDepthTitle, renderDepthCol } from './table-depth';
import { markdown } from 'sdi/ports/marked';
import { rect } from 'sdi/app';
import { fromNullable } from 'fp-ts/lib/Option';
import { updateConnect } from 'brugeotool/src/events/geothermie';

const getUH = () => HYDROGEOLOGY_UNITS_NAME.map(u => keyAndTrad(u));

const getHydroState = () =>
    addSuffixToMessageKey(HYDROGEOLOGY_UNITS_NAME, '-hydro_state').map(u =>
        tr.geo(u)
    );

// const getPotential = () =>
//     addSuffixToMessageKey(HYDROGEOLOGY_UNITS_NAME, '-potential').map(u =>
//         tr.geo(u)
//     );

const getPotentialClassName = () => POTENTIAL_CLASS;

const getUHClassName = () => UH_GREY_SHADE;

// FIXTURES
// const getLayersThicknesses = () => [9.331809997558594, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.7692070007324219, 3.0461788177490234, 27.77418327331543, 45.48012161254883, 13.581069946289062, 19.563568115234375, 11.043914794921875] // for debug when no connection
// const getLayersSat = () => [0.3, 0, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.8, 1, 1, 1, 1, 1, 1, 1]; // (fixture) get real data from x,y
// const getQuaternaireThicknesses = () => [0.2, 0.3, 0.4, 0.1]; // (fixture)

// Render of tables

const renderLeftOpenTableLine = (l: number, index: number) =>
    l === 0
        ? renderNoTableLine()
        : TR(
              {
                  className: '',
                  style: {
                      // height: computeLineHeight(l) + '%'
                  },
                  key: index,
              },
              TD({}, getHydroState()[index]),
              TD(
                  { className: `uh ${getUHClassName()[index]}` },
                  getUH()[index]
              ),
              renderDepthCol(getHydroLayersDepths(), index)
          );

export const renderLeftOpenTable = () =>
    DIV(
        { className: '~table--open' },
        TABLE(
            {
                style: {
                    // height: getDiagramHeight()
                },
            },
            THEAD(
                {},
                TR(
                    {},
                    TH({}, markdown(tr.geo('openTableColState'))),
                    TH({}, markdown(tr.geo('openTableColUH'))),
                    renderDepthTitle()
                )
            ),
            TBODY(
                {},
                ...getHydroLayersThicknesses().map((l, i) =>
                    renderLeftOpenTableLine(l, i)
                )
            )
        )
    );

const renderRightOpenTableLine = (l: number, index: number) =>
    l === 0
        ? renderNoTableLine()
        : TR(
              {
                  className: '',
                  style: {
                      // height: computeLineHeight(l) + '%'
                  },
                  key: index,
              },
              TD({}, getKMin()[index]),
              TD({}, getKMean()[index]),
              TD({}, getKMax()[index]),
              TD(
                  {
                      className: `potential ---${
                          getPotentialClassName()[index]
                      }---`,
                      style: {
                          backgroundColor: getUHPotentialColor(
                              getUHCode(index)
                          ),
                      },
                  },
                  getUHPotentialLabel(getUHCode(index))
              )
          );

export const renderRightOpenTable = () =>
    DIV(
        { className: '~table--open' },
        TABLE(
            {
                style: {
                    // height: getDiagramHeight()
                },
            },
            THEAD(
                {},
                TR(
                    {},
                    TH({}, markdown(tr.geo('openTableColKmin'))),
                    TH({}, markdown(tr.geo('openTableColKmean'))),
                    TH({}, markdown(tr.geo('openTableColKmax'))),
                    TH({}, markdown(tr.geo('openTableColPotential')))
                )
            ),
            TBODY(
                {},
                ...getHydroLayersThicknesses().map((l, i) =>
                    renderRightOpenTableLine(l, i)
                )
            )
        )
    );

const renderOpenTableLine = (l: number, index: number) =>
    l === 0
        ? renderNoTableLine()
        : TR(
              {
                  className: '',
                  style: {
                      // height: computeLineHeight(l) + '%'
                      height: `${computeSmartOpenLineHeight(index)}px`,
                  },
                  key: index,
              },
              renderDepthCol(getHydroLayersDepths(), index),
              TD(
                  { className: `uh ${getUHClassName()[index]}` },
                  getUH()[index]
              ),
              TD({}, getHydroState()[index]),
              TD({}, getKMin()[index]),
              TD({}, getKMean()[index]),
              TD({}, getKMax()[index]),
              TD(
                  {
                      className: `potential ---${
                          getPotentialClassName()[index]
                      }--- ${index} ${getUHCode(index)}`,
                      style: {
                          backgroundColor: getUHPotentialColor(
                              getUHCode(index)
                          ),
                      },
                  },
                  getUHPotentialLabel(getUHCode(index))
              )
          );

const rectifyTable = (_r: any, el: Element) => {
    const offset = fromNullable(el.querySelector('thead'))
        .map(h => h.clientHeight)
        .getOrElse(0);
    fromNullable(el.querySelector('tbody')).map(tbody => {
        const heights = [...tbody.children]
            .map(c => c.clientHeight)
            .filter(h => h > 0);
        updateConnect('depth', () => ({ offset, heights }));
    });
};

export const renderOpenTable = () =>
    DIV(
        { className: '~table--open guide' },
        TABLE(
            {
                key: 'render-open-table',
                ref: rect(rectifyTable),
                style: {
                    height: getDiagramHeight(),
                },
            },
            THEAD(
                {},
                TR(
                    {},
                    renderDepthTitle(),
                    TH({}, markdown(tr.geo('openTableColUH'))),
                    TH({}, markdown(tr.geo('openTableColState'))),
                    TH({}, markdown(tr.geo('openTableColKmin'))),
                    TH({}, markdown(tr.geo('openTableColKmean'))),
                    TH({}, markdown(tr.geo('openTableColKmax'))),
                    TH({}, markdown(tr.geo('openTableColPotential')))
                )
            ),
            TBODY(
                {},
                ...getHydroLayersThicknesses().map((l, i) =>
                    renderOpenTableLine(l, i)
                )
            )
        )
    );
