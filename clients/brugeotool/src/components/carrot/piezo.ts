/* draw details for piezo on the right of the carrot */

import * as debug from 'debug';
import { DIV, CANVAS } from 'sdi/components/elements';
import { none, some, Option } from 'fp-ts/lib/Option';
import { markdown } from 'sdi/ports/marked';
import tr from 'sdi/locale';

import {
    getDiagramHeight,
    getLayersTotalDepth,
    getLayersThicknesses,
    getGreyShades,
    getHydroLayerDepth,
    getHydroLayersThicknesses,
    getHydroLayerIndex,
    getPiezoValue,
    getSystem,
    getDepthMode,
    isUH1b,
    getUH_RBC1aThicknessFactor,
    getUH_RBC1bThicknessFactor,
} from 'brugeotool/src/queries/geothermie';
import { rect } from 'sdi/app';
import { Nullable, isNotNullNorUndefined } from 'sdi/util';
import { getDepthPixelRatio, color } from './engine'; // todo refactoriser (importer les couleurs de là ?)
import { drawYAxisTick, writeYAxisLabels } from './axes';
import {
    STRATIGRAPHY_COLORS,
    PIEZO_LAYERS_NAME_GEOLOGY_OPEN,
    PIEZO_LAYERS_NAME,
} from '../settings';
import { Layout } from '../../events/route';
import { getLayout } from 'brugeotool/src/queries/app';
import { GeoMessageKey } from 'brugeotool/src/locale';
import { formatPiezo } from '../table/format';

const logger = debug('sdi:piezo');

const settings = {
    width: {
        axe: 35,
        coloredColumn: 10,
        piezoPadding: 35, // avant de dessiner les piezo (sur le gris)
        piezoGap: 40, // entre les piezo
        piezoColumn: 10,
    },
    noPizeoValue: -1000,
    maxPositivePiezoValue: 2, // val max pour piezo positive
    ratioForPositiveValue: 0.5, // ratio pour piezo positive (pour pas aller trop haut)
};

let updater: Option<() => void> = none;
const rectify = rect(_ => updater.map(u => u())); // modify if the size of the canvas change

function reducedPositivePiezoValue(val: number) {
    // rectify the value for positive (below a max)
    const valWithRatio = val > 1 ? val * settings.ratioForPositiveValue : val; // divide by 2 if positive
    return valWithRatio > settings.maxPositivePiezoValue
        ? settings.maxPositivePiezoValue
        : valWithRatio;
}

// savoir le nombre de piezo
const getPiezoColumnNumber = () => {
    const layout = getLayout(); // todo check si le charger à chaque fois
    const system = getSystem().getOrElse('open');
    const piezoLayersName =
        (layout === 'geology' && system === 'open') ||
        (layout === 'print' && system === 'open')
            ? PIEZO_LAYERS_NAME_GEOLOGY_OPEN
            : PIEZO_LAYERS_NAME;

    const ret = piezoLayersName.reduce(
        (cpt: number, currentPiezoLayerName: GeoMessageKey) => {
            const currentPiezoVal = getPiezoValue(
                currentPiezoLayerName
            ).getOrElse(settings.noPizeoValue);
            const piezoThickness =
                getHydroLayersThicknesses()[
                    getHydroLayerIndex(currentPiezoLayerName)
                ];

            if (currentPiezoVal > settings.noPizeoValue && piezoThickness > 0) {
                return cpt + 1;
            } else {
                return cpt;
            }
        },
        0
    );

    return ret;
};

const getPiezoColumnWidth = () =>
    2 * settings.width.piezoPadding +
    getPiezoColumnNumber() * settings.width.piezoColumn +
    (getPiezoColumnNumber() - 1) * settings.width.piezoGap;

// canvas width
const getCanvasWidth = () =>
    getLayout() === 'technical'
        ? settings.width.coloredColumn +
          settings.width.axe +
          getPiezoColumnWidth()
        : settings.width.axe + getPiezoColumnWidth();
//
// const getCanvasWidth = () => settings.width.axe + settings.width.coloredColumn + getPiezoColumnWidth() - 10;

/**
 *
 * @param vBL Left vertex (black color) at the bottum of the crepine
 * @param vBR Right vertex (black color) at the bottum of the crepine
 * @param crepineProf Deph (profondeur) of the crepine
 * @param crepineStep Height of each step of the crepine
 */
function _drawCrepine(
    ctx: CanvasRenderingContext2D,
    vBLX: number, // abscice of vBL
    vBRX: number, // abscice of vBR
    vBY: number, // ord of VB
    crepineProf: number = 0,
    crepineStep: number = 5
) {
    //  | b |   c   | b |    ^
    //  | l |   o   | l |    |
    //  | a |   l   | a |    | crepineProf
    //  | c |   o   | c |    |
    //  | k |   r   | k |    v
    // vBL             vBR

    if (crepineProf > 0) {
        // draw crepine
        const numberSteps: number = crepineProf / crepineStep;

        // draw crepine as horizontal line
        for (let i = 0; i < numberSteps; i++) {
            ctx.beginPath();
            ctx.moveTo(vBLX, vBY - crepineProf + i * crepineStep);
            ctx.lineTo(vBRX, vBY - crepineProf + i * crepineStep);
            ctx.strokeStyle = color.black;
            ctx.stroke();
        }
    }
}

const drawPiezo = (
    layout: Layout,
    system: string,
    ctx: CanvasRenderingContext2D,
    ratioMeterPixel: number
) => {
    let it = 0;

    const piezoLayersName =
        (layout === 'geology' && system === 'open') ||
        (layout === 'print' && system === 'open')
            ? PIEZO_LAYERS_NAME_GEOLOGY_OPEN
            : PIEZO_LAYERS_NAME;

    piezoLayersName.slice().map(piezoName => {
        const piezoDepth = getHydroLayerDepth(piezoName); // a améliorer en fonction de ce qui vient d'être fait (partir US et non UH)
        const piezoThickness =
            getHydroLayersThicknesses()[getHydroLayerIndex(piezoName)];

        const piezoLayerBottom = piezoDepth + piezoThickness; // the bottom off the piezo layer // ici X

        const piezoStartX =
            getLayout() === 'technical'
                ? settings.width.axe +
                  settings.width.coloredColumn +
                  settings.width.piezoPadding
                : settings.width.axe + settings.width.piezoPadding;
        const piezoLevZeroY = 99;

        const currentPiezoStartX =
            piezoStartX +
            it * (settings.width.piezoColumn + settings.width.piezoGap);
        const currentPiezoEndX =
            currentPiezoStartX + settings.width.piezoColumn;

        getPiezoValue(piezoName).fold(null, piezoValue => {
            if (piezoValue > settings.noPizeoValue && piezoThickness > 0) {
                ctx.save();
                ctx.setLineDash([5]);
                ctx.beginPath();
                ctx.moveTo(
                    currentPiezoStartX + settings.width.piezoColumn / 2,
                    50
                );
                ctx.lineTo(
                    currentPiezoStartX + settings.width.piezoColumn / 2,
                    piezoLevZeroY
                );
                ctx.strokeStyle = color.darkgray;
                ctx.stroke();
                ctx.restore();

                // draw white
                ctx.beginPath();
                ctx.moveTo(currentPiezoStartX - 0.1, piezoLevZeroY); // todo voir pour le max - piezoYMaxVal); // -0.1 -> to draw over the black line at 0
                ctx.lineTo(
                    currentPiezoStartX - 0.1,
                    piezoLevZeroY + piezoLayerBottom / ratioMeterPixel
                );
                ctx.lineTo(
                    currentPiezoEndX,
                    piezoLevZeroY + piezoLayerBottom / ratioMeterPixel
                );
                ctx.lineTo(currentPiezoEndX, piezoLevZeroY); // todo voir pour le max- piezoYMaxVal);
                ctx.closePath();
                ctx.fillStyle = color.white;
                ctx.strokeStyle = color.white;
                ctx.fill();
                ctx.stroke();

                const reducedPizeoValue =
                    reducedPositivePiezoValue(piezoValue) == 0
                        ? 0.1
                        : reducedPositivePiezoValue(piezoValue); // to draw over the black line at 0

                // draw water
                ctx.beginPath();
                ctx.moveTo(
                    currentPiezoStartX,
                    piezoLevZeroY - reducedPizeoValue / ratioMeterPixel //
                );
                ctx.lineTo(
                    currentPiezoEndX,
                    piezoLevZeroY - reducedPizeoValue / ratioMeterPixel
                );
                ctx.lineTo(
                    currentPiezoEndX,
                    piezoLevZeroY + piezoLayerBottom / ratioMeterPixel
                );
                ctx.lineTo(
                    currentPiezoStartX,
                    piezoLevZeroY + piezoLayerBottom / ratioMeterPixel
                );
                ctx.closePath();
                ctx.fillStyle = color.saturated_wo_transp;
                ctx.strokeStyle = color.saturated_wo_transp;
                ctx.fill();
                ctx.stroke();

                // draw the pipe
                ctx.beginPath();
                ctx.moveTo(
                    currentPiezoStartX,
                    piezoLevZeroY // todo gérer valeur positive
                );
                ctx.lineTo(
                    currentPiezoStartX,
                    piezoLevZeroY + piezoLayerBottom / ratioMeterPixel
                );
                ctx.lineTo(
                    currentPiezoEndX,
                    piezoLevZeroY + piezoLayerBottom / ratioMeterPixel
                );
                ctx.lineTo(
                    currentPiezoEndX,
                    piezoLevZeroY // todo gérer valeur positive
                );

                ctx.strokeStyle = color.black;
                ctx.stroke();

                _drawCrepine(
                    ctx,
                    currentPiezoStartX,
                    currentPiezoEndX,
                    piezoLevZeroY + piezoLayerBottom / ratioMeterPixel,
                    piezoThickness / ratioMeterPixel
                );

                // write blue arrow if artesianism
                if (
                    piezoValue - reducedPizeoValue >
                    settings.maxPositivePiezoValue
                ) {
                    ctx.save();
                    ctx.beginPath();
                    ctx.moveTo(
                        currentPiezoStartX - 5,
                        piezoLevZeroY - reducedPizeoValue / ratioMeterPixel + 10
                    );
                    ctx.lineTo(
                        currentPiezoStartX - 5,
                        piezoLevZeroY - reducedPizeoValue / ratioMeterPixel - 10
                    );
                    ctx.lineTo(
                        currentPiezoStartX - 5 - 3,
                        piezoLevZeroY - reducedPizeoValue / ratioMeterPixel - 6
                    ); // left arrow head
                    ctx.moveTo(
                        currentPiezoStartX - 5,
                        piezoLevZeroY - reducedPizeoValue / ratioMeterPixel - 10
                    );
                    ctx.lineTo(
                        currentPiezoStartX - 5 + 3,
                        piezoLevZeroY - reducedPizeoValue / ratioMeterPixel - 6
                    ); // right arrow head
                    ctx.closePath();
                    ctx.strokeStyle = color.black;
                    ctx.stroke();
                    ctx.restore();
                }

                // write piezo values
                ctx.save();
                ctx.beginPath();
                ctx.rect(
                    currentPiezoStartX + 12,
                    piezoLevZeroY - reducedPizeoValue / ratioMeterPixel - 7,
                    28,
                    12
                );
                ctx.fillStyle = 'rgba(255, 255, 255, 0.7)'; // white with transparent (for firefox)
                ctx.fill();
                ctx.restore();

                ctx.fillStyle = color.black;
                ctx.font = '12px arial';
                ctx.textAlign = 'right';
                ctx.textBaseline = 'middle';

                const piezoValueStr = formatPiezo(piezoValue);

                ctx.fillText(
                    piezoValueStr,
                    currentPiezoStartX + 40,
                    piezoLevZeroY - reducedPizeoValue / ratioMeterPixel
                );

                // write piezo UH
                ctx.fillStyle = color.black;
                ctx.font = '12px arial';
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                ctx.fillText(
                    piezoName.replace('/RBC_', ''),
                    currentPiezoStartX + 3,
                    40
                );

                it = it + 1;
            }
        });
    });
};

const drawInto = (canvasRef: HTMLCanvasElement) => {
    // todo renommer ?
    const width = getCanvasWidth();
    const diagramHeight = getDiagramHeight();
    canvasRef.height = diagramHeight;
    canvasRef.width = width;

    const piezoWidth = getPiezoColumnWidth();

    const ctx = canvasRef.getContext('2d')!;
    const ratioMeterPixel = getDepthPixelRatio(diagramHeight, 99);

    // draw the left axis
    drawYAxisTick(ctx, getLayersTotalDepth(), diagramHeight - 99, 30, 99);
    writeYAxisLabels(ctx, getLayersTotalDepth(), diagramHeight - 99, 30, 99);

    const y0 = 99;
    const x0 = settings.width.axe; // draw afeter axes

    // draw colored colum
    const thicknesses = getLayersThicknesses().fold([], arr => arr);

    const drawLevels = thicknesses.reduce(
        (currentTopDepthPxl, currentThickness, qindex) => {
            let currentThicknessPxl = currentThickness / ratioMeterPixel;

            if (qindex === 0 && isUH1b()) {
                //draw quaternaire in 2 layers
                ctx.save();
                // 1st layer
                const thicknessUH1a =
                    currentThicknessPxl * getUH_RBC1aThicknessFactor();
                let grayX01a = settings.width.axe; // todo renommer
                if (getLayout() === 'technical') {
                    ctx.beginPath();
                    ctx.fillStyle = STRATIGRAPHY_COLORS[qindex];
                    ctx.rect(
                        x0,
                        y0 + currentTopDepthPxl,
                        settings.width.coloredColumn,
                        thicknessUH1a
                    );
                    ctx.stroke();
                    ctx.fill();

                    grayX01a = grayX01a + settings.width.coloredColumn;
                }

                // ici dark light
                ctx.beginPath();
                ctx.fillStyle = getGreyShades()[0].left; // hardcoded
                ctx.rect(
                    grayX01a,
                    y0 + currentTopDepthPxl,
                    piezoWidth,
                    thicknessUH1a
                );
                ctx.fill();
                if (getLayout() === 'technical') {
                    ctx.stroke();
                }

                // 2nd layer
                const thicknessUH1b =
                    currentThicknessPxl * getUH_RBC1bThicknessFactor();
                let grayX01b = settings.width.axe; // todo renommer
                if (getLayout() === 'technical') {
                    ctx.beginPath();
                    ctx.fillStyle = STRATIGRAPHY_COLORS[qindex];
                    ctx.rect(
                        x0,
                        y0 + currentTopDepthPxl + thicknessUH1a,
                        settings.width.coloredColumn,
                        thicknessUH1b
                    );
                    ctx.stroke();
                    ctx.fill();

                    grayX01b = grayX01b + settings.width.coloredColumn;
                }

                // ici dark light
                ctx.beginPath();
                ctx.fillStyle = getGreyShades()[1].left; // hardcoded
                ctx.rect(
                    grayX01b,
                    y0 + currentTopDepthPxl + thicknessUH1a,
                    piezoWidth,
                    thicknessUH1b
                );
                ctx.fill();
                if (getLayout() === 'technical') {
                    ctx.stroke();
                }

                ctx.restore();
            } else {
                ctx.save();
                let grayX0 = settings.width.axe; // todo renommer
                if (getLayout() === 'technical') {
                    ctx.beginPath();
                    ctx.fillStyle = STRATIGRAPHY_COLORS[qindex];
                    ctx.rect(
                        x0,
                        y0 + currentTopDepthPxl,
                        settings.width.coloredColumn,
                        currentThicknessPxl
                    );
                    ctx.stroke();
                    ctx.fill();

                    grayX0 = grayX0 + settings.width.coloredColumn;
                }

                // ici dark light
                ctx.beginPath();
                ctx.fillStyle = getGreyShades()[qindex].left;

                ctx.rect(
                    grayX0,
                    y0 + currentTopDepthPxl,
                    piezoWidth,
                    currentThicknessPxl
                );
                ctx.fill();
                if (getLayout() === 'technical') {
                    ctx.stroke();
                }
                ctx.restore();
            }

            return currentTopDepthPxl + currentThicknessPxl;
        },
        0
    );

    // draw a black box around everything
    if (getLayout() !== 'technical') {
        ctx.save();
        ctx.beginPath();
        ctx.rect(x0, y0, getPiezoColumnWidth() - 1, diagramHeight);
        ctx.strokeStyle = color.black;
        ctx.stroke();
        ctx.closePath();
        ctx.restore();
    }

    logger(drawLevels); // to force to drawLevels first (the return is useless)

    drawPiezo(getLayout(), getSystem().getOrElse('open'), ctx, ratioMeterPixel);

    return { diagramHeight, width };
};

const ref = (node: Nullable<HTMLCanvasElement>) => {
    if (isNotNullNorUndefined(node)) {
        updater = some(() => drawInto(node));
        rectify(node);
    } else {
        updater = none;
    }
};

export const renderPiezo = () => {
    updater.map(u => u());
    return DIV(
        { className: 'piezo' },
        DIV(
            { className: 'piezo-title' },
            getDepthMode() === 'relative'
                ? markdown(tr.geo('openRelativePiezo'))
                : markdown(tr.geo('openRelativePiezoAbsolute'))
        ),
        CANVAS({
            key: 'piezo',
            ref,
        })
    );
};

export default renderPiezo;

logger('loaded');
