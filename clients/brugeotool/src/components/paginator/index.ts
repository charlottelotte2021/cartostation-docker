import * as debug from 'debug';

import { BUTTON, DIV, NAV } from 'sdi/components/elements';
import tr from 'sdi/locale';

import {
    navigateGeneral,
    navigateLicense,
    navigateGeology,
    navigateFinance,
} from '../../events/route';
import { getX, getY } from '../../queries/geothermie';
import { getLayout } from '../../queries/app';
import { markdown } from 'sdi/ports/marked';

const logger = debug('sdi:geothermie');

const activeClass = (a: boolean) => (a ? 'selected' : 'inactive');

const render = () =>
    NAV(
        { className: 'content-header' },
        BUTTON(
            {
                className: `content-header--item item-general ${activeClass(
                    getLayout() === 'general'
                )}`,
                key: 'general',
                onClick: () => navigateGeneral(getX(), getY()),
            },
            DIV(
                { className: 'content-header--label-wrapper' },
                DIV({}, '1. '),
                DIV({}, markdown(tr.geo('paginatorGeneral')))
            )
        ),
        BUTTON(
            {
                className: `content-header--item item-license ${activeClass(
                    getLayout() === 'license'
                )}`,
                key: 'license',
                onClick: () => navigateLicense(getX(), getY()),
            },
            DIV(
                { className: 'content-header--label-wrapper' },
                DIV({}, '2. '),
                DIV({}, markdown(tr.geo('paginatorLicense')))
            )
        ),
        BUTTON(
            {
                className: `content-header--item item-geology ${activeClass(
                    getLayout() === 'geology'
                )}`,
                key: 'geology',
                onClick: () => navigateGeology(getX(), getY()),
            },
            DIV(
                { className: 'content-header--label-wrapper' },
                DIV({}, '3. '),
                DIV({}, markdown(tr.geo('paginatorGeology')))
            )
        ),
        BUTTON(
            {
                className: `content-header--item item-finance ${activeClass(
                    getLayout() === 'finance'
                )}`,
                key: 'finance',
                onClick: () => navigateFinance(getX(), getY()),
            },
            DIV(
                { className: 'content-header--label-wrapper' },
                DIV({}, '4. '),
                DIV({}, markdown(tr.geo('paginatorFinance')))
            )
        )
    );

export default render;

logger('loaded');
