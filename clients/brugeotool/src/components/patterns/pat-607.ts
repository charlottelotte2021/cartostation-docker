
export default `<ns0:svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:ns0="http://www.w3.org/2000/svg" xmlns:ns1="http://www.inkscape.org/namespaces/inkscape" xmlns:ns2="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:ns4="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" height="14.699909mm" id="svg3596" version="1.1" viewBox="0 0 19.049999 14.699909" width="19.049999mm" ns2:docname="607.svg" ns1:version="0.92.0 r15299">
    <ns0:defs id="defs3590">
        <ns0:clipPath clipPathUnits="userSpaceOnUse" id="clipPath1088">
            <ns0:path d="m 510.233,595.671 h 36.482 v -42.634 h -36.482 z" id="path1086" ns1:connector-curvature="0" />
        </ns0:clipPath>
        <ns0:clipPath clipPathUnits="userSpaceOnUse" id="clipPath1076">
            <ns0:path d="m 492.715,595.671 h 17.72 v -42.634 h -17.72 z" id="path1074" ns1:connector-curvature="0" />
        </ns0:clipPath>
    </ns0:defs>
    <ns2:namedview bordercolor="#666666" borderopacity="1.0" fit-margin-bottom="0" fit-margin-left="0" fit-margin-right="0" fit-margin-top="0" id="base" pagecolor="#ffffff" showgrid="false" ns1:current-layer="svg3596" ns1:cx="63.767606" ns1:cy="18.959478" ns1:document-units="mm" ns1:pageopacity="0.0" ns1:pageshadow="2" ns1:window-height="1017" ns1:window-maximized="1" ns1:window-width="1920" ns1:window-x="1672" ns1:window-y="-8" ns1:zoom="4" />
    <ns0:metadata id="metadata3593">
        <rdf:RDF>
            <ns4:Work rdf:about="">
                <dc:format>image/svg+xml</dc:format>
                <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
                <dc:title />
            </ns4:Work>
        </rdf:RDF>
    </ns0:metadata>
    <ns0:rect height="14.69946" id="rect5266" style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none" width="19.048296" x="0.001033525" y="0.0010649107" />
    <ns0:circle cx="14.97573" cy="0.59226656" id="circle4530" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="5.4186292" cy="0.59226656" id="circle4559" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="10.161856" cy="0.59226656" id="circle4561" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="0.59673297" cy="0.59226656" id="circle4572" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="12.543578" cy="2.0889759" id="circle4594" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="2.9864771" cy="2.0889759" id="circle4600" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="7.7297039" cy="2.0889759" id="circle4602" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="17.220318" cy="2.0889759" id="circle4604" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="14.97573" cy="3.5364444" id="circle4646" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="5.4186292" cy="3.5364444" id="circle4652" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="10.161856" cy="3.5364444" id="circle4654" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="0.59673297" cy="3.5364444" id="circle4656" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="12.543578" cy="4.986382" id="circle4662" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="2.9864771" cy="4.986382" id="circle4668" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="7.7297039" cy="4.986382" id="circle4670" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="17.220318" cy="4.986382" id="circle4672" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="14.97573" cy="6.5427561" id="circle4756" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="5.4186292" cy="6.5427561" id="circle4762" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="10.161856" cy="6.5427561" id="circle4764" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="0.59673297" cy="6.5427561" id="circle4766" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="12.543578" cy="7.9926939" id="circle4772" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="2.9864771" cy="7.9926939" id="circle4778" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="7.7297039" cy="7.9926939" id="circle4780" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="17.220318" cy="7.9926939" id="circle4782" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="14.97573" cy="9.5103207" id="circle4790" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="5.9808688" cy="9.4441748" id="circle4796" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="10.161856" cy="9.5103207" id="circle4798" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="0.59673297" cy="9.5103207" id="circle4800" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="12.543578" cy="11.100574" id="circle4806" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="2.9864771" cy="11.100574" id="circle4812" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="7.7297039" cy="11.100574" id="circle4814" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="17.220318" cy="11.100574" id="circle4816" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="14.97573" cy="12.503739" id="circle4849" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="5.0217543" cy="12.602958" id="circle4855" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="10.161856" cy="12.503739" id="circle4857" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="0.59673297" cy="12.503739" id="circle4859" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="12.543578" cy="13.953677" id="circle4865" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="2.9864771" cy="13.953677" id="circle4871" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="7.7297039" cy="13.953677" id="circle4873" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="17.220318" cy="13.953677" id="circle4875" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="5.4957294" cy="10.981522" id="circle4871-6" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="2.3853805" cy="6.584938" id="circle4871-4" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="7.8811102" cy="6.0236721" id="circle4871-8" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="11.178548" cy="1.6037024" id="circle4871-7" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="17.890354" cy="3.7084501" id="circle4871-2" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="18.708866" cy="9.9057617" id="circle4871-67" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="11.973675" cy="6.7018685" id="circle4871-674" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="8.6996231" cy="0.62148702" id="circle4871-9" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="2.0579753" cy="1.0086751" id="circle4871-1" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="0.88867116" cy="1.9077215" id="circle4871-0" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="15.084023" cy="8.6429138" id="circle4871-75" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="9.0036421" cy="9.7186737" id="circle4871-5" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="1.7773423" cy="11.425858" id="circle4871-63" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="12.160764" cy="11.963737" id="circle4871-07" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="12.612802" cy="9.7263746" id="circle4871-23-5" r="0" style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.21166669;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="12.612802" cy="9.7263746" id="circle4871-23-2" r="0" style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.21166669;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="9.541522" cy="2.1181962" id="circle4654-4" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="10.219719" cy="5.1116152" id="circle4654-3" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="5.355413" cy="4.9712987" id="circle4654-7" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="4.6304445" cy="2.1883545" id="circle4654-6" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="8.4891481" cy="3.7318361" id="circle4654-0" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="2.9934187" cy="9.4146547" id="circle4654-77" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="14.171967" cy="11.004908" id="circle4654-9" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="6.3493214" cy="2.5742249" id="circle4654-99" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="3.4845264" cy="0.25315616" id="circle4654-2" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="18.132227" cy="0.2346424" id="circle4654-03" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="14.42806" cy="14.447729" id="circle4654-92" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="6.1872811" cy="14.177735" id="circle4654-78" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="1.1244792" cy="13.773868" id="circle4654-61" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="16.834114" cy="12.583242" id="circle4654-1" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="15.246614" cy="2.0329823" id="circle4654-5" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="12.955891" cy="0.94889218" id="circle4654-15" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="16.41703" cy="1.0892086" id="circle4654-49" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="18.57305" cy="1.9450502" id="circle4654-22" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="1.554427" cy="5.3568101" id="circle4654-30" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="18.788469" cy="7.7542424" id="circle4654-72" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="18.289322" cy="6.0182686" id="circle4654-70" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="18.802698" cy="13.390692" id="circle4654-05" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="0.2414806" cy="4.9479122" id="circle4654-06" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="13.989843" cy="5.4725657" id="circle4654-19" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
    <ns0:circle cx="13.576432" cy="3.4055083" id="circle4654-57" r="0.22900039" style="opacity:1;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.21166667;stroke-miterlimit:4;stroke-dasharray:none" />
</ns0:svg>`
            