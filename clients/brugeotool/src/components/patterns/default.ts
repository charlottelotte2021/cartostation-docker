
export default `<ns0:svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:ns0="http://www.w3.org/2000/svg" xmlns:ns1="http://www.inkscape.org/namespaces/inkscape" xmlns:ns2="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:ns4="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" height="14.699909mm" id="svg3596" version="1.1" viewBox="0 0 19.049999 14.699909" width="19.049999mm" ns2:docname="0.svg" ns1:version="0.92.3 (2405546, 2018-03-11)">
    <ns0:defs id="defs3590">
        <ns0:clipPath clipPathUnits="userSpaceOnUse" id="clipPath1088">
            <ns0:path d="m 510.233,595.671 h 36.482 v -42.634 h -36.482 z" id="path1086" ns1:connector-curvature="0" />
        </ns0:clipPath>
        <ns0:clipPath clipPathUnits="userSpaceOnUse" id="clipPath1076">
            <ns0:path d="m 492.715,595.671 h 17.72 v -42.634 h -17.72 z" id="path1074" ns1:connector-curvature="0" />
        </ns0:clipPath>
    </ns0:defs>
    <ns2:namedview bordercolor="#666666" borderopacity="1.0" fit-margin-bottom="0" fit-margin-left="0" fit-margin-right="0" fit-margin-top="0" id="base" pagecolor="#ffffff" showgrid="false" ns1:current-layer="svg3596" ns1:cx="45.194573" ns1:cy="35.451227" ns1:document-units="mm" ns1:pageopacity="0.0" ns1:pageshadow="2" ns1:window-height="1052" ns1:window-maximized="1" ns1:window-width="1865" ns1:window-x="55" ns1:window-y="0" ns1:zoom="8" />
    <ns0:metadata id="metadata3593">
        <rdf:RDF>
            <ns4:Work rdf:about="">
                <dc:format>image/svg+xml</dc:format>
                <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
                <dc:title />
            </ns4:Work>
        </rdf:RDF>
    </ns0:metadata>
</ns0:svg>`
            