#!/usr/bin/env python3


import xml.etree.ElementTree as ET
from pathlib import PosixPath
from io import BytesIO


def main():

    for path in PosixPath().glob("*.svg"):
        tree = ET.parse(str(path))
        output = BytesIO()
        tree.write(output)
        contents = output.getvalue().decode("utf-8")
        output.close()

        with path.parent.joinpath(f"{path.stem}.ts").open("w") as file:
            file.write(
                f"""
export default `{contents}`
            """
            )


if __name__ == "__main__":
    main()
