import { Box, makeLine, makeImage, Rect, Coords } from 'sdi/print/context';
import { ApplyFn } from 'sdi/print/template';

const coordsFromRect = (rect: Rect, strokeWidth: number): Coords[] => [
    [-(strokeWidth / 2), 0],
    [rect.width, 0],
    [rect.width, rect.height],
    [0, rect.height],
    [0, -(strokeWidth / 2)],
];

const getMapSrc = () => {
    const olMap = document.getElementsByClassName('ol-viewport')[0]
        .children[0] as HTMLCanvasElement;
    return olMap.toDataURL();
};

export const renderMap = (f: ApplyFn<Box>) =>
    f('map', ({ rect, strokeWidth, color }) => ({
        ...rect,
        children: [
            makeImage(getMapSrc()),
            makeLine(coordsFromRect(rect, strokeWidth), strokeWidth, color),
        ],
    }));
