import { DIV } from 'sdi/components/elements';

import { renderPDF } from './makePDF';

const render = () =>
    DIV({ className: '', onClick: () => renderPDF() }, '~PRINT');

export default render;
