import { Box, makeImage, boxContent } from 'sdi/print/context';
import { ApplyFn } from 'sdi/print/template';

import { engine as carrotEngine } from '../carrot/engine';

const { getImage } = carrotEngine();

const getCarrotSrc = () => getImage(200, 1600);

export const renderCarrotBox = (f: ApplyFn<Box>) =>
    f('carrot', ({ rect }) => ({
        ...rect,
        children: [boxContent(rect, makeImage(getCarrotSrc()))],
    }));
