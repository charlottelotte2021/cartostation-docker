//import { getLang } from 'sdi/app';
import {
    createContext,
    Box,
    boxContent,
    makeText,
    makeImage,
    paintBoxes,
    makeLayoutVertical,
    // makePolygon,
} from 'sdi/print/context';
import { Template, ApplyFn, makeSpec, applySpec } from 'sdi/print/template';
import tr, { formatDate } from 'sdi/locale';

import { getFormatAddress, getXY } from '../../queries/geothermie';
import { renderLeftCloseTable } from '../table/table-closed';

import {
    textGeneralInfo,
    textConstraintHeader,
    textConstraintBody,
} from './text';
import { renderMap } from './map';
import { renderCarrotBox } from './carrot';
import { logoBE } from './logo';

const margin = 15;
const pwidth = 210;

const template: Template = {
    header: makeSpec({
        rect: { x: margin, y: 260, width: pwidth - 2 * margin, height: 15 },
        fontSize: 24,
        color: '#8db63c',
    }),
    title: makeSpec({
        rect: { x: margin, y: 20, width: 180, height: 24 },
        fontSize: 24,
    }),
    subtitle: makeSpec({
        rect: { x: margin, y: 20, width: 180, height: 24 },
        fontSize: 18,
    }),
    logo: makeSpec({
        rect: {
            x: pwidth - margin - 300 / 10,
            y: 20,
            width: 300 / 10,
            height: 136 / 10,
        },
    }),
    'creation-date': makeSpec({
        rect: { x: margin, y: 30, width: 180, height: 24 },
        fontSize: 9,
    }),
    url: makeSpec({
        rect: { x: margin, y: 36, width: 180, height: 24 },
        fontSize: 9,
    }),

    address: makeSpec({
        rect: { x: margin, y: 60, width: 80, height: 12 },
        fontSize: 9,
    }),
    capakey: makeSpec({
        rect: { x: margin, y: 70, width: 80, height: 12 },
        fontSize: 9,
    }),
    coordinates: makeSpec({
        rect: { x: margin, y: 80, width: 80, height: 12 },
        fontSize: 9,
    }),

    map: makeSpec({
        rect: { x: margin + 90, y: 60, width: 90, height: 90 },
    }),

    general: makeSpec({
        rect: { x: margin, y: 160, width: 180, height: 150 },
    }),

    'license-header': makeSpec({
        rect: { x: margin, y: 30, width: 180, height: 100 },
    }),

    'license-body': makeSpec({
        rect: { x: margin, y: 130, width: 180, height: 160 },
    }),

    carrot: makeSpec({
        rect: { x: margin, y: 10, width: pwidth - 2 * margin, height: 260 },
    }),

    separator: makeSpec({
        rect: { x: margin, y: 0, width: pwidth - 2 * margin, height: 48 },
    }),

    footer: makeSpec({
        rect: { x: margin, y: 278, width: 180, height: 24 },
        fontSize: 9,
    }),
};

const renderItem = (f: ApplyFn<Box>, content: string, item: string) =>
    f(item, ({ rect, textAlign, fontSize }) => ({
        ...rect,
        children: [
            makeLayoutVertical(rect.width, rect.height / 2, [
                makeText(content, fontSize, '#006f90', textAlign),
            ]),
        ],
    }));

// const renderSeparator =
//     (f: ApplyFn<Box>, y: number) =>
//         f('separator', ({ rect, color }) => ({
//             ...rect,
//             y,
//             children: [
//                 makeLine([[0, 0], [rect.width, 0]], 0.1, color),
//             ],
//         }));
// const separators = [10, 35, 160, 274];

const renderTitle = (f: ApplyFn<Box>, title: string) =>
    f('title', ({ rect, textAlign, fontSize }) => ({
        ...rect,
        children: [
            makeLayoutVertical(rect.width, rect.height / 2, [
                makeText(title, fontSize, '#006f90', textAlign),
            ]),
        ],
    }));

const renderSubTitle = (f: ApplyFn<Box>, title: string) =>
    f('subtitle', ({ rect, textAlign, fontSize }) => ({
        ...rect,
        children: [
            makeLayoutVertical(rect.width, rect.height / 2, [
                makeText(title, fontSize, '#006f90', textAlign),
            ]),
        ],
    }));

const renderLogo = (f: ApplyFn<Box>) =>
    f('logo', ({ rect }) => ({
        ...rect,
        children: [boxContent(rect, makeImage(logoBE))],
    }));

const renderCreationDate = (f: ApplyFn<Box>) =>
    f('creation-date', ({ rect, fontSize, color }) => ({
        ...rect,
        children: [
            makeText(`Date: ${formatDate(new Date())}`, fontSize, color),
        ],
    }));

const renderURL = (f: ApplyFn<Box>) =>
    f('url', ({ rect, fontSize, color }) => ({
        ...rect,
        children: [
            makeText(
                `URL: ${getXY().fold(
                    '--',
                    xy => 'https://cartegeothermique.be/' + xy.x + '/' + xy.y
                )}`,
                fontSize,
                color
            ),
        ],
    }));

const coordinates = () =>
    getXY().fold('', pos => `${pos.x.toFixed()}, ${pos.y.toFixed()}`);

export const renderPDF = () => {
    const pdf = createContext('portrait', 'a4');
    // page 1
    pdf.setPage(1);
    const boxesPage1: Box[] = [];
    const apply = applySpec(template, 200);
    renderTitle(apply, "Rapport d'information géothermique").map(b =>
        boxesPage1.push(b)
    );
    renderLogo(apply).map(b => boxesPage1.push(b));
    renderCreationDate(apply).map(b => boxesPage1.push(b));
    renderURL(apply).map(b => boxesPage1.push(b));

    renderItem(
        apply,
        `Adresse: ${getFormatAddress().fold('no-adress', a => a)}`,
        'address'
    ).map(b => boxesPage1.push(b));
    renderItem(apply, `Capakey: ~~~todo~~~`, 'capakey').map(b =>
        boxesPage1.push(b)
    );
    renderItem(apply, `Coordonnées: ${coordinates()}`, 'coordinates').map(b =>
        boxesPage1.push(b)
    );
    renderMap(apply).map(b => boxesPage1.push(b));
    renderItem(apply, textGeneralInfo(), 'general').map(b =>
        boxesPage1.push(b)
    );

    // separators.forEach(s => renderSeparator(apply, s)
    //     .map(b => boxes.push(b)));

    paintBoxes(pdf, boxesPage1);

    // page 2
    pdf.addPage();
    pdf.setPage(2);
    const boxesPage2: Box[] = [];

    renderSubTitle(apply, tr.geo('projectSteps')).map(b => boxesPage2.push(b));

    console.log(textConstraintHeader());

    renderItem(apply, textConstraintHeader(), 'license-header').map(b =>
        boxesPage2.push(b)
    );
    renderItem(apply, textConstraintBody(), 'license-body').map(b =>
        boxesPage2.push(b)
    );

    paintBoxes(pdf, boxesPage2);

    // page 3
    pdf.addPage();
    pdf.setPage(3);
    const boxesPage3: Box[] = [];

    renderCarrotBox(apply).map(b => boxesPage3.push(b));
    paintBoxes(pdf, boxesPage3);

    // page 4
    pdf.addPage();
    pdf.setPage(4);
    const boxesPage4: Box[] = [];
    console.log(renderLeftCloseTable());
    // pdf.fromHTML('<table><thead class="close-thead"><tr><th>Remarque</th><th>Unité stratigraphique</th><th class="depth"><span>0</span>Profondeur (m)</th></tr></thead><tbody class="close-tbody"><tr class="closed-tr"><td class="large"></td><td class="large" style="background: rgba(204, 204, 204, 0.4);">US/RBC 11-14 Formations quaternaires indifférenciées</td><td class="depth">-13.5</td></tr><tr style="display: none;"></tr><tr style="display: none;"></tr><tr style="display: none;"></tr><tr style="display: none;"></tr><tr style="display: none;"></tr><tr style="display: none;"></tr><tr style="display: none;"></tr><tr style="display: none;"></tr><tr style="display: none;"></tr><tr style="display: none;"></tr><tr style="display: none;"></tr><tr style="display: none;"></tr><tr class="closed-tr"><td class="large"></td><td class="large" style="background: rgb(198, 208, 232);">US/RBC_72 Sables et argiles de Kortrijk (membre de Moen)</td><td class="depth">-15.4</td></tr><tr class="closed-tr"><td class="large"></td><td class="large" style="background: rgb(171, 184, 220);">US/RBC_73 Argiles de Kortrijk (membre de Saint Maur)</td><td class="depth">-42.0</td></tr><tr class="closed-tr"><td class="large"></td><td class="large" style="background: rgb(255, 150, 80);">US/RBC_81 Sables de Hannut (Membre de Grandglise)</td><td class="depth">-54.2</td></tr><tr class="closed-tr"><td class="large"></td><td class="large" style="background: rgb(222, 134, 77);">US/RBC_82 Argiles de Hannut (Membre de Lincent)</td><td class="depth">-67.8</td></tr><tr class="closed-tr"><td class="large"></td><td class="large" style="background: rgb(159, 205, 112);">US/RBC_91 Craies de Gulpen</td><td class="depth">-73.3</td></tr><tr class="closed-tr"><td class="large"></td><td class="large" style="background: rgb(204, 204, 204);">US/RBC_92 Socle Paléozoïque</td><td class="depth"></td></tr></tbody></table>',100,100,{});

    paintBoxes(pdf, boxesPage4);

    pdf.save('file.pdf');
};
