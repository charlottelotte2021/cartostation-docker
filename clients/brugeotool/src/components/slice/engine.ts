import { DepthPoint } from 'brugeotool/src/remote/io';
import * as debug from 'debug';
import { fromNullable, Option } from 'fp-ts/lib/Option';
import { Coord2D } from 'sdi/map';
import { drawAxis } from './axes';
import { drawBreakLines } from './breaklines';
import {
    CANONICAL_PATTERN_US,
    PIEZO_LAYERS_NAME,
    STRATIGRAPHY_COLORS,
    UH_COLORS,
    UH_COLORS_DARKER,
    UH_FOR_US_ID,
} from '../settings';
import { getCurveControlPoints, Point } from './smooth-curve';
import {
    displayPiezo,
    getDisplayPhreatic,
    getGreyShades,
    getSliceData,
    getDepthScaling,
} from 'brugeotool/src/queries/geothermie';
import { color } from '../carrot/engine';
import { rect } from 'sdi/app';
import { getPattern } from '../patterns';

const logger = debug('sdi:slice/engine');

const DEFAULT_STROKE_COLOR = 'rgba(79, 79, 79, 1)';

import {
    MAX_ELEV_MARGIN,
    GRAPH_LEFT_MARGIN,
    GRAPH_TOP_MARGIN,
    PIEZO_NO_DATA,
} from './constants';
import { makePattern } from 'sdi/map/style/pattern';

const getMaxElevation = (data: DepthPoint[]) =>
    Math.round(Math.max(...data.map(d => d.thickness[0])) / 10) * 10 +
    MAX_ELEV_MARGIN;

const getFillPattern = (
    ctx: CanvasRenderingContext2D,
    patternId: number
): Option<CanvasPattern> => {
    const inverse = new DOMMatrix(
        // 2x scale here comes from that we operate at double the size of the canvas final geometry
        // to trigger browser downscaling
        ctx.getTransform().inverse().scale(2, 2).toString()
    );

    return getPattern(ctx, patternId).map(pattern => {
        pattern.setTransform(inverse);
        return pattern;
    });
};

type curveData = {
    x: number;
    y: number;
    cp1: Point;
    cp2: Point;
    first: boolean;
};

const pointsToCurve = (points: Coord2D[]): curveData[] => {
    const { first, second } = getCurveControlPoints(
        points.map(([x, y]) => ({ x, y }))
    );
    const nullPoint = { x: 0, y: 0 };
    return points.map(([x, y], i) => ({
        x,
        y,
        cp1: i > 0 ? first[i - 1] : nullPoint,
        cp2: i > 0 ? second[i - 1] : nullPoint,
        first: i === 0,
    }));
};

/*
 * Translate thickness value into absolute depth value
 */
const topLayerDepth = (
    elev: number,
    thickness: number[],
    index: number,
    maxElevation: number
) => thickness.slice(0, index).reduce((acc, n) => acc + n, maxElevation - elev);

const coordsToSliceX = (data: DepthPoint[], index: number) =>
    data
        .slice(1, index)
        .map((current, i) => ({
            prev: data[i],
            current,
        }))
        .reduce(
            (acc, { prev, current }) =>
                acc +
                Math.sqrt(
                    (current.x - prev.x) ** 2 + (current.y - prev.y) ** 2
                ),
            0
        );

const withContext = (ctx: CanvasRenderingContext2D) => {
    const drawBandOverlay = (
        ctx: CanvasRenderingContext2D,
        curve: curveData[],
        fillStyle: string | CanvasPattern,
        strokeStyle: string,
        withFill = true,
        withStroke = true,
        lineWidth = 0.8
    ) => {
        ctx.save();
        ctx.fillStyle = fillStyle;

        ctx.strokeStyle = strokeStyle;
        ctx.lineWidth = lineWidth;

        ctx.beginPath();
        curve.map(({ x, y, cp1, cp2, first }) => {
            if (first) {
                ctx.moveTo(x, y);
            } else {
                ctx.bezierCurveTo(cp1.x, cp1.y, cp2.x, cp2.y, x, y);
            }
        });
        const first = curve[0];
        const last = curve[curve.length - 1];
        ctx.lineTo(last.x, 10000); /* 10000 is a far y-axis depth value*/
        ctx.lineTo(first.x, 10000);
        ctx.closePath();
        if (withStroke) {
            ctx.stroke();
        }
        if (withFill) {
            ctx.fill();
        }
        ctx.restore();
    };

    const drawPiezoHead = (
        ctx: CanvasRenderingContext2D,
        curve: curveData[],
        strokeStyle: string,
        lineWidth: number
    ) => {
        ctx.save();

        ctx.strokeStyle = strokeStyle;
        ctx.lineWidth = lineWidth;

        ctx.beginPath();
        curve.map(({ x, y }, i) => {
            if (i > 0) {
                if (Math.abs(curve[i - 1].y - y) > PIEZO_NO_DATA) {
                    ctx.moveTo(x, y);
                } else {
                    ctx.lineTo(x, y);
                }
            }
        });
        ctx.stroke();

        ctx.restore();
    };

    const drawBand = (data: DepthPoint[], index: number) => {
        const maxElevation = getMaxElevation(data);

        const curve = pointsToCurve(
            data.map(({ thickness }, i) => [
                coordsToSliceX(data, i),
                topLayerDepth(
                    thickness[0],
                    thickness.slice(1),
                    index,
                    maxElevation
                ),
            ])
        );

        if (getSliceData() === 'hydro') {
            /* draw grey shades */
            drawBandOverlay(
                ctx,
                curve,
                getGreyShades()[index].left,
                DEFAULT_STROKE_COLOR
            );
            if (index === 0) {
                drawBandOverlay(
                    ctx,
                    curve,
                    getGreyShades()[index].left,
                    'black',
                    true,
                    true,
                    1.8
                );
            }

            if (index > 0) {
                /* do not draw a pattern for 1st layer (quaternaire) */

                /* draw litho patterns */
                getFillPattern(ctx, CANONICAL_PATTERN_US[index]).map(pattern =>
                    drawBandOverlay(ctx, curve, pattern, DEFAULT_STROKE_COLOR)
                );

                /* draw piezo layer overlay */
                const uhName = UH_FOR_US_ID[index];
                const uhPiezoIndex = PIEZO_LAYERS_NAME.indexOf(uhName);
                if (uhPiezoIndex !== -1 && displayPiezo(uhName)) {
                    const colorUH = UH_COLORS[uhPiezoIndex];
                    const piezoPattern = makePattern(4, 45, colorUH);
                    drawBandOverlay(
                        ctx,
                        curve,
                        piezoPattern,
                        colorUH,
                        true,
                        false
                    );
                }
            }

            if (index === 18) {
                /* draw piezo head */
                PIEZO_LAYERS_NAME.slice(1).map((piezoName, pi) => {
                    const piezoCurve = pointsToCurve(
                        data.map(({ piezo }, i) => [
                            coordsToSliceX(data, i),
                            maxElevation -
                                (piezo[pi + 1] < -PIEZO_NO_DATA
                                    ? -PIEZO_NO_DATA
                                    : piezo[pi + 1]),
                        ])
                    );

                    if (displayPiezo(piezoName)) {
                        const colorUH = UH_COLORS_DARKER[pi + 1];
                        //drawPiezoHead(ctx, piezoCurve, color.white, 2 / getDepthScaling());
                        drawPiezoHead(ctx, piezoCurve, colorUH, 1.5 / getDepthScaling());
                    }
                });
                /* draw phreatic head */
                if (getDisplayPhreatic()) {
                    const saturatedCurve = pointsToCurve(
                        data.map(({ phreatic_head }, i) => [
                            coordsToSliceX(data, i),
                            maxElevation - phreatic_head[0],
                        ])
                    );
                    //drawPiezoHead(ctx, saturatedCurve, color.white, 2 / getDepthScaling());
                    drawPiezoHead(ctx, saturatedCurve, color.light_blue, 1.5 / getDepthScaling());
                }
            }
        } else {
            if (index === 0) {
                drawBandOverlay(
                    ctx,
                    curve,
                    STRATIGRAPHY_COLORS[index],
                    'black',
                    true,
                    true,
                    1.4
                );
            } else {
                drawBandOverlay(
                    ctx,
                    curve,
                    STRATIGRAPHY_COLORS[index],
                    DEFAULT_STROKE_COLOR
                );
            }
        }
    };

    const draw = (width: number, height: number, data: DepthPoint[]) => {
        const length = coordsToSliceX(data, data.length);

        const drawingWidth = width - 2 * GRAPH_LEFT_MARGIN;
        const xScale = drawingWidth / length;
        ctx.save();

        ctx.clearRect(0, 0, width, height);
        ctx.translate(GRAPH_LEFT_MARGIN, GRAPH_TOP_MARGIN);
        ctx.scale(xScale, getDepthScaling() * 5);

        for (let i = 0; i < 19; i += 1) {
            drawBand(data, i);
        }
        ctx.restore();

        ctx.save();
        drawAxis(ctx, drawingWidth, height, xScale, getMaxElevation(data));
        ctx.restore();

        ctx.save();
        drawBreakLines(ctx, height, xScale);
        ctx.restore();
    };
    return draw;
};

export const engine = () => {
    const onCanvas = (canvasRef: HTMLCanvasElement) => {
        let width = 0;
        let height = 0;
        const updateSize = () => {
            const rect = canvasRef.getBoundingClientRect();
            width = rect.width * 2;
            height = rect.height * 2;
            canvasRef.width = width;
            canvasRef.height = height;
        };

        updateSize();

        const context = () => fromNullable(canvasRef.getContext('2d'));
        let it: number | null = null;
        let startTime = Date.now();
        const loaderFrameRate = 32;

        const drawLoader = (
            ctx: CanvasRenderingContext2D,
            x: number,
            y: number,
            width: number,
            height: number
        ) => {
            logger(`drawLoader ${width}`);
            ctx.save();
            ctx.resetTransform();
            ctx.fillStyle = '#8db63c';
            ctx.fillRect(x, y, width, height);
            ctx.restore();
        };

        let lastKnownData: DepthPoint[] = [];

        const update = (data: DepthPoint[]) => {
            lastKnownData = data;
            return context().map(ctx => {
                if (it) {
                    window.clearInterval(it);
                    it = null;
                }
                const draw = withContext(ctx);
                draw(width, height, data);
            });
        };

        const load = () =>
            context().map(ctx => {
                if (it === null) {
                    startTime = Date.now();
                    it = window.setInterval(() => {
                        const max =
                            ((Date.now() - startTime) / loaderFrameRate) * 1.9 +
                            50; // this 50 is horizontal scale (hardcoded in style)
                        // logger(
                        //     `max: ${max}; width: ${width}; height: ${height}`
                        // );
                        if (max < width) {
                            drawLoader(ctx, 0, height - 6, max, 6);
                        } else if (it !== null) {
                            window.clearInterval(it);
                            it = null;
                        }
                    }, loaderFrameRate);
                }
            });

        rect(() => {
            if (lastKnownData.length > 0) {
                updateSize();
                update(lastKnownData);
            }
        })(canvasRef);

        return { update, load };
    };

    return { onCanvas };
};

logger('loaded');
