import {
    setDisplayPhreatic,
    setDisplayPiezo,
} from 'brugeotool/src/events/geothermie';
import { GeoMessageKey } from 'brugeotool/src/locale';
import {
    getDisplayPhreatic,
    getDisplayPhreaticOpt,
    getDisplayPiezo,
    getDisplayPiezzoOpt,
    getSliceData,
    getUSName,
} from 'brugeotool/src/queries/geothermie';
import { DIV, H4, IMG } from 'sdi/components/elements';
import {
    PIEZO_LAYERS_NAME,
    STRATIGRAPHY_UNITS,
    UH_COLORS,
    UH_COLORS_DARKER,
} from '../settings';
import { clear, fillStyle, prect, fill, image } from '../sidebar/legend';
import renderLithostratigraphy, {
    renderHydroLegend,
} from '../legend/lithostratigraphy';
import { makeIcon } from 'sdi/components/button';
import tr from 'sdi/locale';
import { patternSample } from 'sdi/map/style/pattern';
import { color } from '../carrot/engine';

const MARGIN = 0.15;

export type DisplayPiezo = {
    'UH/RBC_2': boolean;
    'UH/RBC_4': boolean;
    'UH/RBC_6': boolean;
    'UH/RBC_7b': boolean;
    'UH/RBC_8a': boolean;
};

const renderSquare = (width: number, height: number, color: string) => {
    clear(0, 0, width, height);
    fillStyle(color);

    prect(
        [MARGIN, MARGIN],
        [MARGIN + width, MARGIN],
        [MARGIN + width, MARGIN + height],
        [MARGIN, MARGIN + height]
    );
    fill();

    return IMG({ src: image(0, 0, width, height), alt: '' });
};

const renderItem = (nameKey: GeoMessageKey, color: string) =>
    DIV(
        {
            className: 'legend-item legend-item-slice',
            key: `slice-legend-${nameKey}`,
        },
        DIV('item-style', renderSquare(100, 100, color)),
        getUSName(nameKey)
    );

const btnTogglePiezo = (p: keyof DisplayPiezo) =>
    getDisplayPiezo(p)
        ? makeIcon('toggle-off', 3, 'eye', {
              position: 'top-right',
              text: tr.core('viewLayer'),
          })
        : makeIcon('toggle-on', 3, 'eye-slash', {
              position: 'top-right',
              text: tr.core('hideLayer'),
          });

const renderPiezoPattern = (key: keyof DisplayPiezo) =>
    getDisplayPiezzoOpt(key).map(() =>
        DIV(
            'piezo-image__wrapper',
            DIV(
                'piezo-image',
                patternSample(
                    1,
                    45,
                    UH_COLORS[PIEZO_LAYERS_NAME.indexOf(key)],
                    '#ffffff'
                )(48, 20)
            ),
            DIV('piezo-image__label', tr.geo('slicePiezoLegendLabelImage'))
        )
    );

const renderPiezoLine = (color: string) =>
    DIV(
        'piezo-line__wrapper',
        DIV({ className: 'piezo-line', style: { borderColor: color } }),
        DIV('piezo-line__label', tr.geo('slicePiezoLegendLabelLine'))
    );

const renderPiezo = () =>
    DIV(
        'slice-legend piezo',
        H4('', tr.geo('piezoLevels')),
        renderPhreatic(),
        PIEZO_LAYERS_NAME.slice(1).map(p => {
            const key = p as keyof DisplayPiezo;
            return DIV(
                'slice-legend-item',
                DIV(
                    'toggle__wrapper',
                    btnTogglePiezo(key)(() =>
                        setDisplayPiezo(key, !getDisplayPiezo(key))
                    )
                ),
                DIV(
                    'piezo-infos__wrapper',
                    DIV('label', tr.geo(key)),
                    getDisplayPiezzoOpt(key).map(() =>
                        renderPiezoLine(
                            UH_COLORS_DARKER[PIEZO_LAYERS_NAME.indexOf(key)]
                        )
                    ),
                    renderPiezoPattern(key)
                )
            );
        })
    );

const btnTogglePhreatic = () =>
    getDisplayPhreatic()
        ? makeIcon('toggle-off', 3, 'eye', {
              position: 'top-right',
              text: tr.core('viewLayer'),
          })
        : makeIcon('toggle-on', 3, 'eye-slash', {
              position: 'top-right',
              text: tr.core('hideLayer'),
          });

const renderPhreatic = () =>
    DIV(
        'slice-legend-item phreatic',
        DIV(
            'toggle__wrapper',
            btnTogglePhreatic()(() => setDisplayPhreatic(!getDisplayPhreatic()))
        ),
        DIV(
            'piezo-infos__wrapper',
            DIV('label', tr.geo('phreaticLevel')),
            getDisplayPhreaticOpt().map(() => renderPiezoLine(color.light_blue))
        )
    );

export const renderLegend = () =>
    getSliceData() === 'hydro'
        ? DIV(
              'legend-slice',
              renderPiezo(),
              H4('', tr.geo('lithoStratiLegend')),
              renderLithostratigraphy(),
              H4('', tr.geo('hydroLegend')),
              renderHydroLegend()
          )
        : DIV(
              'legend-slice',
              STRATIGRAPHY_UNITS.map(({ id, color }) => renderItem(id, color))
          );

export default renderLegend;
