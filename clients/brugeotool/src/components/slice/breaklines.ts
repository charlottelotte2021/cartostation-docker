import { getSliceGeometrySegments } from 'brugeotool/src/queries/geothermie';
import { fromNullable } from 'fp-ts/lib/Option';
import { iife } from 'sdi/lib';
import { GRAPH_LEFT_MARGIN, GRAPH_TOP_MARGIN } from './constants';

const sliceIconImageSize = 28;
export const sliceIconImage = iife(() => {
    const canvas = document.createElement('canvas');
    canvas.width = sliceIconImageSize;
    canvas.height = sliceIconImageSize;
    fromNullable(canvas.getContext('2d')).map(ctx => {
        ctx.translate(0, sliceIconImageSize);
        ctx.scale(1, -1);
        ctx.clearRect(0, 0, sliceIconImageSize, sliceIconImageSize);
        const all = sliceIconImageSize;
        const mid = all / 2;
        const thi = all / 5;
        ctx.beginPath();
        ctx.moveTo(thi, 0);
        ctx.lineTo(mid, thi);
        ctx.lineTo(all - thi, 0);
        ctx.lineTo(all - thi, all - thi);
        ctx.lineTo(mid, all);
        ctx.lineTo(thi, all - thi);
        ctx.closePath();

        ctx.fillStyle = '#75b626';
        ctx.fill();
    });

    return canvas;
});

const drawMarker = (
    ctx: CanvasRenderingContext2D,
    index: number,
    x: number
) => {
    const letter = String.fromCodePoint(index + 65);
    const bg = sliceIconImage;
    const adjX = x - bg.width;
    ctx.drawImage(bg, adjX, GRAPH_TOP_MARGIN + 10, bg.width * 2, bg.height * 2);

    ctx.arc(x, GRAPH_TOP_MARGIN, 10, 0, 2 * Math.PI);
    ctx.fillStyle = '#75b626';
    ctx.fill();

    ctx.font = `bold 30px monospace`;
    ctx.fillStyle = 'white';
    ctx.fillText(letter, x - 10, GRAPH_TOP_MARGIN + bg.height * 1.6);
};

const drawXLine = (
    ctx: CanvasRenderingContext2D,
    x: number,
    y: number,
    height: number
) => {
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.lineTo(x, y + height);
    ctx.closePath();
    // ctx.setLineDash([5, 15]);
    // ctx.strokeStyle = 'rgba(50, 50, 50, 1)';
    ctx.lineWidth = 4;
    ctx.strokeStyle = '#75b626';
    ctx.stroke();
};

export const drawBreakLines = (
    ctx: CanvasRenderingContext2D,
    heightPx: number,
    xScaling: number
) => {
    ctx.translate(GRAPH_LEFT_MARGIN, 0);
    let dist = 0;
    drawXLine(ctx, dist, 32, heightPx);
    drawMarker(ctx, 0, dist);
    getSliceGeometrySegments().map(segments => {
        // segments.slice(0, -1).map((s, i) => {
        segments.map((s, i) => {
            dist = dist + s * xScaling;
            drawXLine(ctx, dist, GRAPH_TOP_MARGIN, heightPx);
            drawMarker(ctx, i + 1, dist);
        });
    });
};
