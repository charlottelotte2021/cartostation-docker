import { CANVAS, DIV } from 'sdi/components/elements';
import { isNotNullNorUndefined, Nullable } from 'sdi/util';
import { engine } from './engine';
import { none, some, Option } from 'fp-ts/lib/Option';
import {
    getSlice,
    getSliceData,
    getSliceGeometry,
} from 'brugeotool/src/queries/geothermie';
import tr from 'sdi/locale';
import { renderRadioOut } from 'sdi/components/input';
import {
    clearBottomRender,
    endSlicing,
    setSliceData,
} from 'brugeotool/src/events/geothermie';
import {
    closeBottomRenderButton,
    depthScalingWidget,
    toggleFullScreenPanelButton,
} from '../button';
import { setInteractionSelect } from 'brugeotool/src/events/map';
// import { getTopZoom } from 'brugeotool/src/queries/map';

const { onCanvas } = engine();

let updater: Option<() => void> = none;

export type SliceData = 'geol' | 'hydro';

const attachEngine = (node: Nullable<HTMLCanvasElement>) => {
    if (isNotNullNorUndefined(node)) {
        const { update, load } = onCanvas(node);
        updater = some(() => {
            getSlice().foldL(load, update);
        });
        // rectify(node);
    } else {
        updater = none;
    }
};

const renderSliceDataSwitch = (a: SliceData) => {
    switch (a) {
        default:
            return tr.geo('geology');
        case 'hydro':
            return tr.geo('hydrogeology');
    }
};

const sliceDataSwitch = renderRadioOut(
    'show-hydrogeology-toggle',
    renderSliceDataSwitch,
    setSliceData
);

const renderSliceTypeToggle = () =>
    DIV(
        'select-hydro-geol__widget',
        sliceDataSwitch(['geol', 'hydro'], getSliceData())
    );

const displaySliceButtons = () =>
    DIV(
        'display--slice',
        toggleFullScreenPanelButton(),
        closeBottomRenderButton(() => {
            endSlicing();
            clearBottomRender();
            setInteractionSelect();
        })
    );

const renderSettingsButtons = () =>
    DIV(
        'slice-settings__buttons',
        renderSliceTypeToggle(),
        // depthScalingWidget(),
        displaySliceButtons()
    );

const renderSliceWithGeometry = () => {
    updater.map(u => window.setTimeout(u, 16));
    return DIV(
        {
            className: 'map-wrapper geo-slice',
            style: {
                position: 'absolute',
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
            },
        },
        renderSettingsButtons(),

        depthScalingWidget(),
        CANVAS({
            ref: attachEngine,
            style: {
                width: '100%',
                height: '100%',
            },
        })
    );
};

const renderSliceWithoutGeometry = () =>
    DIV('map-wrapper geo-slice no-geom', '~TODO');

// const display2DNotVisible = () =>
//     DIV(
//         'slice-message',
//         DIV(
//             'display--3D',
//             closeBottomRenderButton(() => {
//                 endSlicing();
//                 clearBottomRender();
//                 setInteractionSelect();
//             })
//         ),
//         tr.geo('sliceMessageNotVisible')
//     );

// export const renderSlice = () =>
//     getTopZoom() < 10
//         ? getSliceGeometry()
//               .map(renderSliceWithGeometry)
//               .getOrElseL(renderSliceWithoutGeometry)
//         : display2DNotVisible();

export const renderSlice = () =>
    getSliceGeometry()
        .map(renderSliceWithGeometry)
        .getOrElseL(renderSliceWithoutGeometry);

export default renderSlice;
