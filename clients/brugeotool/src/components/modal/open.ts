import { DIV, A } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { renderSelect } from 'sdi/components/input';
import { getLang } from 'sdi/app';

import {
    getInputUH,
    getHydroLayerIndex,
    getKMean,
    getHydroLayerDepth,
    getHydroLayersThicknesses,
    getPiezoValue,
} from '../../queries/geothermie';
import { UH_SYSTEM, getSmartGOThermBaseUrl } from '../settings';
import { setInputUH } from '../../events/geothermie';
import { GeoMessageKey } from 'brugeotool/src/locale';
import { encodeUrlQuery } from 'sdi/util';
import { setoidString } from 'fp-ts/lib/Setoid';

const getAqType = () =>
    /*
     * aqType	str: conf or freat	type of aquifer (confined or freatic)
     */
    getInputUH().fold('freat', uh => UH_SYSTEM[getHydroLayerIndex(uh)]);

const getAqPerm = () =>
    /*
     * aqPerm	kmoy (conductivité hydraulique moyenne) de l’UH sélectionnée
     */
    getInputUH().fold(
        0,
        uh => parseFloat(getKMean()[getHydroLayerIndex(uh)]) * 86400
    );

const getAqTop = () =>
    /*
     * aqTop = altitude du toit de l’UH sélectionné
     */
    getInputUH().fold(0, uh => Number(getHydroLayerDepth(uh).toFixed(2)));

const getAqBot = () =>
    /*
     * aqBot = altitude de la base de l’UH sélectionné
     */
    getInputUH().fold(0, uh =>
        Number(
            (
                getAqTop() + getHydroLayersThicknesses()[getHydroLayerIndex(uh)]
            ).toFixed(2)
        )
    );

const getAqS = () =>
    /*
     * aqS = niveau piézométrique de l’UH sélectionnée
     */
    getInputUH().fold(
        0,
        uh =>
            -1 *
            Number(
                getPiezoValue(uh)
                    .fold(0, p => (p < 0 ? p : 0))
                    .toFixed(2)
            )
    );

const getAqSth = () =>
    /*
     * aqSth = épaisseur saturée de l’UH sélectionnée, mais min. 0 et max. epaisseur uh
     */
    Math.max(
        ...[Math.min(...[getAqBot() - getAqS(), getAqBot() - getAqTop()]), 0]
    ).toFixed(1);

const getLinkOpen = () =>
    getInputUH().fold(
        `${getSmartGOThermBaseUrl('open')}?v=1&lang=${getLang()}`,
        uh =>
            uh === 'otherUH'
                ? `${getSmartGOThermBaseUrl('open')}?v=1&lang=${getLang()}`
                : `${getSmartGOThermBaseUrl('open')}?${encodeUrlQuery({
                      aqType: getAqType(),
                      aqPerm: getAqPerm(),
                      aqTop: getAqTop(),
                      aqBot: getAqBot(),
                      aqS: getAqS(),
                      aqSth: getAqSth(),
                      v: 1,
                      lang: getLang(),
                  })}`
    );

export const renderLinkOpen = () =>
    DIV(
        { className: 'geo-btn geo-btn--1' },
        A(
            { href: getLinkOpen(), target: '_blank' },
            tr.geo('goToSmartGeotherm')
        )
    );

const renderHelpTextFormOpen = () =>
    DIV({ className: 'helptext' }, tr.geo('helptext:modalSmartOpen'));

const renderHelpTextFormOpen2 = () =>
    DIV({ className: 'helptext' }, tr.geo('helptext:modalSmartOpen2'));

const renderUH = (uh: GeoMessageKey) =>
    uh === 'otherUH' ? `${tr.geo(uh)}` : `${tr.geo(uh)} (${uh})`;
// DIV(
//     {
//         className: '',
//         key: `user-input-${uh}`
//     },
//     uh === 'otherUH' ? `${tr.geo(uh)}` : `${tr.geo(uh)} (${uh})`
// );

const renderSelectUH = renderSelect<GeoMessageKey>(
    'select UH',
    renderUH,
    setInputUH,
    setoidString
);

const makeSelectUH = (): GeoMessageKey[] => {
    let arr: GeoMessageKey[] = [];
    if (getHydroLayersThicknesses()[getHydroLayerIndex('UH/RBC_4')] > 0) {
        arr.push('UH/RBC_4');
    }
    if (getHydroLayersThicknesses()[getHydroLayerIndex('UH/RBC_8a')] > 0) {
        arr.push('UH/RBC_8a');
    }
    arr.push('otherUH');
    return arr;
};

export const renderFormOpen = () =>
    DIV(
        {},
        renderHelpTextFormOpen(),
        DIV({}, renderSelectUH(makeSelectUH(), getInputUH())),
        renderHelpTextFormOpen2()
    );
