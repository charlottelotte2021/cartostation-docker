import * as io from 'io-ts';

export const DepthPointThicknessIO = io.interface({
    x: io.Integer,
    y: io.Integer,
    thickness: io.array(io.number),
    nodata: io.boolean,
});
export type DepthPointThickness = io.TypeOf<typeof DepthPointThicknessIO>;

export const DepthPointIO = io.intersection(
    [
        DepthPointThicknessIO,
        io.interface({
            quat_thickness: io.array(io.number),
            saturation_rate: io.array(io.number),
            phreatic_head: io.array(io.number),
            piezo: io.array(io.number),
        }),
    ],
    'DepthPointIO'
);

export type DepthPoint = io.TypeOf<typeof DepthPointIO>;

export const SliceIO = io.array(DepthPointIO);

export type Slice = io.TypeOf<typeof SliceIO>;

// type RegionIO = io.literal('in')

export const ConstraintPointIO = io.interface({
    x: io.Integer,
    y: io.Integer,
    constraints: io.interface({
        natura: io.boolean,
        water: io.boolean,
        soil: io.string,
        region: io.string,
        metro_nord: io.boolean,
    }),
});

export type ConstraintPoint = io.TypeOf<typeof ConstraintPointIO>;

export const DepthRowIO = io.array(DepthPointThicknessIO);
export const DepthArrayIO = io.array(DepthRowIO);

export type DepthRow = io.TypeOf<typeof DepthRowIO>;
export type DepthArray = io.TypeOf<typeof DepthArrayIO>;

export const DepthTileIO = io.interface({
    x: io.Integer,
    y: io.Integer,
    size: io.Integer,
    data: DepthArrayIO,
});

export type DepthTile = io.TypeOf<typeof DepthTileIO>;

export const MapPanelIO = io.union(
    [io.literal('top'), io.literal('bot')],
    'MapPanelIO'
);

export const MapSelectionIO = io.interface(
    {
        id: io.Integer,
        map: io.string,
        sort: io.Integer,
        panel: MapPanelIO,
    },
    'MapSelectionIO'
);

export const MapSelectionListIO = io.array(
    MapSelectionIO,
    'MapSelectionListIO'
);

export type MapPanel = io.TypeOf<typeof MapPanelIO>;
export type MapSelection = io.TypeOf<typeof MapSelectionIO>;
export type MapSelectionList = io.TypeOf<typeof MapSelectionListIO>;
