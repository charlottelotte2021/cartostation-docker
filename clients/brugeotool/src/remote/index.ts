// import * as io from 'io-ts';

import {
    fetchIO,
    IMapInfoIO,
    InspireIO,
    FeatureCollection,
    fetchWithoutValidationIO,
    FeatureCollectionIO,
    Attachment,
    AttachmentIO,
    IServiceBaseLayersIO,
    postUnrelatedIO,
    LineString,
} from 'sdi/source';
import { getApiUrl } from 'sdi/app';

import {
    DepthPointIO,
    DepthTileIO,
    ConstraintPointIO,
    MapSelectionListIO,
    SliceIO,
    Slice,
} from './io';
import { CakeRes } from '../components/cake';

export const fetchPoint = (x: number, y: number) =>
    fetchIO(DepthPointIO, getApiUrl(`geodata/geothermie/point/${x}/${y}/`));

export const fetchConstraint = (x: number, y: number) =>
    fetchIO(
        ConstraintPointIO,
        getApiUrl(`geodata/geothermie/constraint/${x}/${y}/`)
    );

export const fetchTile = (
    base: CakeRes,
    minx: number,
    miny: number,
    maxx: number,
    maxy: number
) =>
    fetchIO(
        DepthTileIO,
        getApiUrl(
            `geodata/geothermie/tile/${base}/${minx}/${miny}/${maxx}/${maxy}/`
        )
    );

export const fetchMap = (mid: string) =>
    fetchIO(IMapInfoIO, getApiUrl(`maps/${mid}`));

export const fetchBaseLayerAll = () =>
    fetchIO(IServiceBaseLayersIO, getApiUrl(`wmsconfig/`));

export const fetchMetadata = (id: string) =>
    fetchIO(InspireIO, getApiUrl(`metadatas/${id}`));

export const fetchLayer = (url: string): Promise<FeatureCollection> =>
    fetchWithoutValidationIO(url);

export const fetchMapSelection = () =>
    fetchIO(MapSelectionListIO, getApiUrl('geodata/geothermie/maps/'));

export const fetchCapakeyCapakey = (capakey: string) =>
    fetchIO(
        FeatureCollectionIO,
        getApiUrl(`geodata/geothermie/capakey/${capakey.replace('/', '-')}`)
    );

export const fetchCapakeyPosition = (x: number, y: number) =>
    fetchIO(
        FeatureCollectionIO,
        getApiUrl(`geodata/geothermie/capakey/${x}/${y}`)
    );

export const fetchAttachment = (id: string): Promise<Attachment> =>
    fetchIO(AttachmentIO, getApiUrl(`attachments/${id}`));

export const fetchSlice = (base: number, geom: LineString) =>
    postUnrelatedIO<Slice, LineString>(
        SliceIO,
        getApiUrl(`geodata/geothermie/multi-points-line/${Math.round(base)}/`),
        geom
    );
