import { Layout } from '../events/route';
import {
    IMapInfo,
    Inspire,
    FeatureCollection,
    Attachment,
    IServiceBaseLayers,
} from 'sdi/source';
import { Collection, Nullable } from 'sdi/util';
import { MapSelectionList } from '../remote/io';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'app/layout': Layout;
        'app/layout/persistent': Layout;
        'app/main/rect': Nullable<[number, number]>;

        'data/selected-maps': MapSelectionList;
        'data/maps': IMapInfo[];
        'data/metadatas': Inspire[];
        'data/layers': Collection<FeatureCollection>;
        'data/baselayers': IServiceBaseLayers;
        'data/attachments': Attachment[];
    }
}

export const defaultAppShape = () => ({
    'app/layout': 'home' as Layout,
    'app/layout/persistent': 'home' as Layout,
    'app/main/rect': null,

    'data/selected-maps': [],
    'data/maps': [],
    'data/metadatas': [],
    'data/layers': {},
    'data/baselayers': [],
    'data/attachments': [],
});
