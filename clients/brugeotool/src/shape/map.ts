import { Nullable } from 'sdi/util';
import { IUgWsResponse } from 'sdi/ports/geocoder';
import {
    IMapViewData,
    Interaction,
    IMapScale,
    FeaturePath,
    IViewEvent,
    defaultInteraction,
    PrintRequest,
    PrintResponse,
    defaultPrintRequest,
    defaultPrintResponse,
} from 'sdi/map';
import { Coordinate } from 'ol/coordinate';
import { MessageRecord } from 'sdi/source';

declare module 'sdi/shape' {
    export interface IShape {
        'component/geocoder/response': Nullable<IUgWsResponse>;
        'component/geocoder/input': Nullable<string>;

        'port/map/top/view': IMapViewData;
        'port/map/bottom/view': IMapViewData;
        'port/map/interaction': Interaction;
        'port/map/scale': IMapScale;
        'port/map/select': Nullable<FeaturePath>;
        'port/map/printRequest': PrintRequest<{} | null>;
        'port/map/printResponse': PrintResponse<{} | null>;
        'port/map/loading': MessageRecord[];
    }
}

const DEFAULT_ZOOM = 6;
const DEFAULT_CENTER: Coordinate = [148885, 170690];

export const defaultMapView = (): IMapViewData => ({
    dirty: 'geo',
    srs: 'EPSG:31370',
    center: DEFAULT_CENTER,
    rotation: 0,
    zoom: DEFAULT_ZOOM,
    feature: null,
    extent: null,
});

export const defaultMapEvent = (): IViewEvent => ({
    dirty: 'geo',
    center: DEFAULT_CENTER,
    rotation: 0,
    zoom: DEFAULT_ZOOM,
});

export const defaultMapShape = () => ({
    'component/geocoder/response': null,
    'component/geocoder/input': null,

    'port/map/scale': { count: 0, unit: '', width: 0 },
    'port/map/top/view': defaultMapView(),
    'port/map/bottom/view': defaultMapView(),
    'port/map/interaction': defaultInteraction(),
    'port/map/select': null,
    'port/map/printRequest': defaultPrintRequest(),
    'port/map/printResponse': defaultPrintResponse(),
    'port/map/loading': [],
});
