import { fromNullable, some, none, Option } from 'fp-ts/lib/Option';
import { Either, right } from 'fp-ts/lib/Either';
import { flatten } from 'fp-ts/lib/Array';
import { identity } from 'fp-ts/lib/function';
import * as proj4 from 'proj4';
import Style from 'ol/style/Style';
import { query, queryK } from 'sdi/shape';
import { tileKeyFromExtent } from '../events/geothermie';
import { iife, scopeOption } from 'sdi/lib';
import { PrintRequest, IGeoMeasureType } from 'sdi/map';
import { Coordinate } from 'ol/coordinate';
import { getArchiExtent, getSliceGeometry } from './geothermie';
import { FeatureCollection, ILayerInfo, Inspire } from 'sdi/source';
import { SyntheticLayerInfo } from 'sdi/app';
import Stroke from 'ol/style/Stroke';
import CircleStyle from 'ol/style/Circle';
import Fill from 'ol/style/Fill';
import TextPlacement from 'ol/style/TextPlacement';
import Text from 'ol/style/Text';
import Icon from 'ol/style/Icon';
import Point from 'ol/geom/Point';

export const topMapName = 'geo-map-top';
export const bottomMapName = 'geo-map-bottom';
export type MapName = typeof topMapName | typeof bottomMapName;

export const getGeocoderResponse = () =>
    fromNullable(query('component/geocoder/response'));

// interface GeocoderInput {
//     addr: string;
//     lang: MessageRecordLang;
// }

export const getGeocoderInput = () =>
    fromNullable(query('component/geocoder/input')).chain(input =>
        input.trim().length > 0 ? some(input) : none
    );

export const findMap = (mid: string) =>
    fromNullable(query('data/maps').find(m => m.id === mid));

export const findMetadata = (mid: string) =>
    fromNullable(query('data/metadatas').find(m => m.id === mid));

export const getCurrentMapId = (name: MapName) => {
    switch (name) {
        case bottomMapName:
            return fromNullable(query('geo/map/bottom'));
        case topMapName:
            return fromNullable(query('geo/map/top'));
    }
};

export const getCurrentMapInfo = (name: MapName) => {
    switch (name) {
        case bottomMapName:
            return fromNullable(query('geo/map/bottom')).chain(findMap);
        case topMapName:
            return fromNullable(query('geo/map/top')).chain(findMap);
    }
};

export const getInteraction = queryK('port/map/interaction');

export const getInteractionMode = () => getInteraction().label;

export const getMeasuredGeometryType = () => {
    const it = getInteraction();
    if (it.label === 'measure') {
        return some(it.state.geometryType);
    }
    return none;
};

export const isMeasurSelected = (t: IGeoMeasureType) =>
    getMeasuredGeometryType()
        .map(gt => gt === t)
        .getOrElse(false);

// export const getBaseLayerNameList = () => Object.keys(query('data/baselayers'));
export const getBaseLayerNameList = () =>
    flatten(
        query('data/baselayers').map(service =>
            service.layers.map(l => `${service.id}/${l.codename}`)
        )
    );

export const getCurrentBaseLayer = (panel: MapName) =>
    getCurrentMapInfo(panel).map(info => info.baseLayer);

export const findBaseLayer =
    // (name: string) => fromNullable(query('data/baselayers')[name]);
    (name: string) => {
        const [serviceId, layerName] = name.split('/');
        return fromNullable(
            query('data/baselayers').find(s => s.id === serviceId)
        ).chain(s =>
            fromNullable(s.layers.find(l => l.codename === layerName))
        );
    };

export const getBaseLayer = (panel: MapName) =>
    getCurrentMapInfo(panel).chain(map => findBaseLayer(map.baseLayer));

export const getView = (name: MapName) => {
    switch (name) {
        case topMapName:
            return queryK('port/map/top/view');
        case bottomMapName:
            return queryK('port/map/bottom/view');
    }
};

export const getTopView = getView(topMapName);

export const getTopZoom = () => getTopView().zoom;

export const getCenter = () => getTopView().center;

const lambert72 =
    '+ proj=lcc +lat_1=51.16666723333333 + lat_2=49.8333339 + lat_0=90 + lon_0=4.367486666666666 + x_0=150000.013 + y_0=5400088.438 + ellps=intl + towgs84=-106.869, 52.2978, -103.724, 0.3366, -0.457, 1.8422, -1.2747 + units=m + no_defs';
const lambert2008 =
    '+ proj=lcc +lat_1=49.83333333333334 + lat_2=51.16666666666666 + lat_0=50.797815 + lon_0=4.359215833333333 + x_0=649328 + y_0=665262 + ellps=GRS80 + towgs84=0, 0, 0, 0, 0, 0, 0 + units=m + no_defs';

const { forward } = proj4(lambert72, lambert2008);

export const getCenterAsLambert2008 = () => forward(getCenter());

export const getTile = () =>
    getArchiExtent()
        .map(tileKeyFromExtent)
        .chain(key => fromNullable(query('geo/tiles')[key]));

export const getScaleLine = queryK('port/map/scale');

export const getSelectedFeatureOpt = () =>
    fromNullable(query('port/map/select'));

export const getSelectedFeature = () =>
    getSelectedFeatureOpt().getOrElse({ layerId: null, featureId: null });

const findLayer = (lid: string) =>
    fromNullable(
        flatten(query('data/maps').map(m => m.layers)).find(i => i.id === lid)
    )
        .chain(l =>
            fromNullable(
                query('data/metadatas').find(m => m.id === l.metadataId)
            )
        )
        .chain(m =>
            fromNullable(query('data/layers')[m.uniqueResourceIdentifier])
        );

export const findSelectedFeature = () =>
    scopeOption()
        .let('path', getSelectedFeatureOpt())
        .let('layerId', ({ path }) => fromNullable(path.layerId))
        .let('featureId', ({ path }) => fromNullable(path.featureId))
        .let('layer', ({ layerId }) => findLayer(layerId))
        .let('feature', ({ featureId, layer }) =>
            fromNullable(layer.features.find(f => f.id === featureId))
        )
        .pick('feature');

export const groupVisibility = (gid: string) =>
    fromNullable(query('geo/legend/group/visibility')[gid]).foldL(
        () =>
            query('data/maps').reduce((acc, { layers }) => {
                if (acc) return acc;
                for (let i = 0; i < layers.length; i += 1) {
                    const group = layers[i].group;
                    if (
                        group !== null &&
                        group.id === gid &&
                        layers[i].visible
                    ) {
                        return true;
                    }
                }
                return acc;
            }, false),
        identity
    );

export const groupFolding = (gid: string) =>
    fromNullable(query('geo/legend/group/folding')[gid]);

export const groupBulleVisibility = (gid: string) =>
    fromNullable(query('geo/legend/group/bulle')[gid]);

export const getLayerInfo = (lid: string) =>
    fromNullable(query('geo/legend/layer/info')[lid]);

export const getPrintResponse = queryK('port/map/printResponse');
export const getPrintRequest = (): PrintRequest<Record<string, unknown>> => {
    const r = query('port/map/printRequest');
    return { ...r, props: {} };
};

export const getPositionMarker = () =>
    fromNullable(query('geo/map/marker')).map(([x, y]) => [x, y] as Coordinate);

export const getExtent = () => fromNullable(getTopView().extent);

export const getLoading = () => {
    const ls = query('port/map/loading');
    return ls;
};

export const getLayerData = (
    ressourceId: string
): Either<string, Option<FeatureCollection>> => {
    const layers = query('data/layers');
    if (ressourceId in layers) {
        return right(some<FeatureCollection>(layers[ressourceId]));
    }
    return right(none);
};

export const SLICE_LAYER_NAME = '__geo_slice__';

export const sliceInfo: ILayerInfo = {
    id: SLICE_LAYER_NAME,
    legend: null,
    group: null,
    metadataId: SLICE_LAYER_NAME,
    featureViewOptions: { type: 'default' },
    layerInfoExtra: null,
    visible: true,
    visibleLegend: true,
    style: {
        kind: 'line-simple',
        dash: [],
        strokeColor: 'white',
        strokeWidth: 0.5,
    },
    opacitySelector: false,
};

export const sliceMetadata: Inspire = {
    id: SLICE_LAYER_NAME,
    geometryType: 'LineString',
    resourceTitle: { fr: SLICE_LAYER_NAME, nl: SLICE_LAYER_NAME },
    resourceAbstract: { fr: SLICE_LAYER_NAME, nl: SLICE_LAYER_NAME },
    uniqueResourceIdentifier: SLICE_LAYER_NAME,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: 'NOW', revision: 'NOW' },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: 'NOW',
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
};

export const sliceSyntheticInfo = (): Option<SyntheticLayerInfo> =>
    some({
        name: {},
        info: sliceInfo,
        metadata: sliceMetadata,
    });

const sliceIconImageSize = 28;
export const sliceIconImage = iife(() => {
    const canvas = document.createElement('canvas');
    canvas.width = sliceIconImageSize;
    canvas.height = sliceIconImageSize;
    fromNullable(canvas.getContext('2d')).map(ctx => {
        ctx.clearRect(0, 0, sliceIconImageSize, sliceIconImageSize);
        const all = sliceIconImageSize;
        const mid = all / 2;
        const thi = all / 5;
        ctx.beginPath();
        ctx.moveTo(thi, 0);
        ctx.lineTo(mid, thi);
        ctx.lineTo(all - thi, 0);
        ctx.lineTo(all - thi, all - thi);
        ctx.lineTo(mid, all);
        ctx.lineTo(thi, all - thi);
        ctx.closePath();

        ctx.fillStyle = '#75b626';
        ctx.fill();
    });

    return canvas;
});

export const sliceInteractionStyle = (): /* feature: RenderFeature */
Style | Style[] => {
    const styles = [
        new Style({
            stroke: new Stroke({
                color: '#096077',
                width: 3,
            }),
        }),
    ];
    getSliceGeometry().map(({ coordinates }) =>
        coordinates.forEach((point, index) => {
            const text = new Text({
                text: String.fromCodePoint(index + 65),
                placement: TextPlacement.POINT,
                overflow: true,
                font: `bold 15px monospace`,
                textAlign: 'center',
                textBaseline: 'bottom',
                offsetX: 0,
                offsetY: -8,
                fill: new Fill({
                    color: 'white',
                }),
                // stroke: new Stroke({
                //     width: 1,
                //     color: 'white',
                // }),
            });
            styles.push(
                new Style({
                    image: new Icon({
                        img: sliceIconImage,
                        imgSize: [sliceIconImageSize, sliceIconImageSize],
                        displacement: [0, 20],
                    }),
                    geometry: new Point(point),
                })
            );
            styles.push(
                new Style({
                    text,
                    image: new CircleStyle({
                        radius: 6,
                        fill: new Fill({
                            color: '#75b626',
                        }),
                    }),
                    geometry: new Point(point),
                })
            );
        })
    );
    return styles;
};
