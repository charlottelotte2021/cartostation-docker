import { fromNullable } from 'fp-ts/lib/Option';

import { query } from 'sdi/shape';
import { MapPanel } from '../remote/io';
import { findMap } from './map';
import { fromRecord } from 'sdi/locale';

export const getLayout = () => query('app/layout');

export const getPersistentLayout = () => {
    const pl = query('app/layout/persistent');
    switch (pl) {
        case 'home':
        case 'print':
            return getLayout();
        default:
            return pl;
    }
};

export const getSelectedMaps = (panel: MapPanel) =>
    query('data/selected-maps')
        .filter(s => s.panel === panel)
        .sort((a, b) => a.sort - b.sort);

export const getMapInfo = (id: string) =>
    fromNullable(query('data/maps').find(m => m.id === id));

export const getDatasetMetadata = (id: string) =>
    fromNullable(query('data/metadatas').find(md => md.id === id));

export const getMapName = (mid: string) =>
    findMap(mid).map(m => fromRecord(m.title));

export const getOrientation = (): 'vertical' | 'horizontal' =>
    fromNullable(query('app/main/rect'))
        .map(([width, height]) => (height > width ? 'vertical' : 'horizontal'))
        .getOrElse('horizontal');

export const getAttachment = (id: string) =>
    fromNullable(query('data/attachments').find(a => a.id === id));

export const findInfoBulle = (groupId: string) =>
    fromNullable(query('geo/bulles').find(b => b.group === groupId));
