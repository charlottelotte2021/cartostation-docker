import { MessageStore, formatMessage, Parameters } from 'sdi/locale';
import * as texts from './marked-texts';

const messages = {
    __empty__: {
        fr: '',
        nl: '',
        en: '',
    },

    individualInstallation: {
        fr: 'Installation individuelle',
        nl: 'Individuele installatie', // nl to check
        en: 'Individual installation',
    },

    loadingData: {
        fr: 'Chargement des données',
        nl: 'Data wordt geladen',
        en: 'Loading datas',
    },

    solLegendConsRank1: {
        fr: 'Petit consommateur (studio/appartement avec éclairage, réfrigérateur etc.)',
        nl: 'Kleinverbruiker (studio/appartement met verlichting, koelkast enz.)',
        en: 'Small consumer (studio/apartment with lighting, refrigerator etc.)',
    },

    solLegendConsRank2: {
        fr: 'Petite famille (avec machine à laver/lave-vaisselle)',
        nl: 'Klein gezin (met wasmachine/vaatwasser)',
        en: 'Small family (with washing machine/dishwasher)',
    },

    solLegendConsRank3: {
        fr: 'Consommateur médian',
        nl: 'Mediaan verbruiker',
        en: 'Median consumer',
    },

    solLegendConsRank4: {
        fr: 'Ménage moyen',
        nl: 'Gemiddeld huishouden',
        en: 'Average household',
    },

    solLegendConsRank5: {
        fr: 'Gros consommateur',
        nl: 'Grootverbruiker',
        en: ' Large consumer',
    },

    solLegendConsWaterRank1: {
        fr: 'Personne seule ou un ménage très économe (douches rapides plutôt que bains).',
        nl: 'Één persoon of een klein en heel zuinig huishouden (snelle douches in plaats van baden).',
        en: 'Single person or a very economical household (quick showers rather than baths).',
    },

    solLegendConsWaterRank2: {
        fr: 'Petit ménage économe (douches rapides plutôt que bains).',
        nl: 'Klein en zuinig huishouden  (snelle douches in plaats van baden).',
        en: 'Small, economical household (quick showers instead of baths).',
    },

    solLegendConsWaterRank3: {
        fr: 'Famille petite, ou une moyenne et économe (douches rapides plutôt que bains).',
        nl: 'Middelgroot en zuinig huishouden  (snelle douches in plaats van baden).',
        en: 'Small or medium and economical family (quick showers rather than baths).',
    },

    solLegendConsWaterRank4: {
        fr: 'Famille moyenne, ou une grande et économe (douches rapides plutôt que bains).',
        nl: 'Groot en zuinig huishouden (snelle douches in plaats van baden).',
        en: 'Average family or a large and economical family (quick showers rather than baths).',
    },

    solLegendConsWaterRank5: {
        fr: 'Grande famille.',
        nl: 'Groot huishouden.',
        en: 'Big family.',
    },

    solLegendConsWaterRank6: {
        fr: 'Très grande famille.',
        nl: 'Heel groot huishouden.',
        en: 'Very big family.',
    },

    resetValue: {
        fr: 'Réinitialiser',
        nl: 'Reset',
        en: 'Reset values',
    },

    solPhotovoltaic: {
        fr: 'Photovoltaïque',
        nl: 'Zonnepanelen',
        en: 'Photovoltaic',
    },

    solThermal: {
        fr: 'Thermique',
        nl: 'Thermisch',
        en: 'Thermal',
    },

    gainEnergyInvoice: {
        fr: 'Gain facture énergétique',
        nl: 'Besparing op uw energiefactuur',
        en: 'Gain on energy bill',
    },

    gainEnergyInvoice10Y: {
        fr: 'Gain sur ma facture énergétique en 10 ans',
        nl: 'Besparing op uw energiefactuur op 10 jaar',
        en: 'Gain on energy bill over 10 years',
    },

    gainEnergyInvoice25Y: {
        fr: 'Gain sur ma facture énergétique en 25 ans',
        nl: 'Besparing op uw energiefactuur op 25 jaar',
        en: 'Gain on energy bill over 25 years',
    },

    bonus: {
        fr: 'Prime',
        nl: 'Premie',
        en: 'Bonus',
    },

    solHotWaterConsumption: {
        fr: 'Consommation d’eau chaude',
        nl: 'Warmwaterverbruik',
        en: 'Hot water consumption',
    },

    solGaz: {
        fr: 'Gaz',
        nl: 'Gas',
        en: 'Gas',
    },

    solMazout: {
        fr: 'Mazout',
        nl: 'Stookolie',
        en: 'Fuel oil',
    },

    solElectricBoiler: {
        fr: 'Boiler électrique',
        nl: 'Elektrische boiler',
        en: 'Electric boiler',
    },

    solHeatProdSys: {
        fr: 'Système actuel de production d’eau chaude',
        nl: 'Huidige warmwaterproductie',
        en: 'Current hot water production system',
    },

    solPellet: {
        fr: 'Chaudière à pellets',
        nl: 'Pellet ketel',
        en: 'Pellet boiler',
    },

    solDailyConsumption: {
        fr: 'Consommation journalière',
        nl: 'Dagelijks verbruik',
        en: 'Daily consumption',
    },

    solSearchAnotherAdress: {
        fr: 'Chercher une nouvelle adresse',
        nl: 'Nieuw adres zoeken',
        en: 'Search for a new address',
    },

    solDedicatedArea: {
        fr: 'Nombre de panneaux souhaités',
        nl: 'Gewenste aantal panelen',
        en: '',
    },

    solFinanceCost: {
        fr: 'Coût financier',
        nl: 'Rentekosten',
        en: 'Financial cost',
    },

    solFinanceGain: {
        fr: 'Gain financier',
        nl: 'Financieel gewin',
        en: 'Financial gain',
    },

    technology: {
        fr: 'Technologie',
        nl: 'Technologie',
        en: 'Technology',
    },

    consumptionYElectricity: {
        fr: 'Consommation annuelle d’électricité',
        nl: 'Jaarlijks elektriciteitsverbruik',
        en: 'Annual electricity consumption',
    },

    solFinanceVAT: {
        fr: 'Taux de TVA',
        nl: 'BTW tarief',
        en: 'VAT rates',
    },

    solSelectedPannels: {
        fr: 'panneaux photovoltaïques envisagés',
        nl: 'zonnepanelen',
        en: 'photovoltaic panels',
    },

    solOptimumInstallation: {
        fr: 'Puissance maximum de l’installation selon la technologie choisie',
        nl: 'Maximaal vermogen van de installatie volgens de gekozen technologie',
        en: 'Maximum power of the installation according to the chosen technology',
    },

    solOptimumInstallationTheoric: {
        fr: 'Puissance maximale théorique conseillée pour une installation non-industrielle (12kWc - non accessible dans le cas présent).',
        nl: 'Theoretisch maximaal vermogen aanbevolen voor een niet-industriële installatie (12kWp - niet toegankelijk in dit geval).',
        en: 'Theoretical maximum power recommended for a non-industrial installation (12kWc - not accessible in this case).',
    },

    solNoCalcSTR1: {
        fr: 'Nous sommes désolés mais nous ne pouvons pas fournir de simulation fiable pour votre toiture. Un pourcentage minime (0,5%) des toitures bruxelloises ne peut être analysé pour des raisons techniques indépendantes de notre volonté.',
        nl: 'Het spijt ons, maar we kunnen geen betrouwbare simulatie voor uw dak bieden. Buiten onze wil om kan een minimaal percentage (0,5%) van de Brusselse daken om technische redenen niet worden geanalyseerd.',
        en: '',
    },

    solNoCalcSTR2: {
        fr: 'Cependant, vous pouvez toujours faire appel aux conseillers énergies de la Région pour déterminer si l’installation de panneaux solaires est une piste intéressante pour votre habitation.  Contactez gratuitement un de nos conseillers:',
        nl: 'In dat geval kan u echter altijd een beroep doen op de energieadviseurs van de Regio om te bepalen of de installatie van zonnepanelen een interessante mogelijkheid is voor uw woning. Neem gratis contact op met één van onze adviseurs:',
        en: '',
    },

    solNoSolSTR1: {
        fr: 'Nous n’avons pas trouvé de configuration rentable pour l’installation de panneaux solaires.',
        nl: 'We hebben geen rentabele configuratie gevonden voor het installeren van zonnepanelen.',
        en: 'We have not found a cost-effective configuration for the installation of solar panels.',
    },

    solNoSolSTR2: {
        fr: 'Mais des alternatives existent si vous souhaitez améliorer la consommation d’énergie de votre bâtiment. ',
        nl: 'Maar er zijn alternatieven als u het energieverbruik van uw gebouw wil verbeteren.',
        en: 'But there are alternatives if you want to improve the energy consumption of your building.',
    },

    solNoSolSTR3: {
        fr: 'Découvrez ces alternatives ',
        nl: 'Ontdek deze alternatieven ',
        en: 'Discover these alternatives ',
    },

    solSharedRoof: {
        fr: 'toitures partagées',
        nl: 'gedeelde daken',
        en: 'shared roofs',
    },

    solSharedRoofLink: {
        fr: 'https://environnement.brussels/thematiques/energie/quest-ce-que-lenergie-verte/partagez-votre-toit-bloc-appartements',
        nl: 'https://leefmilieu.brussels/themas/energie/wat-groene-energie/deel-uw-dak-appartementsgebouwen',
        en: 'https://environnement.brussels/thematiques/energie/quest-ce-que-lenergie-verte/partagez-votre-toit-bloc-appartements',
    },

    solNoSolSTR4: {
        fr: '…) sur le site de ',
        nl: '…) op de website van ',
        en: '…) on the',
    },

    solNoSolSTR5: {
        fr: ' ou contactez gratuitement un de nos conseillers:',
        nl: ' of neem gratis contact op met één van onze adviseurs:',
        en: ' website or contact one of our consultants free of charge:',
    },

    solNoSolSTR6: {
        fr: ' pour les particuliers ou le ',
        nl: ' voor particulieren of de ',
        en: ' for individuals or the ',
    },

    solNoSolSTR7: {
        fr: 'pour les professionnels.',
        nl: 'voor professionelen.',
        en: 'for professionals.',
    },

    solHeatPumpLabel: {
        fr: 'pompe à chaleur',
        nl: 'warmtepomp',
        en: 'heat pump',
    },
    solHeatPumpLink: {
        fr: 'https://environnement.brussels/thematiques/energie/quest-ce-que-lenergie-verte/produire-votre-propre-energie-verte/les-pompes',
        nl: 'https://leefmilieu.brussels/themas/energie/groene-energie/produceer-uw-eigen-groene-energie/warmtepompen',
        en: 'https://environnement.brussels/thematiques/energie/quest-ce-que-lenergie-verte/produire-votre-propre-energie-verte/les-pompes',
    },

    solBuyGreenEnergyLabel: {
        fr: 'achat d’énergie verte',
        nl: 'aankoop van groene energie',
        en: 'buying green energy',
    },
    solBuyGreenEnergyLink: {
        fr: 'https://environnement.brussels/thematiques/energie/quest-ce-que-lenergie-verte/acheter-de-lenergie-verte',
        nl: 'https://leefmilieu.brussels/themas/energie/groene-energie/groene-energie-kopen',
        en: 'https://environnement.brussels/thematiques/energie/quest-ce-que-lenergie-verte/acheter-de-lenergie-verte',
    },

    solWaterStorage: {
        fr: 'Ballon de stockage',
        nl: 'Inhoud boiler',
        en: 'Storage device',
    },

    solPanels: {
        fr: 'panneaux',
        nl: 'panelen',
        en: 'panels',
    },

    solPanelsPV: {
        fr: 'panneaux photovoltaïques',
        nl: 'zonnepanelen',
        en: 'solar panels',
    },

    solPanelsTH: {
        fr: 'panneaux thermiques',
        nl: 'thermische panelen',
        en: 'thermic panels',
    },

    solSolarProdYear: {
        fr: 'Production solaire',
        nl: 'Totale productie aan warmte van de panelen',
        en: 'Yearly solar production',
    },

    solSolarConsumptionYear: {
        fr: 'Consommation d’énergie annuelle',
        nl: 'Huishoudelijk verbruik',
        en: 'Yearly energy consumption',
    },

    solSolarRateArea: {
        fr: 'Pourcentage d’eau chaude sanitaire produite',
        nl: 'Het totaal percentage geproduceerd huishoudelijk warm water',
        en: '',
    },

    solInstallationLifeTime: {
        fr: 'Durée de vie de l’installation',
        nl: 'Levensduur van de installatie',
        en: 'Installation lifetime',
    },

    solOn10Years: {
        fr: 'sur 10 ans',
        nl: 'op 10 jaar',
        en: 'over 10 years',
    },

    searchResult: {
        fr: 'Résultat de recherche',
        nl: 'Zoekresultaat',
        en: 'Search results',
    },

    solSolarWaterHeater: {
        fr: 'Solaire thermique',
        nl: 'Zonneboiler',
        en: '',
    },

    solSolarPanels: {
        fr: 'Panneaux solaires',
        nl: 'Zonnepanelen',
        en: 'Solar panels',
    },

    solCalculInfoStrPart1: {
        fr: 'FAQ',
        nl: 'Zonnekaart',
        en: 'Frequently asked',
    },

    solCalculInfoStrPart2: {
        fr: 'carte solaire',
        nl: 'FAQ’s',
        en: 'questions',
    },

    solMyRooftop: {
        fr: 'Ma toiture',
        nl: 'Mijn dak',
        en: '',
    },

    solTotalSurface: {
        fr: 'Surface totale',
        nl: 'Totale oppervlakte',
        en: '',
    },

    solMyInstallation: {
        fr: 'Mon installation',
        nl: 'Mijn installatie',
        en: '',
    },

    solMyEnergy: {
        fr: 'Mon énergie',
        nl: 'Mijn energie',
        en: '',
    },

    solMyFinance: {
        fr: 'Mes finances',
        nl: 'Mijn financiën',
        en: '',
    },

    solUsableRoofArea: {
        fr: 'Surface utilisable',
        nl: 'Bruikbare oppervlakte',
        en: 'Usable area',
    },

    solProductionPanels: {
        fr: 'Production des panneaux',
        nl: 'Productie van de panelen',
        en: '',
    },

    solHomeConsumption: {
        fr: 'Consommation du ménage',
        nl: 'Huishoudelijk verbruik',
        en: '',
    },

    solNumberOfPanels: {
        fr: 'Nombre de panneaux',
        nl: 'Aantal panelen',
        en: '',
    },

    solInstallationSurface: {
        fr: 'Superficie installée',
        nl: 'Geïnstalleerde oppervlakte',
        en: '',
    },

    solTotalPower: {
        fr: 'Puissance totale installée',
        nl: 'Totaal geïnstalleerd vermogen',
        en: '',
    },

    gainInvoice25Y: {
        fr: 'Gain sur ma facture en 25 ans',
        nl: 'Besparing op uw elektriciteitsfactuur na 25 jaar',
        en: '',
    },

    solTogglePV: {
        fr: 'Les panneaux solaires photovoltaïques produisent de l’électricité.',
        nl: 'Zonnepanelen produceren elektriciteit. Ze worden ook fotovoltaïsche panelen genoemd.',
        en: '',
    },

    solToggleThermal: {
        fr: 'Le chauffe-eau solaire produit de l’eau chaude sanitaire via des panneaux thermiques.',
        nl: 'De zonneboiler produceert sanitair warm water via thermische panelen.',
        en: '',
    },

    solDisclaimerLink: {
        fr: 'Ces données sont des estimations et n’engagent pas la responsabilité de Bruxelles Environnement.',
        nl: 'Deze gegevens zijn schattingen en vallen niet onder de verantwoordelijkheid van Leefmilieu Brussel.',
        en: 'These data are estimates and do not engage the responsibility of Brussels Environment.',
    },

    solContactLabel: {
        fr: 'Pour toute demande d’information, contactez ',
        nl: 'Voor alle informatie, contacteer ',
        en: 'For any information, contact',
    },

    solLinkContactBELabel: {
        fr: 'Bruxelles Environnement',
        nl: 'Leefmilieu Brussel',
        en: 'Bruxelles Environnement',
    },

    solLinkContactBE: {
        fr: 'https://environnement.brussels/bruxelles-environnement/nous-contacter',
        nl: 'https://leefmilieu.brussels/leefmilieu-brussel/contacteer-ons',
        en: 'https://environnement.brussels/bruxelles-environnement/nous-contacter',
    },

    solLinkFAQ: {
        fr: 'https://environnement.brussels/content/carte-solaire-de-la-region-bruxelloise-faq',
        nl: 'https://leefmilieu.brussels/content/zonnekaart-brussel-faq',
        en: 'https://environnement.brussels/content/carte-solaire-de-la-region-bruxelloise-faq',
    },

    solLinkDisclaimer: {
        fr: 'https://environnement.brussels/content/disclaimer-carte-solaire',
        nl: 'https://leefmilieu.brussels/content/disclaimer-zonnekaart',
        en: 'https://environnement.brussels/content/disclaimer-carte-solaire',
    },

    moreInfos: {
        fr: 'En savoir plus.',
        nl: 'Meer info.',
        en: 'More informations.',
    },

    solContactLinkLabel: {
        fr: 'Contacts et informations :',
        nl: 'Info en contacten :',
        en: 'Information and contacts :',
    },

    solDisclaimerLimit: {
        fr: 'Limite de responsabilité :',
        nl: 'Beperking van aansprakelijkheid :',
        en: 'Limitation of Liability :',
    },

    solLinkInfoGreenEnergy: {
        fr: 'https://environnement.brussels/thematiques/energie/quest-ce-que-lenergie-verte',
        nl: 'https://leefmilieu.brussels/themas/energie/wat-groene-energie',
        en: 'https://environnement.brussels/thematiques/energie/quest-ce-que-lenergie-verte',
    },

    solLinkInstallateurPV: {
        fr: 'https://rescert.be/fr/list?res_category=2',
        nl: 'https://rescert.be/nl/list?res_category=2',
        en: 'https://rescert.be/fr/list?res_category=2',
    },
    solLinkInstallateurTH: {
        fr: 'https://rescert.be/fr/list?res_category=4',
        nl: 'https://rescert.be/nl/list?res_category=4',
        en: 'https://rescert.be/fr/list?res_category=4',
    },

    solProduced: {
        fr: ' produits estimés',
        nl: ' geschat productie',
        en: ' estimated produced',
    },

    solConsumed: {
        fr: ' consommés estimés',
        nl: ' geschat verbruik',
        en: ' estimated consumed',
    },

    solUrbisLabel: {
        fr: 'Orthophotographie, géocodeur et données 3D © ',
        nl: 'Orthofotografie, geocoder en 3D-gegevens © ',
        en: 'Orthophotography, geocoder and 3D data © ',
    },

    solUrbisLink: {
        fr: 'https://cirb.brussels/fr/nos-solutions/urbis-solutions/urbis-data',
        nl: 'https://cibg.brussels/nl/onze-oplossingen/urbis-solutions/urbis-data?set_language=nl',
        en: 'https://bric.brussels/en/our-solutions/urbis-solutions/urbis-data?set_language=en',
    },

    solCreatorsLabel: {
        fr: 'Conception et réalisation : ',
        nl: 'Ontwerp en productie : ',
        en: 'Design and production : ',
    },

    solFacilitatorLabel: {
        fr: 'Facilitateur Bâtiment Durable',
        nl: 'Facilitator Duurzame Gebouwen',
        en: 'Facilitateur Bâtiment Durable',
    },

    solFacilitatorLink: {
        fr: 'https://environnement.brussels/thematiques/batiment/la-gestion-de-mon-batiment/pour-vous-aider/le-facilitateur-batiment-durable',
        nl: 'https://leefmilieu.brussels/themas/gebouwen/het-beheer-van-mijn-gebouw/om-u-te-helpen/facilitator-duurzame-gebouwen',
        en: 'https://environnement.brussels/thematiques/batiment/la-gestion-de-mon-batiment/pour-vous-aider/le-facilitateur-batiment-durable',
    },

    solHomegradeLabel: {
        fr: 'Homegrade',
        nl: 'Homegrade',
        en: 'Homegrade',
    },

    solHomegradeLink: {
        fr: 'https://homegrade.brussels/homegrade/contact/',
        nl: 'https://homegrade.brussels/homegrade/contact/?lang=nl',
        en: 'https://homegrade.brussels/homegrade/contact/',
    },

    solInstallMoreMsgSTR1: {
        fr: 'Vous disposez d’une toiture avec une surface utilisable supérieure à 200 m², et il est très certainement avantageux d’installer plus. Pour plus d’information, consultez le ',
        nl: 'U heeft een dak met een bruikbare oppervlakte van meer dan 200 m² en het is zeker voordelig om meer te installeren. Voor meer informatie, raadpleeg de ',
        en: 'You have a roof with a usable area of more than 200 m², it is certainly advantageous to install more. For more information, consult the ',
    },

    solInstallMoreMsgSTR2: {
        fr: ' ou adressez-vous a un ',
        nl: ' of neem contact op met een  ',
        en: ' or contact an ',
    },

    solInstallMoreMsgSTR3: {
        fr: 'installateur.',
        nl: 'installateur.',
        en: 'installator.',
    },

    solIncompleteAdress: {
        fr: 'Adresse incomplète.',
        nl: 'Onvolledig adres.',
        en: 'Incomplete address.',
    },

    solSummary10Y: {
        fr: 'Résumé sur 10 ans',
        nl: 'Samenvatting over 10 jaar',
        en: 'Summary on 10 years',
    },
    analyse: {
        nl: 'Examineren',
        fr: 'Analyser',
        en: 'Analyse',
    },

    backToMap: {
        fr: 'Retour à la carte',
        nl: 'Terug naar de kaart',
        en: 'Back to map',
    },

    roofTotalArea: {
        fr: 'Surface totale de toiture traitée',
        nl: 'Totale dakoppervlakte behandeld',
        en: 'Total processed rooftop area',
    },

    // obsolete
    roofComputedArea: {
        fr: 'Surface de toiture traitée',
        nl: 'Verwerkt dakoppervlak',
        en: 'Processed roof area',
    },

    solarPV: {
        fr: 'Solaire photovoltaïque',
        nl: 'Fotovoltaïsche zonne-energie ',
        en: 'Solar photovoltaic',
    },

    solarThermal: {
        fr: 'Solaire thermique',
        nl: 'Zonthermische',
        en: 'Solar thermal',
    },

    in: {
        fr: 'à',
        nl: 'in',
        en: 'in',
    },

    buyingPrice: {
        fr: 'Prix d’achat TVAC',
        nl: 'Aankoopprijs Incl.BTW',
        en: 'buying price Incl. VAT',
    },

    gainGreenCertif: {
        fr: 'Gains certificat vert',
        nl: 'Winst aan groenestroomcertificaten',
        en: 'Gain Green Certificate',
    },

    gainGreenCertif25Y: {
        fr: 'Gains certificat vert (10 ans)',
        nl: 'Winst aan groenestroomcertificaten (10 jaar)',
        en: 'Gain Green Certificate (10 years)',
    },

    gainElecInvoice: {
        fr: 'Gain facture d’électricité',
        nl: 'Besparing op uw elektriciteitsfactuur',
        en: 'Electricity bill savings',
    },

    gainElecInvoice25Y: {
        fr: 'Gain facture d’électricité sur 25 ans',
        nl: 'Besparing op uw elektriciteitsfactuur na 25 jaar',
        en: 'Electricity bill savings 25 years',
    },

    gainEnvironment: {
        fr: 'Gain pour l’environnement',
        nl: 'Winst voor het milieu',
        en: 'Environmental gain',
    },
    gainEnvironment25Y: {
        fr: 'Gain pour l’ environnement sur 25 ans',
        nl: 'Winst voor het milieu over 25 jaar',
        en: 'Environmental gain over 25 years',
    },

    orientationGreat: {
        fr: 'Excellent potentiel',
        nl: 'Uitstekend potentieel',
        en: 'Great potential',
    },

    orientationGood: {
        fr: 'Bon potentiel',
        nl: 'Goed potentieel',
        en: 'Good potential',
    },

    unusable: {
        fr: 'Faible potentiel (exclu du calcul)',
        nl: 'Laag potentieel (niet opgenomen in de berekening)',
        en: 'Low potential (excluded from the calculation)',
    },

    personalize: {
        fr: 'Personnaliser',
        nl: 'Personaliseren',
        en: 'personaliseren',
    },

    contactInstallator: {
        fr: 'Contacter un installateur',
        nl: 'Neem contact op met een installateur',
        en: 'Contact an installer',
    },

    changeMyHabits: {
        fr: 'Profiter au maximum de mon installation',
        nl: 'Profiteer optimaal van mijn installatie',
        en: 'Make the most of my installation',
    },

    installationObstacle: {
        fr: 'Contraintes en toiture',
        nl: 'Obstakels op het dak',
        en: 'Roofing constraints',
    },

    consumption: {
        fr: 'Consommation',
        nl: 'Consumerend',
        en: 'Consumption',
    },

    solAutoproduction: {
        fr: 'Gestes d’auto consommation',
        en: '',
        nl: 'Autoconsumptie verhogen',
    },

    finance: {
        fr: 'Finance',
        nl: 'Geldwezen',
        en: 'Finance',
    },

    loan: {
        fr: 'Emprunt',
        nl: 'Lening',
        en: 'Loan',
    },

    loanDuration: {
        fr: 'Durée du prêt',
        nl: 'Looptijd',
        en: 'Loan duration',
    },

    loanYes: {
        fr: 'Avec emprunt',
        nl: 'Met een lening',
        en: 'With a loan',
    },
    loanNo: {
        fr: 'Sans emprunt',
        nl: 'Zonder een lening',
        en: 'Without a loan',
    },
    loanRate: {
        fr: 'Taux du prêt',
        nl: 'Leningstarief',
        en: 'Loan rate',
    },

    installation: {
        fr: 'Installation',
        nl: 'Installatie',
        en: 'Installation',
    },

    surface: {
        fr: 'Superficie',
        nl: 'Plaats',
        en: 'Area',
    },

    power: {
        fr: 'Puissance',
        nl: 'Mogendheid',
        en: 'Power',
    },

    obstacleEstimation: {
        fr: 'Obstacles estimés',
        nl: 'Vermoedelijke obstakels',
        en: 'Estimated obstacles',
    },

    energy: {
        fr: 'Énergie',
        nl: 'Energie',
        en: 'Energy',
    },

    yearProduction: {
        fr: 'Production annuelle',
        nl: 'Jaarlijkse productie',
        en: 'Annual production',
    },

    yearConsumption: {
        fr: 'Consommation annuelle',
        nl: 'Jaarlijks verbruik',
        en: 'Annual consumption',
    },

    solarAutonomy: {
        fr: 'Auto consommation',
        nl: 'Autoconsumptie',
        en: '',
    },

    gainTotal25Y: {
        fr: 'Gains nets sur 25 ans',
        nl: 'Nettowinst na 25 jaar',
        en: '',
    },

    gainTotal: {
        fr: 'Gains totaux',
        nl: 'Totale winst',
        en: 'Total earnings',
    },

    returnTime: {
        fr: 'Temps de retour actualisé',
        nl: 'Bijgewerkte terugverdientijd',
        en: 'Return time',
    },

    noConsumptionChange: {
        fr: 'Je ne change pas ma consommation.',
        nl: 'Ik veranderd mijn verbruik niet.', //nltodo
        en: "I don't change my consumption.",
    },
    reduceConsumption: {
        fr: 'Je diminue ma consommation.',
        nl: 'Ik verminder mijn verbruik.',
        en: 'I reduce my consumption.',
    },

    dayConsumption: {
        fr: 'Je consomme en journée.',
        nl: 'Ik verbruik overdag.',
        en: 'I use during the day.',
    },

    hotWaterDuringDay: {
        fr: 'Eau chaude préparée en journée.',
        nl: 'Ik laat mijn water overdag opwarmen.',
        en: 'Hot water prepared during the day.',
    },

    boiler: {
        fr: 'Boiler',
        nl: 'Ketel',
        en: 'Boiler',
    },

    heatPump: {
        fr: 'Pompe à chaleur',
        nl: 'warmtepomp',
        en: 'Heat pump',
    },

    installBatteries: {
        fr: 'J’installe des batteries.',
        nl: 'Ik plaats batterijen.',
        en: 'I install bateries.',
    },

    technoType: {
        fr: 'Type de technologie photovoltaïque',
        nl: 'Type zonnepanelen technologie',
        en: 'Photovoltaic technologie type',
    },

    monocristal: {
        fr: 'Monocristallin',
        nl: 'Monokristallijn',
        en: 'Monocrystalin',
    },

    polycristal: {
        fr: 'Polycristallin',
        nl: 'Polykristallijn',
        en: 'Polycrystalin',
    },

    monocristalHR: {
        fr: 'Monocristallin haut rendement',
        nl: 'Monokristallijn met hoog rendement',
        en: 'High efficiency monocrystalin',
    },

    panelIntegration: {
        fr: 'Intégration des panneaux à la toiture',
        nl: 'Integratie van de panelen in het dak',
        en: 'Integration of the panels into the roof',
    },

    annualConsumptionKWh: {
        fr: 'Consommation annuelle',
        nl: 'Jaarlijks verbruik',
        en: 'Annual consumption',
    },

    solConsumptionEstimated: {
        fr: 'consommés estimés',
        nl: 'geschat verbruikt',
        en: 'consumed estimated',
    },

    annualMaintenance: {
        fr: 'Entretiens et monitoring annuel',
        nl: 'Jaarlijkse onderhouds en monitoring',
        en: 'Annual maintenance and monitoring',
    },

    sellingPrice: {
        fr: 'Vente de l’électricité injectée dans le réseau',
        nl: 'Verkoopprijs van aan het net geleverde elektriciteit',
        en: 'Price of electricity fed into the grid',
    },

    installationPrice: {
        fr: 'Coût de l’installation',
        nl: 'installatiekosten',
        en: 'Installation cost',
    },

    VATinstallation: {
        fr: 'TVA installation',
        nl: 'BTW installatie',
        en: 'VAT installation',
    },

    VAT21: {
        fr: '21%',
        nl: '21%',
        en: '21%',
    },

    VAT6: {
        fr: '6%',
        nl: '6%',
        en: '6%',
    },

    VAT0: {
        fr: '0%',
        nl: '0%',
        en: '0%',
    },

    solVAT21: {
        fr: '21% - Bâtiment < 10 ans',
        nl: '21% - Gebouw < 10 jaar',
        en: '21% - Building < 10 years',
    },

    solVAT6: {
        fr: '6% - Bâtiment > 10 ans',
        nl: '6% - Gebouw > 10 jaar',
        en: '6% - Building > 10 years',
    },

    solVAT0: {
        fr: '0% - Non residentiel',
        nl: '0% - Niet-residentieel',
        en: '0% - Non residential',
    },

    sellingGreenCertifPrice: {
        fr: 'Revente des certificats verts',
        nl: 'Doorverkoop van groenestroomcertificaten',
        en: 'Resale of Green Certificates',
    },

    monthlyPayment: {
        fr: 'Mensualité',
        nl: 'maandelijkse betalingen',
        en: 'Monthly payment',
    },

    durationYear: {
        fr: 'Durée en années',
        nl: 'Looptijd in jaren',
        en: 'Duration in year',
    },

    amountBorrowed: {
        fr: 'Montant emprunté',
        nl: 'Geleend bedrag',
        en: 'Amount borrowed',
    },

    estim10Y: {
        fr: 'Estimation sur 10 ans',
        nl: '10-jarige schatting',
        en: '10-year estimate',
    },

    velux: {
        fr: 'Fenêtre de toit (Velux)',
        nl: 'Dakraam (Velux)',
        en: 'Roof window (Velux)',
    },

    dormerWindow: {
        fr: 'Chien-assis',
        nl: 'Dakkapel',
        en: 'Dormer window',
    },

    flatRoofWindow: {
        fr: 'Fenêtre de toit plat',
        nl: 'Plat dakraam',
        en: 'Flat roof window',
    },

    existingSolarPannel: {
        fr: 'Panneaux solaires existants',
        nl: 'Bestaande zonnepanelen',
        en: 'Existing solar pannels',
    },

    chimneyAir: {
        fr: 'Cheminée de ventilation',
        nl: 'Ventilatieschoorsteen',
        en: 'Ventilation chimney',
    },

    chimneySmoke: {
        fr: 'Cheminée',
        nl: 'Schoorsteen',
        en: 'Chimney',
    },

    terraceInUse: {
        fr: 'Terrasse utilisée',
        nl: 'Terras',
        en: 'Terrace in use',
    },

    lift: {
        fr: 'Ascenseur',
        nl: 'Lift',
        en: 'Lift',
    },

    usableArea: {
        fr: 'Surface utile',
        nl: 'bruikbare ruimte',
        en: 'Usable area',
    },

    sheet: {
        fr: 'Fiche',
        nl: 'Blad',
        en: 'Sheet',
    },

    yes: {
        fr: 'Oui',
        nl: 'Ja',
        en: 'Yes',
    },

    no: {
        fr: 'Non',
        nl: 'Nee',
        en: 'No',
    },

    heat: {
        fr: 'chaleur ',
        nl: 'warmte ',
        en: 'heat ',
    },

    electricity: {
        fr: 'électricité ',
        nl: 'elektriciteit ',
        en: 'electricity ',
    },

    solLocatePitchStr1: {
        fr: 'Des',
        nl: '',
        en: '',
    },

    solLocatePitchStr2: {
        fr: 'panneaux solaires',
        nl: 'Zonnepanelen',
        en: 'Solar pannels',
    },

    solLocatePitchStr3: {
        fr: 'sur ma toiture ?',
        nl: 'op mijn dak ?',
        en: 'on my roof ?',
    },

    solLocatePitchStr4: {
        fr: 'Une solution avantageuse ',
        nl: 'Een voordelige oplossing ',
        en: 'An advantageous solution ',
    },

    solLocatePitchStr5: {
        fr: 'pour produire de l’',
        nl: 'om ',
        en: 'to produce',
    },

    solLocatePitchStr6: {
        fr: 'électricité',
        nl: 'elektriciteit',
        en: 'electricity',
    },

    solLocatePitchStr7: {
        fr: 'ou de la ',
        nl: 'of ',
        en: 'or ',
    },

    solLocatePitchStr8: {
        fr: 'chaleur',
        nl: 'warmte',
        en: 'heat',
    },

    solLocatePitchStr9: {
        fr: '',
        nl: ' te produceren',
        en: '',
    },

    solCalculateStrPart1: {
        fr: 'Calculer le ',
        nl: 'Bereken het ',
        en: 'Calculate the',
    },

    solSolarPotential: {
        fr: 'potentiel solaire',
        nl: 'zonnepotentieel',
        en: 'solar potential',
    },

    solSolarPotentialStr1: {
        fr: 'Installation possible de ',
        nl: 'Er is een installatie mogelijk van ',
        en: '',
    },
    solSolarPotentialStr2: {
        fr: 'avec un ',
        nl: 'met een ',
        en: '',
    },
    solSolarPotentialStr3: {
        fr: 'gain net ',
        nl: 'nettowinst ',
        en: '',
    },

    solSolarPotentialStr3Thermal: {
        fr: 'gain ',
        nl: 'winst ',
        en: '',
    },

    solSolarPotentialStr4: {
        fr: 'de ',
        nl: 'van ',
        en: '',
    },

    solSolarPotentialExcellent: {
        fr: 'Excellent potentiel solaire',
        nl: '***',
        en: '***',
    },

    solSolarPotentialGood: {
        fr: 'Bon potentiel solaire ',
        nl: '***',
        en: '***',
    },

    solCalculateStrPart2: {
        fr: 'de ma toiture',
        nl: 'van mijn dak',
        en: 'of my roof',
    },

    solOrSelectBuilding: {
        fr: 'Je sélectionne mon bâtiment',
        nl: 'Ik kies mijn gebouw',
        en: 'Select a building ',
    },

    solOnMap: {
        fr: 'sur la carte',
        nl: 'op de kaart',
        en: 'on the map',
    },

    solSolarGeocode: {
        fr: 'Adresse : rue, numéro, commune',
        nl: 'Adres: straat, nummer, plaats',
        en: 'Address: street, number, town',
    },

    solResearch: {
        fr: 'Chercher',
        nl: 'Zoeken',
        en: 'Search',
    },

    solBackToMap: {
        fr: 'Revenir à la carte',
        nl: 'Terug naar de kaart',
        en: 'Back to map',
    },

    solAndNow: {
        fr: 'Et maintenant ?',
        nl: 'Wat nu ?',
        en: 'Now what ?',
    },

    solActSettingStr1: {
        fr: 'avec les données',
        nl: 'met gedetailleerde',
        en: 'with detailed',
    },

    solActSettingStr2: {
        fr: 'détaillées',
        nl: 'projectgegevens',
        en: 'project data',
    },

    solActSettingStr3: {
        fr: 'du projet',
        nl: 'en bijzonderheden',
        en: 'and specifics',
    },

    solActSettingStr4: {
        fr: 'et ses spécificités',
        nl: '',
        en: '',
    },

    solActContactStr1: {
        fr: 'pour une installation',
        nl: 'voor een optimale',
        en: 'for optimal',
    },

    solActContactStr2: {
        fr: 'optimale',
        nl: 'installatie',
        en: 'installation',
    },

    solActContactStr3: {
        fr: 'et se renseigner',
        nl: 'en informatie',
        en: 'and information',
    },

    solActContactStr4: {
        fr: 'sur les aides potentielles',
        nl: 'over mogelijke hulpmiddelen',
        en: 'on potential aids',
    },

    solActChangeStr1: {
        fr: 'l’énergie produite',
        nl: 'van de energie',
        en: 'of the energy produced',
    },

    solActChangeStr2: {
        fr: 'par les panneaux',
        nl: 'die de panelen produceren',
        en: 'by the panels',
    },

    solActChangeStr3: {
        fr: 'et optimiser',
        nl: 'en het verbruik',
        en: 'and optimize',
    },

    solActChangeStr4: {
        fr: 'la consommation',
        nl: 'optimaliseren',
        en: 'consumption',
    },

    solActPrintStr1: {
        fr: 'la fiche détaillée',
        nl: 'het gedetailleerde',
        en: 'the detailed',
    },

    solActPrintStr2: {
        fr: 'de l’analyse',
        nl: 'analyseformulier',
        en: 'analysis sheet',
    },

    solAdjustStr1: {
        fr: 'Je personnalise',
        nl: 'Mijn installatie',
        en: '',
    },

    solAdjustStr2: {
        fr: 'mon installation',
        nl: 'personaliseren',
        en: '',
    },

    solContactStr1: {
        fr: 'Je trouve',
        nl: 'Mijn installateur',
        en: 'Contact',
    },

    solContactStr2: {
        fr: 'un installateur',
        nl: 'vinden',
        en: 'an installer',
    },

    solChangeStr1: {
        fr: 'Tout savoir',
        nl: 'Alles over ',
        en: '',
    },

    solChangeStr2: {
        fr: 'sur l’énergie verte',
        nl: 'groene energie',
        en: '',
    },

    solPrintStr1: {
        fr: 'Je télécharge',
        nl: 'Mijn rapport',
        en: 'View and',
    },

    solPrintStr2: {
        fr: 'mon rapport',
        nl: 'bekijken',
        en: 'download',
    },

    solPrintStr3: {
        fr: 'Télécharger le rapport',
        nl: 'Download het rapport',
        en: 'Download the report',
    },

    tradVersion: {
        fr: 'versie',
        nl: 'version',
        en: 'version',
    },

    solFlatRoof1: {
        fr: `Sur une toiture plate (hachurée) le positionnement des panneaux est plus libre et nécessite plus d’espace à puissance égale que sur une toiture inclinée.`,
        nl: `Op een plat dak (gearceerd) is de plaatsing van de panelen vrijer en is meer ruimte nodig om hetzelfde vermogen te verkrijgen dan op een hellend dak.`,
        en: `On a flat roof (hatched) the positioning of the panels is freer and requires more space for the same power than on a sloped roof.`,
    },

    solFlatRoof2: {
        fr: `La surface réellement utilisable est donc plus  réduite que sur une toiture inclinée.`,
        nl: `De werkelijk bruikbare oppervlakte is dus kleiner dan op een hellend dak.`,
        en: `The actual usable area is therefore smaller than on a sloped roof.`,
    },

    solFlatRoof3: {
        fr: `Voir les `,
        nl: `Zie de veelgestelde vragen `,
        en: `See the `,
    },

    solFlatRoof3Link: {
        fr: `FAQ`,
        nl: `(FAQ)`,
        en: `FAQs`,
    },

    solFlatRoof4: {
        fr: ` à ce propos et à propos de l’ombrage.`,
        nl: ` hierover, alsook over schaduw.`,
        en: ` about this and about shading.`,
    },

    solNotComputed: {
        fr: `
        Un pourcentage minime (0,5%) des toitures bruxelloises ne peut être analysé 
        pour des raisons techniques indépendantes de notre volonté.
        `,
        nl: `
        Een klein percentage (0,5%) van de Brusselse daken kan niet worden geanalyseerd
        om technische redenen buiten onze controle.
        `,
        en: `
        A small percentage (0.5%) of Brussels roofs can not be analyzed 
        for technical reasons beyond our control.
         `,
    },

    geocode: {
        fr: 'Chercher une adresse',
        nl: 'Zoek een adres',
        en: 'Search an adress',
    },
    unitYear: {
        fr: '{value, plural, =0 {an} =1 {an} other {ans}}',
        nl: '{value, plural, =0 {jaar} =1 {jaar} other {jaar}}',
        en: '{value, plural, =0 {years} =1 {year} other {years}}',
        parameters: {
            value: 1,
        },
    },
    unitPercent: {
        fr: '%',
        nl: '%',
        en: '%',
    },

    largePitchStr1: {
        fr: 'Environ',
        nl: 'Ongeveer',
        en: '',
    },

    largePitchStr2: {
        fr: 'utilisables',
        nl: 'zijn bruikbaar',
        en: '',
    },

    largePitchStr3: {
        fr: `vous permettent d'envisager une`,
        nl: ' en dus kunt u een',
        en: '',
    },

    largePitchStr4: {
        fr: 'installation collective',
        nl: 'collectieve',
        en: '',
    },

    largePitchStr5: {
        fr: 'ou',
        nl: 'of',
        en: '',
    },

    largePitchStr6: {
        fr: 'industrielle.',
        nl: 'industriële installatie',
        en: '',
    },

    largePitchStr7: {
        fr: '',
        nl: 'overwegen.',
        en: '',
    },

    largePitchInfos: {
        fr: 'Retrouvez sur cette page nos informations à ce sujet.',
        nl: 'Op deze pagina vindt u informatie hierover.',
        en: '',
    },

    largePitchSkip: {
        fr: 'Si votre souhait est une installation individuelle, nous vous invitons à poursuivre la simulation.',
        nl: 'Wenst u een individuele installatie? Dan nodigen we u uit om uw analyse voort te zetten.',
        en: '',
    },

    largeTextStr1: {
        fr: texts.largeTextStr1FR,
        nl: texts.largeTextStr1NL,
        en: '',
    },

    largeTextStr2: {
        fr: texts.largeTextStr2FR,
        nl: texts.largeTextStr2NL,
        en: '',
    },

    largeTextStr3: {
        fr: texts.largeTextStr3FR,
        nl: texts.largeTextStr3NL,
        en: '',
    },
};

type MDB = typeof messages;
export type SolarMessageKey = keyof MDB;

declare module 'sdi/locale' {
    export interface MessageStore {
        solar(k: SolarMessageKey, params?: Parameters): Translated;
    }
}

MessageStore.prototype.solar = (k: SolarMessageKey, params?: Parameters) =>
    formatMessage(messages[k], params);
