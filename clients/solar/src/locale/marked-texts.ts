export const largeTextStr1FR = `
À l'adresse
`;

export const largeTextStr1NL = `
Op het adres 
`;

export const largeTextStr2FR = `
environ
`;

export const largeTextStr2NL = `
heeft ongeveer
`;

export const largeTextStr3FR = `
 des surfaces de toitures ont un potentiel exploitable, compte tenu des obstacles et des ombres portées. Ils sont donc utilisables pour placer des panneaux solaires.

Une telle surface vous permet d’envisager une installation solaire photovoltaïque qui dépasse les besoins résidentiels individuels.

Avec de tels espaces, vous pouvez envisager différentes options:

**Équiper collectivement** la toiture dans le cadre d’une dynamique de «Toiture partagée». Dans ce cas, la copropriété met la toiture à disposition des investisseurs.  

  - Des renseignements sont disponibles sur le site web de Bruxelles Environnement vous permettant d’identifier [les étapes de votre projet](https://environnement.brussels/decision_tree/arbres-decision/des-modeles-de-contrats-pour-linstallation-de-panneaux-photovoltaiques).
  - Homegrade peut ensuite vous aider pour aller plus loin : https://homegrade.brussels/ 


**Équiper la toiture** pour des besoins de type **industriel** ou des **grands espaces de bureaux**.

  - Le [facilitateur bâtiment durable](https://environnement.brussels/thematiques/batiment-et-energie/accompagnements-gratuits/le-facilitateur-batiment-durable) sera votre soutien idéal : [facilitateur@environnement.brussels](facilitateur@environnement.brussels) 

**Démarrer un projet pilote** de partage d’énergie, que ce soit au sein du même bâtiment ou à l’échelle de votre voisinage par exemple.

  - En consultant la page [partagez votre électricité](https://environnement.brussels/thematiques/batiment-et-energie/energie-verte/auto-consommation-collective-mutualiser-lenergie), vous en découvrirez les avantages et les démarches à entreprendre.
`;

export const largeTextStr3NL = `
 van de oppervlakte van het dak bruikbaar voor zonnepanelen, rekening houdend met de schaduwen en obstakels. 

Een dergelijke oppervlakte maakt het mogelijk om een fotovoltaïsche installatie te overwegen die de individuele behoeften overstijgt.

U kunt verschillende opties overwegen:

Het dak **gezamenlijk uitrusten** als onderdeel van een "Deel je dak"-dynamiek. In dit geval stelt de mede-eigendom het dak ter beschikking van de investeerders.

  - Op de pagina's van Leefmilieu Brussel is informatie beschikbaar waarmee u de [fasen van uw project](https://leefmilieu.brussels/decision_tree/beslissingsboom/modelcontracten-voor-het-gebruik-van-gedeelde-daken-voor-de) kunt identificeren.
  - Homegrade kan u helpen: https://homegrade.brussels/ 

Het **uitrusten van het dak** voor **industriële** of **grote kantoorruimtes**.

  - De [Facilitator Duurzame Gebouwen](https://leefmilieu.brussels/themas/gebouwen-en-energie/gratis-begeleiding/facilitator-duurzame-gebouwen) zal u de nodige ondersteuning bieden: [facilitator@leefmilieu.brussels](facilitator@leefmilieu.brussels)

Een **proefproject** voor het delen van energie opstarten. Dat kunt u doen binnen hetzelfde gebouw of op de schaal van uw wijk.

  - Op de pagina [Deel uw elektriciteit](https://leefmilieu.brussels/themas/gebouwen-en-energie/groene-energie/collectief-zelfverbruik-bundel-lokaal-de-krachten) ontdekt u de voordelen en de administratieve stappen.
`;
