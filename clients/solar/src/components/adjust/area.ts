import { DIV, SPAN, NODISPLAY, A, H2 } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { withKWc, withKWhY } from 'sdi/util';

import {
    getPanelUnits,
    getMaxPower,
    getOutputPv,
    usableRoofArea,
    getAnnualPower,
    getPanelUnitsFromRank,
} from '../../queries/simulation';
import { setPower } from '../../events/simulation';
import { note } from './note';
import { trackedLink } from '../tracked-link';
import { renderRadioIn } from 'sdi/components/input';

type rank = number;

const getPower = () => Math.round(getOutputPv('power'));

const powers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
const getReachablePowers = () =>
    powers.filter(p => getStatus(p) != 'unreachable');

type Status =
    | 'under'
    | 'selected'
    | 'selected-max'
    | 'over'
    | 'last-over'
    | 'unreachable';

const getStatus = (n: number): Status => {
    const p = getPower();
    const maxPower = Math.round(getMaxPower());
    if (n < p) {
        return 'under';
    } else if (n === p) {
        if (n === maxPower || n === 12) {
            return 'selected-max';
        }
        return 'selected';
    } else if (n > maxPower) {
        return 'unreachable';
    } else if (n === maxPower || n === 12) {
        return 'last-over';
    }
    return 'over';
};

const hasOver = () => {
    return (
        powers.filter(
            a => getStatus(a) === 'over' || getStatus(a) === 'last-over'
        ).length > 0
    );
};

// const hasUnreachable =
//     () => {
//         return powers.filter(a => getStatus(a) === 'unreachable').length > 0;
//     };
const renderItemLabel = (rank: rank) =>
    getAnnualPower(rank).chain(ap =>
        getPanelUnitsFromRank(rank).map(
            pu =>
                `${pu} ${tr.solar('solSelectedPannels')}, ${withKWhY(
                    ap
                )} ${tr.solar('solProduced')}`
        )
    );

const selectItemClickable = (rank: rank, className: string) =>
    DIV(
        {
            title: withKWc(rank, 1),
            className: `select-item  ${className}`,
            onClick: () => setPower(rank),
        },
        DIV(
            { className: 'invisible' },
            renderItemLabel(rank)
            // `${getPanelUnits()} ${tr.solar('solSelectedPannels')} (${withKWhY(getOutputPv('annualProduction'))} ${tr.solar('solProduced')})`,
        )
    );

const selectItemNotClickable = (rank: rank, className: string) =>
    DIV(
        {
            title: withKWc(rank, 1),
            className: `select-item  ${className}`,
        },
        DIV({ className: 'invisible' }, renderItemLabel(rank))
    );

const selectItem = (rank: rank) => {
    switch (getStatus(rank)) {
        case 'under':
            return selectItemClickable(rank, 'under');
        case 'selected':
            return selectItemNotClickable(rank, 'selected');
        case 'selected-max':
            return selectItemNotClickable(rank, 'selected selected-max');
        case 'over':
            return selectItemClickable(rank, 'over');
        case 'last-over':
            return selectItemClickable(rank, 'last over');
        case 'unreachable':
            return selectItemNotClickable(rank, 'unreachable');
    }
};

const radioInSelect = renderRadioIn(
    'solar-select-widget',
    selectItem,
    setPower
);
const selectWidget = () =>
    DIV(
        { className: 'adjust-item-widget area-select' },
        // ...(powers.map(selectItem)),
        radioInSelect(getReachablePowers(), getPower())
    );

const title = () =>
    DIV(
        { className: 'adjust-item-header' },
        H2(
            { className: 'adjust-item-title' },
            `3. ${tr.solar('solDedicatedArea')}`
        ),
        DIV({ className: 'adjust-picto picto-panel' })
    );

const production = () =>
    DIV(
        {
            className: 'item-legend legend-output',
        },
        SPAN(
            { className: 'output-value' },
            withKWhY(getOutputPv('annualProduction'))
        ),
        SPAN({}, tr.solar('solProduced'))
    );

const installMoreSentence = () =>
    DIV(
        { className: 'adjust-item-note install-more' },
        tr.solar('solInstallMoreMsgSTR1'),
        A(
            { href: tr.solar('solFacilitatorLink') },
            tr.solar('solFacilitatorLabel')
        ),
        tr.solar('solInstallMoreMsgSTR2'),
        trackedLink(
            'installateur',
            { href: tr.solar('solLinkInstallateurPV') },
            tr.solar('solInstallMoreMsgSTR3')
        )
    );

const installMore = () => {
    if (usableRoofArea() > 200) {
        return installMoreSentence();
    }
    return NODISPLAY();
};

const legend = () => {
    const elements = [
        DIV(
            {
                className: 'item-legend selected',
                id: 'item-legend-selected',
            },
            `${getPanelUnits()} ${tr.solar('solSelectedPannels')}`
        ),
    ];

    if (hasOver()) {
        elements.push(
            DIV(
                { className: 'item-legend over' },
                tr.solar('solOptimumInstallation')
            )
        );
    }

    // if (hasUnreachable()) {
    //     elements.push(
    //         DIV({ className: 'item-legend unreachable' },
    //             tr.solar('solOptimumInstallationTheoric')));
    // }

    return DIV(
        {
            className: 'adjust-item-legend',
            'aria-hidden': true,
        },
        production(),
        ...elements
    );
};

export const calcArea = () =>
    DIV(
        { className: `adjust-item area` },
        title(),
        note('pv_num'),
        installMore(),
        selectWidget(),
        legend()
    );
