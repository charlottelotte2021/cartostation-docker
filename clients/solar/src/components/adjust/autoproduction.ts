import * as debug from 'debug';

import { DIV, H2 } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { getInputF } from '../../queries/simulation';
import { setInputF } from '../../events/simulation';
import { note } from './note';
import { renderRadioIn } from 'sdi/components/input';

const logger = debug('sdi:adjust/auto');

const getEnergySobriety = getInputF('energySobriety');
const getChargeShift = getInputF('chargeShift');
const getPVHeater = getInputF('pvHeater');
const getBattery = getInputF('battery');

const setEnergySobriety = setInputF('energySobriety');
const setChargeShift = setInputF('chargeShift');
const setPVHeater = setInputF('pvHeater');
const setBattery = setInputF('battery');

const NOPE = 0x1;
const A = 0x2;
const B = A << 1;
const C = B << 1;
const D = C << 1;

const ranks = {
    [NOPE]: 'zero',
    [A]: 'first',
    [B]: 'second',
    [C]: 'third',
    [D]: 'fourth',
};
type Rank = typeof ranks;
type rank = keyof Rank;
const rankArray = [NOPE, A, B, C, D];

const ranksLabels = {
    [NOPE]: () => tr.solar('noConsumptionChange'),
    [A]: () => tr.solar('reduceConsumption'),
    [B]: () => tr.solar('dayConsumption'),
    [C]: () => tr.solar('hotWaterDuringDay'),
    [D]: () => tr.solar('installBatteries'),
};

const setLevel = (l: number) => {
    setEnergySobriety(l >= A);
    setChargeShift(l >= B);
    setPVHeater(l >= C);
    setBattery(l >= D);
};
const getLevel = () => {
    if (getBattery()) {
        return D;
    }
    if (getPVHeater()) {
        return C;
    }
    if (getChargeShift()) {
        return B;
    }
    if (getEnergySobriety()) {
        return A;
    }
    return NOPE;
};

// const isActive =
//     (exact: boolean) => (n: number) => {
//         let score = 0x1;
//         if (getEnergySobriety()) { score = score << 1; }
//         if (getChargeShift()) { score = score << 1; }
//         if (getPVHeater()) { score = score << 1; }
//         if (getBattery()) { score = score << 1; }

//         if (exact) {
//             return n === score;
//         }
//         return n < score;
//     };

// const isUnderActive = isActive(false);
// const isExactlyActive = isActive(true);

// const underActiveClass = (n: number) => isUnderActive(n) ? 'under-active' : '';
// const exactActiveClass = (n: number) => isExactlyActive(n) ? 'active' : '';

// const selectItem =
//     (rank: rank) =>
//         DIV({
//             className: `select-item ${ranks[rank]} ${underActiveClass(rank)} ${exactActiveClass(rank)}`,
//             onClick: () => setLevel(rank),
//         });

const renderRank = (rank: rank) => {
    return DIV(
        {
            // className: `select-item ${ranks[rank]} ${underActiveClass(rank)} ${exactActiveClass(rank)}`,
            // "aria-labelledby": `autoproduction-legend-${ranks[rank]}`
        },
        DIV({ className: 'invisible' }, ranksLabels[rank]())
    );
};
const renderRadio = renderRadioIn(
    'autoproduction-consumption-selector',
    renderRank,
    setLevel
);

const selectWidget = () =>
    DIV(
        { className: 'adjust-item-widget autoproduction-select' },
        renderRadio(rankArray, getLevel())
        // selectItem(NOPE),
        // selectItem(A),
        // selectItem(B),
        // selectItem(C),
        // selectItem(D),
    );

const legend = () =>
    DIV(
        { className: 'adjust-item-legend', 'aria-hidden': true },
        DIV(
            {
                className: 'reduce' + (getEnergySobriety() ? ' active' : ''),
                id: `autoproduction-legend-${ranks[A]}`,
            },
            tr.solar('reduceConsumption')
        ),
        DIV(
            {
                className: 'day' + (getChargeShift() ? ' active' : ''),
                id: `autoproduction-legend-${ranks[B]}`,
            },
            tr.solar('dayConsumption')
        ),
        DIV(
            {
                className: 'waterheating' + (getPVHeater() ? ' active' : ''),
                id: `autoproduction-legend-${ranks[C]}`,
            },
            tr.solar('hotWaterDuringDay')
        ),
        DIV(
            {
                className: 'battery' + (getBattery() ? ' active' : ''),
                id: `autoproduction-legend-${ranks[D]}`,
            },
            tr.solar('installBatteries')
        )
    );

export const calcAutoproduction = () =>
    DIV(
        { className: 'adjust-item autoproduction' },
        DIV(
            { className: 'adjust-item-header' },
            H2(
                { className: 'adjust-item-title' },
                '5. ' + tr.solar('solAutoproduction')
            ),
            DIV({ className: 'adjust-picto picto-solar-energy' })
        ),
        note('pv_autonomy'),
        selectWidget(),
        legend()
    );

logger('loaded');
