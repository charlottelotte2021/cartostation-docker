import { DIV, H2 } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { withEuro } from 'sdi/util';

import { getInputF } from '../../queries/simulation';
import { setInputF } from '../../events/simulation';
import { note } from './note';
import { renderRadioIn } from 'sdi/components/input';

const getThermicGrant = getInputF('thermicGrant');
const setThermicGrant = setInputF('thermicGrant');
const grants = [2500, 3000, 3500];

// const klass =
//     (n: number) => getThermicGrant() === n ? 'checkbox active' : 'checkbox';

const item = (n: number) =>
    DIV(
        {
            key: `thermal-grant-${n}`,
            className: 'wrapper-checkbox',
            // onClick: () => setThermicGrant(n),
        },
        // DIV({ className: klass(n) }),
        DIV({ className: 'checkbox-label' }, withEuro(n))
    );

const renderThermalGainRadio = renderRadioIn(
    'thermal-finance-gain-radio',
    item,
    setThermicGrant,
    'radio'
);

const bonus = () =>
    DIV(
        { className: 'gain' },
        DIV(
            { className: 'wrapper-multi-checkbox' },
            // grants.map(item)),
            renderThermalGainRadio(grants, getThermicGrant())
        )
    );

export const calcFinanceThermalGain = () =>
    DIV(
        { className: 'adjust-item finance' },
        DIV(
            { className: 'adjust-item-header' },
            H2({ className: 'adjust-item-title' }, '3. ' + tr.solar('bonus')),
            DIV({ className: 'adjust-picto picto-gain' })
        ),
        note('thermal_grant'),
        DIV({ className: 'adjust-item-widget' }, bonus())
    );
