import * as debug from 'debug';

import { DIV, SPAN, INPUT, H1, BUTTON } from 'sdi/components/elements';
import { isENTER } from 'sdi/components/keycodes';
import tr from 'sdi/locale';
import { switcherLarge } from 'sdi/components/lang-switch';

import map from '../map';

import { geocoderResponse, geocoderInput } from '../../queries/map';
import { IUgWsAddress, IUgWsResult, queryGeocoder } from 'sdi/ports/geocoder';
import { updateGeocoderTerm, updateGeocoderResponse } from '../../events/map';
import { fetchKey } from '../../remote/index';
import { navigatePreview } from '../../events/route';
import { setAddress } from '../../events/simulation';
import { setLayout } from '../../events/app';
import { getLang } from 'sdi/app/queries';
import { setFocusId, setLang } from 'sdi/app';
import { MessageRecordLang } from 'sdi/source';
import { makeLabel } from 'sdi/components/button';

const logger = debug('sdi:solar');

const updateAddress = (e: React.ChangeEvent<HTMLInputElement>) => {
    updateGeocoderTerm(e.target.value);
};

const changeLang = (l: MessageRecordLang) => () =>
    l === 'nl' ? setLang('fr') : setLang('nl');

export const bigLangSwitch = () => {
    const lc = getLang();
    return BUTTON(
        {
            className: 'lang-switch-wrapper',
            onClick: changeLang(lc),
        },
        switcherLarge(() => tr.solar('tradVersion'), lc)
    );
};

const searchAddress = () => {
    geocoderInput().foldL(
        () => updateGeocoderResponse(null),
        ({ addr, lang }) => {
            queryGeocoder(addr, lang).then(updateGeocoderResponse);
        }
    );
};

const addressToString = (a: IUgWsAddress) =>
    `${a.street.name} ${a.number}, ${a.street.postCode} ${a.street.municipality}`;

const renderGeocoderInput = () =>
    INPUT({
        className: 'locate-input',
        type: 'text',
        name: 'adress',
        placeholder: tr.solar('solSolarGeocode'),
        onChange: updateAddress,
        onKeyPress: (e: React.KeyboardEvent<HTMLInputElement>) => {
            if (isENTER(e)) {
                searchAddress();
            }
        },
    });

const renderGeocoderButton = () =>
    BUTTON(
        { className: 'btn-analyse', onClick: searchAddress },
        SPAN({ className: 'sun' }),
        tr.solar('solResearch')
    );

const renderGeocoderInputWrapper = (...n: React.ReactNode[]) =>
    DIV({ className: 'input-wrapper' }, ...n);

const renderGeocoderResults = (results: IUgWsResult[]) => {
    return results.map(({ point, address }, key) => {
        // const coords: [number, number] = [result.point.x, result.point.y];
        if ('' === address.number) {
            return BUTTON(
                { className: 'adress-result', key },
                DIV({
                    className: 'select-icon',
                    'aria-labelledby': `adress-${key}`,
                }),
                DIV(
                    {
                        className: 'no-address',
                        id: `adress-${key}`,
                    },
                    addressToString(address),
                    ' - ',
                    tr.solar('solIncompleteAdress')
                )
            );
        }
        return BUTTON(
            {
                className: 'adress-result',
                key,
                onClick: () => {
                    setAddress(address);
                    updateGeocoderResponse(null);
                    fetchKey(point.x, point.y)
                        .then(({ capakey }) => navigatePreview(capakey))
                        .catch((err: string) => {
                            logger(`Could not fetch a capakey: ${err}`);
                        });
                },
            },
            DIV({
                className: 'select-icon',
                'aria-labelledby': `adress-${key}`,
            }),
            DIV(
                {
                    id: `adress-${key}`,
                },
                addressToString(address)
            )
        );
    });
};

// const renderClearResults =
//     () => DIV({
//         className: 'btn-reset geocoder-clear',
//         onClick: clearGeocoderResponse,
//     }, tr.solar('solSearchAnotherAdress'));

const renderGeocoderResultsWrapper = (...n: React.ReactNode[]) =>
    DIV(
        { className: 'geocoder-wrapper' },
        H1({}, tr.solar('searchResult')),
        DIV('results', ...n)
    );

const renderGeocoder = (): React.ReactNode[] =>
    geocoderResponse().fold(
        [
            renderGeocoderInputWrapper(
                renderGeocoderInput(),
                renderGeocoderButton()
            ),
        ],
        ({ result }) => [
            renderGeocoderInputWrapper(
                renderGeocoderInput(),
                renderGeocoderButton()
            ),
            renderGeocoderResultsWrapper(renderGeocoderResults(result)),
        ]
    );

const pitch = () =>
    DIV(
        { className: 'locate-pitch' },
        DIV({}, tr.solar('solLocatePitchStr1'), ' '),
        DIV({ className: 'pitch-bold' }, tr.solar('solLocatePitchStr2')),
        DIV({}, ' ', tr.solar('solLocatePitchStr3')),
        DIV(
            {},
            DIV({}, tr.solar('solLocatePitchStr4')),
            SPAN({}, tr.solar('solLocatePitchStr5')),
            SPAN({ className: 'pitch-bold' }, tr.solar('solLocatePitchStr6'))
        ),
        DIV(
            {},
            SPAN({}, tr.solar('solLocatePitchStr7')),
            SPAN({ className: 'pitch-bold' }, tr.solar('solLocatePitchStr8')),
            SPAN({}, tr.solar('solLocatePitchStr9'))
        )
    );

const illu = () => DIV({ className: 'locate-illu' });

const pitchWrapper = () =>
    DIV({ className: 'locate-pitch-wrapper' }, pitch(), illu());

const goToMap = () =>
    DIV(
        { className: 'locate-goto-map' },
        DIV('sub-pitch', tr.solar('solOrSelectBuilding')),
        makeLabel('navigate', 1, () => tr.solar('solOnMap'))(() => {
            setLayout('Locate:Map');
            setFocusId('locate-goto-search');
        }, 'solar-btn map-button')
    );

const goToSearch = () =>
    DIV(
        { className: 'locate-goto-search', id: 'locate-goto-search' },
        makeLabel('navigate', 1, () => tr.solar('geocode'))(
            () => setLayout('Locate:Geocoder'),
            'solar-btn map-button'
        )
        // BUTTON({
        //     className: 'map-button',
        //     onClick: () => setLayout('Locate:Geocoder'),
        // }, tr.solar('geocode')),
    );

const searchWrapper = () =>
    DIV(
        { className: 'locate-geocode' },
        DIV(
            'sub-pitch',
            SPAN({}, tr.solar('solCalculateStrPart1')),
            SPAN({ className: 'pitch-bold' }, tr.solar('solSolarPotential')),
            SPAN({}, ' ', tr.solar('solCalculateStrPart2'))
        ),
        ...renderGeocoder()
    );

const wrapperTop = () =>
    DIV(
        { className: 'wrapper-top' },
        pitchWrapper(),
        searchWrapper(),
        goToMap()
    );

const render = (withWrapper: boolean) => {
    if (withWrapper) {
        return DIV(
            { className: 'locate-box' },
            bigLangSwitch(),
            map(),
            wrapperTop()
        );
    }
    return DIV(
        { className: 'locate-box' },
        bigLangSwitch(),
        map(),
        goToSearch()
    );
};

export default render;

logger('loaded');
