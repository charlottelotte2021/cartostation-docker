import { DIV, H1, SPAN } from 'sdi/components/elements';
import { withM2 } from 'sdi/util';
import {
    usableRoofArea,
    streetName,
    streetNumber,
    locality,
} from '../queries/simulation';
import { markdown } from 'sdi/ports/marked';
import { navigate } from '../events/route';
import { contextWithoutDisclaimer } from './context';
import tr from 'sdi/locale';
// import { nameToString } from 'sdi/components/button/names';
import {
    contactLinks,
    disclaimer,
    urbisReward,
    createdBy,
} from './footer-infos';
import { makeLabelAndIcon } from 'sdi/components/button';

// const renderText = () => DIV({className: 'actions large-infos'}, markdown(`
// L'analyse des surfaces de toitures à l'adresse ${streetName()} ${streetNumber()} ${tr.solar('in')} ${locality()}, conclut qu'environ ${withM2(usableRoofArea())} sont utilisables pour placer des panneaux solaires.
// Cela signifie que ${withM2(usableRoofArea())} sont classés comme ayant un potentiel moyen et/ou excellent (plus d’informations sur la [FAQ](https://environnement.brussels/content/carte-solaire-de-la-region-bruxelloise-faq)).

// Une telle surface vous permet d’envisager la réalisation d’une installation solaire photovoltaïque pour des usages relativement importants, qui dépassent les besoins résidentiels individuels.

// Avec de tels espaces, vous pouvez envisager les options suivantes:

// **Équiper collectivement** la toiture dans le cadre d’une dynamique de «Toiture partagée».

//   - Des renseignements sont disponibles aux pages de Bruxelles Environnement vous permettant d’identifier [les étapes de votre projet](https://environnement.brussels/decision_tree/arbres-decision/des-modeles-de-contrats-pour-linstallation-de-panneaux-photovoltaiques).
//   - Homegrade peut ensuite vous aider pour aller plus loin : https://homegrade.brussels/

// **Équiper la toiture** pour des besoins de type **industriel** ou des **grands espaces de bureaux**.

//   - Le facilitateur bâtiment durable sera votre soutien idéal : facilitateur@environnement.brussels

// **Démarrer un projet pilote** de partage d’énergie, que ce soit au sein du même bâtiment ou à l’échelle de votre voisinage par exemple.

//   - Lien 1 et référence

// `));

const renderText = () =>
    DIV(
        { className: 'actions large-infos' },
        markdown(`
${tr.solar('largeTextStr1')} ${streetName()} ${streetNumber()} ${tr.solar(
            'in'
        )} ${locality()}, ${tr.solar('largeTextStr2')} ${withM2(
            usableRoofArea()
        )} ${tr.solar('largeTextStr3')}
`)
    );

const renderAdress = () =>
    DIV(
        {},
        H1(
            {},
            `${streetName()} ${streetNumber()} ${tr.solar('in')} ${locality()}`
        )
    );

const renderTagLine1 = () =>
    DIV(
        { className: 'potential-label' },

        DIV(
            {},
            tr.solar('largePitchStr1'),
            SPAN(
                { className: 'highlight-value' },
                ` ${withM2(usableRoofArea())} ${tr.solar('largePitchStr2')} `
            ),
            `${tr.solar('largePitchStr3')} `,
            SPAN(
                { className: 'highlight-value' },
                `${tr.solar('largePitchStr4')} `
            ),
            tr.solar('largePitchStr5'),
            SPAN(
                { className: 'highlight-value' },
                ` ${tr.solar('largePitchStr6')} `
            ),
            tr.solar('largePitchStr7')
        )
    );
const renderTagLine2 = () =>
    DIV({ className: 'potential-label' }, DIV({}, tr.solar('largePitchInfos')));

const renderSidebarText = () =>
    DIV({ className: 'large-indiv' }, tr.solar('largePitchSkip'));

const renderButton = () =>
    makeLabelAndIcon('navigate', 1, 'chevron-right', () =>
        tr.solar('individualInstallation')
    )(() => {
        navigate();
    }, 'solar-btn');

// const renderButton = () => BUTTON({
//   className: 'solar-btn next',
//   onClick: () => {
//     navigate();
//   },
// },
//   DIV({ className: 'solar-inner-btn' },
//     tr.solar('individualInstallation'),
//     SPAN({ className: 'picto' }, nameToString('chevron-right')),
//   ));

const renderSidebar = () =>
    DIV(
        { className: 'sidebar' },
        DIV(
            { className: 'summary' },
            renderAdress(),
            renderTagLine1(),
            renderTagLine2(),
            renderSidebarText(),
            renderButton()
        )
    );

const contentFooter = () =>
    DIV(
        { className: 'footer-infos' },
        contactLinks(),
        disclaimer(),
        urbisReward(),
        createdBy()
    );

export const render = () =>
    DIV(
        { className: 'solar-main-and-right-sidebar' },
        DIV(
            { className: 'content' },
            contextWithoutDisclaimer(),
            renderSidebar(),
            renderText(),
            contentFooter()
        )
    );

export default render;
