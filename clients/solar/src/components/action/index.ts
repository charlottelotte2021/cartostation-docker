import { DIV, BR, H3 } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { trackedLink } from '../tracked-link';

import { getSystem } from '../../queries/simulation';
import { navigateSolarContactAction } from 'sdi/activity';

const switchLinkPVTH = () => {
    const system = getSystem();
    switch (system) {
        case 'photovoltaic':
            return tr.solar('solLinkInstallateurPV');
        case 'thermal':
            return tr.solar('solLinkInstallateurTH');
    }
};

export const actionContact = () =>
    trackedLink(
        'installateur',
        {
            className: 'action-link',
            href: switchLinkPVTH(),
            target: '_blank',
            onClick: () => navigateSolarContactAction(getSystem()),
        },
        DIV(
            { className: 'action action-contact' },
            DIV({ className: 'action-picto' }),
            H3(
                {},
                tr.solar('solContactStr1'),
                BR({}),
                tr.solar('solContactStr2')
            )
        )
    );

export const actionChange = () =>
    trackedLink(
        'energie-verte',
        {
            className: 'action-link',
            href: tr.solar('solLinkInfoGreenEnergy'),
            target: '_blank',
        },
        DIV(
            { className: 'action action-change' },
            DIV({ className: 'action-picto' }),
            H3({}, tr.solar('solChangeStr1'), BR({}), tr.solar('solChangeStr2'))
        )
    );

export const actionInfo = () =>
    trackedLink(
        'faq',
        {
            className: 'action-link',
            href: tr.solar('solLinkFAQ'),
            target: '_blank',
        },
        DIV(
            { className: 'action action-info' },
            DIV({ className: 'action-picto' }),
            H3(
                {},
                tr.solar('solCalculInfoStrPart1'),
                BR({}),
                tr.solar('solCalculInfoStrPart2')
            )
        )
    );

export const actionPrint = () =>
    DIV(
        { className: 'action action-print' },
        DIV({ className: 'action-picto' }),
        H3({}, tr.solar('solPrintStr1'), BR({}), tr.solar('solPrintStr2'))
    );
