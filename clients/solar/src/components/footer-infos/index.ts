import { DIV, A } from 'sdi/components/elements';
import tr from 'sdi/locale';

export const contactLinks = () =>
    DIV(
        { className: 'disclaimer contact-links' },
        DIV(
            { className: 'beContact-link' },
            tr.solar('solContactLabel'),
            A(
                {
                    href: tr.solar('solLinkContactBE'),
                    target: '_blank',
                },
                tr.solar('solLinkContactBELabel')
            )
        )
    );

export const disclaimer = () =>
    DIV(
        { className: 'disclaimer' },
        DIV({}, tr.solar('solDisclaimerLink')),
        DIV(
            {},
            A(
                {
                    href: tr.solar('solLinkDisclaimer'),
                    target: '_blank',
                },
                tr.solar('moreInfos')
            )
        )
    );

export const urbisReward = () =>
    DIV(
        { className: 'reward urbis' },
        tr.solar('solUrbisLabel'),
        A({ href: tr.solar('solUrbisLink') }, 'UrbIS')
    );

export const createdBy = () =>
    DIV(
        { className: 'reward created-by' },
        tr.solar('solCreatorsLabel'),
        A({ href: 'http://www.apere.org' }, 'APERe'),
        ', ',
        A({ href: 'https://champs-libres.coop' }, 'Champs Libres'),
        ' & ',
        A(
            { href: 'https://atelier-cartographique.be' },
            'Atelier Cartographique'
        )
    );
