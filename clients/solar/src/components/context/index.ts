import * as debug from 'debug';
import bbox from '@turf/bbox';

import { DIV, IMG, NODISPLAY, SPAN, BR, A } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { withM2 } from 'sdi/util';
import { scopeOption } from 'sdi/lib';
import { FeatureCollection, Feature } from 'sdi/source';

import map from '../map';
import {
    areaExcellent,
    areaLow,
    areaMedium,
    getBuildings,
    getOrthoURL,
    getRoofs,
    streetNumber,
    totalArea,
    getPerpectiveCamera,
    hasNotComputedRoof,
    hasFlatRoof,
    areaNotComputed,
    totalComputedArea,
} from '../../queries/simulation';
import {
    perspective,
    reduceMultiPolygon,
    Reducer,
    reducePolygon,
} from './perspective';
import { Camera } from './mat';
import { vec3, vec2 } from 'gl-matrix';
import { Option, some, none } from 'fp-ts/lib/Option';
import { navigateLocate } from '../../events/route';
import { clearRoofLayer } from '../../events/map';
import { setPerspectiveCamera } from '../../events/simulation';

import { buildingAdress } from '../item-factory';
import { patternSample } from 'sdi/map/style/pattern';
import { getLayout, get3dWidth } from 'solar/src/queries/app';
import { set3dWidth } from 'solar/src/events/app';
import { makeLabel } from 'sdi/components/button';

const logger = debug('sdi:context');

const mobileAdress = () =>
    DIV({ className: 'mobile-adress' }, buildingAdress());

const makeBar = (v: number, colorClass: string, label: string) =>
    DIV(
        { className: 'bar' },
        DIV(
            { className: 'bar-color-wrapper' },
            DIV({
                className: `bar-color ${colorClass}`,
                style: {
                    width: `${
                        (v * 100) / Math.max(0.01, totalComputedArea())
                    }%`,
                },
            })
        ),
        DIV(
            { className: 'bar-label' },
            label,
            SPAN({ className: 'bar-value' }, withM2(v))
        )
    );

const barChart = () => {
    const a = areaExcellent();
    const b = areaMedium();
    const c = areaLow();

    const bars = [];
    if (a > 0) {
        bars.push(makeBar(a, 'great', tr.solar('orientationGreat')));
    }
    if (b > 0) {
        bars.push(makeBar(b, 'good', tr.solar('orientationGood')));
    }
    if (c > 0) {
        bars.push(makeBar(c, 'unusable', tr.solar('unusable')));
    }
    return DIV({ className: 'barchart' }, ...bars);
};

const backToMap = () =>
    DIV(
        'back-to-map__wrapper',
        makeLabel('navigate', 2, () => tr.solar('solBackToMap'))(() => {
            clearRoofLayer();
            navigateLocate();
        }, 'solar-btn')
    );

// const backToMap =
//     () =>
//         BUTTON({
//             className: 'illu-text solar-btn btn-level-2', onClick: () => {
//                 clearRoofLayer();
//                 navigateLocate();
//             },
//         },
//             DIV({ className: 'solar-inner-btn' }, tr.solar('solBackToMap')),
//         );

const renderNotComputedArea = () => {
    // I keep it here if we change decision soon, but I think it's better to just
    // display (and label accordingly) the total computed area.
    // if (hasNotComputedRoof()) {
    //     return DIV({ className: 'not-computed' },
    //         DIV({ className: 'not-computed-label' }, tr.solar('roofComputedArea')),
    //         DIV({ className: 'roof-area-value' }, withM2(totalArea() - areaNotComputed())));
    // }
    return NODISPLAY();
};

const roofArea = () =>
    DIV(
        { className: 'illu-text roof-area' },
        DIV({}, tr.solar('roofTotalArea')),
        DIV(
            { className: 'roof-area-value' },
            withM2(totalArea() - areaNotComputed())
        ),
        renderNotComputedArea()
    );

const wrapperOrtho = () =>
    DIV(
        { className: 'wrapper-illu' },
        DIV(
            { className: 'illu ortho' },
            DIV(
                { className: 'circle-wrapper' },
                IMG({ src: getOrthoURL(), alt: '' })
            ),
            DIV(
                { className: `map-pin top numnum-${streetNumber().length}` },
                DIV({ className: 'pin-head' }, `${streetNumber()}`)
            )
        )
    );

const wrapperPlan = () =>
    DIV(
        { className: 'wrapper-illu' },
        DIV(
            { className: 'illu plan' },
            DIV({ className: 'circle-wrapper' }, map())
        )
    );

const wrapper3D = () =>
    DIV(
        { className: 'wrapper-illu' },
        DIV(
            { className: 'illu volume' },
            DIV(
                {
                    className: 'circle-wrapper',
                    ref: update3dSize,
                },
                IMG({
                    key: 'rendered-3d',
                    src: render3D(),
                    alt: '',
                })
            )
        )
    );

const update3dSize = (elem: HTMLElement | null) => {
    if (elem !== null) set3dWidth(elem.clientWidth);
};

const render3D = () =>
    scopeOption()
        .let('width', some(get3dWidth()))
        .let('buildings', getBuildings())
        .let(
            'roofs',
            getRoofs().fold(emptyRoofs, roofs => some(roofs))
        )
        .let('camera', ({ buildings, width }) => getCamera(buildings, width))
        .let('src', ({ camera, roofs, buildings }) =>
            perspective(camera, buildings, roofs)
        )
        .fold('', ({ src }) => src);

const ALTITUDE_0 = 0;
const ALTITUDE_100 = 100;

const isExactFeature = ({ properties }: Feature): boolean => {
    if (properties !== null) {
        return 'is_exact' in properties && properties['is_exact'];
    }
    return false;
};

export const getCamera = (
    fc: FeatureCollection,
    width: number
): Option<Camera> =>
    getPerpectiveCamera().foldL(
        () => {
            const fcExact: FeatureCollection = {
                type: 'FeatureCollection',
                features: fc.features.filter(isExactFeature),
            };
            type n3 = [number, number, number];
            const [minx, miny, maxx, maxy] = bbox(fcExact);
            const cx = minx + (maxx - minx) / 2;
            const cy = miny + (maxy - miny) / 2;
            const maxxer = (acc: number, p: n3) => Math.max(acc, p[2]);
            const maxz = fcExact.features.reduce((acc, f) => {
                const geom = f.geometry;
                const gt = geom.type;
                const r: Reducer = {
                    f: maxxer,
                    init: acc,
                };
                if ('MultiPolygon' === gt) {
                    return Math.max(
                        acc,
                        reduceMultiPolygon(r, geom.coordinates as n3[][][])
                    );
                } else if ('Polygon' === gt) {
                    return Math.max(
                        acc,
                        reducePolygon(r, geom.coordinates as n3[][])
                    );
                }
                return acc;
            }, ALTITUDE_0);

            const minzzer = (acc: number, p: n3) => Math.min(acc, p[2]);
            const minz = fcExact.features.reduce((acc, f) => {
                const geom = f.geometry;
                const gt = geom.type;
                const r: Reducer = {
                    f: minzzer,
                    init: acc,
                };
                if ('MultiPolygon' === gt) {
                    return Math.min(
                        acc,
                        reduceMultiPolygon(r, geom.coordinates as n3[][][])
                    );
                } else if ('Polygon' === gt) {
                    return Math.min(
                        acc,
                        reducePolygon(r, geom.coordinates as n3[][])
                    );
                }
                return acc;
            }, ALTITUDE_100);

            if (maxz !== undefined && minz !== undefined) {
                const dist = Math.max(maxx - minx, maxy - miny) * 1;
                const pos = vec3.fromValues(cx, cy - dist, maxz + dist);
                const target = vec3.fromValues(cx, cy, maxz - dist / 2);
                const viewport = vec2.fromValues(width, width);
                const cam = {
                    pos,
                    target,
                    viewport,
                };
                setTimeout(() => setPerspectiveCamera(cam), 0);
                return some(cam);
            }
            return none;
        },
        c => some(c)
    );

export const emptyRoofs = some<FeatureCollection>({
    type: 'FeatureCollection',
    features: [],
});

const contextInfos = () =>
    DIV(
        { className: 'context-infos-wrapper' },
        backToMap(),
        roofArea(),
        barChart()
    );

const contextIllus = () =>
    DIV(
        {
            className: 'context-illus-wrapper',
            'aria-hidden': true,
        },
        wrapperOrtho(),
        wrapperPlan(),
        wrapper3D()
    );

const renderNotComputed = () =>
    DIV(
        { className: 'not-computed' },
        DIV({ className: 'not-computed-visual' }),
        DIV({ className: 'not-computed-text' }, tr.solar('solNotComputed'))
    );

const flatRoofText = () =>
    DIV(
        { className: 'flat-roof-text' },
        tr.solar('solFlatRoof1'),
        BR({}),
        tr.solar('solFlatRoof2'),
        BR({}),
        tr.solar('solFlatRoof3'),
        A({ href: tr.solar('solLinkFAQ') }, tr.solar('solFlatRoof3Link')),
        tr.solar('solFlatRoof4')
    );

const renderFlat = () =>
    DIV(
        { className: 'flat-roof' },
        DIV(
            { className: 'flat-roof-visual' },
            DIV(
                { className: 'sample-box' },
                patternSample(1.5, 90, 'white', '#8db63c')(64, 64)
            ),
            DIV(
                { className: 'sample-box' },
                patternSample(1.5, 90, 'white', '#ebe316')(64, 64)
            ),
            DIV(
                { className: 'sample-box' },
                patternSample(1.5, 90, 'white', '#006f90')(64, 64)
            )
        ),
        flatRoofText()
    );

const renderDisclaimers = () => {
    const notComputed = hasNotComputedRoof();
    const flat = hasFlatRoof();
    if (getLayout() === 'Loader' || (!notComputed && !flat)) {
        return NODISPLAY();
    }

    return DIV(
        { className: 'disclaimer' },
        flat ? renderFlat() : NODISPLAY(),
        notComputed ? renderNotComputed() : NODISPLAY()
    );
};

export const context = () =>
    DIV(
        { className: 'context' },
        mobileAdress(),
        contextIllus(),
        contextInfos(),
        renderDisclaimers()
    );

export const contextWithoutDisclaimer = () =>
    DIV(
        { className: 'context' },
        mobileAdress(),
        contextIllus(),
        contextInfos()
    );

logger('loaded');
