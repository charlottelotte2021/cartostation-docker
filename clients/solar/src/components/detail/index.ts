import { DIV, H1 } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { context } from '../context';
import { summaryDetailed } from '../summary';
import {
    disclaimer,
    contactLinks,
    createdBy,
    urbisReward,
} from '../footer-infos';

import { actionContact, actionChange, actionInfo } from '../action';

import {
    calcArea,
    calcAutoproduction,
    calcConsumption,
    calcFinanceCost,
    calcTechnology,
    calcLoan,
    calcObstacle,
    calcConsumptionThermal,
    calcTechnologyThermal,
    calcFinanceThermalGain,
    calcFinanceThermalCost,
    calcLoanThermal,
} from '../adjust';

import { getSystem } from '../../queries/simulation';
import { setSystem } from '../../events/simulation';

import { renderRadioIn } from 'sdi/components/input';
// import { identity } from 'io-ts';
import { System } from 'solar/src/shape';

// const toggleSystem = toggle(
//     () => getSystem() === 'photovoltaic',
//     v => v ? setSystem('photovoltaic') : setSystem('thermal'));

const renderSystem = (system: System) =>
    system === 'photovoltaic'
        ? tr.solar('solPhotovoltaic')
        : tr.solar('solThermal');
const solarToggle = renderRadioIn(
    'toggle-solar-system',
    renderSystem,
    setSystem
);

const calculatorTitle = () =>
    DIV(
        { className: 'adjust-item calculator-header' },
        H1(
            { className: 'calculator-title' },
            tr.solar('solAdjustStr1'),
            ' ',
            tr.solar('solAdjustStr2')
        ),
        // toggleSystem('solPhotovoltaic', 'solSolarWaterHeater'),
        solarToggle(['photovoltaic', 'thermal'], getSystem(), 'toggle')
    );

const photovoltaicWidgets = () =>
    DIV(
        { className: 'calculator' },
        calculatorTitle(),
        calcObstacle(),
        calcTechnology(),
        calcArea(),
        calcConsumption(),
        calcAutoproduction(),
        calcFinanceCost(),
        calcLoan()
    );

const thermalWidgets = () =>
    DIV(
        { className: 'calculator' },
        calculatorTitle(),
        calcTechnologyThermal(),
        calcConsumptionThermal(),
        calcFinanceThermalGain(),
        calcFinanceThermalCost(),
        calcLoanThermal()
    );

const adjustWidgets = () => {
    const system = getSystem();
    switch (system) {
        case 'photovoltaic':
            return photovoltaicWidgets();
        case 'thermal':
            return thermalWidgets();
    }
};

const action = () =>
    DIV(
        { className: 'actions' },
        actionContact(),
        actionChange(),
        actionInfo()
    );

const contentFooter = () =>
    DIV(
        { className: 'footer-infos' },
        contactLinks(),
        disclaimer(),
        urbisReward(),
        createdBy()
    );

const sidebar = () => DIV({ className: 'sidebar' }, summaryDetailed());

const content = () =>
    DIV(
        { className: 'content' },
        context(),
        adjustWidgets(),
        sidebar(),
        action(),
        contentFooter()
    );

const render = () =>
    DIV({ className: 'solar-main-and-right-sidebar' }, content());

export default render;
