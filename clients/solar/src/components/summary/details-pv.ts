import { DIV, SPAN, H2 } from 'sdi/components/elements';
import tr from 'sdi/locale';
import {
    withPercent,
    withEuro,
    withM2,
    withKWc,
    withKWhY,
    withYear,
    withTCO2Y,
} from 'sdi/util';

import {
    getPanelUnits,
    pvTechnologyLabel,
    totalArea,
    getAnimatedValuePv,
    usableRoofArea,
    getObstacleArea,
} from '../../queries/simulation';
import { buildingAdress } from '../item-factory';
import { clearInputs } from '../../events/simulation';
import { renderPDF } from './print';
import { SolarMessageKey } from 'solar/src/locale';
import { makeLabel } from 'sdi/components/button';

const vk = <T>(v: T, key: SolarMessageKey, vkClass = '') =>
    DIV(
        { className: `vk ${vkClass}` },
        SPAN({ className: 'key' }, tr.solar(key)),
        SPAN({ className: 'value' }, `${v}`)
    );

const vks = <T>(v: T, key: string, vkClass = '') =>
    DIV(
        { className: `vk ${vkClass}` },
        SPAN({ className: 'key' }, key),
        SPAN({ className: 'value' }, `${v}`)
    );

const sumRooftop = () =>
    DIV(
        { className: 'sum-wrapper' },
        H2({}, tr.solar('solMyRooftop')),
        vk(withM2(totalArea()), 'solTotalSurface'),
        vk(withM2(getObstacleArea()), 'obstacleEstimation'),
        vk(withM2(usableRoofArea()), 'solUsableRoofArea')
    );

const sumEnergy = () =>
    DIV(
        { className: 'sum-wrapper' },
        H2({}, tr.solar('solMyEnergy')),
        vk(
            withKWhY(getAnimatedValuePv('annualProduction')),
            'solProductionPanels'
        ),
        vk(
            withKWhY(getAnimatedValuePv('annualConsumption')),
            'solHomeConsumption'
        ),
        vk(
            withPercent(getAnimatedValuePv('autonomy', 0.001) * 100),
            'solarAutonomy'
        ),
        vk(
            withTCO2Y(getAnimatedValuePv('savedCO2emissions') / 1000 / 10, 1),
            'gainEnvironment',
            'gain-env'
        )
    );

const sumInstallation = () =>
    DIV(
        { className: 'sum-wrapper' },
        H2({}, tr.solar('solMyInstallation')),
        vks(
            getPanelUnits(),
            `${tr.solar('solNumberOfPanels')} (${tr.solar(
                pvTechnologyLabel()
            )})`
        ),
        vk(withM2(getAnimatedValuePv('maxArea')), 'solInstallationSurface'),
        vk(withKWc(getAnimatedValuePv('power'), 1), 'solTotalPower'),
        vk(withYear(25), 'solInstallationLifeTime')
    );

const sumFinance = () =>
    DIV(
        { className: 'sum-wrapper' },
        H2({}, tr.solar('solMyFinance')),
        vk(withEuro(getAnimatedValuePv('installationCost')), 'buyingPrice'),
        vk(
            withEuro(getAnimatedValuePv('CVAmountYear25')),
            'gainGreenCertif25Y'
        ),
        vk(
            withEuro(getAnimatedValuePv('selfConsumptionAmountYear25')),
            'gainElecInvoice25Y'
        ),
        vk(
            withEuro(
                getAnimatedValuePv('totalGain25Y') -
                    getAnimatedValuePv('installationCost')
            ),
            'gainTotal25Y'
        ),
        vk(withYear(getAnimatedValuePv('returnTime')), 'returnTime')
    );

const reset = () =>
    makeLabel('reset', 2, () => tr.solar('resetValue'))(
        () => clearInputs(),
        'solar-btn'
    );

// const reset =
//     () => BUTTON({
//         className: 'solar-btn btn-level-2 reset',
//         onClick: () => clearInputs(),
//     },
//         DIV({ className: 'solar-inner-btn' },
//             tr.solar('resetValue'),
//         ));

const printBtn = () =>
    makeLabel('open', 1, () => tr.solar('solPrintStr3'))(
        () => renderPDF(),
        'solar-btn'
    );

// const printBtn =
//     () => BUTTON({
//         className: 'solar-btn print',
//         onClick: () => renderPDF(),
//     },
//         DIV({ className: 'solar-inner-btn' },
//             tr.solar('solPrintStr3'),
//         ));

const footer = () => DIV({ className: 'detail-footer' }, printBtn(), reset());

export const summaryDetailedPhotovoltaic = () =>
    DIV(
        { className: 'summary-detailled' },
        buildingAdress(),
        sumRooftop(),
        sumEnergy(),
        sumInstallation(),
        sumFinance(),
        footer()
    );
