import { DIV, SPAN, H2 } from 'sdi/components/elements';
import tr from 'sdi/locale';
import {
    withEuro,
    withLiter,
    withM2,
    withKWhY,
    withPercent,
    withTCO2Y,
    withYear,
} from 'sdi/util';

import { buildingAdress } from '../item-factory';
import {
    totalArea,
    usableRoofArea,
    getObstacleArea,
    getAnimatedValueThermal,
} from '../../queries/simulation';
import { clearInputs } from '../../events/simulation';
import { renderPDF } from './print';
import { SolarMessageKey } from 'solar/src/locale';
import { makeLabel } from 'sdi/components/button';

const vk = <T>(v: T, key: SolarMessageKey, vkClass = '') =>
    DIV(
        { className: `vk ${vkClass}` },
        SPAN({ className: 'key' }, tr.solar(key)),
        SPAN({ className: 'value' }, `${v}`)
    );

const sumRooftop = () =>
    DIV(
        { className: 'sum-wrapper' },
        H2({}, tr.solar('solMyRooftop')),
        vk(withM2(totalArea()), 'solTotalSurface'),
        vk(withM2(getObstacleArea()), 'obstacleEstimation'),
        vk(withM2(usableRoofArea()), 'solUsableRoofArea')
    );

const sumEnergy = () =>
    DIV(
        { className: 'sum-wrapper' },
        H2({}, tr.solar('solMyEnergy')),
        vk(
            withKWhY(getAnimatedValueThermal('annualProduction')),
            'solSolarProdYear'
        ),
        vk(
            withKWhY(getAnimatedValueThermal('annualConsumption')),
            'solSolarConsumptionYear'
        ),
        vk(
            withPercent(getAnimatedValueThermal('autonomyRate') * 100),
            'solSolarRateArea'
        ),
        vk(
            withTCO2Y(
                getAnimatedValueThermal('savedCO2emissions') / 1000 / 10,
                1
            ),
            'gainEnvironment'
        )
    );

const sumInstallation = () =>
    DIV(
        { className: 'sum-wrapper' },
        H2({}, tr.solar('solMyInstallation')),
        vk(2, 'solNumberOfPanels'),
        vk(withM2(4.5, 1), 'surface'),
        vk(withLiter(300), 'solWaterStorage')
        // vk(withYear(25), 'solInstallationLifeTime'),
    );

const sumFinance = () =>
    DIV(
        { className: 'sum-wrapper' },
        H2({}, tr.solar('solMyFinance')),
        vk(
            withEuro(getAnimatedValueThermal('installationCost')),
            'buyingPrice'
        ),
        vk(withEuro(getAnimatedValueThermal('grant')), 'bonus'),
        vk(
            withEuro(getAnimatedValueThermal('thermalGain25')),
            'gainEnergyInvoice25Y'
        ),
        vk(withYear(getAnimatedValueThermal('returnTime')), 'returnTime')
    );

const infosThermal = () =>
    DIV(
        { className: 'infos-thermal-wrapper' },
        sumRooftop(),
        sumEnergy(),
        sumInstallation(),
        sumFinance()
    );

const reset = () =>
    makeLabel('reset', 2, () => tr.solar('resetValue'))(
        () => clearInputs(),
        'solar-btn'
    );

const printBtn = () =>
    makeLabel('open', 1, () => tr.solar('solPrintStr3'))(
        () => renderPDF(),
        'solar-btn'
    );

// const reset =
//     () => BUTTON({
//         className: 'solar-btn btn-level-2 reset',
//         onClick: () => clearInputs(),
//     },
//         DIV({ className: 'solar-inner-btn' },
//             tr.solar('resetValue'),
//         ));

// const printBtn =
//     () => BUTTON({
//         className: 'solar-btn print',
//         onClick: () => renderPDF(),
//     },
//         DIV({ className: 'solar-inner-btn' },
//             tr.solar('solPrintStr3'),
//         ));

const footer = () => DIV({ className: 'detail-footer' }, printBtn(), reset());

export const summaryDetailedThermal = () =>
    DIV(
        { className: 'summary-detailled' },
        buildingAdress(),
        infosThermal(),
        footer()
    );
