import { DIV, LI, SPAN, UL } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { withEuro, withTonsCO2 } from 'sdi/util';

import { buildingAdress } from '../item-factory';

import { getOutputPv, getPanelUnits } from '../../queries/simulation';
import { SolarMessageKey } from 'solar/src/locale';

const vk = <T>(v: T, key: SolarMessageKey, vkClass = '') =>
    LI(
        { className: `vk ${vkClass}` },
        SPAN({ className: 'key' }, tr.solar(key)),
        SPAN({ className: 'value' }, `${v}`)
    );

const gains = () => SPAN({}, withEuro(getOutputPv('totalGain10Y')));

const sumPotentialLabel = () =>
    DIV(
        { className: 'potential-label' },
        DIV(
            {},
            SPAN({}, tr.solar('solSolarPotentialStr1')),
            SPAN(
                { className: 'highlight-value' },
                getPanelUnits(),
                ` ${tr.solar('solPanelsPV')}`
            ),
            SPAN({}, ` ${tr.solar('solSolarPotentialStr2')} `),
            SPAN(
                { className: 'highlight-value' },
                `${tr.solar('solSolarPotentialStr3')} ${tr.solar(
                    'solSolarPotentialStr4'
                )} `,
                gains(),
                ` ${tr.solar('solOn10Years')}`
            )
        )
    );

const sumPotentialValues = () =>
    UL(
        { className: 'potential-values' },
        vk(
            withEuro(getOutputPv('CVAmountYear10')),
            'gainGreenCertif',
            'green-cert'
        ),
        vk(
            withEuro(getOutputPv('selfConsumptionAmountYear10')),
            'gainElecInvoice',
            'gain-elec'
        ),
        vk(
            withTonsCO2(getOutputPv('savedCO2emissions') / 1000, 1),
            'gainEnvironment',
            'gain-env'
        ),
        vk(
            withEuro(getOutputPv('installationCost', 0)),
            'buyingPrice',
            'buying-price'
        )
    );

export const summary = () =>
    DIV(
        { className: 'summary' },
        buildingAdress(),
        sumPotentialLabel(),
        sumPotentialValues()
    );
