import { DIV, SPAN, BR, LI, UL } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { withEuro, withTonsCO2 } from 'sdi/util/';

import { buildingAdress } from '../item-factory';

import { getOutputThermal } from '../../queries/simulation';
import { SolarMessageKey } from 'solar/src/locale';

const vk = <T>(v: T, key: SolarMessageKey, vkClass = '') =>
    LI(
        { className: `vk ${vkClass}` },
        SPAN({ className: 'key' }, tr.solar(key)),
        SPAN({ className: 'value' }, `${v}`)
    );

// const gains =
//     () => SPAN({}, withEuro(getOutputThermal('grant') + getOutputThermal('thermalGain10')));

const sumPotentialLabel = () =>
    DIV(
        { className: 'potential-label' },
        DIV(
            {},
            SPAN({}, tr.solar('solSolarPotentialStr1')),
            SPAN(
                { className: 'highlight-value' },
                `2 ${tr.solar('solPanelsTH')}.`
            ),
            BR({}),
            SPAN({}, `${tr.solar('solSummary10Y')} : `),
            BR({}),
            BR({})
        )
    );

const sumPotentialValues = () =>
    UL(
        { className: 'potential-values' },
        vk(withEuro(getOutputThermal('grant')), 'bonus', 'green-cert'),
        vk(
            withEuro(getOutputThermal('thermalGain10')),
            'gainEnergyInvoice',
            'gain-thermal'
        ),
        vk(
            withTonsCO2(getOutputThermal('savedCO2emissions') / 1000, 1),
            'gainEnvironment',
            'gain-env'
        ),
        vk(
            withEuro(getOutputThermal('installationCost', 0)),
            'buyingPrice',
            'buying-price'
        )
    );

export const summary = () =>
    DIV(
        { className: 'summary' },
        buildingAdress(),
        sumPotentialLabel(),
        sumPotentialValues()
    );
