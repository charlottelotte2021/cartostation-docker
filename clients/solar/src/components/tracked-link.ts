import { A } from 'sdi/components/elements';
import { ClassAttributes, AllHTMLAttributes, ReactNode } from 'react';
import { activityLogger } from '../events/app';
import { linkAction } from 'sdi/activity/index';

export const trackedLink = (
    identifier: string,
    props: ClassAttributes<HTMLAnchorElement> &
        AllHTMLAttributes<HTMLAnchorElement>,
    ...children: ReactNode[]
) =>
    A(
        {
            ...props,
            onClick: _e => {
                activityLogger(linkAction(identifier));
            },
        },
        ...children
    );
