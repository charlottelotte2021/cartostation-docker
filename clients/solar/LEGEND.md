légende
=======

La légende des toits de la carte solaire se construit en quatres étapes. Les étapes sont détaillées dans ce document, et accompagnées de l'extrait de code correspondant.

## 1 -- valeurs configurées

Un modéle *Django* administrable retient la configuration générale de la carte solaire. Certains champs de cette configuration sont utilisés pour la légende:

 - `max_solar_productivity`
 - `max_solar_irradiance`
 - `medium_solar_productivity`
 - `low_productivity_limit`
 - `flat_roof_tilt`
 

Les valeurs correspondantes doivent provenir de la configuration courante, c.-à-d. celle dont le champs `current` est vrai.

## 2 -- propriétés d'un pan de toiture

Les valeurs prises de la base de données sont transformées afin de fournir deux valeurs clés, 

 - l'inclinaison sur la bas de la géométrie du pan;
 - la productivité comme fonction de l'irradiance  multipliée par `max_solar_productivity` et divisée par `max_solar_irradiance`




```python
# solar_loader/handler.py ligne 53

GEOM = 1
AREA = 2
IRRADIANCE = 3

def make_roof_props_from_row(solar_sim, row):
    area = float(row[AREA])
    r = float(row[IRRADIANCE])
    geom = row[GEOM]
    return {
        "area": area,
        "tilt": get_roof_tilt(geom),
        "azimuth": get_roof_azimuth(geom),
        "irradiance": r / 1000.0,
        "productivity": (r / 1000.0)
        * solar_sim.max_solar_productivity
        / solar_sim.max_solar_irradiance,
    }
```
 
## 3 -- catégorisation des pans de toitures

Pour chaque configuration des valeurs d'irradiance, inclinaison, aire et productivité, chaque pan se voit attribué une catégorie.

 - `not-computed`: l'irradiance est nulle
 - `unusable`: l'aire est inférieure à 5m2 ou la productivité est inférieure à `low_productivity_limit`
 - `good`: la productivité est supérieure ou égale à `low_productivity_limit` et inférieure à `medium_solar_productivity`
 - `great`: tous les autres cas, implicitement une aire supérieure à 5m2 et une productivité supérieure ou égale à `medium_solar_productivity`

De plus, pour les cas où l'inclinaison est supérieure à `flat_roof_tilt`, les catégories `unusable`, `good` et `great` deviennent respectivement `unusable-flat`, `good-flat` et `great-flat`.

 ```typescript
// clients/solar/src/events/app.ts ligne 115

 const tagFeature = (f: Feature): Properties => {
    const area = getFeatureProp(f, 'area', 0.000001);
    const irradiance = getFeatureProp(f, 'irradiance', 0);
    const productivity = getFeatureProp(f, 'productivity', 0);
    const tilt = getFeatureProp(f, 'tilt', 35);
    const tag = 'great';
    const andFlat = (t: Tag): Tag => {
        if (tilt > flatRoofTilt()) {
            return t;
        }
        switch (t) {
            case 'great':
                return 'great-flat';
            case 'good':
                return 'good-flat';
            case 'unusable':
                return 'unusable-flat';
            default:
                return t;
        }
    };

    if (0 === irradiance) {
        return { tag: 'not-computed' };
    } else if (area < 5) {
        return { tag: andFlat('unusable') };
    } else if (productivity < prodThresholdMedium()) {
        return { tag: andFlat('unusable') };
    } else if (
        productivity >= prodThresholdMedium() &&
        productivity < prodThresholdHigh()
    ) {
        return { tag: andFlat('good') };
    }
    return { tag: andFlat(tag) };
};
 ```


## 4 -- assignation de style

Toutes les entités ont  une couleur de filet de #666 et une épaisseur de 1px.

Les styles de remplissage sont les suivants:

 - `great` : couleur unie #8db63c
 - `good` : couleur unie #ebe316
 - `unusable` : couleur unie #006f90
 - `great-flat` : hachure blanche à 90 degrés sur fond #8db63c
 - `good-flat` : hachure blanche à 90 degrés sur fond #ebe316
 - `unusable-flat` : hachure blanche à 90 degrés sur fond #006f90
 - `not-computed`: couleur unie noire

```typescript
const roofPrepper: Prepper = (c, f) => {
    c.save();
    c.strokeStyle = '#666';
    c.lineWidth = 1;

    switch (getProp(f, 'tag', 'great' as Tag)) {
        case 'great':
            c.fillStyle = '#8db63c';
            break;
        case 'good':
            c.fillStyle = '#ebe316';
            break;
        case 'unusable':
            c.fillStyle = '#006f90';
            break;
        case 'great-flat':
            c.fillStyle = makePattern(1.5, 90, 'white', '#8db63c');
            break;
        case 'good-flat':
            c.fillStyle = makePattern(1.5, 90, 'white', '#ebe316');
            break;
        case 'unusable-flat':
            c.fillStyle = makePattern(1.5, 90, 'white', '#006f90');
            break;
        case 'not-computed':
            c.fillStyle = 'black';
            break;
    }

    return () => c.restore();
};

```