import { DIV, H1 } from 'sdi/components/elements';
import tr from 'sdi/locale';


const render =
    () =>
        DIV({},
            H1({}, tr.angled('domains')),
            DIV({}, tr.angled('loadingData')));

export default render;
