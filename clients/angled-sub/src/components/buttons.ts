import buttonFactory from 'sdi/components/button';
import {
    queryK,
    dispatchK,
} from 'sdi/shape';
import { isUserLogged, getRoot } from 'sdi/app/queries';
import { getPathElements } from 'sdi/util';

export const { makeIcon, makeLabel, makeLabelAndIcon } = buttonFactory(
    queryK('component/button'), dispatchK('component/button'));

export const isActiveButton =
    () => isUserLogged() ? 'active' : 'inactive';

export const goToLoginPage =
    () => {
        const path = getPathElements(document.location.pathname);
        const root = getPathElements(getRoot());
        const next = path.filter((p, i) => i < root.length ? p !== root[i] : true).join('/');
        const loginUrl = `${getRoot()}login/${next}`;
        window.location.assign(loginUrl);
            };
