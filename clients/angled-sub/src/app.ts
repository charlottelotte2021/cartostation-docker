import * as debug from 'debug';

import { DIV } from 'sdi/components/elements';
import header from 'sdi/components/header';
import footer from 'sdi/components/footer';
import { loop, getUserId, getApiUrl } from 'sdi/app';

import { loadUser, setLayout } from './events/app';
import { loadTerms, loadDomains } from 'angled-core/events/universe';
import { DomainList, DomainForm } from './components/domain';
import { TermDetail, TermList, TermForm } from './components/term';
import splash from './components/splash';
import { getLayout } from './queries/app';
import { loadRoute } from './events/route';

const logger = debug('sdi:app');



const wrappedMain = (name: string, ...elements: React.DOMElement<{}, Element>[]) => (
    DIV({ className: 'universe-inner' },
        header('angled:universe')(() => DIV({}))(),
        DIV({ className: `main ${name}` }, ...elements),
        footer())
);


const render =
    () => {
        switch (getLayout()) {
            case 'splash': return wrappedMain('spash', splash());
            case 'domain-list': return wrappedMain('domain-list',
                DomainList());
            case 'domain-select': return wrappedMain('domain-select',
                DomainList(), TermList());
            case 'domain-form': return wrappedMain('domain-form',
                DomainList(), DomainForm());
            case 'term-select': return wrappedMain('term-select',
                DomainList(), TermList(), TermDetail());
            case 'term-form': return wrappedMain('term-form',
                DomainList(), TermList(), TermForm());
        }
    };

const effects =
    (initialRoute: string[]) =>
        () => {
            getUserId()
                .map(userId =>
                    loadUser(getApiUrl(`users/${userId}`)));
            loadDomains()
                .then(() => loadTerms())
                .then(() => {
                    setLayout('domain-list');
                    loadRoute(initialRoute);
                });
        };

const app = (initialRoute: string[]) => loop('univers', render, effects(initialRoute));
export default app;

logger('loaded');


