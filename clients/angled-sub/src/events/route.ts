import * as debug from 'debug';

import { index } from 'fp-ts/lib/Array';
import { literal, union, TypeOf } from 'io-ts';

import { Router, Path } from 'sdi/router';
import { getNumber } from 'sdi/util';

import { setLayout } from './app';
import { } from './sub';


const logger = debug('sdi:route');

const RouteIO = union([
    literal('index'),
    literal('domain'),
    literal('term'),
    literal('domain-form'),
    literal('term-form'),
]);
type Route = TypeOf<typeof RouteIO>;

const { route, navigate } = Router<Route>('angled-universe');




// const domainParser =
//     (p: Path) => index(0, p).chain(getNumber);

// const termParser =
//     (p: Path) => index(0, p).chain(getNumber);

route('index', (_p) => {
    setLayout('domain-list');
});

// route('domain',
//     r => r.foldL(
//         () => logger('failed on parser for domain', r),
//         (id) => {
//             setDomain(id);
//             setLayout('domain-select');
//         }), domainParser);



export const loadRoute =
    (initial: string[]) =>
        index(0, initial)
            .map(prefix =>
                RouteIO
                    .decode(prefix)
                    .map(c => navigate(c, initial.slice(1))));

export const navigateHome =
    () => navigate('index', []);


logger('loaded');
