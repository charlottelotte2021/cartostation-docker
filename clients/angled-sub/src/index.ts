
import * as debug from 'debug';

import 'sdi/polyfill';
import { source } from 'sdi/source';
import { IShape, configure, defaultShape } from 'sdi/shape';
import { defaultCollection } from 'sdi/util';

import './shape';
import 'angled-core/locale';

import App from './app';

const logger = debug('sdi:index');


const displayException = (err: string) => {
    const title = document.createElement('h1');
    const errorBlock = document.createElement('div');
    const link = document.createElement('a');
    const body = document.body;
    while (body.firstChild) {
        body.removeChild(body.firstChild);
    }
    title.appendChild(document.createTextNode('Sorry, Application Crashed'));
    err.split('\n').forEach((line) => {
        const e = document.createElement('pre');
        e.appendChild(document.createTextNode(line));
        errorBlock.appendChild(e);
    });
    link.setAttribute('href', document.location.href);
    link.appendChild(document.createTextNode('Reload the application'));

    body.appendChild(title);
    body.appendChild(link);
    body.appendChild(errorBlock);

};

export const main =
    (SDI: any) => {
        const initialState: IShape = {
            ...defaultShape(),
            'app/user': SDI.user,
            'app/root': SDI.root,
            'app/api-root': SDI.api,
            'app/csrf': SDI.csrf,
            'app/route': SDI.args,
            'app/lang': 'fr',
            'app/layout': 'splash',
            'component/button': {},

            'data/user': null,
            'data/alias': [],
        };

        try {
            const start = source<IShape>(['app/lang']);
            const store = start(initialState);
            configure(store);
            const app = App(SDI.args)(store);
            app();
        }
        catch (err) {
            displayException(`${err}`);
        }
    };


logger('loaded');
