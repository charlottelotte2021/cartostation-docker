import {
    fetchIO,
    IUser,
    IUserIO,
} from 'sdi/source';


export const fetchUser =
    (url: string): Promise<IUser> =>
        fetchIO(IUserIO, url);
