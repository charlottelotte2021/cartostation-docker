import { query } from 'sdi/shape';


export const getUserData =
    () => query('data/user');

export const getLayout =
    () => query('app/layout');

