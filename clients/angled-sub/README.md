This application is a first draft for subscriptions within the `angled` apps constellation.

For now this application is not used, and its main functionalities has been moved into _angled-project_.
