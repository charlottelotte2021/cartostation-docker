/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { query } from 'sdi/shape';
import { fromNullable } from 'fp-ts/lib/Option';

export const getUserData = () => query('data/user');

// export const getLayout = (): Layout => {
//     const ll = query('app/layout');
//     if (ll.length === 0) {
//         throw new Error('PoppingEmptyLayoutList');
//     }
//     return ll[ll.length - 1];
// };
export const getLayout = () => query('app/layout');

export const isLayoutFull = () => {
    switch (getLayout()) {
        case 'all-permits':
        case 'env':
        case 'home':
        case 'loader':
        case 'map':
        case 'no-permit':
        case 'print':
        case 'urba':
            return false;
        case 'all-permits-full':
        case 'env-full':
        case 'no-permit-full':
        case 'urba-full':
            return true;
    }
};

export const getMapInfo = () => {
    const mid = query('infiltration/map');
    const info = query('data/maps').find(m => m.id === mid);
    return info !== undefined ? info : null;
};

export const getMapInfoOpt = () => fromNullable(getMapInfo());

export const getCapakey = () => fromNullable(query('app/capakey'));
