/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { fromNullable, some, none, Option } from 'fp-ts/lib/Option';

import { query, queryK } from 'sdi/shape';
import { fnOpt0, scopeOption } from 'sdi/lib';
import { getLang, SyntheticLayerInfo } from 'sdi/app';
import {
    findMapBaseLayer,
    MessageRecordLang,
    getMessageRecord,
    FeatureCollection,
} from 'sdi/source';
import { Either, left, right } from 'fp-ts/lib/Either';

export const MINI_MAP = 'mini-infiltration-map';
export const MAIN_MAP = 'main-infiltration-map';
export type MapName = typeof MINI_MAP | typeof MAIN_MAP;

export const getCurrentBaseLayerName = fnOpt0(() => {
    const mid = query('app/current-map');
    const map = query('data/maps').find(m => m.id === mid);
    if (map) {
        return map.baseLayer;
    }
    return null;
});

export const getCurrentBaseLayerOpt = () =>
    getCurrentBaseLayerName().chain(name =>
        findMapBaseLayer(query('data/baselayers'), name)
    );

export const findMap = (mid: string) =>
    fromNullable(query('data/maps').find(m => m.id === mid));

export const getCurrentMapInfo = () =>
    fromNullable(query('infiltration/map')).chain(findMap);

export const findBaseLayer =
    // (name: string) => fromNullable(query('data/baselayers')[name]);
    (name: string) => {
        const [serviceId, layerName] = name.split('/');
        return fromNullable(
            query('data/baselayers').find(s => s.id === serviceId)
        ).chain(s =>
            fromNullable(s.layers.find(l => l.codename === layerName))
        );
    };

export const getBaseLayer = () =>
    getCurrentMapInfo().chain(map => findBaseLayer(map.baseLayer));

export const getInteraction = queryK('port/map/interaction');

export const getCurrentBaseLayer = () => getCurrentBaseLayerOpt().toNullable();

export const getView = (name: MapName) => {
    switch (name) {
        case MAIN_MAP:
            return queryK('port/map/main/view');
        case MINI_MAP:
            return queryK('port/map/mini/view');
    }
};

export const getMetadata = (id: string) =>
    fromNullable(query('data/metadata')[id]);

export const getSyntheticLayerInfo = (
    layerId: string
): Option<SyntheticLayerInfo> =>
    scopeOption()
        .let('info', getCurrentMapInfo())
        .let('layerInfo', ({ info }) =>
            fromNullable(info.layers.find(l => l.id === layerId))
        )
        .let('metadata', ({ layerInfo }) => getMetadata(layerInfo.metadataId))
        .map(({ layerInfo, metadata }) => ({
            name: getMessageRecord(metadata.resourceTitle),
            info: layerInfo,
            metadata,
        }));

export const getLayerData = (
    ressourceId: string
): Either<string, Option<FeatureCollection>> => {
    const layers = query('data/layers');
    const errors = query('data/remote/errors');
    if (ressourceId in layers) {
        return right(some<FeatureCollection>(layers[ressourceId]));
    } else if (ressourceId in errors) {
        return left(errors[ressourceId]);
    }
    return right(none);
};

export const getMiniMapView = getView(MINI_MAP);
export const getMainMapView = getView(MAIN_MAP);

export const geocoderResponse = () =>
    fromNullable(query('component/geocoder/response'));

interface GeocoderInput {
    addr: string;
    lang: MessageRecordLang;
}

export const geocoderInput = (): Option<GeocoderInput> => {
    const addr = query('component/geocoder/input').trim();
    const lang = getLang();
    if (addr.length > 0) {
        const i: GeocoderInput = { addr, lang };
        return some(i);
    }
    return none;
};
