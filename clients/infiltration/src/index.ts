/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'sdi/polyfill';
import './shape';
import './locale';

import * as debug from 'debug';
import { source, AppConfigIO, getMessage } from 'sdi/source';
import { IShape, configure, defaultShape } from 'sdi/shape';

import { displayException } from 'sdi/app';
import {
    defaultAppShape,
    defaultInfiltrationShape,
    defaultMapShape,
} from './shape';
import app from './app';

const logger = debug('sdi:index');

export const main = (SDI: any) => {
    AppConfigIO.decode(SDI).fold(
        errors => {
            const textErrors = errors.map(e => getMessage(e.value, e.context));
            displayException(textErrors.join('\n'));
        },
        config => {
            const initialState: IShape = {
                'app/codename': 'infiltration',
                ...defaultShape(config),
                ...defaultInfiltrationShape(),
                ...defaultAppShape(config),
                ...defaultMapShape(),

                'data/user': null,
            };

            try {
                const stateActions = source<IShape>(['app/lang']);
                const store = stateActions(initialState);
                configure(store);

                const start = app(config.args)(store);
                start();
            } catch (err) {
                displayException(`${err}`);
            }
        }
    );
};

logger('loaded');
