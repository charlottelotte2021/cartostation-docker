// URLS
const attestationURLfr =
    'https://environnement.brussels/thematiques/sols/lattestation-du-sol';
const facilitatorURLfr =
    'https://environnement.brussels/thematiques/sols/facilitateur-sol';
const soilConditionURLfr =
    'https://environnement.brussels/thematiques/sols/les-etudes-de-la-pollution-du-sol/les-differentes-etudes/reconnaissance-de-letat-du';
const soilExpertURLfr =
    'https://app.bruxellesenvironnement.be/listes/?nr_list=EPS0001';
const riskURLfr =
    'https://environnement.brussels/thematiques/sols/les-etudes-de-la-pollution-du-sol/les-differentes-etudes/etude-de-risque';
const riskActivityURLfr =
    'https://environnement.brussels/thematiques/sols/linventaire-de-letat-du-sol/quest-ce-quune-activite-risque';

const attestationURLnl =
    'https://leefmilieu.brussels/themas/bodem/bodemverontreiniging/het-bodemattest';
const facilitatorURLnl =
    'https://leefmilieu.brussels/themas/bodem/bodemfacilitator';
const soilConditionURLnl =
    'https://leefmilieu.brussels/themas/bodem/bodemverontreiniging/de-bodemonderzoeken/de-verschillende-bodemonderzoeken/verkennend';
const soilExpertURLnl =
    'https://app.leefmilieubrussel.be/lijsten/?nr_list=EPS0001';
const riskURLnl =
    'https://leefmilieu.brussels/themas/bodem/bodemverontreiniging/de-bodemonderzoeken/de-verschillende-bodemonderzoeken/risico';
const riskActivityURLnl =
    'https://leefmilieu.brussels/themas/bodem/bodemverontreiniging/de-inventaris-van-de-bodemtoestand/wat-een-risicoactiviteit';

export const infLocatePitchFR = `
**L'infiltration d'eau de pluie** est gênée par l'urbanisation.  
Un problème croissant qui amplifie les inondations,  
engorge les systèmes d'épuration,  
diminue les stocks d'eau souterrains  
et renforce les ilôts de chaleur.`;

export const infLocatePitchNL = `
**De infiltratie van regenwater** wordt belemmerd door verstedelijking.  
Een groeiend probleem dat overstromingen versterkt, rioolsystemen verzadigt,  
ondergrondse watervoorraden vermindert en hitte-eilanden versterkt. 
`;

export const infLegalEnvFR = `
L’**objectif** à prendre en compte dans le cadre de la demande d'un **permis d'environnement** doit être un **zéro 
rejet hors de la parcelle**. Un maximum d’eau de pluie devra dès lors être infiltré à la parcelle selon les prescriptions 
du permis d’environnement. Plus d'informations: 
https://environnement.brussels/le-permis-denvironnement/les-conditions-generales-et-specifiques/les-conditions-generales-dexploitation/les-obligations-en-matiere-de-gestion-des-eaux-pluviales.
`;
export const infLegalEnvNL = `
De **doelstelling** waarmee rekening moet worden gehouden bij de aanvraag van een **milieuvergunning** is een **nullozing buiten het 
perceel**. Een maximale hoeveelheid regenwater moet bijgevolg op het perceel worden geïnfiltreerd overeenkomstig de voorschriften 
van de milieuvergunning. Meer informatie: 
https://leefmilieu.brussels/de-milieuvergunning/algemene-en-specifieke-exploitatievoorwaarden/algemene-exploitatievoorwaarden/de-verplichtingen-inzake-regenwaterbeheer.`;

export const infLegalEnvUrbaFR = `
${infLegalEnvFR}
`;

export const infLegalEnvUrbaNL = `
${infLegalEnvNL}
`;

export const infRRUPitchFR = `
**Le Règlement Régional d’Urbanisme (RRU)** s'applique sur l’ensemble du territoire de la Région de Bruxelles-Capitale. Ce texte a valeur réglementaire et fixe entre autres une série de règles pour la gestion de l’eau  à respecter lors de projets de constructions ou de rénovations. Le RRU est consultable à l'adresse: https://urbanisme.irisnet.be/lesreglesdujeu/les-reglements-durbanisme/le-reglement-regional-durbanisme-rru`;

export const infRRUPitchNL = `
**De Gewestelijke Stedenbouwkundige Verordening (GSV)** is van toepassing in het hele Brussels Hoofdstedelijk Gewest. Deze tekst heeft regelgevende waarde en bevat o.a. een reeks regels voor waterbeheer die bij bouw- of renovatieprojecten moeten worden nageleefd. De GSV kan worden geraadpleegd op het adres: https://stedenbouw.irisnet.be/spelregels/stedenbouwkundige-verordeningen-svs/de-gewestelijke-stedenbouwkundige-verordening-gsv?set_language=nl`;

export const infNoCategoryFR = `
L’infiltration d’eau de pluie est prévue sur un site qui n’est pas cadastré, 
de sorte que cette analyse **ne peut pas fournir une information** concernant la 
pollution du sol de ce site. Il est recommandé d’effectuer des **recherches 
complémentaires** pour savoir s’il y a ou non une pollution du sol: demander 
au propriétaire du site si des études de sol ont été réalisées et/ou demander 
une [attestation du sol](${attestationURLfr}) pour le site à Bruxelles Environnement. 
Pour plus d’informations: prendre contact avec le [facilitateur sol](${facilitatorURLfr}) de 
Bruxelles Environnement.  
*Source consultée pour l’analyse: [https://geodata.environnement.brussels > Atlas > Etat du sol](https://geodata.environnement.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infNoCategoryNL = `De regenwaterinfiltratie wordt voorzien op een niet gekadastreerd terrein, 
waardoor er in deze analyse **geen informatie** kan bezorgd worden inzake bodemverontreiniging. 
Er wordt aangeraden om **bijkomend opzoekingswerk** uit te voeren om te achterhalen of hier al dan niet een 
bodemverontreiniging aanwezig is: informeer bij de eigenaar van het terrein of er bodemonderzoeken uitgevoerd 
werden en/of vraag voor het terrein een [bodemattest](${attestationURLnl}) aan bij Leefmilieu Brussel.   
Voor meer informatie: neem contact op met de [bodemfacilitator](${facilitatorURLnl}) van Leefmilieu Brussel.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Bodemtoestand](https://geodata.leefmilieu.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCatBlancoFR = `
L’infiltration d’eau de pluie est prévue sur une parcelle qui **n’est 
pas reprise à l’inventaire de l’état du sol**, ce qui signifie que Bruxelles 
Environnement ne dispose d’aucune information permettant de suspecter une pollution 
du sol de cette parcelle. **L’infiltration d’eau de pluie est autorisée** à moins qu’une 
(suspicion de) pollution du sol n’a pas (encore) été signalée à Bruxelles Environnement.
 Pour plus d’informations: prendre contact avec le [facilitateur sol](${facilitatorURLfr}) de Bruxelles 
 Environnement.  
 *Source consultée pour l’analyse: [https://geodata.environnement.brussels > Atlas > Etat du sol](https://geodata.environnement.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCatBlancoNL = `
De regenwaterinfiltratie wordt voorzien op een  perceel dat **niet opgenomen is in de inventaris van de bodemtoestand**, 
wat wil zeggen dat Leefmilieu Brussel voor dit perceel over geen informatie beschikt inzake bodemverontreiniging. 
**Regenwaterinfiltratie is toegelaten**, tenzij er een (vermoeden van) bodemverontreiniging gekend is die (nog) niet aangegeven werd bij Leefmilieu Brussel. 
Voor meer informatie: neem contact op met de [bodemfacilitator](${facilitatorURLnl}) van Leefmilieu Brussel.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Bodemtoestand](https://geodata.leefmilieu.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat0FR = `
 L’infiltration d’eau de pluie est prévue sur une parcelle **potentiellement polluée**. 
 Une **[reconnaissance de l’état du sol](${soilConditionURLfr})** doit être effectuée par un 
 [expert en pollution du sol](${soilExpertURLfr}) afin de déterminer s’il y a (ou non) 
 une pollution du sol. La possibilité de pouvoir procéder à une 
 infiltration des eaux de pluie dépendra des résultats de cette reconnaissance 
 de l’état du sol. Pour plus d’informations: prendre contact avec le [facilitateur sol](${facilitatorURLfr}) de Bruxelles Environnement.  
 *Source consultée pour l’analyse: [https://geodata.environnement.brussels > Atlas > Etat du sol](https://geodata.environnement.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat0NL = `
De regenwaterinfiltratie wordt voorzien op een  perceel dat **mogelijk verontreinigd** is. Er dient een **[verkennend bodemonderzoek](${soilConditionURLnl})** 
uitgevoerd te worden door een [bodemverontreinigingsdeskundige](${soilExpertURLnl}) om na te gaan of er al dan niet bodemverontreiniging aanwezig is. 
De mogelijkheid om al dan niet tot regenwaterinfiltratie te kunnen overgaan hangt af van de uitkomst van dit verkennend bodemonderzoek. 
Voor meer informatie: neem contact op met de [bodemfacilitator](${facilitatorURLnl}) van Leefmilieu Brussel.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Bodemtoestand](https://geodata.leefmilieu.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat1FR = `
L’infiltration d’eau de pluie est prévue sur une **parcelle non polluée**. 
**L’infiltration d’eau de pluie est autorisée**.  
*Source consultée pour l’analyse: [https://geodata.environnement.brussels > Atlas > Etat du sol](https://geodata.environnement.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat1NL = `
De regenwaterinfiltratie wordt voorzien op een **niet-verontreinigd perceel. Regenwaterinfiltratie is toegelaten.**  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Bodemtoestand](https://geodata.leefmilieu.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat2FR = `
L’infiltration d’eau de pluie est prévue sur une **parcelle légèrement polluée sans risque**. 
**L’infiltration d’eau de pluie est autorisée.**  
*Source consultée pour l’analyse: [https://geodata.environnement.brussels > Atlas > Etat du sol](https://geodata.environnement.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat2NL = `
De regenwaterinfiltratie wordt voorzien op een **licht verontreinigd perceel zonder risico. Regenwaterinfiltratie is toegelaten.**  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Bodemtoestand](https://geodata.leefmilieu.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat3FR = `
L’infiltration d’eau de pluie est prévue sur une **parcelle polluée sans risque**. 
Selon le type et l’emplacement de la pollution, une **nouvelle [étude de risque](${riskURLfr})** par un [expert en pollution du sol](${soilExpertURLfr}) 
sera éventuellement nécessaire afin de déterminer si l’infiltration d’eau de pluie est possible. Pour plus d’informations: 
prendre contact avec le [facilitateur sol](${facilitatorURLfr}) de Bruxelles Environnement.  
*Source consultée pour l’analyse: [https://geodata.environnement.brussels > Atlas > Etat du sol](https://geodata.environnement.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat3NL = `
De regenwaterinfiltratie wordt voorzien op een **verontreinigd perceel zonder risico**. Afhankelijk van het type en de 
locatie van de verontreiniging kan het zijn dat een **nieuw [risico-onderzoek](${riskURLnl})** moet uitgevoerd worden door een [bodemverontreinigingsdeskundige](${soilExpertURLnl}) 
om te kunnen bepalen of regenwaterinfiltratie mogelijk is. Voor meer informatie: neem contact op met de [bodemfacilitator](${facilitatorURLnl}) van Leefmilieu Brussel.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Bodemtoestand](https://geodata.leefmilieu.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat4FR = `
L’infiltration d’eau de pluie est prévue sur une parcelle **polluée** sur 
laquelle **des études de sol ou des traitements du sol sont toujours en cours**. Prendre contact avec l’[expert en pollution du sol](${soilExpertURLfr}) 
responsable de ces travaux pour voir si l’infiltration d’eau de pluie est possible sachant que cette 
infiltration ne doit en aucun cas entraver les  études ou les traitements du sol en cours ni augmenter les risques 
engendrés par la pollution du sol. Pour plus d’informations: prendre contact avec le [facilitateur sol](${facilitatorURLfr}) de Bruxelles Environnement.  
*Source consultée pour l’analyse: [https://geodata.environnement.brussels > Atlas > Etat du sol](https://geodata.environnement.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat4NL = `
De regenwaterinfiltratie wordt voorzien op een **verontreinigd**  perceel waarop nog **bodemonderzoeken of bodembehandelingen in uitvoering** zijn. 
Neem contact op met de [bodemverontreinigingsdeskundige](${soilExpertURLnl}) die hiervoor verantwoordelijk is om na te laten gaan of regenwaterinfiltratie mogelijk is, 
wetende dat deze infiltratie geenszins de lopende bodemonderzoeken of bodembehandelingen mag belemmeren en de risico’s die uitgaan van de 
bodemverontreiniging mogen verhogen. Voor meer informatie: neem contact op met de [bodemfacilitator](${facilitatorURLnl}) van Leefmilieu Brussel.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Bodemtoestand](https://geodata.leefmilieu.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat01FR = `
L’infiltration d’eau de pluie est prévue sur une parcelle pour laquelle
 une étude de sol réalisée dans le passé **n’a pas montré de pollution** mais sur 
 laquelle il existe toujours une **suspicion de pollution** comme par exemple une [activité à 
 risque](${riskActivityURLfr}) toujours en activité. En principe une **nouvelle** [reconnaissance de l’état du sol](${soilConditionURLfr}) 
 doit être réalisée par un [expert en pollution du sol](${soilExpertURLfr}) afin de déterminer s’il existe (ou non)
  une nouvelle pollution du sol ayant un impact sur la possibilité d’infiltration d’eau de pluie. 
  Toutefois si une **dispense** est obtenue pour cette nouvelle reconnaissance de l’état du sol, l’infiltration 
  d’eau de pluie est quand même autorisée. Pour plus d’informations: prendre contact avec le facilitateur 
  sol de Bruxelles Environnement.  
  *Source consultée pour l’analyse: [https://geodata.environnement.brussels > Atlas > Etat du sol](https://geodata.environnement.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat01NL = `
De regenwaterinfiltratie wordt voorzien op een perceel waarvoor een bodemonderzoek in het verleden **geen verontreiniging heeft aangetoond**, 
maar waarop nog steeds een **vermoeden van verontreiniging** rust omdat er bvb. nog steeds een [risico-activiteit](${riskActivityURLnl}) uitgebaat wordt. In principe dient 
er een **nieuw [verkennend bodemonderzoek](${soilConditionURLnl})** uitgevoerd te worden door een [bodemverontreinigingsdeskundige](${soilExpertURLnl}) om na te gaan of er al dan niet 
bodemverontreiniging aanwezig is die een impact heeft op de mogelijkheid om regenwater te infiltreren. Indien men een **vrijstelling** bekomt 
voor dit nieuw verkennend bodemonderzoek, is regenwaterinfiltratie sowieso toegelaten. Voor meer informatie: neem contact op met de [bodemfacilitator](${facilitatorURLnl}) 
van Leefmilieu Brussel.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Bodemtoestand](https://geodata.leefmilieu.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat02FR = `
L’infiltration d’eau de pluie est prévue sur une parcelle pour 
laquelle une étude de sol réalisée dans le passé **a montré une légère pollution sans risque**, 
mais sur laquelle il existe toujours une **suspicion de pollution** comme par exemple une [activité 
à risque](${riskActivityURLfr}) toujours en activité. En principe une **nouvelle [reconnaissance de l’état du sol](${soilConditionURLfr})** doit être 
réalisée par un [expert en pollution du sol](${soilExpertURLfr}) afin de déterminer s’il existe (ou non) une nouvelle pollution 
du sol ayant un impact sur la possibilité d’infiltration d’eau de pluie. Toutefois si une **dispense** est obtenue 
pour cette nouvelle reconnaissance de l’état du sol, l’infiltration d’eau de pluie est quand même autorisée. 
Pour plus d’informations: prendre contact avec le [facilitateur sol](${facilitatorURLfr}) de Bruxelles Environnement.  
*Source consultée pour l’analyse: [https://geodata.environnement.brussels > Atlas > Etat du sol](https://geodata.environnement.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat02NL = `
De regenwaterinfiltratie wordt voorzien op een perceel waarvoor een bodemonderzoek in het verleden **lichte verontreinigingen 
zonder risico heeft aangetoond**, maar waarop nog steeds een **vermoeden van verontreiniging** rust omdat er bvb. nog steeds een 
[risico-activiteit](${riskActivityURLnl}) uitgebaat wordt. In principe dient er een **nieuw [verkennend bodemonderzoek](${soilConditionURLnl})** uitgevoerd te worden door een 
[bodemverontreinigingsdeskundige](${soilExpertURLnl}) om na te gaan of er al dan niet bodemverontreiniging aanwezig is die een impact heeft op de mogelijkheid 
om regenwater te infiltreren. Indien men een **vrijstelling** bekomt voor dit nieuw verkennend bodemonderzoek, is regenwaterinfiltratie sowieso 
mogelijk. Voor meer informatie: neem contact op met de [bodemfacilitator](${facilitatorURLnl}) van Leefmilieu Brussel.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Bodemtoestand](https://geodata.leefmilieu.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat03FR = `
L’infiltration d’eau de pluie est prévue sur une parcelle pour laquelle une étude de sol réalisée dans le passé 
**a montré une pollution sans risque**, mais sur laquelle il existe toujours une **suspicion de pollution** comme par exemple une [activité 
à risque](${riskActivityURLfr}) toujours en activité. En principe une **nouvelle [reconnaissance de l’état du sol](${soilConditionURLfr})** doit être 
réalisée par un [expert en pollution du sol](${soilExpertURLfr}) afin de déterminer s’il existe (ou non) une nouvelle pollution du sol ayant un 
impact sur la possibilité d’infiltration d’eau de pluie. 
On peut être **dispensé** de cette reconnaissance de l’état du sol sous certaines conditions. Selon le type et l’emplacement de la pollution, une 
**nouvelle [étude de risque](${riskURLfr})** par un expert en pollution du sol sera éventuellement nécessaire afin de déterminer si l’infiltration 
d’eau de pluie 
est possible ou non. Pour plus d’informations: prendre contact avec le [facilitateur sol](${facilitatorURLfr}) de Bruxelles Environnement.  
*Source consultée pour l’analyse: [https://geodata.environnement.brussels > Atlas > Etat du sol](https://geodata.environnement.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat03NL = `De regenwaterinfiltratie wordt voorzien op een perceel waarvoor een bodemonderzoek in het verleden een 
**verontreiniging zonder risico** heeft aangetoond en waarop tevens een **vermoeden van bijkomende verontreiniging** rust omdat er bvb. 
nog steeds een [risico-activiteit](${riskActivityURLnl}) uitgebaat wordt. In principe dient er een **nieuw [verkennend bodemonderzoek](${soilConditionURLnl})** uitgevoerd te worden door een 
[bodemverontreinigingsdeskundige](${soilExpertURLnl}) om na te gaan of er al dan niet bijkomende bodemverontreiniging aanwezig is die een impact heeft op de mogelijkheid 
om regenwater te infiltreren. Men kan onder bepaalde voorwaarden **vrijgesteld** worden van dit verkennend bodemonderzoek. Afhankelijk van het type en de 
locatie van de verontreiniging kan het zijn dat tevens een **nieuw [risico-onderzoek](${riskURLnl})** moet uitgevoerd worden door een bodemverontreinigingsdeskundige om te 
kunnen bepalen of regenwaterinfiltratie mogelijk is. Voor meer informatie: neem contact op met de [bodemfacilitator](${facilitatorURLnl}) van Leefmilieu Brussel.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Bodemtoestand](https://geodata.leefmilieu.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat04FR = `
L’infiltration d’eau de pluie est prévue sur une parcelle **polluée** sur laquelle 
**des études de sol ou des traitements du sol sont en cours** et sur laquelle il existe toujours une **suspicion de pollution** 
comme par exemple une [activité à risque](${riskActivityURLfr}) toujours en activité. 
En principe une **nouvelle [reconnaissance de l’état du sol](${soilConditionURLfr})** doit 
être réalisée par un [expert en pollution du sol](${soilExpertURLfr}) afin de déterminer s’il existe (ou non) une nouvelle pollution du sol 
ayant un impact sur la possibilité d’infiltration d’eau de pluie. On peut être **dispensé** de cette reconnaissance de l’état 
du sol sous certaines conditions. Pour la pollution du sol pour laquelle **des études ou des traitements sont toujours en cours**: 
prendre contact avec l’expert en pollution du sol responsable de ces travaux pour voir si l’infiltration d’eau de pluie est possible 
sachant que cette infiltration ne doit en aucun cas entraver les  études ou les traitements du sol en cours ni augmenter les risques 
engendrés par la pollution du sol. Pour plus d’informations: prendre contact avec le [facilitateur sol](${facilitatorURLfr}) de Bruxelles Environnement.  
*Source consultée pour l’analyse: [https://geodata.environnement.brussels > Atlas > Etat du sol](https://geodata.environnement.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infCat04NL = `
De regenwaterinfiltratie wordt voorzien op een **verontreinigd**  perceel waarop nog **bodemonderzoeken of bodembehandelingen 
in uitvoering** zijn en waarop tevens een **vermoeden van bijkomende verontreiniging** rust omdat er bvb. nog steeds een [risico-activiteit](${riskActivityURLnl}) 
uitgebaat wordt. In principe dient er een **nieuw [verkennend bodemonderzoek](${soilConditionURLnl})** uitgevoerd te worden door een 
[bodemverontreinigingsdeskundige](${soilExpertURLnl}) 
om na te gaan of er al dan niet bijkomende bodemverontreiniging aanwezig is die een impact heeft op de mogelijkheid om regenwater 
te infiltreren. Men kan onder bepaalde voorwaarden **vrijgesteld** worden van dit verkennend bodemonderzoek. Voor de bodemverontreiniging 
waarvoor **nog bodemonderzoeken of bodembehandeling in uitvoering zijn** dient sowieso contact opgenomen te worden met de 
bodemverontreinigingsdeskundige die hiervoor verantwoordelijk is om na te laten gaan of regenwaterinfiltratie mogelijk is, 
wetende dat deze infiltratie geenszins de lopende onderzoeken/behandelingen mag belemmeren en de risico’s die uitgaan van de 
bodemverontreiniging mogen verhogen. Voor meer informatie: neem contact op met de [bodemfacilitator](${facilitatorURLnl}) van Leefmilieu Brussel.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Bodemtoestand](https://geodata.leefmilieu.brussels/client/view/01445cff-7034-463e-853c-e918232a8a5e)*`;

export const infPPASyesFR = `
**Dans le périmètre d’un Plan particulier d’affectation du sol (PPAS)**, des dispositions particulières relatives à la gestion 
de l’eau par infiltration peuvent figurer au niveau des prescriptions. Le concepteur de projet sera tenu de respecter celles-ci. 
En cas contraire, il devra justifier des raisons pour lesquelles il y déroge.  
*Source consultée pour l'analyse: https://gis.urban.brussels > perspective.brussels > PPAS.*`;

export const infPPASyesNL = `
**Binnen de perimeter van een Bijzonder bestemmingsplan (BBP)** kunnen specifieke bepalingen inzake waterbeheer d.m.v. 
infiltratie in de voorschriften zijn opgenomen. De projectontwerper zal zich hieraan moeten houden. Zo niet, 
dan moet hij motiveren waarom ervan wordt afgeweken.  
*Geraadpleegde bron voor de analyse: https://gis.urban.brussels > perspective.brussels > BBP.*`;

export const infPPASnoFR = `Pas dans le périmètre d’un Plan particulier d’affectation du sol (PPAS).  
*Source consultée pour l'analyse: https://gis.urban.brussels > perspective.brussels > PPAS.*`;

export const infPPASnoNL = `Niet binnen de perimeter van een Bijzonder bestemmingsplan (BBP).  
*Geraadpleegde bron voor de analyse: https://gis.urban.brussels > perspective.brussels > BBP.*`;

export const infRCUyesFR = `
Le Règlement Communal d’Urbanisme (RCU) comprends des prescriptions spécifiques en termes d'infiltration d’eau de pluie. 
Pour plus d'informations: prenez contact avec le service compétent de la commune concernée.  
*Source: information reçue de la part de la commune en mars-avril 2022*.`;

export const infRCUyesNL = `De Gemeentelijke Stedenbouwkundige Verordening (GemSV) bevat specifieke regels op het gebied van 
regenwaterinfiltratie. Voor meer informatie: neem contact op met de bevoegde dienst van de desbetreffende gemeente.  
*Bron: informatie ontvangen van de gemeente in maart-april 2022.*`;

export const infRCUnoFR = `Le Règlement Communal d’Urbanisme (RCU) ne comprends pas des prescriptions spécifiques en termes 
d'infiltration d’eau de pluie.  
*Source: information reçue de la part de la commune en mars-avril 2022.*`;

export const infRCUnoNL = `De Gemeentelijke Stedenbouwkundige Verordening (GemSV) bevat geen specifieke regels op het gebied van regenwaterinfiltratie.  
*Bron: informatie ontvangen van de gemeente in maart-april 2022.*`;

export const infPADyesFR = `
**Dans le périmètre d’un (futur) Plan d’Aménagement Directeur (PAD)**, des dispositions particulières relatives à la gestion de l’eau 
par infiltration peuvent figurer au niveau des prescriptions, elles seront en général établies sous forme de grands principes. Le PAD 
n’étant pas un document dérogatoire, le concepteur de projet sera tenu de respecter ces prescriptions.  
*Source consultée: données cartographiques des PAD fournies par perspective.brussels en avril 2022.*`;

export const infPADyesNL = `**Binnen de perimeter van een (toekomstig) Richtplan van aanleg (RPA)** 
kunnen specifieke bepalingen inzake waterbeheer d.m.v. infiltratie in de voorschriften zijn opgenomen. Deze zullen overwegend 
vastgelegd zijn in de vorm van algemene principes. Aangezien er van een RPA niet kan afgeweken worden, zal de projectontwerper 
deze voorschriften in acht moeten nemen.  
*Geraadpleegde bron: cartografische gegevens van de RPA's verstrekt door perspective.brussels in april 2022.*`;

export const infPADnoFR = `Pas dans le périmètre d'un (futur) Plan d’Aménagement Directeur (PAD).  
*Source consultée: données cartographiques des PAD fournies par perspective.brussels en avril 2022.*`;

export const infPADnoNL = `Niet binnen de perimeter van een (toekomstig) Richtplan van aanleg (RPA).  
*Geraadpleegde bron: cartografische gegevens van de RPA's verstrekt door perspective.brussels in april 2022.*`;

export const infHeritagePitchFR = `L'infiltration d'eau pluviale est prévue **au niveau d'un patrimoine classé ou inscrit sur liste de sauvegarde**.  
L’infiltration peut y être soumise à des prescriptions particulières visant à protéger et conserver le patrimoine immobilier. Les 
aménagements projetés devront s’intégrer au mieux dans leur environnement paysager-patrimonial, en cohérence avec le contexte urbain 
et tenant compte de l’identité du lieu. Dans ces zones, il est en outre **préférable de demander l’avis préalable de la Direction du Patrimoine 
Culturel** : http://patrimoine.brussels/news/urban-brussels.  
*Source consultée pour l'analyse: https://gis.urban.brussels > Bruxelles Urbanisme et Patrimoine > Statut légal et Registre > Monuments et Sites > Patrimoine*`;

export const infHeritagePitchNL = `De infiltratie van regenwater is gepland **ter hoogte van gevrijwaard erfgoed  of erfgoed dat 
ingeschreven is op de bewaarlijst**.  Voor infiltratie op deze plaats kunnen speciale eisen gelden met het oog op de bescherming en 
het behoud van het bouwkundig erfgoed. De geplande infiltratievoorziening moet zo goed mogelijk in de landschappelijke en erfgoedomgeving 
worden ingepast, in samenhang met de stedelijke context en rekening houdend met de identiteit van de locatie. In deze gebieden verdient het 
o.a. de voorkeur **vooraf het advies in te winnen van de Directie Cultureel Erfgoed**: http://erfgoed.brussels/news/urban-brussels.  
*Geraadpleegde bron voor de analyse: https://gis.urban.brussels > Brussel Stedenbouw en Erfgoed > Monumenten en Landschappen > Wettelijk statuut en register > Patrimonium*`;

export const infHeritagePitchNoFR = `L'infiltration d'eau pluviale **n'est pas prévue au niveau d'un patrimoine classé ou inscrit sur liste de sauvegarde**.  
*Source consultée pour l'analyse: https://gis.urban.brussels > Bruxelles Urbanisme et Patrimoine > Monuments et Sites > Patrimoine*`;

export const infHeritagePitchNoNL = `De infiltratie van regenwater is **niet gepland ter hoogte van gevrijwaard erfgoed  of erfgoed dat ingeschreven 
is op de bewaarlijst**.  
*Geraadpleegde bron voor de analyse: https://gis.urban.brussels > Brussel Stedenbouw en Erfgoed > Monumenten en Landschappen > Wettelijk statuut en register > Patrimonium*`;

export const infWaterCaptureFR = `L'infiltration d'eau pluviale est prévue **dans la zone de protection des captages d'eau souterraine** exploités 
par Vivaqua au Bois de la Cambre/forêt de Soignes et qui alimentent le réseau de distribution d'eau potable. L’infiltration d’eau pluviale peut 
être soumise à des prescriptions particulières visant à préserver la qualité de la nappe exploitée. L'infiltration est en outre interdite à 
proximité immédiate des points de puisage de Vivaqua  (zone de protection 1 et 2), ce qui représente toutefois une zone relativement limitée.  
Plus d'informations: https://environnement.brussels/thematiques/geologie-et-hydrogeologie/eaux-souterraines/monitoring-des-eaux-souterraines/zones.  
*Source consultée pour l'analyse: [https://geodata.environnement.brussels > Atlas > Hydrogéologie](https://geodata.environnement.brussels/client/view/bae6fa81-1f21-4121-96d0-b33fb98ac2db)*`;

export const infWaterCaptureNL = `De infiltratie van regenwater is gepland **in de beschermingszone van de grondwaterwinningen** die door Vivaqua 
worden geëxploiteerd in het Ter Kamerenbos/Zoniënwoud en die het drinkwaterdistributienet voeden. Voor de infiltratie van regenwater kunnen 
speciale voorschriften gelden die erop gericht zijn de kwaliteit van de geëxploiteerde grondwaterlaag in stand te houden. Bovendien is infiltratie 
verboden in de onmiddellijke omgeving van de winningen van Vivaqua (beschermingszones 1 en 2), wat evenwel om een beperkt gebied gaat.  
Meer informatie: https://leefmilieu.brussels/themas/geologie-en-hydrogeologie/grondwater/monitoring-van-het-grondwater/beschermde-gebieden.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Hydrogeologie](https://geodata.leefmilieu.brussels/client/view/bae6fa81-1f21-4121-96d0-b33fb98ac2db).*`;

export const infWaterCaptureNoFR = `L'infiltration d'eau pluviale est prévue **en dehors de la zone de protection des captages d'eau 
souterraine** exploités par Vivaqua au Bois de la Cambre/forêt de Soignes et qui alimentent le réseau de distribution d'eau.  
*Source consultée pour l'analyse: [https://geodata.environnement.brussels > Atlas > Hydrogéologie](https://geodata.environnement.brussels/client/view/bae6fa81-1f21-4121-96d0-b33fb98ac2db)*`;

export const infWaterCaptureNoNL = `De infiltratie van regenwater is gepland **buiten de beschermingszone van de grondwaterwinningen** 
die door Vivaqua worden geëxploiteerd in het Ter Kamerenbos/Zoniënwoud en die het drinkwaterdistributienet voeden.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Hydrogeologie](https://geodata.leefmilieu.brussels/client/view/bae6fa81-1f21-4121-96d0-b33fb98ac2db).*`;

export const infInWaterSurfaceFR = `Plouf! Les plans d'eau ont un mode d'infiltration qui sort du focus de la présente plateforme :)!`;
export const infInWaterSurfaceNL = `Plons! Waterlichamen hebben een infiltratiewijze die buiten de focus van dit platform valt :)`;

export const infBufferWaterSurfaceFR = `L’infiltration d’eau pluviale aura lieu **à proximité d’eaux de surface** (canal, rivière, ruisseau, étang, etc.). 
Il est toléré que le surplus qui ne pourrait être géré à la parcelle par infiltration aille directement vers une eau de surface en privilégiant toutefois 
dans la mesure du possible un tamponnage préalable dans des dispositifs végétalisés et en évitant les trop longs cheminements.


Demandez l’accord préalable sur votre projet et les modalités éventuelles d’un rejet du trop-plein au gestionnaire de l’exutoire du dispositif d'infiltration, à savoir:

- Si directement dans un élément du réseau hydrographique, le gestionnaire est Bruxelles Environnement (contact: eau_water@environnement.brussels)
- Si directement dans le Canal, le gestionnaire est le Port de Bruxelles (contact: rent@port.brussels).


Dans les autres cas de figure tels que mare, ancien fossé, etc (non pris en compte dans cette analyse), il vous faudra en outre, 
conformément au Code Civil, disposer de l’accord du propriétaire des lieux (convention à établir entre parties).  
*Source consultée pour l'analyse: [https://geodata.environnement.brussels > Atlas > Eau à Bruxelles](https://geodata.environnement.brussels/client/view/030319b5-9197-44b7-b675-1e0ca9e90bb2)*`;

export const infBufferWaterSurfaceNL = `De infiltratie van regenwater wordt voorzien **in de nabijheid van een oppervlaktewater** (kanaal, rivier, beek, vijver, enz.). H
et surplus dat niet door infiltratie op het perceel kan worden beheerd, mag rechtstreeks naar het oppervlaktewater worden afgevoerd, waarbij zoveel 
mogelijk de voorkeur wordt gegeven aan een voorafgaande buffering in vegetatievoorzieningen en waarbij te lange trajecten worden vermeden.


Vraag aan de waterbeheerder om een voorafgaand akkoord over uw project en de mogelijke voorwaarden voor de aansluiting van de overloop van de infiltratievoorziening op het oppervlaktewater:
- Voor een aansluiting op  een element van het hydrografisch netwerk: de waterbeheerder is Leefmilieu Brussel (contact: eau_water@leefmilieu.brussels);
- Voor een aansluiting op het kanaal: de waterbeheerder is de Haven van Brussel (contact: rent@port.brussels).


In andere gevallen, zoals vijvers, oude sloten, enz. (waarmee in deze analyse geen rekening is gehouden), hebt u o.a. de toestemming nodig van de eigenaar van het terrein, 
overeenkomstig het Burgerlijk Wetboek (overeenkomst af te sluiten tussen de partijen).  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Water in Brussel](https://geodata.leefmilieu.brussels/client/view/030319b5-9197-44b7-b675-1e0ca9e90bb2)*`;

export const infInNatura2000FR = `L’infiltration d’eau de pluie **aura lieu dans un site Natura 2000 ou une réserve naturelle**. 
Certaines précautions doivent être prises pour respecter et sauvegarder les habitats des espèces protégées comme par exemple le drainage des 
zones humides naturelles existantes est proscrit, les plantations existantes servant de support au développement de la faune doivent être conservées, 
l’infiltration d’eau y est réglementée. Ainsi, les projets d’infiltration qui nécessitent un remaniement en profondeur des terres, comme les tranchées, 
les massifs, fossés, ne sont pas autorisés. Dans ces zones, il sera préférable d’utiliser des techniques peu intrusives telles que les noues ou bassins 
secs de faibles profondeurs avec rejet différé en eau de surface et qui ne drainent donc pas ces zones. On peut également parfois profiter des reliefs 
existants pour gérer l’eau pluviale (bassin secs naturels). Dans ces zones, il est en outre préférable de **demander l’avis préalable de la cellule Natura 
2000 de Bruxelles Environnement** :
natura2000@environnement.brussels.

De manière générale, dans le cas où un aménagement est susceptible d’avoir un impact significatif sur les objectifs Natura 2000 la législation prévoit 
une **évaluation appropriée des incidences** de ce projet sur ces objectifs nature. Plus d’informations : 
https://environnement.brussels/thematiques/espaces-verts-et-biodiversite/action-de-la-region/natura-2000/travaux-et-amenagements.  
*Source consultée pour l’analyse : [https://geodata.environnement.brussels > Atlas > Sites nature](https://geodata.environnement.brussels/client/view/5f80baca-0f9b-40e4-90d9-f64b03c0da7f)*.`;

export const infInNatura2000NL = `De infiltratie van regenwater **wordt voorzien in een Natura 2000-gebied of een natuurreservaat**. 
Er moeten bepaalde voorzorgsmaatregelen worden genomen om de habitats van beschermde soorten te respecteren en te beschermen, b.v. 
drainage van bestaande natuurlijke wetlands is verboden, bestaande aanplantingen die de ontwikkeling van fauna ondersteunen moeten behouden blijven, 
infiltratie van water wordt gereguleerd. Derhalve zijn infiltratieprojecten die een diepe omwoeling van de bodem vereisen, zoals greppels, massieven, 
sloten, niet toegestaan. In deze gebieden verdient het de voorkeur minder ingrijpende technieken toe te passen, zoals ondiepe droge bassins of greppels 
met uitgestelde afvoer in oppervlaktewater en die deze gebieden niet draineren. Soms is het ook mogelijk gebruik te maken van het bestaande reliëf om 
het regenwater te beheren (natuurlijk droog bekken).

In deze gebieden verdient het ook de voorkeur om **vooraf advies in te winnen bij de cel Natura 2000 van Leefmilieu Brussel**: natura2000@leefmilieu.brussels.

In het algemeen voorziet de wetgeving in een **passende beoordeling van de effecten** van een ontwikkeling op de Natura 2000-doelstellingen, 
indien die ontwikkeling een significant effect kan hebben op die natuurdoelstellingen. Meer informatie: 
https://leefmilieu.brussels/themas/groene-ruimten-en-biodiversiteit/acties-van-het-gewest/natura-2000/werken-en-inrichtingen.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Natuurgebieden](https://geodata.leefmilieu.brussels/client/view/5f80baca-0f9b-40e4-90d9-f64b03c0da7f).*`;

export const infBufferNatura2000FR = `L’infiltration d’eau de pluie **aura lieu à proximité d'un site Natura 2000 ou d’une réserve naturelle**. 
De manière générale, et pas spécifiquement pour la gestion de l'eau pluviale, dans le cas où un aménagement est susceptible d’avoir un impact significatif 
sur les objectifs Natura 2000, la législation 
prévoit une **évaluation appropriée des incidences** de ce projet sur ces objectifs nature. Sont notamment concernés les projets soumis à étude d’incidences sur l’environnement ou à rapport d’incidences 
concernant des biens immobiliers, car ils sont réputés susceptibles d’affecter les sites naturels de manière significative. Plus d’informations : 
https://environnement.brussels/thematiques/espaces-verts-et-biodiversite/action-de-la-region/natura-2000/travaux-et-amenagements ou 
prenez contact avec la cellule Natura 2000 de Bruxelles Environnement (natura2000@environnement.brussels).  
*Source consultée pour l’analyse : [https://geodata.environnement.brussels > Atlas > Sites nature](https://geodata.environnement.brussels/client/view/5f80baca-0f9b-40e4-90d9-f64b03c0da7f)*.`;

export const infBufferNatura2000NL = `De infiltratie van regenwater **wordt voorzien in de nabijheid van een Natura 2000-gebied of natuurreservaat**. 
In het algemeen, en niet specifiek in het kader van regenwaterbeheer, voorziet de wetgeving in een **passende beoordeling van de effecten** van een 
ontwikkeling op de Natura 2000-doelstellingen, indien die ontwikkeling een significant effect kan hebben op die natuurdoelstellingen. Dit geldt 
o.a. voor vastgoedprojecten waarvoor een milieueffectenstudie of milieueffectenverslag moet worden uitgevoerd, omdat het waarschijnlijk wordt 
geacht dat zij significante gevolgen zullen hebben voor natuurgebieden. 
Meer informatie: https://leefmilieu.brussels/themas/groene-ruimten-en-biodiversiteit/acties-van-het-gewest/natura-2000/werken-en-inrichtingen 
of neem contact op met de cel Natura 2000 van Leefmilieu Brussel (natura2000@leefmilieu.brussels).  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Natuurgebieden](https://geodata.leefmilieu.brussels/client/view/5f80baca-0f9b-40e4-90d9-f64b03c0da7f).*`;

export const infOutNatura2000FR = `L’infiltration d’eau de pluie **n’aura pas lieu à proximité d'un site Natura 2000 ou d’une réserve naturelle**. Il n’y a 
donc pas de prescriptions particulières à prendre en compte.  
*Source consultée pour l’analyse : [https://geodata.environnement.brussels > Atlas > Sites nature](https://geodata.environnement.brussels/client/view/5f80baca-0f9b-40e4-90d9-f64b03c0da7f)*.`;

export const infOutNatura2000NL = `De infiltratie van regenwater **wordt niet voorzien in de nabijheid van een Natura 2000-gebied of natuurreservaat**. 
Er zijn dus geen bijzondere eisen waarmee rekening moet gehouden worden op dit vlak.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Natuurgebieden](https://geodata.leefmilieu.brussels/client/view/5f80baca-0f9b-40e4-90d9-f64b03c0da7f).*`;

export const infNearPhreaticFR = `Afin de permettre une infiltration efficiente et éviter le moindre risque de contamination des sols et des eaux souterrains, il convient 
de placer le **fond** de chacun des ouvrages de gestion des eaux pluviales, permettant une infiltration des eaux pluviales, 
**à minima 1 mètre du niveau de la nappe dans sa période de plus hautes eaux**.
Selon le Brussels Phreatic System Model (BPSM = estimation profondeur de la nappe phréatique pour la période mai 2013), **cet endroit est concerné 
ou situé dans une zone où le niveau phréatique est peut-être subaffleurant (profondeur estimée de la nappe < 4 m)** et des informations supplémentaires 
seront à collecter (la nature des couches de sols concernés, piézomètres dans la zone,…).  
*Source consultée pour l'analyse: [https://geodata.environnement.brussels > Atlas > Hydrogéologie](https://geodata.environnement.brussels/client/view/82645188-dd20-430c-b1d1-df829c94dc1d)*`;

export const infNearPhreaticNL = `Om een efficiënte infiltratie mogelijk te maken en elk risico van bodem- en grondwaterverontreiniging te voorkomen, 
moet de **onderzijde** van elke structuur voor regenwaterinfiltratie **ten minste 1 meter boven de hoogste grondwaterstand** worden geplaatst.
Volgens het Brussels Phreatic System Model (BPSM = geschatte diepte van de grondwaterspiegel voor de periode mei 2013), 
**bevindt deze locatie zich in een zone met een mogelijks oppervlakkige grondwaterstand (geschatte grondwaterdiepte < 4 m)**, 
waardoor er aanvullende informatie moet verzameld worden (de aard van de betrokken bodemlagen, peilbuizen in de zone,...).  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Hydrogeologie](https://geodata.leefmilieu.brussels/client/view/82645188-dd20-430c-b1d1-df829c94dc1d)*`;

export const infNoPhreaticFR = `Afin de permettre une infiltration efficiente et éviter le moindre risque de contamination des sols et des eaux souterrains, il convient 
de placer le **fond** de chacun des ouvrages de gestion des eaux pluviales, permettant une infiltration des eaux pluviales, 
**à minima 1 mètre du niveau de la nappe dans sa période de plus hautes eaux.**
Selon le Brussels Phreatic System Model (BPSM = estimation profondeur de la nappe phréatique pour la période mai 2013), **cet endroit n’est pas concerné par 
un niveau de nappe subaffleurant (profondeur estimée de la nappe > 4 m)**. La préfaisabilité de procéder à l’infiltration naturelle des eaux pluviales peut être 
considérée comme positive pour cet aspect, aucune mesure particulière n’est à envisager.  
*Source consultée pour l'analyse: [https://geodata.environnement.brussels > Atlas > Hydrogéologie](https://geodata.environnement.brussels/client/view/82645188-dd20-430c-b1d1-df829c94dc1d)*`;

export const infNoPhreaticNL = `Om een efficiënte infiltratie mogelijk te maken en elk risico van bodem- en grondwaterverontreiniging te voorkomen, 
moet de **onderzijde** van elke structuur voor regenwaterinfiltratie **ten minste 1 meter boven de hoogste grondwaterstand** worden geplaatst.
Volgens het Brussels Phreatic System Model (BPSM = geschatte diepte van de grondwaterspiegel voor de periode mei 2013), **wordt deze locatie niet gekenmerkt 
door een oppervlakkige grondwaterstand (geschatte grondwaterdiepte > 4 m)**. De pre-haalbaarheid van natuurlijke infiltratie van regenwater kan voor dit aspect 
als positief worden beschouwd, er hoeven geen specifieke maatregelen te worden overwogen.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Hydrogeologie](https://geodata.leefmilieu.brussels/client/view/82645188-dd20-430c-b1d1-df829c94dc1d)*`;

export const infFloodAleaFR = `L'infiltration d'eau pluviale est **prévue au niveau d'une zone où pourraient se produire des 
inondations** (d’ampleur et de fréquence faibles, moyennes ou élevées). Il n’y a pas de prescriptions particulières associées à 
ces zones. L’infiltration d’eau de pluie y est possible mais la présence d’aléas d’inondation est souvent indicatif de 
contraintes obligeants à concevoir un projet différemment (ex: ennoiement plus fréquent). En pratique, ces zones étant 
généralement en fond de vallée, il est conseillé, dans la mesure du possible, de **maximiser les superficies d’infiltration 
des ouvrages** (à volume de temporisation égal, des ouvrages infiltrant plus étendus).
Pour des prescriptions plus générales sur les projets urbanistiques en zones inondables: 
https://www.guidebatimentdurable.brussels/gerer-eaux-pluviales-parcelle/identifier-contraintes-liees-localisation-parcelle.  
*Source consultée pour l'analyse: [https://geodata.environnement.brussels > Atlas > Inondation aléa et risque](https://geodata.environnement.brussels/client/view/1a3cae6b-dd04-4b28-a3e2-c432dc83e24f)*`;

export const infFloodAleaNL = `
De infiltratie van regenwater is **voorzien in een gebied waar overstromingen kunnen voorkomen** (van kleine, middelgrote of 
  grote omvang en frequentie). Er zijn geen specifieke eisen verbonden aan deze gebieden. In deze gebieden is infiltratie 
  van regenwater mogelijk, maar de aanwezigheid van overstromingsgevaar wijst vaak op beperkingen die een ander ontwerp 
  vereisen (bv. de infiltratievoorziening zal frequenter volledig gevuld zijn). Aangezien deze gebieden zich over het algemeen 
  op de bodem van de vallei bevinden, wordt in de praktijk aanbevolen om, voor zover mogelijk, **het infiltratieoppervlak van de 
  infiltratievoorziening te maximaliseren** (bij een gelijk buffervolume, een meer uitgebreidere infiltratievoorziening voorzien).
Voor meer algemene voorschriften inzake stadsontwikkelingsprojecten in overstromingsgevoelige gebieden: 
https://www.gidsduurzamegebouwen.brussels/beheer-regenwater-perceel/beperkingen-ligging-perceel-identificeren.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Overstromingsgevaar en risico](https://geodata.leefmilieu.brussels/client/view/1a3cae6b-dd04-4b28-a3e2-c432dc83e24f)*`;

export const infNoFloodAleaFR = `L'infiltration d'eau pluviale **n'est pas prévue au niveau d'une zone où pourraient 
se produire des inondations**. L’infiltration y est particulièrement préconisée, ces zones étant souvent indemnes de 
contraintes (ex. plateau) et la gestion de l’eau par infiltration dans ces zones permet de protéger les zones à l’aval.  
*Source consultée pour l'analyse: [https://geodata.environnement.brussels > Atlas > Inondation aléa et risque](https://geodata.environnement.brussels/client/view/1a3cae6b-dd04-4b28-a3e2-c432dc83e24f)*`;

export const infNoFloodAleaNL = `De infiltratie van regenwater **is niet voorzien in een gebied waar overstromingen 
kunnen voorkomen**. Infiltratie wordt hier in het bijzonder aanbevolen, aangezien deze gebieden vaak niet onder druk 
staan (bvb. hoogvlakte) en waterbeheer door infiltratie in deze gebieden bijdraagt tot de bescherming van stroomafwaarts 
gelegen gebieden.  
*Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels > Atlas > Overstromingsgevaar en risico](https://geodata.leefmilieu.brussels/client/view/1a3cae6b-dd04-4b28-a3e2-c432dc83e24f)*`;

export const goToBrugeotoolAnalysisFR = `
Voir l'analyse géothermique  
à cette adresse`;

export const goToBrugeotoolAnalysisNL = `
Zie de geothermische analyse  
op dit adres`;

export const infGeology1FR = `L’(hydro)géologie attendue au niveau de l’infiltration d’eau de pluie est présenté sur ce`;

export const infGeology1NL = `De verwachte (hydro)geologie ter hoogte van de voorziene locatie voor infiltratie van regenwater 
wordt weergegeven op het profiel van deze `;

export const infGeology2FR = ` profil de forage virtuel.`;
export const infGeology2NL = ' virtuele boring.';

export const infGeology3FR = `Attention : ce profil de forage virtuel est purement théorique. **Pour connaître la perméabilité réelle, il convient 
d’effectuer de toute façon des tests d'infiltration sur le terrain**. Pour avoir un dispositif d'infiltration d’eau de pluie 
correctement dimensionné, il faut absolument se baser sur les résultats de ces tests d’infiltration. La méthodologie pour la 
réalisation des ceux-ci est décrite sur les FAQ’s Eau de Bruxelles Environnement 
(https://environnement.brussels/thematiques/eau/le-professionnel-en-action/outils-et-accompagnement/faqs) 
et intégrée également dans la méthodologie IQSB-Pro 
(https://environnement.brussels/thematiques/sols/good-soil/indices-de-qualite-des-sols-bruxellois/lindice-de-qualite-des-sols-0).`;

export const infGeology3NL = `Opgelet: het profiel van de virtuele boring is louter theoretisch. 
**Om de werkelijke permeabiliteit te kennen, moeten in het veld infiltratieproeven worden uitgevoerd.** Voor een correct 
gedimensioneerd regenwaterinfiltratiesysteem moet worden uitgegaan van de resultaten van deze infiltratieproeven. De 
methodologie voor de uitvoering van deze proeven wordt beschreven in de FAQ's Water van Leefmilieu Brussel 
(https://leefmilieu.brussels/themas/water/professionelen-actie/tools-en-begeleiding/faqs) en is ook opgenomen in de IBKB-Pro 
methodologie (https://leefmilieu.brussels/themas/bodem/good-soil/index-voor-bodemkwaliteit-brussel/de-brusselse-bodemkwaliteitsindex-voor-0).`;

export const printToPdfFR = `
**TIP** : Pour enregistrer le document en .PDF, vous pouvez utiliser la fonctionnalité "*Print to PDF*" de votre menu d'impression.
Selon la quantité d'information, un ajustement de l'échelle d'impression et/ou du format de papier (A4 ou A3) peut être utile.`;

export const printToPdfNL = `**TIP**: Om het document als PDF-bestand op te slaan, kunt u de functie "*Print to PDF*" in uw afdrukmenu gebruiken. Afhankelijk van de hoeveelheid informatie kan een aanpassing van de afdrukschaal en/of het papierformaat (A4 of A3) nuttig zijn.`;

export const disclaimerPrintFR = `
Les données et informations fournies par l’application InfiltraSoil sont des estimations. Bruxelles Environnement ne pourra être tenu responsable de l’utilisation qui en sera faite. 
`;

export const disclaimerPrintNL = `
De gegevens en informatie die door de BrugeoTool applicatie worden verstrekt zijn schattingen. Leefmilieu Brussel kan niet verantwoordelijk worden gesteld voor het gebruik ervan.
`;

export const userProQuestionFR = `
Etes-vous un professionnel de la construction ou de l’aménagement des espaces publics?`;

export const userProQuestionNL = `Bent u een professional actief in de bouw of de inrichting van de openbare ruimte?`;

export const infPitchFacilitatorFR = `
Le Facilitateur Eau de Bruxelles Environnement est là pour vous appuyer gratuitement tout au long de votre projet 
d’infiltration des eaux pluviales et ce à tout stade (de l’esquisse à la mise en oeuvre). 
Plus d’informations: https://environnement.brussels/thematiques/eau/le-professionnel-en-action/outils-et-accompagnement/le-facilitateur-eau`;

export const infPitchFacilitatorNL = `De Facilitator Water van Leefmilieu Brussel staat u gratis bij gedurende de hele 
duur van uw project voor infiltratie van regenwater en dit tijdens elk stadium (van schets tot uitvoering). 
Meer informatie: https://leefmilieu.brussels/themas/gebouwen-en-energie/bouwen-en-renoveren/technische-info-en-tools/thema/water/de-facilitator`;

export const creditsFR = `Fond de plan & géocodeur © UrbIS. Données fournies par Bruxelles Environnement. 
Conception et réalisation : [A.-C.](https://atelier-cartographique.be)`;

export const creditsNL = `Achtergrond & geocoder © UrbIS. Gegevens verstrekt door Leefmilieu Brussel. 
Ontwerp en productie : [A.-C.](https://atelier-cartographique.be)`;

export const fundingInfoLocalityFR = `À cet endroit, la commune prévoit également des primes complémentaires pour faciliter l'infiltration des eaux de pluie. 
Nous vous invitons à prendre contact avec la service compétent de la commune concernée pour plus d'informations.  
*Source: information reçue de la part de la commune en mars-avril 2022.*`;

export const fundingInfoLocalityNL = `Ter hoogte van deze locatie  worden door de gemeente ook aanvullende premies voorzien om de infiltratie van regenwater te faciliteren. 
Voor meer informatie kunt u contact opnemen met de bevoegde dienst van de desbetreffende gemeente.  
*Bron: informatie ontvangen van de gemeente in maart-april 2022.*`;
