/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { AllHTMLAttributes, DetailedReactHTMLElement } from 'react';
import { MessageStore, formatMessage, Parameters } from 'sdi/locale';
import { markdown } from 'sdi/ports/marked';
import * as texts from './marked-texts';

const messages = {
    __empty__: {
        fr: '',
        nl: '',
        en: '',
    },

    appName: {
        fr: 'Infiltrasoil',
        nl: 'Infiltrasoil',
    },

    loadingData: {
        fr: 'Chargement des données',
        nl: 'Data wordt geladen',
        en: 'Loading datas',
    },

    geocode: {
        fr: 'Chercher une adresse',
        nl: 'Zoek een adres',
        en: 'Search an adress',
    },
    backToGeocode: {
        fr: 'Retour à la recherche par adresse',
        nl: 'Terug naar zoeken op adres', //nltocheck
        en: 'Back to address lookup',
    },
    goToBrugeotool: {
        fr: 'Voir BruGeoTool',
        nl: 'Zie BruGeoTool', //nltocheck
        en: 'Go to Brugeotool',
    },
    goToBrugeotoolAnalysis: {
        fr: texts.goToBrugeotoolAnalysisFR,
        nl: texts.goToBrugeotoolAnalysisNL, //nltocheck
        en: 'See the geothermal analysis at this address',
    },
    infGeology1: {
        fr: texts.infGeology1FR,
        nl: texts.infGeology1NL,
        en: '',
    },
    infGeology2: {
        fr: texts.infGeology2FR,
        nl: texts.infGeology2NL,
        en: '',
    },
    infGeology3: {
        fr: texts.infGeology3FR,
        nl: texts.infGeology3NL,
        en: '',
    },
    goToBruWater: {
        fr: 'Voir BruWater',
        nl: 'Zie BruWater', //nltocheck
        en: 'Go to BruWater',
    },
    in: {
        fr: 'à',
        nl: 'in',
        en: 'in',
    },
    searchResult: {
        fr: 'Résultat de recherche',
        nl: 'Zoekresultaat',
        en: 'Search results',
    },
    selectBuilding: {
        fr: 'Je sélectionne mon bâtiment',
        nl: 'Ik kies mijn gebouw',
        en: 'Select a building ',
    },
    onMap: {
        fr: 'sur la carte',
        nl: 'op de kaart',
        en: 'on the map',
    },
    tradVersion: {
        fr: 'versie',
        nl: 'version',
        en: 'version',
    },
    infiltrationGeocode: {
        fr: 'Adresse : rue, numéro, commune',
        nl: 'Adres: straat, nummer, plaats',
        en: 'Address: street, number, town',
    },
    research: {
        fr: 'Chercher',
        nl: 'Zoeken',
        en: 'Search',
    },
    incompleteAdress: {
        fr: 'Adresse incomplète.',
        nl: 'Onvolledig adres.',
        en: 'Incomplete address.',
    },
    missingNumber: {
        fr: 'Adresse incomplète: numéro manquant',
        nl: 'Onvolledig adres: ontbrekend nummer',
        en: 'Incomplete address: number missing',
    },
    badNumber: {
        fr: `Adresse incorrecte: ce numéro n'existe pas`,
        nl: 'Verkeerd adres: dit nummer bestaat niet',
        en: 'Bad address: incorrect number',
    },
    contactLabel: {
        fr: 'Pour toute demande d’information, contactez ',
        nl: 'Voor alle informatie, contacteer ',
        en: 'For any information, contact',
    },
    linkContactBE: {
        fr: 'https://environnement.brussels/bruxelles-environnement/nous-contacter',
        nl: 'https://leefmilieu.brussels/leefmilieu-brussel/contacteer-ons',
        en: 'https://environnement.brussels/bruxelles-environnement/nous-contacter',
    },
    linkContactBELabel: {
        fr: 'Bruxelles Environnement',
        nl: 'Leefmilieu Brussel',
        en: 'Bruxelles Environnement',
    },
    urbisLabel: {
        fr: 'Orthophotographie et géocodeur © ',
        nl: 'Orthofotografie en geocoder  © ',
        en: 'Orthophotography and geocoder © ',
    },
    urbisLink: {
        fr: 'https://cirb.brussels/fr/nos-solutions/urbis-solutions/urbis-data',
        nl: 'https://cibg.brussels/nl/onze-oplossingen/urbis-solutions/urbis-data?set_language=nl',
        en: 'https://bric.brussels/en/our-solutions/urbis-solutions/urbis-data?set_language=en',
    },
    creatorsLabel: {
        fr: 'Conception et réalisation : ',
        nl: 'Ontwerp en productie : ',
        en: 'Design and production : ',
    },
    infLocatePitch: {
        fr: texts.infLocatePitchFR,
        nl: texts.infLocatePitchNL,
        en: 'Rainwater infiltration is hampered by urbanisation. This is a growing problem that increases flooding, clogs up wastewater treatment systems, reduces underground water stocks and reinforces heat islands.',
    },
    infAnalyse: {
        fr: "Analyser mon **contexte** d'infiltration d'eau de pluie",
        nl: 'Analyseer mijn **context** van regenwaterinfiltratie',
        en: 'Analyse **my context** of rainwater infiltration',
    },
    sidebarText: {
        fr: "Analyse de l'infiltration d'eau de pluie au",
        nl: 'Analyse van de infiltratie van regenwater bij de', //nltodo
        en: 'Analysis of rainwater infiltration at',
    },
    backToMap: {
        fr: 'Revenir à la carte',
        nl: 'Terug naar de kaart', //nltodo
        en: 'Back to map',
    },
    printAnalysis: {
        fr: "Imprimer l'analyse complète en pdf",
        nl: 'De analyse in PDF afdrukken', //nltodo
        en: 'Download full analysis in PDF',
    },
    report: {
        fr: 'Rapport',
        nl: 'Verslag', //nltodo
        en: 'Report',
    },
    projectAt: {
        fr: 'Le projet au',
        nl: 'Het project op', //nltodo
        en: 'The project at',
    },
    hasRestrictions: {
        fr: "fait l'objet:",
        nl: 'is onderworpen aan', //nltocheck
    },
    environmentalPermits: {
        fr: "d'une demande de permis d'environnement",
        nl: 'een aanvraag voor een milieuvergunning', // nltocheck
        en: 'environmental permits',
    },
    planningPermits: {
        fr: "d'une demande de permis d'urbanisme",
        nl: 'een aanvraag voor een bouwvergunning', // nltocheck
        en: 'planning permits',
    },

    relatedTools: {
        fr: "D'autres outils liés: ",
        nl: 'Andere gerelateerde tools: ',
        en: '',
    },
    relatedToolsBruwater: {
        fr: 'BruWater',
        nl: 'BruWater', // nltocheck
        en: 'BruWater',
    },
    relatedToolsBrugeotool: {
        fr: 'BruGeoTool',
        nl: 'BruGeoTool', // nltocheck
        en: 'BruGeoTool',
    },
    relatedToolsAnd: {
        fr: 'et',
        nl: 'en', // nltocheck
        en: 'and',
    },
    seeDetail: {
        fr: 'Toutes les informations',
        nl: 'Alle informatie',
    },
    seeSummary: {
        fr: 'Résumé',
        nl: 'Samenvatting',
    },
    legalMeasures: {
        fr: 'Dispositions réglementaires',
        nl: 'Wettelijke bepalingen', //nltodo
    },
    proximityToElements: {
        fr: "Proximité d'éléments sensibles",
        nl: 'Nabijheid van gevoelige elementen', //nltodo
    },
    hydrogeoAndFloodRisk: {
        fr: "Hydrogéologie et risques d'inondation",
        nl: 'Hydro&shy;geologie en over&shy;stro&shy;mings&shy;ge&shy;vaar', //nltodo
    },

    seeCarrot: {
        fr: 'Voir analyse',
        nl: `Naar de analyse`, //nltodo
    },
    rruPitch: {
        fr: texts.infRRUPitchFR,
        nl: texts.infRRUPitchNL,
    },
    infNoCategory: {
        fr: texts.infNoCategoryFR,
        nl: texts.infNoCategoryNL,
    },
    infNoCatShort: {
        fr: `pas d'info`,
        nl: 'geen info', //nltodo
    },
    infCatBlanco: {
        fr: texts.infCatBlancoFR,
        nl: texts.infCatBlancoNL,
    },
    infCatBlancoShort: {
        fr: 'non repris',
        nl: 'niet opgenomen',
    },
    infCat0: {
        fr: texts.infCat0FR,
        nl: texts.infCat0NL,
    },
    infCat1: {
        fr: texts.infCat1FR,
        nl: texts.infCat1NL,
    },
    infCat2: {
        fr: texts.infCat2FR,
        nl: texts.infCat2NL,
    },
    infCat3: {
        fr: texts.infCat3FR,
        nl: texts.infCat3NL,
    },
    infCat4: {
        fr: texts.infCat4FR,
        nl: texts.infCat4NL,
    },
    infCat01: {
        fr: texts.infCat01FR,
        nl: texts.infCat01NL,
    },
    infCat02: {
        fr: texts.infCat02FR,
        nl: texts.infCat02NL,
    },
    infCat03: {
        fr: texts.infCat03FR,
        nl: texts.infCat03NL,
    },
    infCat04: {
        fr: texts.infCat04FR,
        nl: texts.infCat04NL,
    },
    infPPASyes: {
        fr: texts.infPPASyesFR,
        nl: texts.infPPASyesNL,
    },
    infPPASno: {
        fr: texts.infPPASnoFR,
        nl: texts.infPPASnoNL,
    },
    infRCUyes: {
        fr: texts.infRCUyesFR,
        nl: texts.infRCUyesNL,
    },
    infRCUno: {
        fr: texts.infRCUnoFR,
        nl: texts.infRCUnoNL,
    },
    infPADyes: {
        fr: texts.infPADyesFR,
        nl: texts.infPADyesNL,
    },
    infPADno: {
        fr: texts.infPADnoFR,
        nl: texts.infPADnoNL,
    },
    infHeritagePitch: {
        fr: texts.infHeritagePitchFR,
        nl: texts.infHeritagePitchNL,
    },
    infHeritageNoPitch: {
        fr: texts.infHeritagePitchNoFR,
        nl: texts.infHeritagePitchNoNL,
    },
    // infPrescription: {
    //     fr: texts.infPrescriptionsFR,
    //     nl: texts.infPrescriptionsNL,
    // },
    infWaterCapture: {
        fr: texts.infWaterCaptureFR,
        nl: texts.infWaterCaptureNL,
    },
    infWaterCaptureNo: {
        fr: texts.infWaterCaptureNoFR,
        nl: texts.infWaterCaptureNoNL,
    },
    infInWaterSurface: {
        fr: texts.infInWaterSurfaceFR,
        nl: texts.infInWaterSurfaceNL,
    },
    infBufferWaterSurface: {
        fr: texts.infBufferWaterSurfaceFR,
        nl: texts.infBufferWaterSurfaceNL,
    },
    infBeyondBufferWaterSurface: {
        fr: `L’infiltration d’eau pluviale **n’aura pas lieu à proximité d'eaux de surface**.  
        *Source consultée pour l'analyse: [https://geodata.environnement.brussels](https://geodata.environnement.brussels/client/view/030319b5-9197-44b7-b675-1e0ca9e90bb2)*`,
        nl: `De infiltratie van regenwater **wordt niet voorzien in de nabijheid van oppervlaktewater**.  
        *Geraadpleegde bron voor de analyse: [https://geodata.leefmilieu.brussels](https://geodata.leefmilieu.brussels/client/view/030319b5-9197-44b7-b675-1e0ca9e90bb2)*`,
    },
    distance: {
        fr: 'Distance',
        nl: 'Afstand',
    },
    infInNatura2000: {
        fr: texts.infInNatura2000FR,
        nl: texts.infInNatura2000NL,
    },
    infBufferNatura2000: {
        fr: texts.infBufferNatura2000FR,
        nl: texts.infBufferNatura2000NL,
    },
    infOutNatura2000: {
        fr: texts.infOutNatura2000FR,
        nl: texts.infOutNatura2000NL,
    },
    infNearPhreatic: {
        fr: texts.infNearPhreaticFR,
        nl: texts.infNearPhreaticNL,
    },
    infNoPhreatic: {
        fr: texts.infNoPhreaticFR,
        nl: texts.infNoPhreaticNL,
    },
    infNoFloodAlea: {
        fr: texts.infNoFloodAleaFR,
        nl: texts.infNoFloodAleaNL,
    },
    infFloodAlea: {
        fr: texts.infFloodAleaFR,
        nl: texts.infFloodAleaNL,
    },
    legalEnv: {
        fr: 'Objectif de zéro rejet hors de la parcelle',
        nl: 'Streven naar nullozing op het perceel', //nltodo
    },
    legalCAT: {
        fr: 'Inventaire sol :',
        nl: 'Bodem&shy;inventaris : ',
    },
    legalRRU: {
        fr: 'RRU',
        nl: 'GSV',
    },
    legalPPAS: {
        fr: 'PPAS',
        nl: 'BBP',
    },
    legalRCU: {
        fr: 'RCU',
        nl: 'GemSV',
    },
    legalPAD: {
        fr: 'PAD',
        nl: 'RPA',
    },
    legalHeritage: {
        fr: 'Patrimoine',
        nl: 'Erfgoed',
    },
    proximityCapture: {
        fr: `Dans une zone de captage`,
        nl: 'Binnen grondwater&shy;winningen', //nltodo
    },
    outCapture: {
        fr: `Hors zone de captage`,
        nl: 'Buiten grondwater&shy;winningen', //nltodo
    },
    proximitySurface: {
        fr: `Eau de surface`,
        nl: 'Oppervlaktewater',
    },
    proximitySurfaceInWater: {
        fr: `Dans l’eau`,
        nl: 'In het water',
    },
    proximitySurfaceOutBuffer: {
        fr: `Hors zone de proximité d’eau de surface`,
        nl: 'Buiten de omgeving van oppervlakte&shy;water',
    },
    nearNatura2000: {
        fr: `Proche d'une zone naturelle`,
        nl: 'Dichtbij een Natuurlijk gebied',
    },
    inNatura2000: {
        fr: `Zone naturelle`,
        nl: 'Natuurlijk gebied',
    },
    outNatura2000: {
        fr: `Hors zone naturelle`,
        nl: 'Buiten Natuurlijk gebied',
    },
    hydrogeoPhreatic: {
        fr: `Nappe phréatique proche`,
        nl: 'Nabijheid van grondwater&shy;spiegel',
    },
    hydrogeoNoPhreatic: {
        fr: `Pas de nappe phréatique proche`,
        nl: 'Geen grondwater&shy;spiegel dichtbij',
    },
    hydrogeoFloodNo: {
        fr: `Hors zone d'aléas d'inondation`,
        nl: 'Buiten overstromings&shy;gevaar',
    },
    hydrogeoFloodYes: {
        fr: `Risque d'inondation`,
        nl: 'Overstromings&shy;gevaar en risico',
    },

    legalIntroDetailsUrba: {
        fr: "L'infiltration d'eau de pluie a lieu dans une zone soumise à des prescriptions spécifiques en matière de gestion de l'eau (infiltration) qui sont consignées dans :",
        nl: 'De infiltratie van regenwater vindt plaats in een gebied waarvoor specifieke eisen inzake waterbeheer (infiltratie) gelden, die zijn vastgelegd in :', //nltodo
    },

    legalIntroDetailsEnv: {
        fr: texts.infLegalEnvFR,
        nl: texts.infLegalEnvNL,
    },

    legalIntroDetailsEnvUrba: {
        fr: texts.infLegalEnvUrbaFR,
        nl: texts.infLegalEnvUrbaNL,
    },
    printTitle: {
        fr: 'Rapport Infiltrasoil',
        nl: 'Verslag Infiltrasoil',
    },
    date: {
        fr: 'Date du rapport',
        nl: 'Rapporteringsdatum',
    },
    url: {
        fr: 'Url du projet',
        nl: 'Projecturl',
    },
    backToAnalysis: {
        fr: "Retour à l'analyse",
        nl: 'Terug naar de analyse',
    },
    'helptext:printToPdf': {
        fr: texts.printToPdfFR,
        nl: texts.printToPdfNL,
    },
    disclaimerPrint: {
        fr: texts.disclaimerPrintFR,
        nl: texts.disclaimerPrintNL,
    },
    userProQuestion: {
        fr: texts.userProQuestionFR,
        nl: texts.userProQuestionNL,
    },
    infPitchFacilitator: {
        fr: texts.infPitchFacilitatorFR,
        nl: texts.infPitchFacilitatorNL,
    },
    credits: {
        fr: texts.creditsFR,
        nl: texts.creditsNL,
    },
    creditsPrint: {
        fr: 'Fond de plan © UrbIS. Données fournies par Bruxelles Environnement et les administrations régionales et communales concernées.',
        nl: 'Achtergrond © UrbIS. Gegevens verstrekt door Leefmilieu Brussel en de betrokken gewestelijke en gemeentelijke administraties.',
    },
    easterEggTitle: {
        fr: "Les infiltrations dans un plan d'eau ?",
        nl: 'Infiltratie in een waterlichaam?',
    },
    easterEggPlouf: {
        fr: 'Plouf! ',
        nl: 'Plons!',
    },
    easterEggText: {
        fr: "Les plans d'eau ont un mode d'infiltration de l'eau de pluie qui sort du focus de cette plateforme :)",
        nl: 'Waterlichamen hebben een infiltratie mode die buiten de focus van dit platform valt :)', //nltodo
    },
    fundingTitle: {
        fr: 'Primes disponibles',
        nl: 'Beschikbare premies',
    },
    fundingRegion: {
        fr: 'Prime régionale',
        nl: 'Regionale premies', //nltodo
    },
    fundingLocality: {
        fr: 'Prime communale',
        nl: 'Gemeente premies', //nltodo
    },
    fundingInfoRegion: {
        fr: `Il existe une prime régionale pour la pérméabilisation du sol. Plus d'informations sur le site web de Renolution: https://renolution.brussels/fr/aidesfinancieres/c4-gros-oeuvre-et-gestion-de-leau-demolition-pour-permeabiliser-le-sol.`,
        nl: `Er bestaat een gewestelijke premie voor bodeminfiltratie. Meer informatie op de Renolution-website: https://renolution.brussels/nl/premies-stimuli/c4-ruwbouw-en-waterbeheer-afbraak-ter-verbetering-van-bodeminfiltratie.`,
    },
    fundingInfoLocality: {
        fr: texts.fundingInfoLocalityFR,
        nl: texts.fundingInfoLocalityNL,
    },
    outOfRegion: {
        fr: 'Veuillez sélectionner un point dans la région Bruxelles-Capitale',
        nl: 'Gelieve een punt binnen het Brussels Hoofdstedelijk Gewest te kiezen', // nl todo
        en: '',
    },
    errorFetchingData: {
        fr: 'Problème de chargement des données. Avez-vous bien sélectionné un point dans la région Bruxelles-Capitale?',
        nl: 'Problemen met het laden van de gegevens. Heeft u een punt gekozen in het Brussels Hoofdstedelijk Gewest?', //nltodo
    },
};

type MDB = typeof messages;
export type InfiltrationMessageKey = keyof MDB;

declare module 'sdi/locale' {
    export interface MessageStore {
        infiltr(k: InfiltrationMessageKey, params?: Parameters): Translated;
        infiltrMD(
            k: InfiltrationMessageKey,
            params?: Parameters
        ): DetailedReactHTMLElement<
            AllHTMLAttributes<HTMLDivElement>,
            HTMLDivElement
        >;
    }
}

MessageStore.prototype.infiltr = (
    k: InfiltrationMessageKey,
    params?: Parameters
) => formatMessage(messages[k], params);

MessageStore.prototype.infiltrMD = (
    k: InfiltrationMessageKey,
    params?: Parameters
) => markdown(formatMessage(messages[k], params));
