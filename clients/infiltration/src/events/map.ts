/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { fromNullable, fromPredicate } from 'fp-ts/lib/Option';
import { dispatchK, dispatch, observe } from 'sdi/shape';
import {
    viewEventsFactory,
    scaleEventsFactory,
    IViewEvent,
    ViewDirt,
    tryCoord2D,
    addFeaturesToLayer,
    addLayer,
    removeLayerAll,
    checkMap,
} from 'sdi/map';
import { IUgWsResponse } from 'sdi/ports/geocoder';
import {
    fetchIO,
    FieldsDescriptorList,
    ILayerInfo,
    IMapInfo,
    Inspire,
    MessageRecord,
    StreamingMetaIO,
} from 'sdi/source';
import { Coordinate } from 'ol/coordinate';
import { navigateCoord } from './route';
import { fetchDatasetMetadata, fetchLayer, fetchMap } from '../remote';
import { clearAddress } from './infiltration';
import { addStream, mapStream, pushStreamExtent } from 'sdi/geodata-stream';
import {
    getLayerData,
    getSyntheticLayerInfo,
    getView,
    MAIN_MAP,
    MapName,
    MINI_MAP,
} from '../queries/map';
import { getLayout, getMapInfoOpt } from '../queries/app';
import { Extent } from 'ol/extent';
import { getApiUrl } from 'sdi/app';
import { isNotNullNorUndefined } from 'sdi/util';

const logger = debug('sdi:events/map');

export const scalelineEvents = scaleEventsFactory(dispatchK('port/map/scale'));

export const updateGeocoderResponse = (serviceResponse: IUgWsResponse | null) =>
    dispatch('component/geocoder/response', () => serviceResponse);

export const clearGeocoderResponse = () => updateGeocoderResponse(null);

export const updateGeocoderTerm = (address: string) =>
    dispatch('component/geocoder/input', () => address);

// const infiltrationLocateId = 'infiltration-locate';

// const metadataTemplate = (): Inspire => ({
//     id: infiltrationLocateId,
//     geometryType: 'MultiPolygon',
//     resourceTitle: { fr: 'solar', nl: 'solar', en: 'solar' },
//     resourceAbstract: { fr: '', nl: '', en: '' },
//     uniqueResourceIdentifier: infiltrationLocateId,
//     topicCategory: [],
//     keywords: [],
//     geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
//     temporalReference: {
//         creation: '2018-04-12T14:51:27.335376Z',
//         revision: '2018-04-12T14:51:27.335030Z',
//     },
//     responsibleOrganisation: [1],
//     metadataPointOfContact: [1],
//     metadataDate: '2018-04-12T14:51:27.335030Z',
//     published: false,
//     dataStreamUrl: null,
//     maintenanceFrequency: 'unknown',
// });

// const groupTemplate = (tag: Tag, fillColor: string): PolygonDiscreteGroup => ({
//     strokeColor: '#666',
//     patternAngle: 0,
//     strokeWidth: 1,
//     label: { nl: '', fr: '', en: '' },
//     fillColor,
//     values: [tag],
//     pattern: false,
// });

// const withPatern = (
//     g: PolygonDiscreteGroup,
//     patColor: string,
//     strokeWidth: number
// ): PolygonDiscreteGroup => ({
//     ...g,
//     strokeWidth,
//     pattern: true,
//     patternColor: g.fillColor,
//     patternAngle: 90,
//     fillColor: patColor,
// });

// const layerTemplate = (): ILayerInfo => ({
//     id: infiltrationLocateId,
//     legend: null,
//     group: null,
//     metadataId: infiltrationLocateId,
//     visible: true,
//     featureViewOptions: { type: 'default' },
//     style: {
//         groups: [
//             // groupTemplate('great', '#8db63c'),
//             // groupTemplate('good', '#ebe316'),
//             // groupTemplate('unusable', '#006f90'),
//             // groupTemplate('not-computed', '#000000'),
//             // withPatern(groupTemplate('great-flat', '#8db63c'), 'white', 1.5),
//             // withPatern(groupTemplate('good-flat', '#ebe316'), 'white', 1.5),
//             // withPatern(groupTemplate('unusable-flat', '#006f90'), 'white', 1.5),
//         ],
//         propName: 'tag',
//         kind: 'polygon-discrete',
//     },
//     layerInfoExtra: null,
//     visibleLegend: true,
// });

// const mapTemplate = (baseLayer: string): IMapInfo => ({
//     baseLayer,
//     id: `${infiltrationLocateId}-${baseLayer}`,
//     url: `/dev/null/infiltration-locate/`,
//     lastModified: 1523599299611,
//     status: 'published',
//     title: { fr: 'INFILTRATION', nl: 'INFILTRATIE', en: 'INFILTRATION' },
//     description: { fr: 'INFILTRATION', nl: 'INFILTRATIE', en: 'INFILTRATION' },
//     categories: [],
//     attachments: [],
//     layers: [layerTemplate()],
// });

// const BASEMAPS = {
//     gray: 'urbis.irisnet.be/urbis_gray',
//     ortho: 'urbis.irisnet.be/ortho2018_Toponymy',
// };

export const loadMap = (mid: string) =>
    fetchMap(mid).then(info =>
        dispatch('data/maps', maps => maps.concat(info))
    );

// export const selectMapGray = () =>
//     dispatch(
//         'app/current-map',
//         () => `${infiltrationLocateId}-${BASEMAPS.gray}`
//     );

// export const selectMapOrtho = () =>
//     dispatch(
//         'app/current-map',
//         () => `${infiltrationLocateId}-${BASEMAPS.ortho}`
//     );

export const dispatchMainView = dispatchK('port/map/main/view');
export const dispatchMiniView = dispatchK('port/map/mini/view');

export const updateMapView = (name: MapName) => {
    const main = viewEventsFactory(dispatchMainView);
    const mini = viewEventsFactory(dispatchMiniView);
    switch (name) {
        case MAIN_MAP:
            return (data: IViewEvent) => {
                main.updateMapView(data);
            };
        case MINI_MAP:
            return (data: IViewEvent) => {
                mini.updateMapView(data);
            };
    }
};

export const { setScaleLine } = scaleEventsFactory(dispatchK('port/map/scale'));

export const updateLoading = (ms: MessageRecord[]) => {
    console.log(`>> updateLoading ${ms.length}`);
    dispatch('port/map/loading', () => ms.concat());
};

export const centerMapOn = (coord: Coordinate) =>
    dispatch('port/map/mini/view', v => ({
        ...v,
        dirty: 'geo' as ViewDirt,
        center: coord,
    }));

export const zoomIn = () =>
    dispatch('port/map/mini/view', v => ({
        ...v,
        dirty: 'geo' as ViewDirt,
        zoom: v.zoom + 1,
    }));

export const zoomOut = () =>
    dispatch('port/map/mini/view', v => ({
        ...v,
        dirty: 'geo' as ViewDirt,
        zoom: v.zoom - 1,
    }));

export const setPosition = (coord: Coordinate) => {
    clearAddress();
    tryCoord2D(coord).map(navigateCoord);
};

const onMainMap = () => {
    const layout = getLayout();
    return layout === 'home' || layout === 'map';
};

const getMapName = () => (onMainMap() ? MAIN_MAP : MINI_MAP);

/**
 * Following is map loading at its finest,
 * taken from `view`, we should put that
 * into a shared component at some point.
 */

observe('port/map/main/view', view => {
    if (onMainMap()) {
        fromNullable(view.extent).map(e =>
            mapStream(e, ({ uri, lid }, extent) =>
                loadLayerDataExtent(lid, uri, extent)
            )
        );
    }
});

observe('port/map/mini/view', view => {
    if (!onMainMap()) {
        fromNullable(view.extent).map(e =>
            mapStream(e, ({ uri, lid }, extent) =>
                loadLayerDataExtent(lid, uri, extent)
            )
        );
    }
});

const layerInRange = (info: ILayerInfo) => {
    const low = fromNullable(info.minZoom).getOrElse(0);
    const high = fromNullable(info.maxZoom).getOrElse(30);
    const zoom = getView(getMapName())().zoom;
    return info.visible && zoom > low && zoom < high;
};

const whenInRange = fromPredicate(layerInRange);

// const { markVisibilityPending, delVisibilityPending, isVisibilityPending } =
//     (() => {
//         const register: Set<string> = new Set();

//         const hash = (a: string, b: string) => `${a}/${b}`;
//         const markVisibilityPending = (mid: string, lid: string) =>
//             register.add(hash(mid, lid));

//         const delVisibilityPending = (mid: string, lid: string) =>
//             register.delete(hash(mid, lid));

//         const isVisibilityPending = (mid: string, lid: string) =>
//             register.has(hash(mid, lid));

//         return {
//             markVisibilityPending,
//             delVisibilityPending,
//             isVisibilityPending,
//         };
//     })();

const prepareStreamedLayer = (
    // mapInfo: IMapInfo,
    layerInfo: ILayerInfo,
    md: Inspire,
    fields: FieldsDescriptorList
) => {
    dispatch('data/layers', state => {
        state[md.uniqueResourceIdentifier] = {
            type: 'FeatureCollection',
            fields,
            features: [],
        };
        return state;
    });
    if (layerInfo.visible && layerInRange(layerInfo)) {
        fromNullable(getView(getMapName())().extent).map(e => {
            loadLayerDataExtent(layerInfo.id, md.uniqueResourceIdentifier, e);
        });
    }
    // else {
    //     markVisibilityPending(mapInfo.id, layerInfo.id);
    // }
};

const loadLayer = (layerInfo: ILayerInfo) => {
    fetchDatasetMetadata(getApiUrl(`metadatas/${layerInfo.metadataId}`))
        .then(md => {
            dispatch('data/metadata', state => {
                state[md.id] = md;
                return state;
            });

            addLayer(
                MAIN_MAP,
                () => getSyntheticLayerInfo(layerInfo.id),
                () => getLayerData(md.uniqueResourceIdentifier)
            );

            addLayer(
                MINI_MAP,
                () => getSyntheticLayerInfo(layerInfo.id),
                () => getLayerData(md.uniqueResourceIdentifier)
            );

            if (isNotNullNorUndefined(md.dataStreamUrl)) {
                addStream({
                    uri: md.uniqueResourceIdentifier,
                    lid: layerInfo.id,
                });
                fetchIO(StreamingMetaIO, md.dataStreamUrl)
                    .then(({ fields }) => {
                        prepareStreamedLayer(layerInfo, md, fields);
                    })
                    .catch(() => {
                        logger(
                            `[ERROR] failed to get fields for ${md.uniqueResourceIdentifier}`
                        );
                        prepareStreamedLayer(layerInfo, md, []);
                    });
            } else {
                loadLayerData(md.uniqueResourceIdentifier);
            }
        })
        .catch(err =>
            logger(`Failed to load MD ${layerInfo.metadataId}: ${err}`)
        );
};

const loadLayerDataExtent = (layerId: string, url: string, bbox: Extent) =>
    getSyntheticLayerInfo(layerId).map(({ info }) =>
        whenInRange(info).map(info => {
            // const title = getMetadata(info.metadataId).map(md =>
            //     getMessageRecord(md.resourceTitle)
            // );
            pushStreamExtent(bbox, { lid: layerId, uri: url });
            // title.map(updateLoadingAddOne);
            fetchLayer(
                `${url}?bbox=${bbox[0]},${bbox[1]},${bbox[2]},${bbox[3]}`
            )
                .then(layer => {
                    if (layer.features !== null) {
                        addFeaturesToLayer(getMapName(), info, layer.features);
                        dispatch('data/layers', state => {
                            if (url in state) {
                                state[url].features = state[
                                    url
                                ].features.concat(layer.features);
                            } else {
                                state[url] = layer;
                            }
                            return state;
                        });
                    }
                    // title.map(updateLoadingRemoveOne);
                })
                .catch(err => {
                    // title.map(updateLoadingRemoveOne);
                    logger(`Failed to load features at ${url} due to ${err}`);
                    dispatch('data/remote/errors', state => ({
                        ...state,
                        [url]: `${err}`,
                    }));
                });
        })
    );

const loadLayerData = (url: string) => {
    logger(`loadLayerData(${url})`);
    fetchLayer(url)
        .then(layer => {
            dispatch('data/layers', state => {
                logger(`Put layer ${url} on state`);
                state[url] = layer;
                return state;
            });
        })
        .catch(err => {
            logger(`Failed to load layer at ${url} due to ${err}`);
            dispatch('data/remote/errors', state => ({
                ...state,
                [url]: `${err}`,
            }));
        });
};

let miniMapIsLoaded = false;
let mainMapIsLoaded = false;

const loadMapFromInfo = (name: MapName) => (info: IMapInfo) => {
    removeLayerAll(name);
    info.layers.forEach(loadLayer);
    if (name === MINI_MAP) {
        miniMapIsLoaded = true;
    } else {
        mainMapIsLoaded = true;
    }
};

export const setupMap = (name: MapName) => {
    if (name === MINI_MAP && !miniMapIsLoaded) {
        checkMap(MINI_MAP)
            .chain(getMapInfoOpt)
            .map(loadMapFromInfo(MINI_MAP))
            .getOrElseL(() => setTimeout(() => setupMap(name), 120));
    } else if (name === MAIN_MAP && !mainMapIsLoaded) {
        checkMap(MAIN_MAP)
            .chain(getMapInfoOpt)
            .map(loadMapFromInfo(MAIN_MAP))
            .getOrElseL(() => setTimeout(() => setupMap(name), 120));
    }
};

logger('loaded');
