/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { assign, dispatch } from 'sdi/shape';
import { defaultInteraction, tryCoord2D } from 'sdi/map';
import { activity } from 'sdi/activity';

import { Layout } from './route';
import { fetchBaseLayerAll, fetchConfig, fetchPointData } from '../remote';

import { remoteError, remoteLoading, remoteSuccess } from 'sdi/source';
import { setCoord } from './infiltration';

const logger = debug('sdi:events/app');

export const activityLogger = activity('infiltration');

export const setLayout = (l: Layout) => {
    assign('app/layout', l);
    if ('map' === l) {
        dispatch('port/map/interaction', () => ({
            label: 'singleclick',
            state: null,
        }));
    } else {
        dispatch('port/map/interaction', defaultInteraction);
    }
};
// export const setLayoutFromPermitSelect = (ps: PermitSelection) => {
//     switch (ps) {
//         case 'env':
//             setLayout('env');
//             break;
//         case 'envAndUrba':
//             setLayout('all-permits');
//             break;
//         case 'urba':
//             setLayout('urba');
//             break;
//         case 'none':
//             setLayout('no-permit');
//             break;
//     }
// };

// const centerMap = (capakey: string) => {
//     const geoms = query('infiltration/data/geoms');
//     if (capakey in geoms) {
//         const [minx, miny, maxx, maxy] = bbox(geoms[capakey]);
//         const width = maxx - minx;
//         const height = maxy - miny;
//         const sideMax = Math.max(width, height);
//         const sideMin = Math.min(width, height);
//         const abbox: Extent = [
//             minx - (sideMax - sideMin) / 2,
//             miny - (sideMax - sideMin) / 2,
//             minx + sideMax,
//             miny + sideMax,
//         ];

//         logger(`bbox: ${[minx, miny, maxx, maxy]}`);
//         logger(`extent: ${abbox}`);

//         dispatch('port/map/main/view', state => ({
//             ...state,
//             dirty: 'geo/extent',
//             extent: abbox,
//         }));
//     }
// };

// export const reCenterMap = () => getCapakey().map(centerMap);

// const createCollection = (fs: Feature[]): FeatureCollection => ({
//     type: 'FeatureCollection',
//     crs: {
//         type: 'name',
//         properties: {
//             name: 'urn:ogc:def:crs:EPSG::31370',
//         },
//     },
//     features: fs,
// });

// const checkAddress = (ck: string) => {
//     logger('checkAddress');
//     const a = query('infiltration/address');
//     if (a) {
//         return Promise.resolve({});
//     }
//     const gs = query('infiltration/data/geoms');
//     const fc = gs[ck];
//     if (!isNotNullNorUndefined(fc)) {
//         return Promise.reject();
//     }
//     const { geometry } = pointOnFeature(fc);
//     if (null === geometry) {
//         return Promise.reject();
//     }
//     const [x, y] = geometry.coordinates;
//     return new Promise((resolve, reject) => {
//         queryReverseGeocoder(x, y, getLang())
//             .then(response => {
//                 if (!response.error) {
//                     logger(`Got address ${response.result.address}`);
//                     dispatch(
//                         'infiltration/address',
//                         () => response.result.address
//                     );
//                     resolve({});
//                 } else {
//                     reject();
//                 }
//             })
//             .catch(reject);
//     });
// };

export const loadAllBaseLayers = () => {
    fetchBaseLayerAll().then(blc => {
        dispatch('data/baselayers', () => blc);
    });
};

export const loadConfig = () =>
    fetchConfig().then(c => {
        assign('infiltration/map', c.mapId);
        return c;
    });

export const loadPointData = (x: number, y: number) => {
    assign('infiltration/point/data', remoteLoading);
    fetchPointData(x, y)
        .then(d =>
            dispatch('infiltration/point/data', () => {
                tryCoord2D(d.coordinates).map(setCoord);
                return remoteSuccess(d);
            })
        )
        .catch(err => assign('infiltration/point/data', remoteError(err)));
};

logger('loaded');
