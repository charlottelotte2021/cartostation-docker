/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { literal, union } from 'io-ts';
import { Router, Path } from 'sdi/router';

import { getLang } from 'sdi/app';
import { Coord2D, tryCoord2D } from 'sdi/map';
import { queryReverseGeocoder } from 'sdi/ports/geocoder';

import { printReportAction } from 'sdi/activity';
import { TypeOf } from 'sdi/source/io/io';
import {
    getAddress,
    getAddressStr,
    getCoord,
    getPermitSelection,
    getPointData,
    getX,
    getY,
    isUserProfessional,
} from '../queries/infiltration';

import { activityLogger, loadPointData, setLayout } from './app';
import {
    clearCoord,
    setAddressFromPoint,
    setEnvPermits,
    setUrbaPermits,
    setUserProfessional,
} from './infiltration';
import { scopeOption } from 'sdi/lib';
import { getNumber } from 'sdi/util';
import { index } from 'fp-ts/lib/Array';
import { none, some, fromEither, Option } from 'fp-ts/lib/Option';
import { getLayout, isLayoutFull } from '../queries/app';
import { centerMapOn, setupMap } from './map';
import { MAIN_MAP, MINI_MAP } from '../queries/map';
import { PointData } from '../remote';

const logger = debug('sdi:infiltration/route');

const RouteIO = union([
    literal('home'),
    literal('map'),
    literal('env'),
    literal('urba'),
    literal('all-permits'),
    literal('no-permit'),
    literal('print'),
]);
export type Route = TypeOf<typeof RouteIO>;
export type Layout =
    | Route
    | 'loader'
    | 'urba-full'
    | 'env-full'
    | 'all-permits-full'
    | 'no-permit-full';

export const { home, route, navigate } = Router<Route>('infiltration');

export const snap = (n: number, on = 1) => Math.round(n / on) * on;

const baseRouteParser = (p: Path) =>
    scopeOption()
        .let('x', index(0, p).chain(getNumber))
        .let('y', index(1, p).chain(getNumber))
        .map(({ x, y }) => [snap(x), snap(y)] as Coord2D)
        .map(coords => ({
            coords,
            pro: index(2, p).map(m => (m === 'pro' ? true : false)),
            full: index(3, p).chain(m => (m === 'full' ? some('full') : none)),
        }));

const isDifferentCoord = (coord: Coord2D, data: Option<PointData>) =>
    data
        .map(d => d.coordinates.toString() !== coord.toString())
        .getOrElse(true);

const initXY = (coord: Coord2D) => {
    const lang = getLang();
    if (getPointData().isNone() || isDifferentCoord(coord, getPointData())) {
        getPointData().chain(d =>
            tryCoord2D(d.coordinates).map(c =>
                logger(`${coord[0] === c[0]} AND ${coord[1] === c[1]}`)
            )
        );
        if (getAddress().isNone()) {
            queryReverseGeocoder(coord[0], coord[1], lang)
                .then(addr => {
                    // logger(`NEW ADDRESSE: ${addr.result.address.street.name}`);
                    setAddressFromPoint(addr.result.address);
                    return Promise.resolve(true);
                })
                .catch(err => logger(`Address Error: ${err}`));
        }
        // setCoord(coord);
        loadPointData(...coord);
    }
    centerMapOn(coord);
};

/**
 * The 'home' special route for index
 */
home('home', () => {
    setLayout('home');
    setupMap(MAIN_MAP);
    clearCoord();
});

route('map', () => {
    setLayout('map');
    setupMap(MAIN_MAP);
});

route(
    'no-permit',
    route => {
        route.map(({ coords, pro, full }) => {
            initXY(coords);
            setUrbaPermits(false);
            setEnvPermits(false);
            full.foldL(
                () => setLayout('no-permit'),
                () => setLayout('no-permit-full')
            );
            pro.map(setUserProfessional);
        });
        setupMap(MINI_MAP);
    },
    baseRouteParser
);
route(
    'urba',
    route => {
        route.map(({ coords, pro, full }) => {
            initXY(coords);
            setUrbaPermits(true);
            setEnvPermits(false);
            full.foldL(
                () => setLayout('urba'),
                () => setLayout('urba-full')
            );
            pro.map(setUserProfessional);
        });
        setupMap(MINI_MAP);
    },
    baseRouteParser
);
route(
    'env',
    route => {
        route.map(({ coords, pro, full }) => {
            initXY(coords);
            setEnvPermits(true);
            setUrbaPermits(false);
            full.foldL(
                () => setLayout('env'),
                () => setLayout('env-full')
            );
            pro.map(setUserProfessional);
        });
        setupMap(MINI_MAP);
    },
    baseRouteParser
);
route(
    'all-permits',
    route => {
        route.map(({ coords, pro, full }) => {
            initXY(coords);
            setEnvPermits(true);
            setUrbaPermits(true);
            full.foldL(
                () => setLayout('all-permits'),
                () => setLayout('all-permits-full')
            );
            pro.map(setUserProfessional);
        });
        setupMap(MINI_MAP);
    },
    baseRouteParser
);
route(
    'print',
    route => {
        route.map(({ coords }) => {
            initXY(coords);
        });
        setLayout('print');
        setupMap(MINI_MAP);
    },
    baseRouteParser
);

// end of route handlers

/**
 * Initial routing, should be called on loop effects
 * @param initial
 */
export const loadRoute = (initial: string[]) =>
    index(0, initial)
        .chain(prefix =>
            fromEither(
                RouteIO.decode(prefix).map(c => navigate(c, initial.slice(1)))
            )
        )
        .getOrElseL(navigateHome);

// navigation functions

export const navigateHome = () => navigate('home', []);

export const navigateMap = () => navigate('map', []);

const fullStr = (full: boolean) => (full ? 'full' : '');
const proStr = (isPro: boolean) => (isPro ? 'pro' : 'normal');

export const navigateDefault = (coord: Coord2D, full = false) =>
    navigate('no-permit', [
        snap(getX(coord)),
        snap(getY(coord)),
        proStr(isUserProfessional()),
        fullStr(full),
    ]);

export const navigateUrba = (coord: Coord2D, full = false) =>
    navigate('urba', [
        snap(getX(coord)),
        snap(getY(coord)),
        proStr(isUserProfessional()),
        fullStr(full),
    ]);

export const navigateEnv = (coord: Coord2D, full = false) =>
    navigate('env', [
        snap(getX(coord)),
        snap(getY(coord)),
        proStr(isUserProfessional()),
        fullStr(full),
    ]);

export const navigateAllPermits = (coord: Coord2D, full = false) =>
    navigate('all-permits', [
        snap(getX(coord)),
        snap(getY(coord)),
        proStr(isUserProfessional()),
        fullStr(full),
    ]);

export const navigateSummary = (coord: Coord2D) => {
    switch (getLayout()) {
        case 'all-permits-full':
            navigateAllPermits(coord);
            break;
        case 'env-full':
            navigateEnv(coord);
            break;
        case 'urba-full':
            navigateUrba(coord);
            break;
        default:
            navigateDefault(coord);
            break;
    }
};

export const navigateFull = (coord: Coord2D) => {
    switch (getLayout()) {
        case 'all-permits':
            navigateAllPermits(coord, true);
            break;
        case 'env':
            navigateEnv(coord, true);
            break;
        case 'urba':
            navigateUrba(coord, true);
            break;
        default:
            navigateDefault(coord, true);
            break;
    }
};

export const navigateCoord = (coord: Coord2D) => {
    switch (getLayout()) {
        case 'map':
        case 'no-permit':
            navigateDefault(coord);
            break;
        case 'all-permits':
            navigateAllPermits(coord);
            break;
        case 'env':
            navigateEnv(coord);
            break;
        case 'urba':
            navigateUrba(coord);
            break;
        case 'no-permit-full':
            navigateDefault(coord, true);
            break;
        case 'all-permits-full':
            navigateAllPermits(coord, true);
            break;
        case 'env-full':
            navigateEnv(coord, true);
            break;
        case 'urba-full':
            navigateUrba(coord, true);
            break;
    }
};

export const navigatePermitSelection = (env: boolean, urba: boolean) => {
    getCoord().map(coord => {
        switch (getPermitSelection(env, urba)) {
            case 'none':
                navigateDefault(coord, isLayoutFull());
                break;
            case 'envAndUrba':
                navigateAllPermits(coord, isLayoutFull());
                break;
            case 'env':
                navigateEnv(coord, isLayoutFull());
                break;
            case 'urba':
                navigateUrba(coord, isLayoutFull());
                break;
        }
    });
};

export const navigateUserPro = (
    env: boolean,
    urba: boolean,
    isPro: boolean
) => {
    setUserProfessional(isPro);
    navigatePermitSelection(env, urba);
};

export const navigatePrint = () => {
    getCoord().map(coord => {
        navigate('print', [snap(getX(coord)), snap(getY(coord))]);
        activityLogger(printReportAction(getAddressStr()));
    });
};

logger('loaded');
