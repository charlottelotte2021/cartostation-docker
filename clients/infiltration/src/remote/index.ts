/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import * as io from 'io-ts';
import { getApiUrl } from 'sdi/app';

import {
    FeatureCollection,
    fetchIO,
    fetchWithoutValidationIO,
    IMapInfo,
    IMapInfoIO,
    Inspire,
    InspireIO,
    IServiceBaseLayersIO,
    PositionIO,
    uuidIO,
} from 'sdi/source';
import { nullable } from 'sdi/source/io/io';

// export const fetchLayer = (url: string): Promise<FeatureCollection> =>
//     fetchWithoutValidationIO(url);

// export const fetchBaseLayer = (url: string): Promise<IMapBaseLayer> =>
//     fetchIO(IMapBaseLayerIO, url);

export const fetchMap = (mid: string): Promise<IMapInfo> =>
    fetchIO(IMapInfoIO, getApiUrl(`maps/${mid}`));

// export const fetchDatasetMetadata = (url: string): Promise<Inspire> =>
//     fetchIO(InspireIO, url);

export const fetchBaseLayerAll = () =>
    fetchIO(IServiceBaseLayersIO, getApiUrl(`wmsconfig/`));

// export const fetchNotes = (url: string) => fetchIO(WidgetDescriptionIO, url);

// const WaterProximityIO = io.union([
//     io.literal('inBuffer'),
//     io.literal('outBuffer'),
//     io.literal('inWater'),
// ]);
// export type WaterProximity = io.TypeOf<typeof WaterProximityIO>;

const Natura2000ProximityIO = io.union([
    io.literal('inBuffer'),
    io.literal('outBuffer'),
    io.literal('inNatura'),
]);
export type Natura2000Proximity = io.TypeOf<typeof Natura2000ProximityIO>;

const GroundCategoriesIO = io.union([
    io.literal('blanco'), // parcelle cadastrée mais catégorie "non repris"
    io.literal('0'),
    io.literal('1'),
    io.literal('2'),
    io.literal('3'),
    io.literal('4'),
    io.literal('0+1'),
    io.literal('0+2'),
    io.literal('0+3'),
    io.literal('0+4'),
]);
export type GroundCategories = io.TypeOf<typeof GroundCategoriesIO>;

const PointDataIO = io.interface({
    coordinates: PositionIO,
    rru: io.boolean, // always true in brussels (regional reglementation)
    rcu: io.boolean,
    pad: io.boolean,
    ppas: io.boolean,
    heritage: io.boolean,
    category: nullable(GroundCategoriesIO), // si parcelle cadastrée: catégorie
    waterCapture: io.boolean,
    surfaceWaterProx: nullable(io.number),
    natura2000Prox: Natura2000ProximityIO,
    phreaticProx: nullable(io.boolean), // if there is a phreatic >4m or <4m
    floodAlea: io.boolean,
    regionalPremium: io.boolean,
    municipalPremium: io.boolean,
});
export type PointData = io.TypeOf<typeof PointDataIO>;

export const ConfigIO = io.interface({
    mapId: uuidIO,
});
export type Config = io.TypeOf<typeof ConfigIO>;

export const fetchConfig = () =>
    fetchIO(ConfigIO, getApiUrl('geodata/infiltration/config/'));

export const fetchPointData = (x: number, y: number) =>
    fetchIO(PointDataIO, getApiUrl(`geodata/infiltration/point/${x}/${y}/`));

export const fetchDatasetMetadata = (url: string): Promise<Inspire> =>
    fetchIO(InspireIO, url);

export const fetchLayer = (url: string): Promise<FeatureCollection> =>
    fetchWithoutValidationIO(url);
