/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { makeLabel } from 'sdi/components/button';
import { renderCheckbox } from 'sdi/components/input/checkbox';
import { DIV, H3, NodeOrOptional, SPAN } from 'sdi/components/elements';

import {
    getAddressStr,
    getCoord,
    getPointDataRemote,
    hasEnvPermits,
    hasUrbaPermits,
    isUserProfessional,
    withEasterEgg,
} from 'infiltration/src/queries/infiltration';
import {
    navigateFull,
    navigateHome,
    navigatePermitSelection,
    navigateSummary,
    navigateUserPro,
} from 'infiltration/src/events/route';
import tr from 'sdi/locale';
import summary from './summary';
import detail from './detail';
import renderPicto from '../pictos';
import { easterEggBackToMap } from '../button';
import { none, some, Option } from 'fp-ts/lib/Option';
import { PointData } from 'infiltration/src/remote';
import { foldRemote } from 'sdi/source';
import { activityLogger } from 'infiltration/src/events/app';
import { checkboxAction } from 'sdi/activity';
// import renderPicto from '../pictos';

const logger = debug('sdi:infiltration');

const selectEnv = (selected: boolean) => {
    navigatePermitSelection(selected, hasUrbaPermits());
    if (selected) {
        activityLogger(checkboxAction('environmentalPermits'));
    }
};

const selectUrba = (selected: boolean) => {
    navigatePermitSelection(hasEnvPermits(), selected);
    if (selected) {
        activityLogger(checkboxAction('urbanPermits'));
    }
};

const selectPro = (selected: boolean) => {
    navigateUserPro(hasEnvPermits(), hasUrbaPermits(), selected);
    if (selected) {
        activityLogger(checkboxAction('professionalUser'));
    }
};

const renderEnvCheckbox = (checked: boolean) =>
    DIV(
        'checkbox-with-text',
        // renderPicto('01-permit-env-shadow'),
        // checked ? renderPicto('checkbox-checked') : renderPicto('checkbox_unchecked'),
        H3('permit', envCheckbox(checked))
        // checked ? tr.infiltrMD('infPrescription') : ''
    );
const envCheckbox = renderCheckbox(
    'env-perm-switch',
    () => tr.infiltr('environmentalPermits'),
    selectEnv
);
const urbaCheckbox = renderCheckbox(
    'urba-perm-switch',
    () => tr.infiltr('planningPermits'),
    selectUrba
);

const renderProCheckbox = (checked: boolean) =>
    DIV(
        'checkbox-with-text professional',
        // renderPicto('16-facilitator'),
        // checked ? renderPicto('checkbox-checked') : renderPicto('checkbox_unchecked'),
        H3('permit', proCheckbox(checked)),
        checked ? tr.infiltrMD('infPitchFacilitator') : ''
    );
const proCheckbox = renderCheckbox(
    'pro-switch',
    () => tr.infiltr('userProQuestion'),
    selectPro
);

const renderCommon = () =>
    DIV(
        'common',
        H3(
            'title',
            `${tr.infiltr('projectAt')} ${getAddressStr()} ${tr.infiltr(
                'hasRestrictions'
            )}`
        ),
        renderEnvCheckbox(hasEnvPermits()),
        DIV('checkbox-with-text', H3('permit', urbaCheckbox(hasUrbaPermits()))),
        renderProCheckbox(isUserProfessional())
    );

const seeDetailsBtn = makeLabel('navigate', 3, () => tr.infiltr('seeDetail'));
const seeSummaryBtn = makeLabel('navigate', 3, () => tr.infiltr('seeSummary'));

const renderFullClass = (full: boolean) =>
    full ? 'view-detail' : 'view-summary';

const withinRegion = (node: NodeOrOptional) =>
    foldRemote<PointData, string, Option<NodeOrOptional>>(
        () => none,
        () =>
            some(
                DIV(
                    { className: 'info' },
                    DIV(
                        'loader',
                        tr.core('loadingData'),
                        DIV({ className: 'loader-spinner' })
                    )
                )
            ),
        _err =>
            some(
                DIV(
                    { className: 'error' },
                    `${tr.infiltr('errorFetchingData')} `
                )
            ),
        data => (data.rru === true ? some(node) : none)
    )(getPointDataRemote());

const renderOutRegion = () =>
    DIV(
        { className: 'content' },
        DIV(
            { className: 'content--body disclaimer' },
            SPAN('out-of-region', tr.infiltr('outOfRegion'))
        )
    );

const renderResults = (full: boolean) =>
    withinRegion(full ? detail() : summary()).getOrElse(renderOutRegion());

const tabsWrapper = (full: boolean) =>
    DIV(
        'content',
        renderCommon(),
        getCoord().map(c =>
            DIV(
                `tab__wrapper ${renderFullClass(full)}`,
                seeSummaryBtn(() => navigateSummary(c), 'summary-btn'),
                seeDetailsBtn(() => navigateFull(c), 'detail-btn')
            )
        ),
        renderResults(full)
    );

const easterEggWrapper = () =>
    DIV(
        'content easter-egg',
        DIV(
            'common',
            H3('', tr.infiltr('easterEggTitle')),
            DIV(
                'link__wrapper',
                renderPicto('pic-arrow-b'),
                easterEggBackToMap(() => navigateHome())
            )
        ),
        DIV(
            'summary',
            DIV('plouf', tr.infiltr('easterEggPlouf')),
            DIV('result', renderPicto('pic-plouf'), tr.infiltr('easterEggText'))
        )
    );

const infoWrapper = (full: boolean) => tabsWrapper(full);

const render = (full: boolean) =>
    withEasterEgg() ? easterEggWrapper() : infoWrapper(full);

export default render;

logger('loaded');
