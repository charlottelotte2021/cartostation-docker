/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV, SPAN, INPUT, H1, BUTTON } from 'sdi/components/elements';
import { isENTER } from 'sdi/components/keycodes';
import tr from 'sdi/locale';
// import { switcherLarge } from 'sdi/components/lang-switch';
import {
    IUgWsAddress,
    IUgWsPoint,
    IUgWsQualificationCode,
    IUgWsResult,
    queryGeocoder,
} from 'sdi/ports/geocoder';
// import { getLang } from 'sdi/app/queries';
import { setFocusId } from 'sdi/app';
// import { MessageRecordLang } from 'sdi/source';
import { makeLabel, makeLabelAndIcon } from 'sdi/components/button';

import { navigateSummary } from '../../events/route';
import { setAddress, setCoord } from '../../events/infiltration';
import { setLayout } from '../../events/app';
import { updateGeocoderTerm, updateGeocoderResponse } from '../../events/map';
import { geocoderResponse, geocoderInput, MAIN_MAP } from '../../queries/map';
import {
    brugeotoolLink,
    // brugeotoolGoToLink,
    bruwaterLink,
    bruwaterGoToLink,
    brugeotoolGoToLink,
} from '../button';
import map from '../map';

const logger = debug('sdi:infiltration');

const updateAddress = (e: React.ChangeEvent<HTMLInputElement>) => {
    updateGeocoderTerm(e.target.value);
};

// const changeLang = (l: MessageRecordLang) => () =>
//     l === 'nl' ? setLang('fr') : setLang('nl');

// export const bigLangSwitch = () => {
//     const lc = getLang();
//     return BUTTON(
//         {
//             className: 'lang-switch-wrapper',
//             onClick: changeLang(lc),
//         },
//         switcherLarge(() => tr.infiltr('tradVersion'), lc)
//     );
// };

const searchAddress = () => {
    geocoderInput().foldL(
        () => updateGeocoderResponse(null),
        ({ addr, lang }) => {
            queryGeocoder(addr, lang).then(updateGeocoderResponse);
        }
    );
};

const addressToString = (a: IUgWsAddress) =>
    `${a.street.name} ${a.number}, ${a.street.postCode} ${a.street.municipality}`;

const renderGeocoderInput = () =>
    INPUT({
        className: 'locate-input',
        type: 'text',
        name: 'adress',
        placeholder: tr.infiltr('infiltrationGeocode'),
        onChange: updateAddress,
        onKeyPress: (e: React.KeyboardEvent<HTMLInputElement>) => {
            if (isENTER(e)) {
                searchAddress();
            }
        },
    });

const renderGeocoderButton = () =>
    BUTTON(
        { className: 'btn-analyse', onClick: searchAddress },
        SPAN('infiltraSoil'),
        tr.infiltr('research')
    );

const renderGeocoderInputWrapper = (...n: React.ReactNode[]) =>
    DIV('input-wrapper', ...n);

const renderMissingNb = (address: IUgWsAddress, key: number) =>
    DIV(
        {
            className: 'no-address',
            id: `adress-${key}`,
        },
        addressToString(address),
        DIV('address-error', tr.infiltr('missingNumber'))
    );

const renderBadNb = (address: IUgWsAddress, key: number) =>
    DIV(
        {
            className: 'no-address',
            id: `adress-${key}`,
        },
        addressToString(address),
        DIV('address-error', tr.infiltr('badNumber'))
    );

const renderAddressComplete = (address: IUgWsAddress, key: number) =>
    DIV(
        {
            id: `adress-${key}`,
        },
        addressToString(address)
    );

const renderAddress = (
    address: IUgWsAddress,
    qualificationCode: IUgWsQualificationCode,
    key: number
) => {
    switch (qualificationCode.policeNumber) {
        case '0':
        case '2':
            return renderMissingNb(address, key);
        case '1':
            return renderAddressComplete(address, key);
        case '3':
            return renderBadNb(address, key);
    }
};

const renderAddressBtn = (
    address: IUgWsAddress,
    point: IUgWsPoint,
    qualificationCode: IUgWsQualificationCode,
    key: number
) =>
    BUTTON(
        {
            className: 'adress-result',
            key,
            onClick: () => {
                setAddress(address, qualificationCode);
                setCoord([point.x, point.y]);
                updateGeocoderResponse(null);
                navigateSummary([point.x, point.y]);
            },
        },
        DIV({
            className: 'select-icon',
            'aria-labelledby': `adress-${key}`,
        }),
        renderAddress(address, qualificationCode, key)
    );

const renderGeocoderResults = (results: IUgWsResult[]) => {
    return results.map(({ point, address, qualificationCode }, key) => {
        // const coords: [number, number] = [result.point.x, result.point.y];
        return renderAddressBtn(address, point, qualificationCode, key);
    });
};

const renderGeocoderResultsWrapper = (...n: React.ReactNode[]) =>
    DIV(
        'geocoder-wrapper',
        H1({}, tr.infiltr('searchResult')),
        DIV('results', ...n)
    );

const renderGeocoder = (): React.ReactNode[] =>
    geocoderResponse().fold(
        [
            renderGeocoderInputWrapper(
                renderGeocoderInput(),
                renderGeocoderButton()
            ),
        ],
        ({ result }) => [
            renderGeocoderInputWrapper(
                renderGeocoderInput(),
                renderGeocoderButton()
            ),
            renderGeocoderResultsWrapper(renderGeocoderResults(result)),
        ]
    );

const pitch = () => DIV('locate-pitch', tr.infiltrMD('infLocatePitch'));

const illu = () => DIV('locate-illu');

const pitchWrapper = () => DIV('locate-pitch-wrapper', pitch(), illu());

const goToMap = () =>
    DIV(
        'locate-goto-map',
        DIV('sub-pitch', tr.infiltr('selectBuilding')),
        makeLabel('navigate', 1, () => tr.infiltr('onMap'))(() => {
            setLayout('map');
            setFocusId('locate-goto-search');
        }, 'infiltration-btn map-button')
    );

const linksOtherApps = () =>
    DIV(
        'locate-related-tools',
        `${tr.infiltr('relatedTools')} `,
        bruwaterLink(),
        ` ${tr.infiltr('relatedToolsAnd')} `,
        brugeotoolLink()
    );

const goToSearch = () =>
    DIV(
        { className: 'locate-goto-search', id: 'locate-goto-search' },
        makeLabelAndIcon('navigate', 1, 'chevron-left', () =>
            tr.infiltr('backToGeocode')
        )(() => setLayout('home'), 'infiltration-btn map-button')
        // BUTTON({
        //     className: 'map-button',
        //     onClick: () => setLayout('Locate:Geocoder'),
        // }, tr.infiltr('geocode')),
    );

const searchWrapper = () =>
    DIV(
        'locate-geocode',
        DIV('sub-pitch', tr.infiltrMD('infAnalyse')),
        ...renderGeocoder()
    );

const wrapperTop = () =>
    DIV(
        'wrapper-top',
        H1('', tr.infiltr('appName')),
        pitchWrapper(),
        searchWrapper(),
        goToMap(),
        linksOtherApps()
    );

const render = (withWrapper: boolean) => {
    if (withWrapper) {
        return DIV(
            'locate-box',
            //  bigLangSwitch(),
            map(MAIN_MAP),
            wrapperTop()
        );
    }
    return DIV(
        'locate-box',
        // bigLangSwitch(),
        map(MAIN_MAP),
        goToSearch(),
        DIV('related-tools', brugeotoolGoToLink(), bruwaterGoToLink())
    );
};

export default render;

logger('loaded');
