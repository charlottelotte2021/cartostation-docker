/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV } from 'sdi/components/elements';

import { IMapOptions, create } from 'sdi/map';
import {
    getBaseLayer,
    getCurrentMapInfo,
    getInteraction,
    getView,
    MAIN_MAP,
    MapName,
    MINI_MAP,
} from '../queries/map';
import { fromNullable } from 'fp-ts/lib/Option';
import {
    setPosition,
    setScaleLine,
    updateLoading,
    updateMapView,
} from '../events/map';

const logger = debug('sdi:comp:map');

const { mapSetTarget, registerTarget } = (function () {
    const targeters: Partial<{ [k in MapName]: (e: Element | null) => void }> =
        {};

    const registerTarget = (name: MapName, t: (e: Element | null) => void) => {
        targeters[name] = t;
    };

    const mapSetTarget = (name: MapName, e: Element | null) =>
        fromNullable(targeters[name]).map(f => f(e));

    return { mapSetTarget, registerTarget };
})();

const { mapUpdate, mapInsert } = (function () {
    const updaters: Partial<{ [k in MapName]: () => void }> = {};

    const mapInsert = (name: MapName, u: () => () => void) => {
        if (!(name in updaters)) {
            updaters[name] = u();
        }
    };
    const mapUpdate = (name: MapName) =>
        fromNullable(updaters[name]).map(f => f());
    return { mapInsert, mapUpdate };
})();

// const pickPlace = (coords: Coordinate) =>
//     tryCoord2D(coords).map(navigateGeneral);

const options = (name: MapName): IMapOptions => ({
    element: null,
    getBaseLayer: () => getBaseLayer().toNullable(),
    getMapInfo: () => getCurrentMapInfo().toNullable(),
    updateView: updateMapView(name),
    getView: getView(name),
    setLoading: updateLoading,
    setScaleLine,
});

const attachMap = (name: MapName) => (element: HTMLElement | null) => {
    if (name === MAIN_MAP) {
        mapInsert(name, () => {
            const {
                update,
                setTarget,
                // selectable,
                clickable,
                // follow,
                // andThen,
                // markable,
                // printable,
                // measurable,
            } = create(name, {
                ...options(name),
                element,
            });

            // selectable(
            //     singleSelectOptions({
            //         selectFeature: setSelectedFeature,
            //         clearSelection: () => clearSelectedFeature(null),
            //         getSelected: getSelectedFeature,
            //     }),
            //     getInteraction
            // );

            clickable({ setPosition }, getInteraction);

            // follow(name, updateMapView(bottomMapName));

            registerTarget(name, setTarget);

            // markable({ startMark, endMark }, getInteraction);

            // printable(
            //     {
            //         getRequest: getPrintRequest,
            //         setResponse: setPrintResponse,
            //     },
            //     getInteraction
            // );

            // measurable(
            //     {
            //         updateMeasureCoordinates,
            //         stopMeasuring: stopMeasure,
            //     },
            //     getInteraction
            // );

            // andThen(m => {
            //     const v = m.getView();
            //     v.setMinZoom(MIN_ZOOM);
            //     v.setMaxZoom(MAX_ZOOM);
            // });

            return update;
        });
    } else if (name === MINI_MAP) {
        mapInsert(name, () => {
            const {
                update,
                setTarget,
                clickable,
                // andThen
            } = create(name, {
                ...options(name),
                element,
            });

            registerTarget(name, setTarget);
            clickable({ setPosition }, getInteraction);
            // andThen(m => {
            //     const v = m.getView();
            //     v.setMinZoom(MIN_ZOOM);
            //     v.setMaxZoom(MAX_ZOOM);
            // });

            return update;
        });
    }

    mapSetTarget(name, element);
};

const render = (name: MapName) => {
    if (mapUpdate) {
        mapUpdate(name);
    }
    return DIV(
        { className: 'map-wrapper unfocused', 'aria-hidden': true },
        DIV({
            id: MAIN_MAP,
            key: name,
            className: 'map',
            ref: attachMap(name),
        })
    );
};

export default render;

logger('loaded');
