/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { fromNullable } from 'fp-ts/lib/Option';
import { getApps } from 'sdi/app/queries';
// import buttonFactory from 'sdi/components/button';
// import { queryK, dispatchK } from 'sdi/shape';
import { appName, appUrl } from 'sdi/source';
import { A, BUTTON, NodeOrOptional } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { getCoord } from '../queries/infiltration';
import { makeLabel } from 'sdi/components/button';
import { activityLogger } from '../events/app';
import { linkAction } from 'sdi/activity';
import { AllHTMLAttributes, ClassAttributes, ReactNode } from 'react';

// export const { button, remove } = buttonFactory(
//     queryK('component/button'),
//     dispatchK('component/button')
// );

const getAppUrl = (routeName: string) =>
    fromNullable(getApps().find(a => appName(a) === routeName)).map(app =>
        appUrl(app)
    );

export const trackedLink = (
    identifier: string,
    props: ClassAttributes<HTMLAnchorElement> &
        AllHTMLAttributes<HTMLAnchorElement>,
    ...children: ReactNode[]
) =>
    A(
        {
            ...props,
            onClick: _e => {
                activityLogger(linkAction(identifier));
            },
        },
        ...children
    );

export const getBrugeotoolURL = () => getAppUrl('brugeotool');
export const getBruwaterURL = () => getAppUrl('timeserie');

export const getBrugeotoolLink = (
    label: NodeOrOptional,
    choice: 'general' | 'technical' = 'general'
) =>
    getBrugeotoolURL().map(url =>
        getCoord().fold(
            trackedLink(
                url,
                {
                    href: url,
                    onClick: () => activityLogger(linkAction(url)),
                },
                label
            ),
            coord =>
                trackedLink(
                    url,
                    {
                        href: `${url}${choice}/${coord[0]}/${coord[1]}`,
                        onClick: () => activityLogger(linkAction(url)),
                    },
                    label
                )
        )
    );

export const getBruwaterLink = (label: NodeOrOptional) =>
    getBruwaterURL().map(url =>
        trackedLink(
            url,
            {
                href: url,
                onClick: () => activityLogger(linkAction(url)),
            },
            label
        )
    );

export const brugeotoolLink = () =>
    getBrugeotoolLink(tr.infiltr('relatedToolsBrugeotool'));

export const bruwaterLink = () =>
    getBruwaterLink(tr.infiltr('relatedToolsBruwater'));

export const brugeotoolGoToLink = () =>
    BUTTON(
        'btn btn-1 brugeotool',
        getBrugeotoolLink(tr.infiltr('goToBrugeotool'))
    );

export const brugeotoolGoToAnalysis = () =>
    BUTTON(
        'btn btn-1 brugeotool-analysis',
        getBrugeotoolLink(tr.infiltrMD('goToBrugeotoolAnalysis'))
    );

export const bruwaterGoToLink = () =>
    BUTTON('btn btn-1 bruwater', getBruwaterLink(tr.infiltr('goToBruWater')));

export const easterEggBackToMap = makeLabel('navigate', 3, () =>
    tr.infiltr('backToMap')
);
