/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV } from 'sdi/components/elements';
import header from 'sdi/components/header';
import footer from './components/footer';
import { loop } from 'sdi/app';

import { getLayout } from './queries/app';
import { loadRoute } from './events/route';
import locate from './components/locate';
import loader from './components/loader';
import general from './components/general';
import { loadMap } from './events/map';
import { loadAllBaseLayers, loadConfig } from './events/app';
import cssPrint from './components/css-print/index';

const logger = debug('sdi:app');

// export type AppLayout =
//     | 'Locate:Geocoder'
//     | 'Locate:Map'
//     | 'Loader'
//     | 'General:Summary'
//     | 'General:Detail';

const wrappedMain = (
    name: string,
    // eslint-disable-next-line @typescript-eslint/ban-types
    ...elements: React.DOMElement<{}, Element>[]
) =>
    DIV(
        { className: `infiltration-inner screen-${name}` },
        header('infiltration'),
        DIV({ className: `main ${name}` }, ...elements),
        footer()
    );

const renderLocateGeocoder = () => wrappedMain('locate-geocoder', locate(true));

const renderLocateMap = () => wrappedMain('locate-map', locate(false));

const renderLoader = () => wrappedMain('loader', loader());

const renderUrba = (full = false) => wrappedMain('urba', general(full));

const renderEnv = (full = false) => wrappedMain('env', general(full));

const renderAllPermits = (full = false) =>
    wrappedMain('all-permits', general(full));

const renderNoPermit = (full = false) =>
    wrappedMain('no-permit', general(full));

const renderMain = () => {
    switch (getLayout()) {
        case 'home':
            return renderLocateGeocoder();
        case 'map':
            return renderLocateMap();
        case 'loader':
            return renderLoader();
        case 'urba':
            return renderUrba();
        case 'env':
            return renderEnv();
        case 'all-permits':
            return renderAllPermits();
        case 'no-permit':
            return renderNoPermit();
        case 'urba-full':
            return renderUrba(true);
        case 'env-full':
            return renderEnv(true);
        case 'all-permits-full':
            return renderAllPermits(true);
        case 'no-permit-full':
            return renderNoPermit(true);
        case 'print':
            return cssPrint();
    }
};

const effects = (initialRoute: string[]) => () => {
    loadAllBaseLayers();
    loadConfig().then(c => loadMap(c.mapId));
    loadRoute(initialRoute);
};

const app = (initialRoute: string[]) =>
    loop('infiltration', renderMain, effects(initialRoute));

export default app;

logger('loaded');
