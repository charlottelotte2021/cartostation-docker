/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { ButtonComponent } from 'sdi/components/button';
import { AppConfig, IUser } from 'sdi/source';

import { Layout } from '../events/route';

// State Augmentation

declare module 'sdi/shape' {
    export interface IShape {
        'app/layout': Layout;
        'app/current-map': string | null;
        'app/route': string[];

        'component/button': ButtonComponent;

        'data/user': IUser | null;
        'app/capakey': null | string;
    }
}

export const defaultAppShape = (config: AppConfig) => ({
    'app/layout': 'home' as Layout,
    'app/current-map': null,
    'app/route': config.args,

    'app/capakey': null,

    'component/button': {},
});
