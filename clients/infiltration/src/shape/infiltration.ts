/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Coord2D } from 'sdi/map';
import { IUgWsAddress } from 'sdi/ports/geocoder';
import { FeatureCollection, remoteNone, RemoteResource } from 'sdi/source';

import { Nullable } from 'sdi/util';
import { PointData } from '../remote';

interface GenericContainer<T> {
    [k: string]: T;
}

declare module 'sdi/shape' {
    export interface IShape {
        'infiltration/address': Nullable<IUgWsAddress>;
        'infiltration/coord': Nullable<Coord2D>;
        'infiltration/loading': string[];
        'infiltration/loaded': string[];

        'infiltration/env-permits': boolean;
        'infiltration/urba-permits': boolean;
        'infiltration/professionnal': boolean;

        'infiltration/point/data': RemoteResource<PointData>;

        'infiltration/map': Nullable<string>;

        'infiltration/data/geoms': GenericContainer<FeatureCollection>;
    }
}

export const defaultInfiltrationShape = () => ({
    'infiltration/address': null,
    'infiltration/coord': null,
    'infiltration/loading': [],
    'infiltration/loaded': [],

    'infiltration/env-permits': false,
    'infiltration/urba-permits': false,
    'infiltration/professionnal': false,

    'infiltration/point/data': remoteNone,

    'infiltration/map': null,

    'infiltration/data/geoms': {},
});
