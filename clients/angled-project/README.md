_angled-project_ is a specific application for the Perspective.brussels data schema.

It is called _plans & projects_ within perspective.brussels deployment.

This application allows to encode and visualize information related to plans and real estate projects. One of its particularities is to keep the whole history of each encoded information.

This application proposes an approach at the project scale.  
For a more global approach, in the form of a list of projects, see the application [_query_](angled-query).

## User guide

The complete user guide is available here : https://cartostation.com/documentation
