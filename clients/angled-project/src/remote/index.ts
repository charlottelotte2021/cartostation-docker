/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import * as io from 'io-ts';
import { fetchIO, IAliasIO } from 'sdi/source';
import { ProfileListIO, ProfileList } from 'angled-core/profile';
import { ProjectIO } from 'angled-core/ui';
import {
    NovaIO,
    ContactIO,
    SiteIO,
    FundingOrgIO,
    TeamIO,
} from 'angled-core/ref';

export * from './project';
export * from './timeline';
export * from './nova';

export const fetchProjects = (url: string) => fetchIO(io.array(ProjectIO), url);

export const fetchProfiles = (url: string): Promise<ProfileList> =>
    fetchIO(ProfileListIO, url);

export const fetchContact = (url: string) => fetchIO(io.array(ContactIO), url);

export const fetchSite = (url: string) => fetchIO(io.array(SiteIO), url);

export const fetchNova = (url: string) => fetchIO(io.array(NovaIO), url);

export const fetchFunding = (url: string) =>
    fetchIO(io.array(FundingOrgIO), url);

export const fetchTeam = (url: string) => fetchIO(io.array(TeamIO), url);

export const fetchAlias = (url: string) => fetchIO(io.array(IAliasIO), url);
