/*
 *  Copyright (C) 2019 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as io from 'io-ts';
import { fetchIO } from 'sdi/source';
import { getApiUrl } from 'sdi/app';

// Too much CORS headache with urban server, lets proxy...
// const NovaURL = 'http://geoservices-others.irisnet.be/geoserver/ows';

// const filter =
//     (k: string, v: string) =>
//         `<Filter><PropertyIsEqualTo><PropertyName>${k}</PropertyName><Literal>${v}</Literal></PropertyIsEqualTo></Filter>`

// const options =
//     (): RequestInit => {
//         const headers = new Headers();
//         headers.append('Content-Type', 'application/json');

//         return {
//             method: "GET",
//             mode: 'no-cors',
//             cache: 'default',
//             redirect: 'follow',
//             credentials: 'omit',
//             headers,
//         };
//     };

const novaUrl = (key: string, id: string) => {
    // const attrs = {
    //     service: 'WFS',
    //     version: '2.0.0',
    //     request: 'GetFeature',
    //     typeName: 'Nova:vmnovaurbanview',
    //     outputFormat: 'application/json',
    //     srsName: 'EPSG:31370',
    //     filter: filter(key, id),
    // }

    // return withQueryString(NovaURL, attrs);

    return getApiUrl(`geodata/angled/nova/${key}/${id}`);
};

const anyDictIO = io.dictionary(io.string, io.any, 'anyDictIO');

export const fetchNova = (key: string, id: string) =>
    fetchIO(anyDictIO, novaUrl(key, id));
