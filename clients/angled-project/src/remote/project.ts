// import * as io from 'io-ts';
import { postIO, postUnrelatedIO, deleteIO } from 'sdi/source';
import { UnitPost, ProjectIO } from 'angled-core/ui';
import { Tagged, TaggedIO } from 'angled-core/ref';
import { getApiUrl } from 'sdi/app';

export const postProject = (url: string) => postIO(ProjectIO, url, {});

export const postUnits = (pid: number, units: UnitPost) =>
    postUnrelatedIO(
        ProjectIO,
        getApiUrl(`geodata/angled/p/project/${pid}/inform/`),
        units
    );

export const postTag = (url: string, t: Partial<Tagged>) =>
    postIO(TaggedIO, url, t);

export const deleteTag = (url: string) => deleteIO(url);
