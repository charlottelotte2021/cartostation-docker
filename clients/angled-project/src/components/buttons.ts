import buttonFactory from 'sdi/components/button';
import { queryK, dispatchK } from 'sdi/shape';
import { DIV } from 'sdi/components/elements';
import { Translated } from 'sdi/locale';

export const radioBtn = (msg: Translated) =>
    DIV(
        { className: 'btn--radio' },
        DIV({ className: 'bullet' }),
        DIV({ className: 'label' }, msg)
    );

export const {
    makeIcon,
    makeLabel,
    makeLabelAndIcon,
    makeRemove,
    makeRemoveLabelAndIcon,
} = buttonFactory(queryK('component/button'), dispatchK('component/button'));
