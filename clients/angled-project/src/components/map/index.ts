/*
 *  Copyright (C) 2019 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV } from 'sdi/components/elements';

import {
    IMapOptions,
    create,
    SingleClickOptions,
    singleSelectOptions,
} from 'sdi/map';
import { mapSelectID } from 'angled-core/map';
import {
    getMapInfo,
    getCurrentBaseLayer,
    getView,
    getInteraction,
} from 'angled-core/queries/map';
import {
    scalelineEvents,
    viewEvents,
    startMark,
    endMark,
} from 'angled-core/events/map';
import { some, none, Option } from 'fp-ts/lib/Option';
import { navigateFormNaked } from 'angled-project/src/events/route';

import { geocoder } from 'angled-core/components/geocoder';
import {
    setSearchUnit,
    setSearchInput,
} from 'angled-project/src/events/project';

const logger = debug('sdi:comp:map');

export const projectSelectMap = 'project-select-map';

const options: IMapOptions = {
    element: null,
    getBaseLayer: getCurrentBaseLayer,
    getView,
    getMapInfo,

    updateView: viewEvents.updateMapView,
    setScaleLine: scalelineEvents.setScaleLine,
};

let mapSetTarget: Option<(t: HTMLElement | null) => void> = none;
let mapUpdate: Option<() => void> = none;

const selectOptions = singleSelectOptions({
    selectFeature: (_, fid) =>
        navigateFormNaked(typeof fid === 'number' ? fid : parseInt(fid, 10)),
    clearSelection: () => logger('clearSelection'),
    getSelected: () => {
        logger(`getSelected`);
        return { layerId: null, featureId: null };
    },
});

const clickOptions: SingleClickOptions = {
    setPosition: coordinates => {
        setSearchUnit('geometry');
        setSearchInput(
            `${Math.round(coordinates[0])}, ${Math.round(coordinates[1])}`
        );
    },
};

const attachMap = (element: HTMLElement | null) => {
    mapUpdate = mapUpdate.foldL(
        () => {
            const { update, setTarget, selectable, markable, clickable } =
                create(projectSelectMap, {
                    ...options,
                    element,
                });
            markable({ startMark, endMark }, getInteraction);
            selectable(selectOptions, getInteraction);
            clickable(clickOptions, getInteraction);
            mapSetTarget = some(setTarget);
            return some(update);
        },
        update => some(update)
    );

    if (element) {
        mapSetTarget.map(f => f(element));
    }
};

const render = () => {
    mapUpdate.map(f => f());
    return DIV(
        { className: 'map-wrapper' },
        DIV({
            id: mapSelectID,
            className: 'map',
            ref: attachMap,
        }),
        geocoder()
    );
};

export default render;

logger('loaded');
