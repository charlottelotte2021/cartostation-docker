import { Project, UnitHousingBatchData } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { PanelOptions } from 'angled-core/profile';
import {
    findTerm,
    termsInDomain,
    getDomainIdByName,
} from 'angled-core/queries/universe';
import tr, { fromRecord, formatNumber } from 'sdi/locale';
import { fromNullable } from 'fp-ts/lib/Option';
import { uniq, isNotNullNorUndefined } from 'sdi/util';
import { setoidNumber } from 'fp-ts/lib/Setoid';
import { withDisplayType } from 'angled-project/src/queries/project';

const uniqTerm = uniq(setoidNumber);

const sumDescription = (ds: UnitHousingBatchData[]) =>
    ds.reduce((acc, d) => acc + d.quantity, 0);

const term = (id: number) =>
    findTerm(id).fold<string>(`Term(${id})`, t => fromRecord(t.name));

// feature: to put in sdi/utils (same function "elem" in ui-list.ts)
const isInArray = (value: any, array: any[]) => array.indexOf(value) > -1;

// Returns the total cost of the project
// firstChoiceUnit is the unit that gives directly the total cost
// units gives the units to sum if firstChoiceUnit = 0
const totCost = (p: Project, firstChoiceUnit: number, units: number[]) => {
    const cost = p.cost;
    if (isNotNullNorUndefined(cost)) {
        const unitIds = cost.find(t => t.type === firstChoiceUnit)
            ? [firstChoiceUnit]
            : units;
        return cost
            .filter(c => isInArray(c.type, unitIds))
            .reduce((acc, c) => acc + c.cost, 0);
    }
    return 0;
};

// Returns the total area of the project
// firstChoiceUnit is the unit that gives directly the total area
// units gives the units to sum if firstChoiceUnit = 0
const totArea = (p: Project, firstChoiceUnit: number, units: number[]) => {
    const area = p.area;
    if (isNotNullNorUndefined(area)) {
        const unitIds = area.find(t => t.type === firstChoiceUnit)
            ? [firstChoiceUnit]
            : units;
        return area
            .filter(a => isInArray(a.type, unitIds))
            .reduce((acc, a) => acc + a.value, 0);
    }
    return 0;
};

export const renderHousing = (p: Project, options: PanelOptions) => {
    const hs = p.housing_batch;
    const title = fromNullable(options['title']).getOrElse(
        tr.angled('housingOverview')
    );
    if (hs === undefined || hs.length === 0) {
        return DIV(
            'container container--widget project-view view--empty cost',
            DIV(
                'widget__header',

                DIV(
                    'widget__header--top',
                    withDisplayType(
                        'writer',
                        DIV(
                            'widget__meta',
                            DIV(
                                'widget__pub-info',
                                tr.angled('computedInfosProjectData')
                            )
                        )
                    ),
                    DIV('widget__name', title)
                )
            ),
            DIV('widget__body', tr.angled('noHousingEntry'))
        );
    }

    // To find the terms of the "Legislature" domain
    const legislatureDomain = getDomainIdByName('Législature');
    const legislatureUnits = legislatureDomain
        ? termsInDomain(legislatureDomain.id).map(t => t.id)
        : [];

    // Use the "cout général HTVA" (unit 174) if there is one, and the sum of the sub-costs (units 10, 11, 12) if not.
    const cost = totCost(p, 174, [10, 11, 12]);
    // Use the "superficie plancher totale" (unit 31) if there is one, or the sum of "emprise au sol", "superficie plancher hors-sol" and "superficie plancher sous-sol" (units 32, 200, 201) if not.
    const area = totArea(p, 31, [32, 200, 201]);

    const costByArea = Math.round((cost * 100) / area) / 100;
    const numElem = sumDescription(hs);
    // CostByNum = total cost / total number of appartements
    const costByNum = Math.round((cost * 100) / numElem) / 100;

    const levels = uniqTerm(hs.map(h => h.rental_level));
    const disposals = uniqTerm(hs.map(h => h.disposal));
    const legislatures = uniqTerm(
        hs
            .map(h => h.planned_release)
            .filter(h => isInArray(h, legislatureUnits))
    );

    const levelElems = levels.map(l =>
        DIV(
            'v',
            `${term(l)}: ${sumDescription(
                hs.filter(h => h.rental_level === l)
            )}`
        )
    );
    const dispElems = disposals.map(d =>
        DIV(
            'v',
            `${term(d)}: ${sumDescription(hs.filter(h => h.disposal === d))}`
        )
    );
    const legElems = legislatures.map(l =>
        DIV(
            'v',
            `${term(l)}: ${sumDescription(
                hs.filter(h => h.planned_release === l)
            )}`
        )
    );
    const priceSqmElems = () => DIV('v', `${formatNumber(costByArea)} €/m²`);
    const priceElems = () => DIV('v', `${formatNumber(costByNum)} €`);

    return DIV(
        'container container--widget project-view cost',
        DIV(
            'widget__header',

            DIV(
                'widget__header--top',
                withDisplayType(
                    'writer',
                    DIV(
                        'widget__meta',
                        DIV(
                            'widget__pub-info',
                            tr.angled('computedInfosProjectData')
                        )
                    )
                ),
                DIV('widget__name', title)
            )
        ),
        DIV(
            'widget__body',
            DIV('kv', DIV('k', tr.angled('housingLevels')), ...levelElems),
            DIV('kv', DIV('k', tr.angled('housingDisposals')), ...dispElems),
            DIV('kv', DIV('k', tr.angled('housingLegislature')), ...legElems),
            DIV(
                'kv',
                DIV('k', tr.angled('housingPriceSquareMeter')),
                priceSqmElems()
            ),
            DIV('kv', DIV('k', tr.angled('housingPrice')), priceElems())
        )
    );
};
