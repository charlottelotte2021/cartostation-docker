import { Project } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { PanelOptions } from 'angled-core/profile';
import { findTerm } from 'angled-core/queries/universe';
import { fromRecord } from 'sdi/locale';
import { fromNullable } from 'fp-ts/lib/Option';

export const renderCost = (p: Project, options: PanelOptions) => {
    const costs = p.cost;
    const typeOfCost = options['type'];
    const unit = fromNullable(options['unit']).getOrElse('euro');
    const title = fromNullable(options['title']).getOrElse('Cost');
    if (typeOfCost === undefined) {
        return DIV({}, `This widget needs a 'type' option`);
    } else if (typeof typeOfCost === 'string') {
        return DIV(
            {},
            `This widget needs a 'type' option of type number (identifier of a term)`
        );
    } else if (costs === undefined || costs.length === 0) {
        return DIV({}, `No costs encoded for this project`);
    }

    const total = costs
        .filter(c => c.type === typeOfCost)
        .reduce((acc, c) => acc + c.cost, 0);
    const typeName = findTerm(typeOfCost).fold<string>('Unknown type', t =>
        fromRecord(t.name)
    );
    return DIV(
        { className: 'container container--widget cost' },
        DIV(
            { className: 'widget__header' },
            DIV({ className: 'widget__name' }, title)
        ),
        DIV({ className: 'widget__body' }, `${typeName}: ${total} ${unit}`)
    );
};
