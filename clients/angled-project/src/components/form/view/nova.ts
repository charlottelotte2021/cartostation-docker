import { Project } from 'angled-core/ui';
import { DIV, A } from 'sdi/components/elements';
import { PanelOptions } from 'angled-core/profile';
import { fromNullable, none, some, Option } from 'fp-ts/lib/Option';
import { loadNovaWFS } from 'angled-project/src/events/project';
import { getNovaWFS } from 'angled-project/src/queries/project';
import { scopeOption } from 'sdi/lib';
import tr, { fromRecord, formatDate } from 'sdi/locale';
import { isNotNullNorUndefined, stringToParagraphs } from 'sdi/util';
import { AngledMessageKey } from 'angled-core/locale';
import { remoteToOption, MessageRecordLang } from 'sdi/source';
import { ReactNode } from 'react';
import { getLang } from 'sdi/app';

const renderLoading = (s: string) =>
    DIV({ className: 'nova-loading' }, `loading ${s}`);
const renderError = (s: string, e: string) =>
    DIV({ className: 'nova-error' }, `error  lodaing ${s}: ${e}`);

const defaultFormat = (a: any): ReactNode =>
    fromNullable(a)
        .map(a => a.toString())
        .getOrElse('-');

const siginficantString = (a: any): Option<string> => {
    if (isNotNullNorUndefined(a)) {
        if (typeof a === 'string') {
            return a.length > 0 ? some(a) : none;
        } else if (typeof a.toString === 'function') {
            return siginficantString(a.toString());
        }
    }
    return none;
};

const siginficantTransString = (c: MessageRecordLang, a: any): Option<string> =>
    siginficantString(a).foldL(
        () => (c === getLang() ? none : some('-')),
        s => some(s)
    );

const simpleKey =
    (o: any) =>
    (k: AngledMessageKey, f = defaultFormat) =>
        siginficantString(o[k.split('/')[2]]).map(v =>
            DIV(
                { className: 'field' },
                DIV({ className: 'field__key' }, tr.angled(k)),
                DIV({ className: 'field__value' }, f(v))
            )
        );

const transKey =
    (o: any) =>
    (k: AngledMessageKey, f = defaultFormat) =>
        scopeOption()
            .let(
                'frVal',
                siginficantTransString('fr', o[`${k.split('/')[2]}fr`])
            )
            .let(
                'nlVal',
                siginficantTransString('nl', o[`${k.split('/')[2]}nl`])
            )
            .map(({ frVal, nlVal }) =>
                DIV(
                    { className: 'field' },
                    DIV({ className: 'field__key' }, tr.angled(k)),
                    DIV(
                        { className: 'field__value' },
                        f(fromRecord({ fr: frVal, nl: nlVal }))
                    )
                )
            );

const makeDate = (a: any) => {
    const d = new Date(a);
    if (Number.isNaN(d.getTime())) {
        return a.toString();
    }
    return formatDate(d);
};

const renderSuccess = (o: object) => {
    if (!isNotNullNorUndefined(o)) {
        return [];
    }
    const s = simpleKey(o);
    const t = transKey(o);

    return [
        t('nova/desc/typedossier'),
        s('nova/desc/refnova'),
        s('nova/desc/novaseq'),
        s('nova/desc/referencespecifique'),
        t('nova/desc/realobject', stringToParagraphs),
        t('nova/desc/statutpermis'),
        t('nova/desc/statutenquete'),
        s('nova/desc/numberpartfrom'),
        t('nova/desc/streetname'),
        s('nova/desc/zipcode'),
        t('nova/desc/municipality'),
        s('nova/desc/ri', a =>
            a === 'true' ? tr.angled('true') : tr.angled('false')
        ),
        s('nova/desc/ei', a =>
            a === 'true' ? tr.angled('true') : tr.angled('false')
        ),
        s('nova/desc/datedepot', makeDate),
        s('nova/desc/dateardosscomplet', makeDate),
        s('nova/desc/datearifirst', makeDate),
        s('nova/desc/datedebutmpp', makeDate),
        s('nova/desc/datefinmpp', makeDate),
        s('nova/desc/datecc', makeDate),
        s('nova/desc/datenotifdecision', makeDate),
        s('nova/desc/avissiamu', a =>
            a === 'true' ? tr.angled('true') : tr.angled('false')
        ),
        s('nova/desc/enquetepublique', a =>
            a === 'true' ? tr.angled('true') : tr.angled('false')
        ),
    ];
};

const render = (ns: string) =>
    remoteToOption(getNovaWFS(ns)).map(renderSuccess);

const urban = (o: any) =>
    fromNullable(o)
        .chain(o => fromNullable(o.s_iddossier))
        .map(v =>
            DIV(
                { className: 'nova-link' },
                A(
                    {
                        target: '_blank',
                        href: `https://data.urban.brussels/be/${getLang()}/cobat/${v}`,
                    },
                    `/cobat/${v}`
                )
            )
        );

const info = (ns: string) => {
    const resource = getNovaWFS(ns);
    switch (resource.tag) {
        case 'none':
            return none;
        case 'loading':
            return renderLoading(ns);
        case 'error':
            return renderError(ns, resource.error);
        case 'success':
            return urban(resource.data);
    }
};

export const renderNova = (p: Project, _options: PanelOptions) =>
    fromNullable(p.nova).map(nova => {
        const ids = nova.map(n => n.nova);
        ids.map(loadNovaWFS);
        return DIV(
            { className: 'container container--widget project-view' },
            DIV(
                { className: 'widget__header' },
                DIV(
                    { className: 'widget__header--top' },
                    DIV({ className: 'widget__name' }, 'NOVA'),
                    DIV({ className: 'widget__tools' }, ...ids.map(info))
                )
            ),
            DIV({ className: 'widget__body unit' }, ...ids.map(render))
        );
    });
