import * as debug from 'debug';
import { BUTTON, DIV, H1, H2, IMG, SPAN } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { sortedProfileList } from 'angled-project/src/queries/profile';
import { navigateHome, navigateForm } from 'angled-project/src/events/route';
import { Profile } from 'angled-core/profile';
import {
    getCurrentProjectName,
    getCurrentProjectDescription,
    getCurrentProjectImage,
    getCurrentProjectId,
    getCurrentProjectImageCaption,
} from 'angled-project/src/queries/project';
import { makeLabelAndIcon } from '../buttons';
import { stringToParagraphs } from 'sdi/util';
import { nameToString } from 'sdi/components/button/names';

const logger = debug('sdi:select');

const btnBackToSearch = makeLabelAndIcon('search', 1, 'chevron-left', () =>
    tr.angled('backToSearch')
);

const description = () =>
    DIV('helptext', stringToParagraphs(tr.angled('appDescriptionForm')));

const renderProjectName = () =>
    // H1('', `${tr.angled('selectedProject')} : ${getCurrentProjectName()}`);
    H1('', getCurrentProjectName());

const renderProjectDescription = () =>
    DIV(
        'project-preview__project-description',
        H2('', tr.angled('labelUnit_description')),
        getCurrentProjectDescription()
    );

const renderProjectImage = () =>
    DIV(
        'project-preview__project-image',
        getCurrentProjectImage() === 'empty'
            ? DIV('empty-image', nameToString('image'))
            : IMG({ src: getCurrentProjectImage() }),
        DIV('caption', getCurrentProjectImageCaption())
    );

const renderProjectPreview = () =>
    DIV(
        'project-preview',
        renderProjectName(),
        renderProjectImage(),
        renderProjectDescription()
    );


// could be factorized
const renderProfile = (p: Profile) =>
    BUTTON(
        {
            className: 'btn btn-3 label-and-icon',
            key: `Profile-${p.id}`,
            onClick: () =>
                getCurrentProjectId().map(id => navigateForm(id, p.name)),
        },
        SPAN('btn-label', p.name),
        SPAN('icon', nameToString('chevron-right'))
    );

// const profileName = (p: Profile) => DIV('list__item profile--name', p.name);

// const renderProfile = (p: Profile) =>
//     DIV(
//         {
//             className: 'profile__item',
//             key: `Profile-${p.id}`,
//             onClick: () =>
//                 getCurrentProjectId().map(id => navigateForm(id, p.name)),
//         },
//         profileName(p)
//     );

const renderProfileList = () =>
    DIV(
        'profile-list__wrapper',
        H2('', tr.angled('selectProfil')),
        description(),
        DIV('profile-list', sortedProfileList().map(renderProfile))
    );

export const render = () =>
    DIV(
        'form-select',
        // sidebar(),
        DIV(
            'content',
            DIV(
                'content--header',
                btnBackToSearch(() => navigateHome())
            ),
            DIV('content--body', renderProjectPreview(), renderProfileList())
        )
    );

export default render;

logger('Loaded');
