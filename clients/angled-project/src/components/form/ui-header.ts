import * as debug from 'debug';
import {
    DIV,
    SPAN,
    NODISPLAY,
    H2,
    NodeOrOptional,
} from 'sdi/components/elements';
import tr, { concat, formatDate, fromRecord } from 'sdi/locale';
import { getUserIdAstNumber, getLang } from 'sdi/app';
import { UnitWidget, getPanelOption } from 'angled-core/profile';
import { Unit, typeNameToName, InformationUnitName } from 'angled-core/ui';
import {
    getUnitbyNameFromCurrentProject,
    getModalStatus,
    getTagsForUnit,
    getTags,
    withDisplayType,
    getAudiencesForUnit,
} from 'angled-project/src/queries/project';
import { findUser } from 'angled-core/queries/ref';
import { unitDisplayName } from 'angled-core/queries/app';
import { renderModal } from 'sdi/components/modal';
import { nameToString } from 'sdi/components/button/names';
import {
    setModalStatus,
    addTagToUnit,
    removeTagFromUnit,
    setEditProjectHidden,
    selectUnit,
    removeAudienceFromUnit,
    addAudienceToUnit,
} from 'angled-project/src/events/project';
import { renderMultiSelect } from 'sdi/components/input';
import { ensureArray, setoidIdentified } from 'sdi/util';
import { Audience, Tag } from 'angled-core/ref';
import { makeLabel, makeIcon } from '../buttons';
import { ReactNode } from 'react';
import { renderSelectAudiences } from '../audience';
import { Tooltip } from 'sdi/components/tooltip';
import { parseWidgetFilter } from './display';
import { getDisplayAudience } from 'angled-core/queries/ui';
import { makeLabelAndIcon } from 'sdi/components/button';
import { fromPredicate } from 'fp-ts/lib/Option';

const logger = debug('sdi:unit/header');

const closeModal = makeLabel('validate', 2, () => tr.core('validate'));
const tagTooltip: Tooltip = {
    text: () => tr.angled('manageTags'),
    position: 'left',
};
const tagButton = makeIcon('tag', 3, 'tags', tagTooltip);
const audienceTooltip: Tooltip = {
    text: () => tr.angled('manageAudiences'),
    position: 'left',
}; // TODO check text and position
const audienceButton = makeIcon('view', 3, 'eye', audienceTooltip);

const getUnitName = (unitData: Unit | Unit[]) => {
    const typeName = Array.isArray(unitData)
        ? unitData.length > 0
            ? unitData[0].unit
            : null
        : unitData.unit;
    if (typeName === null) {
        return 'unknown';
    }
    return typeNameToName[typeName];
};

const tagModalKey = (u: Unit | Unit[]) => `tag-${getUnitName(u)}`;
const audienceModalKey = (u: Unit | Unit[]) => `audience-${getUnitName(u)}`;
const setTagModalStatus = (u: Unit | Unit[]) => setModalStatus(tagModalKey(u));
const setAudienceModalStatus = (u: Unit | Unit[]) =>
    setModalStatus(audienceModalKey(u));

const modal = (key: string) =>
    renderModal(getModalStatus(key), setModalStatus(key));

const getTitle = getPanelOption('title');

const computeTitle = (widget: UnitWidget) =>
    getTitle(widget).getOrElse(
        parseWidgetFilter(widget, 'filter').fold(
            unitDisplayName(widget.name),
            ({ term }) => fromRecord(term.name)
        )
    );

const uiName = (widget: UnitWidget, ...units: Unit[]) =>
    DIV(
        {
            className: `widget__name`,
            title: `unit identifiers: ${units.map(u => u.id).join(', ')}`,
        },
        computeTitle(widget)
    );

const renderTagItem = (t: Tag) => DIV('tag-item', fromRecord(t.label));

const tagSelect = (unitData: Unit | Unit[]) => {
    logger('tagSelect', unitData);
    return renderMultiSelect<Tag>(
        setoidIdentified<Tag>(),
        renderTagItem,
        addTagToUnit(unitData),
        removeTagFromUnit(unitData)
    );
};

const editTags = (unitData: Unit | Unit[]) =>
    tagButton(() => setTagModalStatus(unitData)('open'));

const renderTag = (t: Tag) =>
    SPAN(
        { className: 'tag-item', key: `tag-item-${t.id}-${getLang()}` },
        SPAN('tag-item--icon', nameToString('tag')),
        SPAN('tag-item--label', fromRecord(t.label))
    );

const tagModalTrigger = (unitData: Unit | Unit[]): ReactNode =>
    modal(tagModalKey(unitData))(
        editTags(unitData),
        H2({}, tr.angled('manageTags')),
        closeModal(() => setTagModalStatus(unitData)('close')),
        tagSelect(unitData)(getTags(), getTagsForUnit(unitData))
    );

const uiTags = (unitData: Unit | Unit[]) =>
    DIV('widget__tag-list', DIV({}, getTagsForUnit(unitData).map(renderTag)));

const editAudiences = (unitData: Unit | Unit[]) =>
    audienceButton(() => setAudienceModalStatus(unitData)('open'));

const audienceModalTrigger = (unitData: Unit | Unit[]): ReactNode =>
    modal(audienceModalKey(unitData))(
        editAudiences(unitData),
        H2({}, tr.angled('manageAudiences')),
        closeModal(() => setAudienceModalStatus(unitData)('close')),
        renderSelectAudiences(unitData)
    );

const isUserAuthor = (unitData: Unit | Unit[]) =>
    getUserIdAstNumber().fold([], userId =>
        (Array.isArray(unitData) ? unitData : [unitData]).filter(
            u => u.user === userId
        )
    ).length > 0;

const getSpanForTime = (timestamp: number) =>
    SPAN('widget__time', formatDate(new Date(timestamp)));

const uiTimeFormMulti = (unitData: Unit[]) => {
    const distinctDates = [
        ...new Set(
            unitData.map(u => new Date(new Date(u.created_at).toDateString()))
        ),
    ];
    if (distinctDates.length === 0) {
        return NODISPLAY();
    } else {
        if (distinctDates.length === 1) {
            return distinctDates.map((t, i) =>
                SPAN({ key: `date-${i}` }, getSpanForTime(t.valueOf()))
            );
        } else {
            const startDate = distinctDates[0];
            const endDate = distinctDates[distinctDates.length - 1];
            return SPAN(
                {},
                getSpanForTime(startDate.valueOf()),
                ' — ',
                getSpanForTime(endDate.valueOf())
            );
        }
    }
};

const uiTimeFormNotMulti = (unitData: Unit) =>
    getSpanForTime(unitData.created_at);

const uiTime = (unitData: Unit | Unit[]) =>
    Array.isArray(unitData)
        ? SPAN('multi-date', uiTimeFormMulti(unitData))
        : SPAN('single-date', uiTimeFormNotMulti(unitData));

const getSpanForAuthor = (authorId: number) =>
    SPAN(
        {
            key: `uiAuthFormMulti-${authorId}`,
            className: 'widget__author',
        },
        findUser(authorId).foldL(
            () => ' No author',
            u => ' ' + u.name
        )
    );

const uiAuthFormMulti = (unitData: Unit[]) => {
    const distinctUsers = [...new Set(unitData.map(u => u.user))];
    if (distinctUsers.length === 0) {
        return [NODISPLAY({ key: `uiAuthFormMulti-no` })];
    } else {
        if (distinctUsers.length === 1) {
            return distinctUsers.map(u => getSpanForAuthor(u));
        } else {
            return distinctUsers.map((u, i) => {
                if (i === distinctUsers.length - 1) {
                    return getSpanForAuthor(u);
                } else {
                    return [getSpanForAuthor(u), ','];
                }
            });
        }
    }
};

const uiAuthFormNotMulti = (unitData: Unit) => getSpanForAuthor(unitData.user);

const uiAuth = (unitData: Unit | Unit[]) =>
    Array.isArray(unitData)
        ? SPAN('multi-author', ...uiAuthFormMulti(unitData))
        : SPAN('single-author', uiAuthFormNotMulti(unitData));

const uiPubInfos = (unitData: Unit | Unit[]) =>
    DIV('widget__pub-info', uiAuth(unitData), SPAN({}, ' '), uiTime(unitData));

const makeArray = <T>(a: T | T[]) => (Array.isArray(a) ? a : [a]);

const editTooltip: Tooltip = {
    text: () => tr.angled('updateUnit'),
    position: 'left',
};

export const editButton = (name: InformationUnitName) =>
    makeIcon(
        'edit',
        3,
        'pencil-alt',
        editTooltip
    )(() => {
        selectUnit(name);
        setEditProjectHidden(false);
    });

const renderAudienceShort = (data: Unit | Unit[]) => (display: Audience) => {
    const unitAudiences = getAudiencesForUnit(data);
    const isInvisible = ensureArray(data).some(({ visible }) => !visible);
    const isOnDisplay =
        unitAudiences.findIndex(({ id }) => id === display.id) >= 0;

    const ifLastAudience = fromPredicate<Audience[]>(xs => xs.length === 1);

    if (isOnDisplay) {
        if (isInvisible) {
            return DIV(
                'on-display',
                DIV('no-edit-label', tr.angled('noEditAudienceManager'))
            );
        }
        const remove = makeLabelAndIcon('clear', 2, 'minus', () =>
            concat(
                // tr.angled('removeAudienceBtnLabel'),
                // ' ',
                fromRecord(display.name)
            )
        );
        return DIV(
            'on-display',
            ifLastAudience(unitAudiences)
                .map<NodeOrOptional>(() =>
                    DIV(
                        'warning',
                        SPAN('icon', nameToString('exclamation-triangle')),
                        tr.angled('warning/last-audience'),
                        remove(() => removeAudienceFromUnit(data)(display))
                    )
                )
                .getOrElse(remove(() => removeAudienceFromUnit(data)(display)))
        );
    } else {
        if (isInvisible) {
            return DIV(
                'bot-on-display',
                DIV('no-edit-label', tr.angled('noEditAudienceManager'))
            );
        }
        const add = makeLabelAndIcon('add', 2, 'plus', () =>
            concat(
                // tr.angled('addAudienceBtnLabel'),
                // ' ',
                fromRecord(display.name)
            )
        );
        return DIV(
            'bot-on-display',
            add(() => addAudienceToUnit(data)(display))
        );
    }
};

const renderTools = (widget: UnitWidget, data: Unit | Unit[]) =>
    getDisplayAudience().fold(
        DIV(
            'widget__tools',
            isUserAuthor(data) ? audienceModalTrigger(data) : NODISPLAY(),
            tagModalTrigger(data),
            editButton(widget.name)
        ),
        renderAudienceShort(data)
    );

const renderMeta = (widget: UnitWidget, data: Unit | Unit[]) =>
    withDisplayType(
        'writer',
        DIV(
            'widget__meta',
            DIV('widget__info', uiPubInfos(data), uiTags(data)),
            renderTools(widget, data)
        )
    );

export const uiHeader = (widget: UnitWidget) =>
    getUnitbyNameFromCurrentProject(widget.name).fold(
        DIV('widget__header', uiName(widget)),
        data =>
            DIV(
                'widget__header',
                DIV(
                    'widget__header--top',
                    renderMeta(widget, data),
                    uiName(widget, ...makeArray(data))
                )
            )
    );

logger('loaded');
