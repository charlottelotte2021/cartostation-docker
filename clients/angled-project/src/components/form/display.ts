import { DIV, NodeOrOptional, NODISPLAY } from 'sdi/components/elements';
import tr from 'sdi/locale';
import {
    getCurrentProfile,
    getCurrentPageNumber,
    getCurrentPage,
} from 'angled-project/src/queries/profile';
import {
    Profile,
    Panel,
    DirectedContainer,
    UnitWidget,
    Page,
    getPanelOption,
    WidgetFilter,
} from 'angled-core/profile';
import { ReactNode } from 'react';

import { editButton, uiHeader } from './ui-header';
import { formHeader } from './header';
import { footer } from './footer';
import { editor } from './edit';
import renderView from './view/index';
import { scopeOption } from 'sdi/lib';
import {
    getCurrentProjectFromTimeline,
    withDisplayType,
    unitNameIsInCurrentTransaction,
    getEditProjectHidden,
} from 'angled-project/src/queries/project';
import { getUnitbyName, Project } from 'angled-core/ui';
import { renderUnitWidget } from 'angled-core/ui/read';
import { unitDisplayName } from 'angled-core/queries/app';
import { findTerm } from 'angled-core/queries/universe';
import { none, some } from 'fp-ts/lib/Option';
import { ensureArray } from 'sdi/util';
import { getDisplayAudience } from 'angled-core/queries/ui';

export type DisplayType = 'reader' | 'writer';

const emptyUnit = (uw: UnitWidget) =>
    DIV(
        'container container--widget unit unit--empty unit--read',
        DIV('widget__name', unitDisplayName(uw.name)),
        withDisplayType(
            'writer',
            getDisplayAudience()
                .map<NodeOrOptional>(() => DIV(''))
                .alt(some(editButton(uw.name)))
        )
    );

const renderDirectedContainer = (project: Project, dc: DirectedContainer) =>
    DIV(
        `container container--${dc.direction}`,
        ...dc.children.map(displayPanel(project))
    );

const getHidden = getPanelOption('hidden');
const getClass = getPanelOption('class');
const filterSplit = new RegExp('[./=]');

export const parseWidgetFilter = (
    widget: UnitWidget,
    optionName: 'filter' | 'exclude'
) =>
    getPanelOption(optionName)(widget).chain<WidgetFilter>(filter => {
        if (typeof filter === 'string') {
            const parts = filter.split(filterSplit);
            if (
                parts.length === 2 &&
                Number.isInteger(parseInt(parts[1], 10))
            ) {
                const fieldName = parts[0];
                const val = parseInt(parts[1], 10);
                return findTerm(val).map(term => ({ fieldName, term }));
            }
        }
        return none;
    });

const txClass = (widget: UnitWidget) =>
    withDisplayType(
        'writer',
        unitNameIsInCurrentTransaction(widget.name) ? 'in-transaction' : ''
    ).getOrElse('');

const audienceClass = (project: Project, widget: UnitWidget) =>
    scopeOption()
        .let('units', getUnitbyName(widget.name, project).map(ensureArray))
        .let('audience', getDisplayAudience())
        .map<string>(({ audience, units }) =>
            units.reduce(
                (acc, unit) =>
                    acc ||
                    unit.readings.findIndex(r => r.audience === audience.id) >=
                        0,
                false
            )
                ? 'current-audience'
                : units.some(({ visible }) => !visible)
                ? 'not-editable-audience'
                : 'editable-audience'
        )
        .getOrElse('');

const renderUiWidget = (project: Project, widget: UnitWidget) => {
    if (getHidden(widget).isSome()) {
        return NODISPLAY();
    }
    const extraClass = getClass(widget).getOrElse('');
    const filter = parseWidgetFilter(widget, 'filter');
    const exclude = parseWidgetFilter(widget, 'exclude');
    const audience = audienceClass(project, widget);

    return renderUnitWidget(project, widget, filter, exclude).fold(
        emptyUnit(widget),
        elem =>
            DIV(
                `container container--widget ${
                    widget.name
                } ${extraClass} ${txClass(widget)} ${audience}`,

                uiHeader(widget),
                DIV('widget__body unit__wrapper', elem)
            )
    );
};

const displayPanel =
    (project: Project) =>
    (panel: Panel): ReactNode => {
        // a panel is a DirectedContainer or an UnitWidget
        switch (panel.type) {
            case 'DirectedContainer':
                return renderDirectedContainer(project, panel);

            case 'UnitWidget':
                return renderUiWidget(project, panel);

            case 'ProjectView':
                return renderView(panel.name, panel.options, project);
        }
    };

const withProfile = () =>
    DIV(
        'form',
        formHeader(),
        DIV(
            'panel',
            DIV(
                'splash',
                DIV(
                    'splash-content',
                    DIV('loader-anim', tr.angled('loadingData'))
                )
                // footer(profile, getCurrentPageNumber())
            )
        )
    );

const withProject = (profile: Profile, page: Page, project: Project) =>
    DIV(
        'form',
        formHeader(),
        getEditProjectHidden()
            ? DIV('panel', ...page.boxes.map(displayPanel(project)))
            : DIV('panel', editor(project)),
        footer(profile, getCurrentPageNumber())
    );

export const render =
    // render the display form for a given UI (given by layout)
    () =>
        scopeOption()
            .let('profile', getCurrentProfile())
            .let('page', getCurrentPage())
            .fold(
                DIV('error', tr.angled('errorDisplayProject')),
                ({ profile, page }) =>
                    getCurrentProjectFromTimeline().fold(
                        withProfile(),
                        project => withProject(profile, page, project)
                    )
            );

export default render;
