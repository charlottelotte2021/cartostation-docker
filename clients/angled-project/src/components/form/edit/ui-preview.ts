import { Ord, ordString } from 'fp-ts/lib/Ord';
import { sort } from 'fp-ts/lib/Array';

import { DIV, H2, H3, NODISPLAY } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { UnitData, typeNameToName, lookupNamedField } from 'angled-core/ui';
import { renderThumbnail } from 'angled-core/ui/write/image';
import { renderUnitData } from 'angled-core/ui/read';
import { unitDisplayName } from 'angled-core/queries/app';

import {
    makeLabelAndIcon,
    makeRemoveLabelAndIcon,
} from 'angled-project/src/components/buttons';
import { RemovedUnitIdentifier } from 'angled-project/src/shape/project';
import { resetStage } from 'angled-project/src/events/project';
import { openSaveFinalStep } from 'angled-project/src/events/modal';
import {
    getStagedUnits,
    hasDataStaged,
    hasRemovedFromMulti,
    getRemovedFromMulti,
    hasRemovedDataStaged,
} from 'angled-project/src/queries/project';

const btnCancel = makeRemoveLabelAndIcon(
    'cancel',
    2,
    'times',
    () => tr.angled('clearUpdate'),
    () => tr.angled('confirmClearUpdate')
);
const pushButton = makeLabelAndIcon('save', 1, 'check', () =>
    tr.angled('saveAndPublish')
);

const noop = () => void 0;

const buttonActiveClass = () =>
    hasRemovedFromMulti() || hasDataStaged() ? '' : 'inactive';

const buttonActiveAction = () =>
    hasRemovedFromMulti() || hasDataStaged() ? openSaveFinalStep : noop;

const renderCommitBtn = () =>
    DIV(
        { className: 'editor__ui-preview__commit' },
        pushButton(buttonActiveAction(), buttonActiveClass()),
        btnCancel(resetStage)
    );

const renderStagedUnit = (u: UnitData, index: number) => {
    const name = typeNameToName[u.unit];
    const fields = lookupNamedField(name).fold([], info =>
        info.fields.map(f => f[1])
    );
    if (name === 'image') {
        return DIV(
            {
                className: 'editor__ui-preview__item',
                key: `StagedUnit-${name}-${index}`,
            },
            renderThumbnail(),
            renderUnitData(name, fields, u)
        );
    } else {
        return DIV(
            {
                className: 'editor__ui-preview__item',
                key: `StagedUnit-${name}-${index}`,
            },
            DIV({ className: 'unit--name' }, H3({}, unitDisplayName(name))),
            renderUnitData(name, fields, u)
        );
    }
};

const renderRemovedUnit = (r: RemovedUnitIdentifier, index: number) => {
    const name = typeNameToName[r[0]];
    return hasRemovedDataStaged()
        ? DIV(
              {
                  className: 'editor__ui-preview__item',
                  key: `StagedUnit-${name}-${index}`,
              },
              DIV({ className: 'unit--name' }, H3({}, unitDisplayName(name))),
              tr.angled('removedUnits')
          )
        : NODISPLAY;
};

// order on the display name of the U
// fixme todo : is it the good place for this code ? not sure
const ordUIDisplayName: Ord<UnitData> = {
    equals: (x, y) =>
        ordString.equals(
            unitDisplayName(typeNameToName[x.unit]),
            unitDisplayName(typeNameToName[y.unit])
        ),
    compare: (x, y) =>
        ordString.compare(
            unitDisplayName(typeNameToName[x.unit]),
            unitDisplayName(typeNameToName[y.unit])
        ),
};

export const editUiPreview = () =>
    DIV(
        { className: 'editor__ui-preview' },
        H2({ className: 'head-title' }, tr.angled('commitPreview')),
        sort(ordUIDisplayName)(getStagedUnits().slice()).map(renderStagedUnit),
        getRemovedFromMulti().map(renderRemovedUnit),
        renderCommitBtn()
    );
