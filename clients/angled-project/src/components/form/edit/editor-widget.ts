import * as debug from 'debug';

import { DIV, NODISPLAY, H2, SPAN, A } from 'sdi/components/elements';
import {
    InformationUnitName,
    lookupNamedField,
    getUnitbyName,
    Unit,
} from 'angled-core/ui';
import { getCurrentWidget } from 'angled-project/src/queries/profile';
import { renderUnitWidget } from 'angled-core/ui/write';
import { renderUnitData } from 'angled-core/ui/read';
import { makeLabel, makeLabelAndIcon } from '../../buttons';
import tr from 'sdi/locale';
import {
    stageCurrentUnit,
    updateStagedCurrentUnit,
    removeUnitFromMulti,
    restoreUnitFromMulti,
} from 'angled-project/src/events/project';
import {
    getNamedDataUnits,
    isRemovedFromMulti,
    hasRemovedFromMulti,
    getCurrentProjectLastVersion,
} from 'angled-project/src/queries/project';
import { scopeOption } from 'sdi/lib';
import { fromNullable, some, none } from 'fp-ts/lib/Option';
import { nameToString } from 'sdi/components/button/names';
import { getUnitInEdition } from 'angled-core/queries/ui';
import { setUnitInEdition } from 'angled-core/events/ui';
import { UnitDataIO } from 'angled-core/ui/ui';

const logger = debug('sdi:app/angled/components/edit');

const stageButton = makeLabelAndIcon('save', 1, 'chevron-right', () =>
    tr.angled('validateUpdate')
);
const editButton = makeLabel('edit', 2, () => tr.core('edit'));

const linkToUIInfos = (unitName: InformationUnitName) =>
    DIV(
        'helptext',
        SPAN(
            {},
            tr.angled('getUiInfo'),
            A(
                {
                    href: `https://gitlab.com/atelier-cartographique/perspective/shared/plate-forme-veille-projets-urbains/wikis/lexicon/information_unit/${unitName}`,
                    target: '_blank',
                },
                tr.angled('getUiInfoLink')
            )
        )
    );

const noop = () => void 0;

const isUnitInEditionValid = () =>
    UnitDataIO.decode(getUnitInEdition()).fold(
        () => false,
        () => true
    );

const buttonActiveClass = () =>
    isUnitInEditionValid() || hasRemovedFromMulti() ? '' : 'inactive';

const stagedButtonActiveAction = () =>
    isUnitInEditionValid() || hasRemovedFromMulti()
        ? stageCurrentUnit()
        : noop();

const updateStagedButtonActiveAction = () =>
    isUnitInEditionValid() || hasRemovedFromMulti()
        ? updateStagedCurrentUnit()
        : noop();

const withFieldNames = <T>(
    unitName: InformationUnitName,
    dflt: T,
    f: (ns: string[]) => T
) =>
    lookupNamedField(unitName).fold(dflt, ({ fields }) =>
        f(fields.map(f => f[1]))
    );

const renderStaged = (unitName: InformationUnitName) =>
    withFieldNames(unitName, NODISPLAY(), fields =>
        DIV(
            'multi-unit multi-unit--staged',
            renderUnitData(unitName, fields, ...getNamedDataUnits(unitName))
        )
    );

const renderMultiItem = (
    unitName: InformationUnitName,
    unit: Unit,
    fields: string[]
) => {
    if (unit.visible === false) {
        return NODISPLAY();
    }

    if (isRemovedFromMulti(unit)) {
        return DIV(
            'multi-item multi-item--removed',
            DIV(
                {
                    className:
                        'multi-item__action btn btn-reset btn-3 icon-only',
                    onClick: () => restoreUnitFromMulti(unit),
                },
                DIV('icon', nameToString('undo'))
            ),
            renderUnitData(unitName, fields, unit)
        );
    }

    return DIV(
        'multi-item',
        DIV(
            {
                className: 'multi-item__action btn btn-reset btn-3 icon-only',
                onClick: () => removeUnitFromMulti(unit),
            },
            DIV('icon', nameToString('times'))
        ),
        renderUnitData(unitName, fields, unit)
    );
};

const wrapMultiUnit = (unitName: InformationUnitName) => (unit: Unit) =>
    withFieldNames(unitName, NODISPLAY(), fields =>
        renderMultiItem(unitName, unit, fields)
    );

const renderMulti = (unitName: InformationUnitName) =>
    getCurrentProjectLastVersion().map(p =>
        getUnitbyName(unitName, p)
            .chain(u => (Array.isArray(u) ? some(u) : none))
            .map(us =>
                DIV(
                    'multi-item--current',
                    H2({}, tr.angled('adjustCurrentInfo')),
                    DIV(
                        'multi-item--collection',
                        ...us.map(wrapMultiUnit(unitName))
                    ),
                    H2({}, tr.angled('addNewInfoToCurrentInfo'))
                )
            )
    );

export const editorWidget = (unitName: InformationUnitName) =>
    scopeOption()
        .let('info', lookupNamedField(unitName))
        .let('widget', getCurrentWidget())
        .let('project', getCurrentProjectLastVersion())
        .fold(
            DIV(`editor__widget editor__widget--${unitName}`),
            ({ info, widget, project }) => {
                const stagedUnits = getNamedDataUnits(unitName);
                if (!info.multi && stagedUnits.length > 0) {
                    // si unitName se trouve dans staged
                    // do not render a form as we've got a staged unit for this
                    return fromNullable(getUnitInEdition()).fold(
                        // display the new value (no edition) (after pre-validation)
                        DIV(
                            `editor__widget editor__widget--${unitName} creation`,

                            H2('head-title', tr.angled('commitTitle')),
                            renderStaged(widget.name),
                            editButton(() => {
                                setUnitInEdition(stagedUnits[0]);
                            })
                        ),
                        // edition via form
                        _fd =>
                            DIV(
                                `editor__widget editor__widget--${unitName} edition`,

                                H2('head-title', tr.angled('commitTitle')),
                                renderUnitWidget(widget, some(project)), // form
                                DIV(
                                    'stage-wrapper',
                                    stageButton(
                                        updateStagedButtonActiveAction,
                                        buttonActiveClass()
                                    )
                                ),
                                linkToUIInfos(unitName)
                            )
                    );
                } // multiform
                return DIV(
                    `editor__widget editor__widget--${unitName} edition-multi`,

                    H2('head-title', tr.angled('commitTitle')),
                    renderMulti(unitName),
                    renderStaged(unitName),
                    renderUnitWidget(widget, some(project)),
                    DIV(
                        'stage-wrapper',
                        stageButton(
                            () => stagedButtonActiveAction(),
                            buttonActiveClass()
                        )
                    ),
                    linkToUIInfos(unitName)
                );
            }
        );

logger('Loaded');
