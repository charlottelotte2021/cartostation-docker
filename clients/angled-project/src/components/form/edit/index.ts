import { DIV, H2 } from 'sdi/components/elements';

import { editorHeader } from './editor-header';
import { editUiList } from './ui-list';
import { editorWidget } from './editor-widget';
import { editUiSingleView } from './ui-single';
import { editUiPreview } from './ui-preview';

import { getSelectedUnit, getStagedUnitNames } from '../../../queries/project';
import { InformationUnitName, Project } from 'angled-core/ui';
import tr from 'sdi/locale';
import { makeLabelAndIcon } from '../../buttons';
import { resetStage } from 'angled-project/src/events/project';

// const setHiddenClass =
//     () => getEditProjectHidden() ? 'hidden' : '';

const btnCancel = makeLabelAndIcon('cancel', 2, 'times', () =>
    tr.core('cancel')
);

const renderWitouthUnit = () =>
    DIV(
        { className: 'editor__body' },
        DIV(
            { className: 'widget-wrapper' },
            H2({ className: 'head-title' }, tr.angled('updateData')),
            DIV(
                { className: 'widget-wrapper-sub' },
                editUiList(getStagedUnitNames()),
                DIV(
                    { className: 'editor__txt-info-select' },
                    H2({}, tr.angled('selectUnit')),
                    btnCancel(resetStage)
                )
            )
        )
    );

const renderWithUnit = (unit: InformationUnitName) =>
    DIV(
        { className: 'editor__body' },
        DIV(
            { className: 'widget-wrapper' },
            H2({ className: 'head-title' }, tr.angled('updateData')),
            DIV(
                { className: 'widget-wrapper-sub' },
                editUiList(getStagedUnitNames()),
                editUiSingleView(unit),
                editorWidget(unit)
            )
        ),

        editUiPreview()
    );

const editorBody = () =>
    getSelectedUnit().foldL(renderWitouthUnit, renderWithUnit);

export const editor = (pj: Project) =>
    DIV(
        { className: `editor__background ` },
        DIV({ className: 'editor' }, editorHeader(pj), editorBody())
    );
