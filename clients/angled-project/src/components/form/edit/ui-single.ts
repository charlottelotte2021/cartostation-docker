import { DIV, H2 } from 'sdi/components/elements';
import tr, { formatDate } from 'sdi/locale';
import { InformationUnitName, Project } from 'angled-core/ui';
import { scopeOption } from 'sdi/lib';
import { getCurrentWidget } from 'angled-project/src/queries/profile';
import {
    getCurrentProjectLastVersion,
    getCurrentTimelineTime,
    getTimelineUnitDateStart,
} from 'angled-project/src/queries/project';
import { UnitWidget } from 'angled-core/profile';
import { renderUnitWidget } from 'angled-core/ui/read';
import { uiHeader } from '../ui-header';
import { none } from 'fp-ts/lib/Option';

const versionTitle = () => {
    const time = getCurrentTimelineTime().fold(0, t => t);
    const timeStart = getTimelineUnitDateStart().fold(0, t => Math.ceil(t));
    return time === 0 || time === timeStart
        ? tr.angled('currentInfo')
        : tr.angled('pastInfo') + formatDate(new Date(time));
};

const emptyUnit = () =>
    DIV(
        {
            className:
                'container container--widget unit unit--empty unit--read',
        },
        tr.angled('emptyUnit')
    );

const renderUiWidget = (project: Project, uw: UnitWidget) =>
    renderUnitWidget(project, uw, none, none).fold(emptyUnit(), elem =>
        DIV(
            { className: `container container--widget ${uw.name}` },
            uiHeader(uw),
            DIV({ className: 'widget__body unit__wrapper' }, elem)
        )
    );

export const editUiSingleView = (_unit: InformationUnitName) =>
    DIV(
        { className: 'editor__ui-singleview' },
        H2({ className: 'head-title' }, versionTitle()),
        scopeOption()
            .let('widget', getCurrentWidget())
            .let('project', getCurrentProjectLastVersion())
            .map(({ widget, project }) => renderUiWidget(project, widget))
    );
