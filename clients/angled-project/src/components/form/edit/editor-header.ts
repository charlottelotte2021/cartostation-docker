import { DIV } from 'sdi/components/elements';

import { Project } from 'angled-core/ui';
import { breadcrumbProjectEdit } from '../../breadcrumb';
import { uiTimeline } from './ui-timeline';
const appNameWrapper = () => DIV('app-name__wrapper');

const headerContent = (pj: Project) =>
    DIV(
        'header__content',
        DIV(
            'breadcrumb__wrapper',
            breadcrumbProjectEdit(pj),
            DIV('header__tools')
        ),
        uiTimeline()
    );

export const editorHeader = (pj: Project) =>
    DIV('editor__header', appNameWrapper(), headerContent(pj));
