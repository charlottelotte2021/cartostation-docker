import { DIV, H1 } from 'sdi/components/elements';
import tr from 'sdi/locale';
import {
    getSavingError,
    getCurrentProjectId,
} from 'angled-project/src/queries/project';
import { makeLabel } from 'sdi/components/button';
import { clearProjectForm } from 'angled-project/src/events/project';
import { setLayout } from 'angled-project/src/events/app';
import { markdown } from 'sdi/ports/marked';

export const renderSaving = () =>
    getSavingError().fold(
        DIV(
            { className: 'splash-content' },
            DIV({ className: 'loader-anim' }, tr.core('saving'))
        ),
        renderSavingError
    );

const navigateFormButton = makeLabel('navigate', 2, () =>
    tr.angled('backToProject')
);

const navigateFormAction = () => {
    getCurrentProjectId().map(clearProjectForm);
    setLayout('Form');
};

export const renderSavingError = (_err: string) =>
    DIV(
        {
            className: 'splash-content saving saving-error',
        },

        H1({}, 'error'),
        DIV({ className: 'info' }, markdown(tr.angled('savingError'))),

        navigateFormButton(navigateFormAction)
    );

export default renderSaving;
