import { DIV, SPAN } from 'sdi/components/elements';
import { makeLabelAndIcon } from '../buttons';
import tr, { concat, fromRecord } from 'sdi/locale';
import { isUserLogged, getRoot } from 'sdi/app/queries';
import { getPathElements, setoidIdentified } from 'sdi/util';

import { timeline } from './timeline';
import { Profile } from 'angled-core/profile';
import { navigateHome, navigateForm } from 'angled-project/src/events/route';

import {
    getEditProjectHidden,
    getCurrentProjectId,
    getDisplayType,
    withDisplayType,
} from '../../queries/project';
import { setEditProjectHidden, setDisplayType } from '../../events/project';

import { renderSelect } from 'sdi/components/input';
import {
    getProfiles,
    getCurrentProfile,
} from 'angled-project/src/queries/profile';
import { renderSubscription } from 'angled-project/src/components/sub';
// import { makeIcon } from 'sdi/components/input';
import { none } from 'fp-ts/lib/Option';
import { Audience } from 'angled-core/ref';
import { renderAudienceDisplayWidget } from 'angled-core/components/audience-display';
import { addAudienceToProfilePage } from 'angled-project/src/events/project';

const btnTriggerEdit = makeLabelAndIcon('edit', 1, 'pencil-alt', () =>
    tr.angled('updateData')
);
const btnBackToSearch = makeLabelAndIcon('search', 2, 'chevron-left', () =>
    tr.angled('backToSearch')
);

const btnSwitchReader = makeLabelAndIcon('switch', 2, 'eye', () =>
    tr.angled('readMode')
);
const btnSwitchWriter = makeLabelAndIcon('switch', 1, 'pencil-alt', () =>
    tr.angled('writeMode')
);
const btnSwitchMode = () =>
    getDisplayType() === 'reader'
        ? btnSwitchWriter(() => setDisplayType('writer'))
        : btnSwitchReader(() => setDisplayType('reader'));

const triggerEdit = () => {
    if (isUserLogged()) {
        getEditProjectHidden()
            ? setEditProjectHidden(false)
            : setEditProjectHidden(true);
        // setCurrentProjectAsTheLatest();
        // setCurrentTimelineUnit(null);
        // setCurrentTimelineId(null);
    } else {
        const path = getPathElements(document.location.pathname);
        const root = getPathElements(getRoot());
        const next = path
            .filter((p, i) => (i < root.length ? p !== root[i] : true))
            .join('/');
        const loginUrl = `${getRoot()}login/${next}`;
        window.location.assign(loginUrl);
    }
};

// const renderProfileItem = (profile: Profile) =>
//     DIV(
//         {
//             onClick: () =>
//                 getCurrentProjectId().map(id => navigateForm(id, profile.name))
//         },
//         profile.name
//     );

const renderSelectProfile = renderSelect<Profile>(
    'select-profile',
    profile => profile.name,
    profile => getCurrentProjectId().map(id => navigateForm(id, profile.name)),
    setoidIdentified()
);

const selectProfile = () =>
    DIV(
        'select-profile',
        renderSelectProfile(getProfiles(), getCurrentProfile())
    );

const appNameWrapper = () => DIV('app-name__wrapper');

const isActiveTriggerEdit = () => (isUserLogged() ? '' : 'inactive');

const applyAudienceToAllUnits = (audience: Audience) =>
    DIV(
        'other-audience--tool',
        SPAN('', tr.angled('addAudienceToAllBtnLabel')),
        makeLabelAndIcon('select', 2, 'check', () =>
            concat('Audience : ', fromRecord(audience.name))
        )(() => addAudienceToProfilePage(audience))
    );

// const applyAudienceToAllUnits = (audience: Audience) =>
//     makeLabel('select', 2, () =>
//         concat(
//             tr.angled('addAudienceToAllBtnLabel'),
//             ': ',
//             fromRecord(audience.name)
//         )
//     )(() => addAudienceToProfilePage(audience));

const renderTools = () =>
    getEditProjectHidden()
        ? withDisplayType(
              'writer',
              DIV(
                  'tools',
                  timeline(),
                  renderAudienceDisplayWidget(applyAudienceToAllUnits)
              )
          )
        : none;

const headerContent = () =>
    DIV(
        'header__content',
        DIV(
            'header__content-top',
            DIV(
                'header__actions',
                btnBackToSearch(() => navigateHome()),
                btnSwitchMode(),
                withDisplayType(
                    'writer',
                    btnTriggerEdit(() => triggerEdit(), isActiveTriggerEdit())
                ),
                renderSubscription()
            ),
            DIV('header__tools', selectProfile())
        ),
        renderTools()
    );

export const formHeader = () =>
    DIV(
        'project-header',
        // fixme todo remplacer number par integer
        appNameWrapper(),
        headerContent()
    );
