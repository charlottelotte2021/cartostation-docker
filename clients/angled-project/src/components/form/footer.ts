import { DIV, SPAN } from 'sdi/components/elements';
import { makeLabelAndIcon } from '../buttons';
import tr from 'sdi/locale';
import { Profile } from 'angled-core/profile';

import { fromPredicate } from 'fp-ts/lib/Option';
import { getCurrentProjectId } from 'angled-project/src/queries/project';
import { prevPannel, nextPannel } from 'angled-project/src/events/profile';

const projectID = () =>
    getCurrentProjectId().map(id =>
        SPAN({}, `${tr.angled('projectID')} : ${id} `)
    );

const previousPageButton = (_p: Profile, pn: number) => {
    const optPn = fromPredicate((n: number) => n > 0)(pn);
    const opButton = optPn.map(_pn =>
        makeLabelAndIcon('navigate', 3, 'chevron-left', () =>
            tr.angled('panelPrev')
        )(() => prevPannel())
    ); // todo fixme l'url n'est pas modifiée
    return opButton.getOrElse(DIV({}));
};

const nextPageButton = (p: Profile, pn: number) => {
    const optPn = fromPredicate((n: number) => n + 1 < p.layout.length)(pn);
    const opButton = optPn.map(_pn =>
        makeLabelAndIcon('navigate', 3, 'chevron-right', () =>
            tr.angled('panelNext')
        )(() => nextPannel())
    ); // todo fixme l'url n'est pas modifiée
    return opButton.getOrElse(DIV({}));
};

const pannelNumberForHuman = (pn: number) => pn + 1;

const navigationBar = (p: Profile, pn: number) =>
    SPAN(
        'navigation',
        previousPageButton(p, pn),
        SPAN('panel-count', pannelNumberForHuman(pn) + '/' + p.layout.length),
        nextPageButton(p, pn)
    );

export const footer = (p: Profile, pn: number) =>
    DIV('footer__content', projectID(), navigationBar(p, pn));
