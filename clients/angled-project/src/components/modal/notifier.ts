import { makeLabel } from '../buttons';
import { tr, fromRecord, formatDate } from 'sdi/locale';
import { DETAILS, DIV, H2, H3, SPAN, SUMMARY } from 'sdi/components/elements';

import {
    DisplayProjectNotification,
    getDisplayProjectNotif,
} from 'angled-core/queries/sub';
import { navigateFormNaked } from 'angled-project/src/events/route';
import { findProject } from 'angled-project/src/queries/project';
import { isNotNullNorUndefined } from 'sdi/util';
import { nameToString } from 'sdi/components/button/names';
import { unitDisplayName } from 'angled-core/queries/app';
import { makeLabelAndIcon } from 'angled-core/components/buttons';
import { markdown } from 'sdi/ports/marked';

const closeModalButton = makeLabel('close', 2, () => tr.core('close'));

const header = () => H2({}, tr.angled('notifications'));

const footer = (close: () => void) => DIV({}, closeModalButton(close));

const buttonViewProject = makeLabelAndIcon('view', 3, 'chevron-right', () =>
    tr.angled('viewProject')
);

const renderProjectNotification = (
    close: () => void,
    pid: number,
    dpn: DisplayProjectNotification
) =>
    findProject(pid).map(p => {
        const name = p.name;
        const projectName = isNotNullNorUndefined(name)
            ? fromRecord(name.name)
            : `Project #${pid}`;

        const actions = dpn.actions.map(a => a.tag);

        // const viewTooltip: Tooltip = {
        //     text: () => tr.angled('viewProject'),
        //     position: 'top-right',
        // };
        // const deleteNotif = (a: string[]) =>
        //     (a.indexOf('delete') > -1)
        //         ? SPAN('fa', nameToString('times'))
        //         : SPAN('fa disabled', nameToString('times'))

        const tagNotif = (a: string[]) =>
            a.indexOf('tag') > -1
                ? SPAN('icon fa', nameToString('tags'))
                : SPAN('icon fa disabled', nameToString('tags'));

        const editNotif = (a: string[]) =>
            a.indexOf('create') > -1
                ? SPAN('icon fa', nameToString('pencil-alt'))
                : SPAN('icon fa disabled', nameToString('pencil-alt'));

        const audienceNotif = (a: string[]) =>
            a.indexOf('audience') > -1
                ? SPAN('icon fa', nameToString('eye'))
                : SPAN('icon fa disabled', nameToString('eye'));

        const actionName = (s: string) => {
            switch (s) {
                case 'create':
                    return SPAN('icon fa', nameToString('pencil-alt'));
                case 'audience':
                    return SPAN('icon fa', nameToString('eye'));
                case 'tag':
                    return SPAN('icon fa', nameToString('tags'));
            }
            return s;
        };

        return DETAILS(
            { key: pid },
            DIV(
                'notif__details',
                H3('', 'Details des mises à jour'),
                dpn.actions.map(a =>
                    a.notifications.map(b =>
                        DIV(
                            'notification--project',
                            actionName(b.action),
                            SPAN(
                                'date',
                                formatDate(new Date(Date.parse(b.created_at)))
                            ),
                            SPAN('label', unitDisplayName(b.unit))
                        )
                    )
                )
            ),

            SUMMARY(
                'notif__summary',
                SPAN(
                    'notif-project--legend',
                    editNotif(actions),
                    audienceNotif(actions),
                    tagNotif(actions)
                ),
                SPAN(
                    'notif-project--label',
                    SPAN('icon fa', nameToString('info')),
                    SPAN('', projectName)
                ),
                buttonViewProject(() => {
                    navigateFormNaked(dpn.project);
                    close();
                })
            )
        );
    });


const helpText = () => {
    return DETAILS(
        'helptext',
        SUMMARY(
            '',
            SPAN(
                'fa',
                nameToString('book'),
                SPAN('label', tr.core('documentation'))
            )
        ),
        markdown(tr.angled('notifHelptext')),
        DIV(
            'notif__legend',
            H3('', tr.angled('signLegend')),
            DIV(
                '',
                SPAN('icon fa', nameToString('pencil-alt')),
                SPAN('label', tr.angled('editHelptext'))
            ),
            DIV(
                '',
                SPAN('icon fa', nameToString('eye')),
                SPAN('label', tr.angled('audienceHelptext'))
            ),
            DIV(
                '',
                SPAN('icon fa', nameToString('tags')),
                SPAN('label', tr.angled('tagHelptext'))
            )
        )
    );
};

const renderNotificationProject =
    (close: () => void) => (pn: DisplayProjectNotification) => {
        return DIV(
            `notif ${pn.read}`,
            renderProjectNotification(close, pn.project, pn)
        );
    };

const body = (close: () => void) => {
    return DIV(
        'notif__wrapper',
        helpText(),
        getDisplayProjectNotif().map(renderNotificationProject(close))
    );
};

export const render = { header, footer, body };
