import { makeLabel } from '../buttons';
import tr from 'sdi/locale';
import { H2, DIV } from 'sdi/components/elements';
import { pushStaged } from 'angled-project/src/events/project';
import { stringToParagraphs } from 'sdi/util';
import { renderSelectAudiencesPre } from '../audience/index';
import { activityLogger } from 'angled-project/src/events/app';
import { saveUnitsAction } from 'sdi/activity';
import {
    getCurrentProjectId,
    getStagedUnits,
} from 'angled-project/src/queries/project';

const closeModalButton = makeLabel('close', 2, () => tr.core('close'));
const confirmButton = makeLabel('confirm', 1, () => tr.core('confirm'));

const header = () => H2({}, tr.angled('setAudience'));

const footer = (close: () => void) =>
    DIV(
        'modal__footer__inner',
        confirmButton(() => {
            close();
            getCurrentProjectId().map(p =>
                activityLogger(saveUnitsAction(p, getStagedUnits().length))
            );
            pushStaged();
        }),
        closeModalButton(close)
    );

const body = () => [
    DIV('helptext', stringToParagraphs(tr.angled('setAudienceInfo'))),
    renderSelectAudiencesPre(),
];

export const render = { header, footer, body };
