
import { getDisplayUnreadnotification, getNotificationsRemote } from 'angled-core/queries/sub';
import { DIV, NodeOrOptional, SPAN } from 'sdi/components/elements';
import { nameToString } from 'sdi/components/button/names';
import { openNotifier } from 'angled-project/src/events/modal';
import tr from 'sdi/locale';
import { foldRemote } from 'sdi/source';
import { Notif } from 'angled-core/sub';

const initNotifications = () => DIV('', 'init')

const loadNotifications = () => DIV('loader-spinner')

const errorNotifications = () => DIV('', 'error loading notifications')

const notificationsLoaded = () => {
    return DIV(
        { className: 'notifier', onClick: openNotifier },
        SPAN('notifier__unread', nameToString('circle')),
        SPAN(
            'notifier__label',
            tr.angled('notifications')))
}

const notificationsUnread = (r: number) => {
    return DIV(
        { className: 'notifier', onClick: openNotifier },
        SPAN('notifier__unread', nameToString('circle')),
        SPAN(
            'notifier__label',
            tr.angled('notifications'), `(${r})`))
}

const remoteProcessor = foldRemote<Notif[], string, NodeOrOptional>(
    initNotifications,
    loadNotifications,
    errorNotifications,
    notificationsLoaded,
)

const renderNotifications = () => remoteProcessor(getNotificationsRemote())

export const renderNotifier = () => {
    const unread = getDisplayUnreadnotification().length;
    if (unread > 0) {
        return renderNotifications(), notificationsUnread(unread);
    }
    return renderNotifications();
};

export default renderNotifier;
