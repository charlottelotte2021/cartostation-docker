import { DIV } from 'sdi/components/elements';
import {
    getSubscriptionForProject,
    getSubscriptions,
} from 'angled-core/queries/sub';
import { getCurrentProjectId } from 'angled-project/src/queries/project';
import { ProjectSubscription } from 'angled-core/sub';
import {
    subscribeProject,
    unsubscribeProject,
    reSubscribeProject,
} from 'angled-core/events/sub';
// import { divTooltipBottom } from 'sdi/components/tooltip';
import { tr } from 'sdi/locale';
import { makeLabelAndIcon } from 'sdi/components/button';
import { divTooltipBottom } from 'sdi/components/tooltip';

const btnSubscribe = makeLabelAndIcon('switch', 3, 'toggle-off', () =>
    tr.angled('notificationOff')
);

const btnUnSubscribe = makeLabelAndIcon('switch', 3, 'toggle-on', () =>
    tr.angled('notificationOn')
);

const renderSubscribe = () =>
    divTooltipBottom(
        tr.angled('unsubscribed'),
        {},
        btnSubscribe(() => getCurrentProjectId().map(subscribeProject))
    );

const renderUnsubscribe = (s: ProjectSubscription) => {
    switch (s.active) {
        case true:
            return divTooltipBottom(
                tr.angled('subscribed'),
                {},
                btnUnSubscribe(() => unsubscribeProject(s.project))
            );

        case false:
            return divTooltipBottom(
                tr.angled('unsubscribed'),
                {},
                btnSubscribe(() => reSubscribeProject(s.project))
            );
    }
};

// const renderSubscribe = () =>

//     divTooltipBottom(
//         tr.angled('tooltipSubscribe'),
//         {},
//         DIV(
//             {
//                 className: 'subscription subscription--off',
//                 onClick: () => getCurrentProjectId().map(subscribeProject),
//             },
//             tr.angled('subscribe'),
//         ),
//     );

// const renderUnsubscribe = (s: ProjectSubscription) => {
//     switch (s.active) {
//         case true:
//             return divTooltipBottom(
//                 tr.angled('tooltipUnSubscribe'),
//                 {},
//                 DIV(
//                     {
//                         className: 'subscription subscription--on',
//                         onClick: () => unsubscribeProject(s.project),
//                     },
//                     // 'UnSUB#' + s.id,
//                     tr.angled('unsubscribe'),
//                 ),
//             );

//         case false:
//             return divTooltipBottom(
//                 tr.angled('tooltipSubscribe'),
//                 {},
//                 DIV(
//                     {
//                         className: 'subscription subscription--off',
//                         onClick: () => reSubscribeProject(s.project),
//                     },
//                     tr.angled('subscribe'),
//                     // 'ReSUB#' + s.id,
//                 ),
//             );

//     }
// };

export const renderSubscription = () =>
    getSubscriptions().map(() =>
        DIV(
            { className: 'subscription__wrapper' },
            getCurrentProjectId()
                .chain(getSubscriptionForProject)
                .foldL(renderSubscribe, renderUnsubscribe)
        )
    );
