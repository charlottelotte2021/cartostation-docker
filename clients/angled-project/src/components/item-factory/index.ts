// import { DIV } from 'sdi/components/elements';
// import { Translated } from 'sdi/locale';

// // Radiobutton and checkbox should come here (in fact, it should be generalized to all cartostation)
// // The following code is from client/solar

// export const inputSelect =
//     <T>(get: () => T, set: (v: T) => void) =>
//         (label: () => Translated, v: T) => {
//             if (get() === v) {
//                 return DIV({ className: 'wrapper-checkbox' },
//                     DIV({ className: 'checkbox active' }),
//                     DIV({ className: 'checkbox-label' }, label));
//             }
//             return DIV({
//                 className: 'wrapper-checkbox',
//                 onClick: () => set(v),
//             }, DIV({ className: 'checkbox' }),
//                 DIV({ className: 'checkbox-label' }, label));
//         };
