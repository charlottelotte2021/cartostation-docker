/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { query } from 'sdi/shape';
import { fromNullable } from 'fp-ts/lib/Option';
import {
    Layout,
    UnitWidget,
    Panel,
    Page,
    DirectedContainer,
    Profile,
} from 'angled-core/profile';
import { index, flatten } from 'fp-ts/lib/Array';
import { getSelectedUnit } from './project';
import { pickSortL, scopeOption } from 'sdi/lib';
import { InformationUnitName } from 'angled-core/ui';

const logger = debug('sdi:app/angled/queries/profile');

const profileSort = pickSortL((p: Profile) =>
    p.name.toLocaleLowerCase())

export const getProfiles = () => profileSort(query('data/profiles').concat())

export const sortedProfileList = () =>
    getProfiles().filter(p => p.name !== "Tout")
        .concat(getProfiles().filter(p => p.name === "Tout"))

export const findProfile = (name: string) =>
    fromNullable(getProfiles().find(p => p.name === name));

export const getCurrentProfile = () =>
    fromNullable(query('profile/current/name')).chain(findProfile);

export const getCurrentProfileName = () =>
    fromNullable(query('profile/current/name')).getOrElse('');

export const getCurrentPageNumber = () =>
    fromNullable(query('profile/current/page')).getOrElse(0);

export const getCurrentPage = () =>
    getCurrentLayout().chain(l => index(getCurrentPageNumber(), l));

export const getCurrentLayout = () => getCurrentProfile().map(p => p.layout);

const unitsFromPanel = (p: Panel, acc = [] as UnitWidget[]): UnitWidget[] => {
    switch (p.type) {
        case 'ProjectView':
            return acc;
        case 'UnitWidget':
            return acc.concat([p]);
        case 'DirectedContainer':
            return acc.concat(
                flatten(p.children.map(c => unitsFromPanel(c, acc)))
            );
    }
};

const unitsFromBox = (box: DirectedContainer) =>
    flatten(box.children.map(c => unitsFromPanel(c)));

const unitsFromPage = (page: Page) => flatten(page.boxes.map(unitsFromBox));

const unitsFromLayout = (layout: Layout) => flatten(layout.map(unitsFromPage));

export const getCurrentWidget = () =>
    scopeOption()
        .let('layout', getCurrentLayout())
        .let('unit', getSelectedUnit())
        .chain(({ layout, unit }) =>
            fromNullable(unitsFromLayout(layout).find(l => l.name === unit))
        );

export const getCurrentUnitNamesByPage = (): InformationUnitName[][] =>
    getCurrentProfile().fold([], profile =>
        profile.layout.map(page => unitsFromPage(page).map(w => w.name))
    );

logger('Loaded');
