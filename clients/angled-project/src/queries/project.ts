/*
 *  Copyright (C) 2019 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import * as debug from 'debug';
import pointInPolygon from '@turf/boolean-point-in-polygon';
import { query } from 'sdi/shape';
import { fromNullable, none, some, Option } from 'fp-ts/lib/Option';
import { index, mapOption } from 'fp-ts/lib/Array';
import {
    InformationUnitName,
    Project,
    nameToTypeName,
    typeNameToName,
    UnitData,
    getUnitbyName,
    Unit,
} from 'angled-core/ui';
import { scopeOption } from 'sdi/lib';
import tr, { fromRecord, rec, EmptyString } from 'sdi/locale';
import {
    MessageRecordLang,
    remoteNone,
    foldRemote,
    remoteToOption,
} from 'sdi/source';
import { identity } from 'fp-ts/lib/function';
import { Tag, Audience } from 'angled-core/ref';
import { findAudience } from 'angled-core/queries/ref';
import { uniqIdentified, isNotNullNorUndefined, ensureArray } from 'sdi/util';
import { SearchType } from '../components/projectslist/search';
import { Coordinate } from 'ol/coordinate';
import { DisplayType } from '../components/form/display';
import { Timeline } from '../remote';

const logger = debug('sdi:events/project');

export const getProjectsRemote = () => query('data/projects');

export const getProjectsOpt = () => remoteToOption(getProjectsRemote());

const projects = () => getProjectsOpt().getOrElse([]);

export const findLastIndex = (a: any, selector: any): Option<number> => {
    // todo fixme à mettre dans utils ?
    const indexListOfSelected = a
        .map((elem: any, index: number) => {
            if (selector(elem)) {
                return index;
            }
            return -1;
        })
        .filter((index: number) => index !== -1);
    if (indexListOfSelected.length > 0) {
        return some(indexListOfSelected[indexListOfSelected.length - 1]);
    }
    return none;
};

export const getNewNameRecord = () =>
    fromNullable(query('component/project/new/name'));

export const getNewName = (l: MessageRecordLang) => () =>
    getNewNameRecord().fold(EmptyString, record => rec(record, l, ''));

export const getSearchInput = () =>
    fromNullable(query('component/project/search/term')).getOrElse('');

export const getAddProjectHidden = () =>
    query('component/project/new/add-project-hidden');

export const getEditProjectHidden = () =>
    query('component/project/edit/edit-project-hidden');

const filterByName = (term: string) => (project: Project) =>
    fromNullable(project.name)
        .map(unit => fromRecord(unit.name))
        .map(translated => translated.toLocaleLowerCase())
        .fold(
            false,
            normalized =>
                normalized.includes(term.toLocaleLowerCase()) && term !== ''
        );

const filterByDescription = (term: string) => (project: Project) =>
    fromNullable(project.description)
        .map(unit => fromRecord(unit.body))
        .map(translated => translated.toLocaleLowerCase())
        .fold(
            false,
            normalized =>
                normalized.includes(term.toLocaleLowerCase()) && term !== ''
        );

const filterByCapakey = (term: string) => (project: Project) =>
    fromNullable(project.capakey).fold(false, ck => term in ck);

const filterByNova = (term: string) => (project: Project) =>
    fromNullable(project.nova).fold(
        false,
        nova => nova.findIndex(n => n.nova === term) >= 0
    );

const filterById = (id: number) => (project: Project) => project.id === id;

const squareDistance = (p0: Coordinate, p1: Coordinate) => {
    const a = p1[0] - p0[0];
    const b = p1[1] - p0[1];

    return a * a + b * b;
};

const withinDistance = (n: number) => (p0: Coordinate, p1: Coordinate) =>
    squareDistance(p0, p1) < n * n;

const within100Meters = withinDistance(100);

// //This function can be simplified...
// const getSquareDistanceToLine =
//     (p: Coordinate, beginLine: Coordinate, endLine: Coordinate): number => {
//         if (beginLine[0] == endLine[0]) {
//             return Math.min((beginLine[0] - p[0]) * (beginLine[0] - p[0]), squareDistance(p, beginLine), squareDistance(p, endLine));
//         }
//         else if (beginLine[1] == endLine[1]) {
//             return Math.min((beginLine[1] - p[1]) * (beginLine[1] - p[1]), squareDistance(p, beginLine), squareDistance(p, endLine));
//         }
//         else if (beginLine[0] == endLine[1] && beginLine[1] == endLine[0]) {
//             const minSquareDistToEnds = Math.min(squareDistance(p, beginLine), squareDistance(p, endLine));
//             return minSquareDistToEnds / 2;
//         }

//         const coef1 = (endLine[1] - beginLine[1]) / (endLine[0] - beginLine[0]);
//         const coef2 = -1 / coef1;
//         if (coef1 == 1 || coef1 == -1) {

//         }
//         const x = (p[1] - beginLine[1] + coef1 * beginLine[0] - coef2 * p[0]) / (coef1 - coef2);
//         const y = coef1 * (x - beginLine[0]) + beginLine[1];
//         return squareDistance(p, [x, y]);
//     }

const filterByGeometry = (coords: Coordinate) => (project: Project) => {
    const point = project.point;
    const polygon = project.polygon;
    // TODO: lines
    // const line = project.line;

    if (isNotNullNorUndefined(point)) {
        const pointCoords = point.geom.coordinates;
        for (const p of pointCoords) {
            if (p.length >= 2 && within100Meters(coords, [p[0], p[1]])) {
                return true;
            }
        }
        return false;
    } else if (isNotNullNorUndefined(polygon)) {
        return pointInPolygon(coords, polygon.geom);
    }
    // else if (isNotNullNorUndefined(line)
    //     //TO DO: check all line of the multiline (for now, only the first one is checked)
    //     && getSquareDistanceToLine(coords, line.geom.coordinates[0][0] as Coordinate, line.geom.coordinates[0][1] as Coordinate) < 100 * 100) {
    //     return true;
    // }
    return false;
};

const parsePoint = (term: string): Option<Coordinate> => {
    const r = term.split(',').map(parseFloat);
    if (r.length === 2 && !Number.isNaN(r[0]) && !Number.isNaN(r[1])) {
        return some([r[0], r[1]]);
    }
    return none;
};

const makeFilter = (unit: SearchType, term: string) => {
    switch (unit) {
        case 'name':
            return filterByName(term);
        case 'description':
            return filterByDescription(term);
        case 'capakey':
            return filterByCapakey(term);
        case 'nova':
            return filterByNova(term);
        case 'id':
            return Number.isNaN(parseInt(term, 10))
                ? () => false
                : filterById(parseInt(term, 10));
        case 'geometry':
            return parsePoint(term)
                .map(filterByGeometry)
                .getOrElse(() => false);
        default:
            return () => false;
    }
};

const filterProjects = (unit: SearchType, term: string) =>
    projects().filter(makeFilter(unit, term));

export const getSearchUnit = () =>
    fromNullable(query('component/project/search/unit'));

export const getFilteredProjects = () =>
    scopeOption()
        .let('term', fromNullable(query('component/project/search/term')))
        .let('unit', getSearchUnit())
        .map(({ term, unit }) => filterProjects(unit, term));

export const findProject = (pid: number) =>
    fromNullable(projects().find(p => p.id === pid));

export const getCurrentProjectId = () => fromNullable(query('project/current'));

export const getCurrentProjectLastVersion = () =>
    getCurrentProjectId().chain(id =>
        fromNullable(projects().find(p => p.id === id))
    );

// export const getCurrentProject = () =>
//     getCurrentProjectFromTimeline()
//     .foldL(
//         getCurrentProjectLastVersion(),
//         p => some(p)
//     );

export const getCurrentProjectName = () =>
    getCurrentProjectLastVersion()
        .chain(p => fromNullable(p.name))
        .fold(tr.angled('projectNoName'), n => fromRecord(n.name));

export const getCurrentProjectDescription = () =>
    getCurrentProjectLastVersion()
        .chain(p => fromNullable(p.description))
        .fold(tr.angled('projectNoDescription'), d => fromRecord(d.body));

export const getCurrentProjectImage = () =>
    getCurrentProjectLastVersion()
        .chain(p => fromNullable(p.image))
        .fold('empty', i => i.url);

export const getCurrentProjectImageCaption = () =>
    getCurrentProjectLastVersion()
        .chain(p => fromNullable(p.image))
        .fold(tr.angled('projectNoImage'), img => fromRecord(img.caption));

export const getUnitbyNameFromCurrentProject = (
    unitName: InformationUnitName
) => getCurrentProjectLastVersion().chain(p => getUnitbyName(unitName, p));

export const getSelectedUnit = () =>
    fromNullable(query('component/project/form/selected-unit'));

export const getStagedUnits = () => query('component/project/form/stage');

export const getStagedUnitNames = () =>
    getStagedUnits().map(unit => typeNameToName[unit.unit]);

export const getNamedDataUnits = (name: InformationUnitName) =>
    getStagedUnits().filter(data => data.unit === nameToTypeName[name]);

export const getLastStageUnitForName =
    // get the last unit in stage for a given ui name
    (unitName: InformationUnitName) =>
        findLastIndex(getStagedUnits(), (iterUnitData: UnitData) => {
            const unit = iterUnitData.unit;
            const unitNameInt = typeNameToName[unit];
            return unitNameInt === unitName;
        });

export const isComitted = (index: number) =>
    query('component/project/form/commit').indexOf(index) >= 0;

export const hasDataToCommit = () =>
    query('component/project/form/commit').length > 0;

export const hasDataStaged = () =>
    query('component/project/form/stage').length > 0;

export const hasRemovedDataStaged = () =>
    query('component/project/form/stage-removed').length > 0;

const filterTimeline = (ts: Timeline | Readonly<Timeline>) => {
    const min = getTimelineWindow()
        .map(w => Date.now() - w)
        .getOrElse(0);
    return ts.filter(t => t.created_at > min);
};

export const getTimeline = () =>
    foldRemote<Timeline, string, Option<Timeline>>(
        () => none,
        () => none,
        () => none,
        timeline => some(filterTimeline(timeline))
    )(query('component/project/timeline'));

const getTimelineDates = () =>
    getTimeline().map(ts => ts.map(t => t.created_at));

export const getTimelineDateStart = () =>
    getTimelineDates().chain(ds => index(0, ds));

export const getTimelineDateEnd = () =>
    getTimelineDates().chain(ds => index(ds.length - 1, ds));

export const getCurrentTimelineId = () =>
    fromNullable(query('component/project/timeline/current-id'));

export const getCurrentTransaction = () =>
    scopeOption()
        .let('timeline', getTimeline())
        .let('txId', getCurrentTimelineId())
        .chain(({ timeline, txId }) =>
            fromNullable(timeline.find(tx => tx.id === txId))
        );

export const unitNameIsInCurrentTransaction = (name: InformationUnitName) =>
    getCurrentTransaction()
        .map(tx => tx.units.indexOf(name) >= 0)
        .getOrElse(false);

export const getCurrentTimelineTime = () =>
    getCurrentTransaction().map(tx => tx.created_at);

export const getCurrentProjectFromTimeline = () =>
    remoteToOption(query('component/project/timeline/project'));

export const getUnitHistory = () =>
    remoteToOption(query('component/project/timeline-unit'));

const getUnitHistoryDates = () =>
    getUnitHistory()
        .map(units => units.map(u => u.created_at))
        .getOrElse([]);

export const getTimelineUnitDateStart = () => index(0, getUnitHistoryDates());

export const getTimelineUnitDateEnd = () =>
    index(getUnitHistoryDates().length - 1, getUnitHistoryDates());

export const getTimelineWindow = () =>
    fromNullable(query('component/project/timeline/window'));

export const getRemovedFromMulti = () =>
    query('component/project/form/removed');

export const hasRemovedFromMulti = () =>
    query('component/project/form/removed').length > 0;

export const isRemovedFromMulti = (unit: Unit) =>
    query('component/project/form/removed').findIndex(
        pair => pair[0] === unit.unit && pair[1] === unit.id
    ) >= 0;

export const getModalStatus = (key: string) => () =>
    fromNullable(query('component/project/form/modal/status')[key]).fold(
        'close',
        identity
    );

export const getTags = () => query('data/tags');

export const findTag = (id: number) =>
    fromNullable(getTags().find(t => id === t.id));

export const getTagsForUnit = (unitData: Unit | Unit[]) => {
    const dataArray = Array.isArray(unitData) ? unitData : [unitData];
    const tags = dataArray.reduce<Tag[]>(
        (acc, u) => acc.concat(mapOption(u.tags, tref => findTag(tref.tag))),
        []
    );
    return uniqIdentified(tags);
};

export const getAudiencesForUnit = (unitData: Unit | Unit[]) =>
    ensureArray(unitData).reduce<Audience[]>(
        (acc, u) =>
            acc.concat(mapOption(u.readings, r => findAudience(r.audience))),
        []
    );

// export const getDisplayAudience = () =>
//     fromNullable(query('component/project/display-audience'));

// export const audienceIsForDisplay = (a: Audience) =>
//     getDisplayAudience()
//         .map(({ id }) => id === a.id)
//         .getOrElse(false);

export const getAudiencesForStaged = () =>
    mapOption(query('component/project/form/stage-audience').concat([]), id =>
        findAudience(id)
    );

export const getNovaWFS = (novaseq: string) =>
    fromNullable(query('component/project/data/nova')[novaseq]).getOrElse(
        remoteNone
    );

export const getDisplayType = () => query('component/project/display/type');

export const withDisplayType = <T>(dt: DisplayType, a: T) =>
    getDisplayType() === dt ? some(a) : none;

export const getSavingError = () =>
    fromNullable(query('component/project/saving/error'));

logger('loaded');
