import * as debug from 'debug';

import { index } from 'fp-ts/lib/Array';
import { Option } from 'fp-ts/lib/Option';
import { literal, union, TypeOf } from 'io-ts';

import { Router, Path } from 'sdi/router';
import { getNumber } from 'sdi/util';

import { setLayout, setProfileAndPannelNumber } from './app';
import { setCurrentProject } from './project';
import { getCurrentProfile, findProfile } from '../queries/profile';
import { resetDisplayAudience } from 'angled-core/events/ui';

const logger = debug('sdi:route');

// tslint:disable-next-line: variable-name
const RouteIO = union([
    literal('index'),
    literal('map-select'),
    literal('form-select'),
    literal('form'),
    literal('review'),
]);
type Route = TypeOf<typeof RouteIO>;

const { home, route, navigate } = Router<Route>('angled-project');

const projectIdParser = (p: Path) => index(0, p).chain(getNumber);

type NameAndPannelNumberResult = [number, Option<string>, Option<number>];

const projectNameAndPositionParser = (
    p: Path
): Option<NameAndPannelNumberResult> =>
    projectIdParser(p).map<NameAndPannelNumberResult>(pid => [
        pid,
        index(1, p),
        index(2, p).chain(getNumber),
    ]);

home('index', _p => {
    setLayout('Home');
});

route('map-select', _p => {
    setLayout('MapSelect');
});

route(
    'form',
    r =>
        r.map(([projectId, pageName, pannelNumber]) => {
            const targetProfile = pageName.foldL(
                getCurrentProfile,
                findProfile
            );
            setCurrentProject(projectId);
            resetDisplayAudience();

            targetProfile.foldL(
                () => setLayout('FormSelect'),
                p => {
                    setProfileAndPannelNumber(
                        p.name,
                        pannelNumber.getOrElse(0)
                    );
                    setLayout('Form');
                }
            );
        }),
    projectNameAndPositionParser
);

export const loadRoute = (initial: string[]) =>
    index(0, initial).map(prefix =>
        RouteIO.decode(prefix).map(c => navigate(c, initial.slice(1)))
    );

export const navigateHome = () => navigate('index', []);

export const navigateMap = () => navigate('map-select', []);

export const navigateForm = (
    projectId: number,
    formId: string,
    pannelNumber = 0
) => navigate('form', [projectId, formId, pannelNumber]);

export const navigateFormNaked = (projectId: number) =>
    navigate('form', [projectId]);

logger('loaded');
