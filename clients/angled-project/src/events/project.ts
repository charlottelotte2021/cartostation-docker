/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {
    prepareWriteDomainForUnit,
    resetDisplayAudience,
    resetDocumentFile,
    resetFormInput,
    resetNewFkFormInput,
    resetWriteDomain,
    setUnitInEdition,
} from 'angled-core/events/ui';
import {
    getCurrentDocumentFiles,
    getCurrentImageFile,
    getUnitInEdition,
} from 'angled-core/queries/ui';
import { Audience, Tag } from 'angled-core/ref';
import { deleteReading, postReading } from 'angled-core/remote/ref';
import {
    getUnitbyName,
    InformationUnitName,
    isMulti,
    nameToTypeName,
    Project,
    typeNameToName,
    Unit,
    UnitData,
    UnitDataIO,
    UnitDataPost,
    updateProject,
} from 'angled-core/ui';
import * as debug from 'debug';
import { identity } from 'fp-ts/lib/function';
import { fromNullable } from 'fp-ts/lib/Option';
import * as io from 'io-ts';
import { getApiUrl } from 'sdi/app';
import { ModalStatus } from 'sdi/components/modal';
import { scopeOption } from 'sdi/lib';
import { assign, dispatch, dispatchK, query } from 'sdi/shape';
import { MessageRecordLang, postIO, MessageRecord } from 'sdi/source';
import {
    defaultFetchOptions,
    remoteError,
    remoteLoading,
    remoteNone,
    remoteSuccess,
    mapRemote,
    foldRemote,
} from 'sdi/source/remote';
import { ensureArray, updateCollection } from 'sdi/util';
import { DisplayType, parseWidgetFilter } from '../components/form/display';
import { SearchType } from '../components/projectslist/search';
import { getCurrentPage, getCurrentProfile } from '../queries/profile';
import {
    getCurrentProjectId,
    getCurrentTimelineTime,
    getLastStageUnitForName,
    getRemovedFromMulti,
    getStagedUnits,
    findProject,
    getProjectsRemote,
    getCurrentProjectFromTimeline,
} from '../queries/project';
import { fetchNova } from '../remote/nova';
import { deleteTag, postProject, postTag, postUnits } from '../remote/project';
import {
    fetchProjectAtTime,
    fetchUnitHistory,
    UnitHistory,
} from '../remote/timeline';
import { RemovedUnitIdentifier } from '../shape/project';
import { loadTimeline, setLayout } from './app';
import { navigateForm, navigateFormNaked } from './route';
import { openSaveFinalStep } from './modal';
import {
    Panel,
    UnitWidget,
    WidgetFilter,
    WidgetFilterOpt,
} from 'angled-core/profile';
import { currentUserCanSeeUnit } from 'angled-core/queries/ref';

const logger = debug('sdi:events/app');

const addProject = (p: Project) =>
    dispatch('data/projects', projects =>
        mapRemote(projects, xs => xs.filter(x => x.id !== p.id).concat(p))
    );

const stageNewProjectName = (name: MessageRecord) =>
    assign('component/project/form/stage', [{ unit: 'UnitName', name }]);

export const addNewProject = () =>
    fromNullable(query('component/project/new/name'))
        .map(name =>
            postProject(getApiUrl('geodata/angled/p/project/'))
                // .then(p =>
                //     postUnits(p.id,
                //         {
                //             units: { name: { unit: 'UnitName', name } },
                //             meta: { audiences: [] },
                //         }))
                .then(p => {
                    addProject(p);
                    assign('project/current', p.id);
                    stageNewProjectName(name);
                    navigateFormNaked(p.id);
                    openSaveFinalStep();
                })
        )
        .map(() => assign('component/project/new/name', null));

export const setNewName = (l: MessageRecordLang) => (name: string) =>
    dispatch('component/project/new/name', record => ({
        ...record,
        [l]: name,
    }));

export const setSearchInput = (name: string) =>
    assign('component/project/search/term', name);

export const setSearchUnit = (unit: SearchType) =>
    assign('component/project/search/unit', unit);

export const resetSearch = () => {
    assign('component/project/search/term', null);
    assign('component/project/search/unit', null);
};

export const setCurrentProject = (id: number) => {
    assign('project/current', id);
    foldRemote(
        () => {},
        () => window.setTimeout(() => setCurrentProject(id), 500),
        () => {},
        () => {
            loadTimeline(id);
            findProject(id).map(project =>
                assign(
                    'component/project/timeline/project',
                    remoteSuccess(project)
                )
            );
        }
    )(getProjectsRemote());
};

export const setAddProjectHidden = (cond: boolean) =>
    assign('component/project/new/add-project-hidden', cond);

export const setEditProjectHidden = (cond: boolean) =>
    assign('component/project/edit/edit-project-hidden', cond);

const setRemovedDataStaged = (r: RemovedUnitIdentifier[]) =>
    assign('component/project/form/stage-removed', r);

const setRemovedUnitToStaged = () => {
    const removedList: RemovedUnitIdentifier[] = [];
    getRemovedFromMulti().map(r => removedList.push(r));
    setRemovedDataStaged(removedList);
};

export const stageCurrentUnit = () => {
    fromNullable(getUnitInEdition())
        .map(u =>
            dispatch('component/project/form/stage', data =>
                UnitDataIO.decode(u).fold(
                    () => data,
                    v => data.concat([v])
                )
            )
        )
        .map(resetFormInput);
    setRemovedUnitToStaged();
};

export const updateLastSageUnitForName =
    // update the last unit in stage for a given ui-name
    (unitName: InformationUnitName, newData: UnitData) =>
        getLastStageUnitForName(unitName).map((index: number) => {
            const stageUnits = getStagedUnits();
            const stageUnitsUpdated = stageUnits.map((u, i) => {
                if (index === i) return newData;
                return u;
            });
            assign('component/project/form/stage', stageUnitsUpdated);
        });

export const displayEditUnitDataForm = (u: UnitData) => {
    setUnitInEdition(u);
    getCurrentProjectId().map(pid =>
        getCurrentProfile().map(profile => navigateForm(pid, profile.name))
    );
};

export const removeUnitFromMulti = (unit: Unit) =>
    dispatch('component/project/form/removed', rs =>
        rs.concat([[unit.unit, unit.id]])
    );

export const restoreUnitFromMulti = (unit: Unit) =>
    dispatch('component/project/form/removed', rs =>
        rs.filter(r => r[0] !== unit.unit || r[1] !== unit.id)
    );

export const clearRemoveUnitFromMulti = () =>
    assign('component/project/form/removed', []);

export const updateStagedCurrentUnit = () =>
    fromNullable(getUnitInEdition())
        .map(u =>
            UnitDataIO.decode(u).fold(
                () => null,
                v => updateLastSageUnitForName(typeNameToName[v.unit], v)
            )
        )
        .map(resetFormInput);

export const commit = (index: number) =>
    dispatch('component/project/form/commit', cs =>
        cs.filter(i => i !== index).concat(index)
    );

export const unCommit = (index: number) =>
    dispatch('component/project/form/commit', cs =>
        cs.filter(i => i !== index)
    );

export const unCommitAll = () =>
    scopeOption()
        .let('pid', getCurrentProjectId())
        .let('form', getCurrentProfile())
        .map(({ pid, form }) => {
            assign('component/project/form/commit', []);
            setEditProjectHidden(false);
            navigateForm(pid, form.name);
        });

export const resetStage = () => {
    assign('component/project/form/stage', []);
    assign('component/project/form/stage-removed', []);
    assign('component/project/form/stage-audience', []);
    assign('component/project/form/commit', []);
    assign('component/ui/form/manip-team-member', []);
    assign('component/project/edit/edit-project-hidden', true);

    // assign('component/project/timeline/window', null);
    // assign('component/project/timeline/current-id', null);
    // getTimeline()
    //     .chain(ts => index(0, ts))
    //     .map(t => setCurrentTimelineUnit(t.name));
};

// tslint:disable-next-line: variable-name
const UploadedIO = io.interface({
    id: io.string,
    url: io.string,
});

type Uploaded = io.TypeOf<typeof UploadedIO>;
const upload = (url: string, f: File): Promise<Uploaded> => {
    const options = defaultFetchOptions();
    const data = new FormData();
    data.append('file', f);
    options.body = data;
    const headers = options.headers as Headers;
    if (headers) {
        headers.delete('Content-Type');
    }
    return postIO(UploadedIO, url, {}, options);
};

const postUnitsInProject = (
    units: UnitDataPost,
    audiences: number[],
    success: (pid: number) => void,
    error: (err: unknown) => void
) =>
    getCurrentProjectId().map(pid =>
        postUnits(pid, { units, meta: { audiences } })
            .then(addProject)
            .then(() => {
                success(pid);
                clearProjectForm(pid);
            })
            .catch(error)
    );

export const clearProjectForm = (pid: number) => {
    setEditProjectHidden(true);
    loadTimeline(pid);
    resetDocumentFile();
    clearRemoveUnitFromMulti();
    resetWriteDomain();
    assign('component/ui/form/manip-team-member', []);
};

export const pushStaged = () => {
    setLayout('Saving');
    assign('component/project/saving/error', null);
    const onSuccess = (pid: number) => {
        loadTimeline(pid).then(loadCurrentProjectAtTime);
        // here we've got to special case at project creation
        // when there's possibly not a profile selected yet.
        getCurrentProfile().foldL(
            () => setLayout('FormSelect'),
            _ => setLayout('Form')
        );
    };
    const onError = (err: unknown) => {
        logger(`SaveError`, err);
        assign('component/project/saving/error', `${err}`);
    };

    dispatch('component/project/form/stage', us => {
        const audiences = query('component/project/form/stage-audience').map(
            identity
        );

        const toBeRemoved = query('component/project/form/removed').reduce(
            (acc, [t, id]) => ({
                ...acc,
                [t]: acc[t] === undefined ? [id] : acc[t]!.concat(id),
            }),
            {} as { [k in Unit['unit']]?: number[] }
        );

        const objects = Object.keys(nameToTypeName).reduce<UnitDataPost>(
            (acc, name: InformationUnitName) => {
                const units = us.filter(u => u.unit === nameToTypeName[name]);
                if (units.length > 0) {
                    if (isMulti(name)) {
                        const typeName = nameToTypeName[name] as Unit['unit'];
                        const removed = toBeRemoved[typeName] || [];
                        return { ...acc, [name]: { new: units, removed } };
                    } else {
                        return { ...acc, [name]: units[0] };
                    }
                }
                return acc;
            },
            {}
        );

        // Not that simple, but here's a way to remove multi which haven't any added item
        Object.keys(toBeRemoved).forEach((k: Unit['unit']) => {
            const unitName = typeNameToName[k];
            const removed = toBeRemoved[k] || [];
            if (!(unitName in objects)) {
                /**
                 * TSC 3.5.2 hurts on this with:
                 * TS2590: Expression produces a union type that is too complex to represent.
                 *
                 * Only trick I found so far to make it compile, and we're in a rush
                 */
                // objects[unitName] = { new: [] as UnitData[], removed };
                Object.assign(objects, {
                    [unitName]: { new: [] as UnitData[], removed },
                });
            }
        });

        if ('image' in objects) {
            const imgFile: File = getCurrentImageFile();
            const imgUrl: string = objects.image!.url;
            if ('document' in objects) {
                const docFiles = getCurrentDocumentFiles();
                const uploadDocsUrlRoot = '/documents/documents/';
                upload(imgUrl, imgFile)
                    .then(data => {
                        objects.image!.url = data.url;
                        const docUrls: {
                            unit: 'UnitDocument';
                            url: string;
                            caption: {};
                        }[] = [];
                        if (docFiles.length === 0) {
                            postUnitsInProject(
                                objects,
                                audiences,
                                onSuccess,
                                onError
                            );
                        } else {
                            docFiles.map((f, i) =>
                                upload(uploadDocsUrlRoot, f)
                                    .then(data => {
                                        docUrls.push({
                                            unit: 'UnitDocument',
                                            url: data.url,
                                            caption:
                                                objects.document!.new[i]
                                                    .caption,
                                        });
                                        if (i === docFiles.length - 1) {
                                            objects.document!.new = docUrls;
                                            postUnitsInProject(
                                                objects,
                                                audiences,
                                                onSuccess,
                                                onError
                                            );
                                        }
                                    })
                                    .catch(err => {
                                        console.log(
                                            `Error posting doc : ${err}`
                                        );
                                    })
                            );
                        }
                    })
                    .catch(err => {
                        console.log(`Error posting image : ${err}`);
                    });
            } else {
                upload(imgUrl, imgFile)
                    .then(data => {
                        objects.image!.url = data.url;
                        postUnitsInProject(
                            objects,
                            audiences,
                            onSuccess,
                            onError
                        );
                    })
                    .catch(err => {
                        console.log(`Error posting image : ${err}`);
                    });
            }
        } else {
            if ('document' in objects) {
                const docFiles = getCurrentDocumentFiles();
                const uploadDocsUrlRoot = '/documents/documents/';
                const docUrls: {
                    unit: 'UnitDocument';
                    url: string;
                    caption: {};
                }[] = [];
                if (docFiles.length === 0) {
                    postUnitsInProject(objects, audiences, onSuccess, onError);
                } else {
                    docFiles.map((f, i) =>
                        upload(uploadDocsUrlRoot, f)
                            .then(data => {
                                docUrls.push({
                                    unit: 'UnitDocument',
                                    url: data.url,
                                    caption: objects.document!.new[i].caption,
                                });
                                if (i === docFiles.length - 1) {
                                    objects.document!.new = docUrls;
                                }
                                postUnitsInProject(
                                    objects,
                                    audiences,
                                    onSuccess,
                                    onError
                                );
                            })
                            .catch(err => {
                                console.log(`Error posting doc : ${err}`);
                            })
                    );
                }
            } else {
                postUnitsInProject(objects, audiences, onSuccess, onError);
            }
        }
        return [];
    });
};

export const selectUnit = (name: InformationUnitName) => {
    assign('component/project/form/selected-unit', name);
    resetFormInput();
    resetNewFkFormInput();
    loadUnitHistory(name);
    resetWriteDomain();
    prepareWriteDomainForUnit(name);
};

export const clearTimeLine = () => {
    assign('component/project/timeline', remoteNone);
    assign('component/project/timeline-unit', remoteNone);
    assign('component/project/timeline/project', remoteNone);
    assign('component/project/timeline/window', null);
    assign('component/project/timeline/current-id', null);
};

export const loadUnitHistory = (name: InformationUnitName) => {
    assign('component/project/timeline-unit', remoteLoading);
    getCurrentProjectId().map(pid =>
        fetchUnitHistory(pid, name)
            .then((u: UnitHistory) =>
                assign('component/project/timeline-unit', remoteSuccess(u))
            )
            .catch(err =>
                assign('component/project/timeline-unit', remoteError(`${err}`))
            )
    );
};

export const setCurrentTimelineId = (id: number) =>
    assign('component/project/timeline/current-id', id);

export const setTimelineWindow = (time: number) =>
    assign('component/project/timeline/window', time);

export const clearTimelineWindow = () =>
    assign('component/project/timeline/window', null);

export const loadCurrentProjectAtTime = () => {
    assign('component/project/timeline/project', remoteLoading);
    scopeOption()
        .let('pid', getCurrentProjectId())
        .let('timestamp', getCurrentTimelineTime())
        .map(({ pid, timestamp }) =>
            fetchProjectAtTime(pid, timestamp)
                .then(p =>
                    assign(
                        'component/project/timeline/project',
                        remoteSuccess(p)
                    )
                )
                .catch(err =>
                    assign(
                        'component/project/timeline/project',
                        remoteError(`${err}`)
                    )
                )
        );
};

export const setModalStatus = (key: string) => (ms: ModalStatus) =>
    dispatch('component/project/form/modal/status', s => ({ ...s, [key]: ms }));

const postTagToUnit = (unitData: Unit, unitId: number, tagId: number) =>
    postTag(getApiUrl(`geodata/angled/t/${typeNameToName[unitData.unit]}`), {
        tag: tagId,
        unit: unitId,
    }).then(t => {
        // update project in place
        dispatch('data/projects', projects =>
            mapRemote(projects, projects => {
                return fromNullable(
                    projects.find(p => p.id === unitData.project)
                ).fold(projects, project => {
                    unitData.tags.push(t);
                    const np = updateProject(unitData)(project);
                    return projects.filter(x => x.id !== np.id).concat(np);
                });
            })
        );
    });

export const addTagToUnit = (unitData: Unit | Unit[]) => (t: Tag) => {
    if (Array.isArray(unitData)) {
        unitData.forEach(u => postTagToUnit(u, u.id, t.id));
    } else {
        postTagToUnit(unitData, unitData.id, t.id);
    }
};

const deleteTagFromUnit = (unitData: Unit, tagId: number) =>
    fromNullable(unitData.tags.find(t => t.tag === tagId)).map(tagged =>
        deleteTag(
            getApiUrl(
                `geodata/angled/t/${typeNameToName[unitData.unit]}/${tagged.id}`
            )
        ).then(() => {
            // update project in place
            dispatch('data/projects', projects =>
                mapRemote(projects, projects => {
                    return fromNullable(
                        projects.find(p => p.id === unitData.project)
                    ).fold(projects, project => {
                        unitData.tags = unitData.tags.filter(
                            t => t.id !== tagged.id
                        );
                        const np = updateProject(unitData)(project);
                        return projects.filter(x => x.id !== np.id).concat(np);
                    });
                })
            );
        })
    );

export const removeTagFromUnit = (unitData: Unit | Unit[]) => (t: Tag) => {
    if (Array.isArray(unitData)) {
        unitData.forEach(u => deleteTagFromUnit(u, t.id));
    } else {
        deleteTagFromUnit(unitData, t.id);
    }
};

// audience

const postAudienceToUnit = (
    unitData: Unit,
    unitId: number,
    audienceId: number
) =>
    postReading(
        getApiUrl(`geodata/angled/e/${typeNameToName[unitData.unit]}`),
        {
            unit: unitId,
            audience: audienceId,
        }
    ).then(r => {
        // update project in place
        dispatch('data/projects', projects =>
            mapRemote(projects, projects => {
                return fromNullable(
                    projects.find(p => p.id === unitData.project)
                ).fold(projects, project => {
                    unitData.readings.push(r);
                    const np = updateProject(unitData)(project);
                    return projects.filter(x => x.id !== np.id).concat(np);
                });
            })
        );
    });

export const addAudienceToUnit = (unitData: Unit | Unit[]) => (a: Audience) => {
    if (Array.isArray(unitData)) {
        unitData.forEach(u => postAudienceToUnit(u, u.id, a.id));
    } else {
        postAudienceToUnit(unitData, unitData.id, a.id);
    }
};

const addAudienceToWidget = (
    audience: Audience,
    project: Project,
    widget: UnitWidget,
    filter: WidgetFilterOpt,
    exclude: WidgetFilterOpt
) => {
    getUnitbyName(widget.name, project).map(unit => {
        const filterUnit =
            ({ fieldName, term }: WidgetFilter) =>
            (u: Unit) =>
                fieldName in u && (u as any)[fieldName] === term.id;
        const excludeUnit =
            ({ fieldName, term }: WidgetFilter) =>
            (u: Unit) =>
                !(fieldName in u && (u as any)[fieldName] === term.id);
        const units = (() => {
            const units = ensureArray(unit);
            if (filter.isSome()) {
                return units.filter(filterUnit(filter.value));
            }
            if (exclude.isSome()) {
                return units.filter(excludeUnit(exclude.value));
            }
            return units;
        })();
        const visibleUnits = units.filter(u => u.visible);
        visibleUnits.map(addAudienceToUnit).map(f => f(audience));
    });
};
const addAudienceToPanel = (
    audience: Audience,
    project: Project,
    panel: Panel
): void => {
    switch (panel.type) {
        case 'DirectedContainer': {
            panel.children.map(panel =>
                addAudienceToPanel(audience, project, panel)
            );
            return;
        }

        case 'UnitWidget': {
            const filter = parseWidgetFilter(panel, 'filter');
            const exclude = parseWidgetFilter(panel, 'exclude');
            return addAudienceToWidget(
                audience,
                project,
                panel,
                filter,
                exclude
            );
        }
        default:
            return;
    }
};

export const addAudienceToProfilePage = (audience: Audience) =>
    scopeOption()
        .let('page', getCurrentPage())
        .let('project', getCurrentProjectFromTimeline())
        .map(({ page, project }) =>
            page.boxes.map(panel =>
                addAudienceToPanel(audience, project, panel)
            )
        );

const deleteAudienceFromUnit = (unitData: Unit, audienceId: number) =>
    unitData.readings
        .filter(r => r.audience === audienceId)
        .map(reading =>
            deleteReading(
                getApiUrl(
                    `geodata/angled/e/${typeNameToName[unitData.unit]}/${
                        reading.id
                    }`
                )
            ).then(() => {
                // update project in place
                dispatch('data/projects', projects =>
                    mapRemote(projects, projects => {
                        return fromNullable(
                            projects.find(p => p.id === unitData.project)
                        ).fold(projects, project => {
                            unitData.readings = unitData.readings.filter(
                                r => r.id !== reading.id
                            );
                            unitData.visible = currentUserCanSeeUnit(unitData);
                            const np = updateProject(unitData)(project);

                            return projects
                                .filter(x => x.id !== np.id)
                                .concat(np);
                        });
                    })
                );
            })
        );

export const removeAudienceFromUnit =
    (unitData: Unit | Unit[]) => (a: Audience) => {
        if (Array.isArray(unitData)) {
            unitData.forEach(u => deleteAudienceFromUnit(u, a.id));
        } else {
            deleteAudienceFromUnit(unitData, a.id);
        }
    };

export const addAudienceToStaged = (a: Audience) =>
    dispatch('component/project/form/stage-audience', as =>
        as.filter(i => i !== a.id).concat(a.id)
    );

export const removeAudienceFromStaged = (a: Audience) =>
    dispatch('component/project/form/stage-audience', as =>
        as.filter(i => i !== a.id)
    );

const dispatchNovaData = dispatchK('component/project/data/nova');
export const loadNovaWFS = (novaseq: string) =>
    fromNullable(query('component/project/data/nova')[novaseq]).foldL(
        () => {
            dispatchNovaData(c => updateCollection(c, novaseq, remoteLoading));
            fetchNova('novaseq', novaseq)
                .then((data: object) =>
                    dispatchNovaData(c =>
                        updateCollection(c, novaseq, remoteSuccess(data))
                    )
                )
                .catch(err =>
                    dispatchNovaData(c => {
                        const errors = Object.keys(err)
                            .map(k => `${k}: ${err[k]}`)
                            .join('\n');
                        return updateCollection(
                            c,
                            novaseq,
                            remoteError(errors)
                        );
                    })
                );
            return false;
        },
        () => true
    );

export const setDisplayType = (t: DisplayType) => {
    assign('component/project/display/type', t);
    resetDisplayAudience();
};

logger('loaded');
