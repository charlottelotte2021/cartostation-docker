/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { assign } from 'sdi/shape';

import {
    fetchProjects,
    fetchProfiles,
    fetchTimeline,
    fetchAlias,
} from '../remote';
import { AppLayout } from '../shape/app';
import { getApiUrl } from 'sdi/app';

import { setCurrentTimelineId, clearTimeLine } from '../events/project';
import { remoteSuccess, remoteLoading, remoteError } from 'sdi/source';
import { last } from 'fp-ts/lib/Array';
import { activity } from 'sdi/activity';

const logger = debug('sdi:events/app');

export const activityLogger = activity('angled-project');

export const setLayout = (l: AppLayout) => assign('app/layout', l);

export const loadProjects = () => {
    assign('data/projects', remoteLoading);
    return fetchProjects(getApiUrl('geodata/angled/p/project/'))
        .then(projects => assign('data/projects', remoteSuccess(projects)))
        .catch(err => assign('data/projects', remoteError(`${err}`)));
};

export const loadProfiles = () =>
    fetchProfiles(getApiUrl('geodata/angled/profi/profile/'))
        .then(profiles => assign('data/profiles', profiles))
        .catch(err => logger(err));

export const loadAlias = () =>
    fetchAlias(getApiUrl('alias')).then(as => assign('data/alias', as));

export const loadTimeline = (id: number) => {
    clearTimeLine();
    assign('component/project/timeline', remoteLoading);
    return fetchTimeline(id)
        .then(timeline => {
            timeline.sort((a, b) => a.created_at - b.created_at);
            assign('component/project/timeline', remoteSuccess(timeline));
            last(timeline).map(tx => setCurrentTimelineId(tx.id));
        })
        .catch(err =>
            assign('component/project/timeline', remoteError(`Error ${err}`))
        );
};

export const setProfileAndPannelNumber = (
    pageName: string,
    pannelNumber: number
) => {
    assign('profile/current/name', pageName);
    assign('profile/current/page', pannelNumber);
};

logger('loaded');
