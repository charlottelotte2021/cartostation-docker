import * as debug from 'debug';

import {
    register,
    renderSaveFinalStep,
    renderNotifier,
} from '../components/modal';
import { getDisplayProjectNotif, getUnreadNotifications } from 'angled-core/queries/sub';
import { markRead } from 'angled-core/events/sub';


const logger = debug('sdi:events/modal');

export const [closeSaveFinalStep, openSaveFinalStep] = register(
    'project/save-final-step',
    renderSaveFinalStep
);

const [, openNotifierImpl] = register('project/notifier', renderNotifier);

export const openNotifier = () => {
    openNotifierImpl();
    const t = Math.min(5, getDisplayProjectNotif().length)
    window.setTimeout(() => getUnreadNotifications().map(markRead), t * 1000)

};


logger('loaded');
