import { dispatch, assign } from 'sdi/shape';
import {
    FetchData,
    removeLayer,
    addLayer,
    singleClickInteraction,
} from 'sdi/map';
import {
    GeometryType,
    Inspire,
    ILayerInfo,
    IMapInfo,
    StyleConfig,
} from 'sdi/source';
import { right } from 'fp-ts/lib/Either';
import { some } from 'fp-ts/lib/Option';
import { Project } from 'angled-core/ui';
import { isPoint, isLine, isPolygon, toGeoJSON } from 'angled-core/project';
import { nameToCode } from 'sdi/components/button/names';
import { mapSelectID } from 'angled-core/map';
import { projectSelectMap } from 'angled-project/src/components/map';
import { getProjectsOpt } from '../queries/project';

////////// select map

const metadataTemplate = (geometryType: GeometryType): Inspire => ({
    id: geometryType,
    geometryType,
    resourceTitle: { fr: geometryType, nl: geometryType, en: geometryType },
    resourceAbstract: { fr: geometryType, nl: geometryType, en: geometryType },
    uniqueResourceIdentifier: geometryType,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: Date(), revision: Date() },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: Date(),
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
});

const makeStyle = (geometryType: GeometryType): StyleConfig => {
    switch (geometryType) {
        case 'Point':
        case 'MultiPoint':
            return {
                kind: 'point-simple',
                marker: {
                    codePoint: nameToCode('circle'),
                    size: 12,
                    color: 'rgb(223,88,68)',
                },
            };
        case 'LineString':
        case 'MultiLineString':
            return {
                kind: 'line-simple',
                strokeColor: 'rgb(223,88,68)',
                strokeWidth: 1,
                dash: [],
            };
        case 'Polygon':
        case 'MultiPolygon':
            return {
                kind: 'polygon-simple',
                fillColor: 'rgb(223,88,68)',
                pattern: true,
                patternAngle: 45,
                strokeColor: 'rgb(223,88,68)',
                strokeWidth: 0.5,
            };
    }
};

const layerTemplate = (geometryType: GeometryType): ILayerInfo => ({
    id: geometryType,
    legend: null,
    group: null,
    metadataId: geometryType,
    visible: true,
    featureViewOptions: { type: 'default' },
    style: makeStyle(geometryType),
    layerInfoExtra: null,
    visibleLegend: true,
    opacitySelector: false,
});

const mapTemplate = (): IMapInfo => ({
    baseLayer: 'urbis.irisnet.be/urbis_gray',
    id: mapSelectID,
    url: `/dev/null/angled/`,
    lastModified: 1523599299611,
    status: 'published',
    title: { fr: 'SOLAR', nl: 'SOLAR', en: 'SOLAR' },
    description: { fr: 'SOLAR', nl: 'SOLAR', en: 'SOLAR' },
    categories: [],
    attachments: [],
    layers: [
        layerTemplate('MultiPoint'),
        layerTemplate('MultiLineString'),
        layerTemplate('MultiPolygon'),
    ],
});

export const loadMaps = () =>
    dispatch('data/maps', state => state.concat([mapTemplate()]));

const layerInfoOption = (geometryType: GeometryType) => () =>
    some({
        name: { fr: geometryType, nl: geometryType, en: geometryType },
        info: layerTemplate(geometryType),
        metadata: metadataTemplate(geometryType),
    });

const fetchData =
    (geometryType: GeometryType): FetchData =>
    () => {
        let filter = (_p: Project) => false;
        switch (geometryType) {
            case 'Point':
            case 'MultiPoint':
                filter = isPoint;
                break;
            case 'LineString':
            case 'MultiLineString':
                filter = isLine;
                break;
            case 'Polygon':
            case 'MultiPolygon':
                filter = isPolygon;
                break;
        }
        const projects = getProjectsOpt().map(projects =>
            toGeoJSON(projects.filter(filter))
        );
        return right(projects);
    };

export const addProjectsLayer = () => {
    assign('port/map/interaction', singleClickInteraction());
    const xs: GeometryType[] = [
        'MultiPoint',
        'MultiLineString',
        'MultiPolygon',
    ];
    const shouldRetry = xs
        .map(x => {
            removeLayer(projectSelectMap, x);
            return addLayer(projectSelectMap, layerInfoOption(x), fetchData(x));
        })
        .reduce((acc, o) => o.isNone() || acc, false);
    if (shouldRetry) {
        setTimeout(addProjectsLayer, 500);
    }
};
