/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV, NodeOrOptional } from 'sdi/components/elements';
import header from 'sdi/components/header';
import footer from 'sdi/components/footer';
import { loop, getApiUrl, getLang } from 'sdi/app';
import { langAction, visitAction } from 'sdi/activity';

import { loadUniverse } from 'angled-core/events/universe';
import {
    loadAllBaseLayers,
    loadAllDatasetMetadata,
} from 'angled-core/events/map';
import { loadAllRefsDebug } from 'angled-core/events/ref';
import { loadSubscriptions, startNotifications } from 'angled-core/events/sub';
import {
    loadProfiles,
    setLayout,
    loadAlias,
    loadProjects,
    activityLogger,
} from './events/app';
import { loadRoute } from './events/route';
import { getLayout } from './queries/app';
import projectslist from './components/projectslist';
import mapSelect from './components/map';
import splash from './components/splash';
import modal from './components/modal';
import notifier from './components/sub/notifier';
import saving from './components/form/saving';
import * as form from './components/form';
import { loadMaps, addProjectsLayer } from './events/map';
import { tr } from 'sdi/locale';
import { getDisplayType } from './queries/project';

const logger = debug('sdi:app');

const wrappedMain = (name: string, ...elements: NodeOrOptional[]) =>
    DIV(
        { className: `project ${getDisplayType()}` },
        modal(),
        header('angled-project', notifier),
        DIV({ className: `main ${name}` }, ...elements),
        footer()
    );

const renderProjectList = () => wrappedMain('home', projectslist());

const renderMapSelect = () => wrappedMain('map', mapSelect());

const renderSelectForm = () => wrappedMain('form', form.renderSelectForm());

const renderDisplayForm = () => wrappedMain('form', form.renderDisplayForm());

const renderSplash = () => wrappedMain('splash', splash());

const renderSaving = () => wrappedMain('splash saving', saving());

const renderMain = () => {
    // weird, but TS 3.7.2 complains if switch on `getLayout()` - pm
    const layout = getLayout();
    switch (layout) {
        case 'Splash':
            return renderSplash();
        case 'Home':
            return renderProjectList();
        case 'MapSelect':
            return renderMapSelect();
        case 'FormSelect':
            return renderSelectForm();
        case 'Form':
            return renderDisplayForm();
        case 'Saving':
            return renderSaving();
    }
};

const effects = (initialRoute: string[]) => () => {
    tr.init_edited();
    loadProjects()
        .then(loadUniverse)
        .then(() => setLayout('Home'))
        .then(loadProfiles)
        .then(() => loadRoute(initialRoute))
        .then(() => {
            loadMaps();
            addProjectsLayer();
        })
        .then(loadAllRefsDebug)
        .then(loadSubscriptions); // for the time being rather log an error than stopping the app
    loadAllBaseLayers(getApiUrl(`wmsconfig/`));
    loadAllDatasetMetadata();
    loadAlias();
    startNotifications();
    activityLogger(langAction(getLang()));
    activityLogger(visitAction());
};

const app = (initialRoute: string[]) =>
    loop('project-app', renderMain, effects(initialRoute));
export default app;

logger('loaded');
