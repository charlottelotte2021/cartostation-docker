/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { defaultProject, defaultApp } from './shape';
import { defaultUniverse } from 'angled-core/shape/universe';
import { defaultRef } from 'angled-core/shape/ref';
import { defaultUnitState } from 'angled-core/shape/ui';
import { defaultMapState } from 'angled-core/shape/map';
import { defaultSub } from 'angled-core/shape/sub';
import 'angled-core/locale';

import 'sdi/polyfill';
import { source, AppConfigIO, getMessage } from 'sdi/source';
import { IShape, configure, defaultShape } from 'sdi/shape';

import App from './app';
import { displayException } from 'sdi/app';

const logger = debug('sdi:index');

export const main = (SDI: any) => {
    AppConfigIO.decode(SDI).fold(
        errors => {
            const textErrors = errors.map(e => getMessage(e.value, e.context));
            displayException(textErrors.join('\n'));
        },
        config => {
            const initialState: IShape = {
                'app/codename': 'angled-project',
                ...defaultShape(config),
                ...defaultUniverse(),
                ...defaultRef(),
                ...defaultProject(),
                ...defaultUnitState(),
                ...defaultMapState(),
                ...defaultApp(),
                ...defaultSub(),
                'app/lang': 'fr',
                'data/alias': [],


                // profile
                'data/profiles': [],
                'profile/current/name': null,
                'profile/current/page': null,
            };

            try {
                const start = source<IShape>(['app/lang']);
                const store = start(initialState);
                configure(store);
                const app = App(SDI.args)(store);
                logger('start rendering');
                app();
            } catch (err) {
                displayException(`${err}`);
            }
        }
    );
};

logger('loaded');
