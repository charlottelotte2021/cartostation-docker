import { Project, InformationUnitName, UnitData, Unit } from 'angled-core/ui';
// import { Contact, Site, Nova, Address, FundingOrg, Team } from 'angled-core/ref';
import { MessageRecord, RemoteResource, remoteNone } from 'sdi/source';

import { Timeline, UnitHistory } from '../remote/timeline';
import { Nullable, Collection } from 'sdi/util';
import { ModalStatus } from 'sdi/components/modal';
import { SearchType } from '../components/projectslist/search';
import { DisplayType } from '../components/form/display';

export type RemovedUnitIdentifier = [Unit['unit'], number];

declare module 'sdi/shape' {
    export interface IShape {
        'data/projects': RemoteResource<Project[]>;

        'component/project/new/name': Nullable<MessageRecord>;
        'component/project/new/add-project-hidden': boolean;

        'component/project/edit/edit-project-hidden': boolean;

        'component/project/search/term': Nullable<string>;
        'component/project/search/unit': Nullable<SearchType>;

        'project/current': Nullable<number>;

        'component/project/form/stage': UnitData[]; // etat des modifs en cours (niveau pjt)
        'component/project/form/stage-removed': RemovedUnitIdentifier[]; // etat des modifs en cours (niveau pjt)
        'component/project/form/stage-audience': number[]; // store audiences to apply to staged units
        'component/project/form/commit': number[];
        'component/project/form/removed': RemovedUnitIdentifier[];

        'component/project/form/selected-unit': Nullable<InformationUnitName>;
        'component/project/form/write/domain': Nullable<number>;

        'component/project/form/modal/status': Collection<ModalStatus>;

        'component/project/timeline': RemoteResource<Timeline>;
        'component/project/timeline-unit': RemoteResource<UnitHistory>;
        'component/project/timeline/project': RemoteResource<Project>;
        'component/project/timeline/window': Nullable<number>;
        'component/project/timeline/current-id': Nullable<number>;

        'component/project/data/nova': Collection<RemoteResource<object>>;

        'component/project/display/type': DisplayType;
        'component/project/saving/error': Nullable<string>;
    }
}

export const defaultProject = () => ({
    'data/projects': remoteNone,
    'component/project/new/name': null,
    'component/project/new/add-project-hidden': true,
    'component/project/edit/edit-project-hidden': true,
    'component/project/search/term': null,
    'component/project/search/unit': 'name' as SearchType,
    'project/current': null,
    'component/project/form/manip': null,
    'component/project/form/stage': [],
    'component/project/form/stage-removed': [],
    'component/project/form/stage-audience': [],
    'component/project/form/commit': [],
    'component/project/form/selected-unit': null,
    'component/project/form/write/domain': null,
    'component/project/minimap': {},

    'component/project/timeline': remoteNone,
    'component/project/timeline-unit': remoteNone,
    'component/project/timeline/project': remoteNone,
    'component/project/timeline/window': null,
    'component/project/timeline/current-id': null,
    'component/project/timeline/current-time': null,

    'component/project/form/removed': [],
    'component/project/form/modal/status': {},
    'component/project/data/nova': {},
    'component/project/display/type': 'reader' as DisplayType,
    'component/project/saving/error': null,

});
