/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {
    MessageRecord,
    IUser,
    IServiceBaseLayers,
    RemoteResource,
    FeatureCollection,
} from 'sdi/source';
import { TableState } from 'sdi/components/table2';
import { IDataTable } from 'sdi/components/table';
import { ButtonComponent } from 'sdi/components/button';
import {
    IMapViewData,
    IMapScale,
    Interaction,
    PrintRequest,
    PrintResponse,
} from 'sdi/map';
import { Collection, Nullable } from 'sdi/util';

import {
    AppLayout,
    ILegend,
    IMenuData,
    IMapNavigator,
    IToolWebServices,
    IPositioner,
    IShare,
} from './types';
import { PrintProps, PrintState } from '../components/print';
// import { HarvestedLayer } from 'mdq/src/components/query';
import { IToolGeocoder } from 'sdi/ports/geocoder';
import { DataSet, DataSetLevel, ExtraLayerInfo } from '../remote';
import { PlotTableCol, PlotTableSort } from '../components/plot';

declare module 'sdi/shape' {
    export interface IShape {
        'app/layout': AppLayout[];
        'app/current-map': Nullable<number>;
        'app/current-baselayer': string;
        // 'app/current-layer': Nullable<string>;
        'app/current-feature': number[];
        'app/map-ready': boolean;
        'app/route': string[];

        'component/legend': ILegend;
        'component/menu': IMenuData;
        'component/table': TableState;
        'component/table/extract': IDataTable;
        'component/mapnavigator': IMapNavigator;
        'component/legend/show-wms-legend': boolean;
        'component/legend/webservices': IToolWebServices;
        'component/legend/geocoder': IToolGeocoder;
        'component/legend/positioner': IPositioner;
        'component/legend/share': IShare;
        'component/button': ButtonComponent;
        'component/print': PrintState;
        'component/bookmark/current-index': Nullable<number>;
        'component/layers/select': boolean[];
        'component/layers/info': ExtraLayerInfo[];
        'component/plot/table/sort': Nullable<[PlotTableCol, PlotTableSort]>;
        'component/legend/show-primary': boolean;
        'component/legend/show-secondary': boolean;
        
        'port/map/view': IMapViewData;
        'port/map/scale': IMapScale;
        'port/map/interaction': Interaction;
        'port/map/loading': MessageRecord[];
        'port/map/printRequest': PrintRequest<PrintProps | null>;
        'port/map/printResponse': PrintResponse<PrintProps | null>;
        'port/map/printToImage': boolean;

        'data/baselayers': IServiceBaseLayers;
        'remote/errors': Collection<string>;

        'data/user': IUser | null;
        'data/dataset': {
            main: RemoteResource<DataSet>;
            extra: RemoteResource<DataSet>;
        };
        'data/georef': {
            [k in DataSetLevel]: RemoteResource<FeatureCollection>;
        };
        'data/extra/layers': RemoteResource<FeatureCollection>[];
    }
}
