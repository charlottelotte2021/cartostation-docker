import * as debug from 'debug';
import * as io from 'io-ts';

import { getLang, getRoute, setLang, setRoute } from 'sdi/app';
import { tryNumber } from 'sdi/util';
import { index } from 'fp-ts/lib/Array';
import { loadMap } from './app';
import { MessageRecordLangIO } from 'sdi/source/io/message';
import { fromEither } from 'fp-ts/lib/Option';
import { scopeOption } from 'sdi/lib';

const logger = debug('sdi:route');

window.addEventListener('message', event =>
    io.Integer.decode(event.data).map(sid => {
        setRoute([getLang(), sid.toString()]);
        navigate();
    })
);

export const navigate = () =>
    scopeOption()
        .let(
            'lang',
            index(0, getRoute()).chain(lang =>
                fromEither(MessageRecordLangIO.decode(lang))
            )
        )
        .let('mid', index(1, getRoute()).chain(tryNumber))
        .map(({ lang, mid }) => {
            setLang(lang);
            loadMap(mid);
        });

logger('loaded');
