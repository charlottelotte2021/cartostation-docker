/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { dispatch, assign, query } from 'sdi/shape';
import {
    MessageRecord,
    remoteError,
    remoteLoading,
    remoteNone,
    remoteSuccess,
    remoteToOption,
} from 'sdi/source';

import { getApiUrl, getUserId } from 'sdi/app';
import { activity } from 'sdi/activity';
import { clearStreams } from 'sdi/geodata-stream';

import { AppLayout } from '../shape/types';
import {
    DataSetLevel,
    fetchAlias,
    fetchDataSet,
    fetchExtraLayerData,
    fetchExtraLayerInfo,
    fetchReferenceLayer,
    fetchUser,
} from '../remote';
import { addLayer, FetchData, removeLayerAll } from 'sdi/map';
import { mapName } from '../components/map';
import { right } from 'fp-ts/lib/Either';
import {
    getSecondaryLayerSyntInfo,
    getLayerDataOption,
    getPrimaryLayerSyntInfo,
    getExtraLayerData,
    getExtraLayerSyntInfo,
    noDisplayLayerSyntInfo,
} from '../queries/app';
import { PlotTableCol, PlotTableSort } from '../components/plot';
// import { addAppIdToFeature } from '../util/app';
// import {
//     getLayerDataFromInfo,
// } from '../queries/app';

const logger = debug('sdi:events/app');
export const activityLogger = activity('mdq');

// observe('data/maps',
//     () => fromNullable(getMapInfo())
//         .map(
//             info => info.attachments.forEach(
//                 aid => fetchAttachment(getApiUrl(`attachments/${aid}`))
//                     .then(a => attachments(s => s.concat([a]))))));

// observe('port/map/view',
//     view =>
//         fromNullable(view.extent)
//             .map(e =>
//                 mapStream(e, ({ uri, lid }, extent) =>
//                     loadLayerDataExtent(lid, uri, extent))));

// const layerInRange =
//     (info: ILayerInfo) => {
//         const low = fromNullable(info.minZoom).getOrElse(0);
//         const high = fromNullable(info.maxZoom).getOrElse(30);
//         const zoom = getView().zoom;
//         return info.visible && ((zoom > low) && (zoom < high));
//     };

// const whenInRange = fromPredicate(layerInRange);

export const loadUser = () =>
    getUserId().map(userId =>
        fetchUser(getApiUrl(`users/${userId}`)).then(user =>
            assign('data/user', user)
        )
    );

// const { markVisibilityPending, delVisibilityPending, isVisibilityPending } = (() => {
//     const register: Set<string> = new Set();

//     const hash = (a: string, b: string) => `${a}/${b}`
//     const markVisibilityPending = (
//         mid: string,
//         lid: string
//     ) => register.add(hash(mid, lid));

//     const delVisibilityPending = (
//         mid: string,
//         lid: string,
//     ) => register.delete(hash(mid, lid))

//     const isVisibilityPending = (
//         mid: string,
//         lid: string,
//     ) => register.has(hash(mid, lid))

//     return {
//         markVisibilityPending,
//         delVisibilityPending,
//         isVisibilityPending,
//     }
// })()

// const loadLayer = (
//     mapInfo: IMapInfo
// ) => (
//     layerInfo: ILayerInfo
// ) => {
//         fetchDatasetMetadata(getApiUrl(`metadatas/${layerInfo.metadataId}`))
//             .then((md) => {
//                 dispatch('data/datasetMetadata', (state) => {
//                     state[md.id] = md;
//                     return state;
//                 });

//                 addLayer(mapName,
//                     () => getSyntheticLayerInfo(layerInfo.id),
//                     () => getLayerData(md.uniqueResourceIdentifier));

//                 if (isNotNullNorUndefined(md.dataStreamUrl)) {
//                     addStream({ uri: md.uniqueResourceIdentifier, lid: layerInfo.id });
//                     dispatch('data/layers', (state) => {
//                         state[md.uniqueResourceIdentifier] = { type: 'FeatureCollection', features: [] };
//                         return state;
//                     });
//                     if (layerInfo.visible && layerInRange(layerInfo)) {
//                         fromNullable(getView().extent)
//                             .map((e) => {
//                                 loadLayerDataExtent(layerInfo.id, md.uniqueResourceIdentifier, e);
//                             });
//                     }
//                     else {
//                         markVisibilityPending(mapInfo.id, layerInfo.id)
//                     }
//                 }
//                 else {
//                     loadLayerData(md.uniqueResourceIdentifier);
//                 }
//             })
//             .catch(err => logger(`Failed to load MD ${layerInfo.metadataId}: ${err}`));
//     };

// const loadLayerDataExtent = (
//     layerId: string,
//     url: string,
//     bbox: Extent,
// ) =>
//     getSyntheticLayerInfo(layerId)
//         .map(({ info }) =>
//             whenInRange(info)
//                 .map((info) => {
//                     const title = getDatasetMetadataOption(info.metadataId).map(md => getMessageRecord(md.resourceTitle));
//                     pushStreamExtent(bbox, { lid: layerId, uri: url });
//                     title.map(updateLoadingAddOne);
//                     fetchLayer(`${url}?bbox=${bbox[0]},${bbox[1]},${bbox[2]},${bbox[3]}`)
//                         .then((layer) => {
//                             if (layer.features !== null) {
//                                 addFeaturesToLayer(mapName, info, layer.features);
//                                 dispatch('data/layers', (state) => {
//                                     if (url in state) {
//                                         state[url].features = state[url].features.concat(layer.features);
//                                     }
//                                     else {
//                                         state[url] = layer;
//                                     }
//                                     return state;
//                                 });
//                             }
//                             title.map(updateLoadingRemoveOne);
//                         })
//                         .catch((err) => {
//                             title.map(updateLoadingRemoveOne);
//                             logger(`Failed to load features at ${url} due to ${err}`);
//                             dispatch('remote/errors', state => ({ ...state, [url]: `${err}` }));
//                         });
//                 })
//         );

// const loadLayerData = (url: string) => {
//     logger(`loadLayerData(${url})`);
//     fetchLayer(url)
//         .then((layer) => {
//             dispatch('data/layers', (state) => {
//                 logger(`Put layer ${url} on state`);
//                 state[url] = layer;
//                 return state;
//             });
//         })
//         .catch((err) => {
//             logger(`Failed to load layer at ${url} due to ${err}`);
//             dispatch('remote/errors', state => ({ ...state, [url]: `${err}` }));
//         });
// };

// const loadMapFromInfo =
//     (info: IMapInfo, delay?: number) => {
//         const mapIsReady = mapReady();
//         logger(`loadMap ${info.id} ${mapIsReady} ${delay}`);
//         if (mapIsReady) {
//             removeLayerAll(mapName);
//             info.layers.forEach(l => notABookmarkLayer(l).map(loadLayer(info)));
//             addBookmarksToMap();
//         }
//         else {
//             const d = (delay !== undefined) ? (2 * delay) : 2;
//             setTimeout(() => loadMapFromInfo(info, d), d);
//         }
//     };

// const findMap = (mid: string) => fromNullable(getMap(mid));

// const attachments = dispatchK('data/attachments');

// const loadLinks =
//     (mid: string) =>
//         fetchLinks(getApiUrl(`map/links?mid=${mid}`))
//             .then((links) => {
//                 dispatch('data/links', data => ({ ...data, [mid]: links }));
//             });

export const setLayout = (l: AppLayout) => {
    logger(`setLayout ${AppLayout[l]}`);
    dispatch('app/layout', state => state.concat([l]));
};

export const signalReadyMap = () => dispatch('app/map-ready', () => true);

// export const loadMap =
//     () =>
//         fromNullable(getCurrentMap())
//             .map(mid =>
//                 findMap(mid)
//                     .foldL(
//                         () => {
//                             fetchMap(getApiUrl(`maps/${mid}`))
//                                 .then((info) => {
//                                     dispatch('data/maps', maps => maps.concat([info]));
//                                     loadMapFromInfo(info);
//                                 })
//                                 .then(() => loadLinks(mid))
//                                 .catch((response) => {
//                                     const user = query('app/user');

//                                     if (response.status === 403 && !user) {
//                                         window.location.assign(getRootUrl(`login/view/${mid}`));
//                                         return;
//                                     }
//                                 });

//                         },
//                         (info) => {
//                             loadLinks(mid);
//                             loadMapFromInfo(info);
//                         },
//                     )
//             );

// export const loadBaseLayer = (id: string, url: string) => {
//     fetchBaseLayer(url)
//         .then((bl) => {
//             dispatch('data/baselayers', state => ({ ...state, [id]: bl }));
//         });
// };

// export const loadAllBaseLayers = (url: string) => {
//     fetchBaseLayerAll(url)
//         .then((blc) => {
//             dispatch('data/baselayers', () => blc);
//         });
// };

// export const loadAllMaps =
//     () => fetchAllMaps(getApiUrl(`maps`))
//         .then(maps => assign('data/maps', maps));

export const loadAlias = (url: string) => {
    fetchAlias(url).then(alias => {
        dispatch('data/alias', () => alias);
    });
};

// export const loadCategories = (url: string) => {
//     fetchCategories(url)
//         .then((categories) => {
//             dispatch('data/categories', () => categories);
//         });
// };

export const setLayerVisibility = (_id: string, _visible: boolean) => {
    // const mid = getCurrentMap();
    // dispatch('data/maps', (maps) => {
    //     const mapInfo = maps.find(m => m.id === mid);
    //     if (mapInfo) {
    //         mapInfo.layers.forEach((l) => {
    //             if (l.id === id) {
    //                 l.visible = visible;
    //                 if (isVisibilityPending(mapInfo.id, l.id)) {
    //                     getMapExtent()
    //                         .map(e => mapStream(e, ({ uri, lid }, extent) =>
    //                             loadLayerDataExtent(lid, uri, extent)))
    //                     delVisibilityPending(mapInfo.id, l.id)
    //                 }
    //             }
    //         });
    //     }
    //     return maps;
    // });
};

export const setMapBaseLayer = (_id: string) => {
    // const mid = getCurrentMap();
    // dispatch('data/maps', (maps) => {
    //     const idx = maps.findIndex(m => m.id === mid);
    //     if (idx !== -1) {
    //         const m = maps[idx];
    //         m.baseLayer = id;
    //     }
    //     return maps;
    // });
};

export const setCurrentMap = (id: number) => {
    clearMap();
    assign('app/current-map', id);
};

// export const setCurrentLayer = (id: string) => {
//     assign('app/current-layer', id);
//     assign('app/current-feature', []);
//     dispatch('component/table', state => ({
//         ...state,
//         selected: -1,
//         loaded: 'none',
//     }));
// };

// export const setCurrentFeature =
//     (data: Feature | null) => dispatch('app/current-feature', () => data);

export const unsetCurrentFeature = () => assign('app/current-feature', []);

export const setCurrentFeatureById = (ids: number[]) => {
    assign('app/current-feature', ids);
};

export const clearMap = () => {
    removeLayerAll(mapName);
    assign('data/dataset', { main: remoteNone, extra: remoteNone });
    assign('app/current-map', null);
    assign('app/current-feature', []);
    dispatch('component/table', state => {
        state.selected = -1;
        return state;
    });
    clearStreams();
};

export const setPrintTitle = (customTitle: MessageRecord) =>
    dispatch('component/print', s => ({ ...s, customTitle }));

export const resetPrintTitle = () =>
    dispatch('component/print', s => ({ ...s, customTitle: null }));

// export const loadMetadata =
//     (mid: string) =>
//         fetchDatasetMetadata(getApiUrl(`metadatas/${mid}`))
//             .then(md =>
//                 dispatch('data/datasetMetadata', (state) => {
//                     state[md.id] = md;
//                     return state;
//                 }))
//             .catch(err => logger(`Failed to load MD ${mid}: ${err}`));

const mountDataSetLayer = (which: 'main' | 'extra') => {
    const fetchDisplay: FetchData = () =>
        right(getLayerDataOption(which, 'visible'));
    if (which === 'main') {
        const fetchNoDisplay: FetchData = () =>
            right(getLayerDataOption(which, 'unvisible'));
        addLayer(mapName, getPrimaryLayerSyntInfo, fetchDisplay);
        addLayer(mapName, noDisplayLayerSyntInfo, fetchNoDisplay);
    } else {
        addLayer(mapName, getSecondaryLayerSyntInfo, fetchDisplay);
    }
};

export const loadDataSet = (id: number, which: 'main' | 'extra') => {
    dispatch('data/dataset', s => ({ ...s, [which]: remoteLoading }));
    return fetchDataSet(id)
        .then(ds =>
            dispatch('data/dataset', s => ({
                ...s,
                [which]: remoteSuccess(ds),
            }))
        )
        .catch(err => remoteError(`${err}`))
        .then(() => mountDataSetLayer(which));
};

export const loadMap = (id: number) => {
    clearMap();
    setCurrentMap(id);
    loadDataSet(id, 'main').then(() => {
        remoteToOption(query('data/dataset')['main']).map(dataset => {
            if (dataset.extra !== null) {
                loadDataSet(dataset.extra, 'extra');
            }
        });
        addExtraLayers();
    });
};

const loadReferenceLayer = (level: DataSetLevel) => {
    dispatch('data/georef', refs => ({ ...refs, [level]: remoteLoading }));
    return fetchReferenceLayer(level)
        .then(data =>
            dispatch('data/georef', refs => ({
                ...refs,
                [level]: remoteSuccess(data),
            }))
        )
        .catch(err =>
            dispatch('data/georef', refs => ({
                ...refs,
                [level]: remoteError(`${err}`),
            }))
        );
};

export const loadReferenceLayers = () => {
    loadReferenceLayer('M');
    loadReferenceLayer('D');
    loadReferenceLayer('S');
};

export const setPlotTableSort = (col: PlotTableCol, sort: PlotTableSort) =>
    assign('component/plot/table/sort', [col, sort]);

export const toggleShowPrimary = () =>
    dispatch('component/legend/show-primary', s => !s);
export const toggleShowSecondary = () =>
    dispatch('component/legend/show-secondary', s => !s);

export const toggleOrtho = () =>
    dispatch('app/current-baselayer', s =>
        s === 'urbis.irisnet.be/urbis_gray'
            ? 'urbis.irisnet.be/urbis_ortho'
            : 'urbis.irisnet.be/urbis_gray'
    );

export const loadExtraLayerInfo = () =>
    fetchExtraLayerInfo()
        .then(infos => {
            assign('component/layers/info', infos);
            assign(
                'component/layers/select',
                infos.map(() => false)
            );
            assign(
                'data/extra/layers',
                infos.map((info, idx) => {
                    fetchExtraLayerData(info.id)
                        .then(layer =>
                            dispatch('data/extra/layers', state => {
                                state.splice(idx, 1, remoteSuccess(layer));
                                return state;
                            })
                        )
                        .catch(err =>
                            dispatch('data/extra/layers', state => {
                                state.splice(idx, 1, remoteError(`${err}`));
                                return state;
                            })
                        );

                    return remoteLoading;
                })
            );

            addExtraLayers();
        })
        .catch(err => logger(`Failed loadExtraLayerInfo`, err));

const addExtraLayers = () =>
    query('component/layers/info').forEach(info => {
        const fetch: FetchData = () => right(getExtraLayerData(info.id));
        addLayer(mapName, getExtraLayerSyntInfo(info), fetch);
    });

logger('loaded');
