/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { none, fromNullable } from 'fp-ts/lib/Option';

import tr, { fromRecord } from 'sdi/locale';
import { DIV, NODISPLAY, SPAN } from 'sdi/components/elements';
import {
    SelectRowHandler,
    TableDataRow,
    baseTable,
} from 'sdi/components/table';
import { scopeOption } from 'sdi/lib';

import { layerTableQueries } from '../../queries/table';
import { layerTableEvents, exportCSV } from '../../events/table';
import {
    getCurrentLayerInfo,
    getLayerData,
    getCurrentName,
} from '../../queries/app';
import {
    unsetCurrentFeature,
    setLayout,
    setCurrentFeature,
} from '../../events/app';
import { AppLayout } from '../../shape/types';
import { makeIcon } from '../button';
import { startExtract, stopExtract } from '../../events/map';
import { withExtract } from '../../queries/map';

const logger = debug('sdi:table/feature-collection');

const closeButton = makeIcon('close', 3, 'times');
const extractButton = makeIcon('toggle-off', 3, 'toggle-off');
const noExtractButton = makeIcon('toggle-on', 3, 'toggle-on');

const renderExtract = () =>
    withExtract().fold(
        DIV(
            { className: 'toggle' },
            DIV({ className: 'active' }, tr.mdq('extractOff')),
            extractButton(startExtract),
            DIV({ className: 'no-active' }, tr.mdq('extractOn'))
        ),
        () =>
            DIV(
                { className: 'toggle' },
                DIV({ className: 'no-active' }, tr.mdq('extractOff')),
                noExtractButton(stopExtract),
                DIV({ className: 'active' }, tr.mdq('extractOn'))
            )
    );

const renderExportCSV = () =>
    getCurrentLayerInfo()
        .map(s =>
            fromNullable(s.info).fold(NODISPLAY(), i =>
                fromNullable(i.layerInfoExtra).fold(NODISPLAY(), extra =>
                    extra.exportable
                        ? DIV(
                              { className: 'table-download' },
                              SPAN(
                                  {
                                      className: 'dl-item',
                                      onClick: () =>
                                          exportCSV(
                                              s.metadata,
                                              i.id,
                                              fromRecord(s.name),
                                              layerTableQueries.getKeys(),
                                              layerTableQueries.getData()
                                          ),
                                  },
                                  'csv'
                              )
                          )
                        : NODISPLAY()
                )
            )
        )
        .getOrElse(NODISPLAY());

const toolbar = () =>
    DIV(
        { className: 'table-toolbar', key: 'table-toolbar' },
        DIV({ className: 'table-title' }, getCurrentName().getOrElse('...')),
        DIV({ className: 'table-download' }, renderExtract()),
        DIV({ className: 'table-download' }, renderExportCSV()),
        closeButton(() => {
            unsetCurrentFeature();
            setLayout(AppLayout.MapFS);
        })
    );

const onRowSelect: SelectRowHandler = (row: TableDataRow) =>
    scopeOption()
        .let('info', getCurrentLayerInfo())
        .let('layer', ({ info }) =>
            getLayerData(info.metadata.uniqueResourceIdentifier).getOrElse(none)
        )
        .let('feature', ({ layer }) =>
            fromNullable(layer.features.find(f => f.id === row.from))
        )
        .map(({ feature }) => {
            setCurrentFeature(feature);
            setLayout(AppLayout.MapAndTableAndFeature);
        });

const base = baseTable(layerTableQueries, layerTableEvents);

const render = base({
    className: 'attr-select-wrapper',
    toolbar,
    onRowSelect,
});

export default render;

logger('loaded');
