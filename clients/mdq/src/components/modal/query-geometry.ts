// import * as debug from 'debug';
// import { fromNullable } from 'fp-ts/lib/Option';

// import { DIV, H2 } from 'sdi/components/elements';
// import { IMapOptions, create } from 'sdi/map';

// import {
//     GeometryType,
// } from 'sdi/source';
// import tr from 'sdi/locale';

// import { makeLabel } from '../button';
// import { getMapInfo, getCurrentBaseLayer } from 'mdq/src/queries/app'
// import { setGeometry, clearGeometry } from 'mdq/src/events/harvest';
// import { getGeometry } from 'mdq/src/queries/harvest';
// import { getView, getInteraction } from 'mdq/src/queries/map';
// import { closeModalQueryGeometry } from 'mdq/src/events/modal';
// import { ModalRender } from 'sdi/components/modal';
// import { viewEvents } from 'mdq/src/events/map';

// const logger = debug('sdi:write/geometry');

// const closeModalButton = makeLabel('close', 3, () => tr.core('close'));
// const confirmSelectButton = makeLabel('close', 3, () => tr.core('confirm'));

// const options: IMapOptions = {
//     getView,
//     element: null,
//     getBaseLayer: getCurrentBaseLayer,
//     getMapInfo: getMapInfo,

//     updateView: viewEvents.updateMapView,
//     setScaleLine: () => void 0,
// };

// const { mapSetTarget, registerTarget } = (function () {
//     const targeters: { [k: string]: (e: Element | null) => void } = {};

//     const registerTarget =
//         (name: string, t: (e: Element | null) => void) => {
//             targeters[name] = t;
//         };

//     const mapSetTarget =
//         (name: string, e: Element | null) =>
//             fromNullable(targeters[name]).map(f => f(e));

//     return { mapSetTarget, registerTarget };
// })();

// const { mapUpdate, mapInsert } = (function () {
//     const updaters: { [k: string]: () => void } = {};

//     const mapInsert = (name: string, u: () => () => void) => {
//         if (!(name in updaters)) {
//             updaters[name] = u();
//         }
//     };
//     const mapUpdate = (name: string) => fromNullable(updaters[name]).map(f => f());
//     return { mapInsert, mapUpdate };
// })();

// const attachMap =
//     (geometryType: GeometryType) =>
//         (element: Element | null) => {
//             mapInsert(geometryType, () => {
//                 const { update, setTarget, editable } = create(geometryType, {
//                     ...options,
//                     element,
//                 });

//                 editable({
//                     addFeature: (f) => setGeometry(f.geometry),
//                     setGeometry: () => void 0,
//                     getCurrentLayerId: () => 'query-geometry',
//                     getGeometryType: () => geometryType,
//                 }, getInteraction);

//                 registerTarget(geometryType, setTarget);
//                 return update;
//             });

//             mapSetTarget(geometryType, element);
//         };

// const renderInput =
//     (gt: GeometryType) =>
//         DIV({ className: 'map-wrapper-modal-content' },
//             DIV({
//                 key: `map-wrapper-query-geometry`,
//                 className: 'map-wrapper',
//                 ref: attachMap(gt),
//             }));

// const renderHeaderModal =
//     () => H2({}, '~draw');

// const renderFooterModal =
//     () => DIV({},
//         getGeometry().fold(
//             confirmSelectButton(() => void 0, 'disabled'),
//             () => confirmSelectButton(closeModalQueryGeometry),
//         ),
//         closeModalButton(() => {
//             clearGeometry();
//             closeModalQueryGeometry();
//         }));

// export const render: ModalRender = {
//     header: renderHeaderModal,
//     footer: renderFooterModal,
//     body: () => {
//         mapUpdate('Polygon');
//         return renderInput('Polygon');
//     },
// };

// logger('loaded');
