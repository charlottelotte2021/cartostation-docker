/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
// import { fromNullable } from 'fp-ts/lib/Option';
import { ReactNode } from 'react';

import { DIV, H1, H2, H3, INPUT, SPAN } from 'sdi/components/elements';
import {
    // getMessageRecord,
    LayerGroup,
    ILayerInfo,
    IMapInfo,
    // GeometryType,
} from 'sdi/source';
import tr, { fromRecord } from 'sdi/locale';
// import { translateMapBaseLayer } from 'sdi/util';
import { divTooltipLeft } from 'sdi/components/tooltip';

import queries from '../../queries/legend';
import { setPage } from '../../events/legend';
import {
    // setLayerVisibility,
    // setCurrentLayer,
    setLayout,
    toggleOrtho,
    toggleShowPrimary,
    toggleShowSecondary,
    unsetCurrentFeature,
} from '../../events/app';
import {
    getCurrentBaseLayerName,
    getDataSet,
    getLegendExtra,
    getMapInfoOption,
    getShowPrimary,
    getShowSecondary,
    secondaryLayerTemplateForLegend,
    makeTitle,
    // getDatasetMetadata,
    // getLayerData,
    // getCurrentBaseLayer,
} from '../../queries/app';
import legendItem from './legend-item';
import { AppLayout, LegendPage } from '../../shape/types';
import { ViewMessageKey } from '../../locale';
// import webservices from '../legend-tools/webservices';
import print from '../legend-tools/print';
import share from '../legend-tools/share';
// import location from '../legend-tools/location';
import measure from '../legend-tools/measure';
// import harvest from '../legend-tools/harvest';
// import { helpText } from 'sdi/components/helptext';
// import { notABookmarkLayer } from '../bookmark/index';
// import { getView } from 'mdq/src/queries/map';
import {
    zoomIn,
    zoomOut,
    enterFullScreen,
    toggleSumInteraction,
} from 'mdq/src/events/map';
// import { makeLabel } from '../button';
// import { startDrawing } from 'mdq/src/events/harvest';
import renderLayers from '../layers';

import plot from '../plot';
import { getInteractionMode } from 'mdq/src/queries/map';
import { none, some } from 'fp-ts/lib/Option';

const logger = debug('sdi:legend');

// const harvestPointButton = makeLabel('info', 2, () =>
//     tr.mdq('harvestInitiatePoint')
// );
// const harvestLineButton = makeLabel('info', 2, () =>
//     tr.mdq('harvestInitiateLine')
// );
// const harvestPolygonButton = makeLabel('info', 2, () =>
//     tr.mdq('harvestInitiatePolygon')
// );

interface Group {
    g: LayerGroup | null;
    layers: ILayerInfo[];
}

const groupItems = (layers: ILayerInfo[]) =>
    layers
        .slice()
        .reverse()
        .reduce<Group[]>((acc, info) => {
            const ln = acc.length;
            if (ln === 0) {
                return [
                    {
                        g: info.group,
                        layers: [info],
                    },
                ];
            }
            const prevGroup = acc[ln - 1];
            const cg = info.group;
            const pg = prevGroup.g;
            // Cases:
            // info.group == null && prevGroup.g == null => append
            // info.group != null && prevGroup.g != null && info.group.id == prevGroup.id => append
            if (
                (cg === null && pg === null) ||
                (cg !== null && pg !== null && cg.id === pg.id)
            ) {
                prevGroup.layers.push(info);
                return acc;
            }
            // info.group == null && prevGroup.g != null => new
            // info.group != null && prevGroup.g == null => new
            // info.group != null && prevGroup.g != null && info.group.id != prevGroup.id => new

            return acc.concat({
                g: cg,
                layers: [info],
            });
        }, []);

const renderLegendGroups = (groups: Group[]) =>
    groups.map((group, key) => {
        const layers = group.layers.filter(l => l.visible === true);

        if (layers.length === 0) {
            return DIV({ key }); // FIXME - we can do better than that
        }
        const items = layers.map(legendItem);
        if (group.g !== null) {
            return DIV(
                { key, className: 'legend-group named' },
                DIV(
                    { className: 'legend-group-title' },
                    fromRecord(group.g.name)
                ),
                DIV({ className: 'legend-group-items' }, ...items)
            );
        }
        return DIV({ key, className: 'legend-group anonymous' }, ...items);
    });

// type InfoRender = (info: ILayerInfo) => ReactNode;

// const branchInfo = (a: InfoRender, b: InfoRender) => (
//     c: boolean,
//     info: ILayerInfo
// ) => {
//     if (c) {
//         return a(info);
//     }
//     return b(info);
// };

// const dataActions = branchInfo(
//     (
//         info: ILayerInfo // normal
//     ) =>
//         DIV(
//             { className: 'layer-actions' },
//             divTooltipTopRight(
//                 tr.mdq('visible'),
//                 {},
//                 SPAN({
//                     className: info.visible ? 'visible' : 'not-visible',
//                     onClick: () => {
//                         setLayerVisibility(info.id, !info.visible);
//                     },
//                 })
//             ),
//             divTooltipTopRight(
//                 tr.mdq('tooltip:dataAccess'),
//                 {},
//                 SPAN({
//                     className: 'table',
//                     onClick: () => {
//                         setCurrentLayer(info.id);
//                         setLayout(AppLayout.MapAndTable);
//                     },
//                 })
//             )
//         ),
//     (
//         info: ILayerInfo // bookmark
//     ) =>
//         DIV(
//             { className: 'layer-actions bookmark' },
//             divTooltipTopRight(
//                 tr.mdq('visible'),
//                 {},
//                 SPAN({
//                     className: info.visible ? 'visible' : 'hidden',
//                     onClick: () => {
//                         setLayerVisibility(info.id, !info.visible);
//                     },
//                 })
//             )
//         )
// );

// const dataTitle = branchInfo(
//     (
//         info: ILayerInfo // normal
//     ) =>
//         DIV(
//             { className: 'layer-title' },
//             fromNullable(getDatasetMetadata(info.metadataId)).fold(
//                 info.id,
//                 md =>
//                     getLayerData(md.uniqueResourceIdentifier).fold<ReactNode>(
//                         err =>
//                             SPAN(
//                                 {
//                                     className: 'error',
//                                     title: err,
//                                 },
//                                 fromRecord(getMessageRecord(md.resourceTitle))
//                             ),
//                         () =>
//                             SPAN(
//                                 {},
//                                 fromRecord(getMessageRecord(md.resourceTitle))
//                             )
//                     )
//             )
//         ),
//     (
//         _info: ILayerInfo // bookmark
//     ) => DIV({ className: 'layer-title bookmark' }, tr.mdq('bookmarks'))
// );

// const dataItem = (info: ILayerInfo) =>
//     DIV(
//         { className: 'layer-item' },
//         dataActions(info.id !== bookmarkLayerID, info),
//         dataTitle(info.id !== bookmarkLayerID, info)
//     );

// const renderData = (groups: Group[]) =>
//     groups.map((group) => {
//         const items = group.layers.map(dataItem);
//         if (group.g !== null) {
//             return DIV(
//                 { className: 'legend-group named' },
//                 DIV(
//                     { className: 'legend-group-title' },
//                     fromRecord(group.g.name)
//                 ),
//                 DIV({ className: 'legend-group-items' }, ...items)
//             );
//         }
//         return DIV({ className: 'legend-group anonymous' }, ...items);
//     });

// const initiateHarvest = (gt: GeometryType) => {
//     startDrawing(gt);
//     setPage('harvest');
// };

// const renderHarvestBox = () =>
//     DIV(
//         { className: 'box box--harvest' },
//         H2({}, tr.mdq('harvestTitle')),
//         harvestPointButton(() => initiateHarvest('Point')),
//         harvestLineButton(() => initiateHarvest('LineString')),
//         harvestPolygonButton(() => initiateHarvest('Polygon'))
//     );

const switchItem = (
    p: LegendPage,
    tk: ViewMessageKey,
    currentPage: LegendPage
) => {
    return divTooltipLeft(
        tr.mdq(tk),
        {
            className: `switch-item switch-${p} ${
                p === currentPage ? 'active' : ''
            }`,
            onClick: () => {
                setLayout(AppLayout.MapAndInfo);
                unsetCurrentFeature();
                setPage(p);
            },
        },
        DIV({ className: 'picto' })
    );
};

const buttonSumToggle = () =>
    divTooltipLeft(
        tr.mdq('toggleSum'),
        {
            className: `switch-item switch-sum ${
                getInteractionMode() === 'select' ? 'active' : ''
            }`,
            onClick: toggleSumInteraction,
        },
        DIV({ className: 'picto' })
    );

const buttonBaseMap = () =>
    divTooltipLeft(
        tr.mdq('ortho'),
        {
            className: `switch-item switch-basemap ${
                getCurrentBaseLayerName() === 'urbis.irisnet.be/urbis_ortho'
                    ? 'active'
                    : ''
            }`,
            onClick: toggleOrtho,
        },
        DIV({ className: 'picto' })
    );

const buttonZoomIn = () =>
    divTooltipLeft(
        tr.core('zoomIn'),
        {
            className: 'switch-item switch-zoom-in',
            onClick: zoomIn,
        },
        DIV({ className: 'picto' })
    );

const buttonZoomOut = () =>
    divTooltipLeft(
        tr.core('zoomOut'),
        {
            className: 'switch-item switch-zoom-out',
            onClick: zoomOut,
        },
        DIV({ className: 'picto' })
    );

const buttonFullscreen = () =>
    divTooltipLeft(
        tr.core('fullscreen'),
        {
            className: 'switch-item switch-fullscreen',
            onClick: enterFullScreen,
        },
        DIV({ className: 'picto' })
    );

// const buttonNorth = () =>
//     divTooltipLeft(
//         tr.core('north'),
//         {
//             className: 'switch-item switch-north',
//             onClick: resetRotate,
//         },
//         DIV({
//             className: 'picto',
//             style: {
//                 transform: `rotate(${getView().rotation}rad)`,
//             },
//         })
//     );

export const switcher = () => {
    const currentPage = queries.currentPage();
    return DIV(
        'switcher-wrapper',
        DIV(
            'switcher',
            switchItem('info', 'tooltip:info', currentPage),
            // switchItem('data', 'tooltip:dataAndSearch', currentPage),
            // switchItem('base-map', 'tooltip:base-map', currentPage),
            switchItem('print', 'tooltip:print', currentPage),
            switchItem('share', 'tooltip:ishare', currentPage),
            switchItem('measure', 'tooltip:measure', currentPage),
            // switchItem('locate', 'tooltip:locate', currentPage)
            buttonSumToggle(),
            buttonBaseMap()
        ),
        DIV(
            'map-tools',
            // buttonNorth(),
            buttonZoomIn(),
            buttonZoomOut(),
            buttonFullscreen()
        )
    );
};

// const wmsLegend = () => {
//     const bl = getCurrentBaseLayer();
//     if (null === bl || undefined === bl) {
//         return NODISPLAY();
//     }
//     if (queries.displayWMSLegend()) {
//         const tl = translateMapBaseLayer(bl);
//         const lyrs = tl.params.LAYERS.split(',').reverse();
//         const legends = lyrs.map(lyr =>
//             DIV(
//                 {
//                     className: 'wms-legend-item',
//                     key: `legend-image-${tl.url}-${lyr}`,
//                 },
//                 IMG({
//                     src: `${tl.url}?SERVICE=WMS&REQUEST=GetLegendGraphic&VERSION=${tl.params.VERSION}&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=${lyr}`,
//                 })
//             )
//         );

//         return DIV(
//             { className: 'wms-legend-wrapper' },
//             DIV(
//                 {
//                     className: 'wms-legend-switch opened',
//                     onClick: () => setWMSLegendVisible(false),
//                 },
//                 tr.mdq('wmsLegendHide')
//             ),
//             ...legends
//         );
//     }

//     return DIV(
//         { className: 'wms-legend-wrapper' },
//         DIV(
//             {
//                 className: 'wms-legend-switch closed',
//                 onClick: () => setWMSLegendVisible(true),
//             },
//             tr.mdq('wmsLegendDisplay')
//         )
//     );
// };

const wrapTheWholeThing = (...es: ReactNode[]) =>
    DIV({ className: 'sidebar-right map-legend' }, ...es);

// const getYear = () =>
//     getDataSet('main')
//         .map(ds => ds.year.toString())
//         .getOrElse('');

const renderMapInfoHeader = (mapInfo: IMapInfo, p: LegendPage) =>
    DIV(
        {
            className: `sidebar-header legend-header-${p}`,
        },
        H1({}, fromRecord(mapInfo.title))
    );

const renderPrimaryLegend = (mapInfo: IMapInfo) =>
    DIV(
        'legend-top-box primary',
        DIV(
            'legend-control',
            INPUT({
                type: 'checkbox',
                checked: getShowPrimary(),
                onChange: toggleShowPrimary,
            }),
            DIV('extra-title', fromRecord(mapInfo.title))
        ),
        ...renderLegendGroups(groupItems(mapInfo.layers.slice(1, 1 + 1)))
    );

const renderNoDisplayLegend = () =>
    getDataSet('main')
        .chain(ds =>
            ds.entries.filter(e => e.flag !== 'NN').length > 0 ? some(0) : none
        )
        .map(() =>
            DIV(
                'legend-item no-display',
                DIV('item-style'),
                DIV('item-label', SPAN('', tr.mdq('noDisplay')))
            )
        );

const renderExtraLegend = () =>
    getLegendExtra().map(extras =>
        DIV(
            'legend-item extra',
            ...extras.map(extra =>
                DIV('extra-legend', H3('', extra.label), DIV('', extra.value))
            )
        )
    );

const renderSecondaryLegend = () =>
    getDataSet('extra').map(ds =>
        DIV(
            'legend-top-box secondary',
            DIV(
                'legend-control',
                INPUT({
                    type: 'checkbox',
                    checked: getShowSecondary(),
                    onChange: toggleShowSecondary,
                }),
                DIV('extra-title', fromRecord(makeTitle(ds)))
            ),
            ...renderLegendGroups(
                groupItems([secondaryLayerTemplateForLegend(ds)])
            )
        )
    );

const renderLegend = (mapInfo: IMapInfo) =>
    DIV(
        { className: 'sidebar-main styles-block' },
        H2({}, tr.mdq('mapLegend')),
        renderPrimaryLegend(mapInfo),
        renderNoDisplayLegend(),
        renderSecondaryLegend(),
        renderExtraLegend()
    );

// const renderLayers = (_mapInfo: IMapInfo) => DIV('layers-block', '~LAYERS')
const renderPlot = () => DIV('plot-block', plot());

const renderMapInfo = (mapInfo: IMapInfo) =>
    wrapTheWholeThing(
        renderMapInfoHeader(mapInfo, 'info'),
        // DIV({ className: 'sidebar-main legend-main' }, info()),
        renderLegend(mapInfo),
        renderLayers(mapInfo),
        renderPlot()
    );

const legend = () => {
    const currentPage = queries.currentPage();
    return getMapInfoOption().map(mapInfo => {
        switch (currentPage) {
            case 'info':
                return renderMapInfo(mapInfo);

            case 'measure':
                return wrapTheWholeThing(
                    renderMapInfoHeader(mapInfo, 'measure'),
                    measure()
                );
            case 'print':
                return wrapTheWholeThing(
                    renderMapInfoHeader(mapInfo, 'print'),
                    print()
                );
            case 'share':
                return wrapTheWholeThing(
                    renderMapInfoHeader(mapInfo, 'share'),
                    share()
                );
        }
    });
};

export default legend;

logger('loaded');
