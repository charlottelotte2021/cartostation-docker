import { fromPredicate } from 'fp-ts/lib/Option';

import {
    FeatureCollection,
    Feature,
    getFeatureProp,
    MessageRecord,
    ILayerInfo,
    Inspire,
} from 'sdi/source';
import { DIV, SPAN, H3 } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { helpText } from 'sdi/components/helptext';
import { divTooltipTopRight } from 'sdi/components/tooltip';

import { viewEvents, startPointerPosition } from '../../events/map';
import { openBookmarkModal } from '../../events/modal';
import {
    addBookmark,
    addBookmarkFromMark,
    removeBookmark,
    setBookmarkIndex,
} from '../../events/bookmark';
import { getBookmarks } from '../../queries/bookmark';

import { makeLabelAndIcon } from '../button';

export interface BookmarkProperties {
    name: string;
}

export const bookmarkLayerID = '__bookmarks_info__';
export const bookmarkMetadataID = '__bookmarks_meta__';
export const bookmarkLayerName: MessageRecord = {
    en: 'Bookmarks',
    fr: 'Repères',
    nl: 'Markeerstiften',
};

export const notABookmarkMetadata = fromPredicate<Inspire>(
    md => md.id !== bookmarkMetadataID
);

export const notABookmarkLayer = fromPredicate<ILayerInfo>(
    info => info.id !== bookmarkLayerID
);

export const bookmarkLayerInfo: ILayerInfo = {
    id: bookmarkLayerID,
    metadataId: bookmarkMetadataID,
    visible: true,
    featureViewOptions: { type: 'default' },
    style: {
        kind: 'point-simple',
        marker: {
            codePoint: 0xf005,
            color: 'black',
            size: 18,
        },
        label: {
            propName: { fr: 'name', nl: 'name', en: 'name' },
            color: 'black',
            size: 10,
            align: 'center',
            baseline: 'bottom',
            resLimit: 50,
            yOffset: -12,
        },
    },
    group: null,
    legend: null,
    layerInfoExtra: null,
};

export const bookmarkMetadata: Inspire = {
    id: bookmarkMetadataID,
    uniqueResourceIdentifier: bookmarkLayerID,
    dataStreamUrl: null,
    resourceTitle: bookmarkLayerName,
    geographicBoundingBox: { east: 0, west: 0, south: 0, north: 0 },
    keywords: [],
    geometryType: 'Point',
    metadataDate: '',
    metadataPointOfContact: [],
    published: false,
    resourceAbstract: { fr: '', nl: '', en: '' },
    responsibleOrganisation: [],
    temporalReference: { revision: '' },
    topicCategory: [],
};

export const defaultBookmarks = (): FeatureCollection => ({
    features: [],
    type: 'FeatureCollection',
});

const renderBookmark = (f: Feature, idx: number) =>
    DIV(
        {
            className: 'layer-item bookmark',
        },
        DIV(
            { className: 'layer-actions bookmark' },

            divTooltipTopRight(
                tr.mdq('zoomOnFeature'),
                {},
                SPAN({
                    className: 'bookmark-pos',
                    onClick: () =>
                        viewEvents.updateMapView({
                            dirty: 'geo/feature',
                            feature: f,
                        }),
                })
            ),
            divTooltipTopRight(
                tr.mdq('remove'),
                {},
                SPAN({
                    className: 'bookmark-remove',
                    onClick: () =>
                        removeBookmark(getFeatureProp(f, 'name', '~')),
                })
            ),
            divTooltipTopRight(
                tr.mdq('editBookmark'),
                {},
                SPAN({
                    className: 'bookmark-edit',
                    onClick: () => {
                        setBookmarkIndex(idx);
                        openBookmarkModal();
                    },
                })
            )
        ),
        SPAN({ className: 'bookmark-name' }, getFeatureProp(f, 'name', '~'))
    );

const addBookmarkBtn = makeLabelAndIcon('add', 2, 'star', () =>
    tr.mdq('addBookmark')
);

export const render = () =>
    DIV(
        {
            className: 'bookmark',
        },
        H3({}, tr.mdq('bookmarks')),
        helpText(tr.mdq('helptext:bookmark')),
        ...getBookmarks().features.map(renderBookmark),
        addBookmarkBtn(() =>
            addBookmarkFromMark().mapLeft(() =>
                startPointerPosition(addBookmark)
            )
        )
    );

export default render;
