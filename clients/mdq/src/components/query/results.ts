import { getHarvests } from 'mdq/src/queries/harvest';
import { DIV, TD, TH, TR, TABLE, THEAD, TBODY } from 'sdi/components/elements';
import { HarvestedLayer, HarvestedLayerLoaded } from './index';
import { Feature, getMessageRecord } from 'sdi/source';
import { fromNullable } from 'fp-ts/lib/Option';
import { getDatasetMetadataOption } from 'mdq/src/queries/app';
import { fromRecord } from 'sdi/locale';
// import mapInfo from '../map-info';

const renderTitle = (mid: string) =>
    DIV(
        { className: 'layer-title' },
        getDatasetMetadataOption(mid).fold(mid, md =>
            fromRecord(getMessageRecord(md.resourceTitle))
        )
    );

const renderTable = (h: HarvestedLayerLoaded) => {
    const features = h.data.features;
    if (features.length === 0) {
        return DIV({});
    }
    const first = features[0];
    const props = first.properties;
    if (props === null) {
        return DIV({});
    }
    const keys = Object.keys(props);
    const header = TR({}, ...keys.map(k => TH({}, k)));

    const renderRow = (f: Feature) => {
        const data = f.properties || {};
        const cells = keys.map(k =>
            TD(
                {},
                fromNullable(data[k]).fold('-', v => `${v}`)
            )
        );
        return TR({}, ...cells);
    };
    const rows = features.map(renderRow);

    return DIV(
        { className: 'result-table' },
        // renderTitle(h.metadataId),
        TABLE(
            { className: 'results' },
            // CAPTION({}, renderTitle(h.metadataId)),
            THEAD(
                {},
                TR({}, TD({ colSpan: 10 }, renderTitle(h.metadataId))),
                header
            ),
            TBODY({}, ...rows)
        )
    );
};

// const renderMapInfo =
//     (info: IMapInfo) =>
//         DIV({}, renderTitle(fromRecord(info.title)));

const renderHarvest = (h: HarvestedLayer) => {
    switch (h.tag) {
        case 'initial':
            return DIV({ key: h.metadataId }, 'initial');
        case 'loading':
            return DIV({ key: h.metadataId }, 'loading');
        case 'error':
            return DIV({ key: h.metadataId }, h.error);
        case 'loaded':
            return DIV({ key: h.metadataId }, renderTable(h));
    }
};

export const renderResults = () =>
    DIV({ className: 'results-wrapper' }, getHarvests().map(renderHarvest));
