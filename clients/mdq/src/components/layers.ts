import { DIV, H2, INPUT } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { IMapInfo } from 'sdi/source';
import { toggleSelectLayer } from '../events/map';
import { getExtraLayerInfo } from '../queries/app';
import { isLayerSelected } from '../queries/map';
import { ExtraLayerInfo } from '../remote';

// type Line = {
//     tag: 'line';
//     color: string;
// }

// type Rect = {
//     tag: 'rect';
//     color: string;
// }

// const line = (color: string): Line => ({ tag: 'line', color })
// const rect = (color: string): Rect => ({ tag: 'rect', color })

// type Shape = Line | Rect

const renderIcon = (info: ExtraLayerInfo) =>
    DIV(
        `shape-wrapper ${info.geometry_type}`,
        DIV({
            className: 'shape-inner',
            style: {
                backgroundColor: info.color,
            },
        })
    );

const renderSelect = (id: number) =>
    INPUT({
        type: 'checkbox',
        checked: isLayerSelected(id),
        onChange: () => toggleSelectLayer(id),
    });

const renderLayer = (info: ExtraLayerInfo) =>
    DIV(
        {
            key: info.id,
            className: 'layer-item',
        },
        renderSelect(info.id),
        renderIcon(info),
        DIV('label', fromRecord(info.name))
    );

// export const theLayers: [Shape, MessageRecord][] = [
//     [line('orange'), { fr: 'communes', nl: '' }],
//     [line('green'), { fr: 'première couronne', nl: '' }],
//     [line('black'), { fr: 'grands axes', nl: '' }],
//     [line('grey'), { fr: 'ilôts', nl: '' }],
//     [line('brown'), { fr: 'zone Canal', nl: '' }],
//     [line('purple'), { fr: 'zone Feder', nl: '' }],
//     [rect('grey'), { fr: 'chemins de fer', nl: '' }],
//     [rect('blue'), { fr: 'zones d\'eau', nl: '' }],
//     [rect('green'), { fr: 'espaces verts', nl: '' }],
// ]

export const renderLayers = (_mapInfo: IMapInfo) =>
    DIV(
        {
            className: 'layers-block',
        },
        H2('', tr.mdq('extra-layers')),
        getExtraLayerInfo().map(renderLayer)
    );

export default renderLayers;
