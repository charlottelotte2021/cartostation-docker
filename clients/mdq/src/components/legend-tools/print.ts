/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import tr from 'sdi/locale';
import { A, DIV, H2, H3 } from 'sdi/components/elements';
import { IMapInfo } from 'sdi/source';
import { uniqId } from 'sdi/util';
import { helpText } from 'sdi/components/helptext';

import {
    getDataSet,
    getMapInfoOption,
    getSelectedFeatures,
} from '../../queries/app';
import { setLayout } from '../../events/app';
import { setPrintRequest, setPrintToImage } from '../../events/map';
import { PrintProps } from '../print';
import { applySpec, getResolution } from '../print/template';
import { AppLayout } from '../../shape/types';
import { DataSet } from 'mdq/src/remote';
import { getApiUrl, getLang } from 'sdi/app';
import { getInteractionMode, getPrintToImage } from 'mdq/src/queries/map';
import { none, some } from 'fp-ts/lib/Option';
import { renderCheckbox } from 'sdi/components/input';

const logger = debug('sdi:tool-print');

const renderButton = (label: string, props: PrintProps) =>
    DIV(
        {
            className: props.orientation,
            onClick: () => {
                const resolution = getResolution(props.template);
                applySpec(props.template)('map', spec => spec.rect).map(
                    ({ width, height }) => {
                        setLayout(AppLayout.Print);
                        const id = uniqId();
                        setPrintRequest({
                            id,
                            width,
                            height,
                            resolution,
                            props,
                        });
                    }
                );
            },
        },
        label
    );

const choiceA4 = () =>
    DIV(
        { className: 'print-format' },
        renderButton('A4', {
            template: 'a4/landscape',
            format: 'a4',
            orientation: 'landscape',
        }),
        renderButton('A4', {
            template: 'a4/portrait',
            format: 'a4',
            orientation: 'portrait',
        })
    );

// const choiceA0 =
//     () => DIV({ className: 'print-format' },
//         renderButton('A0', {
//             template: 'a0/landscape',
//             format: 'a0',
//             orientation: 'landscape',
//         }),
//         renderButton('A0', {
//             template: 'a0/portrait',
//             format: 'a0',
//             orientation: 'portrait',
//         }));

// const renderExportCsvBtn = () =>
//     makeLabel('export', 2, () => tr.core('csvExtention'));

const makeLink = (label: string, href: string) =>
    DIV('export', A({ href, target: '_blank' }, label));

const renderSelectExport = (
    ds: DataSet,
    label: string,
    exportType: 'shapefile' | 'table' | 'geojson'
) => {
    if (getInteractionMode() !== 'select') {
        return none;
    }
    const selection = getSelectedFeatures();
    if (selection.length === 0) {
        return none;
    }
    const qs = selection.map(id => `filter=${id}`).join('&');
    return some(
        makeLink(
            `${label} (${tr.mdq('selection')})`,
            getApiUrl(`geodata/mdq/${exportType}/${ds.id}/${getLang()}?${qs}`)
        )
    );
};

const renderExportItem = (
    ds: DataSet,
    label: string,
    exportType: 'shapefile' | 'table' | 'geojson'
) =>
    DIV(
        'export-block',
        makeLink(
            label,
            getApiUrl(`geodata/mdq/${exportType}/${ds.id}/${getLang()}`)
        ),
        renderSelectExport(ds, label, exportType)
    );

const renderExportTool = () =>
    getDataSet('main').map(dataset =>
        DIV(
            'tool export',
            H2({}, tr.core('export')),
            helpText(tr.mdq('helptext:exportTool')),
            DIV(
                { className: 'tool-body' },
                renderExportItem(dataset, 'ESRI Shapefile', 'shapefile'),
                renderExportItem(dataset, 'Microsoft Excel', 'table'),
                renderExportItem(dataset, 'GeoJSON', 'geojson')
            )
        )
    );

const renderPrintBody = (_mapInfo: IMapInfo) =>
    DIV(
        { className: 'tool-body' },
        // renderCustom(mapInfo),
        DIV(
            { className: 'print-block' },
            H3({}, tr.mdq('printChoseOrientation')),
            choiceA4(),
            // H3({}, tr.mdq('printBigFormat')),
            // choiceA0(),
            renderCheckbox(
                'printToImage',
                () => tr.mdq('print-to-image'),
                setPrintToImage
            )(getPrintToImage())
        )
    );

const render = () =>
    getMapInfoOption().fold(DIV({}, 'Print: Somethin missing'), mapInfo =>
        DIV(
            { className: 'sidebar-main' },
            renderExportTool(),
            DIV(
                { className: 'tool print' },
                H2({}, tr.mdq('printMap')),
                helpText(tr.mdq('helptext:printMapTool')),
                renderPrintBody(mapInfo)
            )
        )
    );

export default render;

logger('loaded');
