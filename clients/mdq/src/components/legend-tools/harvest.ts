import { DIV, H2 } from 'sdi/components/elements';
import { stringToParagraphs } from 'sdi/util';
import tr, { Translated } from 'sdi/locale';

import { setLayout } from 'mdq/src/events/app';
import { setPage } from 'mdq/src/events/legend';
import { resetInteraction } from 'mdq/src/events/map';
import { AppLayout } from 'mdq/src/shape/types';
import { makeLabelAndIcon } from '../button';
import { getGeometryType } from 'mdq/src/queries/harvest';
import {
    clearHarvested,
    clearGeometry,
    clearGeometryType,
} from 'mdq/src/events/harvest';

const buttonCancel = makeLabelAndIcon('cancel', 2, 'times', () =>
    tr.core('cancel')
);

const renderTitle = () => H2({}, tr.mdq('harvestTitle'));

const wrapInfo = (t: Translated) =>
    DIV({ className: 'helptext' }, stringToParagraphs(t));

const renderInfo = () => {
    const gt = getGeometryType().getOrElse('Polygon');
    switch (gt) {
        case 'Point':
        case 'MultiPoint':
            return wrapInfo(tr.mdq('harvestInfoPoint'));
        case 'LineString':
        case 'MultiLineString':
            return wrapInfo(tr.mdq('harvestInfoLine'));
        case 'Polygon':
        case 'MultiPolygon':
            return wrapInfo(tr.mdq('harvestInfoPolygon'));
    }
};

export const renderHarvest = () =>
    DIV(
        { className: 'xxx' },
        renderTitle(),
        renderInfo(),
        buttonCancel(() => {
            clearHarvested();
            clearGeometry();
            clearGeometryType();
            setLayout(AppLayout.MapAndInfo);
            setPage('data');
            resetInteraction();
        })
    );

export default renderHarvest;
