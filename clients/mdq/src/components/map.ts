/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV, SPAN, H2 } from 'sdi/components/elements';
import { renderScaleline } from 'sdi/map/controls';
import {
    create,
    FeaturePath,
    IMapOptions,
    multiSelectOptions,
    singleSelectOptions,
} from 'sdi/map';
import tr, { formatNumber, fromRecord } from 'sdi/locale';
import { Feature, MessageRecord } from 'sdi/source';
import { markdown } from 'sdi/ports/marked';

import {
    getCurrentBaseLayer,
    getMapInfo,
    getCurrentFeature,
    getCurrentLayer,
    getCurrentLayerOpt,
    getSelectedFeatures,
    getSelectedFeaturesAsFeatures,
} from '../queries/app';
import {
    setCurrentFeatureById,
    unsetCurrentFeature,
    signalReadyMap,
} from '../events/app';
import {
    getInteraction,
    getInteractionMode,
    getLoading,
    getPrintRequest,
    getScaleLine,
    getView,
} from '../queries/map';
import {
    endMark,
    measureEvents,
    resetRotate,
    scalelineEvents,
    setExtractCollection,
    setPointerPosition,
    setPrintResponse,
    startMark,
    stopPointerPosition,
    storeOlMap,
    trackerEvents,
    updateLoading,
    viewEvents,
} from '../events/map';

import { getFeaturePropOption, tryNumber } from 'sdi/util';
import { catOptions } from 'fp-ts/lib/Array';
import { fromEither, none, some } from 'fp-ts/lib/Option';
import { scopeOption } from 'sdi/lib';
import { MessageRecordIO } from 'sdi/source/io/io';
import Layer from 'ol/layer/Layer';
import { FeatureLike } from 'ol/Feature';
import Source from 'ol/source/Source';

// import geocoder from './legend-tools/geocoder';
// import { setGeometry, endDrawing } from '../events/harvest';
// import { getGeometryType } from '../queries/harvest';
// import baseSwitch from './base-layer-switch';

const logger = debug('sdi:comp/map');
export const mapName = 'main-view';

const options: IMapOptions = {
    element: null,
    getBaseLayer: getCurrentBaseLayer,
    getMapInfo,
    getView,

    updateView: viewEvents.updateMapView,
    setScaleLine: scalelineEvents.setScaleLine,
    setLoading: updateLoading,
};

let mapSetTarget: (t: HTMLElement | null) => void;
let mapUpdate: () => void;

const selectFeature = (_lid: string, fid: string | number) =>
    tryNumber(fid).map(fid => setCurrentFeatureById([fid]));

const selectFeatures = (ps: FeaturePath[]) => {
    getCurrentLayerOpt().map(lid =>
        setCurrentFeatureById(
            catOptions(
                ps
                    .filter(({ layerId }) => layerId === lid)
                    .map(({ featureId }) => tryNumber(featureId))
            )
        )
    );
};

const onlyPrimaryLayer = (_feature: FeatureLike, layer: Layer<Source>) =>
    getCurrentLayer() === layer.get('id');

const clearSelection = () => {
    unsetCurrentFeature();
    // setLayout(AppLayout.MapAndInfo);
    // setPage('info');
};

const getSelected = () => ({
    featureId: getCurrentFeature(),
    layerId: getCurrentLayer(),
});

const getMultiSelected = () =>
    getCurrentLayerOpt()
        .map(layerId =>
            getSelectedFeatures().map(featureId => ({ layerId, featureId }))
        )
        .getOrElse([]);

// const prepareGeometryForHarvesting =
//     (f: Feature) => {
//         setGeometry(f.geometry);
//         endDrawing();
//         setLayout(AppLayout.Query);
//     };

const attachMap = () => (element: HTMLElement | null) => {
    // logger(`attachMap ${typeof element}`);
    if (!mapUpdate) {
        const {
            update,
            setTarget,
            selectable,
            enterable,
            measurable,
            trackable,
            extractable,
            markable,
            highlightable,
            printable,
            positionable,
            // editable,
            andThen,
        } = create(mapName, { ...options, element });
        mapSetTarget = setTarget;
        mapUpdate = update;
        signalReadyMap();

        selectable(
            multiSelectOptions({
                selectFeatures,
                getSelected: getMultiSelected,
                filter: onlyPrimaryLayer,
            }),
            getInteraction
        );

        enterable(
            singleSelectOptions({
                selectFeature,
                clearSelection,
                getSelected,
                filter: onlyPrimaryLayer,
            }),
            getInteraction
        );

        measurable(
            {
                updateMeasureCoordinates:
                    measureEvents.updateMeasureCoordinates,
                stopMeasuring: measureEvents.stopMeasure,
            },
            getInteraction
        );

        trackable(
            {
                resetTrack: trackerEvents.resetTrack,
                setCenter: center =>
                    viewEvents.updateMapView({ dirty: 'geo', center }),
                updateTrack: trackerEvents.updateTrack,
            },
            getInteraction
        );

        extractable(
            {
                setCollection: setExtractCollection,
            },
            getInteraction
        );

        markable({ startMark, endMark }, getInteraction);

        highlightable(getSelected);

        printable(
            {
                getRequest: getPrintRequest,
                setResponse: setPrintResponse,
            },
            getInteraction
        );

        positionable(
            {
                setPosition: setPointerPosition,
                stopPosition: stopPointerPosition,
            },
            getInteraction
        );

        // editable({
        //     addFeature: prepareGeometryForHarvesting,
        //     setGeometry: () => void 0,
        //     getCurrentLayerId: () => 'query-geometry',
        //     getGeometryType: () => getGeometryType().getOrElse('Polygon'),
        // }, getInteraction);

        andThen(storeOlMap);
    }
    if (element) {
        mapSetTarget(element);
    } else {
        mapSetTarget(null);
    }
};

const renderLoading = (ms: Readonly<MessageRecord[]>) =>
    DIV(
        {
            className: `loading-layer-wrapper ${
                ms.length === 0 ? 'hidden' : ''
            }`,
        },
        H2({}, tr.mdq('loadingData')),
        ms.map(r =>
            DIV(
                {
                    className: 'loading-layer',
                    key: fromRecord(r),
                },
                SPAN({ className: 'loader-spinner' }),
                fromRecord(r)
            )
        )
    );

const renderSum = () => {
    if (getInteractionMode() !== 'select') {
        return none;
    }
    const selection = getSelectedFeaturesAsFeatures();
    if (selection.length === 0) {
        // return none
        return DIV('sum-block', markdown(tr.mdq('helptext:sum')));
    }

    const getName = (f: Feature) =>
        getFeaturePropOption(f, 'name')
            .chain(name => fromEither(MessageRecordIO.decode(name)))
            .map(fromRecord);

    const getValue = (f: Feature) =>
        getFeaturePropOption(f, 'value').chain(tryNumber);

    const values: number[] = [];

    const items = selection.map(f =>
        scopeOption()
            .let('value', getValue(f))
            .let('name', getName(f))
            .map(({ name, value }) => {
                values.push(value);
                return SPAN('sum-item', `${name} (${formatNumber(value)}), `);
            })
    );
    const sum = values.reduce((acc, v) => acc + v, 0);
    const average = sum / values.length;
    const infos = [
        // DIV('sum-sum', `${tr.mdq('sum')}: ${formatNumber(sum)}`),
        DIV('sum-average', `${tr.mdq('average')}: ${formatNumber(average)}`),
    ];

    return some(
        DIV('sum-block', DIV('sum-items', ...items), DIV('sum-info', ...infos))
    );
};

const buttonNorth = () =>
    DIV({
        className: 'map-north',
        onClick: resetRotate,
        style: {
            transform: `rotate(${getView().rotation}rad)`,
        },
    });

const credits = () => DIV('credits', markdown(tr.mdq('credits')));

const render = () => {
    if (mapUpdate) {
        mapUpdate();
    }
    // FIXME - this is a hell of the same hack from view - pm
    const withPrintSize = (() => {
        const i = getInteraction();
        const req = getPrintRequest();
        if (i.label === 'print' && req !== null) {
            return {
                style: {
                    width: Math.round((req.width * req.resolution) / 25.4),
                    height: Math.round((req.height * req.resolution) / 25.4),
                },
            };
        }
        return {};
    })();

    return DIV(
        { className: `map-wrapper map-interaction-${getInteractionMode()}` },
        DIV({
            key: '__this_is_a_unique_map__',
            className: 'map',
            ref: attachMap(),
            ...withPrintSize,
        }),
        renderLoading(getLoading()),
        // geocoder(),
        // baseSwitch(),
        DIV(
            { className: 'map-footer' },
            buttonNorth(),
            renderScaleline(getScaleLine()),
            credits()
        ),
        renderSum()
    );
};

export default render;

logger('loaded');
