import { none, Option, some } from 'fp-ts/lib/Option';
import { setCurrentFeatureById, setPlotTableSort } from 'mdq/src/events/app';
import {
    getCurrentFeature,
    getPlotTableSort,
    getTableEntries,
} from 'mdq/src/queries/app';
import { makeIcon } from 'sdi/components/button';
import { DIV } from 'sdi/components/elements';
import { tooltipConfig } from 'sdi/components/tooltip';
import tr, { formatNumber } from 'sdi/locale';
import { PlotTableCol, PlotTableSort, TableEntry } from '.';

const evenness = (i: number) => (i % 2 === 1 ? 'odd' : 'even');
const selectedness = (s: boolean) => (s === true ? 'selected' : '');

const sortButton = (sort: Option<PlotTableSort>, action: () => void) =>
    sort
        .map(sort =>
            sort == 'asc'
                ? makeIcon(
                      'filter',
                      3,
                      'sort-down',
                      tooltipConfig(tr.core('sort'))
                  )
                : makeIcon(
                      'filter',
                      3,
                      'sort-up',
                      tooltipConfig(tr.core('sort'))
                  )
        )
        .getOrElse(
            makeIcon('filter', 3, 'sort', tooltipConfig(tr.core('sort')))
        )(action);

const renderSort = (col: PlotTableCol) =>
    getPlotTableSort()
        .map(([curCol, sort]) =>
            curCol !== col
                ? sortButton(none, () => setPlotTableSort(col, 'asc'))
                : sortButton(some(sort), () =>
                      setPlotTableSort(col, sort === 'asc' ? 'desc' : 'asc')
                  )
        )
        .getOrElse(sortButton(none, () => setPlotTableSort(col, 'asc')));

const renderHeader = () =>
    DIV(
        'table-header',
        DIV('table-header-cell', tr.mdq('name'), renderSort('name')),
        DIV('table-header-cell', tr.mdq('value'), renderSort('value'))
    );

const formatId = (n: number) => `id_${n}`;

const renderRow = (entry: TableEntry, index: number) =>
    DIV(
        {
            key: entry.id,
            id: formatId(entry.id),
            className: `table-row ${evenness(index)} ${selectedness(
                entry.selected
            )}`,
            onClick: () => setCurrentFeatureById([entry.id]),
        },
        DIV('table-cell', entry.name),
        DIV('table-cell', entry.flagged ? '-' : formatNumber(entry.value))
    );

let update: Option<() => void> = none;

const selectPosition = (element: HTMLElement | null) => {
    if (element) {
        element.style.position = 'relative';
        update = some(() => {
            const cid = getCurrentFeature();
            if (cid !== null) {
                const row = element.querySelector<HTMLElement>(
                    `#${formatId(cid)}`
                );
                if (row) {
                    const { height } = element.getBoundingClientRect();
                    // const rowRect = row.getBoundingClientRect()

                    if (
                        element.scrollTop > row.offsetTop ||
                        element.scrollTop + height < row.offsetTop
                    ) {
                        element.scrollTo({ top: row.offsetTop });
                    }
                }
            }
        });
    } else {
        update = none;
    }
};

const renderBody = () =>
    DIV(
        {
            ref: selectPosition,
            className: 'table-body',
        },
        getTableEntries().map(renderRow)
    );

export const renderTable = () => {
    update.map(u => u());

    return DIV(
        {
            className: 'table',
        },
        renderHeader(),
        renderBody()
    );
};
