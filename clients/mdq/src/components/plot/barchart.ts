import { createElement as svg, ReactSVGElement, SVGAttributes } from 'react';
import { getPercentile, uniqId } from 'sdi/util';
import { getBarchartEntries } from 'mdq/src/queries/app';
import { Adjust, BarchartEntry } from '.';
import { scopeOption } from 'sdi/lib';
import { DIV } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { plotWidth, plotHeight, plotPadding, axisWidth } from './index';

// /**
//  * events
//  */

// export const selectBarchartEntry = (_id: number) => {};

// /**
//  * end of events
//  */

type Coord = [number, number];
const coord = (x: number, y: number): Coord => [x, y];

const wrapSVG = (nodes: ReactSVGElement[], attrs?: SVGAttributes<SVGElement>) =>
    svg(
        'svg',
        {
            ...attrs,
        },
        ...nodes
    );

const line = (
    start: Coord,
    end: Coord,
    attrs?: SVGAttributes<SVGLineElement>
) =>
    svg('line', {
        key: uniqId(),
        x1: start[0],
        y1: start[1],
        x2: end[0],
        y2: end[1],
        stroke: 'black',
        ...attrs,
    });

const rect = (
    pos: Coord,
    width: number,
    height: number,
    attrs?: SVGAttributes<SVGRectElement>
) =>
    svg('rect', {
        key: uniqId(),
        x: pos[0],
        y: pos[1],
        width,
        height,
        ...attrs,
    });

const text = (
    pos: Coord,
    text: string,
    attrs?: SVGAttributes<SVGTextElement>
) =>
    svg(
        'text',
        {
            key: uniqId(),
            x: pos[0],
            y: pos[1],
            ...attrs,
        },
        text
    );

export const group = (
    elems: React.ReactSVGElement[],
    attrs?: SVGAttributes<SVGElement>
) =>
    svg(
        'g',
        {
            key: uniqId(),
            ...attrs,
        },
        ...elems
    );

const { enter, leave } = (() => {
    let currentBar: number | null = null;

    const enter = (id: number) => {
        if (id !== currentBar) {
            currentBar = id;
            // selectBarchartEntry(id);
        }
    };
    const leave = () => {
        currentBar = null;
    };

    return { enter, leave };
})();

const renderBar =
    (width: number, min: number) => (entry: BarchartEntry, index: number) => {
        const offset = index * width;
        const pos = coord(offset, 0);
        if (entry.selected) {
            return rect(pos, width, entry.value - min, {
                className: 'entry active',
                fill: entry.color,
            });
        }
        return rect(pos, width, entry.value - min, {
            className: 'entry',
            fill: entry.color,
            onMouseEnter: () => enter(entry.id),
            onMouseLeave: leave,
        });
    };

const renderAxis = (adjust: Adjust, axisWidth: number) => {
    const scaledDivider = adjust.divider * adjust.scale;

    const values = new Array(
        Math.ceil((adjust.max - adjust.min) / adjust.divider) + 1
    )
        .fill(false)
        .map((_, i) => adjust.min + i * adjust.divider);

    const labels = group(
        values
            .map(value => adjust.format(value))
            .map((v, i) =>
                text(
                    [
                        axisWidth - 4,
                        plotHeight + plotPadding - i * scaledDivider + 1,
                    ],
                    v,
                    {
                        textAnchor: 'end',
                        className: 'axis-label',
                        fontSize: '4px',
                    }
                )
            )
    );

    const ticks = group(
        values.map((_, i) =>
            line(
                [axisWidth, i * scaledDivider],
                [axisWidth - 2, i * scaledDivider]
            )
        ),
        {
            transform: `translate(0, ${plotHeight + plotPadding}) scale(1 -1)`,
        }
    );

    const top = line(
        [axisWidth, plotPadding],
        [axisWidth + plotWidth, plotPadding]
    );
    const right = line(
        [axisWidth + plotWidth, plotPadding],
        [axisWidth + plotWidth, plotHeight + plotPadding]
    );
    const bottom = line(
        [axisWidth + plotWidth, plotHeight + plotPadding],
        [axisWidth, plotHeight + plotPadding]
    );
    const left = line(
        [axisWidth, plotHeight + plotPadding],
        [axisWidth, plotPadding]
    );
    return group([labels, ticks, top, right, bottom, left]);
};

const renderPercentiles = (adjust: Adjust, entries: BarchartEntry[]) => {
    const values = entries.map(e => e.value);

    const f = (p: number, stroke: string) =>
        getPercentile(values, p).map(val =>
            line(
                coord(0, val - adjust.min),
                coord(plotWidth, val - adjust.min),
                { stroke, strokeWidth: 0.5 / adjust.scale }
            )
        );

    return scopeOption()
        .let('ten', f(0.1, '#FF9800'))
        .let('med', f(0.5, '#A56F68'))
        .let('nin', f(0.9, '#D53E2A'))
        .fold(group([]), ({ ten, med, nin }) =>
            group([ten, med, nin], {
                transform: `translate(${axisWidth}, ${
                    plotHeight + plotPadding
                }) scale(1 -${adjust.scale})`,
            })
        );
};

const renderLegend = () =>
    DIV(
        'barchart-legend',
        DIV('percentile', DIV('icon p10'), DIV('label', '10%')),
        DIV('percentile', DIV('icon p50'), DIV('label', tr.mdq('median'))),
        DIV('percentile', DIV('icon p90'), DIV('label', '90%'))
    );

export const renderChart = () =>
    getBarchartEntries().map(({ entries, adjust }) => {
        const width = plotWidth / entries.length;
        const bars = group(entries.map(renderBar(width, adjust.min)), {
            transform: `translate(${axisWidth}, ${
                plotHeight + plotPadding
            }) scale(1 -${adjust.scale})`,
        });
        const axis = renderAxis(adjust, axisWidth);
        const percentiles = renderPercentiles(adjust, entries);

        return DIV(
            'barchart',
            wrapSVG([bars, axis, percentiles], {
                className: 'barchart-svg',
                viewBox: `0 0 ${plotWidth + axisWidth + plotPadding} ${
                    plotHeight + 2 * plotPadding
                }`,
            }),
            renderLegend()
        );
    });
