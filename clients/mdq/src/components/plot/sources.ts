import { getSources } from 'mdq/src/queries/app';
import { DataSetSource } from 'mdq/src/remote';
import { A, DIV, H3 } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';

const renderSource = (source: DataSetSource) =>
    DIV(
        'source',
        A({ href: fromRecord(source.url) }, fromRecord(source.label))
    );

const renderSourcesImpl = (sources: DataSetSource[]) =>
    DIV(
        'sources',
        H3('', tr.mdq('sources-label')),
        ...sources.map(renderSource)
    );

export const renderSources = () => getSources().map(renderSourcesImpl);

export default renderSources;
