/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'sdi/polyfill';
import './shape';
import './locale';

import * as debug from 'debug';
import { source, AppConfigIO, getMessage, remoteNone } from 'sdi/source';
import { IShape, configure, defaultShape } from 'sdi/shape';
import { defaultPrintRequest, defaultPrintResponse } from 'sdi/map';
import { initialTableState } from 'sdi/components/table';
import { defaultTableState } from 'sdi/components/table2';

import App from './app';
import { AppLayout } from './shape/types';
import { defaultPrintState } from './components/print';
import { displayException } from 'sdi/app';
import { defaultToolGeocoder } from 'sdi/ports/geocoder';
import { defaultInteraction } from './events/map';

const logger = debug('sdi:index');

export const main = (SDI: any) => {
    AppConfigIO.decode(SDI).fold(
        errors => {
            const textErrors = errors.map(e => getMessage(e.value, e.context));
            displayException(textErrors.join('\n'));
        },
        config => {
            const initialState: IShape = {
                'app/codename': 'mdq',
                ...defaultShape(config),
                'app/user': SDI.user,
                'app/root': SDI.root,
                'app/api-root': SDI.api,
                'app/csrf': SDI.csrf,
                'app/lang': 'fr',
                'app/layout': [AppLayout.MapAndInfo],
                'app/map-ready': false,
                'app/current-map': null,
                // 'app/current-layer': null,
                'app/current-feature': [],
                'app/route': SDI.args,

                'component/legend': {
                    currentPage: 'info',
                },

                'component/menu': {
                    folded: true,
                },

                'component/mapnavigator': {
                    query: '',
                },

                'component/table': defaultTableState(),
                'component/table/extract': initialTableState(),

                'component/legend/show-wms-legend': false,

                'component/legend/webservices': {
                    folded: true,
                    url: '',
                    layers: [],
                },

                'component/legend/geocoder': defaultToolGeocoder(),

                'component/legend/positioner': {
                    point: {
                        latitude: 0,
                        longitude: 0,
                    },
                },

                'component/legend/share': {
                    withView: false,
                },

                'component/button': {},
                'component/bookmark/current-index': null,

                'component/layers/select': [],
                'component/layers/info': [],
                'component/plot/table/sort': null,
                'component/legend/show-primary': true,
                'component/legend/show-secondary': false,

                'port/map/scale': {
                    count: 0,
                    unit: '',
                    width: 0,
                },

                'port/map/view': {
                    dirty: 'geo/extent',
                    srs: 'EPSG:31370',
                    center: [149546, 169775],
                    rotation: 0,
                    zoom: 4.8,
                    feature: null,
                    extent: [140369.1, 160852.2, 159145.1, 178534.6],
                },

                'port/map/interaction': defaultInteraction(),
                'port/map/printRequest': defaultPrintRequest(),
                'port/map/printResponse': defaultPrintResponse(),
                'port/map/printToImage': false,

                'component/print': defaultPrintState(),

                'port/map/loading': [],
                'data/alias': [],
                'app/current-baselayer': 'urbis.irisnet.be/urbis_gray',
                'data/baselayers': [
                    {
                        id: 'urbis.irisnet.be',
                        layers: [
                            {
                                url: 'https://geoservices-urbis.irisnet.be/geoserver/ows',
                                codename: 'urbis_ortho',
                                name: {
                                    fr: 'Orthophoto',
                                    nl: 'Luchtfoto',
                                },
                                srs: 'EPSG:31370',
                                params: {
                                    LAYERS: {
                                        en: 'Ortho2019',
                                        fr: 'Ortho2019',
                                        nl: 'Ortho2019',
                                    },
                                    VERSION: '1.1.1',
                                },
                            },
                            {
                                url: 'https://geoservices-urbis.irisnet.be/geoserver/ows',
                                codename: 'urbis_gray',
                                name: {
                                    en: 'Urbis Gray',
                                    fr: 'Urbis Gray',
                                    nl: 'Urbis grijswaarden',
                                },
                                srs: 'EPSG:31370',
                                params: {
                                    LAYERS: {
                                        en: 'urbisFRGray',
                                        fr: 'urbisFRGray',
                                        nl: 'urbisNLGray',
                                    },
                                    VERSION: '1.1.1',
                                },
                            },
                        ],
                    },
                ],

                'remote/errors': {},

                'data/user': null,
                'data/dataset': {
                    main: remoteNone,
                    extra: remoteNone,
                },
                'data/georef': {
                    M: remoteNone,
                    D: remoteNone,
                    S: remoteNone,
                },
                'data/extra/layers': [],
            };

            try {
                const start = source<IShape>(['app/lang']);
                const store = start(initialState);
                configure(store);
                const app = App(store);
                logger('start rendering');
                app();
            } catch (err) {
                displayException(`${err}`);
            }
        }
    );
};

logger('loaded');
