/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {
    FeatureCollection,
    fetchIO,
    fetchWithoutValidationIO,
    IAliasCollection,
    IAliasCollectionIO,
    IMapBaseLayer,
    IMapBaseLayerIO,
    IUser,
    IUserIO,
    IServiceBaseLayersIO,
    IServiceBaseLayers,
    FeatureCollectionIO,
} from 'sdi/source';

import * as io from 'io-ts';
import { MessageRecordIO, MessageRecordLangIO } from 'sdi/source/io/message';
import { nullable } from 'sdi/source/io/io';
import { getApiUrl } from 'sdi/app';

export const fetchLayer = (url: string): Promise<FeatureCollection> =>
    fetchWithoutValidationIO(url);
export const fetchBaseLayer = (url: string): Promise<IMapBaseLayer> =>
    fetchIO(IMapBaseLayerIO, url);
export const fetchBaseLayerAll = (url: string): Promise<IServiceBaseLayers> =>
    fetchIO(IServiceBaseLayersIO, url);
export const fetchAlias = (url: string): Promise<IAliasCollection> =>
    fetchIO(IAliasCollectionIO, url);
export const fetchUser = (url: string): Promise<IUser> => fetchIO(IUserIO, url);

const DataEntryFlagIO = io.union(
    [io.literal('NN'), io.literal('HS'), io.literal('GM'), io.literal('ND')],
    'DataEntryFlagIO'
);
export type DataEntryFlag = io.TypeOf<typeof DataEntryFlagIO>;

const DataEntryIO = io.interface(
    {
        id: io.Integer,
        value: io.number,
        flag: DataEntryFlagIO,
    },
    'DataEntryIO'
);
export type DataEntry = io.TypeOf<typeof DataEntryIO>;

const DataLegendIO = io.interface(
    {
        low: io.number,
        high: io.number,
        color: io.string,
        flag: DataEntryFlagIO,
        label: MessageRecordIO,
    },
    'DataLegendIO'
);
export type DataLegend = io.TypeOf<typeof DataLegendIO>;

const DataLegendExtraIO = io.interface(
    {
        lang: MessageRecordLangIO,
        label: io.string,
        value: io.string,
    },
    'DataLegendIO'
);
export type DataLegendExtra = io.TypeOf<typeof DataLegendExtraIO>;

const DataSetLevelIO = io.union(
    [io.literal('M'), io.literal('D'), io.literal('S')],
    'DataSetLevelIO'
);
export type DataSetLevel = io.TypeOf<typeof DataSetLevelIO>;

export const DataSetSourceIO = io.interface(
    {
        id: io.Integer,
        url: MessageRecordIO,
        label: MessageRecordIO,
    },
    'DataSetSourceIO'
);
export type DataSetSource = io.TypeOf<typeof DataSetSourceIO>;

const DataSetIO = io.interface(
    {
        id: io.Integer,
        level: DataSetLevelIO,
        year: io.string,
        name: MessageRecordIO,
        unit: MessageRecordIO,
        description: MessageRecordIO,
        extra: nullable(io.Integer),
        entries: io.array(DataEntryIO),
        legend: io.array(DataLegendIO),
        sources: io.array(DataSetSourceIO),
        legendExtra: io.array(DataLegendExtraIO),
    },
    'DataSetIO'
);
export type DataSet = io.TypeOf<typeof DataSetIO>;

export const fetchDataSet = (id: number) =>
    fetchIO(DataSetIO, getApiUrl(`geodata/mdq/dataset/${id}`));

export const fetchReferenceLayer = (level: DataSetLevel) =>
    fetchIO(FeatureCollectionIO, getApiUrl(`geodata/mdq/ref/${level}`));

const ExtraLayerTypeIO = io.union(
    [io.literal('line'), io.literal('poly')],
    'ExtraLayerTypeIO'
);

const ExtraLayerInfoIO = io.interface(
    {
        id: io.Integer,
        name: MessageRecordIO,
        source: MessageRecordIO,
        geometry_type: ExtraLayerTypeIO,
        color: io.string,
    },
    'ExtraLayerInfoIO'
);

export type ExtraLayerInfo = io.TypeOf<typeof ExtraLayerInfoIO>;

export const fetchExtraLayerInfo = () =>
    fetchIO(io.array(ExtraLayerInfoIO), getApiUrl(`geodata/mdq/extra/`));

export const fetchExtraLayerData = (id: number) =>
    fetchIO(FeatureCollectionIO, getApiUrl(`geodata/mdq/extra/${id}`));
