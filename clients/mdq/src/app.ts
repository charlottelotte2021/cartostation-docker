/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { getLang, loop } from 'sdi/app';
import { DIV, NodeOrOptional } from 'sdi/components/elements';

import tr from 'sdi/locale';

import map from './components/map';
// import table from './components/table/feature-collection2';
// import feature from './components/feature-view';
import legend, { switcher as legendSwitch } from './components/legend';
// import mapnavigator from './components/mapnavigator';
// import tracker from './components/geo-tracker';
import measure from './components/geo-measure';
// import extract from './components/extract';
import print from './components/print';
// import query from './components/query';
import {
    activityLogger,
    loadExtraLayerInfo,
    loadReferenceLayers,
} from './events/app';
import { getLayout } from './queries/app';

// import { viewEvents } from './events/map';
import { AppLayout } from './shape/types';
import { navigate } from './events/route';
import { langAction, visitAction } from 'sdi/activity';

const logger = debug('sdi:app');

const wrappedMain = (name: string, ...elements: NodeOrOptional[]) =>
    DIV(
        { className: 'mdq-inner' },
        // modal(),
        // header('mdq', renderAppListingButton),
        DIV({ className: `main ${name}` }, ...elements)
        // footer()
    );

// const renderMapFs =
//     () => wrappedMain('map-fs', map(), legendSwitch(), legend());

const renderMapAndInfo = () =>
    wrappedMain('map-and-info', map(), legendSwitch(), legend());

// const renderMapAndFeature =
//     () => wrappedMain('map-and-info', map(), legendSwitch(), feature());

// const renderTableFs =
//     () => wrappedMain('table-fs', table());

// const renderMapAndTable =
//     () => wrappedMain('map-and-table', DIV({ className: 'vertical-split' }, map(), table()), legendSwitch(), legend());

// const renderMapNavigatorFS =
//     () => wrappedMain('map-navigator-fs', mapnavigator());

// const renderMapAndTableAndFeature =
//     () => wrappedMain('map-and-table', DIV({ className: 'vertical-split' }, map(), table()), legendSwitch(), feature());

// const renderMapAndTracker =
//     () => wrappedMain('map-and-tracker', map(), tracker());

const renderMapAndMeasure = () =>
    wrappedMain('map-and-measure', map(), measure());

// const renderMapAndExtract =
//     () => wrappedMain('map-and-extract', map(), extract());

const renderPrint = () => wrappedMain('print', map(), print());

// const renderQuery =
//     () => wrappedMain('query', query());

// const renderQueryPrint =
//     () => wrappedMain('query', query(true));

const renderMain = () => {
    const layout = getLayout();
    switch (layout) {
        // case AppLayout.MapFS: return renderMapFs();
        // case AppLayout.MapAndTable: return renderMapAndTable();
        // case AppLayout.MapAndTableAndFeature: return renderMapAndTableAndFeature();
        case AppLayout.MapAndInfo:
            return renderMapAndInfo();
        // case AppLayout.MapAndFeature: return renderMapAndFeature();
        // case AppLayout.TableFs: return renderTableFs();
        // case AppLayout.MapNavigatorFS: return renderMapNavigatorFS();
        // case AppLayout.MapAndTracker: return renderMapAndTracker();
        case AppLayout.MapAndMeasure:
            return renderMapAndMeasure();
        // case AppLayout.MapAndExtract: return renderMapAndExtract();
        case AppLayout.Print:
            return renderPrint();
        // case AppLayout.Query: return renderQuery();
        // case AppLayout.QueryPrint: return renderQueryPrint();
    }
};

const effects = () => {
    // viewEvents.updateMapView({ dirty: 'geo' });
    // loadAlias(getApiUrl(`alias`));
    // loadUser();
    navigate();
    tr.init_edited();
    // loadTerms();
    loadReferenceLayers();
    loadExtraLayerInfo();
    activityLogger(visitAction());
    activityLogger(langAction(getLang()));
};

const app = loop('mdq-app', renderMain, effects);
export default app;

logger('loaded');
