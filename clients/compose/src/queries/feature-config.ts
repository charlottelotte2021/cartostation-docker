/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { query } from 'sdi/shape';
import { getLang, SyntheticLayerInfo } from 'sdi/app';
import { FeatureCollection, RowConfig } from 'sdi/source';
import { getLayerPropertiesKeys } from 'sdi/util';

import { getCurrentLayerInfo, getLayerData } from './app';
import { identity } from 'fp-ts/lib/function';
import { some, none } from 'fp-ts/lib/Option';

export const getFeatureViewType = () =>
    getCurrentLayerInfo()
        .map(({ info }) => info.featureViewOptions.type)
        .fold(null, identity);

const extractConfig = ({ info }: SyntheticLayerInfo) =>
    info.featureViewOptions && info.featureViewOptions.type === 'config'
        ? some(info.featureViewOptions)
        : none;

export const getConfig = () =>
    getCurrentLayerInfo()
        .chain(extractConfig)
        .getOrElse({ type: 'config', rows: [] as RowConfig[] });

export const getRows = () => {
    const lc = getLang();
    const allRows = getConfig().rows;
    return allRows.filter(r => r.lang === lc);
};

export const getRow = (index: number): RowConfig | undefined =>
    getRows()[index];

export const getCurrentIndex = () =>
    query('component/feature-config').currentRow;

export const getCurrentRow = () => getRow(getCurrentIndex());

export const getEditedValue = () => {
    return query('component/feature-config').editedValue;
};

// Layer / FeatureCollection
const emptyCollection: FeatureCollection = {
    type: 'FeatureCollection',
    features: [],
};
export const getLayer = (): FeatureCollection =>
    getCurrentLayerInfo()
        .map(({ metadata }) => metadata.uniqueResourceIdentifier)
        .chain(rid => getLayerData(rid).fold(() => none, identity))
        .getOrElse(emptyCollection);

export const getKeys = (): string[] => {
    return getLayerPropertiesKeys(getLayer());
};
