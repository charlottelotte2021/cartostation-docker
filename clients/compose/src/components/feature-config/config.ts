/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { ReactNode } from 'react';

import {
    BooleanConfig,
    ConfigWithLabel,
    ImageConfig,
    NumberConfig,
    PiechartConfig,
    PiechartPiece,
    PropType,
    RowConfig,
    StringConfig,
    StringOptionLevel,
    StringOptionStyle,
    TextConfig,
    TimeserieConfig,
    URLConfig,
    getReferencePoint,
    TermConfig,
} from 'sdi/source';
import { DIV, SPAN, NODISPLAY, LABEL } from 'sdi/components/elements';
import {
    inputText,
    inputNullableNumber,
    inputColor,
    renderSelect,
    attrOptions,
    options,
} from 'sdi/components/input';
import tr from 'sdi/locale';
import { getLayerPropertiesKeys, stringToParagraphs } from 'sdi/util';

import {
    addPieChartPiece,
    addPropToConfig,
    movePropDown,
    movePropUp,
    removePieChartPiece,
    removePropFromConfig,
    resetEditedValue,
    setCurrentRow,
    setPiechartPieceColor,
    setPiechartPieceLabel,
    setPiechartRadius,
    setPiechartScale,
    setPropLevelForConfig,
    setPropStyleForConfig,
    setPropTypeForConfig,
    setPropWithLabelForConfig,
    setTimeserieReference,
    setTimeserieUrl,
    setTextText,
} from '../../events/feature-config';
import legendEvents from '../../events/legend-editor';
import {
    getKeys,
    getCurrentIndex,
    getRows,
    getRow,
} from '../../queries/feature-config';
import { getCurrentLayerInfo, getLayerData } from '../../queries/app';
import { makeIcon, makeRemove, makeRemoveIcon } from '../button';
import { ComposeMessageKey } from 'compose/src/locale';
import { identity } from 'fp-ts/lib/function';
import { none, fromNullable, some } from 'fp-ts/lib/Option';
import { spanTooltipBottom } from 'sdi/components/tooltip';
import { nameToString } from 'sdi/components/button/names';
import { getAlias } from 'sdi/app';
import { Setoid } from 'fp-ts/lib/Setoid';

const logger = debug('sdi:feature-config/config');

const moveUpButton = makeIcon('move-up', 3, 'arrow-up', {
    position: 'top',
    text: () => tr.core('move-up'),
});
const moveDownButton = makeIcon('move-down', 3, 'arrow-down', {
    position: 'bottom',
    text: () => tr.core('move-down'),
});

const propType: PropType[] = [
    'string',
    'number',
    'boolean',
    'url',
    'image',
    'html',
    'term',
];
const propLevel: StringOptionLevel[] = ['normal', 'subtitle', 'title'];
const propStyle: StringOptionStyle[] = [
    'normal',
    'bold',
    'italic',
    'bold-italic',
];

const WIDGET_TIMESERIE = '__WIDGET_TIMESERIE__';
const WIDGET_PIECHART = '__WIDGET_PIECHART__';
const WIDGET_TEXT = '__WIDGET_TEXT__';
const WIDGETS = [WIDGET_PIECHART, WIDGET_TIMESERIE, WIDGET_TEXT];
const WIDGET_NAME: { [k: string]: PropType } = {
    [WIDGET_PIECHART]: 'piechart',
    [WIDGET_TIMESERIE]: 'timeserie',
    [WIDGET_TEXT]: 'text',
};

const isWidget = (pn: string) =>
    WIDGETS.reduce((acc, w) => {
        if (acc) {
            return acc;
        }
        return w === pn;
    }, false);

// const renderSelectItem =
//     <T extends string>(set: (a: T) => void) =>
//         (v: T) =>
//             DIV({ className: 'interactive', onClick: () => set(v) }, SPAN({}, v));

// export const renderSelect = <T extends string>(
//     list: T[],
//     selected: T | null,
//     set: (a: T) => void
// ) => {
//     const mkItem = renderSelectItem(set);
//     const below = list.filter(i => i !== selected).map(mkItem);

//     return DIV(
//         { className: 'select' },
//         DIV({ className: 'selected' }, SPAN({}, !selected ? 'none' : selected)),
//         DIV({ className: 'below' }, ...below)
//     );
// };

const setoidPropType: Setoid<PropType> = {
    equals: (x: PropType, y: PropType) => x.toString() === y.toString(),
};
const setoidPropLevel: Setoid<StringOptionLevel> = {
    equals: (x: StringOptionLevel, y: StringOptionLevel) =>
        x.toString() === y.toString(),
};
const setoidPropStyle: Setoid<StringOptionStyle> = {
    equals: (x: StringOptionStyle, y: StringOptionStyle) =>
        x.toString() === y.toString(),
};
type Scale = 'normal' | 'log';
const setoidScale: Setoid<Scale> = {
    equals: (x: Scale, y: Scale) => x.toString() === y.toString(),
};
type Radius = 'normal' | 'dynamic';
const setoidRadius: Setoid<Radius> = {
    equals: (x: Radius, y: Radius) => x.toString() === y.toString(),
};

const inputWithLabel = (index: number, row: ConfigWithLabel) =>
    DIV(
        {
            className: row.options.withLabel
                ? 'style-tool with-label active'
                : 'style-tool with-label',
            onClick: () => {
                setPropWithLabelForConfig(index, !row.options.withLabel);
            },
        },
        tr.compose('displayLabel')
    );

const inputType = (index: number) =>
    renderSelect(
        'select-row-type',
        rt => rt.toString(),
        v => setPropTypeForConfig(index, v),
        setoidPropType,
        tr.compose('dataType')
    );

const inputLevel = (index: number) =>
    renderSelect(
        'select-input-level',
        il => il.toString(),
        v => setPropLevelForConfig(index, v),
        setoidPropLevel,
        tr.compose('textFormat')
    );

const inputStyle = (index: number) =>
    renderSelect(
        'select-row-style',
        rs => rs.toString(),
        v => setPropStyleForConfig(index, v),
        setoidPropStyle,
        tr.compose('textStyle')
    );

const renderSelectRow = (pn: string) =>
    DIV(
        {
            className: 'row',
            onClick: () => addPropToConfig(pn),
        },
        DIV('propName', getAlias(pn))
    );

const renderSelectWidget = (wn: string) =>
    DIV(
        {
            className: 'row',
            onClick: () => addPropToConfig(wn, WIDGET_NAME[wn]),
        },
        DIV('propName', WIDGET_NAME[wn])
    );

const renderMoveUpButton = (index: number) =>
    moveUpButton(() => movePropUp(index));

const renderMoveDownButton = (index: number) =>
    moveDownButton(() => movePropDown(index));

const renderRemoveButton = (index: number) =>
    makeRemoveIcon(
        `remove-feature-config-${index}`,
        3,
        'times',
        () => tr.compose('rmvMsgGeneric'),
        { position: 'left', text: tr.core('remove') }
    )(() => removePropFromConfig(index));

const renderExportable = () =>
    getCurrentLayerInfo().map(s =>
        fromNullable(s.info.layerInfoExtra).map(infoExtra =>
            DIV(
                'exportable-input-box',
                DIV(
                    {
                        className: infoExtra.exportable ? 'active' : '',
                        onClick: () =>
                            legendEvents.setExportable(
                                s.info.id,
                                !infoExtra.exportable
                            ),
                    },
                    tr.compose('exportable')
                ),
                spanTooltipBottom(
                    tr.compose('helptext:displayExport'),
                    {},
                    nameToString('info-circle')
                )
            )
        )
    );

const renderSelectedRow = (row: RowConfig, key: number, rows: RowConfig[]) => {
    const current = getCurrentIndex();
    const propName = row.propName;
    const displayName = isWidget(propName)
        ? WIDGET_NAME[propName]
        : getAlias(propName);
    let tools = [];

    if (key === 0) {
        tools = [renderMoveDownButton(key), renderRemoveButton(key)];
    } else if (key + 1 === rows.length) {
        tools = [renderMoveUpButton(key), renderRemoveButton(key)];
    } else {
        tools = [
            renderMoveDownButton(key),
            renderMoveUpButton(key),
            renderRemoveButton(key),
        ];
    }

    return DIV(
        {
            key,
            className:
                current === key ? 'row configured active' : 'row configured',
            onClick: () => setCurrentRow(key),
        },
        DIV('propName', displayName),
        DIV('tools', ...tools)
    );
};

// const label = (k: ComposeMessageKey, c = 'label') =>
//     DIV({ className: c }, tr.compose(k));

const renderStringEditor = (
    index: number,
    config: StringConfig | NumberConfig | BooleanConfig | URLConfig | TermConfig
) => [
    inputLevel(index)(propLevel, some(config.options.level)),
    inputStyle(index)(propStyle, some(config.options.style)),
    inputWithLabel(index, config),
];

const renderTextEditor = (index: number, config: TextConfig) => [
    DIV(
        `style-tool text`,
        LABEL(
            '',
            tr.compose('featureText'),
            inputText(
                attrOptions(
                    `feature-text-${index}`,
                    () => config.options.text,
                    newVal => setTextText(index, newVal),
                    { placeholder: tr.core('placeholderInsertText') }
                )
            )
        )
    ),
    inputLevel(index)(propLevel, some(config.options.level)),
    inputStyle(index)(propStyle, some(config.options.style)),
];

const renderImageEditor = (index: number, config: ImageConfig) => [
    inputWithLabel(index, config),
];

// const renderStringEditor = (
//     index: number,
//     config: StringConfig | NumberConfig | BooleanConfig | URLConfig | TermConfig
// ) => [
//         DIV( 'style-tool item', inputWithLabel(index, config)),
//         DIV( 'style-tool item', label('textFormat'), inputLevel(index)(propLevel, some(config.options.level))),
//         DIV( 'style-tool item', label('textStyle'), inputStyle(index)(propStyle, some(config.options.style))),
//     ];

// const renderTextEditor = (index: number, config: TextConfig) => [
//     DIV(
//         { className: `style-tool text` },
//         SPAN({ className: 'label' }, tr.compose('featureText')),
//         inputText(
//             () => config.options.text,
//             newVal => setTextText(index, newVal),
//             { placeholder: tr.core('placeholderInsertText') }
//         )
//     ),
//     DIV( 'style-tool item', label('textFormat'), inputLevel(index)(propLevel, some(config.options.level))),
//     DIV( 'style-tool item', label('textStyle'), inputStyle(index)(propStyle, some(config.options.style))),
// ];

// const renderImageEditor = (index: number, config: ImageConfig) => [
//     DIV( 'style-tool item', inputWithLabel(index, config)),
// ];

const renderPiechartPiece =
    (index: number) => (piece: PiechartPiece, key: number) => {
        const removeButton = makeRemove(
            `renderStyleGroupValue-${piece.propName}-${key}`,
            3,
            () => tr.compose('remove'),
            () => tr.compose('rmvMsgGeneric')
        );

        const inputColorElem = inputColor(
            () => piece.color,
            c => setPiechartPieceColor(index, piece.propName, c)
        );
        // const inputColor = renderInputAlphaColor(
        //     'style-tool color',
        //     '',
        //     () => Color(piece.color),
        //     c => setPiechartPieceColor(index, piece.propName, c.string())
        // );

        const inputLabel = DIV(
            'style-tool label',
            LABEL(
                '',
                tr.compose('alias'),
                inputText(
                    attrOptions(
                        `piechart-label-${key}`,
                        () =>
                            piece.label === undefined
                                ? piece.propName
                                : piece.label,
                        newVal =>
                            setPiechartPieceLabel(
                                index,
                                piece.propName,
                                newVal
                            ),
                        { placeholder: tr.core('placeholderInsertText') }
                    )
                )
            )
        );

        return DIV(
            {
                className: 'value-name',
                key: `pie-piece-${piece.propName}-${key}`,
            },
            DIV(
                'piece-header',
                removeButton(() => removePieChartPiece(index, piece.propName)),
                SPAN({}, piece.propName),
                inputLabel
            ),
            DIV('piece-body', inputColorElem)
        );
    };

const pieceAdd = (index: number, value: string) => {
    if (value) {
        resetEditedValue();
        addPieChartPiece(index, value);
    }
};

const renderColumnNames = (index: number, keys: string[]) =>
    keys.map(key => {
        return DIV(
            {
                key,
                className: 'column-name',
                onClick: () => pieceAdd(index, key),
            },
            key
        );
    });

const columnPicker = (index: number) => {
    // const props = getCurrentLayerInfo()
    //     .chain(({ metadata }) =>
    //         getLayerData(metadata.uniqueResourceIdentifier).fold(
    //             () => none,
    //             identity
    //         )
    //     )
    //     .fold([], layerData => {
    //         return getLayerPropertiesKeys(layerData);
    //     });

    // return renderSelect(
    //     'column-picker',
    //     identity,
    //     (key) => pieceAdd(index, key),
    //     setoidString
    // )(props, fromNullable(getEditedValue())) //TODO: make a state record for last selected value?;

    const children: React.ReactNode[] = getCurrentLayerInfo()
        .chain(({ metadata }) =>
            getLayerData(metadata.uniqueResourceIdentifier).fold(
                () => none,
                identity
            )
        )
        .fold([], layerData => {
            return renderColumnNames(index, getLayerPropertiesKeys(layerData));
        });

    // {
    //     const { metadata } = getCurrentLayerInfo();
    //     const children: React.ReactNode[] = [];
    //     if (metadata) {
    //         getLayerData(metadata.uniqueResourceIdentifier)
    //             .map(
    //                 opt => opt.map(
    //                     (layerData) => {
    //                         const keys = getLayerPropertiesKeys(layerData);
    //                         children.push(...renderColumnNames(index, keys));
    //                     }));
    //     }
    return DIV(
        'column-picker',
        DIV('title', tr.compose('columnPicker')),
        ...children
    );
};

const renderPiechartEditor = (index: number, config: PiechartConfig) => {
    const columns = config.options.columns;
    const pieces = columns.map(renderPiechartPiece(index));
    const elements: ReactNode[] = [];

    elements.push(DIV('selected-items', ...pieces, columnPicker(index)));

    const scaleSelect = renderSelect(
        'select-piechart-scale',
        s => s.toString(),
        v => setPiechartScale(index, v),
        setoidScale,
        tr.compose('piechartScale')
    )(['normal', 'log'], some(config.options.scale));
    // renderSelect(
    //     ['normal', 'log'],
    //     config.options.scale,
    //     v => {
    //         setPiechartScale(index, v);
    //     }
    // );

    const radiusSelect = renderSelect(
        'select-piechart-radius',
        r => r.toString(),
        v => setPiechartRadius(index, v),
        setoidRadius,
        tr.compose('piechartRadius')
    )(['normal', 'dynamic'], some(config.options.radius));
    // renderSelect(
    //     ['normal', 'dynamic'],
    //     config.options.radius,
    //     v => {
    //         setPiechartRadius(index, v);
    //     }
    // );

    return [scaleSelect, radiusSelect, DIV('item item-piechart', ...elements)];
};

const renderTimeserieEditor = (index: number, config: TimeserieConfig) => {
    return [
        DIV(
            'style-tool item',
            LABEL(
                '',
                tr.compose('timeserieTemplateURL'),
                inputText(
                    options(
                        `timeserie-editor-${index}`,
                        () => config.options.urlTemplate,
                        value => setTimeserieUrl(index, value)
                    )
                )
            ),
            LABEL(
                '',
                tr.compose('timeserieReference'),
                inputNullableNumber(
                    options(
                        `timeserie-editor-nb-${index}`,
                        () => getReferencePoint(config.options),
                        value => setTimeserieReference(index, value)
                    )
                )
            )
        ),
    ];
};

const renderTypeSelector = (index: number, config: RowConfig) =>
    inputType(index)(propType, some(config.type));

const renderRowEditor = (index: number) => {
    const row = getRow(index);
    const elements: ReactNode[] = [];
    if (row) {
        switch (row.type) {
            case 'string':
            case 'number':
            case 'boolean':
            case 'term':
            case 'url':
                elements.push(
                    renderTypeSelector(index, row),
                    ...renderStringEditor(index, row)
                );
                break;
            case 'image':
                elements.push(
                    renderTypeSelector(index, row),
                    ...renderImageEditor(index, row)
                );
                break;
            case 'html':
                elements.push(
                    renderTypeSelector(index, row),
                    inputWithLabel(index, row)
                );
                break;
            case 'piechart':
                elements.push(...renderPiechartEditor(index, row));
                break;
            case 'timeserie':
                elements.push(...renderTimeserieEditor(index, row));
                break;
            case 'text':
                elements.push(...renderTextEditor(index, row));
                break;
        }
    }
    return elements;
};

const renderPanel = (
    key: string,
    className: string,
    label: ComposeMessageKey,
    ...children: ReactNode[]
) =>
    DIV(
        { className: `column ${className}`, key: `${label}-${key}` },
        DIV('column__title', DIV('title', tr.compose(label))),
        DIV('column__body', ...children)
    );

const renderHelptext = (hasRows: boolean) => {
    if (hasRows) {
        return NODISPLAY();
    }

    return DIV(
        'helptext',
        stringToParagraphs(tr.compose('helptext:featureView'))
    );
};

const render = () => {
    const propNames = getKeys();
    const current = getCurrentIndex();
    const rows = getRows();
    const editor = renderRowEditor(current);

    const columElements = propNames
        .filter(pn => !isWidget(pn))
        .map(renderSelectRow);

    const widgetElements = WIDGETS.map(renderSelectWidget);

    const elements: ReactNode[] = [];
    if (widgetElements.length > 0) {
        elements.push(
            DIV(
                'group',
                DIV(
                    {},
                    SPAN('group-label', tr.compose('widgets')),
                    spanTooltipBottom(
                        tr.compose('helptext:widgetFeature'),
                        { className: 'icon' },
                        nameToString('info-circle')
                    )
                ),
                ...widgetElements
            )
        );
    }
    if (columElements.length > 0) {
        elements.push(
            DIV(
                'group',
                DIV('group-label', tr.compose('columns')),
                ...columElements
            )
        );
    }

    const row = getRow(current);
    const key = `${row ? row.type : 'none'}-${current}`;
    return DIV(
        'app-split-main',
        renderPanel(key, 'rows-remaining', 'infoChoice', ...elements),
        renderPanel(
            key,
            'rows-selected',
            'infoReorder',
            renderHelptext(rows.length > 0),
            rows.map(renderSelectedRow),
            renderExportable()
        ),
        renderPanel(key, 'row-editor', 'style', ...editor)
    );
};

export default render;

logger('loaded');
