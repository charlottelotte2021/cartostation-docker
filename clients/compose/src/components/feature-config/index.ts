/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV, H2, SPAN } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { setLayout } from '../../events/app';

import { AppLayout } from '../../shape/types';
import { makeLabelAndIcon } from '../button';
import config from './config';
import { ensureTableSelection } from 'compose/src/events/feature-config';

const logger = debug('sdi:feature-config');

export interface FeatureConfig {
    currentRow: number;
    editedValue: string | null;
}

export const initialFeatureConfigState = (): FeatureConfig => ({
    currentRow: -1,
    editedValue: null,
});

const closeButton = makeLabelAndIcon('close', 2, 'check', () =>
    tr.compose('backToMap')
);

const renderHeader = () =>
    DIV(
        { className: 'app-split-header' },
        H2(
            { className: 'app-split-title' },
            SPAN(
                { className: 'editor-title' },
                tr.compose('displayFeatureEdit')
            )
        ),

        DIV(
            { className: 'app-header-close' },
            closeButton(() => {
                setLayout(AppLayout.LegendEditor);
            })
        )
    );

const render = () => {
    ensureTableSelection();
    return DIV(
        { className: 'app-split-wrapper feature-config' },
        renderHeader(),
        config()
    );
};

export default render;

logger('loaded');
