import { fromNullable } from 'fp-ts/lib/Option';

import { ILayerInfo } from 'sdi/source';
import { DIV } from 'sdi/components/elements';
import featureView from 'sdi/components/feature-view';
import timeserie from 'sdi/components/timeserie';

import {
    getCurrentLayerId,
    getCurrentFeature,
    getCurrentLayerInfo,
} from '../queries/app';
import { dispatchTimeserie, loadData } from '../events/timeserie';
import { getData, queryTimeserie } from '../queries/timeserie';

const tsPlotter = timeserie(
    queryTimeserie,
    getData,
    getCurrentLayerId,
    dispatchTimeserie,
    loadData
);
const noView = () => DIV({ className: 'feature-view' });

const withInfo = (info: ILayerInfo) =>
    fromNullable(getCurrentFeature()).fold(noView(), feature =>
        featureView(info.featureViewOptions, feature, tsPlotter)
    );

const render = () =>
    DIV(
        { className: 'sidebar-right' },
        getCurrentLayerInfo().fold(noView(), ({ info }) => withInfo(info))
    );

export default render;
