/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { table } from 'sdi/components/table2';
import { DIV } from 'sdi/components/elements';

import { tableQuery, getLayerSource } from '../../queries/table';
import {
    clearCurrentStreamData,
    tableDispatch,
    selectFeatureFromRow,
} from '../../events/table';
import { getCurrentLayerInfo } from '../../queries/app';
import { fromRecord } from 'sdi/locale';

const logger = debug('sdi:table/feature-collection');

const toolbar = () =>
    getCurrentLayerInfo()
        .map(({ name }) => fromRecord(name))
        .fold(
            DIV(
                { className: 'table-toolbar', key: 'table-toolbar' },
                DIV({ className: 'table-title' }, '')
            ),
            layerName =>
                DIV(
                    { className: 'table-toolbar', key: 'table-toolbar' },
                    DIV({ className: 'table-title' }, layerName)
                )
        );

// {
//     const { name } = getCurrentLayerInfo();
//     const layerName = name ? fromRecord(name) : '';
//     return DIV({ className: 'table-toolbar', key: 'table-toolbar' },
//         DIV({ className: 'table-title' }, layerName));
// };

const base = table(
    tableDispatch,
    tableQuery,
    selectFeatureFromRow,
    clearCurrentStreamData
);

// const render = base({
//     className: 'attr-headless-wrapper',
//     toolbar,
//     onRowSelect: selectFeature,
// });
const render = () => base(getLayerSource(), toolbar());

export default render;

logger('loaded');
