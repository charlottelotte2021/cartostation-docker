/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV, H2, LABEL, NODISPLAY, SPAN } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { FeatureCollection, MessageRecord, SubType } from 'sdi/source';

import queries from '../../queries/legend-editor';
import {
    getLayout,
    getSynteticLayerInfoOption,
    getCurrentLayerInfo,
    getLayerData,
} from '../../queries/app';
import { setLayout } from '../../events/app';
import * as featureConfigEvents from '../../events/feature-config';
import selectMain from './select-main';
import selectType from './select-type';
import { renderStyleSettings, renderLabelSettings } from './tools-main';
import continuous from './select-item-continuous';
import discrete from './select-item-discrete';
import { AppLayout } from '../../shape/types';
import { makeIcon } from '../button';
import {
    getLayerPropertiesKeys,
    getLayerPropertiesKeysForNumbers,
    Nullable,
} from 'sdi/util';
import events from '../../events/legend-editor';
import { Getter, inputColor, renderSelect, Setter } from 'sdi/components/input';
import { setoidString } from 'fp-ts/lib/Setoid';
import { none, some } from 'fp-ts/lib/Option';
import { getAlias, getLang } from 'sdi/app';
import renderMovable from 'sdi/components/movable';

const logger = debug('sdi:legend-editor');

type TextGetter = () => string;
type TextSetter = (a: string) => void;

export type LegendPage = 'legend' | 'tools';

export type PointConfigSelected = 'label' | 'marker';

export interface ILegend {
    currentPage: LegendPage;
}

export interface ILegendEditor {
    mainSelected: number;
    itemSelected: number;
    styleGroupSelected: number;
    styleGroupEditedValue: Nullable<number | string>;
    pointConfig: PointConfigSelected;
    autoClassValue: number;
    autoClassNbDecimals: number;
}

export const initialLegendEditorState = (): ILegendEditor => ({
    mainSelected: -1,
    itemSelected: -1,
    styleGroupSelected: -1,
    pointConfig: 'marker',
    styleGroupEditedValue: null,
    autoClassValue: 2,
    autoClassNbDecimals: 2,
});

export const editRecord = (
    get: Getter<MessageRecord>,
    set: Setter<MessageRecord>,
    text: string
) => {
    const lc = getLang();
    const rec = { ...get() };

    rec[lc] = text;
    set(rec);
};

const closeButton = makeIcon('close', 3, 'times', {
    position: 'bottom-left',
    text: () => tr.core('close'),
});
const switchTableButton = makeIcon('layerInfoSwitchTable', 3, 'table', {
    position: 'bottom-left',
    text: () => tr.compose('displayTableView'),
});
const switchMapButton = makeIcon('layerInfoSwitchMap', 3, 'map', {
    position: 'bottom-left',
    text: () => tr.compose('displayMapView'),
});

export const renderUpAndDownBtns = renderMovable(
    events.moveGroupUp,
    events.moveGroupDown
);

const renderCloseButton = () =>
    closeButton(() => setLayout(AppLayout.MapAndInfo));

const renderSwitchTableButton = () =>
    switchTableButton(() => {
        setLayout(AppLayout.LegendEditorAndTable);
    }, 'table');

const renderSwitchMapButton = () =>
    switchMapButton(() => {
        setLayout(AppLayout.LegendEditor);
    }, 'map');

const renderMapTableSwitch = () => {
    const l = getLayout();
    if (AppLayout.LegendEditor === l) {
        return renderSwitchTableButton();
    }
    return renderSwitchMapButton();
};

const layerTableSwitch = (lid: string) =>
    getSynteticLayerInfoOption(lid).map(() => renderMapTableSwitch());

// {
//     const { name, metadata } = getSynteticLayerInfoOption(lid);
//     if (name && metadata) {
//         return (
//             renderMapTableSwitch());
//     }
//     return NODISPLAY();
// };

const layerName = (lid: string) =>
    getSynteticLayerInfoOption(lid)
        // .map(({ name }) => fromRecord(name))
        .map(({ name }) => DIV({ className: 'layer-name' }, fromRecord(name)));

// {
//     const { name, metadata } = getSynteticLayerInfoOption(lid);
//     if (name && metadata) {
//         return (DIV({ className: 'layer-name' }, fromRecord(name)));
//     }
//     return NODISPLAY();
// };

const renderHeader = (legendType: SubType, _lid: string, _propName: string) => {
    const titleComps: React.ReactNode[] = [
        SPAN({ className: 'app-title' }, tr.compose('legendBuilder')),
        renderLegendType(legendType),
    ];

    return DIV(
        { className: 'app-split-header' },
        H2({ className: 'app-split-title' }, ...titleComps),
        DIV(
            { className: 'app-split-tools' },
            layerName(_lid),
            layerTableSwitch(_lid),
            renderCloseButton()
        )
    );
};

export const renderInputColor = (
    get: Getter<string>,
    set: Setter<string>,
    label: string
) => DIV('style-tool color', LABEL('input-label', label, inputColor(get, set)));

const selectProp = (set: TextSetter) =>
    renderSelect<string>(
        'select-prop-for-label',
        getAlias,
        set,
        setoidString,
        tr.compose('columnName')
    );

const getPropList = (layerData: FeatureCollection) =>
    getLayerPropertiesKeys(layerData).concat('-').reverse();

const getFilteredPropList = (layerData: FeatureCollection) => {
    const legendType = queries.getLegendType();
    if (legendType === 'continuous') {
        return getLayerPropertiesKeysForNumbers(layerData)
            .concat('-')
            .reverse();
    }
    return getPropList(layerData);
};

export const renderPropSelect = (set: TextSetter, get: TextGetter) =>
    getCurrentLayerInfo()
        .chain(({ metadata }) =>
            getLayerData(metadata.uniqueResourceIdentifier).getOrElse(none)
        )
        .fold(NODISPLAY(), layerData => {
            return selectProp(set)(getPropList(layerData), some(get()));
        });

export const renderFilteredPropSelect = (set: TextSetter, get: TextGetter) =>
    getCurrentLayerInfo()
        .chain(({ metadata }) =>
            getLayerData(metadata.uniqueResourceIdentifier).getOrElse(none)
        )
        .fold(NODISPLAY(), layerData => {
            return selectProp(set)(getFilteredPropList(layerData), some(get()));
        });

const renderLegendType = (legendType: SubType) => {
    if (legendType === 'simple') {
        return SPAN(
            { className: 'legend-type-value' },
            tr.compose('legendTypeSimple')
        );
    } else {
        const label =
            legendType === 'continuous'
                ? tr.compose('legendTypeContinuous')
                : tr.compose('legendTypeDiscrete');

        return SPAN({ className: 'legend-type-value' }, label);
    }
};

const column = (
    className: string,
    title: string,
    ...children: React.ReactNode[]
): React.ReactNode =>
    DIV(
        { className: `column ${className}` },
        DIV({ className: 'column__title' }, DIV({ className: 'title' }, title)),
        ...children
    );

const renderBody = () => {
    const style = queries.getStyle();
    const legendType = queries.getLegendType();

    if (style !== null) {
        if (legendType === 'simple') {
            return DIV(
                { className: 'app-split-main' },
                column('picker', tr.compose('legendTypeSelect'), selectType()),
                column('settings', tr.compose('style'), renderStyleSettings()),
                column(
                    'label',
                    tr.compose('legendLabelHeader'),
                    renderLabelSettings()
                )
            );
        } else {
            const propName = queries.getSelectedMainName();
            if (propName === '') {
                return DIV(
                    { className: 'app-split-main' },
                    column(
                        'picker',
                        tr.compose('legendTypeSelect'),
                        selectType()
                    ),
                    column(
                        'settings',
                        tr.compose('columnPicker'),
                        selectMain()
                    ),
                    column(
                        'label',
                        tr.compose('legendLabelHeader'),
                        renderLabelSettings()
                    )
                );
            } else {
                if (legendType === 'discrete') {
                    return DIV(
                        { className: 'app-split-main' },
                        column(
                            'picker',
                            `${tr.compose('legendItems')} - ${getAlias(
                                propName
                            )}`,
                            discrete()
                        ),
                        column(
                            'settings',
                            tr.compose('style'),
                            renderStyleSettings()
                        ),
                        column(
                            'label',
                            tr.compose('legendLabelHeader'),
                            renderLabelSettings()
                        )
                    );
                } else {
                    return DIV(
                        { className: 'app-split-main' },
                        column(
                            'picker',
                            `${tr.compose('legendItems')} - ${getAlias(
                                propName
                            )}`,
                            continuous()
                        ),
                        column(
                            'settings',
                            tr.compose('style'),
                            renderStyleSettings()
                        ),
                        column(
                            'label',
                            tr.compose('legendLabelHeader'),
                            renderLabelSettings()
                        )
                    );
                }
            }
        }
    }

    return DIV({});
};

export const loaderAnim = () => DIV({ className: 'loader-anim__small' });

const render = () =>
    getCurrentLayerInfo().fold(
        DIV({ className: 'app-split-wrapper legend-builder' }),
        ({ info }) => {
            featureConfigEvents.ensureSelectedFeature();
            return DIV(
                { className: 'app-split-wrapper legend-builder' },
                renderHeader(
                    queries.getLegendType(),
                    info.id,
                    queries.getSelectedMainName()
                ),
                renderBody()
            );
        }
    );

// {
//                 const { info } = getCurrentLayerInfo();
//                 if(!info) {
//                     return DIV({ className: 'app-split-wrapper legend-builder' });
//                 }
//     featureConfigEvents.ensureSelectedFeature();
//                 return(
//                     DIV({ className: 'app-split-wrapper legend-builder' },
//                         renderHeader(queries.getLegendType(),
//                             info.id,
//                             queries.getSelectedMainName()),
//                         renderBody(info.id))
//         );
// };

export default render;

logger('loaded');
