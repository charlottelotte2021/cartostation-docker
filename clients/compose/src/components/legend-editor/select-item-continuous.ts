/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {
    DETAILS,
    DIV,
    H3,
    LABEL,
    SPAN,
    SUMMARY,
} from 'sdi/components/elements';
import {
    MessageRecord,
    isContinuous,
    ContinuousInterval,
    ContinuousStyle,
    foldRemote,
} from 'sdi/source';
import tr, { fromRecord } from 'sdi/locale';
import {
    attrOptions,
    inputNumber,
    inputText,
    options,
} from 'sdi/components/input';

import { editRecord, loaderAnim } from '.';
import queries from '../../queries/legend-editor';
import events from '../../events/legend-editor';
import { makeLabelAndIcon } from '../button';
import { renderInputColor, renderUpAndDownBtns } from '.';
import { wrapEditable } from '../editable';
import {
    loadDataInterval,
    setAutoClassNbDecimals,
    setAutoClassValue,
} from 'compose/src/events/legend-editor-continuous';

const makeClassesButton = makeLabelAndIcon('validate', 2, 'check', () =>
    tr.core('validate')
);
const addGroupButton = makeLabelAndIcon('add', 2, 'plus', () =>
    tr.compose('addInterval')
);

const renderAdd = (groups: ContinuousInterval[]) =>
    addGroupButton(() => {
        events.addItem();
        events.selectStyleGroup(groups.length - 1);
    });

// const formatTitle = (
//     props: React.AllHTMLAttributes<HTMLElement> & React.Attributes,
//     title: string
// ) => {
//     const isEmpty = title.trim().length === 0;
//     const text = isEmpty ? tr.compose('styleGroupDefaultName') : title;
//     return DIV({ className: 'group-title', ...props }, text);
// };

const renderIntervalActive = (i: ContinuousInterval, idx: number) => {
    const getLabel = () => i.label;
    const setLabel = (r: MessageRecord) => {
        events.setLabelForStyleInterval(idx, r);
    };
    // const removeGroupButton = makeRemove(`continuous-renderStyleGroup-${idx}`, 3, () => tr.compose('remove'), () => tr.compose('rmvMsgGeneric'));
    const removeGroupButton = makeLabelAndIcon('clear', 2, 'trash-alt', () =>
        tr.compose('remove')
    );
    const closeGroupButton = makeLabelAndIcon('close', 2, 'times', () =>
        tr.core('close')
    );

    const setLow = (val: number) => {
        if (!isNaN(val)) {
            events.setInterval(idx, val, i.high);
        }
    };
    const setHigh = (val: number) => {
        if (!isNaN(val)) {
            events.setInterval(idx, i.low, val);
        }
    };
    const k = `interval_${idx}`;
    const label = fromRecord(getLabel());

    return DIV(
        'group unfolded active',
        wrapEditable(
            getLabel(),
            k,
            inputText(
                attrOptions(
                    k,
                    () => label,
                    t => editRecord(getLabel, setLabel, t),
                    {
                        className: k,
                        placeholder:
                            label === ''
                                ? tr.compose('styleGroupDefaultName')
                                : label,
                    }
                )
            )
        ),
        // editable(`interval_${idx}`, getLabel, setLabel, formatTitle)(),
        DIV(
            'style-tool interval low',
            LABEL(
                'input-label',
                tr.compose('lowValue'),
                inputNumber(
                    options(`input-int-low-${idx}`, () => i.low, setLow)
                )
            )
        ),
        DIV(
            'style-tool interval high',
            LABEL(
                'input-label',
                tr.compose('highValue'),
                inputNumber(
                    options(`input-int-high-${idx}`, () => i.high, setHigh)
                )
            )
        ),
        DIV(
            'footer-actions',
            removeGroupButton(() => events.removeItem(idx)),
            closeGroupButton(() => events.clearStyleGroup())
        )
    );
};

const renderIntervalFolded = (
    i: ContinuousInterval,
    key: number,
    len: number
) => {
    let title = fromRecord(i.label);
    if (title === '') {
        title = tr.compose('styleGroupDefaultName');
    }
    return DIV(
        {
            key,
            className: 'group folded interactive',
            onClick: () => events.selectStyleGroup(key),
        },
        DIV(
            { className: 'group-title' },
            SPAN('label', title),
            renderUpAndDownBtns(key, len)
        )
    );
};

const renderInterval =
    (current: number) => (i: ContinuousInterval, key: number, len: number) => {
        if (key === current) {
            return renderIntervalActive(i, key);
        }
        return renderIntervalFolded(i, key, len);
    };

const renderAutoClass = (style: ContinuousStyle) =>
    DETAILS(
        { className: 'style-tool autoclass' },
        SUMMARY('', H3({}, tr.compose('autoClass'))),
        DIV(
            { className: 'autoclass-picker' },
            LABEL(
                'input-label',
                tr.compose('classNumber'),
                inputNumber(
                    options(
                        `input-auto-class-nb`,
                        queries.getAutoClassValue,
                        setAutoClassValue
                    )
                )
            )
        ),
        DIV(
            'nb-decimals',
            LABEL(
                'input-label',
                tr.compose('nbDecimals'),
                inputNumber(
                    options(
                        'input-nb-decimals',
                        queries.getAutoClassNbDecimals,
                        setAutoClassNbDecimals
                    )
                )
            )
        ),
        DIV(
            'style-tool color',
            renderInputColor(
                queries.getFirstGroupColor,
                events.setFirstGroupColor,
                tr.compose('firstIntervalColor')
            ),
            renderInputColor(
                queries.getLastGroupColor,
                events.setLastGroupColor,
                tr.compose('lastIntervalColor')
            )
        ),
        makeClassesButton(() => {
            loadDataInterval(style.propName);
        })
        // renderInputAlphaColor(
        //     'input-color-interval begin',
        //     tr.compose('fillColor'),
        //     () => makeColor(queries.getFirstGroupColor()),
        //     c => events.setFirstGroupColor(c.string())
        // ),
        // renderInputAlphaColor(
        //     'input-color-interval end',
        //     tr.compose('fillColor'),
        //     () => makeColor(queries.getLastGroupColor()),
        //     c => events.setLastGroupColor(c.string())
        // )
    );

const renderAddBtns = (style: ContinuousStyle) =>
    DIV('add-btn', renderAdd(style.intervals), renderAutoClass(style));

const renderList = (style: ContinuousStyle) =>
    DIV(
        'column__body value-picker-groups',
        ...style.intervals.map((int, i, array) =>
            renderInterval(queries.getSelectedStyleGroup())(
                int,
                i,
                array.length
            )
        ),
        renderAddBtns(style)
    );

const renderGroups = () => {
    const style = queries.getStyle();
    if (style && isContinuous(style)) {
        return foldRemote(
            () => renderList(style),
            () =>
                DIV(
                    'loader loader__small',
                    loaderAnim(),
                    tr.compose('loadingData')
                ),
            (err: string) => DIV('error', err),
            () => renderList(style)
        )(queries.getDataInterval());
    }

    return DIV({ className: 'column__body value-picker-groups' });
};

const render = () => renderGroups();

export default render;
