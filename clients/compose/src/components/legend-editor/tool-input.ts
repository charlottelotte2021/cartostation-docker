/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
// import * as Color from 'color';

import tr from 'sdi/locale';
import { DIV, LABEL } from 'sdi/components/elements';
import { inputNumber, InputOptions } from 'sdi/components/input';
import { PatternAngle } from 'sdi/source';

import queries from '../../queries/legend-editor';
import events from '../../events/legend-editor';

import { renderInputColor, renderPropSelect } from '.';
import { getCurrentLayerId } from 'compose/src/queries/app';

const logger = debug(`sdi:legend-editor/tool-input`);

type Getter<T> = () => T;
type Setter<T> = (v: T) => void;

type NumberGetter = () => number;
type NumberSetter = (a: number) => void;

// export const makeColor = (i: string | number, a?: number) => {
//     try {
//         if (a) {
//             return colord(i).alpha(a);
//         }
//         return colord(i);
//     } catch (e) {
//         return colord('rgb(0,0,0)');
//     }
// };

interface Selection<T> {
    [k: string]: T;
}

const renderSelection =
    <T>(series: Selection<T>[]) =>
    (className: string, get: Getter<T>, set: Setter<T>) => {
        const selected = get();

        const renderItem = (k: string, val: T) =>
            DIV(
                {
                    key: `serie-item-${k}`,
                    className: val === selected ? 'selected' : '',
                    onClick: () => set(val),
                },
                k
            );

        const renderSerie = (serie: Selection<T>) =>
            DIV(
                {
                    className: 'serie',
                },
                Object.keys(serie).map(k => renderItem(k, serie[k]))
            );

        return DIV(
            { className },
            DIV({ className: 'library' }, ...series.map(renderSerie))
        );
    };

const renderInputIcon = renderSelection([
    {
        [String.fromCodePoint(0xf111)]: 0xf111,
        [String.fromCodePoint(0xf1db)]: 0xf1db,
        [String.fromCodePoint(0xf10c)]: 0xf10c,
        [String.fromCodePoint(0xf192)]: 0xf192,
    },
]);

const renderInputPatternAngle = renderSelection<PatternAngle>([
    {
        '0°': 0,
        '45°': 45,
        '90°': 90,
        '135°': 135,
    },
]);

const renderInputNumberAttrs = (
    className: string,
    label: string,
    { key, get, set, attrs }: InputOptions<number>
) => {
    const update = (newVal: number) => {
        if (!isNaN(newVal)) {
            set(newVal);
        }
    };

    return DIV(
        { className: `style-tool ${className}` },
        LABEL(
            { className: 'input-label' },
            label,
            inputNumber({
                get,
                set: update,
                key: `legend-editor-input-nb-${key}-${getCurrentLayerId()}`,
                attrs,
                monitor: update,
            })
        )
    );
};

const renderInputNumber = (
    className: string,
    label: string,
    get: NumberGetter,
    set: NumberSetter
) =>
    renderInputNumberAttrs(className, label, {
        key: `legend-editor-input-nb-${label}-${getCurrentLayerId()}`,
        get,
        set,
        attrs: {},
    });

export const lineWidth = () => {
    return renderInputNumber(
        'size',
        tr.compose('lineWidth'),
        () => queries.getStrokeWidth(1),
        (n: number) => events.setStrokeWidth(n)
    );
};

export const lineColor = () =>
    renderInputColor(
        queries.getStrokeColor,
        events.setStrokeColor,
        tr.compose('lineColor')
    );

export const fillColor = () =>
    renderInputColor(
        queries.getFillColor,
        events.setFillColor,
        tr.compose('fillColor')
    );

export const pattern = () => {
    const p = queries.getPattern();
    if (!p) {
        return DIV(
            { className: 'style-tool hatch-wrapper' },
            DIV(
                {
                    className: 'hatch-pattern inactive',
                    onClick: () => events.setPattern(true),
                },
                tr.compose('usePattern')
            )
        );
    }
    return DIV(
        { className: 'style-tool hatch-wrapper' },
        DIV(
            {
                className: 'hatch-pattern active',
                onClick: () => events.setPattern(false),
            },
            tr.compose('usePattern')
        ),
        renderInputPatternAngle(
            'angle',
            queries.getPatternAngle,
            events.setPatternAngle
        )
    );
};

export const lineWidthForGroup = (idx: number) => {
    return renderInputNumber(
        'size',
        tr.compose('lineWidth'),
        () => queries.getStrokeWidthForGroup(idx, 1),
        (n: number) => events.setStrokeWidthForGroup(idx, n)
    );
};

export const lineColorForGroup = (idx: number) =>
    renderInputColor(
        () => queries.getStrokeColorForGroup(idx),
        c => events.setStrokeColorForGroup(idx, c),
        tr.compose('lineColor')
    );

export const fillColorForGroup = (idx: number) =>
    renderInputColor(
        () => queries.getFillColorForGroup(idx),
        c => events.setFillColorForGroup(idx, c),
        tr.compose('fillColor')
    );

export const patternForGroup = (idx: number) => {
    const p = queries.getPatternForGroup(idx);
    if (!p) {
        return DIV(
            { className: 'style-tool hatch-wrapper' },
            DIV(
                {
                    className: 'hatch-pattern inactive',
                    onClick: () => events.setPatternForGroup(idx, true),
                },
                tr.compose('usePattern')
            ),
            renderInputPatternAngle(
                'angle disabled',
                () => queries.getPatternAngleForGroup(idx),
                () => ''
            )
        );
    }
    return DIV(
        { className: 'style-tool hatch-wrapper' },
        DIV(
            {
                className: 'hatch-pattern active',
                onClick: () => events.setPatternForGroup(idx, false),
            },
            tr.compose('usePattern')
        ),
        renderInputPatternAngle(
            'angle',
            () => queries.getPatternAngleForGroup(idx),
            v => events.setPatternAngleForGroup(idx, v)
        )
    );
};

export const fontColor = () =>
    renderInputColor(
        queries.getFontColor,
        events.setFontColor,
        tr.compose('fontColor')
    );

export const fontSize = () => {
    return renderInputNumberAttrs('size', tr.compose('fontSize'), {
        key: 'fontSize',
        get: () => queries.getFontSize(),
        set: (n: number) => events.setFontSize(n),
        attrs: { min: 0 },
    });
};

export const offsetX = () => {
    return renderInputNumber(
        'size',
        tr.compose('offsetX'),
        () => queries.getOffsetXForLabel(),
        (n: number) => events.setOffsetXForLabel(n)
    );
};

export const offsetY = () => {
    return renderInputNumber(
        'size',
        tr.compose('offsetY'),
        () => queries.getOffsetYForLabel(),
        (n: number) => events.setOffsetYForLabel(n)
    );
};

export const labelResolution = () => {
    return renderInputNumberAttrs('size', tr.compose('labelZoom'), {
        key: `labelZoom`,
        get: () => queries.getZoomForLabel(),
        set: (n: number) => events.setZoomForLabel(n),
        attrs: { step: 0.2, min: 0, max: 30 },
    });
};

export const propSelectForLabel = () =>
    renderPropSelect(events.setPropNameForLabel, queries.getPropNameForLabel);

export const markerSizeForGroup = (idx: number) => {
    return renderInputNumber(
        'size',
        tr.compose('size'),
        () => queries.getMarkerSizeForGroup(idx),
        (n: number) => events.setMarkerSizeForGroup(idx, n)
    );
};

export const markerColorForGroup = (idx: number) =>
    renderInputColor(
        () => queries.getMarkerColorForGroup(idx),
        c => events.setMarkerColorForGroup(idx, c),
        tr.compose('pointColor')
    );

export const markerCodepointForGroup = (idx: number) => {
    return renderInputIcon(
        'style-tool picto-collection code',
        () => queries.getMarkerCodepointForGroup(idx),
        (n: number) => events.setMarkerCodepointForGroup(idx, n)
    );
};

export const markerSize = () => {
    return renderInputNumber(
        'size',
        tr.compose('size'),
        () => queries.getMarkerSize(),
        (n: number) => events.setMarkerSize(n)
    );
};

export const markerColor = () =>
    renderInputColor(
        queries.getMarkerColor,
        events.setMarkerColor,
        tr.compose('pointColor')
    );

export const markerCodepoint = () => {
    return renderInputIcon(
        'style-tool picto-collection code',
        () => queries.getMarkerCodepoint(),
        (n: number) => events.setMarkerCodepoint(n)
    );
};

logger('loaded');
