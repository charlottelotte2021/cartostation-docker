/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV, H2, SPAN } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { Attachment, IMapInfo } from 'sdi/source';
import {
    addAttachment,
    setAttachmentName,
    removeAttachment,
    setAttachmentUrl,
    moveAttachmentUp,
    moveAttachmentDown,
} from '../../events/attachments';

import { makeLabelAndIcon, makeRemoveIcon } from '../button';
import { getAttachment } from '../../queries/attachments';
import { attrOptions, inputText } from 'sdi/components/input';
import { editRecord } from '../legend-editor';
import { wrapEditable } from '../editable';
import renderMovable from 'sdi/components/movable';

const logger = debug('sdi:map-info/attachments');
// const nonEmptyString = fromPredicate<string>(s => s.length > 0);

// const addButton = makeIcon('add', 1, 'link', {
//     position: 'top-left',
//     text: () => tr.compose('attachmentAdd'),
// });

const addButton = makeLabelAndIcon('add', 2, 'plus', () =>
    tr.compose('attachmentAdd')
);

// const renderAttachmentEditableName = (
//     props: React.AllHTMLAttributes<HTMLElement> & React.Attributes,
//     name: string,
//     a: Attachment
// ) => {
//     return DIV(
//         { className: 'map-file-label' },
//         SPAN(
//             { className: 'file-label' },
//             A(
//                 {
//                     href: fromRecord(a.url),
//                     ...props,
//                 },
//                 nonEmptyString(name).fold(tr.compose('attachmentName'), n => n)
//             )
//         )
//     );
// };

// const renderAttachmentEditableUrl = (
//     props: React.AllHTMLAttributes<HTMLElement> & React.Attributes,
//     url: string,
//     _a: Attachment
// ) => {
//     return DIV(
//         { className: 'map-file-url' },
//         SPAN(
//             { ...props, className: 'file-url' },
//             nonEmptyString(url).fold(tr.compose('attachmentUrl'), u => u)
//         )
//     );
// };

// const renderAttachmentName =
//     (a: Attachment) => (props: React.ClassAttributes<Element>) =>
//         getAttachmentForm(a.id)
//             .map(f => renderAttachmentEditableName(props, f.name, a))
//             .getOrElse(DIV({}));

// const renderAttachmentUrl =
//     (a: Attachment) => (props: React.ClassAttributes<Element>) =>
//         getAttachmentForm(a.id)
//             .map(f => renderAttachmentEditableUrl(props, f.url, a))
//             .getOrElse(DIV({}));

const renderAddButton = () => addButton(() => addAttachment());

const inputAttachementName = (a: Attachment, k: string) =>
    inputText(
        attrOptions(
            `ata_name_${k}`,
            () => fromRecord(a.name),
            t =>
                editRecord(
                    () => a.name,
                    n => setAttachmentName(a.id, n),
                    t
                ),
            {
                className: `ata_name_${k}`,
                placeholder: tr.compose('attachmentName'),
            }
        )
    );
const inputAttachementUrl = (a: Attachment, k: string) =>
    inputText(
        attrOptions(
            `ata_url_${k}`,
            () => fromRecord(a.url),
            t =>
                editRecord(
                    () => a.url,
                    n => setAttachmentUrl(a.id, n),
                    t
                ),
            {
                className: k,
                placeholder: tr.compose('attachmentUrl'),
            }
        )
    );

const renderMoveAttachment = renderMovable(
    moveAttachmentUp,
    moveAttachmentDown
);

const attachments = (mapInfo: IMapInfo) =>
    mapInfo.attachments.map((k, index) =>
        getAttachment(k).map(a =>
            DIV(
                { className: 'map-file' },
                // editable(
                //     `ata_name_${k}`,
                //     () => a.name,
                //     n => setAttachmentName(a.id, n),
                //     renderAttachmentName(a)
                // )(),
                // editable(
                //     `ata_url_${k}`,
                //     () => a.url,
                //     n => setAttachmentUrl(a.id, n),
                //     renderAttachmentUrl(a)
                // )(),
                DIV(
                    'map-file--actions',
                    SPAN('link-label', `${tr.core('link')} ${index + 1}`),
                    DIV(
                        'action__wrapper',
                        renderMoveAttachment(index, mapInfo.attachments.length),
                        makeRemoveIcon(
                            `renderAttachmentEditable-${a.id}`,
                            3,
                            'trash-alt',
                            () => tr.compose('rmvMsgRemoveAttachment'),
                            {
                                position: 'top-left',
                                text: () => tr.compose('remove'),
                            }
                        )(() => removeAttachment(a.id))
                    )
                ),
                wrapEditable(
                    a.name,
                    `ata_name_${k}`,
                    inputAttachementName(a, k)
                ),
                wrapEditable(
                    a.url,
                    `ata_name_${k}_url`,
                    inputAttachementUrl(a, k)
                )
            )
        )
    );

const render = (mapInfo: IMapInfo) =>
    DIV(
        'map-attached-files',
        H2({}, tr.compose('links')),
        DIV(
            'description-wrapper',
            DIV('helptext', tr.compose('compose:externalLinkInfo'))
        ),
        renderAddButton(),
        ...attachments(mapInfo)
    );

export default render;

logger('loaded');
