/*
 *  Copyright (C) 2019 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { DIV, A } from 'sdi/components/elements';
import { helpText } from 'sdi/components/helptext';
import { IMapInfo } from 'sdi/source';
import tr from 'sdi/locale';
import { getRoot } from 'sdi/app';

import { makeLabel, makeRemove } from '../button';
import mapInfoEvents from '../../events/map-info';
import { deleteMapFromId } from '../../events/app';

const publishBtn = makeLabel('publish', 1, () =>
    tr.compose('compose:publishMap')
);
const unpublishBtn = makeLabel('publish', 2, () =>
    tr.compose('compose:unpublishMap')
);

const renderStatus = ({ status }: IMapInfo) => {
    if ('published' === status) {
        return DIV(
            { className: 'published' },
            // helpText(tr.compose('compose:helptext:mapPublished')),
            unpublishBtn(() => mapInfoEvents.mapStatus('draft'))
        );
    }
    return DIV(
        { className: 'toggle' },
        // helpText(tr.compose('compose:helptext:mapUnpublished')),
        publishBtn(() => mapInfoEvents.mapStatus('published'))
    );
};

const renderStatusHelptext = ({ status }: IMapInfo) => {
    if ('published' === status) {
        return helpText(tr.compose('compose:helptext:mapPublished'));
    }
    return helpText(tr.compose('compose:helptext:mapUnpublished'));
};

const renderPreviewLink = ({ id }: IMapInfo) =>
    DIV(
        {
            className: 'preview-link',
        },
        A(
            {
                title: tr.compose('preview'),
                target: '_blank',
                href: `${getRoot()}view/${id}`,
            },
            tr.compose('preview')
        )
    );

const renderRemove = (mapInfo: IMapInfo) =>
    DIV(
        { className: 'remove-map' },
        makeRemove(
            `remove-map-${mapInfo.id}`,
            3,
            () => tr.compose('compose:removeMap'),
            () => tr.compose('rmvMsgRemoveMap')
        )(() => deleteMapFromId(mapInfo.id as string))
    );

const mapPublishingTools = (mapInfo: IMapInfo) =>
    DIV(
        { className: 'map-publishing-tools' },
        renderStatusHelptext(mapInfo),
        renderStatus(mapInfo),
        renderPreviewLink(mapInfo),
        renderRemove(mapInfo)
    );

export default mapPublishingTools;

// const hasCategory =
//     (c: string, info: IMapInfo) => info.categories.indexOf(c) >= 0;

// const renderCategories =
//     (info: IMapInfo) => {
//         const categories = queries.getCategories();
//         const elements = categories.map((cat) => {
//             if (hasCategory(cat.id, info)) {
//                 return (
//                     DIV({
//                         className: 'category selected interactive',
//                         onClick: () => events.removeCategory(cat.id),
//                     }, fromRecord(cat.name)));
//             }
//             return (
//                 DIV({
//                     className: 'category interactive',
//                     onClick: () => events.addCategory(cat.id),
//                 }, fromRecord(cat.name)));
//         });

//         return (
//             DIV({ className: 'category-wrapper' }, ...elements));
//     };
