/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { fromNullable } from 'fp-ts/lib/Option';

import {
    ILayerInfo,
    getMessageRecord,
    Inspire,
    IMapInfo,
    makeRecord,
    MessageRecord,
} from 'sdi/source';
import tr, { formatNumber, fromRecord, Translated } from 'sdi/locale';
import { DETAILS, DIV, H2, H3, SPAN, SUMMARY } from 'sdi/components/elements';
import { divTooltipTop } from 'sdi/components/tooltip';

import legendPoint from 'sdi/components/legend/legend-point';
// import legendPoint from './legend-point';
import legendLinestring from 'sdi/components/legend/legend-linestring';
// import legendLinestring from './legend-linestring';
import legendPolygon from 'sdi/components/legend/legend-polygon';
// import legendPolygon from './legend-polygon';
import {
    getLayerData,
    getSynteticLayerInfoOption,
    getCurrentLayerId,
} from '../../queries/app';
import { clearCurrentStreamData } from '../../events/table';
import legendEvents from '../../events/legend-editor';
import legendQueries from '../../queries/legend-editor';
import { AppLayout } from '../../shape/types';
import { makeIcon, makeLabelAndIcon, makeRemoveLabelAndIcon } from '../button';
import { getDatasetMetadata } from '../../queries/metadata';
import { resetFeatureConfigEditor } from 'compose/src/events/feature-config';
import { wrapEditable } from '../editable';
import {
    attrOptions,
    inputText,
    renderCheckbox,
    renderSelect,
} from 'sdi/components/input';
import { IconName } from 'sdi/components/button/names';
import mapQueries from '../../queries/map';
import {
    setLayerVisibility,
    setCurrentLayerId,
    setLayout,
    resetLegendEditor,
} from '../../events/app';
import {
    reasonableScaleList,
    scaleList,
    scaleToZoom,
    tryScale,
    tryZoom,
    zoomToRoundScale,
} from 'sdi/map';
import { tryNumber } from 'sdi/util';
import { setoidNumber } from 'fp-ts/lib/Setoid';
import { editRecord } from '../legend-editor';
import { getLang } from 'sdi/app';

const addButton = makeLabelAndIcon('add', 2, 'table', () =>
    tr.compose('addLayer')
);

const upButton = makeIcon('move-up', 3, 'arrow-up', {
    position: 'top',
    text: () => tr.compose('moveLayerUp'),
});
const downButton = makeIcon('move-down', 3, 'arrow-down', {
    position: 'top',
    text: () => tr.compose('moveLayerDown'),
});
const featureEditButton = makeIcon('edit', 3, 'list-alt', {
    position: 'top-right',
    text: () => tr.compose('displayFeatureEdit'),
});
// const editButton = makeIcon('edit', 3, 'pencil');
// const resetLegendButton = makeRemoveIcon('reset', 3, 'refresh', () => tr.compose('rmvMsgResetLegend'));

const logger = debug('sdi:info/legend');

const renderLayerTitle = (info: ILayerInfo) => {
    const md = getDatasetMetadata(info.metadataId).fold(null, md => md);
    const label =
        md === null
            ? ('' as Translated)
            : fromRecord(getMessageRecord(md.resourceTitle));

    const wrap = wrapItem(md, (): boolean => {
        const zoom = mapQueries.getView().zoom;
        const minZoom = fromNullable(info.minZoom).getOrElse(0);
        const maxZoom = fromNullable(info.maxZoom).getOrElse(30);

        return info.visible && zoom >= minZoom && zoom <= maxZoom;
    });

    return DIV(
        {
            className: 'table-name',
            title: label,
        },
        divTooltipTop(label, {}, SPAN({}, ...wrap(label)))
    );
};

const wrapItem =
    (md: Inspire | null, visible: () => boolean) =>
        (...nodes: React.ReactNode[]) =>
            fromNullable(md).fold(nodes, md =>
                getLayerData(md.uniqueResourceIdentifier).fold(
                    err => [
                        SPAN(
                            {
                                className: 'load-error',
                                title: err,
                            },
                            ...nodes
                        ),
                    ],
                    o =>
                        o.fold(
                            nodes.concat(
                                SPAN({
                                    className: visible() ? 'loader-spinner' : '',
                                })
                            ),
                            () => nodes
                        )
                )
            );

const renderLegendItem = (info: ILayerInfo): React.ReactNode[] => {
    const md = getDatasetMetadata(info.metadataId);
    switch (info.style.kind) {
        case 'polygon-continuous':
        case 'polygon-discrete':
        case 'polygon-simple':
            return legendPolygon(info.style, info, md);
        case 'point-discrete':
        case 'point-simple':
        case 'point-continuous':
            return legendPoint(info.style, info, md);
        case 'line-simple':
        case 'line-discrete':
        case 'line-continuous':
            return legendLinestring(info.style, info, md);
        default:
            throw new Error('UnknownStyleKind');
    }
};

// const renderVisible =
//     (info: ILayerInfo) => {
//         const isVisible = info.visible;
//         const icon: IconName = isVisible ? 'eye' : 'eye-slash';
//         const toolTipMessage = isVisible ? tr.compose('makeUnvisible') : tr.compose('makeVisible');
//         const buttonFn = makeIcon('view', 3, icon, { position: 'top-right', text: toolTipMessage });
//         return buttonFn(() => setLayerVisibility(info.id, !isVisible));
//     };

const renderVisible = (info: ILayerInfo) => {
    const isVisible = info.visible;
    const icon: IconName = isVisible ? 'eye' : 'eye-slash';
    const toolTipMessage = isVisible
        ? tr.compose('makeUnvisible')
        : tr.compose('makeVisible');
    const labelMessage = isVisible
        ? tr.compose('layerIsVisible')
        : tr.compose('layerIsHidden');
    const buttonFn = makeIcon('view', 3, icon, {
        position: 'top-right',
        text: toolTipMessage,
    });
    return DIV(
        'edit-layer-visibility',
        buttonFn(() => setLayerVisibility(info.id, !isVisible)),
        SPAN('label', labelMessage)
    );
};

const handleFeatureEditButton = (info: ILayerInfo) => () => {
    setCurrentLayerId(info.id);
    resetFeatureConfigEditor();
    setLayout(AppLayout.FeatureConfig);
};

const renderFeatureEditButton = (info: ILayerInfo) =>
    DIV(
        'edit-feature-infos',
        featureEditButton(handleFeatureEditButton(info)),
        SPAN('label', tr.compose('editFeatureLabel'))
    );

const handleResetButton = (info: ILayerInfo) => () => {
    setCurrentLayerId(info.id);
    resetLegendEditor();
    legendEvents.resetLegend();
};

const renderResetLegendButton = (info: ILayerInfo) => {
    const button = makeRemoveLabelAndIcon(
        `reset-layer-${info.id}`,
        3,
        'times',
        () => tr.compose('resetLegend'),
        () => tr.compose('rmvMsgResetLegend')
        // { position: 'top', text: tr.compose('resetLegend') }
    );
    return button(handleResetButton(info));
};

const getLabel = (info: ILayerInfo) =>
    info.legend !== null ? info.legend : makeRecord();
const setLabel = (info: ILayerInfo) => (r: MessageRecord) =>
    legendEvents.setLegendLabel(info.id, r);

const inputLabel = (info: ILayerInfo) => {
    const key = `layer_legend_label_${info.id}_${getLang()}`;
    return wrapEditable(
        getLabel(info),
        key,
        inputText(
            attrOptions(
                key,
                () => fromRecord(getLabel(info)),
                l => editRecord(() => getLabel(info), setLabel(info), l),
                {
                    className: key,
                    placeholder: tr.compose('layerLegendDefaultLabel'),
                }
            )
        )
    );
};

const maxScale = scaleList.reduce(
    (acc, v) => Math.min(acc, v),
    Number.POSITIVE_INFINITY
); //max uses min because scale is in fact the denominator of the scale
const minScale = scaleList.reduce(
    (acc, v) => Math.max(acc, v),
    Number.NEGATIVE_INFINITY
);

const renderScale = (scale: number) => {
    if (scale === minScale || scale === maxScale) {
        return tr.compose('noRestriction');
    }
    return `1/${formatNumber(scale)}`;
};

const renderZoomRange = (lid: string) => {
    const g = () => getSynteticLayerInfoOption(lid).map(s => s.info);

    const getMin = () => g().map(i => i.minZoom || 0);
    const getMinScale = () =>
        getMin()
            .chain(z => tryZoom(z).map(z => zoomToRoundScale(z)))
            .chain(s => tryNumber(s));

    const getMax = () => g().map(i => i.maxZoom || 30);
    const getMaxScale = () =>
        getMax()
            .chain(z => tryZoom(z).map(z => zoomToRoundScale(z)))
            .chain(s => tryNumber(s));

    const setMin = (n: number) => {
        tryScale(n).map(s =>
            legendEvents.setZoomRange(scaleToZoom(s), getMax().getOrElse(30))
        );
    };

    const setMax = (n: number) => {
        tryScale(n).map(s =>
            legendEvents.setZoomRange(getMin().getOrElse(0), scaleToZoom(s))
        );
    };

    // const minSelect = renderSelect(
    //     setoidNumber,
    //     s => renderScale(`min-zoom-${lid}`, s),
    //     setMin
    // );
    // const maxSelect = renderSelect(
    //     setoidNumber,
    //     s => renderScale(`max-zoom-${lid}`, s),
    //     setMax
    // );
    const minScaleList = [minScale]
        .concat(reasonableScaleList)
        .filter((v, i, arr) => arr.indexOf(v) === i);
    const minSelect = renderSelect(
        `min-zoom-${lid}`,
        renderScale,
        setMin,
        setoidNumber
    );
    const maxScaleList = [maxScale]
        .concat(reasonableScaleList.reverse())
        .filter((v, i, arr) => arr.indexOf(v) === i);
    const maxSelect = renderSelect(
        `max-zoom-${lid}`,
        renderScale,
        setMax,
        setoidNumber
    );

    return DIV(
        'zoom-range__wrapper',
        DIV(
            '',
            // SPAN('icon', nameToString('search')),
            SPAN('zoom-range__label', tr.compose('zoomRangeLabel'))
        ),
        DIV(
            { className: 'zoom-range' },
            divTooltipTop(
                tr.compose('tooltip:zoomMin'),
                { className: 'zoom-range-input-box' },
                DIV({ className: 'label' }, tr.compose('minZoomShort')),
                minSelect(minScaleList, getMinScale())
                // minSelect(reasonableScaleList.concat(minScale), getMinScale()),
            ),
            divTooltipTop(
                tr.compose('tooltip:zoomMax'),
                { className: 'zoom-range-input-box' },
                DIV({ className: 'label' }, tr.compose('maxZoomShort')),
                maxSelect(maxScaleList, getMaxScale())
            )
        )
    );
};

// {
//     const { info } = getSynteticLayerInfoOption(lid);
//     if (info) {
//         const getMin =
//             () => getInfo(lid).fold(0, i => i.minZoom || 0);
//         const getMax =
//             () => getInfo(lid).fold(0, i => i.maxZoom || 30);
//         const setMin =
//             (n: number) => {
//                 legendEvents.setZoomRange(lid, n, getMax());
//             };
//         const setMax =
//             (n: number) => {
//                 legendEvents.setZoomRange(lid, getMin(), n);
//             };
//         const minInput = inputNumber(
//             getMin, setMin, { key: `min-zoom-${lid}` });
//         const maxInput = inputNumber(
//             getMax, setMax, { key: `max-zoom-${lid}` });

//         return (
//             DIV({ className: 'zoom-range' },
//                 DIV({ className: 'zoom-range-input-box' },
//                     DIV({ className: 'label' }, tr.compose('minZoomShort')),
//                     minInput),
//                 DIV({ className: 'zoom-range-input-box' },
//                     DIV({ className: 'label' }, tr.compose('maxZoomShort')),
//                     maxInput))
//         );
//     }
//     return DIV();
// };

const renderDeleteButton = (info: ILayerInfo) => {
    const removeButton = makeRemoveLabelAndIcon(
        `legend::renderDeleteButton-${info.id}`,
        3,
        'trash-alt',
        () => tr.compose('remove'),
        () => tr.compose('rmvMsgDeletLegendItem')
        // { position: 'top-left', text: tr.compose('remove') }
    );
    return DIV(
        { className: 'remove-layer' },
        removeButton(() => {
            legendEvents.removeLayer(info);
            setLayout(AppLayout.MapAndInfo);
        })
    );
};

const renderUpButton = (info: ILayerInfo) =>
    upButton(() => {
        legendEvents.moveLayerUp(info.id);
    });

const renderDownButton = (info: ILayerInfo) =>
    downButton(() => {
        legendEvents.moveLayerDown(info.id);
    });

const renderUpButtonDisabled = () => upButton(() => { }, 'disabled');
const renderDownButtonDisabled = () => downButton(() => { }, 'disabled');

const setLegendHidding = (hidding: boolean) =>
    hidding === true
        ? legendEvents.setLegendVisibility(false)
        : legendEvents.setLegendVisibility(true);
const legendVisibilityCheckbox = renderCheckbox(
    'legend-visibility-checkbox',
    () => tr.compose('hideLegend'),
    setLegendHidding
);

const renderOrder = (info: ILayerInfo, idx: number, len: number) => {
    const order: React.ReactNode[] = [];
    if (len === 1) {
        order.push();
    } else if (idx === 0) {
        order.push(renderUpButtonDisabled(), renderDownButton(info));
    } else if (idx === len - 1) {
        order.push(renderUpButton(info), renderDownButtonDisabled());
    } else {
        order.push(renderUpButton(info), renderDownButton(info));
    }
    return DIV({ className: 'order-box' }, ...order);
};

const renderTools = (info: ILayerInfo) =>
    DETAILS(
        { className: 'legend-block-tools' },
        SUMMARY('', H3('wrapper-head', tr.compose('visibilityOptions'))),
        renderFeatureEditButton(info),
        renderVisible(info),
        renderZoomRange(info.id),
        legendVisibilityCheckbox(!legendQueries.getLegendVisibility())
    );

const renderRemoveBtns = (info: ILayerInfo) =>
    DIV(
        'reset__wrapper',
        renderResetLegendButton(info),
        renderDeleteButton(info)
    );

const renderLayer = (info: ILayerInfo, idx: number, layers: ILayerInfo[]) => {
    const items = renderLegendItem(info);
    const active = getCurrentLayerId() === info.id ? 'active' : '';
    return DIV(
        {
            className: `legend-block ${active}`,
            // probably hacky : couldn't intergate handleEditLayer
            onClick: () => {
                setCurrentLayerId(info.id);
                resetLegendEditor();
                setLayout(AppLayout.LegendEditor);
            },
        },
        DIV(
            'title__wrapper',
            renderLayerTitle(info),
            renderOrder(info, idx, layers.length)
        ),
        inputLabel(info),
        renderTools(info),
        ...items,
        renderRemoveBtns(info)
    );
};

const renderAddButton = () => {
    return addButton(() => {
        clearCurrentStreamData();
        setLayout(AppLayout.LayerSelect);
    });
};

const reverse = (a: ILayerInfo[]): ILayerInfo[] =>
    a.reduceRight<ILayerInfo[]>((acc, v) => acc.concat([v]), []);

const render = (mapInfo: IMapInfo) => {
    const blocks = reverse(mapInfo.layers).map(renderLayer);
    return DIV(
        'map-layers',
        H2({}, tr.compose('mapLegend')),
        DIV(
            { className: 'description-wrapper' },
            DIV({ className: 'helptext' }, tr.compose('compose:addLayerInfo'))
        ),
        renderAddButton(),
        ...blocks
    );
};

export default render;

logger('loaded');
