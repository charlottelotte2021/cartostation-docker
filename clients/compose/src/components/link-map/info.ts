import { DIV, H2 } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';

import { setLayout } from 'compose/src/events/app';
import { AppLayout } from 'compose/src/shape/types';
import { getLinkedMaps } from 'compose/src/queries/map-info';
// import { makeIcon } from '../button';
import { makeLabelAndIcon } from '../button';

// const addButton = makeIcon('add', 1, 'map', {
//     position: 'top-left',
//     text: () => tr.compose('compose:addLinkedMap'),
// });

const addButton = makeLabelAndIcon('add', 2, 'plus', () =>
    tr.compose('compose:addLinkedMap')
);

const renderLinks = () =>
    getLinkedMaps('forward').map(link =>
        DIV({ className: 'link' }, fromRecord(link.title))
    );

const renderLinksBackward = () =>
    getLinkedMaps('backward').map(link =>
        DIV({ className: 'link backward' }, fromRecord(link.title))
    );

const renderAddButton = () =>
    // divTooltipTopLeft(
    //     tr.compose('compose:addLinkedMap'),
    DIV(
        {},
        addButton(() => setLayout(AppLayout.MapLink))
    );

export const render = () =>
    DIV(
        { className: 'map-related-maps' },
        H2({}, tr.compose('relatedMapsLabel')),
        DIV(
            { className: 'description-wrapper' },
            DIV({ className: 'helptext' }, tr.compose('compose:infoRelatedMap'))
        ),
        renderAddButton(),
        DIV(
            { className: 'related-maps' },
            ...renderLinks(),
            ...renderLinksBackward()
        )
    );
