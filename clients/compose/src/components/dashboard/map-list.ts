/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { ReactNode } from 'react';

import tr, { fromRecord, formatDate } from 'sdi/locale';
import { DIV, IMG, SPAN } from 'sdi/components/elements';
import { IMapInfo } from 'sdi/source';

import { getUserMaps } from '../../queries/app';
import { newMap } from '../../events/app';
import { makeLabel, makeLabelAndIcon } from '../button';
import { navigateMap } from '../../events/route';
import { stringToParagraphs } from 'sdi/util';

const selectMap = (mid?: string) => () => {
    if (mid) {
        navigateMap(mid);
    }
};

const addMap = () => {
    newMap();
};

const editButton = makeLabelAndIcon('edit', 2, 'pencil-alt', () =>
    tr.compose('editMap')
);
const addButton = makeLabel('add', 1, () => tr.compose('createMap'));

const renderAdd = () =>
    DIV(
        { className: 'dashboard-map-item add' },
        DIV(
            { className: 'app-infos' },
            stringToParagraphs(tr.compose('infoStudio'))
        ),
        addButton(addMap)
    );

const renderItems = (maps: IMapInfo[]) =>
    maps.map(map => {
        const elements: ReactNode[] = [];
        elements.push(
            DIV({ className: 'publication-status' }, tr.compose(map.status)),
            DIV({ className: 'dashboard-map-title' }, fromRecord(map.title))
        );

        if (map.imageUrl) {
            elements.push(
                DIV(
                    {
                        className: 'image',
                    },
                    IMG({ src: `${map.imageUrl}?size=small`, alt: '' })
                )
            );
        }
        elements.push(
            DIV(
                { className: 'dashboard-date-updated' },
                SPAN({}, tr.compose('lastModified')),
                formatDate(new Date(map.lastModified))
            ),
            editButton(selectMap(map.id))
        );

        return DIV(
            { className: `dashboard-map-item ${map.status}` },
            ...elements
        );
    });

const render = () =>
    DIV(
        { className: 'dashboard-maps' },
        renderAdd(),
        ...renderItems(getUserMaps())
    );

export default render;
