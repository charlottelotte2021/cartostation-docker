// /*
//  *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
//  *
//  *  This program is free software: you can redistribute it and/or modify
//  *  it under the terms of the GNU General Public License as published by
//  *  the Free Software Foundation, version 3 of the License.
//  *
//  *  This program is distributed in the hope that it will be useful,
//  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  *  GNU General Public License for more details.
//  *
//  *  You should have received a copy of the GNU General Public License
//  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//  */

// import { fromNullable, none } from 'fp-ts/lib/Option';

// import { dispatchK, observe } from 'sdi/shape';
// import { initialTableState, tableEvents, TableDataRow } from 'sdi/components/table';
// import { FeatureCollection } from 'sdi/source';
// import { scopeOption } from 'sdi/lib';

// import { getCurrentLayerInfo, getLayerData } from '../queries/app';
// import { setCurrentFeatureData } from './app';

// const table = dispatchK('component/table/features');

// observe('app/current-layer', () => {
//     table(initialTableState);
// });

// export const layerTableEvents = tableEvents(table);
// export const metadataTableEvents = tableEvents(table);

// export const selectFeatureRow =
//     (idx: number) => layerTableEvents.select(idx);

// export const resetLayerTable =
//     () => layerTableEvents.reset();

import { makeTable2Manip } from 'sdi/components/table2/layer';
import {
    getCurrentLayerInfo,
    getSynteticLayerInfoOption,
    getLayerData,
} from '../queries/app';
import { queryK, dispatchK, dispatch, observe } from 'sdi/shape';
import { none, fromNullable } from 'fp-ts/lib/Option';
import { Feature, FeatureCollection } from 'sdi/source';
import { TableDataRow } from 'sdi/components/table2';
import { scopeOption } from 'sdi/lib';
import { setCurrentFeatureData } from './app';

export const tableDispatch = dispatchK('component/table/features');

const findFeature = (fc: FeatureCollection, id: string | number) =>
    fromNullable(fc.features.find(f => f.id === id));

export const selectFeatureFromRow = (row: TableDataRow, selected: number) => {
    scopeOption()
        .let(
            'meta',
            getCurrentLayerInfo().map(s => s.metadata)
        )
        .let('layer', s =>
            getLayerData(s.meta.uniqueResourceIdentifier).getOrElse(none)
        )
        .let('feature', s => findFeature(s.layer, row.from))
        .map(({ feature }) => {
            setCurrentFeatureData(feature);
            tableDispatch(tableState => ({ ...tableState, selected }));
        });
};

export const updateLayer = (uri: string, features: Feature[]) =>
    dispatch('data/layers', layers => {
        const layer = layers[uri];
        if (layer !== undefined) {
            features.forEach(feature => {
                if (layer.features.findIndex(f => f.id === feature.id) < 0) {
                    layer.features.push(feature);
                }
            });
        }
        return layers;
    });

export const {
    layerObserver,
    tableObserver,
    clearCurrentStreamData,
    // exportCSV,
} = makeTable2Manip(
    getCurrentLayerInfo,
    getSynteticLayerInfoOption,
    getLayerData,
    () => none,
    queryK('component/table/features'),
    queryK('data/features/stream'),
    () => none,
    dispatchK('data/features/stream'),
    updateLayer,
    false
);

observe('app/current-layer', layerObserver);
observe('component/table/features', tableObserver);
