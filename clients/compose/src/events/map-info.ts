/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { dispatch } from 'sdi/shape';
import { getApiUrl } from 'sdi/app';
import { MapStatus } from 'sdi/source';

import { MapInfoIllustrationState } from '../shape/types';
import { getCurrentMap } from '../queries/app';
import { putMap, upload, postLink, deleteLink } from '../remote';
import { fromNullable } from 'fp-ts/lib/Option';
import { getLinks } from '../queries/map-info';

const events = {
    showImg() {
        dispatch(
            'app/map-info/illustration',
            () => MapInfoIllustrationState.showImage
        );
    },

    generatingSelectedImgPreview() {
        dispatch(
            'app/map-info/illustration',
            () => MapInfoIllustrationState.generateSelectedImagePreview
        );
    },

    showSelectedImg() {
        dispatch(
            'app/map-info/illustration',
            () => MapInfoIllustrationState.showSelectedImage
        );
    },

    uploadImg(img: File) {
        const mid = getCurrentMap();

        dispatch('app/map-info/illustration', () => {
            window.setTimeout(() => {
                upload('/documents/images/', img)
                    .then(data => {
                        dispatch('data/maps', maps => {
                            const idx = maps.findIndex(m => m.id === mid);

                            if (idx >= 0) {
                                const map = maps[idx];
                                const endpoint = getApiUrl(`maps/${mid}`);
                                map.imageUrl = data.url;
                                setTimeout(() => {
                                    putMap(endpoint, map);
                                }, 1);
                            }
                            return maps;
                        });
                        events.showImg();
                    })
                    .catch(() => {
                        events.showSelectedImg();
                    });
            }, 1);
            return MapInfoIllustrationState.uploadSelectedImage;
        });
    },

    mapStatus(status: MapStatus) {
        const mid = getCurrentMap();
        dispatch('data/maps', maps => {
            const idx = maps.findIndex(m => m.id === mid);

            if (idx !== -1) {
                const m = maps[idx];
                m.status = status;
                setTimeout(() => {
                    putMap(getApiUrl(`maps/${mid}`), m);
                }, 1);
            }

            return maps;
        });
    },
};

export default events;

export const addLinkMap = (target: string) =>
    fromNullable(getCurrentMap()).map(source =>
        postLink(getApiUrl('map/links'), { source, target }).then(link =>
            dispatch('data/links', allLinks => {
                const links = source in allLinks ? allLinks[source] : [];
                allLinks[source] = links.concat([link]);
                return allLinks;
            })
        )
    );

export const removeLinkMap = (target: string) =>
    fromNullable(getCurrentMap()).map(source =>
        fromNullable(getLinks('forward').find(l => l.target === target)).map(
            link =>
                deleteLink(getApiUrl('map/links'), link.id).then(_ =>
                    dispatch('data/links', allLinks => {
                        const links =
                            source in allLinks ? allLinks[source] : [];
                        allLinks[source] = links.filter(l => l.id !== link.id);
                        return allLinks;
                    })
                )
        )
    );
