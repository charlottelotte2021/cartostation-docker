/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { fromNullable } from 'fp-ts/lib/Option';

import { assign, dispatch } from 'sdi/shape';
import {
    defaultStyle,
    ILayerInfo,
    IMapInfo,
    isDiscrete,
    addAutoFilledGroupStyle,
    remoteLoading,
    RemoteResource,
    remoteSuccess,
    remoteError,
    remoteNone,
    Feature,
    streamFieldName,
    streamFieldType,
    DiscreteGroup,
    makeRecord,
} from 'sdi/source';

import {
    getCurrentLayerData,
    getCurrentLayerId,
    getCurrentLayerInfo,
    getCurrenFields,
} from '../queries/app';
import queries from '../queries/legend-editor';
import { DistinctValues, fetchDistinctValues } from '../remote';
import { getFeaturePropOption, tryNumber } from 'sdi/util';
import events, { getLayer, saveMap, getCurrentMap } from './legend-editor';
import { findTerm, getLang } from 'sdi/app';
import { fromRecord, Translated } from 'sdi/locale';

import debug = require('debug');

const logger = debug('sdi:legend-editor-discrete');

export const getDiscreteStyleGroup = (
    layer: ILayerInfo | null,
    idx: number | null
) => {
    if (
        layer &&
        idx !== null &&
        isDiscrete(layer.style) &&
        idx < layer.style.groups.length
    ) {
        return layer.style.groups[idx];
    }

    return null;
};

const setDistinctValues = (rdv: RemoteResource<DistinctValues>) =>
    assign('component/legend-editor/distinct-values', rdv);

export const clearDistinctValues = () =>
    assign('component/legend-editor/distinct-values', remoteNone);

const getOccurences = (features: Feature[], propName: string) => {
    const occurences: { [key: string]: number } = {};
    features
        .map(f => getFeaturePropOption<string | number>(f, propName))
        .map(pOpt =>
            pOpt.map(p => {
                if (typeof p === 'number' ? !isNaN(p) : p != '')
                    occurences[p] = (occurences[p] || 0) + 1;
            })
        );
    return occurences;
};

const getDistinctValuesList = (
    features: Feature[],
    propName: string
): DistinctValues => {
    const occurences = getOccurences(features, propName);
    return Object.keys(getOccurences(features, propName)).map(m => ({
        value: m,
        count: occurences[m],
    }));
};

export const setDistinctValuesFromData = (propName: string) => {
    getCurrentLayerData()
        .map(ldEither =>
            ldEither
                .map(ldOpt => ldOpt.map(ld => ld.features).getOrElse([]))
                .getOrElse([])
        )
        .map(f => getDistinctValuesList(f, propName))
        .map(dv => {
            setDistinctValues(remoteSuccess(dv));
            makeDiscreteClasses(dv, propName);
        });
};

export const loadDistinctValues = (propName: string) => {
    setDistinctValues(remoteLoading);
    getCurrentLayerInfo().map(li =>
        fromNullable(li.metadata.dataStreamUrl).map(url =>
            fetchDistinctValues(url, propName)
                .then(dv => {
                    events.resetLegendForTypeAndCol('discrete', propName);
                    setDistinctValues(remoteSuccess(dv));
                    makeDiscreteClasses(dv, propName);
                })
                .catch(err => setDistinctValues(remoteError(err)))
        )
    );
};

// const defaultLabel = (v: string | number) => {
//     const l = getLang();
//     if (l === 'fr') {
//         return makeRecord(v.toString());
//     } else if (l === 'nl') {
//         return makeRecord(v.toString());
//     }
//     return makeRecord();
// };

const termToRecord = (tid: number) =>
    findTerm(tid)
        .map(t => t.name)
        .getOrElse(makeRecord(tid.toString(), tid.toString()));

const discreteClassInner = (
    lid: string,
    layer: ILayerInfo,
    distinctValues: DistinctValues,
    propName: string
) => {
    const style = { ...layer.style };
    if (isDiscrete(style)) {
        distinctValues.map(v => {
            const defaultRecord = makeRecord(
                v.value.toString(),
                v.value.toString()
            );
            let value = makeRecord();
            switch (fieldType(propName)) {
                case 'term':
                    value =
                        typeof v.value === 'number'
                            ? termToRecord(v.value)
                            : tryNumber(v.value)
                                  .map(n => termToRecord(n))
                                  .getOrElse(defaultRecord);
                    break;
                default:
                    value = defaultRecord;
            }
            addAutoFilledGroupStyle(style, distinctValues.length, value, [
                v.value.toString(),
            ]);
        });
        saveMap(lid, style);
    }
};

export const removeDiscreteStyleGroupValue = (k: number) => {
    const lid = getCurrentLayerId();
    if (lid) {
        dispatch('data/maps', (maps: IMapInfo[]) => {
            const layer = getLayer(getCurrentMap(maps), lid);
            const gt = queries.getGeometryType();
            if (layer && gt) {
                const gidx = queries.getSelectedStyleGroup();
                const group = getDiscreteStyleGroup(layer, gidx);

                if (group) {
                    group.values.splice(k, 1);
                }
                saveMap(lid, layer.style);
            }
            return maps;
        });
    }
};

export const addDiscreteStyleGroupValue = (value: number | string) => {
    const lid = getCurrentLayerId();
    if (lid) {
        dispatch('data/maps', (maps: IMapInfo[]) => {
            const layer = getLayer(getCurrentMap(maps), lid);
            const gt = queries.getGeometryType();
            if (layer && gt && isDiscrete(layer.style)) {
                const gidx = queries.getSelectedStyleGroup();
                const group = getDiscreteStyleGroup(layer, gidx);

                if (group) {
                    group.values.push(value);
                }
                saveMap(lid, layer.style);
            }
            return maps;
        });
    }
    dispatch('component/legend-editor', state => {
        state.styleGroupEditedValue = null;
        return state;
    });
};

export const selectDiscrete = () => {
    const lid = getCurrentLayerId();

    if (lid) {
        const gt = queries.getGeometryType();
        if (gt) {
            saveMap(lid, defaultStyle(gt, 'discrete'));
        }
    }
};
export const makeDiscreteClasses = (
    distinctValues: DistinctValues,
    propName: string
) => {
    const lid = getCurrentLayerId();
    if (lid) {
        dispatch('data/maps', maps => {
            fromNullable(getLayer(getCurrentMap(maps), lid)).map(layer => {
                discreteClassInner(lid, layer, distinctValues, propName);
            });

            return maps;
        });
    }
};

const fieldType = (propName: string) =>
    getCurrenFields()
        .chain(f =>
            fromNullable(f.find(field => streamFieldName(field) === propName))
        )
        .map(f => streamFieldType(f))
        .getOrElse('string');

const title = (acc: string, val: string | number, link: string) =>
    acc + ` ${val}` + link;

const previousTitle = (
    acc: string,
    val: string | number,
    link: string,
    isLastElem: boolean
) => (isLastElem ? acc : acc + ` ${val}` + link);

const termToString = (tid: number) =>
    findTerm(tid)
        .map(t => fromRecord(t.name))
        .getOrElse('' as Translated);

const defaultGroupTitle = (
    values: (string | number)[],
    propName: string,
    isCurrentLabel: boolean
) => {
    return values
        .reduce((acc, val, i) => {
            const isEnd = isCurrentLabel
                ? i < values.length - 1
                : i < values.length - 2;
            const link = isEnd ? ' & ' : '';
            let value = val;
            switch (fieldType(propName)) {
                case 'term':
                    value =
                        typeof val === 'number'
                            ? termToString(val)
                            : tryNumber(val)
                                  .map(n => termToString(n))
                                  .getOrElse(val.toString() as Translated);
                    break;
                default:
                    val.toString();
            }
            return isCurrentLabel
                ? title(acc.toString(), value, link)
                : previousTitle(
                      acc.toString(),
                      value,
                      link,
                      values.length - 1 === i
                  );
        }, '')
        .toString();
};

// const previousDefaultLabel = (group: DiscreteGroup, propName: string) => {
//     const v = group.values;
//     return v.reduce((acc, val, i) => {
//         const link = i < v.length - 2 ? ' & ' : '';
//         if (fieldType(propName) === 'term') {
//             const term = tryNumber(val).map(v => findTerm(v));
//             return i === v.length - 1 ? acc : acc + ` ${term}` + link;
//         }
//         return i === v.length - 1 ? acc : acc + ` ${val}` + link;
//     }, '') as Translated;
// };

export const makeGroupTitle = (group: DiscreteGroup, propName: string) => {
    const label = group.label;
    if (
        fromRecord(label) === '' ||
        fromRecord(label) === defaultGroupTitle(group.values, propName, false)
    ) {
        switch (getLang()) {
            case 'fr':
                return (label.fr = defaultGroupTitle(
                    group.values,
                    propName,
                    true
                ));
            case 'nl':
                return (label.nl = defaultGroupTitle(
                    group.values,
                    propName,
                    true
                ));
        }
    }
    return fromRecord(group.label);
};

logger(`loaded`);
