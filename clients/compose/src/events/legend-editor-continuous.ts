/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { fromNullable } from 'fp-ts/lib/Option';

import { assign, dispatch } from 'sdi/shape';
import {
    addDefaultIntervalStyle,
    defaultStyle,
    ILayerInfo,
    isContinuous,
    makeRecord,
    remoteLoading,
    RemoteResource,
    remoteSuccess,
    remoteError,
    remoteNone,
} from 'sdi/source';

import { getCurrentLayerId, getCurrentLayerInfo } from '../queries/app';
import queries from '../queries/legend-editor';
import { DataInterval, fetchDataInterval } from '../remote';
import events, { getLayer, saveMap, getCurrentMap } from './legend-editor';

export const getContinuousStyleInterval = (
    layer: ILayerInfo | null,
    idx: number | null
) => {
    if (
        layer &&
        idx !== null &&
        isContinuous(layer.style) &&
        idx < layer.style.intervals.length
    ) {
        return layer.style.intervals[idx];
    }

    return null;
};

export const setDataInterval = (rdi: RemoteResource<DataInterval>) =>
    assign('component/legend-editor/data-interval', rdi);

export const clearDataInterval = () =>
    assign('component/legend-editor/data-interval', remoteNone);

export const loadDataInterval = (propName: string) => {
    setDataInterval(remoteLoading);
    getCurrentLayerInfo().map(li =>
        fromNullable(li.metadata.dataStreamUrl).map(url =>
            fetchDataInterval(url, propName)
                .then(i => {
                    events.resetLegendForTypeAndCol('continuous', propName);
                    setDataInterval(remoteSuccess(i));
                    makeContinuousClasses(i);
                })
                .catch(err => setDataInterval(remoteError(err)))
        )
    );
};

// round to 2 significant digit
const roundToN = (num: number, decimals: number) => {
    const d = Math.pow(10, decimals);
    return Math.round(num * d) / d;
};

const continuousClassInner = (
    lid: string,
    n: number,
    layer: ILayerInfo,
    dataInterval: DataInterval
) => {
    const style = { ...layer.style };
    if (isContinuous(style)) {
        const step = (dataInterval.max - dataInterval.min) / n;
        const decimals = queries.getAutoClassNbDecimals();
        for (let i = 0; i < n; i += 1) {
            addDefaultIntervalStyle(
                style,
                i,
                n,
                queries.getFirstGroupColor(),
                queries.getLastGroupColor()
            );
            const interval = style.intervals[i];
            (interval.low = dataInterval.min + i * step), decimals;
            (interval.high = interval.low + step), decimals;
            const label = `${roundToN(interval.low, decimals)} - ${roundToN(
                interval.high,
                decimals
            )}`;
            interval.label = makeRecord(label, label, label);
        }
        saveMap(lid, style);
    }
};

export const selectContinuous = () => {
    const lid = getCurrentLayerId();

    if (lid) {
        const gt = queries.getGeometryType();
        if (gt) {
            saveMap(lid, defaultStyle(gt, 'continuous'));
        }
    }
};
export const setAutoClassValue = (n: number) => {
    dispatch('component/legend-editor', state => {
        state.autoClassValue = Math.max(Math.min(7, n), 2);
        state.autoClassValue = Math.max(n, 2);
        return state;
    });
};

export const setAutoClassNbDecimals = (n: number) => {
    dispatch('component/legend-editor', state => {
        state.autoClassNbDecimals = n;
        return state;
    });
};

export const makeContinuousClasses = (interval: DataInterval) => {
    const lid = getCurrentLayerId();
    const n = queries.getAutoClassValue();
    if (lid && n > 1) {
        dispatch('data/maps', maps => {
            fromNullable(getLayer(getCurrentMap(maps), lid)).map(layer => {
                continuousClassInner(lid, n, layer, interval);
            });

            return maps;
        });
    }
};
