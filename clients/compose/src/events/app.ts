/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { v4 as uuid } from 'uuid';
import { Setoid } from 'fp-ts/lib/Setoid';

import { dispatch, observe } from 'sdi/shape';
import {
    defaultStyle,
    Feature,
    ILayerInfo,
    IMapInfo,
    Inspire,
    MessageRecord,
    makeRecord,
    FieldsDescriptorList,
    fetchIO,
    StreamingMetaIO,
} from 'sdi/source';
import { getApiUrl } from 'sdi/app';
import { addLayer, removeLayerAll, addFeaturesToLayer } from 'sdi/map';
import {
    addStream,
    clearStreams,
    mapStream,
    pushStreamExtent,
} from 'sdi/geodata-stream';
import { uniq, isNotNullNorUndefined } from 'sdi/util';

import {
    fetchAlias,
    fetchAllDatasetMetadata,
    fetchCategories,
    fetchDatasetMetadata,
    fetchLayer,
    fetchUser,
    fetchBaseLayer,
    postLayerInfo,
    postMap,
    putMap,
    deleteMap,
    fetchAllLinks,
    fetchAllMaps,
} from '../remote';
import {
    getCurrentMap,
    getMapInfo,
    getSynteticLayerInfoOption,
    getLayerData,
} from '../queries/app';
import { getView } from '../queries/map';

import { AppLayout } from '../shape/types';
import { initialLegendEditorState } from '../components/legend-editor/index';
import { navigateMap, navigateHome } from './route';
import { mapName } from '../components/map';
import { fromNullable, fromPredicate } from 'fp-ts/lib/Option';
import { Extent } from 'ol/extent';
import { activity } from 'sdi/activity';

const logger = debug('sdi:events/app');
export const activityLogger = activity('compose');

observe('app/current-layer', cl => logger(`app/current-layer = ${cl}`));

export const toDataURL = (f: File) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => {
            const result = reader.result;
            if (result) {
                resolve(result);
            } else {
                reject();
            }
        };
        reader.onerror = reject;
        reader.onabort = reject;
        reader.readAsDataURL(f);
    });
};

const inspireS: Setoid<Inspire> = {
    equals(a, b) {
        return a.id === b.id;
    },
};

const uniqInspire = uniq(inspireS);

observe('port/map/view', view =>
    fromNullable(view.extent).map(e =>
        mapStream(e, ({ uri, lid }, extent) =>
            loadLayerDataExtent(lid, uri, extent)
        )
    )
);

const loadMapFromInfo = (info: IMapInfo) => {
    removeLayerAll(mapName);
    clearStreams();
    info.layers.forEach(loadLayer(info));
    loadLinks(info.id);
};

const layerInRange = (info: ILayerInfo) => {
    const low = fromNullable(info.minZoom).getOrElse(0);
    const high = fromNullable(info.maxZoom).getOrElse(30);
    const zoom = getView().zoom;
    return info.visible && zoom > low && zoom < high;
};

const whenInRange = fromPredicate(layerInRange);

const { markVisibilityPending, delVisibilityPending, isVisibilityPending } =
    (() => {
        const register: Set<string> = new Set();

        const hash = (a: string, b: string) => `${a}/${b}`;
        const markVisibilityPending = (mid: string, lid: string) =>
            register.add(hash(mid, lid));

        const delVisibilityPending = (mid: string, lid: string) =>
            register.delete(hash(mid, lid));

        const isVisibilityPending = (mid: string, lid: string) =>
            register.has(hash(mid, lid));

        return {
            markVisibilityPending,
            delVisibilityPending,
            isVisibilityPending,
        };
    })();

const prepareStreamedLayer = (
    mapInfo: IMapInfo,
    layerInfo: ILayerInfo,
    md: Inspire,
    fields: FieldsDescriptorList
) => {
    dispatch('data/layers', state => {
        if (!(md.uniqueResourceIdentifier in state)) {
            state[md.uniqueResourceIdentifier] = {
                type: 'FeatureCollection',
                fields,
                features: [],
            };
        }
        return state;
    });
    if (layerInfo.visible && layerInRange(layerInfo)) {
        fromNullable(getView().extent).map(e => {
            loadLayerDataExtent(layerInfo.id, md.uniqueResourceIdentifier, e);
        });
    } else {
        markVisibilityPending(mapInfo.id, layerInfo.id);
    }
};

const loadLayer = (mapInfo: IMapInfo) => (layerInfo: ILayerInfo) => {
    fetchDatasetMetadata(getApiUrl(`metadatas/${layerInfo.metadataId}`))
        .then(md => {
            dispatch('data/datasetMetadata', state =>
                state.filter(i => i.id != md.id).concat(md)
            );

            addLayer(
                mapName,
                () => getSynteticLayerInfoOption(layerInfo.id),
                () => getLayerData(md.uniqueResourceIdentifier)
            );

            if (isNotNullNorUndefined(md.dataStreamUrl)) {
                addStream({
                    uri: md.uniqueResourceIdentifier,
                    lid: layerInfo.id,
                });
                // if (layerInfo.visible && layerInRange(layerInfo)) {
                //     fromNullable(getView().extent)
                //         .map((e) => {
                //             loadLayerDataExtent(layerInfo.id, md.uniqueResourceIdentifier, e);
                //         });
                // }
                fetchIO(StreamingMetaIO, md.dataStreamUrl)
                    .then(({ fields }) => {
                        prepareStreamedLayer(mapInfo, layerInfo, md, fields);
                    })
                    .catch(() => {
                        logger(
                            `[ERROR] failed to get fields for ${md.uniqueResourceIdentifier}`
                        );
                        prepareStreamedLayer(mapInfo, layerInfo, md, []);
                    });
            } else {
                loadLayerData(md.uniqueResourceIdentifier);
            }
        })
        .catch(err =>
            logger(`Failed to load MD ${layerInfo.metadataId}: ${err}`)
        );
};

const loadLayerDataExtent = (layerId: string, url: string, bbox: Extent) =>
    getSynteticLayerInfoOption(layerId).map(({ info }) =>
        whenInRange(info).map(info => {
            pushStreamExtent(bbox, { lid: layerId, uri: url });
            fetchLayer(
                `${url}?bbox=${bbox[0]},${bbox[1]},${bbox[2]},${bbox[3]}`
            )
                .then(layer => {
                    if (layer.features !== null) {
                        addFeaturesToLayer(mapName, info, layer.features);
                        dispatch('data/layers', state => {
                            if (url in state) {
                                state[url].features = state[
                                    url
                                ].features.concat(layer.features);
                            } else {
                                state[url] = layer;
                            }
                            return state;
                        });
                    }
                })
                .catch(err => {
                    logger(`Failed to load features at ${url} due to ${err}`);
                    dispatch('remote/errors', state => ({
                        ...state,
                        [url]: `${err}`,
                    }));
                });
        })
    );

const loadLayerData = (url: string) => {
    logger(`loadLayerData(${url})`);
    fetchLayer(url)
        .then(layer => {
            dispatch('data/layers', state => {
                logger(`Put layer ${url} on state`);
                state[url] = layer;
                return state;
            });
        })
        .catch(err => {
            logger(`Failed to load layer at ${url} due to ${err}`);
            dispatch('remote/errors', state => ({ ...state, [url]: `${err}` }));
        });
};

const makeMap = (): Omit<IMapInfo, 'id'> => {
    return {
        status: 'draft',
        title: makeRecord(),
        description: makeRecord(),
        attachments: [],
        layers: [],
        categories: [],
        lastModified: Date.now(),
        url: '',
        baseLayer: 'urbis.irisnet.be/urbis_gray',
    };
};

export const loadUser = (url: string) => {
    fetchUser(url).then(user => {
        logger(`got user`);
        dispatch('data/user', () => user);
    });
};

export const loadAllMaps = () => {
    fetchAllMaps(getApiUrl(`maps`)).then(maps => {
        dispatch('data/maps', () => maps);
    });
};

// export const loadAllLinks = () => {
//     fetchAllLinks(getApiUrl(`map/links`))
//         .then((links) => {
//             dispatch('data/links', (ls) => {
//                 const allLinks: typeof ls = {};
//                 links.forEach((link) => {
//                     if (!(link.source in allLinks)) {
//                         allLinks[link.source] = [];
//                     }
//                     allLinks[link.source].push(link);
//                 });
//                 return allLinks;
//             });
//         });
// };

const loadLinks = (mid: string) =>
    fetchAllLinks(getApiUrl(`map/links?mid=${mid}`)).then(links => {
        dispatch('data/links', data => ({ ...data, [mid]: links }));
    });

export const setLayout = (l: AppLayout) => {
    dispatch('app/layout', () => l);
};

export const signalReadyMap = () => {
    dispatch('app/map-ready', () => true);
};

export const loadBaseLayer = (id: string, url: string) => {
    fetchBaseLayer(url).then(bl => {
        dispatch('data/baselayers', state => ({ ...state, [id]: bl }));
    });
};

export const loadCategories = (url: string) => {
    fetchCategories(url).then(categories => {
        dispatch('data/categories', () => categories);
    });
};

export const loadAlias = (url: string) => {
    fetchAlias(url).then(alias => {
        dispatch('data/alias', () => alias);
    });
};

export const loadDatasetMetadata = (url: string) => {
    fetchDatasetMetadata(url).then(md =>
        dispatch('data/datasetMetadata', state =>
            state.filter(i => i.id !== md.id).concat([md])
        )
    );
};

export const loadAllDatasetMetadata = (done?: () => void) => {
    dispatch('component/table/features', ts => ({ ...ts, loaded: 'loading' }));

    fetchAllDatasetMetadata(getApiUrl('metadatas'))(
        frame => {
            dispatch('data/datasetMetadata', state =>
                uniqInspire(state.concat(frame.results))
            );
            dispatch('component/table/features', ts => ({
                ...ts,
                loaded: 'loading',
            }));
            dispatch('component/splash', () =>
                Math.floor((frame.page * 100) / frame.total)
            );
        },
        () => {
            dispatch('component/table/features', ts => ({
                ...ts,
                loaded: 'done',
            }));
            if (done) {
                done();
            }
        }
    );
};

export const setLayerVisibility = (id: string, visible: boolean) => {
    const mid = getCurrentMap();
    dispatch('data/maps', maps => {
        // const idx = maps.findIndex(m => m.id === mid);
        // if (idx !== -1) {
        //     const m = maps[idx];
        //     m.layers.forEach((l) => {
        //         if (l.id === id) {
        //             l.visible = visible;
        //         }
        //     });
        //     setTimeout(() => {
        //         putMap(getApiUrl(`maps/${mid}`), m);
        //     }, 1);
        // }
        const mapInfo = maps.find(m => m.id === mid);
        if (mapInfo) {
            mapInfo.layers.forEach(l => {
                if (l.id === id) {
                    l.visible = visible;
                    if (isVisibilityPending(mapInfo.id, l.id)) {
                        fromNullable(getView().extent).map(e =>
                            mapStream(e, ({ uri, lid }, extent) =>
                                loadLayerDataExtent(lid, uri, extent)
                            )
                        );
                        delVisibilityPending(mapInfo.id, l.id);
                    } else if (!visible) {
                        markVisibilityPending(mapInfo.id, l.id);
                    }
                    setTimeout(() => {
                        putMap(getApiUrl(`maps/${mid}`), mapInfo);
                    }, 1);
                }
            });
        }
        return maps;
    });
};

export const setCurrentMapId = (id: string) => {
    clearMap();
    dispatch('app/current-map', () => id);
    fromNullable(getMapInfo()).map(loadMapFromInfo);
};

export const clearMap = () => {
    dispatch('app/current-map', () => null);
    dispatch('app/current-layer', () => null);
    dispatch('app/current-feature', () => null);
};

export const setCurrentLayerId = (id: string) => {
    dispatch('app/current-layer', () => id);
    dispatch('app/current-feature', () => null);
    dispatch('component/table/features', state => {
        state.selected = -1;
        return state;
    });
};

export const setCurrentFeatureData = (data: Feature) => {
    dispatch('app/current-feature', () => data);
};

export const unsetCurrentFeatureData = () => {
    dispatch('app/current-feature', () => null);
};

export const setMapTitle = (r: MessageRecord) => {
    const mid = getCurrentMap();
    dispatch('data/maps', maps => {
        const idx = maps.findIndex(m => m.id === mid);
        if (idx !== -1) {
            const m = maps[idx];
            m.title = r;
            setTimeout(() => {
                putMap(getApiUrl(`maps/${mid}`), m);
            }, 1);
        }
        return maps;
    });
};

export const setMapDescription = (r: MessageRecord) => {
    const mid = getCurrentMap();
    dispatch('data/maps', maps => {
        const idx = maps.findIndex(m => m.id === mid);
        if (idx !== -1) {
            const m = maps[idx];
            m.description = r;
            setTimeout(() => {
                putMap(getApiUrl(`maps/${mid}`), m);
            }, 1);
        }
        return maps;
    });
};

export const addMapLayer = (metadata: Inspire) => {
    const mid = getCurrentMap();
    dispatch('data/maps', maps => {
        const idx = maps.findIndex(m => m.id === mid);
        if (idx !== -1) {
            const mapInfo = maps[idx];
            const layerId = uuid();
            const layerInfo: ILayerInfo = {
                id: layerId,
                metadataId: metadata.id,
                visible: true,
                featureViewOptions: { type: 'default' },
                style: defaultStyle(metadata.geometryType),
                group: null,
                legend: null,
                minZoom: 0,
                maxZoom: 30,
                visibleLegend: true,
                layerInfoExtra: null,
                opacitySelector: false,
            };
            postLayerInfo(getApiUrl(`layerinfos`), layerInfo)
                .then(result => {
                    logger(
                        `Recorded Layer ${result.id} / ${result.metadataId}`
                    );
                    mapInfo.layers.push(result);

                    putMap(getApiUrl(`maps/${mid}`), mapInfo)
                        .then(() => {
                            addLayer(
                                mapName,
                                () => getSynteticLayerInfoOption(result.id),
                                () =>
                                    getLayerData(
                                        metadata.uniqueResourceIdentifier
                                    )
                            );
                            setCurrentLayerId(result.id);
                            loadLayer(mapInfo)(result);
                        })
                        .catch(err => logger(`addMapLayer map ${err}`));
                })
                .catch(err => logger(`addMapLayer layer info ${err}`));
        }
        return maps;
    });
};

export const newMap = () => {
    postMap(getApiUrl(`maps`), makeMap()).then(map => {
        if (map.id) {
            const mid = map.id;
            dispatch('data/maps', state => state.concat([map]));
            dispatch('data/user', user => {
                if (user && user.id) {
                    user.maps = user.maps.concat([mid]);
                }
                return user;
            });
            navigateMap(mid);
        }
    });
};

export const deleteMapFromId = (id: string) => {
    navigateHome();
    deleteMap(getApiUrl(`maps/${id}`))
        .then(() =>
            dispatch('data/maps', state => state.filter(m => m.id !== id))
        )
        .catch(err => logger(`Failed to delete map ${id} ${err}`));
};

export const removeCategory = (c: string) => {
    const mid = getCurrentMap();
    dispatch('data/maps', maps => {
        const idx = maps.findIndex(m => m.id === mid);
        if (idx !== -1) {
            const m = maps[idx];
            m.categories = m.categories.filter(mc => mc !== c);
            setTimeout(() => {
                putMap(getApiUrl(`maps/${mid}`), m);
            }, 1);
        }
        return maps;
    });
};

export const addCategory = (c: string) => {
    const mid = getCurrentMap();
    dispatch('data/maps', maps => {
        const idx = maps.findIndex(m => m.id === mid);
        if (idx !== -1) {
            const m = maps[idx];
            m.categories.push(c);
            setTimeout(() => {
                putMap(getApiUrl(`maps/${mid}`), m);
            }, 1);
        }

        return maps;
    });
};

export const removeMapInfoIllustration = () => {
    const mid = getCurrentMap();
    dispatch('data/maps', maps => {
        const idx = maps.findIndex(m => m.id === mid);

        if (idx !== -1) {
            maps[idx].imageUrl = undefined;

            setTimeout(() => {
                putMap(getApiUrl(`maps/${mid}`), maps[idx]);
            }, 1);
        }

        return maps;
    });
};

export const setMapBaseLayer = (id: string) => {
    const mid = getCurrentMap();
    dispatch('data/maps', maps => {
        const idx = maps.findIndex(m => m.id === mid);

        if (idx !== -1) {
            const m = maps[idx];
            m.baseLayer = id;

            setTimeout(() => {
                putMap(getApiUrl(`maps/${mid}`), m);
            }, 1);
        }

        return maps;
    });
};

export const resetLegendEditor = () => {
    dispatch('component/legend-editor', () => initialLegendEditorState());
};

logger('loaded');
