/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { fromNullable } from 'fp-ts/lib/Option';

import { assign, dispatch } from 'sdi/shape';
import { getApiUrl, getLang } from 'sdi/app';
import {
    addDefaultGroupStyle,
    addDefaultIntervalStyle,
    ContinuousInterval,
    defaultStyle,
    DiscreteGroup,
    getGroup,
    getInterval,
    ILayerInfo,
    IMapInfo,
    isContinuous,
    isDiscrete,
    isLabeled,
    isLineStyle,
    isMarkered,
    isPointStyle,
    isPolygonStyle,
    isSimple,
    LineDiscreteGroup,
    LineInterval,
    MessageRecord,
    PointDiscreteGroup,
    LabelStyle,
    PointMarker,
    PointStyleConfig,
    PolygonDiscreteGroup,
    PolygonInterval,
    StyleConfig,
    PatternAngle,
    makeRecord,
    SubType,
} from 'sdi/source';

import {
    getCurrentLayerId,
    getCurrentMap as appGetCurrentMap,
} from '../queries/app';
import queries from '../queries/legend-editor';
import { saveStyle, syncMap } from '../util/app';
import { putMap } from '../remote';

import { clearDataInterval } from './legend-editor-continuous';
import { clearDistinctValues } from './legend-editor-discrete';

const logger = debug('sdi:events/legend-editor');

export const saveMap = saveStyle(dispatch);

export const getMap = (
    maps: IMapInfo[] | undefined | null,
    mid: string | null
) => {
    if (maps && mid) {
        const idx = maps.findIndex(m => m.id === mid);
        if (idx !== -1) {
            return maps[idx];
        }
    }

    return null;
};

export const getLayer = (map: IMapInfo | null, lid: string | null) => {
    if (map && lid) {
        const layer = map.layers.find(l => l.id === lid);

        if (layer) {
            return layer;
        }
    }

    return null;
};

type GroupEditFn = (g: DiscreteGroup | ContinuousInterval) => void;
// type IntervalEditFn = (g: ContinuousInterval) => void;
type StyleEditFn = <T extends StyleConfig>(g: T, l: string) => void;
type PointStyleEditFn = <T extends PointStyleConfig>(g: T, l: string) => void;

const defaultPointLabel = (): LabelStyle => ({
    propName: makeRecord(),
    align: 'center',
    baseline: 'alphabetic',
    resLimit: 3,
    color: '#000000',
    size: 12,
});

const defaultPointMarker = (): PointMarker => ({
    codePoint: 0xf111,
    color: '#000000',
    size: 10,
});

const updateGroupStyle = (idx: number, f: GroupEditFn) => {
    const lid = getCurrentLayerId();
    if (lid) {
        dispatch('data/maps', maps => {
            const layer = getLayer(getCurrentMap(maps), lid);
            if (layer) {
                const style = { ...layer.style };
                if (isDiscrete(style)) {
                    const group = getGroup(style, idx);
                    if (group) {
                        f(<DiscreteGroup>group);
                    }
                    saveMap(lid, style);
                } else if (isContinuous(style)) {
                    const interval = getInterval(style, idx);
                    if (interval) {
                        f(<ContinuousInterval>interval);
                    }
                    saveMap(lid, style);
                }
            }
            return maps;
        });
    }
};

// const updateIntervalStyle =
//     (idx: number, f: IntervalEditFn) => {
//         const lid = getCurrentLayerId();
//         if (lid) {
//             dispatch('data/maps', (maps) => {
//                 const layer = getLayer(getCurrentMap(maps), lid);
//                 if (layer) {
//                     const style = { ...layer.style };
//                     if (isContinuous(style)) {
//                         const interval = getInterval(style, idx);
//                         if (interval) {
//                             f(<ContinuousInterval>interval);
//                         }
//                         saveMap(lid, style);
//                     }
//                 }
//                 return maps;
//             });
//         }
//     };

const updateStyle = (f: StyleEditFn) => {
    const lid = getCurrentLayerId();
    if (lid) {
        dispatch('data/maps', maps => {
            const layer = getLayer(getCurrentMap(maps), lid);
            if (layer) {
                const style = { ...layer.style };
                if (isDiscrete(style)) {
                    f(style, lid);
                } else if (isContinuous(style)) {
                    f(style, lid);
                } else {
                    f(style, lid);
                }
                saveMap(lid, style);
            }
            return maps;
        });
    }
};

const updateStyleLabel = (f: StyleEditFn) => {
    updateStyle((s, lid) => {
        if (!s.label) {
            s.label = defaultPointLabel();
        }
        f(s, lid);
    });
};

const updatePointStyleMarker = (f: PointStyleEditFn) => {
    updateStyle((s, lid) => {
        if (isPointStyle(s) && isSimple(s)) {
            if (!isMarkered(s)) {
                s.marker = defaultPointMarker();
            }
            f(s, lid);
        }
    });
};

export const getCurrentMap = (maps: IMapInfo[]) => {
    return getMap(maps, appGetCurrentMap());
};

const events = {
    setMainName(propName: string) {
        if (propName !== queries.getSelectedMainName()) {
            updateStyle(s => {
                if (isContinuous(s) || isDiscrete(s)) {
                    s.propName = propName;
                }
            });
        }
    },

    resetLegend() {
        clearDataInterval();
        clearDistinctValues();
        const lid = getCurrentLayerId();
        const gt = queries.getGeometryType();
        if (lid && gt) {
            saveMap(lid, defaultStyle(gt));
        }
    },

    resetLegendForTypeAndCol(subtype: SubType, propName: string) {
        const lid = getCurrentLayerId();
        const gt = queries.getGeometryType();
        if (lid && gt) {
            saveMap(lid, defaultStyle(gt, subtype, propName));
        }
    },

    setZoomRange(min: number | undefined, max: number | undefined) {
        const lid = getCurrentLayerId();
        const mid = appGetCurrentMap();
        dispatch('data/maps', maps => {
            const map = maps.find(m => m.id === mid);
            if (map) {
                const info = map.layers.find(l => l.id === lid);
                if (info) {
                    info.maxZoom = max;
                    info.minZoom = min;
                    syncMap(map);
                }
            }
            return maps;
        });
    },

    setLegendVisibility(visibleLegend: boolean) {
        const lid = getCurrentLayerId();
        const mid = appGetCurrentMap();
        dispatch('data/maps', maps => {
            const mapInfo = maps.find(m => m.id === mid);
            if (mapInfo) {
                mapInfo.layers.forEach(l => {
                    if (l.id === lid) {
                        l.visibleLegend = visibleLegend;
                        putMap(getApiUrl(`maps/${mid}`), mapInfo);
                    }
                });
            }
            return maps;
        });
    },

    setExportable(lid: string, exportable: boolean) {
        const mid = appGetCurrentMap();
        dispatch('data/maps', maps => {
            const map = maps.find(m => m.id === mid);
            if (map) {
                const info = map.layers.find(l => l.id === lid);
                if (info) {
                    info.layerInfoExtra = { exportable: exportable };
                    syncMap(map);
                }
            }
            return maps;
        });
    },

    setLegendLabel(lid: string, label: MessageRecord) {
        const mid = appGetCurrentMap();
        dispatch('data/maps', maps => {
            const map = maps.find(m => m.id === mid);
            if (map) {
                const info = map.layers.find(l => l.id === lid);
                if (info) {
                    info.legend = label;
                    syncMap(map);
                }
            }
            return maps;
        });
    },

    moveLayerUp(lid: string) {
        const mid = appGetCurrentMap();
        dispatch('data/maps', maps => {
            const map = maps.find(m => m.id === mid);
            if (map) {
                const currentIndex = map.layers.findIndex(l => l.id === lid);
                const info = map.layers[currentIndex];
                if (currentIndex >= 0 && currentIndex < map.layers.length - 1) {
                    map.layers[currentIndex] = map.layers[currentIndex + 1];
                    map.layers[currentIndex + 1] = info;
                    syncMap(map);
                }
            }
            return maps;
        });
    },

    moveLayerDown(lid: string) {
        const mid = appGetCurrentMap();
        dispatch('data/maps', maps => {
            const map = maps.find(m => m.id === mid);
            if (map) {
                const currentIndex = map.layers.findIndex(l => l.id === lid);
                const info = map.layers[currentIndex];
                if (currentIndex > 0 && currentIndex < map.layers.length) {
                    map.layers[currentIndex] = map.layers[currentIndex - 1];
                    map.layers[currentIndex - 1] = info;
                    syncMap(map);
                }
            }
            return maps;
        });
    },

    removeLayer(info: ILayerInfo) {
        const mid = appGetCurrentMap();
        const lid = info.id;
        dispatch('data/maps', maps => {
            const map = maps.find(m => m.id === mid);
            if (map) {
                const currentIndex = map.layers.findIndex(l => l.id === lid);
                if (currentIndex >= 0 && currentIndex < map.layers.length) {
                    map.layers.splice(currentIndex, 1);
                    syncMap(map);
                }
            }
            return maps;
        });
    },

    // Simple Style
    setStrokeWidth(w: number) {
        updateStyle(s => {
            if (isSimple(s) && (isPolygonStyle(s) || isLineStyle(s))) {
                s.strokeWidth = w;
            }
        });
    },

    setStrokeColor(c: string) {
        updateStyle(s => {
            if (isSimple(s) && (isPolygonStyle(s) || isLineStyle(s))) {
                s.strokeColor = c;
            }
        });
    },

    setFillColor(c: string) {
        updateStyle(s => {
            if (isSimple(s) && isPolygonStyle(s)) {
                s.fillColor = c;
            }
        });
    },

    setFirstGroupColor(c: string) {
        assign('component/legend-editor/interval-color/begin', c);
    },
    setLastGroupColor(c: string) {
        assign('component/legend-editor/interval-color/end', c);
    },
    setPattern(p: boolean) {
        updateStyle(s => {
            if (isSimple(s) && isPolygonStyle(s)) {
                s.pattern = p;
            }
        });
    },

    setPatternAngle(a: PatternAngle) {
        updateStyle(s => {
            if (isSimple(s) && isPolygonStyle(s)) {
                s.patternAngle = a;
            }
        });
    },

    // Group Style

    setLabelForStyleGroup(idx: number, r: MessageRecord) {
        updateGroupStyle(idx, g => {
            g.label = r;
        });
    },

    setLabelForStyleInterval(idx: number, r: MessageRecord) {
        updateGroupStyle(idx, i => {
            i.label = r;
        });
    },

    setInterval(idx: number, low: number, high: number) {
        updateGroupStyle(idx, (g: ContinuousInterval) => {
            g.low = low;
            g.high = high;
        });
    },

    setStrokeWidthForGroup(idx: number, w: number) {
        updateStyle((s, lid) => {
            if (isLineStyle(s) || isPolygonStyle(s)) {
                if (isDiscrete(s)) {
                    const group = getGroup(s, idx);
                    if (group) {
                        (<LineDiscreteGroup | PolygonDiscreteGroup>(
                            group
                        )).strokeWidth = w;
                    }
                    saveMap(lid, s);
                } else if (isContinuous(s)) {
                    const group = getInterval(s, idx);
                    if (group) {
                        (<PolygonInterval | LineInterval>group).strokeWidth = w;
                    }
                    saveMap(lid, s);
                }
            }
        });
    },

    setStrokeColorForGroup(idx: number, c: string) {
        logger(`setStrokeColorForGroup ${idx} ${c}`);
        updateStyle((s, lid) => {
            if (isLineStyle(s) || isPolygonStyle(s)) {
                if (isDiscrete(s)) {
                    const group = getGroup(s, idx);
                    if (group) {
                        (<LineDiscreteGroup | PolygonDiscreteGroup>(
                            group
                        )).strokeColor = c;
                    }
                    saveMap(lid, s);
                } else if (isContinuous(s)) {
                    const group = getInterval(s, idx);
                    if (group) {
                        (<PolygonInterval | LineInterval>group).strokeColor = c;
                    }
                    saveMap(lid, s);
                }
            }
        });
    },

    setFillColorForGroup(idx: number, c: string) {
        logger(`setFillColorForGroup ${idx} ${c}`);
        updateStyle((s, lid) => {
            if (isPolygonStyle(s)) {
                if (isDiscrete(s)) {
                    const group = getGroup(s, idx);
                    if (group) {
                        (<PolygonDiscreteGroup>group).fillColor = c;
                    }
                    saveMap(lid, s);
                } else if (isContinuous(s)) {
                    const group = getInterval(s, idx);
                    if (group) {
                        (<PolygonInterval>group).fillColor = c;
                    }
                    saveMap(lid, s);
                }
            }
        });
    },

    setPatternForGroup(idx: number, p: boolean) {
        updateStyle((s, lid) => {
            if (isPolygonStyle(s)) {
                if (isDiscrete(s)) {
                    const group = getGroup(s, idx);
                    if (group) {
                        (<PolygonDiscreteGroup>group).pattern = p;
                    }
                    saveMap(lid, s);
                } else if (isContinuous(s)) {
                    const group = getInterval(s, idx);
                    if (group) {
                        (<PolygonInterval>group).pattern = p;
                    }
                    saveMap(lid, s);
                }
            }
        });
    },

    setPatternAngleForGroup(idx: number, a: PatternAngle) {
        updateStyle((s, lid) => {
            if (isPolygonStyle(s)) {
                if (isDiscrete(s)) {
                    const group = getGroup(s, idx);
                    if (group) {
                        (<PolygonDiscreteGroup>group).patternAngle = a;
                    }
                    saveMap(lid, s);
                } else if (isContinuous(s)) {
                    const group = getInterval(s, idx);
                    if (group) {
                        (<PolygonInterval>group).patternAngle = a;
                    }
                    saveMap(lid, s);
                }
            }
        });
    },

    // Point

    setPointConfig(a: 'label' | 'marker') {
        dispatch('component/legend-editor', state => {
            state.pointConfig = a;
            return state;
        });
    },

    setMarkerColor(c: string) {
        updatePointStyleMarker(s => {
            if (isMarkered(s)) {
                s.marker.color = c;
            }
        });
    },

    setMarkerSize(sz: number) {
        updatePointStyleMarker((s: PointStyleConfig) => {
            if (isMarkered(s)) {
                s.marker.size = sz;
            }
        });
    },

    setMarkerCodepoint(c: number) {
        updatePointStyleMarker((s: PointStyleConfig) => {
            if (isMarkered(s)) {
                s.marker.codePoint = c;
            }
        });
    },

    setFontColor(c: string) {
        updateStyleLabel((s: StyleConfig) => {
            if (isLabeled(s)) {
                s.label.color = c;
            }
        });
    },

    setFontSize(fs: number) {
        updateStyleLabel((s: StyleConfig) => {
            if (isLabeled(s)) {
                s.label.size = fs;
            }
        });
    },

    setPropNameForLabel(pn: string) {
        updateStyleLabel((s: StyleConfig) => {
            // logger(`setPropNameForLabel ${pn} ${isLabeled(s)}`);
            if (isLabeled(s)) {
                s.label.propName[getLang()] = pn;
            }
        });
    },

    setPositionForLabel(pos: 'above' | 'under' | 'left' | 'right') {
        updateStyleLabel((s: StyleConfig) => {
            if (isLabeled(s)) {
                switch (pos) {
                    case 'above':
                        s.label.align = 'center';
                        s.label.baseline = 'bottom';
                        s.label.yOffset = s.label.size * -1;
                        break;
                    case 'under':
                        s.label.align = 'center';
                        s.label.baseline = 'top';
                        s.label.yOffset = s.label.size;
                        break;
                    case 'left':
                        s.label.align = 'end';
                        s.label.baseline = 'middle';
                        break;
                    case 'right':
                        s.label.align = 'start';
                        s.label.baseline = 'middle';
                        break;
                }
            }
        });
    },

    setOffsetXForLabel(x: number) {
        updateStyleLabel((s: StyleConfig) => {
            if (isLabeled(s)) {
                s.label.xOffset = x;
            }
        });
    },

    setOffsetYForLabel(y: number) {
        updateStyleLabel((s: StyleConfig) => {
            if (isLabeled(s)) {
                s.label.yOffset = y;
            }
        });
    },

    // setResolutionForLabel(r: number) {
    //     updateStyleLabel((s: StyleConfig) => {
    //         if (isLabeled(s)) {
    //             s.label.resLimit = r;
    //         }
    //     });
    // },
    setZoomForLabel(zoom: number) {
        updateStyleLabel((s: StyleConfig) => {
            if (isLabeled(s)) {
                s.label.resLimit = zoom;
            }
        });
    },

    // Marker
    setMarkerColorForGroup(idx: number, c: string) {
        updateGroupStyle(idx, (g: PointDiscreteGroup) => (g.marker.color = c));
    },

    setMarkerSizeForGroup(idx: number, sz: number) {
        updateGroupStyle(idx, (g: PointDiscreteGroup) => {
            g.marker.size = sz;
        });
    },

    setMarkerCodepointForGroup(idx: number, c: number) {
        updateGroupStyle(idx, (g: PointDiscreteGroup) => {
            g.marker.codePoint = c;
        });
    },

    addItem() {
        const lid = getCurrentLayerId();
        if (lid) {
            dispatch('data/maps', maps => {
                const layer = getLayer(getCurrentMap(maps), lid);
                if (layer) {
                    const gt = queries.getGeometryType();
                    if (gt) {
                        const style = { ...layer.style };
                        if (isDiscrete(style)) {
                            addDefaultGroupStyle(style);
                            saveMap(lid, style);
                        } else if (isContinuous(style)) {
                            addDefaultIntervalStyle(
                                style,
                                0,
                                10,
                                queries.getFirstGroupColor(),
                                queries.getLastGroupColor()
                            );
                            saveMap(lid, style);
                        }
                    }
                }
                return maps;
            });
        }
    },

    removeItem(k: number) {
        const lid = getCurrentLayerId();
        if (lid) {
            dispatch('data/maps', (maps: IMapInfo[]) => {
                const layer = getLayer(getCurrentMap(maps), lid);
                if (layer) {
                    const gt = queries.getGeometryType();
                    if (gt) {
                        if (isDiscrete(layer.style)) {
                            layer.style.groups.splice(k, 1);
                            saveMap(lid, layer.style);
                        } else if (isContinuous(layer.style)) {
                            layer.style.intervals.splice(k, 1);
                            saveMap(lid, layer.style);
                        }
                    }
                }
                this.clearStyleGroup();
                return maps;
            });
        }
    },

    selectStyleGroup(k: number) {
        dispatch('component/legend-editor', state => {
            state.styleGroupSelected = k;
            return state;
        });
    },
    clearStyleGroup() {
        dispatch('component/legend-editor', state => {
            state.styleGroupSelected = -1;
            return state;
        });
    },

    setStyleGroupEditedValue(v: number | string) {
        dispatch('component/legend-editor', state => {
            state.styleGroupEditedValue = v;
            return state;
        });
    },
    setStyleGroupEditedText(v: string) {
        dispatch('component/legend-editor', state => {
            state.styleGroupEditedValue = v;
            return state;
        });
    },

    resetStyleGroupEditedValue() {
        dispatch('component/legend-editor', state => {
            state.styleGroupEditedValue = null;
            return state;
        });
    },

    moveGroupUp(index: number) {
        const lid = getCurrentLayerId();
        if (lid) {
            dispatch('data/maps', (maps: IMapInfo[]) => {
                const layer = fromNullable(getLayer(getCurrentMap(maps), lid));
                layer.map(l => {
                    if (isDiscrete(l.style)) {
                        if (index >= 0) {
                            const next = index - 1;
                            const row = l.style.groups[index];
                            l.style.groups[index] = l.style.groups[next];
                            l.style.groups[next] = row;
                        }
                        saveMap(lid, l.style);
                    }
                    if (isContinuous(l.style)) {
                        if (index >= 0) {
                            const next = index - 1;
                            const row = l.style.intervals[index];
                            l.style.intervals[index] = l.style.intervals[next];
                            l.style.intervals[next] = row;
                        }
                        saveMap(lid, l.style);
                    }
                });
                return maps;
            });
        }
    },
    moveGroupDown(index: number) {
        const lid = getCurrentLayerId();
        if (lid) {
            dispatch('data/maps', (maps: IMapInfo[]) => {
                const layer = fromNullable(getLayer(getCurrentMap(maps), lid));
                layer.map(l => {
                    if (isDiscrete(l.style)) {
                        if (index >= 0 && index + 1 < l.style.groups.length) {
                            const next = index + 1;
                            const row = l.style.groups[index];
                            l.style.groups[index] = l.style.groups[next];
                            l.style.groups[next] = row;
                        }
                        saveMap(lid, l.style);
                    }
                    if (isContinuous(l.style)) {
                        if (
                            index >= 0 &&
                            index + 1 < l.style.intervals.length
                        ) {
                            const next = index + 1;
                            const row = l.style.intervals[index];
                            l.style.intervals[index] = l.style.intervals[next];
                            l.style.intervals[next] = row;
                        }
                        saveMap(lid, l.style);
                    }
                });
                return maps;
            });
        }
    },

    // TODO: finish this function or delete it. nw
    // increaseOrder(k: number) {
    //     const lid = getCurrentLayerId();
    //     if (lid) {
    //         dispatch('data/maps', (maps: IMapInfo[]) => {
    //             const layer = getLayer(getCurrentMap(maps), lid);
    //             const gt = queries.getGeometryType();
    //             if (layer && gt) {
    //                 const gidx = queries.getSelectedStyleGroup();
    //                 const group = getDiscreteStyleGroup(layer, gidx);

    //                 if (group) {
    //                     group.order;
    //                 }
    //                 saveMap(lid, layer.style);
    //             }
    //             return maps;
    //         });
    //     }
    // },

    selectSimple(style?: StyleConfig) {
        const lid = getCurrentLayerId();

        if (lid) {
            const gt = queries.getGeometryType();

            if (gt) {
                if (!style) {
                    style = defaultStyle(gt, 'simple');
                }

                saveMap(lid, style);
            }
        }
    },
};

export default events;

logger('loaded');
