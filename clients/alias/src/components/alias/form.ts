import { Component, createElement } from 'react';

import { DIV, INPUT, LABEL } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { getFormSelect, getFormReplace } from '../../queries/alias';
import {
    setFormSelect,
    setFormReplace,
    formObserve,
    saveForm,
    deleteAlias,
} from '../../events/alias';
import { makeRemove, makeLabelAndIcon } from '../button';
import { AliasMessageKey } from 'alias/src/locale';

type TextGetter = () => string;
type TextSetter = (a: string) => void;

interface GS {
    g: TextGetter;
    s: TextSetter;
    label: AliasMessageKey;
    className: string;
}

interface VS {
    value: string;
}

class Input extends Component<any, VS> {
    constructor(gs: GS, vs: VS) {
        super(gs, vs);
        this.state = vs;
    }

    shouldComponentUpdate() {
        // const { value } = this.state;
        // return value === this.props.g();
        return true;
    }

    componentWillMount() {
        formObserve(() => {
            this.setState({ value: this.props.g() });
        });
        this.setState({ value: this.props.g() });
    }

    render() {
        return DIV(
            { className: this.props.className },
            LABEL(
                { className: 'input-label' },
                tr.alias(this.props.label),
                INPUT({
                    value: this.state.value,
                    type: 'text',
                    placeholder: tr.alias(this.props.placeholder),
                    onChange: e =>
                        this.setState({ value: e.currentTarget.value }),
                    onBlur: () => this.props.s(this.state.value),
                })
            )
        );
    }
}

const renderInputText =
    (className: string, label: AliasMessageKey, placeholder: AliasMessageKey) =>
    (get: TextGetter, set: TextSetter) => {
        return createElement(
            Input,
            { s: set, g: get, className, label, placeholder },
            { value: '' }
        );
    };

const renderSelect = renderInputText('form-select', 'term', 'insertTerm');

const renderReplaceFr = renderInputText(
    'form-replace',
    'replaceFR',
    'insertAliasFr'
);

const renderReplaceNl = renderInputText(
    'form-replace',
    'replaceNL',
    'insertAliasNl'
);

const saveBtn = makeLabelAndIcon('save', 2, 'save', () => tr.alias('save'));

const removeBtn = makeRemove(
    'remove',
    3,
    () => tr.alias('remove'),
    () => tr.alias('rmvMsgKeyword')
);

const renderActions = () =>
    DIV(
        { className: 'form-actions' },
        saveBtn(saveForm),
        removeBtn(deleteAlias)
    );

const render = () =>
    DIV(
        {
            className: 'form',
        },
        renderSelect(getFormSelect, setFormSelect),
        renderReplaceNl(getFormReplace('nl'), setFormReplace('nl')),
        renderReplaceFr(getFormReplace('fr'), setFormReplace('fr')),
        renderActions()
    );

export default render;
