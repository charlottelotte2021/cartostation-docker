import { getCurrentQueryData } from 'angled-query/src/queries/preview';
import { withQueryString } from 'sdi/source';
import { getApiUrl, getCSRF, getLang } from 'sdi/app';
import { ExportForm } from 'sdi/components/export';
import { SPAN } from 'sdi/components/elements';
import tr from 'sdi/locale';

/**
 * This whole module is a bit of an anomaly
 */

const save = (filename: string) => (blob: Blob) => {
    const object = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = object;
    a.download = filename;
    a.click();

    // For Firefox it is necessary to delay revoking the ObjectURL.
    setTimeout(() => {
        window.URL.revokeObjectURL(object);
    }, 250);
};

const download = (filename: string, url: string, data: unknown) => {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    getCSRF().map(csrf => headers.append('X-CSRFToken', csrf));

    const options: RequestInit = {
        body: JSON.stringify(data),
        method: 'POST',
        mode: 'cors',
        cache: 'default',
        redirect: 'follow',
        credentials: 'same-origin',
        headers,
    };

    return fetch(url, options)
        .then(response => {
            if (response.ok) {
                return response.blob();
            }
            throw response;
        })
        .then(save(filename));
};

const makeUrl = (form: ExportForm) =>
    withQueryString(getApiUrl(`geodata/angled/l/point`), {
        form,
        lang: getLang(),
    });

const exportQuery = (form: ExportForm) =>
    getCurrentQueryData().map(data =>
        download(`query.${form}`, makeUrl(form), data)
    );

const label = (form: ExportForm) => {
    switch (form) {
        case 'csv':
            return tr.core('exportCSV');
        case 'xlsx':
            return tr.core('exportXLSX');
    }
};

export const pseudoLink = (form: ExportForm) =>
    SPAN(
        {
            className: 'table-download-link',
            onClick: () => exportQuery(form),
        },
        SPAN(
            {
                className: 'dl-item',
                'aria-label': label(form),
            },
            form
        )
    );
