import { IMapOptions, create } from 'sdi/map';
import { getSelectBaseLayer } from 'angled-query/src/queries/builder';
import { getView, getMapInfo } from 'angled-core/queries/map';
import { viewEvents, scalelineEvents } from 'angled-core/events/map';
import { none, some, Option } from 'fp-ts/lib/Option';
import { DIV } from 'sdi/components/elements';
import { updateLoading } from 'angled-query/src/events/preview';
import { previewMapName, getSelected } from 'angled-query/src/queries/preview';

const options: IMapOptions = {
    element: null,
    getBaseLayer: getSelectBaseLayer,
    getView,
    getMapInfo,

    updateView: viewEvents.updateMapView,
    setScaleLine: scalelineEvents.setScaleLine,
    setLoading: updateLoading,
};

let mapSetTarget: Option<(t: HTMLElement | null) => void> = none;
let mapUpdate: Option<() => void> = none;

// const selectOptions: SelectOptions = {
//     selectFeature,
//     clearSelection: clearSelectedFeature,
//     getSelected: () => getSelectedFeature().fold({ layerId: null, featureId: null }, identity),
// };

const attachMap = (element: HTMLElement | null) => {
    mapUpdate = mapUpdate.foldL(
        () => {
            const { update, setTarget, highlightable } = create(
                previewMapName,
                {
                    ...options,
                    element,
                }
            );
            // selectable(selectOptions, getInteraction);
            highlightable(getSelected);
            mapSetTarget = some(setTarget);
            return some(update);
        },
        update => some(update)
    );

    if (element) {
        mapSetTarget.map(f => f(element));
    }
};

export const render = () => {
    mapUpdate.map(f => f());
    return DIV(
        { className: '_map-view' },
        DIV({ className: 'map-wrapper', key: previewMapName, ref: attachMap })
    );
};

export default render;
