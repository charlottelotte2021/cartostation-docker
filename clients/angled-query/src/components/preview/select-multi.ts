import { DIV, H2 } from 'sdi/components/elements';
import { ReactNode } from 'react';
import { getSelectBuilderStep } from 'angled-query/src/queries/builder';
import { renderFieldSelect } from '../builder/field';
import {
    renderAggSelect,
    renderAggPreview,
    renderAggFilter,
} from '../builder/agg';
import tr from 'sdi/locale';

export type SelectBuilderStep =
    | 'Initial'
    | 'Fielded'
    | 'Aggregated'
    | 'Filtered';

const wrapBuilder = (...nodes: ReactNode[]) =>
    DIV(
        { className: 'modal__query-builder' },
        // H2({ className: 'head-title' }, tr.angled('queryBuilder')),
        DIV({ className: 'query-builder__wrapper' }, ...nodes)
    );

const renderFinal = () =>
    H2('query-preview-final', tr.angled('aggregatorFinalMsg'));

const renderPreview = () =>
    DIV(
        { className: 'query-preview' },
        // renderFieldPreview(),
        H2('', tr.angled('aggregatorPreview')),
        renderAggPreview()
    );

const renderInitial = () =>
    wrapBuilder(
        // renderPreview(),
        renderFieldSelect()
    );

const renderFielded = () =>
    wrapBuilder(
        // renderPreview(),
        renderAggSelect()
    );

const renderAgged = () => wrapBuilder(renderAggFilter(), renderPreview());

const renderFiltered = () => wrapBuilder(renderFinal(), renderPreview());

const render = () => {
    const step = getSelectBuilderStep();
    switch (step) {
        case 'Initial':
            return renderInitial();
        case 'Fielded':
            return renderFielded();
        case 'Aggregated':
            return renderAgged();
        case 'Filtered':
            return renderFiltered();
    }
};

export default render;
