import { fromNullable, Option } from 'fp-ts/lib/Option';
import { DIV, H2, SPAN } from 'sdi/components/elements';
import {
    getSelectableUnits,
    isSelectedField,
    getSelectedFieldsMapped,
    defaultFieldNameParts,
} from 'angled-query/src/queries/preview';
import { deselectIndex } from 'angled-query/src/events/preview';
import { makeIcon } from 'sdi/components/button';
import tr from 'sdi/locale';
import {
    InformationUnitName,
    isMulti,
    mapFieldName,
    mapFieldType,
    MappedField,
} from 'angled-core/ui';
import {
    setSelectUnit,
    addSelectQueryForSingle,
    execQuery,
    setSelectField,
    withRecordField,
    updateSelectQueryLang,
    initSelectNameForm,
} from '../../events/builder';
import { openModalColumnName, openSelect } from 'angled-query/src/events/modal';
import { QuerySelect } from 'angled-query/src/remote/query';
import { iife } from 'sdi/lib';
import { divTooltipBottomLeft, Tooltip } from 'sdi/components/tooltip';
import { getAliasOption } from 'sdi/app';

const aggregateTooltip: Tooltip = {
    text: () => tr.angled('tooltipFieldAgregate'),
    position: 'bottom-left',
};
const editNameTooltip: Tooltip = {
    text: () => tr.angled('tooltipFieldRename'),
    position: 'bottom-left',
};
const aggregateButton = makeIcon('settings', 3, 'cog', aggregateTooltip);
const editNameButton = makeIcon('settings', 3, 'pencil-alt', editNameTooltip);

const switchLangButton = (index: number, select: QuerySelect) =>
    fromNullable(select.field[1]).map(lang =>
        divTooltipBottomLeft(
            tr.angled('tooltipFieldLangSwitch'),
            {
                className: 'select-record-lang-picker',
                onClick: () => switchLangSelect(index, select),
            },
            DIV(`select-record-lang ${lang === 'fr' ? 'active' : ''}`, 'FR'),
            DIV(`select-record-separator`, ' | '),
            DIV(`select-record-lang ${lang === 'nl' ? 'active' : ''}`, 'NL')
        )
    );

const startRenaming = (currentName: string) => {
    initSelectNameForm(currentName);
    openModalColumnName();
};

const switchLangSelect = (index: number, select: QuerySelect) =>
    fromNullable(select.field[1])
        .chain(lang =>
            updateSelectQueryLang(index, lang === 'fr' ? 'nl' : 'fr')
        )
        .map(execQuery);

const listItemClassName = (unitName: InformationUnitName, field: string) =>
    isSelectedField(unitName, field)
        ? 'list__item selected-field'
        : 'list__item';

const renderUnselectedFieldsMulti = (
    unit: InformationUnitName,
    fields: MappedField[]
) =>
    fields.map(field =>
        DIV(
            {
                className: listItemClassName(unit, mapFieldName(field)),
                onClick: () => {
                    openSelect();
                    setSelectUnit(unit, null);
                    setSelectField({
                        unit: unit,
                        type: mapFieldType(field),
                        name: [mapFieldName(field)],
                    });
                },
            },
            iife(() => {
                const { fieldName, unitName } = defaultFieldNameParts(
                    unit,
                    mapFieldName(field)
                );
                return fieldName.fold(
                    DIV({ className: 'unit-name only-unit' }, unitName),
                    fieldName =>
                        DIV(
                            { className: 'with-field' },
                            SPAN({ className: 'unit-name' }, unitName),
                            SPAN({ className: 'separator' }, ' → '),
                            SPAN({ className: 'field-name' }, fieldName)
                        )
                );
            })
        )
    );

const renderUnselectedFieldsSingle = (
    unit: InformationUnitName,
    fields: MappedField[]
) =>
    fields.map(field =>
        DIV(
            {
                className: listItemClassName(unit, mapFieldName(field)),
                onClick: () => {
                    addSelectQueryForSingle(unit, mapFieldName(field));
                    execQuery();
                },
            },
            iife(() => {
                const { fieldName, unitName } = defaultFieldNameParts(
                    unit,
                    mapFieldName(field)
                );
                return fieldName.fold(
                    DIV({ className: 'unit-name only-unit' }, unitName),
                    fieldName =>
                        DIV(
                            { className: 'with-field' },
                            SPAN({ className: 'unit-name' }, unitName),
                            SPAN({ className: 'separator' }, ' → '),
                            SPAN({ className: 'field-name' }, fieldName)
                        )
                );
            })
        )
    );

const renderUnselectedFields = () =>
    getSelectableUnits().map(opt =>
        opt.map(({ name, multi, fields }) =>
            multi
                ? renderUnselectedFieldsMulti(name, fields)
                : renderUnselectedFieldsSingle(name, fields)
        )
    );

const renderName = (name: string) =>
    getAliasOption(name).fold(SPAN('noalias', name), alias =>
        SPAN('alias', alias)
    );

const renderSelectedFieldsMulti = (
    index: number,
    select: QuerySelect,
    optField: Option<MappedField>
) =>
    DIV(
        { className: 'select__item select--selected--multi' },
        DIV({ className: 'label' }, renderName(select.name)),
        DIV(
            { className: 'action' },
            optField
                .chain(withRecordField)
                .chain(_ => switchLangButton(index, select)),

            aggregateButton(() =>
                optField.map(field => {
                    openSelect();
                    setSelectUnit(select.unit, index);
                    setSelectField({
                        unit: select.unit,
                        type: mapFieldType(field),
                        name: [mapFieldName(field)],
                    });
                })
            ),

            editNameButton(() => startRenaming(select.name)),

            makeIcon('clear', 3, 'times', {
                text: tr.angled('tooltipFieldRemove'),
                position: 'bottom-left',
            })(() => deselectIndex(index))
        )
    );

const renderSelectedFieldsSingle = (
    index: number,
    select: QuerySelect,
    optField: Option<MappedField>
) =>
    DIV(
        { className: 'select__item select--selected--single' },
        DIV({ className: 'label' }, renderName(select.name)),
        DIV(
            { className: 'action' },
            optField
                .chain(withRecordField)
                .chain(_ => switchLangButton(index, select)),
            editNameButton(() => startRenaming(select.name)),

            makeIcon('clear', 3, 'times', {
                text: tr.angled('tooltipFieldRemove'),
                position: 'bottom-left',
            })(() => deselectIndex(index))
            // divTooltipBottomLeft(
            //     tr.angled('tooltipFieldRemove'),
            //     {},
            //     makeIcon('clear', 3, 'times')(() => deselectIndex(index))
            // )
        )
    );

const renderSelectedFields = () =>
    getSelectedFieldsMapped().map(([select, optField], index) =>
        isMulti(select.unit)
            ? renderSelectedFieldsMulti(index, select, optField)
            : renderSelectedFieldsSingle(index, select, optField)
    );

const render = () =>
    DIV(
        { className: 'select' },
        DIV(
            { className: 'select--selected' },
            H2({ className: 'head-title' }, tr.angled('fieldSelected')),
            ...renderSelectedFields()
        ),
        DIV(
            { className: 'select--unselected' },
            H2({ className: 'head-title' }, tr.angled('fieldUnselected')),
            ...renderUnselectedFields()
        )
    );

export default render;
