import { BUTTON, DIV, H2, NODISPLAY, SPAN } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { renderAudienceDisplayWidget } from 'angled-core/components/audience-display';
import {
    resultTableQueries,
    mapIsVisible,
    getLoadingLayers,
    audienceHighlighter,
} from 'angled-query/src/queries/preview';
import {
    resultTableEvents,
    hideMap,
    showMap,
    selectProject,
    zoomToFeature,
    loadProject,
} from 'angled-query/src/events/preview';
import { baseTable, TableDataRow } from 'sdi/components/table';
import { foldRemote, RemoteResource } from 'sdi/source';
import {
    getCurrentQueryId,
    getResults,
    getWhereStatementList,
} from 'angled-query/src/queries/builder';
import select from './select';
import map from './map';
import { nameToString } from 'sdi/components/button/names';
import { renderWhereStatement } from '../statement/index';
import rowView from './row-view';
import { divTooltipBottom } from 'sdi/components/tooltip';
import { exportLinks } from 'sdi/components/export';
import { getExportLink } from 'angled-query/src/queries/app';
import { pseudoLink } from './export-raw';

const onRowSelect = (row: TableDataRow) => {
    const pid = row.from as number;
    selectProject(pid);
    zoomToFeature(pid);
    loadProject(pid);
};

const base = baseTable(resultTableQueries, resultTableEvents);

const querySummary = () =>
    DIV('query-summary', ...getWhereStatementList().map(renderWhereStatement));

const sidebar = () =>
    DIV(
        '_sidebar',
        H2('head-title', tr.angled('queryPreviewBlockTitle')),
        querySummary(),
        select()
    );

const loaderAnim = () => DIV('loader-anim__small');

const loadingData = (r: RemoteResource<unknown[]>) =>
    foldRemote(
        () => DIV('loader loader__small', loaderAnim(), 'waiting action'),
        () => DIV('loader loader__small', loaderAnim(), 'loading'),
        (err: string) => DIV('error', err),
        () => NODISPLAY()
    )(r);

const loadingLayers = () =>
    getLoadingLayers().map(r =>
        DIV(
            'loader loader__small',
            loaderAnim(),
            SPAN({}, `loading ${fromRecord(r)}`)
        )
    );

const toggle = () => {
    if (mapIsVisible()) {
        return DIV(
            'toggle',
            SPAN('toggle-label', tr.angled('displayMap')),
            SPAN(
                { onClick: hideMap },
                SPAN('toggle-value', 'Off'),
                SPAN('toggle-icon active-left', nameToString('toggle-on'))
            ),
            SPAN('toggle-value active', 'On')
        );
    }
    return DIV(
        'toggle',
        SPAN('toggle-label', tr.angled('displayMap')),
        SPAN('toggle-value active', 'Off'),
        SPAN(
            { onClick: showMap },
            SPAN('toggle-icon active-right', nameToString('toggle-off')),
            SPAN('toggle-value ', 'On')
        )
    );
};

const tooltippedToggle = () =>
    divTooltipBottom(tr.angled('viewLayerInfos'), {}, toggle());

const renderExportLinks = (qid: number) => exportLinks(getExportLink(qid));

const renderPseudoLinks = () =>
    DIV(
        'export__wrapper',
        DIV(
            'table-download',
            BUTTON(
                'btn btn-3 label-and-icon',
                SPAN('btn-label', pseudoLink('csv')),
                SPAN('icon', nameToString('download'))
            ),
            BUTTON(
                'btn btn-3 label-and-icon',
                SPAN('btn-label', pseudoLink('xlsx')),
                SPAN('icon', nameToString('download'))
            )
        )
    );

const renderTable = base({
    className: 'attr-select-wrapper',
    onRowSelect,
    highlighter: audienceHighlighter,
});

const resultTable = () => {
    return DIV('query-result query-result--table', renderTable());
};

const resultMap = () => DIV('query-result query-result--map', map());

const resultsWrapper = () =>
    DIV(
        'results__wrapper',
        DIV(
            'results__header',
            // toggle(),
            tooltippedToggle(),
            loadingData(getResults()),
            ...loadingLayers(),
            getCurrentQueryId().foldL(renderPseudoLinks, qid =>
                DIV('export__wrapper', renderExportLinks(qid))
            )
        ),
        renderAudienceDisplayWidget(),
        DIV(
            'results',
            DIV('results__table-and-row', resultTable(), rowView()),
            mapIsVisible() ? resultMap() : NODISPLAY()
        )
    );

export const render = () => DIV('_project__list', sidebar(), resultsWrapper());

export default render;
