import { DIV } from 'sdi/components/elements';
import {
    InformationUnitName,
    lookupNamedField,
    MappedField,
    mapFieldName,
    mapFieldType,
} from 'angled-core/ui';
import { getSelectField } from 'angled-query/src/queries/builder';
import { FieldDescription } from '../statement/index';
import { tr } from 'sdi/locale';
import { getSelectUnit } from '../../queries/builder';
import { setSelectField } from '../../events/builder';
import { fieldDisplayName } from 'angled-core/queries/app';
import { scopeOption } from 'sdi/lib';

const renderField = (unit: InformationUnitName, fs: MappedField[]) =>
    DIV(
        { className: 'ui-list__field-list' },
        fs.map(f => {
            const name = mapFieldName(f);
            const typ = mapFieldType(f);
            return DIV(
                {
                    className: 'ui-list__field interactive',
                    key: `${typ}.${name}`,
                    onClick: () =>
                        setSelectField({ unit, type: typ, name: [name] }),
                },
                fieldDisplayName(unit, name)
            );
        })
    );

const renderItem = (u: InformationUnitName) =>
    DIV(
        { className: 'field-list__item', key: u },
        lookupNamedField(u).map(field => DIV({}, renderField(u, field.fields)))
    );

export const renderFieldSelect = () =>
    DIV(
        { className: 'query-tool' },
        DIV(
            { className: 'field-list' },
            getSelectUnit().map(u => renderItem(u.unit))
        )
    );

const renderPreviewNone = () =>
    DIV({ className: 'query-preview--none' }, tr.angled('selectField'));

const renderPreviewSome = (
    unit: InformationUnitName,
    field: Readonly<FieldDescription>
) =>
    DIV(
        { className: 'query-preview--field' },
        fieldDisplayName(unit, `${field.name}`)
    );

export const renderFieldPreview = () =>
    scopeOption()
        .let('unit', getSelectUnit())
        .let('field', getSelectField())
        .fold(renderPreviewNone(), ({ unit, field }) =>
            renderPreviewSome(unit.unit, field)
        );
