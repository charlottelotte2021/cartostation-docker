import { DIV, SPAN } from 'sdi/components/elements';
import {
    InformationUnitName,
    lookupNamedField,
    MappedField,
    FieldType,
    mapFieldType,
    mapFieldName,
} from 'angled-core/ui';
import {
    setInfoFilterPattern,
    setWhereField,
} from 'angled-query/src/events/builder';
import {
    getInfoFilterPattern,
    getUIFilteredList,
    getWhereField,
} from 'angled-query/src/queries/builder';
import { FieldDescription } from '../statement/index';
import { tr } from 'sdi/locale';
import { getLang } from 'sdi/app';
import { Field } from 'angled-query/src/remote/query';
import { defaultFieldNameParts } from 'angled-query/src/queries/preview';
import { iife } from 'sdi/lib';
import { inputText } from 'sdi/components/input';
import { nameToString } from 'sdi/components/button/names';

const isMessageRecord = (t: FieldType) => t === 'label' || t === 'text';

const fielClickHandler = (u: InformationUnitName, f: MappedField) => () => {
    const fieldType = mapFieldType(f);
    const fieldName = mapFieldName(f);
    const name: Field = isMessageRecord(fieldType)
        ? [fieldName, getLang()]
        : [fieldName];
    setWhereField(u, { unit: u, type: fieldType, name });
};

const renderFields = (u: InformationUnitName, fs: MappedField[]) =>
    DIV(
        { className: 'ui-list__field-list' },
        fs.map(f =>
            DIV(
                {
                    key: `ui-list__field-${mapFieldName(f)}`,
                    className: 'ui-list__field',
                    onClick: fielClickHandler(u, f),
                },
                // SPAN({ className: 'ui-list__unit-label' }, unitDisplayName(u)),
                // SPAN({ className: 'ui-list__unit-field' }, fieldDisplayName(u, mapFieldName(f))),
                iife(() => {
                    const { fieldName, unitName } = defaultFieldNameParts(
                        u,
                        mapFieldName(f)
                    );
                    return fieldName.fold(
                        DIV(
                            { className: 'ui-list__unit-name only-unit' },
                            unitName
                        ),
                        fieldName =>
                            DIV(
                                { className: 'ui-list__unit-field' },
                                SPAN({ className: 'unit-name' }, unitName),
                                SPAN({ className: 'separator' }, ' → '),
                                SPAN({ className: 'field-name' }, fieldName)
                            )
                    );
                })
            )
        )
    );

// const renderUiName =
//     (u: InformationUnitName) => DIV({ className: 'ui-list__label' }, displayName(u));

const renderItem = (u: InformationUnitName) =>
    DIV(
        { className: 'ui-list__item', key: u },
        lookupNamedField(u).map(
            // field => DIV({}, renderUiName(field.name), renderFields(u, field.fields)),
            field => DIV({}, renderFields(u, field.fields))
        )
    );

export const renderUnitSelect = () =>
    DIV(
        { className: 'query-tool' },
        DIV({ className: 'ui-list' }, getUIFilteredList().map(renderItem))
    );

const renderPreviewNone = () =>
    DIV({ className: 'query-preview--none' }, tr.angled('selectField'));

const renderPreviewSelected = () =>
    DIV(
        { className: 'query-preview--none selected' },
        tr.angled('selectField')
    );

const renderPreviewSome = (
    unit: InformationUnitName,
    field: Readonly<FieldDescription>
) => {
    const { fieldName, unitName } = defaultFieldNameParts(unit, field.name[0]);
    return fieldName.fold(
        DIV({ className: 'ui-list__unit-name only-unit' }, unitName),
        fieldName =>
            DIV(
                { className: 'ui-list__unit-field' },
                SPAN({ className: 'query-preview--unit-label' }, unitName),
                SPAN({ className: 'query-preview--unit-separator' }, ' → '),
                SPAN({ className: 'query-preview--field' }, fieldName)
            )
    );
};
// SPAN({},
//     SPAN({ className: 'query-preview--unit-label' }, unitDisplayName(unit)),
//     SPAN({ className: 'query-preview--field' }, fieldDisplayName(unit, `${field.name}`)),
// );

export const renderUnitStep = () =>
    getWhereField().fold(renderPreviewNone(), renderPreviewSelected);

const renderInfoFilter = () =>
    DIV(
        'input__wrapper',
        SPAN('icon', nameToString('search')),
        inputText({
            key: `filter-info-input`,
            get: getInfoFilterPattern, // TODO
            set: setInfoFilterPattern, // TODO
            attrs: {
                placeholder: tr.angled('selectInfoToFilter'),
            },
            monitor: setInfoFilterPattern,
        })
    );

export const renderUnitPreview = () =>
    getWhereField().fold(
        // SPAN({}, tr.angled('selectInfoToFilter')),
        renderInfoFilter(),
        ({ unit, field }) => renderPreviewSome(unit, field)
    );
