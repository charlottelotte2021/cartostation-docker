import { rec, EmptyString } from 'sdi/locale';
import { getLang } from 'sdi/app';
import {
    InformationUnitName,
    makeValueMapper,
    FieldType,
} from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { inputText, options } from 'sdi/components/input';
import { getFormInput } from 'angled-core/queries/ui';
import { setFormInput } from 'angled-core/events/ui';
import { MessageRecord, MessageRecordLang } from 'sdi/source';
// import { none, Option, some } from 'fp-ts/lib/Option';

type Self = MessageRecord;

const fieldMapper = makeValueMapper('label', (a: Self) => a, {
    fr: '',
    nl: '',
});

// const { getCurrentLang } = (function () {
//     let lc: Option<MessageRecordLang> = none;

//     const setCurrentLang = (l: MessageRecordLang) => lc = some(l);
//     const getCurrentLang = () => lc.getOrElseL(getLang);

//     return { setCurrentLang, getCurrentLang };
// })()

// const renderName =
//     (unitName: InformationUnitName, fieldName: string) =>
//         DIV({ className: 'field__key field--write' }, fieldDisplayName(unitName, fieldName));

const getRecord = (unit: InformationUnitName, fieldName: string) =>
    getFormInput(unit, fieldName).map(fieldMapper);

const getValue = (
    unit: InformationUnitName,
    fieldName: string,
    l: MessageRecordLang
) => {
    const record = getRecord(unit, fieldName);
    return record.fold(EmptyString, record => rec(record, l, ''));
};

const renderInput = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__value field--write' },
        inputText(
            options(
                `input-label-${unit}-${fieldName}`,
                () => getValue(unit, fieldName, getLang()),
                i => {
                    const inputRecord = { [getLang()]: i };
                    setFormInput(unit, fieldName)(inputRecord);
                }
            )
        )
    );

export const render = (
    unit: InformationUnitName,
    _ft: FieldType,
    fieldName: string
) =>
    DIV(
        { className: `field field--${fieldName} field--write` },
        renderInput(unit, fieldName)
    );

export default render;
