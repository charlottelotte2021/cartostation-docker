import * as debug from 'debug';
import { DIV, SPAN } from 'sdi/components/elements';
// import { inputNumber, inputText } from 'angled-core/sdi/components/input';
import {
    FieldValueType,
    FieldType,
    UnitField,
    InformationUnitName,
} from 'angled-core/ui';
import { ReactNode } from 'react';
import { getWhereField, getValue } from 'angled-query/src/queries/builder';
import { setValue } from 'angled-query/src/events/builder';
import tr, { rec } from 'sdi/locale';
import { renderField } from 'angled-core/ui/write';
import { getFormInput } from 'angled-core/queries/ui';
import renderSelectGeometry from './geometry';
import renderSetLabel from './label';
import renderSetFk from './fk';
import { makeLabelAndIcon } from 'sdi/components/button';
import { getLang } from 'sdi/app';
import renderValue from '../../statement/value';
import { none } from 'fp-ts/lib/Option';

const validateButton = makeLabelAndIcon('add', 1, 'check', () =>
    tr.core('validate')
);

const logger = debug('sdi:builder/value');

export const submit = (node: ReactNode, submitFunction: () => void) =>
    DIV(
        { className: 'query-tool' },
        DIV({ className: 'submitable' }, node, validateButton(submitFunction))
    );

const getWriteRenderer = (ft: FieldType) => {
    switch (ft) {
        case 'boolean':
            return renderField;
        case 'date':
            return renderField;
        case 'decimal':
            return renderField;
        case 'fk':
            return renderSetFk;
        case 'geometry':
            return renderSelectGeometry;
        case 'label':
            return renderSetLabel;
        case 'month':
            return renderField;
        case 'number':
            return renderField;
        case 'raw_text':
            return renderField;
        case 'term':
            return renderField;
        case 'text':
            return renderField;
        case 'varchar':
            return renderField;
        case 'year':
            return renderField;
    }
};

const processValue = (f: UnitField) => {
    switch (f.kind) {
        case 'label':
        case 'text':
            return rec(f.value, getLang());
        default:
            return f.value;
    }
};

const trickGeometry = (u: InformationUnitName) =>
    u === 'point' || u === 'line' ? 'polygon' : u;

export const renderValueSelect = () =>
    getWhereField().fold(DIV({}, 'Error finding field'), ({ unit, field }) =>
        submit(
            getWriteRenderer(field.type)(unit, field.type, field.name[0], none),
            () => {
                const iun = trickGeometry(unit);
                const fn = field.name[0];
                const input = getFormInput(iun, fn);
                input.map(f => setValue(processValue(f)));
            }
        )
    );

const renderPreviewNone = () =>
    DIV({ className: 'query-preview--none' }, tr.angled('selectValue'));

const renderPreviewSelected = () =>
    DIV(
        { className: 'query-preview--none selected' },
        tr.angled('selectValue')
    );

const renderPreviewSome = (_value: FieldValueType) =>
    SPAN(
        { className: 'query-preview--value' },
        getWhereField().fold(
            SPAN({}, 'Error finding field'),
            ({ unit, field }) =>
                getFormInput(unit, field.name[0]).fold(
                    SPAN({}, 'Error Getting Input'),
                    f => renderValue(f.value, unit, field.name[0])
                )
        )
    );

export const renderValueStep = () =>
    getValue().fold(renderPreviewNone(), renderPreviewSelected);

export const renderValuePreview = () => getValue().map(renderPreviewSome);

logger('loaded');
