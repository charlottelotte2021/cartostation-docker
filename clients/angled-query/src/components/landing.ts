import { DIV, H2 } from 'sdi/components/elements';
import { stringToParagraphs } from 'sdi/util';
import tr from 'sdi/locale';
import { markdown } from 'sdi/ports/marked';

import { navigateNewQuery } from '../events/route';
import list from './list';
import { makeLabelAndIcon } from 'sdi/components/button';

const addQueryButton = makeLabelAndIcon('add', 1, 'plus', () =>
    tr.angled('addNewQuery')
);

const renderBuilderCreate = () =>
    DIV(
        'statement__initial',
        DIV('app-infos', stringToParagraphs(tr.angled('queryAppInfos'))),
        DIV('statement__initial--action', addQueryButton(navigateNewQuery)),
        DIV(
            'wfs-infos__wrapper',
            H2('', tr.angled('wfsBlockInfoTitle')),
            DIV(
                'wfs-infos__content',
                markdown(tr.angled('wfsBlockInfoContent'))
            )
        )
    );

export const render = () =>
    DIV('query__list--wrapper', renderBuilderCreate(), list());

export default render;
