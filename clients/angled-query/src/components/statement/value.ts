import * as debug from 'debug';
import { SPAN, DIV, NODISPLAY } from 'sdi/components/elements';
import tr, {
    formatNumber,
    formatDate,
    fromRecord,
    rec,
    EmptyString,
} from 'sdi/locale';
import { getLang } from 'sdi/app';
import { weakIsMessageRecord, MessageRecord } from 'sdi/source/io/io';
import { MultiPolygon, MultiLineString, MultiPoint } from 'sdi/source';
import {
    FieldValueType,
    InformationUnitName,
    lookupNamedFieldType,
} from 'angled-core/ui';
import { renderValue as renderFkValue } from 'angled-core/ui/read/fk';
import { findTerm } from 'angled-core/queries/universe';

const logger = debug('sdi:statement/value');

const renderNumber = (v: number) =>
    SPAN({ className: 'value--number' }, formatNumber(v));

const renderString = (v: string) => SPAN({ className: 'value--string' }, v);

const renderBoolean = (v: boolean) =>
    SPAN(
        { className: 'value--bool' },
        v ? tr.angled('true') : tr.angled('false')
    );

const renderArray = (xs: number[]) =>
    SPAN(
        { className: 'value--number' },
        xs.map(x => formatNumber(x)).join(', ')
    );

const renderRecord = (v: MessageRecord) =>
    SPAN({ className: 'value--string' }, fromRecord(v));

const renderGeom = (v: MultiPoint | MultiLineString | MultiPolygon) =>
    DIV({ className: 'value--string' }, v.type); // TODO

const renderTerm = (v: number) =>
    SPAN(
        { className: 'value--string' },
        findTerm(v).fold(EmptyString, term => rec(term.name, getLang()))
    );

const renderDate = (v: number) =>
    SPAN({ className: 'value--string' }, formatDate(new Date(v)));

export const render = (
    val: FieldValueType,
    unit: InformationUnitName,
    fieldName: string
) => {
    const ft = lookupNamedFieldType(unit, fieldName).fold('number', t => t);
    if (typeof val === 'number') {
        if (ft === 'term') {
            return renderTerm(val);
        }
        if (ft === 'fk') {
            return renderFkValue(fieldName, val).getOrElse(
                DIV(
                    { className: 'field__value error' },
                    `~Could not find ${fieldName}(${val})`
                )
            );
        }
        if (ft === 'date') {
            return renderDate(val);
        }
        return renderNumber(val);
    } else if (typeof val === 'string') {
        return renderString(val);
    } else if (typeof val === 'boolean') {
        return renderBoolean(val);
    } else if (Array.isArray(val)) {
        return renderArray(val);
    } else if (typeof val === 'object') {
        if (weakIsMessageRecord(val)) {
            return renderRecord(val);
        }
        return renderGeom(val);
    }

    return NODISPLAY(); // weird, TS doesnt pick that we exhausted types
};

export default render;

logger('loaded');
