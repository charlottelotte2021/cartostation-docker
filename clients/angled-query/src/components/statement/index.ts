import { FieldType, InformationUnitName } from 'angled-core/ui';
import { DIV, H2, SPAN } from 'sdi/components/elements';
import {
    getWhereStatementList,
    getCurrentQuery,
} from 'angled-query/src/queries/builder';
import {
    Op,
    QueryConj,
    QueryStatement,
    QueryFilter,
    QueryWhere,
    Field,
} from 'angled-query/src/remote/query';
import { makeLabelAndIcon } from 'sdi/components/button';
import tr, { fromRecord } from 'sdi/locale';
import { fromPredicate } from 'fp-ts/lib/Option';
import { clearWhereBuilderAndStatements } from 'angled-query/src/events/builder';
import { stringToParagraphs } from 'sdi/util';
import { AngledMessageKey } from 'angled-core/locale';
import { navigateExec, navigateExecSaved } from 'angled-query/src/events/route';
import renderValue from './value';
import { defaultFieldNameParts } from 'angled-query/src/queries/preview';
import { iife } from 'sdi/lib';

const execButton = makeLabelAndIcon('start', 1, 'play', () =>
    tr.angled('runQuery')
);
const clearButton = makeLabelAndIcon('cancel', 2, 'times', () =>
    tr.angled('clearQuery')
);

export interface FieldDescription {
    unit: InformationUnitName;
    type: FieldType;
    name: Field;
}

const displayOpName = (n: AngledMessageKey) => () => tr.angled(n);

// tslint:disable-next-line: variable-name
export const Operations = {
    exact: displayOpName('op/exact'),
    // iexact: displayOpName('iexact'),
    contains: displayOpName('op/contains'),
    icontains: displayOpName('op/icontains'),
    // startswith: displayOpName('startswith'),
    istartswith: displayOpName('op/istartswith'),
    // endswith: displayOpName('endswith'),
    iendswith: displayOpName('op/iendswith'),
    // in: displayOpName('in'),

    gt: displayOpName('op/gt'),
    gte: displayOpName('op/gte'),
    lt: displayOpName('op/lt'),
    lte: displayOpName('op/lte'),

    // range: displayOpName('range'),
    // year: displayOpName('year'),

    intersects: displayOpName('op/intersects'),
    // distance_lte: displayOpName('is closer than'),
    // distance_gte: displayOpName('is farther than'),
};

export const applicableOps = (ft: FieldType): Op[] => {
    switch (ft) {
        case 'boolean':
            return ['exact'];
        case 'date':
            return ['exact', 'gt', 'gte', 'lt', 'lte'];
        case 'decimal':
            return ['exact', 'gt', 'gte', 'lt', 'lte'];
        case 'fk':
            return ['exact'];
        case 'geometry':
            return ['intersects'];
        // return ['contains', 'intersects'];
        case 'label':
            return ['exact', 'icontains', 'istartswith', 'iendswith'];
        case 'month':
            return ['exact', 'gt', 'gte', 'lt', 'lte'];
        case 'number':
            return ['exact', 'gt', 'gte', 'lt', 'lte'];
        case 'raw_text':
            return ['contains'];
        case 'term':
            return ['exact'];
        case 'text':
            return ['icontains'];
        case 'varchar':
            return ['exact', 'contains', 'istartswith', 'iendswith'];
        case 'year':
            return ['exact', 'gt', 'gte', 'lt', 'lte'];
    }
};

// const renderFilter =
//     (f: QueryFilter) =>
//         DIV( 'statement-record' },
//             `${f.field.join('__')} ${Operations[f.op]()} ${f.value}`);

const renderFilter = (f: QueryFilter, u: InformationUnitName) =>
    SPAN(
        {},
        // SPAN( 'statement-record__field' , fieldDisplayName(u, `${f.field}`)),
        // SPAN( 'statement-record__field' , f.field.join('__')),
        iife(() => {
            const { fieldName, unitName } = defaultFieldNameParts(
                u,
                f.field[0]
            );
            return fieldName.fold(
                DIV('statement-record__unit-label only-unit', unitName),
                fieldName =>
                    DIV(
                        'ui-list__unit-field',
                        SPAN('statement-record__unit-label', unitName),
                        SPAN('statement-record__unit-separator', ' → '),
                        SPAN('statement-record__unit-field', fieldName)
                    )
            );
        }),
        SPAN('statement-record__operator', Operations[f.op]()),
        SPAN('statement-record__value', renderValue(f.value, u, f.field[0]))
    );

const renderQueryStatement = (s: QueryStatement) =>
    DIV(
        'statement-record',
        // SPAN( 'statement-record__unit-label' }, unitDisplayName(s.unit)),
        ...s.filters.map(f => renderFilter(f, s.unit))
    );

const renderStatementConj = (s: QueryConj) => {
    if (s.conj === 'AND') {
        return DIV('statement--operator', tr.angled('conj/AND'));
    }
    return DIV('statement--operator', tr.angled('conj/OR'));
};

export const renderWhereStatement = (w: QueryWhere) => {
    switch (w.tag) {
        case 'statement':
            return renderQueryStatement(w);
        case 'conj':
            return renderStatementConj(w);
    }
};

const execHandler = () =>
    getCurrentQuery().foldL(navigateExec, q => navigateExecSaved(q.id));
const noop = () => void 0;

const withStatements = fromPredicate<Readonly<QueryWhere[]>>(
    sl => sl.length > 0
);

const queryName = () =>
    getCurrentQuery().fold(tr.angled('newQuery'), q => fromRecord(q.name));

export const render = () =>
    withStatements(getWhereStatementList()).fold(
        DIV(
            'statement empty',
            H2('head-title', queryName()),
            DIV(
                'statement__actions',
                clearButton(noop, 'inactive'),
                execButton(noop, 'inactive')
            )
        ),
        statements =>
            DIV(
                'statement',
                H2('head-title', queryName()),
                DIV('statement--tagline', tr.angled('statementTagline')),
                DIV(
                    'statement--wrapper',
                    ...statements.map(renderWhereStatement)
                ),
                DIV(
                    'helptext',
                    stringToParagraphs(tr.angled('addStatementInfos'))
                ),

                DIV(
                    'statement__actions',
                    clearButton(clearWhereBuilderAndStatements),
                    execButton(execHandler)
                )
            )
    );

export default render;
