import * as debug from 'debug';
import { init } from 'sdi/components/modal';

const logger = debug('sdi:query/modal');

export const { render, register } = init();
export default render;

export { render as renderPublish } from './publish';
export { render as renderSelect } from './select';
export { render as renderGeometry } from './geometry';
export { render as renderColumnName } from './column-name';

logger('loaded');
