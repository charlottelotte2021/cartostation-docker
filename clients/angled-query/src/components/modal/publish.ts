import { fromPredicate, none } from 'fp-ts/lib/Option';

import { ModalRender } from 'sdi/components/modal';
import { DIV, NODISPLAY, H2 } from 'sdi/components/elements';
import {
    inputText,
    inputLongText,
    renderCheckbox,
    attrOptions,
} from 'sdi/components/input';
import tr from 'sdi/locale';
import { scopeOption } from 'sdi/lib';

import { makeLabelAndIcon, makeLabel } from 'angled-core/components/buttons';
import {
    currentQueryHasMetadata,
    currentQueryHasMetadataGU,
    findShare,
    getCurrentQueryId,
    getQueryName,
    getShareDescriptionForLang,
    getCurrentQuery,
} from 'angled-query/src/queries/builder';
import {
    saveMetadata,
    setQueryName,
    setShareDescription,
    saveNewQuery,
    shareQuery,
    unShareQuery,
    updateQuery,
} from 'angled-query/src/events/builder';
import { navigateExecSaved } from 'angled-query/src/events/route';
import { stringToParagraphs } from 'sdi/util';
import { closePublish } from 'angled-query/src/events/modal';

const buttonSave = makeLabelAndIcon('save', 1, 'check', () => tr.core('save'));
const buttonClose = makeLabelAndIcon('close', 1, 'check', () =>
    tr.core('close')
);
const buttonAddShare = makeLabel('publish', 1, () => tr.angled('shareQuery'));
const buttonRemoveShare = makeLabel('remove', 1, () =>
    tr.angled('unShareQuery')
);

const switchPublishPoint = (qid: number) =>
    renderCheckbox(
        'publish-point',
        () => tr.angled('publishPoint'),
        value => (value ? saveMetadata(qid, 'point') : none)
    )(currentQueryHasMetadataGU('point'));

const switchPublishLine = (qid: number) =>
    renderCheckbox(
        'publish-line',
        () => tr.angled('publishLine'),
        value => (value ? saveMetadata(qid, 'line') : none)
    )(currentQueryHasMetadataGU('line'));

const switchPublishPolygon = (qid: number) =>
    renderCheckbox(
        'publish-polygon',
        () => tr.angled('publishPolygon'),
        value => (value ? saveMetadata(qid, 'polygon') : none)
    )(currentQueryHasMetadataGU('polygon'));

const checkStringLength = fromPredicate<string>(s => s.length > 0);

const renderNameInput = () => {
    const onSave = () => {
        getCurrentQuery().foldL(
            () => {
                saveNewQuery(q => navigateExecSaved(q.id));
            },
            _qid => {
                updateQuery();
            }
        );
    };

    const save = scopeOption()
        .let('fr', checkStringLength(getQueryName('fr')()))
        .let('nl', checkStringLength(getQueryName('nl')()))
        .fold(
            buttonSave(() => void 0, 'disabled'),
            () => buttonSave(onSave)
        );

    const inputs = [
        inputText(
            attrOptions(
                'query-name-fr',
                getQueryName('fr'),
                setQueryName('fr'),
                {
                    placeholder: tr.angled('insertNameFR'),
                }
            )
        ),
        inputText(
            attrOptions(
                'query-name-nl',
                getQueryName('nl'),
                setQueryName('nl'),
                {
                    placeholder: tr.angled('insertNameNL'),
                }
            )
        ),
    ];

    return DIV(
        { className: 'modal__content--item query--save' },
        H2({}, tr.angled('saveQueryTitle')),
        DIV({ className: 'helptext' }, tr.angled('saveQueryHelptxt')),
        ...inputs,
        save
    );
};

const isShareDescriptionOk = () =>
    checkStringLength(getShareDescriptionForLang('fr')())
        .chain(() => checkStringLength(getShareDescriptionForLang('nl')()))
        .isSome();

const renderShareButton = () =>
    getCurrentQueryId().map(qid => {
        if (currentQueryHasMetadata()) {
            return NODISPLAY();
        }
        return findShare(qid).foldL(
            () =>
                isShareDescriptionOk()
                    ? buttonAddShare(() => shareQuery(qid))
                    : buttonAddShare(() => void 0, 'disabled'),
            () => buttonRemoveShare(() => unShareQuery(qid))
        );
    });

const renderShareDescriptionInput = () =>
    getCurrentQueryId().map(() =>
        DIV(
            { className: 'modal__content--item query--share' },
            H2({}, tr.angled('shareQueryTitle')),
            DIV(
                { className: 'helptext' },
                stringToParagraphs(tr.angled('shareQueryHelptxt'))
            ),
            inputLongText(
                getShareDescriptionForLang('fr'),
                setShareDescription('fr'),
                {
                    key: 'query-share-description-fr',
                    placeholder: tr.angled('insertDescriptionFR'),
                    rows: 5,
                }
            ),
            inputLongText(
                getShareDescriptionForLang('nl'),
                setShareDescription('nl'),
                {
                    key: 'query-share-description-nl',
                    placeholder: tr.angled('insertDescriptionNL'),
                    rows: 5,
                }
            ),
            renderShareButton()
        )
    );

const renderMDButtons = (qid: number) =>
    DIV(
        { className: 'modal__content--item query--metadata' },
        H2({}, tr.angled('publishQueryGeomTitle')),
        DIV(
            { className: 'helptext' },
            stringToParagraphs(tr.angled('publishQueryGeomHelptxt'))
        ),
        switchPublishPoint(qid),
        switchPublishLine(qid),
        switchPublishPolygon(qid)
    );

const renderMetadataInput = () =>
    scopeOption()
        .let('qid', getCurrentQueryId())
        .let('s', ({ qid }) => findShare(qid))
        .map(({ qid }) => renderMDButtons(qid));

export const renderButtons = (onCancel: () => void) => {
    const cancel = buttonClose(onCancel);
    const buttons = [cancel];

    return DIV({ className: '_buttons' }, ...buttons);
};

export const renderInputs = () =>
    DIV(
        { className: 'modal__content--item-list modal--publish-query' },
        renderNameInput(),
        renderShareDescriptionInput(),
        renderMetadataInput()
    );

export const render: ModalRender = {
    header: () => H2({}, tr.angled('publishQueryTitle')),
    footer: () => renderButtons(closePublish),
    body: () => renderInputs(),
};

export default render;
