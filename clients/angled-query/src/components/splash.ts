import { DIV, H1 } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { getAppName } from '../queries/app';

const render = () =>
    DIV(
        {},
        H1({}, getAppName()),
        DIV('splash-content', tr.angled('loadingData'), DIV('loader-anim'))
    );

export default render;
