// imports from sdi
import { IUser } from 'sdi/source';
import { ButtonComponent } from 'sdi/components/button';
import { Collection, Nullable } from 'sdi/util';

export type AppLayout =
    | 'splash'
    | 'landing'
    | 'builder'
    | 'confirm'
    | 'preview';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'app/layout': AppLayout;
        'app/route': string[];

        'data/user': Nullable<IUser>;
        'component/button': ButtonComponent;
        'component/foldable': Collection<boolean>;
    }
}
