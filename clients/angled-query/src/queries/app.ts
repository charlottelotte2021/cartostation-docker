import { query } from 'sdi/shape';
import { fromNullable } from 'fp-ts/lib/Option';
import {
    getApiUrl,
    getAppManifest,
    getLang,
    getUserIdAstNumber,
} from 'sdi/app';
import { appDisplayName, withQueryString } from 'sdi/source';
import tr, { fromRecord } from 'sdi/locale';
import { ExportForm } from 'sdi/components/export';

export const getUserData = () => query('data/user');

export const getUserGroups = () =>
    fromNullable(getUserData())
        .map(u => u.roles.map(r => r.id))
        .getOrElse([]);

export const isQueryEditable = (queryUser: number) => {
    const isAdmin = getAdminQuery()
        .map(admin => getUserGroups().indexOf(admin.toString()) > -1)
        .getOrElse(false);
    const isQueryUser = getUserIdAstNumber()
        .map(uid => uid === queryUser)
        .getOrElse(false);
    return isAdmin || isQueryUser ? true : false;
};

export const getAdminQuery = () => fromNullable(query('app/admin-query'));

export const getLayout = () => query('app/layout');

export const getAppName = () =>
    getAppManifest('angled-query')
        .chain(appDisplayName)
        .map(fromRecord)
        .getOrElse(tr.core('angled:query'));

export const getFolded = (name: string) =>
    fromNullable(query('component/foldable')[name]).fold(true, v => v);

export const getExportLink = (qid: number) => (form: ExportForm) =>
    withQueryString(
        getApiUrl(`geodata/angled/l/${qid}/point`), // geom is not used there
        {
            form,
            lang: getLang(),
        }
    );
