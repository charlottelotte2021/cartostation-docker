import { query, queryOption } from 'sdi/shape';
import { scopeOption } from 'sdi/lib';
import { fromNullable, fromPredicate } from 'fp-ts/lib/Option';
import { QueryStatement } from 'angled-query/src/remote/query';
import { MessageRecordLang } from 'sdi/source';
import { rec } from 'sdi/locale';
import {
    FieldValueType,
    GeometryUnit,
    InformationUnitName,
} from 'angled-core/ui';
import { asMutable } from 'sdi/util';
import { getLang, getUserIdAstNumber } from 'sdi/app';
import { SelectBuilderStep } from '../components/preview/select-multi';
import { unitDisplayName } from 'angled-core/queries/app';

export const getWhereBuilderStep = () => query('component/builder/where/step');

export const getSelectBuilderStep = () =>
    query('component/builder/select/step');

export const withSelectBuilderFiltered = () =>
    fromPredicate<SelectBuilderStep>(s => s === 'Filtered')(
        getSelectBuilderStep()
    );

export const getSelectName = () => queryOption('component/builder/select/name');

export const getSelectNameForm = () =>
    query('component/builder/select/name/form');

export const getSelectUnit = () => queryOption('component/builder/select/ui');

export const getWhereField = () =>
    scopeOption()
        .let('unit', queryOption('component/builder/where/ui'))
        .let('field', queryOption('component/builder/where/field'));

export const getSelectField = () =>
    queryOption('component/builder/select/field');

export const getOp = () => queryOption('component/builder/where/op');

export const getAggregate = () => queryOption('component/builder/select/agg');

export const getAggregateNumberOp = () =>
    queryOption('component/builder/select/agg/number/op');

export const getValue = () => queryOption('component/builder/where/value');

export const getInFlightValue = (dflt: FieldValueType) =>
    queryOption('component/builder/where/in-flight-value').getOrElse(dflt);

export const getBuilderStatement = () =>
    scopeOption()
        .let('unit', queryOption('component/builder/where/ui'))
        .let('op', fromNullable(query('component/builder/where/op')))
        .let(
            'value',
            fromNullable(query('component/builder/where/value')).map(asMutable)
        )
        .let('field', fromNullable(query('component/builder/where/field')))
        .map<QueryStatement>(scope => ({
            tag: 'statement',
            unit: scope.unit,
            filters: [
                {
                    tag: 'filter',
                    field: scope.field.name,
                    negate: false, // TODO
                    op: scope.op,
                    value: scope.value,
                },
            ],
        }));

export const getWhereStatementList = () => query('component/builder/where');

export const getSelectQuery = () => query('component/builder/select');

export const isWhereStatementListEmpty = () =>
    getWhereStatementList().length === 0;

export const getSelectFromCurrentQuery = () =>
    scopeOption()
        .let('query', getCurrentQuery())
        .map(({ query }) => query.statements.select);

const getAllQueries = () => query('data/queries');

export const getUserQueries = () =>
    getUserIdAstNumber().fold([], uid =>
        getAllQueries().filter(q => q.user === uid)
    );

export const getSharedQueries = () =>
    getUserIdAstNumber().fold([], uid =>
        getAllQueries().filter(q => q.user !== uid)
    );

export const findQuery = (id: number) =>
    fromNullable(query('data/queries').find(q => q.id === id));

export const getCurrentQueryId = () =>
    queryOption('component/builder/current-query');

export const getCurrentQuery = () => getCurrentQueryId().chain(findQuery);

export const getQueryName = (l: MessageRecordLang) => () =>
    queryOption('component/builder/query-name').fold<string>('', r =>
        rec(r, l, '')
    );

export const getLocalizedQueryName = () => getQueryName(getLang())(); // make it lazy to not hit `DispatchNotConfigured`

export const getResults = () => query('data/results');

export const getSpatialRefList = () =>
    query('data/datasetMetadata').filter(
        md =>
            md.geometryType === 'Polygon' || md.geometryType === 'MultiPolygon'
    );

const BASELAYER_SERVICE = 'urbis.irisnet.be';
const BASELAYER_LAYER = 'urbis_gray';

export const getSelectBaseLayer = () =>
    fromNullable(
        query('data/baselayers').find(
            service => service.id === BASELAYER_SERVICE
        )
    )
        .chain(service =>
            fromNullable(
                service.layers.find(layer => layer.codename === BASELAYER_LAYER)
            )
        )
        .toNullable();

export const getSelectedFeature = () =>
    queryOption('component/builder/geometry/select');

export const getShareDescription = () =>
    queryOption('component/builder/query-share-desciption');

export const getShareDescriptionForLang = (l: MessageRecordLang) => () =>
    queryOption('component/builder/query-share-desciption').fold<string>(
        '',
        r => rec(r, l, '')
    );

export const isQueryShared = (qid: number) =>
    findShare(qid).fold(false, () => true);

export const getUserSharedQuery = () =>
    getUserQueries().filter(q => isQueryShared(q.id));

export const getUserOnlyQuery = () =>
    getUserQueries().filter(q => !isQueryShared(q.id));

export const findShare = (qid: number) =>
    fromNullable(query('data/shares').find(s => s.query === qid));

export const currentQueryHasMetadataGU = (gu: GeometryUnit) =>
    query('component/builder/metadata/published').indexOf(gu) !== -1;

export const currentQueryHasMetadata = () =>
    currentQueryHasMetadataGU('point') ||
    currentQueryHasMetadataGU('line') ||
    currentQueryHasMetadataGU('polygon');

export const getInfoFilterPattern = () =>
    query('component/builder/info/filter-pattern');

const displayName = (uin: InformationUnitName) =>
    // (uin as string)
    //     .split('_')
    //     .map(part => part[0].toUpperCase() + part.slice(1))
    //     .join(' ');
    unitDisplayName(uin);

const uiMap = () => ({
    actor: displayName('actor'),
    address: displayName('address'),
    address_struct: displayName('address_struct'),
    applicants: displayName('applicants'),
    area: displayName('area'),
    auxiliary_equipments: displayName('auxiliary_equipments'),
    bidders_with_fee: displayName('bidders_with_fee'),
    bidders: displayName('bidders'),
    candidates: displayName('candidates'),
    capakey: displayName('capakey'),
    city_type: displayName('city_type'),
    construction_type: displayName('construction_type'),
    cost: displayName('cost'),
    date_applications: displayName('date_applications'),
    date_committee: displayName('date_committee'),
    date_notice: displayName('date_notice'),
    date_offers: displayName('date_offers'),
    date_opening: displayName('date_opening'),
    document: displayName('document'),
    description: displayName('description'),
    fee: displayName('fee'),
    funding: displayName('funding'),
    horizon: displayName('horizon'),
    housing_batch: displayName('housing_batch'),
    image: displayName('image'),
    line: displayName('line'),
    manager: displayName('manager'),
    name: displayName('name'),
    note: displayName('note'),
    nova: displayName('nova'),
    point: displayName('point'),
    polygon: displayName('polygon'),
    procedure: displayName('procedure'),
    program: displayName('program'),
    program_name: displayName('program_name'),
    project_type: displayName('project_type'),
    public_procurement: displayName('public_procurement'),
    public_survey: displayName('public_survey'),
    school_creation: displayName('school_creation'),
    school_seats: displayName('school_seats'),
    school_info: displayName('school_info'),
    school_track: displayName('school_track'),
    site_description_current: displayName('site_description_current'),
    site: displayName('site'),
    status: displayName('status'),
    step: displayName('step'),
    temporary: displayName('temporary'),
    tender_winner: displayName('tender_winner'),
    zone: displayName('zone'),
});

const sortUnitByAlias = (a: InformationUnitName, b: InformationUnitName) =>
    unitDisplayName(a).localeCompare(unitDisplayName(b));

export const getUIFilteredList = () => {
    const pat = new RegExp(`.*${getInfoFilterPattern()}.*`, 'i');
    const list = Object.keys(uiMap()).sort(sortUnitByAlias);

    return list.filter(c =>
        pat.test(unitDisplayName(c as InformationUnitName))
    );
};
