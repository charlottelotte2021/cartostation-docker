import * as debug from 'debug';

import 'sdi/polyfill';
import { source, AppConfigIO, getMessage } from 'sdi/source';
import { IShape, configure, defaultShape } from 'sdi/shape';
import { defaultCollection } from 'sdi/util';
import {
    defaultBuilderState,
    defaultPreviewState,
    initialRemoteState,
} from './shape';
import { defaultUnitState } from 'angled-core/shape/ui';
import { defaultRef } from 'angled-core/shape/ref';
import { defaultMapState } from 'angled-core/shape/map';
import { defaultUniverse } from 'angled-core/shape/universe';

import 'angled-core/locale';
import App from './app';
import { displayException } from 'sdi/app';

const logger = debug('sdi:index');

export const main = (SDI: any) => {
    AppConfigIO.decode(SDI).fold(
        errors => {
            const textErrors = errors.map(e => getMessage(e.value, e.context));
            displayException(textErrors.join('\n'));
        },
        config => {
            const initialState: IShape = {
                'app/codename': 'angled-query',
                ...defaultShape(config),
                ...defaultBuilderState(),
                ...defaultPreviewState(),
                ...defaultRef(),
                ...defaultUnitState(),
                ...defaultMapState(),
                ...defaultUniverse(),
                ...initialRemoteState(),
                'app/lang': 'fr',
                'app/layout': 'splash',
                'component/button': {},
                'component/foldable': defaultCollection(),

                'data/user': null,
                'data/alias': [],
            };

            try {
                const start = source<IShape>(['app/lang']);
                const store = start(initialState);
                configure(store);
                const app = App(SDI.args)(store);
                app();
            } catch (err) {
                displayException(`${err}`);
            }
        }
    );
};

logger('loaded');
