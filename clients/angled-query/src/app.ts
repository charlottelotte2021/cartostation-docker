import * as debug from 'debug';

import { DIV, NodeOrOptional } from 'sdi/components/elements';
import header from 'sdi/components/header';
import footer from 'sdi/components/footer';
import { loop, getUserId, getApiUrl, loadAlias } from 'sdi/app';

import {
    loadUser,
    loadApplicationData,
    loadMaps,
    activityLogger,
} from './events/app';
import splash from './components/splash';
import { getLayout } from './queries/app';
import { loadRoute, navigateIndex } from './events/route';
import { queryHeaderWhere, queryHeaderSelect } from './components/header';
import landing from './components/landing';
import builder from './components/builder/where';
import preview from './components/preview';
import modal from './components/modal';
import { loadAllBaseLayers } from 'angled-core/events/map';
import { tr } from 'sdi/locale';
import { loadAudienceAll, loadTeamAll } from 'angled-core/events/ref';
import { loadUniverse } from 'angled-core/events/universe';
import { langAction, visitAction } from 'sdi/activity';
import { getLang } from 'sdi/app';

const logger = debug('sdi:app');

const wrappedMain = (name: string, ...elements: NodeOrOptional[]) =>
    DIV(
        { className: 'query-inner' },
        modal(),
        header('angled-query', () =>
            DIV(
                {
                    className: 'navigate app-listview',
                    onClick: navigateIndex,
                },
                tr.angled('toQueryList')
            )
        ),
        DIV({ className: `main ${name}` }, ...elements),
        footer()
    );

const renderBuilder = () =>
    wrappedMain(
        'builder',
        DIV(
            { className: 'builder--wrapper' },
            queryHeaderWhere(),
            DIV({ className: 'content builder--content' }, builder())
        )
    );

const renderConfirm = () =>
    wrappedMain(
        'confirm',
        DIV(
            { className: 'builder--wrapper' }
            // TODO decide if we keep this screen or go full modal
        )
    );

const renderPreview = () =>
    wrappedMain(
        'preview',
        DIV(
            { className: 'builder--wrapper' },
            queryHeaderSelect(),
            DIV({ className: 'content builder--content' }, preview())
        )
    );

const render = () => {
    const layout = getLayout();
    switch (layout) {
        case 'splash':
            return wrappedMain('splash', splash());
        case 'landing':
            return wrappedMain('landing', landing());
        case 'builder':
            return renderBuilder();
        case 'confirm':
            return renderConfirm();
        case 'preview':
            return renderPreview();
    }
};

const effects = (initialRoute: string[]) => () => {
    getUserId().map(userId => loadUser(getApiUrl(`users/${userId}`)));
    loadApplicationData()
        .then(navigateIndex)
        .then(() => loadRoute(initialRoute))
        .catch(err => logger(`failed on : ${err}`));
    loadAllBaseLayers(getApiUrl(`wmsconfig/`));
    loadAlias(getApiUrl('alias'));
    tr.init_edited();
    loadUniverse();
    loadTeamAll();
    loadMaps();
    activityLogger(visitAction());
    activityLogger(langAction(getLang()));
    loadAudienceAll();
};

const app = (initialRoute: string[]) =>
    loop('query', render, effects(initialRoute));
export default app;

logger('loaded');
