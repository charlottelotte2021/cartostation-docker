import * as debug from 'debug';

import {
    register,
    renderPublish,
    renderSelect,
    renderGeometry,
    renderColumnName,
} from '../components/modal';

const logger = debug('sdi:events/modal');

export const [closePublish, openPublish] = register(
    'query/publish',
    renderPublish
);

export const [closeSelect, openSelect] = register('query/select', renderSelect);

export const [closeModalGeometry, openModalGeometry] = register(
    'query/geometry',
    renderGeometry
);

export const [closeModalColumnName, openModalColumnName] = register(
    'query/column-name',
    renderColumnName
);

logger('loaded');
