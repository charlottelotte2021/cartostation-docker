import * as debug from 'debug';
import { tableEvents } from 'sdi/components/table';
import { dispatchK, assign, dispatch, query } from 'sdi/shape';
import { QueryStatement, QueryWhere, QuerySelect } from '../remote/query';
import { InformationUnitName } from 'angled-core/ui';
import {
    GeometryType,
    Inspire,
    StyleConfig,
    ILayerInfo,
    IMapInfo,
    FeatureCollectionIO,
    remoteLoading,
    remoteSuccess,
    remoteError,
    postUnrelatedIO,
    remoteNone,
    MessageRecord,
} from 'sdi/source';
import { nameToCode } from 'sdi/components/button/names';
import { mapSelectID } from 'angled-core/map';
import { SyntheticLayerInfo, getApiUrl, getUserIdAstNumber } from 'sdi/app';
import { addLayer, removeLayerAll } from 'sdi/map';
import { right, left } from 'fp-ts/lib/Either';
import {
    previewMapName,
    getFeatureForSelected,
    getProjectResource,
} from '../queries/preview';
import { none, some } from 'fp-ts/lib/Option';
import { scopeOption } from 'sdi/lib';
import { updateMapView } from 'angled-core/events/map';
import { fetchProject } from 'angled-query/src/remote';

const logger = debug('sdi:preview/events');

export const resultTableEvents = tableEvents(
    dispatchK('component/preview/table')
);

export const extractRecords = (lst: Readonly<QueryWhere[]>) => {
    const records: QueryStatement[] = [];
    lst.forEach(s => {
        if (s.tag === 'statement') records.push(s);
    });
    return records;
};

export const deselectIndex = (i: number) => {
    dispatch('component/builder/select', ss => {
        ss.splice(i, 1);
        return ss;
    });
};

export const deselectUnit = (u: InformationUnitName) => {
    dispatch('component/builder/select', ss => ss.filter(s => s.unit !== u));
};

// MAP

const metadataTemplate = (geometryType: GeometryType): Inspire => ({
    id: geometryType,
    geometryType,
    resourceTitle: { fr: geometryType, nl: geometryType, en: geometryType },
    resourceAbstract: { fr: geometryType, nl: geometryType, en: geometryType },
    uniqueResourceIdentifier: geometryType,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: Date(), revision: Date() },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: Date(),
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
});

const makeStyle = (geometryType: GeometryType): StyleConfig => {
    switch (geometryType) {
        case 'Point':
        case 'MultiPoint':
            return {
                kind: 'point-simple',
                marker: {
                    codePoint: nameToCode('circle'),
                    size: 12,
                    color: 'rgb(223,88,68)',
                },
            };
        case 'LineString':
        case 'MultiLineString':
            return {
                kind: 'line-simple',
                strokeColor: 'rgb(223,88,68)',
                strokeWidth: 1,
                dash: [],
            };
        case 'Polygon':
        case 'MultiPolygon':
            return {
                kind: 'polygon-simple',
                fillColor: 'rgb(223,88,68)',
                pattern: true,
                patternAngle: 45,
                strokeColor: 'rgb(223,88,68)',
                strokeWidth: 0.5,
            };
    }
};

const layerTemplate = (geometryType: GeometryType): ILayerInfo => ({
    id: geometryType,
    legend: null,
    group: null,
    metadataId: geometryType,
    visible: true,
    featureViewOptions: { type: 'default' },
    style: makeStyle(geometryType),
    layerInfoExtra: null,
    visibleLegend: true,
    opacitySelector: false,
});

const mapTemplate = (): IMapInfo => ({
    baseLayer: 'urbis.irisnet.be/urbis_gray',
    id: mapSelectID,
    url: `/dev/null/solar-locate/`,
    lastModified: 1523599299611,
    status: 'published',
    title: { fr: 'SOLAR', nl: 'SOLAR', en: 'SOLAR' },
    description: { fr: 'SOLAR', nl: 'SOLAR', en: 'SOLAR' },
    categories: [],
    attachments: [],
    layers: [
        layerTemplate('MultiPoint'),
        layerTemplate('MultiLineString'),
        layerTemplate('MultiPolygon'),
    ],
});

export const loadMaps = () =>
    dispatch('data/maps', state => state.concat([mapTemplate()]));

const layerInfo = (geometryType: GeometryType) => () =>
    some<SyntheticLayerInfo>({
        name: { fr: geometryType, nl: geometryType, en: geometryType },
        info: layerTemplate(geometryType),
        metadata: metadataTemplate(geometryType),
    });

export const addResultsLayer = () => {
    // act 0 - clearing
    const isReady = removeLayerAll(previewMapName).fold(false, () => true);
    if (!isReady) {
        const timeout = 100;
        logger(`${previewMapName} not ready, retrying in ${timeout}`);
        setTimeout(addResultsLayer, timeout);
        return;
    }
    assign('component/preview/geojson/point', remoteNone);
    assign('component/preview/geojson/line', remoteNone);
    assign('component/preview/geojson/polygon', remoteNone);
    assign('component/preview/map/loading', []);

    scopeOption()
        .let('user', getUserIdAstNumber())
        .let('where', some(query('component/builder/where') as QueryWhere[])) // we cast to drop readonly
        .let('select', some(query('component/builder/select') as QuerySelect[]))
        .map(({ user, where, select }) => {
            const data = { user, statements: { where, select } };

            // act 1 - initiating
            assign('component/preview/geojson/point', remoteLoading);
            assign('component/preview/geojson/line', remoteLoading);
            assign('component/preview/geojson/polygon', remoteLoading);

            // act 2 - fetching
            postUnrelatedIO(
                FeatureCollectionIO,
                getApiUrl(`geodata/angled/l/point`),
                data
            )
                .then(fs =>
                    assign('component/preview/geojson/point', remoteSuccess(fs))
                )
                .catch(err =>
                    assign(
                        'component/preview/geojson/point',
                        remoteError(`${err}`)
                    )
                );
            postUnrelatedIO(
                FeatureCollectionIO,
                getApiUrl(`geodata/angled/l/line`),
                data
            )
                .then(fs =>
                    assign('component/preview/geojson/line', remoteSuccess(fs))
                )
                .catch(err =>
                    assign(
                        'component/preview/geojson/line',
                        remoteError(`${err}`)
                    )
                );
            postUnrelatedIO(
                FeatureCollectionIO,
                getApiUrl(`geodata/angled/l/polygon`),
                data
            )
                .then(fs =>
                    assign(
                        'component/preview/geojson/polygon',
                        remoteSuccess(fs)
                    )
                )
                .catch(err =>
                    assign(
                        'component/preview/geojson/polygon',
                        remoteError(`${err}`)
                    )
                );

            // act 3 - outcoming
            addLayer(previewMapName, layerInfo('MultiPoint'), () => {
                const remoteData = query('component/preview/geojson/point');
                logger(`point => ${remoteData.tag}`);
                switch (remoteData.tag) {
                    case 'none':
                        return left('A reset Occured');
                    case 'error':
                        return left(remoteData.error);
                    case 'loading':
                        return right(none);
                    case 'success':
                        return right(some(remoteData.data));
                }
            });
            addLayer(previewMapName, layerInfo('MultiLineString'), () => {
                const remoteData = query('component/preview/geojson/line');
                switch (remoteData.tag) {
                    case 'none':
                        return left('A reset Occured');
                    case 'error':
                        return left(remoteData.error);
                    case 'loading':
                        return right(none);
                    case 'success':
                        return right(some(remoteData.data));
                }
            });
            addLayer(previewMapName, layerInfo('MultiPolygon'), () => {
                const remoteData = query('component/preview/geojson/polygon');
                switch (remoteData.tag) {
                    case 'none':
                        return left('A reset Occured');
                    case 'error':
                        return left(remoteData.error);
                    case 'loading':
                        return right(none);
                    case 'success':
                        return right(some(remoteData.data));
                }
            });
        });
};

export const updateLoading = (ms: MessageRecord[]) =>
    assign('component/preview/map/loading', ms);

export const showMap = () => {
    assign('component/preview/map', true);
    addResultsLayer();
};

export const hideMap = () => assign('component/preview/map', false);

export const selectProject = (pid: number) =>
    assign('component/preview/table/selected', pid);

export const zoomToFeature = (pid: number) =>
    getFeatureForSelected(pid).map(feature =>
        updateMapView({ dirty: 'geo/feature', feature })
    );

const dispatchProject = dispatchK('data/projects');

export const loadProject = (pid: number) =>
    getProjectResource(pid).foldL(
        () => {
            const key = pid.toString();
            dispatchProject(ps => Object.assign(ps, { [key]: remoteLoading }));
            fetchProject(pid)
                .then(p =>
                    dispatchProject(ps =>
                        Object.assign(ps, { [key]: remoteSuccess(p) })
                    )
                )
                .catch(err =>
                    dispatchProject(ps =>
                        Object.assign(ps, {
                            [key]: remoteError(err.toString()),
                        })
                    )
                );
        },
        res => {
            logger(`Project resource already in store "${pid}": ${res.tag}`);
        }
    );

logger('loaded');
