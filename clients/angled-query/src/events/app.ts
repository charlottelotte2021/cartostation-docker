import { assign, dispatch } from 'sdi/shape';
import { loadAllRefs } from 'angled-core/events/ref';
import { loadDomains, loadTerms } from 'angled-core/events/universe';

import { fetchUser } from '../remote';
import { AppLayout } from '../shape';
import { loadQueries, loadShares } from './builder';
import { getApiUrl } from 'sdi/app';
import { Setoid } from 'fp-ts/lib/Setoid';
import {
    Inspire,
    getMessageRecord,
    ILayerInfo,
    GeometryType,
    StyleConfig,
    IMapInfo,
} from 'sdi/source';
import { uniq } from 'sdi/util';
import { addLayer, removeLayerAll } from 'sdi/map';
import { right } from 'fp-ts/lib/Either';
import { some } from 'fp-ts/lib/Option';
import { querySelectMap } from '../components/builder/value/geometry';
import { fetchAllDatasetMetadata, fetchLayer } from 'angled-core/remote/map';
import { nameToCode } from 'sdi/components/button/names';
import { mapSelectID } from 'angled-core/map';
import { activity } from 'sdi/activity';

export const loadUser = (url: string) =>
    fetchUser(url).then(user => {
        assign('data/user', user);
    });
export const activityLogger = activity('angled-query');

export const setLayout = (l: AppLayout) => assign('app/layout', l);

export const unfold = (name: string) =>
    dispatch('component/foldable', folds => ({ ...folds, [name]: false }));

export const fold = (name: string) =>
    dispatch('component/foldable', folds => ({ ...folds, [name]: true }));

const inspireS: Setoid<Inspire> = {
    equals(a, b) {
        return a.id === b.id;
    },
};

const uniqInspire = uniq(inspireS);

export const loadAllDatasetMetadata = (done?: () => void) =>
    fetchAllDatasetMetadata(getApiUrl('metadatas'))(
        frame => {
            dispatch('data/datasetMetadata', state =>
                uniqInspire(state.concat(frame.results))
            );
            // dispatch('component/table/layers',
            //     ts => ({ ...ts, loaded: 'loading' as LoadingStatus }));
            // dispatch('component/splash', () => Math.floor(frame.page * 100 / frame.total));
        },
        () => {
            // dispatch('component/table/layers',
            //     ts => ({ ...ts, loaded: 'done' as LoadingStatus }));
            if (done) {
                done();
            }
        }
    );

export const loadApplicationData = () =>
    loadQueries()
        .then(loadShares)
        .then(loadDomains)
        .then(loadTerms)
        .then(loadAllRefs)
        .then(() => loadAllDatasetMetadata());

// map select things

const selectLayerInfo = (md: Inspire): ILayerInfo => ({
    id: md.uniqueResourceIdentifier,
    visible: true,
    group: null,
    legend: null,
    metadataId: md.id,
    featureViewOptions: { type: 'default' },
    style: {
        kind: 'polygon-simple',
        fillColor: 'blue',
        pattern: false,
        patternAngle: 0,
        strokeColor: 'white',
        strokeWidth: 1,
    },
    layerInfoExtra: null,
    visibleLegend: true,
    opacitySelector: false,
});

export const loadLayer = (md: Inspire) => {
    removeLayerAll(querySelectMap);
    fetchLayer(md.uniqueResourceIdentifier).then(layer => {
        dispatch('data/layer', state => ({
            ...state,
            [md.uniqueResourceIdentifier]: layer,
        }));
        addLayer(
            querySelectMap,
            () =>
                some({
                    metadata: md,
                    name: getMessageRecord(md.resourceTitle),
                    info: selectLayerInfo(md),
                }),
            () => right(some(layer))
        );
    });
};

// export const loadBaseLayer =
//     (id: string, url: string) => {
//         fetchBaseLayer(url)
//             .then((bl) => {
//                 dispatch('data/baselayers', state => ({ ...state, [id]: bl }));
//             });
//     };

const makeStyle = (geometryType: GeometryType): StyleConfig => {
    switch (geometryType) {
        case 'Point':
        case 'MultiPoint':
            return {
                kind: 'point-simple',
                marker: {
                    codePoint: nameToCode('circle'),
                    size: 12,
                    color: 'rgb(223,88,68)',
                },
            };
        case 'LineString':
        case 'MultiLineString':
            return {
                kind: 'line-simple',
                strokeColor: 'rgb(223,88,68)',
                strokeWidth: 1,
                dash: [],
            };
        case 'Polygon':
        case 'MultiPolygon':
            return {
                kind: 'polygon-simple',
                fillColor: 'rgb(223,88,68)',
                pattern: true,
                patternAngle: 45,
                strokeColor: 'rgb(223,88,68)',
                strokeWidth: 0.5,
            };
    }
};

const layerTemplate = (geometryType: GeometryType): ILayerInfo => ({
    id: geometryType,
    legend: null,
    group: null,
    metadataId: geometryType,
    visible: true,
    featureViewOptions: { type: 'default' },
    style: makeStyle(geometryType),
    layerInfoExtra: null,
    visibleLegend: true,
    opacitySelector: false,
});

const mapTemplate = (): IMapInfo => ({
    baseLayer: 'urbis.irisnet.be/urbis_gray',
    id: mapSelectID,
    url: `/dev/null/solar-locate/`,
    lastModified: 1523599299611,
    status: 'published',
    title: { fr: 'SOLAR', nl: 'SOLAR', en: 'SOLAR' },
    description: { fr: 'SOLAR', nl: 'SOLAR', en: 'SOLAR' },
    categories: [],
    attachments: [],
    layers: [
        layerTemplate('MultiPoint'),
        layerTemplate('MultiLineString'),
        layerTemplate('MultiPolygon'),
    ],
});

export const loadMaps = () =>
    dispatch('data/maps', state => state.concat([mapTemplate()]));
