This app, called _statistics_ in _cartosation_ interface, provides functionalities to see statistics around other apps

## User guide

The complete user guide is available here : https://cartostation.com/documentation

## Add a new activity to log

To log an activity in a client, and then use the data in the statistics client, we need to create an "action model" that can be logged. Here is the way you can declare a new action:

-   create the action model in the appropriate sdi/source/io/activity file, for example (based on a BaseAction model):

    ```
    const OtherActionDataIO = i({
        action: io.literal('other'),
        parameter: i({
            id: io.string,
            foo: io.number,
        })
    }, 'xxx')

    const OtherActionIO = io.intersection([
            BaseActionIO,
            OtherActionDataIO
        ], 'OtherActionIO')

    export type OtherAction = io.TypeOf<typeof OtherActionIO>;
    ```

-   create the function that will make the new action record in sdi/activity/index.ts:
    ```
    export const otherAction =
        (id: string, foo: number): OtherAction
            => ({
                action : 'other',
                parameter: {id, foo}
                })
    ```
-   add the new elements in the three next lists: ActivityActionDataIO, ActivityActionNameIO and ActivityIO in sdi/source/io/activity/public

After that, you can use it in the "activity logger" of your client, to log the activity where it happens:

```
const activityLogger = activity('appName');
activityLogger(otherAction(id, foo))
```
