import { MessageStore, fromRecord } from 'sdi/locale';

const messages = {
    appTitle: {
        fr: 'Monitoring',
        nl: 'Monitoring',
    },
    times: {
        fr: 'fois',
        nl: 'keer',
    },
    mapNumber: {
        fr: 'Nombre de cartes',
        nl: 'Aantal kaarten',
    },
    top10: {
        fr: 'Top 10',
        nl: 'Top 10',
    },
    top5: {
        fr: 'Top 5',
        nl: 'Top 5',
    },
    visitedMaps: {
        fr: 'cartes visitées',
        nl: 'bezochte kaarten',
    },
    mapsVisits: {
        fr: 'visites de cartes',
        nl: 'bezochte kaarten',
    },
    printedMaps: {
        fr: 'cartes imprimées',
        nl: 'gedrukte kaarten',
    },
    countOf: {
        fr: 'nombre de',
        nl: 'aantal of',
    },
    empty: {
        fr: 'vide',
        nl: 'leeg',
    },
    days: {
        fr: 'jours',
        nl: 'dagen',
    },
    hours: {
        fr: 'heures',
        nl: 'uren',
    },
    months: {
        fr: 'mois',
        nl: 'maanden',
    },
    years: {
        fr: 'années',
        nl: 'jaren',
    },
    observedRange: {
        fr: 'Intervalle observé : ',
        nl: '-',
    },
    dashboard: {
        fr: 'Dashboard',
        nl: '-',
    },
    dateRangeTo: {
        fr: 'au',
        nl: '-',
    },
    dateRangeFrom: {
        fr: 'Du',
        nl: '-',
    },
    link: {
        fr: 'Clic sur un lien',
        nl: 'Klik op een link',
    },
    visit: {
        fr: 'Nombre de visites',
        nl: 'Aantal bezoeken', // nltocheck
    },
    'navigate-capakey': {
        fr: 'Selection par capakey',
        nl: 'Selectie door capakey', // nltocheck
    },
    'navigate-detail': {
        fr: 'Je personnalise mon installation   ',
        nl: 'Mijn installatie personaliseren', // nltocheck
    },
    'navigate-solar-contact': {
        fr: 'Trouver mon installateur',
        nl: 'Mijn installateur vinden', // nltocheck
    },
    'navigate-map': {
        fr: 'Aller à la carte',
        nl: 'Naar de kaart', // nltocheck
    },
    'print-map': {
        fr: 'Imprimer la carte',
        nl: 'Kaart afdrukken', // nl done
    },
    'print-report': {
        fr: 'Imprimer le rapport',
        nl: 'Rapport afdrukken', // nltocheck
    },
    'navigate-query': {
        fr: 'Voir la requête',
        nl: 'Zie de zoekopdracht', // nltocheck
    },
    'lang-choice': {
        fr: 'Choix de la langue',
        nl: 'Taalkeuze', // nltocheck
    },
    'select-water-level': {
        fr: 'Sélection du niveau',
        nl: 'Niveau keuze',
    },
    'save-units': {
        fr: "Nombre de modification lors de la modification d'une unité",
        nl: 'Aantal wijzigingen bij het veranderen van een eenheid', // nltocheck
    },
    visualize: {
        fr: 'Visualiser',
        nl: 'Zie', // nltocheck
    },
    withoutName: {
        fr: 'sans nom',
        nl: 'zonder naam',
    },
    errorFetchingData: {
        fr: 'Problème de chargement des données.',
        nl: 'Problemen met het laden van de gegevens.', //nltodo
    },
};

type MDB = typeof messages;
export type StatMessageKey = keyof MDB;

export const isMessageKey = (text: string) => text in messages;

declare module 'sdi/locale' {
    export interface MessageStore {
        stat(k: StatMessageKey): Translated;
    }
}

MessageStore.prototype.stat = (k: StatMessageKey) => fromRecord(messages[k]);
