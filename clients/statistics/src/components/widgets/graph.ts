import { Activity, ActivityActionName } from 'sdi/source/io/activity/index';
import {
    getActivitiesForActionRemote,
    getTimeserieForActionRemote,
} from '../../queries/statistics';
import { DIV, H2, NodeOrOptional, SPAN } from 'sdi/components/elements';
import {
    getAppOccurencesArray,
    getOccurencesArray,
    renderLabel,
} from './occurences';
import timeBarChart from '../charts/time-barchart';
import { pieChart } from '../charts/piechart';
import { barChart } from '../charts/barchar';
import { ChartElem, PlotPoint } from '../charts';
import { foldRemote } from 'sdi/source';
import { none } from 'fp-ts/lib/Option';
import tr from 'sdi/locale';

export const renderTimeBarChart = (
    namespace: string,
    action: ActivityActionName,
    title: string
) =>
    foldRemote<PlotPoint[], string, NodeOrOptional>(
        () => none,
        () =>
            DIV(
                { className: 'info' },
                DIV(
                    'loader',
                    tr.core('loadingData'),
                    DIV({ className: 'loader-spinner' })
                )
            ),
        err =>
            DIV(
                { className: 'error' },
                `${tr.stat('errorFetchingData')} (${err})`
            ),
        timeserie =>
            DIV(
                { className: 'container container--widget occurences' },
                DIV(
                    { className: 'widget__header' },
                    H2({ className: 'widget__name' }, title)
                ),
                DIV(
                    { className: 'widget__body' },
                    timeBarChart(timeserie, 'Day')
                )
            )
    )(getTimeserieForActionRemote(namespace, action));

// export const renderTimeBarChart = (
//     namespace: string,
//     action: ActivityActionName,
//     title: string
// ) =>
//     DIV(
//         { className: 'container container--widget occurences' },
//         DIV(
//             { className: 'widget__header' },
//             H2({ className: 'widget__name' }, title)
//         ),
//         DIV(
//             { className: 'widget__body' },
//             timeBarChart(getTimeserieForAction(namespace, action), 'Day')
//         )
//     );

const getBarChartElements = (activities: Activity[]) =>
    getOccurencesArray(activities).map(o => ({
        label: renderLabel(o[0], activities),
        value: o[1],
    }));

const getMultiAppChartElements = (activities: Activity[]) =>
    getAppOccurencesArray(activities).map(
        o =>
            ({
                label: SPAN('label', o[0]),
                value: o[1],
            } as ChartElem)
    );

export const renderBarChart = (
    namespace: string,
    action: ActivityActionName,
    title: string
) =>
    foldRemote<Activity[], string, NodeOrOptional>(
        () => none,
        () =>
            DIV(
                { className: 'info' },
                DIV(
                    'loader',
                    tr.core('loadingData'),
                    DIV({ className: 'loader-spinner' })
                )
            ),
        err =>
            DIV(
                { className: 'error' },
                `${tr.stat('errorFetchingData')} (${err})`
            ),
        activities =>
            DIV(
                { className: 'container container--widget occurences' },
                DIV(
                    { className: 'widget__header' },
                    H2({ className: 'widget__name' }, title)
                ),
                DIV(
                    { className: 'widget__body' },

                    barChart(getBarChartElements(activities))
                )
            )
    )(getActivitiesForActionRemote(namespace, action));

export const renderMultiAppBarChart = (
    namespaces: string[],
    action: ActivityActionName,
    title: string
) =>
    foldRemote<Activity[], string, NodeOrOptional>(
        () => none,
        () =>
            DIV(
                { className: 'info' },
                DIV(
                    'loader',
                    tr.core('loadingData'),
                    DIV({ className: 'loader-spinner' })
                )
            ),
        err =>
            DIV(
                { className: 'error' },
                `${tr.stat('errorFetchingData')} (${err})`
            ),
        activities =>
            DIV(
                { className: 'container container--widget occurences' },
                DIV(
                    { className: 'widget__header' },
                    H2({ className: 'widget__name' }, title)
                ),
                DIV(
                    { className: 'widget__body' },

                    barChart(getMultiAppChartElements(activities))
                )
            )
    )(getActivitiesForActionRemote(namespaces, action));

export const renderPieChart = (
    namespace: string,
    action: ActivityActionName,
    title: string
) =>
    foldRemote<Activity[], string, NodeOrOptional>(
        () => none,
        () =>
            DIV(
                { className: 'info' },
                DIV(
                    'loader',
                    tr.core('loadingData'),
                    DIV({ className: 'loader-spinner' })
                )
            ),
        err =>
            DIV(
                { className: 'error' },
                `${tr.stat('errorFetchingData')} (${err})`
            ),
        activities =>
            DIV(
                { className: 'container container--widget occurences' },
                DIV(
                    { className: 'widget__header' },
                    H2({ className: 'widget__name' }, title)
                ),
                DIV(
                    { className: 'widget__body' },
                    pieChart(
                        getOccurencesArray(activities).map(o => ({
                            label: renderLabel(o[0], activities),
                            value: o[1],
                        })),
                        70
                    )
                )
            )
    )(getActivitiesForActionRemote(namespace, action));
