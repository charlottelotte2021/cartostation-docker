import {
    getActivitiesForActionRemote,
    getAppDisplayName,
} from '../../queries/statistics';
import {
    A,
    DIV,
    H2,
    LI,
    NodeOrOptional,
    SPAN,
    UL,
} from 'sdi/components/elements';
import {
    Activity,
    ActivityActionName,
    ExportAction,
} from 'sdi/source/io/activity';
import tr, { fromRecord } from 'sdi/locale';
import { fromNullable, some, Option, none } from 'fp-ts/lib/Option';
import { getAlias, getRoot } from 'sdi/app';
import { foldRemote, MessageRecord } from 'sdi/source';
import { aggregate } from './aggregate';
import { AggregationType } from 'statistics/src/dashboard';

const makeKey = (activity: Activity): Option<string> => {
    switch (activity.action) {
        case 'navigate-map':
            return some(activity.parameter.id);
        case 'print-map':
            return some(activity.parameter.id);
        case 'visit':
            return some(activity.namespace);
        case 'link':
            return some(activity.parameter.link);
        case 'print-report':
            return some(activity.parameter.address);
        case 'navigate-detail':
            return some(activity.parameter.system);
        case 'navigate-solar-contact':
            return some(activity.parameter.system);
        case 'navigate-capakey':
            return some(activity.parameter.capakey);
        case 'navigate-query':
            return some(fromRecord(activity.parameter.title));
        case 'lang-choice':
            return some(activity.parameter.lang);
        case 'save-units':
            return some(activity.parameter.numberOfEdits.toString());
        case 'select-water-level':
            return some(activity.parameter.type);
        case 'export':
            return some(keyOrFileName(activity));
        case 'checked':
            return some(activity.parameter.id);
    }
};

const labelOrId = (mid: string, label: MessageRecord) => {
    const translated = fromRecord(label);
    return translated === ''
        ? `${mid} (${tr.stat('withoutName')})`
        : translated;
};

const renderMapLink = (mid: string, label: MessageRecord) =>
    A(
        {
            className: 'map-link',
            href: `${getRoot()}view/${mid}`,
        },
        labelOrId(mid, label)
    );

const keyOrFileName = (activity: ExportAction) =>
    activity.parameter.key
        ? activity.parameter.key
        : activity.parameter.fileName;

export const renderLabel = (key: string, activities: Activity[]) => {
    const activity = fromNullable(
        activities.find(a =>
            makeKey(a)
                .map(k => k === key)
                .getOrElse(false)
        )
    );
    return SPAN(
        'label',
        activity.map(a => {
            switch (a.action) {
                case 'navigate-map':
                    return renderMapLink(a.parameter.id, a.parameter.title);
                case 'print-map':
                    return renderMapLink(a.parameter.id, a.parameter.title);
                case 'link':
                    return A({ href: a.parameter.link }, a.parameter.link);
                case 'visit':
                    return getAlias(a.namespace);
                case 'print-report':
                    return a.parameter.address;
                case 'navigate-detail':
                    return getAlias(a.parameter.system);
                case 'navigate-solar-contact':
                    return `Contact (${getAlias(a.parameter.system)})`;
                case 'navigate-capakey':
                    return a.parameter.capakey;
                case 'navigate-query':
                    return a.parameter.title;
                case 'lang-choice':
                    return getAlias(a.parameter.lang);
                case 'save-units':
                    return a.parameter.numberOfEdits;
                case 'select-water-level':
                    return getAlias(a.parameter.type);
                case 'export':
                    return getAlias(keyOrFileName(a));
                case 'checked':
                    return a.parameter.id;
            }
        })
    );
};

const getOccurences = (activities: Activity[]) => {
    const occurences: { [key: string]: number } = {};
    activities.map(a => {
        const key = makeKey(a);
        key.map(k => {
            occurences[k] = (occurences[k] || 0) + 1;
        });
    });
    return occurences;
};

export const getOccurencesArray = (
    activities: Activity[]
): [string, number][] => {
    const occurences = getOccurences(activities);
    return Object.keys(getOccurences(activities)).map(m => [m, occurences[m]]);
};

const getAppOccurences = (activities: Activity[]) => {
    const occurences: { [key: string]: number } = {};
    activities.map(a => {
        const key = a.namespace;
        occurences[key] = (occurences[key] || 0) + 1;
    });
    return occurences;
};

export const getAppOccurencesArray = (activities: Activity[]) => {
    const occurences = getAppOccurences(activities);
    return Object.keys(getAppOccurences(activities)).map(m => [
        getAppDisplayName(m),
        occurences[m],
    ]);
};

export const getMostVisited = (n: number, activities: Activity[]) =>
    getOccurencesArray(activities)
        .sort((a, b) => b[1] - a[1])
        .slice(0, n);

export const getLessVisited = (n: number, activities: Activity[]) =>
    getOccurencesArray(activities)
        .sort((a, b) => a[1] - b[1])
        .slice(0, n);

const renderList = (
    n: number,
    appName: string,
    action: ActivityActionName,
    lessVisited = false
) =>
    foldRemote<Activity[], string, NodeOrOptional>(
        () => none,
        () =>
            DIV(
                { className: 'info' },
                DIV(
                    'loader',
                    tr.core('loadingData'),
                    DIV({ className: 'loader-spinner' })
                )
            ),
        err =>
            DIV(
                { className: 'error' },
                `${tr.stat('errorFetchingData')} (${err})`
            ),
        activities => {
            const list = lessVisited
                ? getLessVisited(n, activities)
                : getMostVisited(n, activities);
            if (list.length > 0) {
                return UL(
                    'occurence-list',
                    list.map((a, i) =>
                        LI(
                            {
                                className: 'occurence-element',
                                key: i,
                            },
                            renderLabel(a[0], activities),
                            SPAN('value', ` ${a[1]} ${tr.stat('times')}`)
                        )
                    )
                );
            } else {
                return DIV('occurence-list empty', tr.stat('empty'));
            }
        }
    )(getActivitiesForActionRemote(appName, action));

const renderListMulti = (
    appNames: string[],
    action: ActivityActionName,
    aggregation: AggregationType
) =>
    UL(
        'occurence-list',
        appNames.map((a, i) =>
            LI(
                {
                    className: 'app-list-element',
                    key: i,
                },
                SPAN('app-name', a),
                aggregate(a, action, aggregation)
            )
        )
    );

export const renderBestOfOneApp = (
    n: number,
    appName: string,
    actionName: ActivityActionName,
    title: string
) =>
    DIV(
        { className: 'container container--widget occurences' },
        DIV(
            { className: 'widget__header' },
            H2({ className: 'widget__name' }, title)
        ),
        DIV({ className: 'widget__body' }, renderList(n, appName, actionName))
    );

export const renderWorstOfOneApp = (
    n: number,
    appName: string,
    actionName: ActivityActionName,
    title: string
) =>
    DIV(
        { className: 'container container--widget occurences' },
        DIV(
            { className: 'widget__header' },
            H2({ className: 'widget__name' }, title)
        ),
        DIV(
            { className: 'widget__body' },
            renderList(n, appName, actionName, true)
        )
    );

export const renderMultiAppList = (
    aggregation: AggregationType,
    appNames: string[],
    actionName: ActivityActionName,
    title: string
) =>
    DIV(
        { className: 'container container--widget multi-app-list' },
        DIV(
            { className: 'widget__header' },
            H2({ className: 'widget__name' }, title)
        ),
        DIV(
            { className: 'widget__body' },
            renderListMulti(appNames, actionName, aggregation)
        )
    );
