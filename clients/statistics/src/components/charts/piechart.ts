import { createElement } from 'react';
import { DIV, SPAN } from 'sdi/components/elements';
import {
    ChartElem,
    getColor,
    svg,
    text,
    circle,
    LABELFONTSIZE,
    group,
    line,
} from '.';

const XOFFSET = 20;
const YOFFSET = 20;

const LABELBORDERSIZE = 7;

const alphabet = [
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'x',
    'y',
    'z',
];
const getLetter = (n: number) => {
    if (n >= 0 && n < alphabet.length) {
        return alphabet[n].toUpperCase();
    }
    return n.toString();
};

const deg2rad = (d: number) => (d * Math.PI) / 180;

const polar2cartesian = (
    R: number,
    theta: number,
    center: [number, number]
) => {
    const x = R * Math.cos(deg2rad(theta)) + center[0];
    const y = R * Math.sin(deg2rad(theta)) + center[1];
    return [x, y];
};

const arcString: (a: number, b: number, c: number) => string = (
    radius,
    start,
    stop
) => {
    const large = stop - start > 180;
    const sweepFlag = 1;
    const largeArcFlag = large ? 1 : 0;
    const center: [number, number] = [radius, radius];
    const [sx, sy] = polar2cartesian(radius, start, center);
    const [ex, ey] = polar2cartesian(radius, stop, center);

    return `
    M ${sx} ${sy} 
    A ${radius} ${radius} 0 ${largeArcFlag} ${sweepFlag} ${ex} ${ey}
    L ${radius} ${radius} Z `;
};

const normData = (data: ChartElem[]) => {
    const tot = data.reduce((acc, val) => acc + val.value, 0);
    return data.map(v => (v.value * 360) / tot);
};

const tot = (data: ChartElem[]) =>
    data.reduce((acc, val) => acc + val.value, 0);

const pieLegend = (data: ChartElem[]) =>
    DIV(
        'pie-legend',
        data.map((d, idx) =>
            DIV(
                'pie-legend-item',
                DIV('letter', getLetter(idx)),
                DIV(
                    'label',
                    d.label,
                    SPAN(
                        'percent',
                        ` ${Math.round((d.value / tot(data)) * 100)}%`
                    )
                )
            )
        )
    );

export const pieChart = (data: ChartElem[], radius: number) => {
    const center: [number, number] = [radius, radius];
    const angles = normData(data);
    let offset = 0;

    const arcs = angles.map((val, idx) => {
        const color = getColor(idx);
        const labelAngle = offset + val / 2;
        const [labelX, labelY] = polar2cartesian(
            radius + 11,
            labelAngle,
            center
        );
        if (val >= 360) {
            return circle(radius, radius, radius, { fill: color });
        }
        const path = arcString(radius, offset, offset + val);
        offset += val;

        const label = text(labelX, labelY, `${getLetter(idx)}`, 'middle', {
            fill: 'black',
            fontSize: LABELFONTSIZE,
            dominantBaseline: 'central',
        });
        const labelLine = line(labelX, labelY, center[0], center[1], {
            stroke: 'black',
            strokeWidth: 0.4,
        });
        const labelCircle = circle(labelX, labelY, LABELBORDERSIZE, {
            stroke: 'black',
            strokeWidth: 0.4,
            fill: 'white',
        });
        return group([
            labelLine,
            labelCircle,
            createElement('path', {
                d: path,
                fill: color,
                id: `path-${(idx + 1).toString()}`,
            }),
            label,
        ]);
    });

    return DIV(
        'piechart',
        pieLegend(data),
        svg(arcs, {
            viewBox: `-${XOFFSET} -${YOFFSET} ${(radius + XOFFSET) * 2} ${
                (radius + YOFFSET) * 2
            }`,
        })
    );
};
