import { createElement } from 'react';
import { NodeOrOptional } from 'sdi/components/elements';
import { uniqId } from 'sdi/util';
import { IDimensions } from './time-barchart';

export const AXIS_COLOR = 'black';
export const BAR_COLOR = '#df5844';

export const LABELFONTSIZE = '0.6em';

export const graphsize: IDimensions = { width: 600, height: 200 };
export type DOMProperties = { [key: string]: any };

export type tickAlignment = 'start' | 'middle' | 'end';

export interface IPadding {
    top: number;
    right: number;
    bottom: number;
    left: number;
}
export const padding: IPadding = { top: 25, right: 30, bottom: 25, left: 30 };
export const innerPadding: IPadding = {
    top: 10,
    right: 20,
    bottom: 0,
    left: 20,
};

export const svg = (
    els: React.ReactSVGElement[],
    properties?: Record<string, unknown>
) =>
    createElement(
        'svg',
        {
            viewBox: `-${padding.left} -${padding.top} ${graphsize.width + padding.left + padding.right
                } ${graphsize.height + padding.top + padding.bottom}`,
            key: uniqId(),
            ...properties,
        },
        ...els
    );

export const line = (
    x1: number,
    y1: number,
    x2: number,
    y2: number,
    properties?: DOMProperties
) => createElement('line', { x1, y1, x2, y2, key: uniqId(), ...properties });

export const text = (
    x: number,
    y: number,
    text: string,
    textAnchor: tickAlignment = 'start',
    properties?: DOMProperties
) =>
    createElement(
        'text',
        { x, y, textAnchor, key: uniqId(), ...properties },
        text
    );

export const circle = (
    cx: number,
    cy: number,
    r: number,
    properties?: DOMProperties
) =>
    createElement(
        'circle',
        { cx, cy, r, ...properties },
        createElement('title', {})
    );

export const circleWithTitle = (
    cx: number,
    cy: number,
    r: number,
    title: string,
    properties?: DOMProperties
) =>
    createElement(
        'circle',
        { cx, cy, r, ...properties },
        createElement('title', {}, title)
    );

export const rect = (
    x: number,
    y: number,
    width: number,
    height: number,
    properties?: DOMProperties
) =>
    createElement('rect', {
        x,
        y,
        width,
        height,
        key: uniqId(),
        ...properties,
    });

export const rectWithTitle = (
    x: number,
    y: number,
    width: number,
    height: number,
    title: string,
    properties?: DOMProperties
) => createElement('rect',
    { x, y, width, height, key: uniqId(), ...properties }, createElement('title', {}, title))

export const group = (
    elems: React.ReactSVGElement[],
    properties?: Record<string, unknown>
) => createElement('g', { key: uniqId(), ...properties }, ...elems);

export interface PlotPoint {
    date: number;
    value: number;
}

export interface ChartElem {
    label: NodeOrOptional;
    value: number;
}
const COLORS = [
    '#D13932',
    '#EBA716',
    '#D64F2C',
    '#F0BD11',
    '#DB6527',
    '#F5D30B',
    '#E07B21',
    '#FAE906',
    '#E5911C',
    '#FFFF00',
];

export const getColor = (i: number) => {
    const colorIndex = i % COLORS.length;
    return COLORS[colorIndex];
};
