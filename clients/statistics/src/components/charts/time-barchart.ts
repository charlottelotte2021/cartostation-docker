import { fromNullable, none, some } from 'fp-ts/lib/Option';
import { ReactSVGElement } from 'react';
import { DIV } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { fmtDateNb } from 'sdi/util';
import {
    graphsize,
    group,
    innerPadding,
    LABELFONTSIZE,
    line,
    padding,
    PlotPoint,
    rectWithTitle,
    svg,
    text,
} from './index';

const AXIS_COLOR = '#bfbfbf';
const BAR_COLOR = '#df5844';

export interface IDimensions {
    width: number;
    height: number;
}

export const YAXISDISTANCE = 27;
export const MAXLENGTH = 800;
export const MAXNBDASHES = 30;

const MIN_TIMESTAMP = 0;
const MAX_TIMESTAMP = new Date().getTime();

const FORMAT_AXIS = [
    // [0.00001, 1, 1],
    // [0.1, 0.005, 3],
    // [1, 0.05, 2],
    // [2, 0.1, 1],
    [20, 1, 0],
    [200, 10, 0],
    [2000, 100, 0],
    [20000, 1000, 0],
    [50000, 5000, 0],
    [100000, 10000, 0],
];

type FormatAndDivide = [number, (n: number) => string];

const subdivision = (range: number) =>
    fromNullable(FORMAT_AXIS.find(([r]) => range < r))
        .map<FormatAndDivide>(([_, divider, toFixedArg]) => [
            divider,
            (n: number) => n.toFixed(toFixedArg),
        ])
        .getOrElse([1, (n: number) => n.toFixed(2)]);

type Interval = 'Hour' | 'Day' | 'Month' | 'Year';

/**
 * The duration of the given interval in milliseconds
 * @param interval Interval
 */
const getIntervalTime = (interval: Interval): number => {
    switch (interval) {
        case 'Hour':
            return 1000 * 60 * 60;
        case 'Day':
            return 1000 * 60 * 60 * 24;
        case 'Month':
            // TODO: make it more accurate?
            return 1000 * 60 * 60 * 24 * 30;
        case 'Year':
            // TODO: make it more accurate?
            return 1000 * 60 * 60 * 24 * 365;
    }
};

// /**
//  * Unit (in pixels) for one millisecond, for a given time interval
//  * @param unit x-unit in pixels
//  * @param interval Interval
//  */
// const toMillisecondUnit = (unit: number, interval: Interval): number => {
//     switch (interval) {
//         case 'Hour':
//             return unit / getIntervalTime('Hour');
//         case 'Day':
//             return unit / getIntervalTime('Day');
//         case 'Month':
//             return unit / getIntervalTime('Month');
//         case 'Year':
//             return unit / getIntervalTime('Year');
//     }
// };

const getFirstDate = (data: PlotPoint[]) =>
    data.length > 0 ? data[0].date : MIN_TIMESTAMP;
const getLastDate = (data: PlotPoint[]) =>
    data.length > 0 ? data[data.length - 1].date : MAX_TIMESTAMP;

// const getMinY = (data: PlotPoint[]) =>
//     data.map(d => d.value).reduce((acc, val) => (val < acc ? val : acc));

const getMaxY = (data: PlotPoint[]) =>
    data.map(d => d.value).reduce((acc, val) => (val > acc ? val : acc));

// // TO USE IF WE WANT TO COMPUTE INTERVAL FROM DATA, considering data is sorted (from dates)
// const getInterval = (data: PlotPoint[]): Interval => {
//     if (data.length > 0) {
//         const firstDate = getFirstDate(data);
//         const lastDate = getLastDate(data);
//         const diff = lastDate - firstDate;
//         const nbYears =
//             new Date(lastDate).getFullYear() -
//             new Date(firstDate).getFullYear();
//         const nbMonths = diff / getIntervalTime('Month');
//         const nbDays = diff / getIntervalTime('Day');

//         if (nbYears > 3) {
//             return 'Year';
//         } else if (nbMonths > 2) {
//             return 'Month';
//         } else if (nbDays > 1) {
//             return 'Day';
//         }
//     }
//     return 'Hour';
// };

const beginOfInterval = (date: number, interval: Interval) => {
    const begin = new Date(date);
    switch (interval) {
        case 'Year':
            begin.setFullYear(begin.getFullYear(), 0, 1);
            break;
        case 'Month':
            begin.setMonth(begin.getMonth(), 1);
            break;
        case 'Day':
            begin.setHours(0);
            break;
        case 'Hour':
            begin.setMinutes(0);
            break;
    }
    return begin.getTime();
};

const xMillisecondRange = (data: PlotPoint[], interval: Interval) => {
    const min = beginOfInterval(getFirstDate(data), interval);
    const max = getLastDate(data);
    if (max > min) {
        return max - min;
    } else {
        return 1;
    }
};

interface BarChartInfo {
    timeserie: PlotPoint[];
    id: number;
    color: string;
    interval: Interval;
    rectWidth: number;
    xFactor: number;
    xMin: number;
    yUnit: number;
    yMin: number;
}

/**
 * X value in pixels from value in milliseconds
 * @param value value in milliseconds
 * @param xUnit number of pixels by millisecond
 * @param min minimal y value in milliseconds
 */
const getXInPixel = (value: number, xFactor: number, min: number) => {
    return (value - min) * xFactor + innerPadding.left;
};
/**
 * Height (y value) in pixels
 * @param value value in original unit
 * @param yUnit number of pixels by original unit
 * @param min minimal y value in original unit
 */
//
const getYInPixel = (value: number, yUnit: number, min: number) => {
    return graphsize.height - ((value - min) * yUnit + innerPadding.bottom);
};
const getYHeightInPixel = (value: number, yUnit: number, min: number) => {
    return (value - min) * yUnit + innerPadding.bottom;
};

// const pointChart = (chartInfo: ChartInfo) => {
//     const ts = chartInfo.timeserie;

//     let points: ReactSVGElement[] = [];
//     let x: number = 0;
//     let y: number = 0;
//     points = ts.map((e, i) => {

//         x = getXInPixel(e.date, chartInfo.xFactor, chartInfo.xMin);
//         y = getYInPixel(
//             e.value,
//             chartInfo.yUnit,
//             chartInfo.yMin
//         );
//         const l = circleWithTitle(x, y, 3, `${i}`, {
//             stroke: chartInfo.color,
//             fill: 'green',
//             className: `point`,
//         });
//         return l;
//     });

//     return group(points, {
//         className: `timeserie-pointchart`,
//     });
// };

// const begin = (date: number, interval: Interval) => date - date % getIntervalTime(interval);

const rectangles = (chartInfo: BarChartInfo) => {
    const ts = chartInfo.timeserie;

    const rectWidth = chartInfo.rectWidth;
    let x: number;
    let y: number;
    let yHeight = 0;
    const drawings = ts.map(e => {
        x = getXInPixel(
            beginOfInterval(e.date, chartInfo.interval),
            chartInfo.xFactor,
            beginOfInterval(chartInfo.xMin, chartInfo.interval)
        );
        const title = `${e.value}`
        y = getYInPixel(e.value, chartInfo.yUnit, chartInfo.yMin);
        yHeight = getYHeightInPixel(e.value, chartInfo.yUnit, chartInfo.yMin);
        const r = rectWithTitle(x, y, rectWidth, yHeight, title, {
            stroke: chartInfo.color,
            fill: chartInfo.color,
            className: `rectangle`,
        });
        const t = text(x + rectWidth / 2, y - 5, `${e.value}`, 'middle', {
            className: 'bar-label',
            fill: chartInfo.color,
            fontSize: LABELFONTSIZE,
        });
        return group([r, t]);
    });

    return group(drawings, {
        className: `timeserie-rectangle`,
    });
};

const dateLabel = (date: Date, interval: Interval): string => {
    switch (interval) {
        case 'Hour':
            return `${date.getHours()}`;
        case 'Day':
            return `${fmtDateNb(date.getDate())}/${fmtDateNb(
                date.getMonth() + 1
            )}`;
        case 'Month': {
            const year = `${date.getFullYear()}`.slice(-2);
            return `${fmtDateNb(date.getMonth() + 1)}/${year}`;
        }
        case 'Year':
            return `${date.getFullYear()}`;
    }
};

const incrementDate = (ts: number, interval: Interval): number => {
    const date = new Date(ts);
    switch (interval) {
        case 'Year':
            date.setFullYear(date.getFullYear() + 1, 0, 1);
            break;
        case 'Month':
            date.setMonth(date.getMonth() + 1, 1);
            break;
        // logger(`date: ${date}, month: ${date.getMonth()}`);
        case 'Day':
            date.setDate(date.getDate() + 1);
            break;
        case 'Hour':
            date.setHours(date.getHours() + 1);
            break;
    }
    return date.getTime();
};

const xAxisLabel = (interval: Interval) => {
    switch (interval) {
        case 'Day':
            return tr.stat('days');
        case 'Hour':
            return tr.stat('hours');
        case 'Month':
            return tr.stat('months');
        case 'Year':
            return tr.stat('years');
    }
};


const barchartAxisX = (
    width: number,
    chartInfo: BarChartInfo,
    nbUnit: number
) => {
    const color = AXIS_COLOR;
    const interval = chartInfo.interval;
    const data = chartInfo.timeserie;
    const yHeight = graphsize.height;
    const unit = (width - innerPadding.left - innerPadding.right) / nbUnit;

    const axis = [
        line(0, yHeight, width, yHeight, { stroke: color }),
        line(width - 5, yHeight + 3, width, yHeight, { stroke: color }),
        line(width - 5, yHeight - 3, width, yHeight, { stroke: color }),
        text(width, yHeight + 10, `${xAxisLabel(interval)}`, 'start', {
            fontSize: LABELFONTSIZE,
        }),
    ];
    if (interval === 'Hour') {
        const labelDate = new Date(getFirstDate(data));
        axis.push(
            text(
                width,
                yHeight + 17,
                `${fmtDateNb(labelDate.getDate())}/${fmtDateNb(
                    labelDate.getMonth() + 1
                )}/${labelDate.getFullYear()}`,
                'middle',
                {
                    fontSize: LABELFONTSIZE,
                }
            )
        );
    }

    let ts = beginOfInterval(getFirstDate(data), interval);

    let dist = innerPadding.left;
    for (let i = 0; i < nbUnit; i++) {
        // axis.push(line(dist, yHeight, dist, yHeight + 2, { stroke: color }));

        // if to many labels, just display one of the two and place one of the four lower.
        let disp = 'ok';
        let translation = 0;
        if (nbUnit > 25 && i % 4 != 0) {
            translation = 5;
        }
        if (nbUnit > 15 && i % 2 != 0) {
            disp = 'none';
        }
        axis.push(
            text(
                dist + chartInfo.rectWidth / 2,
                yHeight + 11,
                dateLabel(new Date(ts), interval),
                'middle',
                {
                    transform: `translate(0,${translation})`,
                    fontSize: LABELFONTSIZE,
                    display: disp,
                }
            )
        );
        ts = incrementDate(ts, interval);
        dist += unit;
    }
    return axis;
};

// // MAY BE USED FOR LINECHART... ?
// const axisX = (width: number, data: PlotPoint[], interval: Interval) => {
//     const color = AXIS_COLOR;
//     const yHeight = graphsize.height;
//     const xInterval = getIntervalTime(interval);
//     const nbUnit = Math.ceil(xMillisecondRange(data, interval) / xInterval);
//     const unit =
//         (width - innerPadding.left - innerPadding.right) / (nbUnit - 1);

//     const axis = [
//         line(0, yHeight, width, yHeight, { stroke: color }),
//         line(width - 5, yHeight + 3, width, yHeight, { stroke: color }),
//         line(width - 5, yHeight - 3, width, yHeight, { stroke: color }),
//         text(width, yHeight + 10, `${xAxisLabel(interval)}`, 'start', {
//             // transform: `scale(1,-1), translate(0,20)`,
//             fontSize: LABELFONTSIZE,
//         }),
//     ];
//     if (interval === 'Hour') {
//         const labelDate = new Date(getFirstDate(data));
//         axis.push(
//             text(
//                 width,
//                 yHeight + 17,
//                 `${fmtDateNb(labelDate.getDate())}/${fmtDateNb(
//                     labelDate.getMonth() + 1
//                 )}/${labelDate.getFullYear()}`,
//                 'middle',
//                 {
//                     // transform: `scale(1,-1), translate(0,20)`,
//                     fontSize: LABELFONTSIZE,
//                 }
//             )
//         );
//     }

//     let ts = beginOfInterval(getFirstDate(data), interval);

//     let dist = innerPadding.left;
//     for (let i = 0; i < nbUnit; i++) {
//         // const dist = getXInPixel(ts, unitMilli, minDateMilli);

//         axis.push(line(dist, yHeight, dist, yHeight + 2, { stroke: color }));

//         // if to many labels, just display one of the two and place one of the four lower.
//         let disp = 'ok';
//         let translation = 0;
//         if (nbUnit > 25 && i % 4 != 0) {
//             translation = 5;
//         }
//         if (nbUnit > 15 && i % 2 != 0) {
//             disp = 'none';
//         }
//         axis.push(
//             text(
//                 dist,
//                 yHeight + 11,
//                 dateLabel(new Date(ts), interval),
//                 'middle',
//                 {
//                     transform: `translate(0,${translation})`,
//                     fontSize: LABELFONTSIZE,
//                     display: disp,
//                 }
//             )
//         );
//         ts = incrementDate(ts, interval);
//         dist += unit;
//     }
//     return axis;
// };

/**
 * Make an y axis
 * @param dataElem data linked to this axis
 * @param axisIndex index of the (visible) axis
 * @param paramIndex index of the parameter (visible or not on the graph), to get the color and the unit
 * @param dataLength number of visible axis (= number of data elements with a length > 0)
 */
const axisY = (data: PlotPoint[], min: number) => {
    const color = AXIS_COLOR;

    const max = getMaxY(data);

    const [divider, formatLabel] = subdivision(max - min);
    const minFloor = Math.floor(min / divider) * divider;
    const maxCeil = Math.ceil(max / divider) * divider;
    const range = maxCeil - minFloor > 0 ? maxCeil - minFloor : 1;
    const toPixelFactor =
        (graphsize.height - innerPadding.bottom - innerPadding.top) / range;

    const yHeight = graphsize.height;
    const axis = [
        // line(0, 0, 0, yHeight, { stroke: color }),
        // line(0 - 3, 5, 0, 0, { stroke: color }),
        // line(0 + 3, 5, 0, 0, { stroke: color }),
        text(
            0,
            -5,
            ``, // axis title
            'middle',
            {
                // transform: `scale(1,-1), translate(0,-${yHeight * 2 + 4})`,
                fontSize: LABELFONTSIZE,
                fill: color,
            }
        ),
    ];

    const nbOfDashes = Math.ceil(range / divider);
    for (let i = 0; i <= nbOfDashes; i++) {
        const y = minFloor + divider * i;
        const dist = getYInPixel(y, toPixelFactor, minFloor);
        if (dist > 0 && dist <= yHeight) {
            // axis.push(line(0, dist, 0 - 2, dist, { stroke: color })); // axis-dashes
            axis.push(line(0, dist, graphsize.width, dist, { stroke: color })); // axis horizontal lines
            axis.push(
                text(0 - 5, dist + 2, formatLabel(y), 'end', {
                    // transform: `translate(0,-${dist * 2})`,
                    fontSize: LABELFONTSIZE,
                })
            );
            // dist += divider * valToPixel;
        }
    }
    return group(axis, { className: `axis` });
};

// Draw chart and axis
export const chart = (data: PlotPoint[], interval: Interval) => {
    if (data.length <= 0) {
        return none;
    }

    // const interval: Interval = getInterval(data); // to use if we want a interval computed from the data...

    const xWidth = graphsize.width;
    const xInterval: number = getIntervalTime(interval);
    const nbUnit = Math.ceil(xMillisecondRange(data, interval) / xInterval) + 1; // +1: we need the place of one more interval because of rectangle width
    const xUnit = (xWidth - innerPadding.left - innerPadding.right) / nbUnit;
    const xMin = getFirstDate(data);

    // const yMin = getMinY(data); // TODO: see if needed?
    const yMin = 0;
    const yMax = getMaxY(data);
    const [divider] = subdivision(yMax - yMin);
    const minYFloor = Math.floor(yMin / divider) * divider;
    const maxYCeil = Math.ceil(yMax / divider) * divider;
    const range = maxYCeil - minYFloor > 0 ? maxYCeil - minYFloor : 1;
    const yUnit =
        (graphsize.height - innerPadding.bottom - innerPadding.top) / range;
    const rectWidth =
        (graphsize.width - innerPadding.left - innerPadding.right) / nbUnit;

    let allLines: ReactSVGElement[] = [];

    const chartInfo = {
        timeserie: data,
        id: 155, //TODO
        color: BAR_COLOR,
        interval,
        rectWidth,
        xFactor: xUnit / xInterval,
        xMin,
        yUnit,
        yMin,
    };
    allLines = allLines.concat(rectangles(chartInfo));

    const xAxis = barchartAxisX(xWidth, chartInfo, nbUnit);
    const yAxis = axisY(data, yMin);

    const drawing = xAxis.concat(yAxis).concat(allLines);

    return some(
        DIV(
            'time-barchart',
            svg(drawing, {
                viewBox: `-${padding.left} -${padding.top} ${graphsize.width + padding.left + padding.right
                    } ${graphsize.height + padding.top + padding.bottom}`,
            })
        )
    );
};

export default chart;
