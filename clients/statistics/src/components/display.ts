import { DIV, NODISPLAY } from 'sdi/components/elements';
import { ReactNode } from 'react';
import { renderAggregation } from './widgets/aggregate';
import {
    renderBestOfOneApp,
    renderMultiAppList,
    renderWorstOfOneApp,
} from './widgets/occurences';
import {
    DirectedContainer,
    Panel,
    Dashboard,
    Widget,
    MultiWidget,
} from '../dashboard/index';
import { computeTitle } from '../queries/statistics';
import {
    renderBarChart,
    renderTimeBarChart,
    renderPieChart,
    renderMultiAppBarChart,
} from './widgets/graph';

const renderDirectedContainer = (dc: DirectedContainer) =>
    DIV(
        {
            className: `container container--${dc.direction}`,
        },
        ...dc.children.map(displayPanel())
    );

const displayPanel =
    () =>
    (panel: Panel): ReactNode => {
        // a panel is a DirectedContainer or an Widget
        switch (panel.type) {
            case 'DirectedContainer':
                return renderDirectedContainer(panel);

            case 'Widget':
                return renderWidgetWrapper(panel);
            case 'MultiWidget':
                return renderMultiWidgetWrapper(panel);
        }
    };

const renderWidget = (widget: Widget) => {
    const namespace = widget.data.ns;
    const action = widget.data.action;
    const title = computeTitle(widget);
    switch (widget.tag) {
        case 'value':
            return renderAggregation(
                widget.data.aggregation,
                namespace,
                action,
                title
            );
        case 'list':
            return renderBestOfOneApp(widget.data.nb, namespace, action, title);
        case 'reversedlist':
            return renderWorstOfOneApp(
                widget.data.nb,
                namespace,
                action,
                title
            );
        case 'graph':
            switch (widget.data.graphtype) {
                case 'barchart':
                    return renderBarChart(namespace, action, title);
                case 'time-barchart':
                    return renderTimeBarChart(namespace, action, title);
                case 'linechart':
                    return DIV('', ''); //~TODO
                case 'piechart':
                    return renderPieChart(namespace, action, title);
            }
    }
};

const renderWidgetWrapper = (widget: Widget) =>
    DIV(`unit__wrapper`, renderWidget(widget));

const renderMultiWidget = (widget: MultiWidget) => {
    const namespaces = widget.data.ns;
    const action = widget.data.action;
    const title = computeTitle(widget);
    switch (widget.tag) {
        case 'value':
            return renderAggregation(
                widget.data.aggregation,
                namespaces,
                action,
                title
            );
        case 'list':
        case 'reversedlist':
            return NODISPLAY(); // TODO
        case 'applist':
            return renderMultiAppList(
                widget.data.aggregation,
                namespaces,
                action,
                title
            ); // TODO
        case 'graph':
            switch (widget.data.graphtype) {
                case 'barchart':
                    return renderMultiAppBarChart(namespaces, action, title);
                // case 'linechart':
                //     return renderLineChart(namespaces, action);
                // case 'piechart':
                //     return renderPieChart(namespaces, action, title);
            }
            return NODISPLAY(); // TODO
    }
};

const renderMultiWidgetWrapper = (widget: MultiWidget) =>
    DIV(`unit__wrapper`, renderMultiWidget(widget));

export const render = (dashboard: Dashboard) =>
    dashboard.layout.map(page =>
        DIV('panel', ...page.boxes.map(displayPanel()))
    );

export default render;
