import { DIV, H1 } from 'sdi/components/elements';
import { getDashboards, getSelectedDashboard } from '../queries/app';
import { Dashboard } from '../dashboard';
import { makeLabelAndIcon } from 'sdi/components/button';
import tr from 'sdi/locale';
import { navigateDashboard } from '../events/route';
import renderDashboard from './dashboard';

const buttonGoTo = makeLabelAndIcon('select', 2, 'chevron-right', () =>
    tr.stat('visualize')
);

const renderDashbordInfo = (dashboard: Dashboard) =>
    DIV(
        'dashboard-info',
        `${dashboard.name} `,
        buttonGoTo(() => navigateDashboard(dashboard.id))
    );

const title = () => H1('', tr.stat('appTitle'));

export const renderHome = () =>
    getSelectedDashboard().fold(
        DIV(
            { className: 'main-app home' },
            title(),
            getDashboards().map(list => list.map(d => renderDashbordInfo(d)))
        ),
        renderDashboard
    );

export default renderHome;
