import { getApiUrl, loadAlias, loop } from 'sdi/app';
import { DIV, NodeOrOptional } from 'sdi/components/elements';
import header from 'sdi/components/header';
import footer from 'sdi/components/footer';

import { loadRoute } from './events/route';
import { getLayout } from './queries/app';

import home from './components/home';
import dashboard from './components/dashboard';
import { loadAllActivity } from './events/statistics';
import { getBeginTsOrDefault, getEndTsOrDefault } from './queries/statistics';
import { loadDashboards, loadDefaultDashboard } from './events/app';

const wrappedMain = (name: string, ...elements: NodeOrOptional[]) =>
    DIV(
        { className: 'project' },
        header('statistics'),
        DIV({ className: `main ${name}` }, ...elements),
        footer()
    );

const renderHome = () => wrappedMain('home', home());

const renderDashboard = () => wrappedMain('dashboard', dashboard());

const render = () => {
    const layout = getLayout();
    switch (layout) {
        case 'home':
            return renderHome();
        case 'dashboard':
            return renderDashboard();
    }
};

const effects = (initialRoute: string[]) => () => {
    loadAlias(getApiUrl(`alias`));
    loadDashboards()
        .then(loadDefaultDashboard)
        .then(() => loadAllActivity(getBeginTsOrDefault(), getEndTsOrDefault()))
        .then(() => loadRoute(initialRoute));
};

export const app = (initialRoute: string[]) =>
    loop('statistics', render, effects(initialRoute));
