import * as debug from 'debug';
import { index } from 'fp-ts/lib/Array';
import { TypeOf, literal } from 'io-ts';
import { Path, Router } from 'sdi/router';
import { selectDashboard, setLayout } from './app';
import * as io from 'io-ts';
import { setBeginDate, setEndDate } from './statistics';
import { scopeOption } from 'sdi/lib';
import { some } from 'fp-ts/lib/Option';
import { dateISOFormat, parseDate, tryNumber } from 'sdi/util';
import { getSelectedDashboardId } from '../queries/app';
import {
    getBeginDateOrDefault,
    getEndDate,
    getEndDateOrDefault,
} from '../queries/statistics';

const logger = debug('sdi:route');

// tslint:disable-next-line: variable-name
const RouteIO = io.union([literal('home'), literal('dashboard')]);
export type Route = TypeOf<typeof RouteIO>;

export const { home, route, navigate } = Router<Route>('statistics');

export type Layout = Route;

const dashboardRouteParser = (p: Path) =>
    scopeOption()
        .let('dashboard', index(0, p))
        .let('begin', some(index(1, p)))
        .let('end', some(index(2, p)));

home('home', () => {
    setLayout('home');
});

// const simpleRoute = (r: Route) => route(r, () => {
//     setLayout(r)
//     setAppName(r)
// });

// Declare route handlers

route(
    'dashboard',
    route => {
        route.map(({ dashboard, begin, end }) => {
            tryNumber(dashboard).map(d => selectDashboard(d));
            begin.map(b => parseDate(b).map(setBeginDate));
            end.map(e => parseDate(e).map(setEndDate));
            setLayout('dashboard');
        });
    },
    dashboardRouteParser
);

// end of route handlers

export const loadRoute = (initial: string[]) =>
    index(0, initial).map(prefix =>
        RouteIO.decode(prefix).map(c => navigate(c, initial.slice(1)))
    );

export const navigateDashboard = (dashboardId: number) => {
    navigate('dashboard', [
        dashboardId,
        dateISOFormat(getBeginDateOrDefault()),
        dateISOFormat(getEndDateOrDefault()),
    ]);
};
export const navigateBegin = (begin: Date) => {
    scopeOption()
        .let('dashboard', getSelectedDashboardId)
        .let('end', getEndDate)
        .fold(
            getSelectedDashboardId()
                .map(d => {
                    navigate('dashboard', [d, dateISOFormat(begin)]);
                })
                .getOrElse(),
            ({ dashboard, end }) => {
                navigate('dashboard', [
                    dashboard,
                    dateISOFormat(begin),
                    dateISOFormat(end),
                ]);
            }
        );
};
export const navigateEnd = (end: Date) => {
    getSelectedDashboardId().map(dashboard => {
        navigate('dashboard', [
            dashboard,
            dateISOFormat(getBeginDateOrDefault()),
            dateISOFormat(end),
        ]);
    });
};

logger('loaded');
