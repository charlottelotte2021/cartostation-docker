import { assign, dispatch, observe } from 'sdi/shape';
import { fetchAllActivity } from '../remote/index';
import { remoteError, remoteLoading, remoteSuccess } from 'sdi/source';
import { getBeginTsOrDefault, getEndTsOrDefault } from '../queries/statistics';

export const setBeginDate = (date: Date) =>
    dispatch('stat/date/begin', () => date.getTime());
export const setEndDate = (date: Date) =>
    dispatch('stat/date/end', () => date.getTime());

export const setSelectedApp = (namespace: string) =>
    dispatch('stat/app', () => namespace);

// export const loadAppActivity = (namespace: string, begin: number, end: number) => {
//     assign('data/activities/remote', remoteLoading);
//     fetchActivity('/activity', namespace, begin, end)
//         .then(result => {
//             assign('data/activities/remote', remoteSuccess(result))
//         })
//         .catch(err => assign('data/activities/remote', remoteError(err)));
// }

export const loadAllActivity = (begin: number, end: number) => {
    assign('data/activities/remote', remoteLoading);
    return fetchAllActivity('/activity', begin, end)
        .then(result => {
            assign('data/activities/remote', remoteSuccess(result));
        })
        .catch(err => assign('data/activities/remote', remoteError(err)));
};

observe('stat/date/begin', () =>
    loadAllActivity(getBeginTsOrDefault(), getEndTsOrDefault())
);
observe('stat/date/end', () =>
    loadAllActivity(getBeginTsOrDefault(), getEndTsOrDefault())
);
