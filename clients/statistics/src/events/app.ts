import * as debug from 'debug';
import { Setoid } from 'fp-ts/lib/Setoid';
import { assign } from 'sdi/shape';
import { IUser, remoteError, remoteLoading, remoteSuccess } from 'sdi/source';
import { Dashboard } from '../dashboard';
import { getDashboardFromName } from '../queries/app';
import { fetchDashboards, fetchDefaultDashboard } from '../remote';
import { Layout } from './route';

const logger = debug('sdi:events/app');

export const setLayout = (l: Layout) => assign('app/layout', l);

export const setUserData = (u: IUser) => assign('data/user', u);

export const setAppName = (n: string) => assign('app/name', n);

export const loadDashboards = () => {
    assign('data/dashboards/remote', remoteLoading);
    return fetchDashboards('/activity/profiles/')
        .then(d => assign('data/dashboards/remote', remoteSuccess(d)))
        .catch(err => assign('data/activities/remote', remoteError(err)));
};

export const SetoidDashboard: Setoid<Dashboard> = {
    equals: (x: Dashboard, y: Dashboard) => x.id === y.id,
};

export const selectDashboard = (selected: number) => {
    assign('data/dashboard/selected', selected);
};

export const clearSelectedDashboard = () =>
    assign('data/dashboard/selected', null);

export const loadDefaultDashboard = () => {
    return fetchDefaultDashboard().then(dd =>
        getDashboardFromName(dd).map(d => {
            selectDashboard(d.id);
        })
    );
};

logger('loaded');
