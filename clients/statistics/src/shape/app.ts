import { Nullable } from 'sdi/util';
import { IUser, remoteNone, RemoteResource } from 'sdi/source';
import { Layout, Route } from '../events/route';
import { ButtonComponent } from 'sdi/components/button';
import { ActivityResult } from 'sdi/source/io/activity';
import { DashboardList } from '../dashboard';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'app/layout': Layout;
        'data/user': Nullable<IUser>;
        'data/dashboards/remote': RemoteResource<DashboardList>;
        'data/dashboard/selected': Nullable<number>;
        'data/activities/remote': RemoteResource<ActivityResult>;
        'component/button': ButtonComponent;

        'navigate/next': Nullable<Route>;
        'app/name': string;
    }
}

export const defaultAppShape = () => ({
    'app/layout': 'home' as Layout,
    'data/user': null,
    'data/dashboards/remote': remoteNone,
    'data/dashboard/selected': null,
    'data/activities/remote': remoteNone,
    'component/button': {},
    'navigate/next': null,
    'app/name': '',
});
