import { Nullable } from 'sdi/util';

declare module 'sdi/shape' {
    export interface IShape {
        'stat/date/begin': Nullable<number>;
        'stat/date/end': Nullable<number>;
        'stat/app': Nullable<string>;
    }
}

export const defaultStatShape = () => ({
    'stat/date/begin': null,
    'stat/date/end': null,
    'stat/app': null,
});
