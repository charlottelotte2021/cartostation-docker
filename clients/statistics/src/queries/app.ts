import { query } from 'sdi/shape';
import { fromNullable } from 'fp-ts/lib/Option';
import { remoteToOption } from 'sdi/source';

export const getLayout = () => query('app/layout');

export const getUsername = () =>
    fromNullable(query('data/user')).map(u => u.name);

export const getAppName = () => query('app/name');

export const getDashboards = () =>
    remoteToOption(query('data/dashboards/remote'));

export const getSelectedDashboardId = () =>
    fromNullable(query('data/dashboard/selected'));

export const getSelectedDashboard = () =>
    getSelectedDashboardId().chain(id =>
        getDashboards().chain(ds => fromNullable(ds.find(d => d.id === id)))
    );

export const getDashboardFromName = (name: string) =>
    getDashboards().chain(d =>
        fromNullable(d.find(dashboard => name === dashboard.name))
    );
