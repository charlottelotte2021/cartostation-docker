import { query } from 'sdi/shape';
import { fromNullable } from 'fp-ts/lib/Option';
import tr, { fromRecord, Translated } from 'sdi/locale';
import { getPanelOption, MultiWidget, Panel, Widget } from '../dashboard';
import { appDisplayName, mapRemote, RemoteResource } from 'sdi/source';
import { getAppManifest, getLang } from 'sdi/app';
import { PlotPoint } from '../components/charts';
import { tryNumber } from 'sdi/util';
import { isMessageKey, StatMessageKey } from '../locale/index';
import { Activity } from 'sdi/source/io/activity';

const dayMilliSec = 1000 * 60 * 60 * 24;
const defaultBeginTimestamp = Date.now() - dayMilliSec * 10;
const defaultEndTimestamp = Date.now();

const roundToDay = (ts: number) => Math.round(ts / dayMilliSec) * dayMilliSec;

export const getBeginTs = () => fromNullable(query('stat/date/begin'));
export const getEndTs = () => fromNullable(query('stat/date/end'));

export const getBeginTsOrDefault = () =>
    roundToDay(getBeginTs().getOrElse(defaultBeginTimestamp));
export const getEndTsOrDefault = () =>
    roundToDay(getEndTs().getOrElse(defaultEndTimestamp));

export const getBeginDate = () => getBeginTs().map(d => new Date(d));
export const getEndDate = () => getEndTs().map(d => new Date(d));
export const getBeginDateOrDefault = () => new Date(getBeginTsOrDefault());
export const getEndDateOrDefault = () => new Date(getEndTsOrDefault());

export const getSelectedApp = () => fromNullable(query('stat/app'));

export const getActivitiesRemote = () => {
    const begin = getBeginTsOrDefault();
    const end = getEndTsOrDefault();
    return mapRemote(query('data/activities/remote'), r =>
        r.data.filter(
            a => a.datetime >= begin && a.datetime <= end + dayMilliSec
        )
    );
};

// export const getActivities = () => {
//     const begin = getBeginTsOrDefault();
//     const end = getEndTsOrDefault();
//     return remoteToOption(query('data/activities/remote'))
//         .map(res => res.data)
//         .map(d =>
//             d.filter(
//                 a => a.datetime >= begin && a.datetime <= end + dayMilliSec
//             )
//         );
// };

export const getAppActivitiesRemote = (namespaces: string | string[]) => {
    if (typeof namespaces === 'string') {
        return mapRemote(getActivitiesRemote(), activities =>
            activities.filter(a => a.namespace === namespaces)
        );
    }
    return mapRemote(getActivitiesRemote(), a =>
        a.filter(a => namespaces.indexOf(a.namespace) > -1)
    );
};
// export const getAppActivities = (namespaces: string | string[]) => {
//     if (typeof namespaces === 'string') {
//         return getActivities().map(data =>
//             data.filter(a => a.namespace === namespaces)
//         );
//     }
//     return getActivities().map(data =>
//         data.filter(a => namespaces.indexOf(a.namespace) > -1)
//     );
// };

export const getActivitiesForActionRemote = (
    namespaces: string | string[],
    action: string
): RemoteResource<Activity[]> =>
    mapRemote(getAppActivitiesRemote(namespaces), a =>
        a.filter(act => act.action === action)
    );
// export const getActivitiesForAction = (
//     namespaces: string | string[],
//     action: string
// ) =>
//     getAppActivities(namespaces).map(a =>
//         a.filter(act => act.action === action)
//     );

export const getTimeserieForActionRemote = (
    namespace: string,
    action: string
) => {
    const occurences: { [key: string]: number } = {};
    return mapRemote(
        getActivitiesForActionRemote(namespace, action),
        activities => {
            activities.map(a => {
                const dateKey =
                    a.datetime - (a.datetime % (24 * 60 * 60 * 1000)); // round timestamp to day
                occurences[dateKey] = (occurences[dateKey] || 0) + 1;
            });
            const keys = Object.keys(occurences);
            const timeserie: PlotPoint[] = [];
            keys.map(k =>
                tryNumber(k).map(n =>
                    timeserie.push({ date: n, value: occurences[k] })
                )
            );
            return timeserie;
        }
    );
};
// export const getTimeserieForAction = (namespace: string, action: string) => {
//     const occurences: { [key: string]: number } = {};
//     getActivitiesForAction(namespace, action).map(act =>
//         act.map(a => {
//             const dateKey = a.datetime - (a.datetime % (24 * 60 * 60 * 1000)); // round timestamp to day
//             occurences[dateKey] = (occurences[dateKey] || 0) + 1;
//         })
//     );
//     const keys = Object.keys(occurences);
//     const timeserie: PlotPoint[] = [];
//     keys.map(k =>
//         tryNumber(k).map(n => timeserie.push({ date: n, value: occurences[k] }))
//     );
//     return timeserie;
// };

// export const getActionLabel = (action: ActivityActionData) => {
//     switch (action.action) {
//         case 'visit':
//             return '~ TODO';
//         case 'link':
//             return action.parameter.link;
//         case 'navigate-capakey':
//             return action.parameter.capakey;
//         case 'navigate-detail':
//             return action.parameter.system;
//         case 'navigate-map':
//             return fromRecord(action.parameter.title);
//         case 'print-map':
//             return fromRecord(action.parameter.title);
//         case 'print-report':
//             return action.parameter.address;
//         case 'navigate-query':
//             return fromRecord(action.parameter.title);
//         case 'lang-choice':
//             return action.parameter.lang;
//         case 'save-units':
//             return action.parameter.numberOfEdits;
//     }
// };

const getTitle = (panel: Panel) => {
    const lang = getLang();
    const defaulTitle = getPanelOption('title')(panel);
    const titlefrOpt = getPanelOption('titleFR')(panel);
    const titlenlOpt = getPanelOption('titleNL')(panel);

    if (lang === 'fr' && titlefrOpt.isSome()) {
        return titlefrOpt;
    } else if (lang === 'nl' && titlenlOpt.isSome()) {
        return titlenlOpt;
    }

    return defaulTitle;
};

export const getAppDisplayName = (codename: string) =>
    getAppManifest(codename)
        .chain(m => appDisplayName(m).map(name => fromRecord(name)))
        .getOrElse(codename as Translated);

const getDefaultTitle = (widget: Widget | MultiWidget) => {
    const action = widget.data.action;
    const actionLabel = isMessageKey(action)
        ? tr.stat(action as StatMessageKey)
        : action;
    return typeof widget.data.ns === 'string'
        ? `${getAppDisplayName(widget.data.ns)}: ${actionLabel}`
        : `${actionLabel}`;
};

export const computeTitle = (widget: Widget | MultiWidget) =>
    getTitle(widget)
        .map(t => t.toString())
        .getOrElse(getDefaultTitle(widget));
