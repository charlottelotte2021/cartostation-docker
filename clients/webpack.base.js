
const { resolve, basename } = require('path');
const { readdirSync, lstatSync } = require('fs');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require('webpack')
const fs = require('fs')
const { execSync } = require('child_process');



class VersionPlugin {
    constructor(name, id, output_dir) {
        this.name = name
        this.id = id
        this.output_dir = output_dir
    }

    apply(compiler) {
        compiler.hooks.afterEmit.tap('VersionPlugin', () => {
            const path = resolve(this.output_dir, `${this.name}.version`)
            const data = JSON.stringify({
                bundle: `${this.name}-${this.id}.bundle.js`,
                style: `${this.name}-${this.id}.style.css`,
            })
            fs.writeFile(path, data, (err) => {
                if (err) {
                    console.error(`
            [FAILED] VersionPlugin:
            ${err}
            `)
                }
                else {
                    console.log(`
            VersionPlugin: ${path}
            ${data}
            `)
                }
            })
            return true
        });
    }

}

const configure =
    (ROOT, extra_paths = {}) =>
        (env = {}) => {
            if (typeof ROOT !== 'string') {
                throw (new Error('ROOT argument is mandatory'));
            }
            const MAP_CLIENT_STATIC = resolve('../sdi/clients/static/clients/apps')
            const ASSETS_PUBLIC_PATH = '/static/clients/apps/'
            const platform = resolve(env.PLATFORM ? env.PLATFORM : `${__dirname}/platform`)
            const pathToCoreStyle = resolve(env.CORE_STYLE ? env.CORE_STYLE : `${__dirname}/platform/style/core`)


            const NAME = basename(ROOT);
            const ID = env.BUNDLE_VERSION ?
                env.BUNDLE_VERSION :
                execSync(`git rev-parse --short HEAD`).toString().trim();

            const BUNDLE_ENTRY_PATH = resolve(ROOT, 'src/index.ts');
            const STYLE_ENTRY_PATH = resolve(ROOT, 'style/index.js');
            const OUTPUT_DIR = MAP_CLIENT_STATIC // resolve(ROOT, 'dist');
            const SDI_ALIAS_ROOT = resolve(ROOT, '../sdi/');
            const SDI_ALIAS = readdirSync(SDI_ALIAS_ROOT).reduce((acc, dir) => {
                const res = resolve(SDI_ALIAS_ROOT, dir);
                if (lstatSync(res).isDirectory()) {
                    acc[`sdi/${dir}`] = res;
                }
                return acc;
            }, {})
            SDI_ALIAS[NAME] = resolve(ROOT);
            SDI_ALIAS['platform'] = resolve(platform);

            Object.keys(extra_paths).forEach(k => SDI_ALIAS[k] = resolve(extra_paths[k]))

            console.log(`NAME                ${NAME}`);
            console.log(`ID                  ${ID}`);
            console.log(`ROOT                ${ROOT}`);
            console.log(`BUNDLE_ENTRY_PATH   ${BUNDLE_ENTRY_PATH}`);
            console.log(`STYLE_ENTRY_PATH    ${STYLE_ENTRY_PATH}`);
            console.log(`OUTPUT_DIR          ${OUTPUT_DIR}`);
            console.log(`SDI_ALIAS           ${JSON.stringify(SDI_ALIAS, null, 20)}`);
            console.log(`PLATFORM            ${platform}`)
            console.log(`CORE_STYLE          ${pathToCoreStyle}`)




            const config = {
                context: ROOT,
                entry: {
                    [`${NAME}-${ID}.bundle`]: BUNDLE_ENTRY_PATH,
                    [`${NAME}-${ID}.style`]: STYLE_ENTRY_PATH,
                },

                output: {
                    path: OUTPUT_DIR,
                    publicPath: '/static/clients/apps/',
                    filename: '[name].js',
                    library: 'bundle',
                    // libraryExport: 'main',
                    libraryTarget: 'umd',
                },



                resolve: {
                    alias: SDI_ALIAS,
                    // proj4 module declaration is not consistent with its ditribution
                    mainFields: ["browser", "main", /* "module" */],
                    extensions: ['.ts', '.js'],
                },

                target: ['web', 'es5'],

                module: {
                    rules: [
                        {
                            enforce: 'pre',
                            test: /\.js$/,
                            exclude: resolve(ROOT, '../node_modules/'),
                            loader: 'source-map-loader',
                        },
                        {
                            enforce: 'pre',
                            test: /\.ts$/,
                            use: "source-map-loader"
                        },
                        {
                            test: /\.ts$/,
                            use: [
                                { loader: 'babel-loader' },
                                { loader: 'ts-loader' },
                            ],
                        },
                        {
                            test: /\.js$/,
                            exclude: resolve(ROOT, '../node_modules/'),
                            use: [
                                { loader: 'babel-loader' }
                            ],
                        },


                        /**
                         * Style
                         */
                        // CSS
                        {
                            test: /\.css$/,
                            use: [
                                { loader: MiniCssExtractPlugin.loader },
                                {
                                    loader: 'css-loader'
                                },
                            ]
                        },

                        // LESS
                        {
                            test: /\.less$/,
                            use: [
                                { loader: MiniCssExtractPlugin.loader },
                                {
                                    loader: 'css-loader'
                                },
                                {

                                    loader: 'less-loader',
                                    options: {
                                        lessOptions: {
                                            paths: [resolve(platform, 'style'),],
                                            math: 'parens-division',
                                        },
                                        additionalData: `@pathToCoreStyle: ${pathToCoreStyle};`,
                                    },
                                },
                            ],
                        },

                        //fonts
                        {
                            test: /\.(eot|ttf|woff|woff2)$/,
                            loader: 'file-loader',
                            options: {
                                publicPath: ASSETS_PUBLIC_PATH
                            }
                        },

                        //images
                        {
                            test: /\.(jpg|png|svg|gif)$/,
                            loader: 'file-loader',
                            options: {
                                publicPath: ASSETS_PUBLIC_PATH
                            }
                        }
                    ]
                },

                plugins: [

                    new VersionPlugin(NAME, ID, OUTPUT_DIR),

                    new MiniCssExtractPlugin({
                        filename: '[name].css',
                        chunkFilename: '[id].css',
                    }),

                    new webpack.DefinePlugin({
                        'PRODUCTION': JSON.stringify(process.env.NODE_ENV === 'production')
                    })
                ],
                devtool: 'source-map',

                watchOptions: {
                    ignored: /node_modules/
                },
            };

            return config;
        }

module.exports = configure;
