import * as io from 'io-ts';
import { v4 as uuidv4 } from 'uuid';
import { getApiUrl, getUserIdAstNumber } from 'sdi/app';
import {
    FeatureCollectionIO,
    fetchIO,
    MultiPolygonIO,
    uuidIO,
    postUnrelatedIO,
    FeatureIO,
    uploadIO,
    uuid,
    postIO,
} from 'sdi/source';
import { MessageRecordIO, nullable } from 'sdi/source/io/io';
import { fromPredicate } from 'fp-ts/lib/Option';
import { findOccupationByCode } from '../queries';
import { tryNumber } from 'sdi/util';

const checkOccupGroups = fromPredicate<string[]>(groups => {
    if (groups.length !== 4) {
        return false;
    }
    return groups.every(g => g.length === 2 && tryNumber(g).isSome());
});

const sourceOccupation = (code: string) =>
    checkOccupGroups(code.split('.')).isSome();

export const SourceOccupationCodeIO = io.refinement(
    io.string,
    sourceOccupation,
    'SourceOccupationCodeIO'
);
export type SourceOccupationCode = io.TypeOf<typeof SourceOccupationCodeIO>;

export const NULL_OCCUPATION_CODE = '00.00.00.00';

const validOccupation = (code: string): boolean =>
    findOccupationByCode(code).isSome();

export const ValidOccupationCodeIO = io.refinement(
    io.string,
    validOccupation,
    'ValidOccupationCodeIO'
);
export type ValidOccupationCode = io.TypeOf<typeof ValidOccupationCodeIO>;

export const OccupationIO = io.interface(
    {
        code: SourceOccupationCodeIO,
        name: MessageRecordIO,
        keywords: io.array(MessageRecordIO),
    },
    'OccupationIO'
);

export type Occupation = io.TypeOf<typeof OccupationIO>;

export const SitexImageIO = io.interface({
    small: nullable(io.string),
    medium: nullable(io.string),
    large: nullable(io.string),
});

export const SitexFileIO = io.interface({
    id: io.Integer,
    type: nullable(io.string),
    name: nullable(io.string),
    url: nullable(io.string),
    images: SitexImageIO,
});

export type SitexFile = io.TypeOf<typeof SitexFileIO>;

export const BuildingOccupationIO = io.interface(
    {
        occupcode: ValidOccupationCodeIO,
        level: io.Integer,
        vacant: io.boolean,
        nbhousing: nullable(io.Integer),
        description: nullable(io.string),
        owner: nullable(io.string),
        area: io.number,
        back_level: io.boolean,
    },
    'BuildingOccupationIO'
);
export const BuildingOccupationArrayIO = io.array(BuildingOccupationIO);
export type BuildingOccupation = io.TypeOf<typeof BuildingOccupationIO>;
export type BuildingOccupationArray = io.TypeOf<
    typeof BuildingOccupationArrayIO
>;

export const newBuildingOccupation = (
    level: number | null = null
): BuildingOccupation => ({
    level: level ?? 0,
    area: 0,
    description: null,
    nbhousing: null,
    occupcode: NULL_OCCUPATION_CODE,
    owner: null,
    vacant: false,
    back_level: false,
});

export const DictTermIO = io.interface(
    {
        id: io.Integer,
        name: MessageRecordIO,
    },
    'DictTermIO'
);

export type DictTerm = io.TypeOf<typeof DictTermIO>;
export type Dictionary = DictTerm[];

export const SurveyTypeIO = io.union(
    [io.literal('building'), io.literal('parcel'), io.literal('public-space')],
    'SurveyTypeIO '
);

export type SurveyType = io.TypeOf<typeof SurveyTypeIO>;

export const RecordStateIO = io.union(
    [io.literal('O'), io.literal('A'), io.literal('X')],
    'RecordStateIO'
);
export const DataStateIO = io.union(
    [io.literal('D'), io.literal('O'), io.literal('V')],
    'DataStateIO'
);
export const GeoStateIO = io.union(
    [io.literal('D'), io.literal('O'), io.literal('V')],
    'GeoStateIO'
);

export const BuildingFileIO = io.interface(
    {
        id: nullable(io.Integer),
        idbuildversion: nullable(io.Integer),
        file: SitexFileIO,
        creator: nullable(io.Integer),
        online: nullable(io.boolean),
        date: nullable(io.string),
    },
    'BuildingFileIO'
);

export const FileContentIo = io.interface({ file: io.string });
export type FileContent = io.TypeOf<typeof FileContentIo>;

export const BuildingFileArrayIO = io.array(BuildingFileIO);
export type BuildingFile = io.TypeOf<typeof BuildingFileIO>;
export type BuildingFileArray = io.TypeOf<typeof BuildingFileArrayIO>;

export type RecordState = io.TypeOf<typeof RecordStateIO>;
export type DataState = io.TypeOf<typeof DataStateIO>;
export type GeoState = io.TypeOf<typeof GeoStateIO>;

export const BuildingIO = io.interface(
    {
        idbuild: uuidIO,
        startdate: nullable(io.string),
        enddate: nullable(io.string),
        description: nullable(io.string),
        internaldesc: nullable(io.string),
        versiondesc: nullable(io.string),
        creator: nullable(io.Integer),
        nblevel: nullable(io.Integer),
        nblevelrec: nullable(io.Integer),
        statecode: nullable(io.Integer),
        typologycode: nullable(io.Integer),
        buildgroup: nullable(io.string),
        groundarea: nullable(io.number),
        nbparkcar: nullable(io.Integer),
        nbparkbike: nullable(io.Integer),
        u2block: nullable(io.Integer),
        u2municipality: nullable(io.Integer),
        u2building: nullable(io.Integer),
        datastate: nullable(DataStateIO),
        geostate: nullable(GeoStateIO),
        recordstate: nullable(RecordStateIO),
        geom: nullable(MultiPolygonIO),
        occupancys: nullable(BuildingOccupationArrayIO),
        files: nullable(BuildingFileArrayIO),
        observation_status: nullable(io.Integer),
    },
    'BuildingIO'
);

export const newBuilding = (): Building => ({
    idbuild: uuidv4(),
    startdate: null,
    enddate: null,
    description: null,
    internaldesc: null,
    versiondesc: null,
    creator: getUserIdAstNumber().getOrElse(0),
    nblevel: null,
    nblevelrec: null,
    statecode: null,
    typologycode: null,
    buildgroup: null,
    groundarea: null,
    nbparkcar: null,
    nbparkbike: null,
    u2block: null,
    u2municipality: null,
    u2building: null,
    datastate: 'D',
    geostate: null,
    recordstate: null,
    geom: null,
    occupancys: [],
    files: [],
    observation_status: null,
});

export type Building = io.TypeOf<typeof BuildingIO>;
export const BuildingArrayIO = io.array(BuildingIO);
export type BuildingArray = io.TypeOf<typeof BuildingArrayIO>;

export const BuildingPropIO = io.keyof(BuildingIO.props);
export type BuildingPropFull = io.TypeOf<typeof BuildingPropIO>;
export type BuildingProp = Exclude<BuildingPropFull, 'idbuild'>;

export const ParcelOccupationIO = io.interface(
    {
        occupcode: ValidOccupationCodeIO,
        vacant: nullable(io.boolean),
        nbparkcar: nullable(io.Integer),
        nbparkpmr: nullable(io.Integer),
        nbelecplug: nullable(io.Integer),
        nbparkbike: nullable(io.Integer),
        description: nullable(io.string),
        owner: nullable(io.string),
        area: io.number,
    },
    'ParcelOccupationIO'
);
export const ParcelOccupationArrayIO = io.array(ParcelOccupationIO);
export type ParcelOccupation = io.TypeOf<typeof ParcelOccupationIO>;
export type ParcelOccupationArray = io.TypeOf<typeof ParcelOccupationArrayIO>;

export const newParcelOccupation = (area = 0): ParcelOccupation => ({
    area,
    description: null,
    nbparkcar: null,
    nbparkpmr: null,
    nbelecplug: null,
    nbparkbike: null,
    occupcode: NULL_OCCUPATION_CODE,
    owner: null,
    vacant: false,
});

export const ParcelFileIO = io.interface(
    {
        id: nullable(io.Integer),
        idparcelversion: nullable(io.Integer),
        file: SitexFileIO,
        creator: nullable(io.Integer),
        online: nullable(io.boolean),
        date: nullable(io.string),
    },
    'ParcelFileIO'
);

export const ParcelFileArrayIO = io.array(ParcelFileIO);
export type ParcelFile = io.TypeOf<typeof ParcelFileIO>;
export type ParcelFileArray = io.TypeOf<typeof ParcelFileArrayIO>;

export const ParcelIO = io.interface(
    {
        idparcel: uuidIO,
        startdate: nullable(io.string),
        enddate: nullable(io.string),
        description: nullable(io.string),
        internaldesc: nullable(io.string),
        versiondesc: nullable(io.string),
        creator: nullable(io.Integer),
        fenced: nullable(io.boolean),
        project: nullable(io.string),
        groundarea: nullable(io.number),
        situation: nullable(io.string),
        lining: nullable(io.string),
        imperviousarea: nullable(io.number),
        u2block: nullable(io.Integer),
        u2municipality: nullable(io.Integer),
        u2parcel: nullable(io.Integer),
        datastate: nullable(DataStateIO),
        geostate: nullable(GeoStateIO),
        recordstate: nullable(RecordStateIO),
        geom: nullable(MultiPolygonIO),
        occupancys: nullable(ParcelOccupationArrayIO),
        files: nullable(ParcelFileArrayIO),
    },
    'ParcelIO'
);

export const newParcel = (): Parcel => ({
    idparcel: uuidv4(),
    startdate: null,
    enddate: null,
    description: null,
    internaldesc: null,
    versiondesc: null,
    creator: getUserIdAstNumber().getOrElse(0),
    fenced: false,
    project: null,
    groundarea: null,
    situation: null,
    lining: null,
    imperviousarea: null,
    u2block: null,
    u2municipality: null,
    u2parcel: null,
    datastate: 'D',
    geostate: null,
    recordstate: null,
    geom: null,
    occupancys: [],
    files: [],
});

export type Parcel = io.TypeOf<typeof ParcelIO>;
export const ParcelPropIO = io.keyof(ParcelIO.props);
export type ParcelPropFull = io.TypeOf<typeof ParcelPropIO>;
export type ParcelProp = Exclude<ParcelPropFull, 'idparcel'>;

export const SurveyPropIO = io.union([BuildingPropIO, ParcelPropIO]);
export type SurveyProp = io.TypeOf<typeof SurveyPropIO>;

// export const fetchBuildingsInBlock = (
//     blockId: number,
// ) => fetchIO(io.array(BuildingIO), getApiUrl(`geodata/sitex/building/in/${blockId}`))

export const UrbisBlockIO = io.interface(
    {
        gid: io.string,
        id: io.string,
        versionid: io.string,
        geom: io.string,
        begin_life: io.string,
        end_life: io.string,
    },
    'UrbisBlockIO'
);

export type UrbisBlock = io.TypeOf<typeof UrbisBlockIO>;

export const UrbisAddressIO = io.interface(
    {
        gid: io.Integer,
        adrn: io.string,
        capakey: io.string,
        building_gid: io.Integer,
        parcel_gid: io.Integer,
        mu_name: MessageRecordIO,
        pn_name: MessageRecordIO,
    },
    'UrbisAddressIO'
);

export type UrbisAddress = io.TypeOf<typeof UrbisAddressIO>;

export const BuildingGroupIO = io.interface(
    {
        id: io.string,
        name: io.string,
    },
    'BuildingGroupIO'
);

export const BuildingGroupArrayIO = io.array(BuildingGroupIO);
export type BuildingGroup = io.TypeOf<typeof BuildingGroupIO>;
export type BuildingGroupArray = io.TypeOf<typeof BuildingGroupArrayIO>;

export const BlockBuildingGroupIO = io.interface(
    {
        blockId: io.number,
        groups: nullable(BuildingGroupArrayIO),
        urbisBuildings: nullable(io.array(FeatureIO)),
    },
    'BlockBuildingGroupIO'
);

export const BlockBuildingGroupArrayIO = io.array(BlockBuildingGroupIO);
export type BlockBuildingGroup = io.TypeOf<typeof BlockBuildingGroupIO>;
export type BlockBuildingGroupArray = io.TypeOf<
    typeof BlockBuildingGroupArrayIO
>;

export const fetchAllBlock = () =>
    fetchIO(FeatureCollectionIO, getApiUrl('geodata/sitex/block'));

export const fetchBuildingInBlock = (blockId: number) =>
    fetchIO(
        FeatureCollectionIO,
        getApiUrl(`geodata/sitex/building?blocks_gid=${blockId}`)
    );

export const fetchParcelInBlock = (blockId: number) =>
    fetchIO(
        FeatureCollectionIO,
        getApiUrl(`geodata/sitex/parcel?blocks_gid=${blockId}`)
    );

export const fetchUrbisBuildingInBlock = (blockId: number) =>
    fetchIO(
        FeatureCollectionIO,
        getApiUrl(`geodata/sitex/urbis_building?blocks_gid=${blockId}`)
    );

export const fetchUrbisAddressInBlock = (blockId: number) =>
    fetchIO(
        io.array(UrbisAddressIO),
        getApiUrl(`geodata/sitex/urbis_address?blocks_gid=${blockId}`)
    );

export const fetchUrbisParcelInBlock = (blockId: number) =>
    fetchIO(
        FeatureCollectionIO,
        getApiUrl(`geodata/sitex/urbis_parcel?blocks_gid=${blockId}`)
    );

export const fetchBuildingGroup = () =>
    fetchIO(BuildingGroupArrayIO, getApiUrl('geodata/sitex/building/group'));

export const postBuilding = (data: Building) =>
    postUnrelatedIO(FeatureIO, getApiUrl('geodata/sitex/building'), data);

export const postParcel = (data: Parcel) =>
    postUnrelatedIO(FeatureIO, getApiUrl('geodata/sitex/parcel'), data);

export const fetchOccupations = () =>
    fetchIO(io.array(OccupationIO), getApiUrl('geodata/sitex/occupancy'));

export const fetchTypology = () =>
    fetchIO(io.array(DictTermIO), getApiUrl('geodata/sitex/topology'));

export const fetchStatecode = () =>
    fetchIO(io.array(DictTermIO), getApiUrl('geodata/sitex/statecode'));

export const fetchObservationStatus = () =>
    fetchIO(io.array(DictTermIO), getApiUrl('geodata/sitex/observationcode'));

export const fetchFileContent = (filelocation: number) =>
    fetchIO(FileContentIo, getApiUrl(`geodata/sitex/file/${filelocation}`));

export const uploadBuildingFile = (file: File) =>
    uploadIO(SitexFileIO, getApiUrl('geodata/sitex/file'), 'file', file);

export const uploadParcelFile = (file: File) =>
    uploadIO(SitexFileIO, getApiUrl('geodata/sitex/file'), 'file', file);

export const fetchBuildingHistory = (id: uuid) =>
    fetchIO(
        io.array(BuildingIO),
        getApiUrl(`geodata/sitex/building/history/${id}`)
    );
export const fetchParcelHistory = (id: uuid) =>
    fetchIO(
        io.array(ParcelIO),
        getApiUrl(`geodata/sitex/parcel/history/${id}`)
    );

export const postBuildingGroup = (name: string) =>
    postIO(BuildingGroupIO, getApiUrl('geodata/sitex/building/group'), {
        name,
    });

export const fetchBuildingGroupSuggestion = (blockId: number) =>
    fetchIO(
        BuildingGroupArrayIO,
        getApiUrl(`geodata/sitex/building/group/${blockId}/suggestion`)
    );
