import { fromNullable } from 'fp-ts/lib/Option';
import { query, queryK } from 'sdi/shape';
import { remoteToOption } from 'sdi/source';
import {
    buildingFormDataIsTouched,
    buildingFormDataIsUntouched,
    parcelFormDataIsTouched,
    parcelFormDataIsUntouched,
} from './field';

export const getLayout = () => query('app/layout');

export const getSurveyType = () => fromNullable(query('form/survey-type'));

export const getSurveyProp = () => fromNullable(query('form/prop'));

export const getFormId = () => fromNullable(query('form/id'));

export const findUrbisBuilding = (urbiId: number) =>
    fromNullable(query('data/urbis-buildings').find(({ id }) => id === urbiId));

export const findUrbisParcel = (urbiId: number) =>
    fromNullable(query('data/urbis-parcels').find(({ id }) => id === urbiId));

export const getBuildings = queryK('form/buildings');

export const getTouchedBuildings = () =>
    getBuildings().filter(buildingFormDataIsTouched);

export const getUntouchedBuildings = () =>
    getBuildings().filter(buildingFormDataIsUntouched);

export const getTouchedParcels = () =>
    query('form/parcels').filter(parcelFormDataIsTouched);

export const getUntouchedParcels = () =>
    query('form/parcels').filter(parcelFormDataIsUntouched);

export const getFullScreen = queryK('app/fullscreen');

export const getStatus = queryK('app/status');

export const getOnlineQuality = () => {
    const score = query('app/online');
    if (score < 25) {
        return 'critical';
    } else if (score < 50) {
        return 'bad';
    } else if (score < 75) {
        return 'medium';
    }
    return 'good';
};

export const getBuildingHistory = () =>
    remoteToOption(query('history/building'));

export const getParcelHistory = () => remoteToOption(query('history/parcel'));
