import { measureQueryFactory } from 'sdi/map';
import { getInteraction } from './map';

export const { getMeasured } = measureQueryFactory(getInteraction);
