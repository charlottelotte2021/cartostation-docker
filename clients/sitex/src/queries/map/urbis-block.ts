import { ILayerInfo, Inspire, PolygonInterval, StyleConfig } from 'sdi/source';
import { SurveyType } from 'sitex/src/remote';
import { getSurveyType } from '../app';
// import { getSurveyType } from '..';

const name = 'urbis-block';

const groupStyle = (
    low: number,
    high: number,
    strokeColor: string
): PolygonInterval => ({
    low,
    high,
    fillColor: 'rgba(250, 50, 50, 0.1)',
    strokeColor,
    strokeWidth: 2,
    pattern: false,
    patternAngle: 0,
    label: {},
});

const style = (st: SurveyType): StyleConfig => ({
    kind: 'polygon-continuous',
    label: {
        size: 12,
        yOffset: 0,
        baseline: 'bottom',
        propName: { fr: 'gid', nl: 'gid' },
        align: 'center',
        resLimit: 36 / 6,
        color: 'blue',
    },
    propName: `${st}_occupancy_percentage`,
    intervals: [
        groupStyle(0, 0.1, 'grey'),
        groupStyle(0.1, 20, 'rgb(243, 82, 7)'),
        groupStyle(20, 50, 'rgb(216, 14, 199)'),
        groupStyle(50, 99, 'rgb(14, 102, 216)'),
        groupStyle(99, 1000000000, 'rgb(21, 216, 14)'),
    ],
    // fillColor: 'rgba(250, 50, 50, 0.1)',
    // strokeColor: 'rgba(200, 200, 200, 1)',
    // strokeWidth: 1,
    // pattern: false,
    // patternAngle: 0,
});

const metadata: Inspire = {
    id: name,
    geometryType: 'MultiPolygon',
    resourceTitle: { fr: name, nl: name },
    resourceAbstract: { fr: name, nl: name },
    uniqueResourceIdentifier: name,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: 'NOW', revision: 'NOW' },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: 'NOW',
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
};

const layer = (): ILayerInfo => ({
    id: name,
    legend: null,
    group: null,
    metadataId: name,
    featureViewOptions: { type: 'default' },
    layerInfoExtra: null,
    visible: true,
    visibleLegend: true,
    opacitySelector: false,
    minZoom: 8,
    style: style(getSurveyType().getOrElse('building')),
});

export default {
    name,
    style,
    metadata,
    layer,
};
