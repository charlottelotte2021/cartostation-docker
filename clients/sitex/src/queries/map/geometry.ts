import { iife } from 'sdi/lib';
import { queryK } from 'sdi/shape';
import {
    ILayerInfo,
    IMapBaseLayer,
    IMapInfo,
    Inspire,
    StyleConfig,
} from 'sdi/source';
import { getSurveyType } from '../app';
import urbisBuilding from './urbis-building';
import urbisParcel from './urbis-parcel';

const name = 'EdidtorMap';

const { incrFeatureId, getFeatureId } = iife(() => {
    let id = 0;

    const incrFeatureId = () => {
        id += 1;
    };
    const getFeatureId = () => {
        return id;
    };

    return { incrFeatureId, getFeatureId };
});

const getBaseLayer = (): IMapBaseLayer => ({
    codename: 'urbis_gray',
    name: {
        fr: 'Urbis Gray',
        nl: 'Urbis Gray',
    },
    srs: 'EPSG:31370',
    url: 'https://geoservices-urbis.irisnet.be/geoserver/ows',
    params: {
        VERSION: '1.1.1',
        LAYERS: {
            fr: 'urbisFRGray',
            nl: 'urbisNLGray',
        },
    },
});
const style: StyleConfig = {
    kind: 'polygon-simple',
    fillColor: 'yellow',
    strokeColor: '#d53e2a',
    strokeWidth: 1,
    pattern: true,
    patternAngle: 45,
};

const metadata: Inspire = {
    id: name,
    geometryType: 'MultiPolygon',
    resourceTitle: { fr: name, nl: name },
    resourceAbstract: { fr: name, nl: name },
    uniqueResourceIdentifier: name,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: 'NOW', revision: 'NOW' },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: 'NOW',
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
};

const layer = (): ILayerInfo => ({
    id: name,
    legend: null,
    group: null,
    metadataId: name,
    featureViewOptions: { type: 'default' },
    layerInfoExtra: null,
    visible: true,
    visibleLegend: true,
    style,
    opacitySelector: false,
});

const getMapInfo = (): IMapInfo => ({
    baseLayer: 'urbis.irisnet.be/urbis_gray',
    id: name,
    url: `/none`,
    lastModified: Date.now(),
    status: 'published',
    title: { fr: 'Editor', nl: 'Editor' },
    description: { fr: 'Editor', nl: 'Editor' },
    categories: [],
    attachments: [],
    layers: [
        getSurveyType().getOrElse('building') === 'building'
            ? urbisBuilding.layer()
            : urbisParcel.layer(),
        layer(),
    ],
});

const getView = queryK('form/geometry/view');
const getInteraction = queryK('form/geometry/interaction');

export default {
    name,
    getMapInfo,
    getView,
    getInteraction,
    getBaseLayer,
    metadata,
    layer,
    incrFeatureId,
    getFeatureId,
};
