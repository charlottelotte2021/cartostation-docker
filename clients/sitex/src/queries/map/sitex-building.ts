import { ILayerInfo, Inspire, PatternAngle, StyleConfig } from 'sdi/source';
import { getSurveyType } from '..';

const name = 'sitex-building';

const colors = {
    old: '#13BBEE',
    'new-D': '#d53e2a', // draft
    'new-O': '#FDC300', // to confirmed
    'new-V': '#457437', // validated
};

const groups = () =>
    Object.entries(colors).map(([k, v]) => ({
        label: {},
        values: [k],
        fillColor: v,
        strokeColor: v,
        strokeWidth: 1,
        pattern: true,
        patternAngle: 45 as PatternAngle,
    }));

const style: StyleConfig = {
    kind: 'polygon-discrete',
    // label: {
    //     size: 12,
    //     yOffset: position === 'top' ? -12 : 24,
    //     baseline: 'bottom',
    //     propName: labelPropName, // { fr: 'no_code', nl: 'no_code' },
    //     align: 'center',
    //     resLimit: LABEL_RES_LIMIT,
    //     color: 'hsl(238, 37.9%, 28.4%)'
    // },
    propName: 'appStatus',
    groups: groups(),
};

const metadata: Inspire = {
    id: name,
    geometryType: 'MultiPolygon',
    resourceTitle: { fr: name, nl: name },
    resourceAbstract: { fr: name, nl: name },
    uniqueResourceIdentifier: name,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: 'NOW', revision: 'NOW' },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: 'NOW',
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
};

const layer = (): ILayerInfo => ({
    id: name,
    legend: null,
    group: null,
    metadataId: name,
    featureViewOptions: { type: 'default' },
    layerInfoExtra: null,
    visible: getSurveyType()
        .map(st => st === 'building')
        .getOrElse(false),
    visibleLegend: true,
    opacitySelector: false,
    style,
});

export default {
    name,
    style,
    metadata,
    layer,
};
