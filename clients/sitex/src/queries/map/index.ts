import * as debug from 'debug';
import { Either, left, right } from 'fp-ts/lib/Either';
import { Option, some, none, fromNullable } from 'fp-ts/lib/Option';
import {
    FeatureCollection,
    foldRemote,
    IMapBaseLayer,
    IMapInfo,
} from 'sdi/source';
import { FetchData, getFeaturePath } from 'sdi/map';
import { query, queryK } from 'sdi/shape';
import urbisBlock from './urbis-block';
import urbisBuilding from './urbis-building';
import sitexBuilding from './sitex-building';
import urbisParcel from './urbis-parcel';
import sitexParcel from './sitex-parcel';
import geometry from './geometry';

export const logger = debug('sdi:queries/map');

export type BaseLayerCode = 'urbis_gray' | 'urbis_ortho';

export const getView = queryK('port/map/view');

export const getInteraction = queryK('port/map/interaction');

export const getBaseLayerCode = queryK('port/map/baselayer');

const wmsUrbisGray: IMapBaseLayer = {
    codename: 'urbis_gray',
    name: {
        fr: 'Urbis Gray',
        nl: 'Urbis Gray',
    },
    srs: 'EPSG:31370',
    url: 'https://geoservices-urbis.irisnet.be/geoserver/ows',
    params: {
        VERSION: '1.1.1',
        LAYERS: {
            fr: 'urbisFRGray',
            nl: 'urbisNLGray',
        },
    },
};

const wmsUrbisOrtho: IMapBaseLayer = {
    codename: 'urbis_ortho',
    name: {
        fr: 'Urbis Ortho',
        nl: 'Urbis Ortho',
    },
    srs: 'EPSG:31370',
    url: 'https://geoservices-urbis.irisnet.be/geoserver/ows',
    params: {
        VERSION: '1.1.1',
        LAYERS: {
            fr: 'Urbis:Ortho2020',
            nl: 'Urbis:Ortho2020',
        },
    },
};

export const getBaseLayer = () => {
    switch (getBaseLayerCode()) {
        case 'urbis_gray':
            return wmsUrbisGray;
        case 'urbis_ortho':
            return wmsUrbisOrtho;
    }
};

export const mainMapName = 'MainMap';

export const getMapInfo = (): IMapInfo => ({
    baseLayer: 'urbis.irisnet.be/urbis_gray',
    id: mainMapName,
    url: `/none`,
    lastModified: Date.now(),
    status: 'published',
    title: { fr: 'Sitex', nl: 'Sitex' },
    description: { fr: 'Sitex', nl: 'Sitex' },
    categories: [],
    attachments: [],
    layers: [
        urbisBlock.layer(),
        urbisBuilding.layer(),
        sitexBuilding.layer(),
        urbisParcel.layer(),
        sitexParcel.layer(),
        geometry.layer(),
    ],
});

export const getSelection = queryK('port/map/selection');
export const getSelectionOpt = () => getFeaturePath(getSelection());

const foldRemoteLayer = foldRemote<
    FeatureCollection,
    string,
    Either<string, Option<FeatureCollection>>
>(
    () => left('not even started'),
    () => right(none),
    err => left(err.toString()),
    fc => right(some(fc))
);

export const getLayerData =
    (name: string): FetchData =>
    () => {
        const layers = query('data/layers');
        if (name in layers) {
            return foldRemoteLayer(layers[name]);
        }
        return left('not even heard of it');
    };

export const getSelected = () => ({
    layerId: null,
    featureId: null,
});

export const getSitexBuildings = queryK('data/buildings');

export const getUrbisBuildings = queryK('data/urbis-buildings');

export const getSitexParcels = queryK('data/parcels');

export const getUrbisParcels = queryK('data/urbis-parcels');

export const getCreateGeometry = () => fromNullable(query('port/map/create'));

logger('loaded');
