import { fromOption } from 'fp-ts/lib/Either';
import { fromEither, fromNullable, none, some } from 'fp-ts/lib/Option';
import * as io from 'io-ts';
import { fromRecord } from 'sdi/locale';
import { query, queryK, assign } from 'sdi/shape';
import { uuid } from 'sdi/source';
import { nonEmpty, uniqString } from 'sdi/util';
import {
    buildingOccupationValue,
    dataStateValue,
    dictValue,
    FieldValueDescriptorRaw,
    geometryValue,
    geoStateValue,
    numberValue,
    recordStateValue,
    buildingGroupValue,
    stringValue,
    fromRawField,
    buildingFileValue,
    booleanValue,
    parcelOccupationValue,
    parcelFileValue,
    compareFieldValue,
    CodePrefix,
} from '../components/field';
import {
    Building,
    BuildingProp,
    BuildingPropIO,
    Parcel,
    ParcelProp,
    ParcelPropIO,
    SurveyProp,
    SurveyType,
    UrbisAddress,
    BuildingGroupArray,
    BuildingIO,
    ParcelIO,
} from '../remote';
import { getFormId } from './app';

export const findBuilding = (id: uuid) =>
    fromNullable(query('data/buildings').find(({ idbuild }) => id === idbuild));

export const findBuildingForm = (id: uuid) =>
    fromNullable(query('form/buildings').find(data => id === data.idbuild));

export const getBuildingValue = (prop: SurveyProp) =>
    fromEither(BuildingPropIO.decode(prop)).chain(key =>
        getFormId()
            .chain(findBuilding)
            .map(b => b[key])
    );

export const findParcel = (id: uuid) =>
    fromNullable(query('data/parcels').find(({ idparcel }) => id === idparcel));

export const findParcelForm = (id: uuid) =>
    fromNullable(query('form/parcels').find(data => id === data.idparcel));

export const getParcelValue = (prop: SurveyProp) =>
    fromEither(ParcelPropIO.decode(prop)).chain(key =>
        getFormId()
            .chain(findParcel)
            .map(b => b[key])
    );

export const getValue = (surveyType: SurveyType, prop: SurveyProp) => {
    switch (surveyType) {
        case 'building':
            return getBuildingValue(prop);
        case 'parcel':
            return getParcelValue(prop);
        case 'public-space':
        default:
            return none;
    }
};

export const getCurrentBlock = () => fromNullable(query('form/block/current'));

export const getCurrentBuildingForm = () =>
    fromNullable(query('form/building/current'));

export const getCurrentBuildingFormFieldValue = (prop: BuildingProp) =>
    getCurrentBuildingForm()
        .map(data => data[prop]?.value)
        .getOrElse([]);

export const getCurrentParcelForm = () =>
    fromNullable(query('form/parcel/current'));

export const getCurrentParcelFormFieldValue = (prop: ParcelProp) =>
    getCurrentParcelForm()
        .map(data => data[prop]?.value)
        .getOrElse([]);

const withFormData = fromOption('not-found' as const);

export const getBuildingFormFieldValue = (prop: BuildingProp) =>
    withFormData(
        getCurrentBuildingForm().map(data => fromRawField(data[prop]))
    );

export const getParcelFormFieldValue = (prop: ParcelProp) =>
    withFormData(getCurrentParcelForm().map(data => fromRawField(data[prop])));

// export const getBuildingFieldStatus = (
//     prop: BuildingProp
// ): 'edited' | 'null' => getBuildingField(prop)
//     .map(opt => opt.isSome() ? 'edited' : 'null')
//     .getOrElse('null')

export const buildingFormData = (building: Building) => ({
    idbuild: building.idbuild,
    startdate: stringValue(building.startdate),
    enddate: stringValue(building.enddate),
    description: stringValue(building.description),
    internaldesc: stringValue(building.internaldesc),
    versiondesc: stringValue(building.versiondesc),
    creator: numberValue(building.creator),
    nblevel: numberValue(building.nblevel),
    nblevelrec: numberValue(building.nblevelrec),
    statecode: dictValue('statecode', building.statecode),
    typologycode: dictValue('typo', building.typologycode),
    buildgroup: buildingGroupValue(building.buildgroup),
    groundarea: numberValue(building.groundarea),
    nbparkcar: numberValue(building.nbparkcar),
    nbparkbike: numberValue(building.nbparkbike),
    u2block: numberValue(building.u2block),
    u2municipality: numberValue(building.u2municipality),
    u2building: numberValue(building.u2building),
    datastate: dataStateValue(building.datastate),
    geostate: geoStateValue(building.geostate),
    recordstate: recordStateValue(building.recordstate),
    geom: geometryValue(building.geom),
    occupancys: buildingOccupationValue(building.occupancys),
    files: buildingFileValue(building.files),
    observation_status: dictValue('observation', building.observation_status),
});

export const buildingFormDataToBuilding = (
    data: BuildingFormData
): Building => ({
    idbuild: data.idbuild,
    startdate: data.startdate.value,
    enddate: data.enddate.value,
    description: data.description.value,
    internaldesc: data.internaldesc.value,
    versiondesc: data.versiondesc.value,
    creator: data.creator.value,
    nblevel: data.nblevel.value,
    nblevelrec: data.nblevelrec.value,
    statecode: data.statecode.value,
    typologycode: data.typologycode.value,
    buildgroup: data.buildgroup.value,
    groundarea: data.groundarea.value,
    nbparkcar: data.nbparkcar.value,
    nbparkbike: data.nbparkbike.value,
    u2block: data.u2block.value,
    u2municipality: data.u2municipality.value,
    u2building: data.u2building.value,
    datastate: data.datastate.value,
    geostate: data.geostate.value,
    recordstate: data.recordstate.value,
    geom: data.geom.value,
    occupancys: data.occupancys.value,
    files: data.files.value,
    observation_status: data.observation_status.value,
});

export type BuildingFormData = ReturnType<typeof buildingFormData>;

const buildingKeys: BuildingProp[] = [
    'startdate',
    'enddate',
    'description',
    'internaldesc',
    'versiondesc',
    'creator',
    'nblevel',
    'nblevelrec',
    'statecode',
    'typologycode',
    'buildgroup',
    'groundarea',
    'nbparkcar',
    'nbparkbike',
    'u2block',
    'u2municipality',
    'u2building',
    'datastate',
    'geostate',
    'recordstate',
    'geom',
    'files',
    'occupancys',
    'observation_status',
];

export const parcelFormData = (parcel: Parcel) => ({
    idparcel: parcel.idparcel,
    startdate: stringValue(parcel.startdate),
    enddate: stringValue(parcel.enddate),
    description: stringValue(parcel.description),
    internaldesc: stringValue(parcel.internaldesc),
    versiondesc: stringValue(parcel.versiondesc),
    creator: numberValue(parcel.creator),
    fenced: booleanValue(parcel.fenced),
    project: stringValue(parcel.project),
    groundarea: numberValue(parcel.groundarea),
    situation: stringValue(parcel.situation),
    lining: stringValue(parcel.lining),
    imperviousarea: numberValue(parcel.imperviousarea),
    u2block: numberValue(parcel.u2block),
    u2municipality: numberValue(parcel.u2municipality),
    u2parcel: numberValue(parcel.u2parcel),
    datastate: dataStateValue(parcel.datastate),
    geostate: geoStateValue(parcel.geostate),
    recordstate: recordStateValue(parcel.recordstate),
    geom: geometryValue(parcel.geom),
    occupancys: parcelOccupationValue(parcel.occupancys),
    files: parcelFileValue(parcel.files),
});

export const parcelFormDataToParcel = (data: ParcelFormData): Parcel => ({
    idparcel: data.idparcel,
    startdate: data.startdate.value,
    enddate: data.enddate.value,
    description: data.description.value,
    internaldesc: data.internaldesc.value,
    versiondesc: data.versiondesc.value,
    creator: data.creator.value,
    fenced: data.fenced.value,
    project: data.project.value,
    groundarea: data.groundarea.value,
    situation: data.situation.value,
    lining: data.lining.value,
    imperviousarea: data.imperviousarea.value,
    u2block: data.u2block.value,
    u2municipality: data.u2municipality.value,
    u2parcel: data.u2parcel.value,
    datastate: data.datastate.value,
    geostate: data.geostate.value,
    recordstate: data.recordstate.value,
    geom: data.geom.value,
    occupancys: data.occupancys.value,
    files: data.files.value,
});

export type ParcelFormData = ReturnType<typeof parcelFormData>;

const parcelKeys: ParcelProp[] = [
    'startdate',
    'enddate',
    'description',
    'internaldesc',
    'versiondesc',
    'creator',
    'fenced',
    'project',
    'groundarea',
    'situation',
    'lining',
    'imperviousarea',
    'u2block',
    'u2municipality',
    'u2parcel',
    'datastate',
    'geostate',
    'recordstate',
    'geom',
    'files',
];

export const buildingFormDataIsTouched = (data: BuildingFormData) =>
    buildingKeys.reduce((acc, k) => (acc ? acc : data[k].touched), false);

export const buildingFormDataIsUntouched = (data: BuildingFormData) =>
    !buildingFormDataIsTouched(data);

export const buildingFormTouched = (data: BuildingFormData) =>
    buildingKeys
        .filter(k => data[k].touched)
        .map<[BuildingProp, FieldValueDescriptorRaw]>(k => [k, data[k]]);

export const parcelFormDataIsTouched = (data: ParcelFormData) =>
    parcelKeys.reduce((acc, k) => (acc ? acc : data[k].touched), false);

export const parcelFormDataIsUntouched = (data: ParcelFormData) =>
    !parcelFormDataIsTouched(data);

export const parcelFormTouched = (data: ParcelFormData) =>
    parcelKeys
        .filter(k => data[k].touched)
        .map<[ParcelProp, FieldValueDescriptorRaw]>(k => [k, data[k]]);

export const getOccupationIndex = (st: SurveyType) => {
    switch (st) {
        case 'public-space':
            return -1;
        case 'building':
            return getBuildingOccupationIndex();
        case 'parcel':
            return getParcelOccupationIndex();
    }
};
export const getOccupationPrefix = (st: SurveyType): CodePrefix => {
    switch (st) {
        case 'public-space':
            return [];
        case 'building':
            return getBuildingOccupationPrefix();
        case 'parcel':
            return getParcelOccupationPrefix();
    }
};
export const getOccupationLookup = (st: SurveyType) => {
    switch (st) {
        case 'public-space':
            return none;
        case 'building':
            return getBuildingOccupationLookup();
        case 'parcel':
            return getParcelOccupationLookup();
    }
};

const getBuildingOccupationIndex = () =>
    query('form/building/occupation').index;
const getBuildingOccupationPrefix = () =>
    query('form/building/occupation').prefix;
const getBuildingOccupationLookup = () =>
    fromNullable(query('form/building/occupation').lookup);

// export const getBuildingLocalIndex = () => query('form/building/local').index;

const getParcelOccupationIndex = () => query('form/parcel/occupation').index;
const getParcelOccupationPrefix = () => query('form/parcel/occupation').prefix;
const getParcelOccupationLookup = () =>
    fromNullable(query('form/parcel/occupation').lookup);

// export const getParcelLocalIndex = () => query('form/parcel/local').index;

export const getTypologyDict = () => query('data/typology').map(t => t); // droping the readonly here

export const findTypology = (id: number) =>
    fromNullable(getTypologyDict().find(t => t.id === id));

export const getStatecodeDict = () => query('data/statecode').map(t => t); // droping the readonly here again

export const findStatecode = (id: number) =>
    fromNullable(getStatecodeDict().find(t => t.id === id));

export const getObservationDict = () => query('data/observation').map(t => t); // droping the readonly here again

export const findObservation = (id: number) =>
    fromNullable(getObservationDict().find(t => t.id === id));

export const getBuildingGroups = () => query('data/building-group').map(t => t); // droping the readonly here again

export const getBuildingGroupSuggestion = () =>
    query('data/building-groups-suggestion').map(g => g); // droping the readonly here again

export const getGroupSuggestionInCurrentBlock = () => {
    const currentBlockId = getCurrentBlock().getOrElse(0);
    return query('data/building-groups-suggestion').find(
        g => g.blockId === currentBlockId
    ); // droping the readonly here again
};

export const getGroupSuggestionByBlock = (blockId: number) =>
    query('data/building-groups-suggestion').find(g => g.blockId === blockId); // droping the readonly here again

export const getGroupSuggestionByBuilding = (buildingId: number) =>
    query('data/building-groups-suggestion').find(g =>
        g.urbisBuildings?.find(b => b.id === buildingId)
    ); // droping the readonly here again

export const setBuildingGroups = (groups: BuildingGroupArray) =>
    assign('data/building-group', groups);

export const findGroup = (id: string) =>
    fromNullable(getBuildingGroups().find(g => g.id === id));

export const filterGroup = (lookup: string) => {
    if (lookup === '') {
        return some([]);
    }
    return fromNullable(
        getBuildingGroups().filter(
            g =>
                g.name
                    .toLocaleLowerCase()
                    .indexOf(lookup.toLocaleLowerCase()) !== -1
        )
    );
};

export const getBuildingGroupLookup = () =>
    fromNullable(query('form/building/group').lookup);

type CompressesedAddressLeaf = {
    streetName: string;
    numbers: string[];
};
export type CompressesedAddressComp = {
    municipality: string;
    comps: CompressesedAddressLeaf[];
};

export type CompressesedAddress = {
    comps: CompressesedAddressComp[];
    capakeys: string[];
};

const compressAddresses = (addresses: UrbisAddress[]) =>
    addresses.reduce<CompressesedAddress>(
        (acc, { adrn, capakey, mu_name, pn_name }) => {
            const localMuName = fromRecord(mu_name);
            const localStreetName = fromRecord(pn_name);
            const addressComp = acc.comps.find(
                c => c.municipality === localMuName
            );
            if (addressComp) {
                const addressLeaf = addressComp.comps.find(
                    l => l.streetName === localStreetName
                );
                if (addressLeaf) {
                    if (addressLeaf.numbers.indexOf(adrn) === -1) {
                        addressLeaf.numbers.push(adrn);
                    }
                } else {
                    const address = {
                        streetName: localStreetName,
                        numbers: [adrn],
                    };
                    const addressCompFiltered = addressComp.comps.filter(
                        val =>
                            Object.entries(val).toString() !==
                            Object.entries(address).toString()
                    );
                    addressCompFiltered.push(address);
                    addressComp.comps = addressCompFiltered;
                }
            } else {
                const address = {
                    municipality: localMuName,
                    comps: [
                        {
                            streetName: localStreetName,
                            numbers: [adrn],
                        },
                    ],
                };
                const accCompsFiltered = acc.comps.filter(
                    val =>
                        Object.entries(val).toString() !==
                        Object.entries(address).toString()
                );
                accCompsFiltered.push(address);
                acc.comps = accCompsFiltered;
            }
            acc.capakeys = uniqString(acc.capakeys.concat(capakey));
            return acc;
        },
        { comps: [], capakeys: [] }
    );

export const findUrbisAddress = (id: number) =>
    nonEmpty(
        query('data/urbis-address').filter(
            ({ building_gid }) => id === building_gid
        )
    ).map(compressAddresses);

export const findUrbisAddressParcel = (id: number) =>
    nonEmpty(
        query('data/urbis-address').filter(
            ({ parcel_gid }) => id === parcel_gid
        )
    ).map(compressAddresses);

export const getUploadBuildingFile = queryK('form/building/upload-file');

export const getUploadParcelFile = queryK('form/parcel/upload-file');

export const compareBuildingForms = (
    a: BuildingFormData,
    b: BuildingFormData
) => buildingKeys.filter(key => !compareFieldValue(a[key], b[key]));

export const compareParcelForms = (a: ParcelFormData, b: ParcelFormData) =>
    parcelKeys.filter(key => !compareFieldValue(a[key], b[key]));

export const buildingFormIsDiverging = () =>
    getCurrentBuildingForm()
        .chain(current =>
            findBuildingForm(current.idbuild).map(old => ({ current, old }))
        )
        .map<BuildingProp[]>(({ old, current }) =>
            compareBuildingForms(old, current)
        )
        .chain(keys => (keys.length > 0 ? some(keys) : none));

export const parcelFormIsDiverging = () =>
    getCurrentParcelForm()
        .chain(current =>
            findParcelForm(current.idparcel).map(old => ({ current, old }))
        )
        .map<ParcelProp[]>(({ old, current }) =>
            compareParcelForms(old, current)
        )
        .chain(keys => (keys.length > 0 ? some(keys) : none));

type ValidationErrorKeys = Set<string>;
const validationErrorKeys = (
    errors: io.ValidationError[]
): ValidationErrorKeys =>
    errors.reduce((acc, { context }) => {
        context.map(({ key }) => {
            if (key.length > 0) {
                acc.add(key);
            }
        });
        return acc;
    }, new Set<string>());

export const buildingFormValidation = () =>
    fromOption(new Set<string>())(getCurrentBuildingForm()).chain(form =>
        BuildingIO.decode(buildingFormDataToBuilding(form)).mapLeft(
            validationErrorKeys
        )
    );
export const parcelFormValidation = () =>
    fromOption(new Set<string>())(getCurrentParcelForm()).chain(form =>
        ParcelIO.decode(parcelFormDataToParcel(form)).mapLeft(
            validationErrorKeys
        )
    );
