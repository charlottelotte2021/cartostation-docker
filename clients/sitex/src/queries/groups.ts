import { fromNullable } from 'fp-ts/lib/Option';
import { query } from 'sdi/shape';
import { getBuildings } from './app';
import { BuildingFormData, findBuildingForm, findGroup } from './field';

export const getGroupsFeatures = () => {
    // TODO OR NOT TODO?
};

export const getCurrentGroupBuildingId = () =>
    fromNullable(query('form/groups/current'));

export const getCurrentGroupBuilding = () =>
    getCurrentGroupBuildingId().chain(findBuildingForm);

export const getCurrentGroup = () =>
    getCurrentGroupBuilding().chain(buildingWithGroup);

export const buildingWithGroup = ({ buildgroup }: BuildingFormData) =>
    fromNullable(buildgroup.value).chain(findGroup);

export const getGroupNameInput = () => fromNullable(query('form/groups/name'));

export const getBuildFormsForGroup = (gid: string) =>
    getBuildings().filter(form => form.buildgroup.value === gid);
