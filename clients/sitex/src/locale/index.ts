import { MessageStore, formatMessage } from 'sdi/locale';

const messages = {
    sitexName: {
        fr: 'SitEx 2',
        nl: 'SitEx 2',
        en: 'SitEx 2',
    },
    validateObservation: {
        fr: 'Valider',
        nl: 'Bevestigen',
        en: '',
    },
    uploadFile: {
        fr: 'Uploader',
        nl: 'Uploaden',
        en: '',
    },
    files: {
        fr: 'Fichiers',
        nl: 'Bestanden',
        en: 'Files',
    },
    prevInput: {
        fr: 'Information précédente',
        nl: 'Vorige informatie',
        en: '',
    },
    nextInput: {
        en: 'Next',
        fr: 'Continuer',
        nl: 'to do',
    },
    selectInfo: {
        fr: 'Selectionner',
        nl: 'Selecteren',
        en: '',
    },

    datastateInitial: {
        en: 'Initial',
        fr: 'Initial',
        nl: 'Initial',
    },
    datastateOngoing: {
        en: 'Ongoing',
        fr: 'En cours',
        nl: 'Lopend',
    },
    datastateValidated: {
        en: 'Validated',
        fr: 'Validé',
        nl: 'Gevalideerd',
    },
    geostateDraft: {
        en: 'Draft',
        fr: 'Brouillon',
        nl: 'Klad',
    },
    geostateOngoing: {
        en: 'To be confirmed',
        fr: 'À confirmer',
        nl: 'Bevestigen',
    },
    geostateValidated: {
        en: 'Validated',
        fr: 'Validé',
        nl: 'Geldig',
    },
    datastate: {
        en: 'Data status',
        fr: 'Statut de la donnée',
        nl: 'Gegevensstatus',
    },
    groundarea: {
        en: 'Ground area (m²)',
        fr: 'Surface au sol (m²)',
        nl: 'Vloeroppervlak (m²)',
    },
    nblevel: {
        en: 'Storeys',
        fr: 'Nombre de niveaux',
        nl: 'Aantal verdiepingen',
    },
    nblevelrec: {
        en: 'Levels in retreat',
        fr: 'Niveaux en recul',
        nl: 'Dalende niveaus',
    },
    nbparkcar: {
        en: 'Car parkings',
        fr: 'Parkings voiture',
        nl: 'Parkeerplaatsen',
    },
    nbparkbike: {
        en: 'Bike parkings',
        fr: 'Parkings vélo',
        nl: 'Fietsparkeerplaatsen',
    },
    nbparkpmr: {
        en: 'PRM parkings',
        fr: 'Parkings PMR',
        nl: 'MBM-parkeren',
    },
    nbelecplug: {
        en: 'Electrical plug',
        fr: 'Prises électriques',
        nl: 'Elektrische stopcontacten',
    },
    fenced: {
        en: 'Fenced',
        fr: 'Clôturé',
        nl: 'Omheind',
    },
    project: {
        en: 'Project',
        fr: 'Projet',
        nl: 'Project',
    },
    situation: {
        en: 'Situation',
        fr: 'Situation',
        nl: 'Situatie',
    },
    lining: {
        en: 'Lining',
        fr: 'Revêtement',
        nl: 'Voering',
    },
    imperviousarea: {
        en: 'Impervious area (m²)',
        fr: 'Superficie imperméable (m²)',
        nl: 'Ondoordringbaar gebied (m²)',
    },
    statecode: {
        en: 'Building state',
        fr: 'État du batiment',
        nl: 'Staat van het gebouw',
    },
    typologycode: {
        en: 'Typology',
        fr: 'Typologie',
        nl: 'Typologie',
    },
    buildgroup: {
        en: 'Group',
        fr: 'Groupe',
        nl: 'Groep',
    },
    otherGroup: {
        en: 'New group',
        fr: 'Nouveau groupe',
        nl: 'Nieuwe groep',
    },
    groupSuggestion: {
        en: 'Suggestion',
        fr: 'Suggestion',
        nl: 'Suggestie',
    },
    description: {
        en: 'Description',
        fr: 'Description',
        nl: 'De omschrijving',
    },
    internaldesc: {
        en: 'Comment',
        fr: 'Commentaire',
        nl: 'Opmerking',
    },
    versiondesc: {
        en: 'Version description',
        fr: 'Motif de mise à jour',
        nl: 'Reden voor update',
    },

    geostate: {
        en: 'Geometry status',
        fr: 'Statut de la géométrie',
        nl: 'Statusgeometrie',
    },
    geom: {
        en: 'Geometry',
        fr: 'Géométrie',
        nl: 'Geometrie',
    },
    occupancys: {
        en: 'Occupancies',
        fr: 'Occupations',
        nl: 'Bezetting',
    },
    locals: {
        fr: 'Localisation',
        nl: 'Plaats',
        en: 'Localization',
    },
    superficiePlancher: {
        en: 'Floor area : ',
        fr: 'Superficie plancher : ',
        nl: 'Vloeroppervlak : ',
    },
    occupationTotalArea: {
        en: 'Total area occupied : ',
        fr: 'Surface totale occupée : ',
        nl: 'Totale oppervlakte bezet : ',
    },
    'clone-occup': {
        en: 'Clone level',
        fr: `Dupliquer le niveau`,
        nl: 'Dubbele vloer',
    },
    addOccupancy: {
        en: 'Add occupancy',
        fr: `Ajouter une occupation`,
        nl: 'Een bezetting toevoegen',
    },
    addLevel: {
        en: 'Add level',
        fr: `Ajouter un niveau`,
        nl: 'Een vloer toevoegen',
    },
    addPartLevel: {
        en: 'Same level occupancie',
        fr: `Occupation même niveau`,
        nl: 'Zelfde verdieping bezetting',
    },
    'clone-local': {
        en: 'Clone',
        fr: 'Ajouter semblable',
        nl: 'Voeg soortgelijke . toe',
    },
    'leave-fullscreen': {
        en: 'Leave Fullscreen',
        fr: 'Quitter le plein écran',
        nl: 'Verlaat volledig scherm',
    },
    revert: {
        en: 'Revert',
        fr: 'Revenir à la valeur précédente',
        nl: 'Keer terug naar de vorige waarde',
    },
    syncElements: {
        en: 'Sync elements',
        fr: 'Synchroniser',
        nl: 'synchroniseren',
    },
    'clear-status': {
        en: 'Clear all notifications',
        fr: 'Supprimer toutes les notifications',
        nl: 'Alle meldingen verwijderen',
    },
    'remove-status': {
        en: 'Remove notification',
        fr: 'Supprimer cette notification',
        nl: 'Deze melding verwijderen',
    },
    'status/error/save-building': {
        en: 'Failed to save building',
        fr: 'Erreur de synchronisation',
        nl: 'Synchronisatiefout',
    },
    'status/error/save-parcel': {
        en: 'Failed to save parcel',
        fr: 'Erreur de synchronisation',
        nl: 'Synchronisatiefout',
    },
    'status/error/add-group': {
        en: 'Failed to add group',
        fr: "Échec de l'ajout du groupe",
        nl: 'Kan groep niet toevoegen',
    },
    UrbisBuildings: {
        fr: 'Bâtiments Urbis',
        nl: 'Urbis Buildings',
        en: 'Urbis-gebouwen',
    },
    UrbisParcels: {
        fr: 'Parcelle Urbis',
        nl: 'Urbis perceel',
        en: 'Urbis Parcels',
    },
    SyncedElements: {
        fr: 'Synchronisé',
        nl: 'Gesynchroniseerd',
        en: '',
    },
    'not-yet-syncedElements-d': {
        fr: 'À synchroniser - initial',
        nl: 'Te synchroniseren - initiele',
        en: '',
    },
    'not-yet-syncedElements-o': {
        fr: 'À synchroniser - en-cours',
        nl: 'Te synchroniseren - in uitvoering',
        en: '',
    },
    'not-yet-syncedElements-v': {
        fr: 'À synchroniser - validé',
        nl: 'Te synchroniseren - gevalideerd',
        en: '',
    },
    noBlockSelected: {
        fr: `
Bienvenue sur l'application d'encodage de la Sitex 2.

Aucun îlot sélectionné`,
        nl: `
Welkom bij de Sitex 2-coderingstoepassing.

Geen eiland geselecteerd`,
        en: '',
    },
    SelectionSummary: {
        fr: 'Résumé pour la sélection',
        nl: 'Samenvatting voor selectie',
        en: '',
    },
    NumberOfLoadedSitexBuildings: {
        fr: 'Nombre de bâtiment SitEx chargés :',
        nl: 'Aantal geladen SitEx-gebouwen:',
        en: '',
    },
    NumberOfLoadedUrbisBuildings: {
        fr: 'Nombre de bâtiment Urbis chargés :',
        nl: 'Aantal Urbis-gebouwen geladen:',
        en: '',
    },
    NumberOfLoadedSitexParcels: {
        fr: 'Nombre de parcelle SitEx chargés :',
        nl: 'Aantal geladen SitEx-plots:',
        en: '',
    },
    NumberOfLoadedUrbisParcels: {
        fr: 'Nombre de parcelle Urbis chargés :',
        nl: 'Aantal Urbis-percelen geladen:',
        en: '',
    },
    statusOnlineGoodHeader: {
        fr: 'En ligne - bonnes conditions',
        nl: 'Online - goede voorwaarden',
        en: '',
    },
    statusOnlineMediumHeader: {
        fr: 'En ligne - conditions moyennes',
        nl: 'Online - gemiddelde voorwaarden',
        en: '',
    },
    statusOnlineBadHeader: {
        fr: 'En ligne - mauvaises conditions',
        nl: 'Online - slechte voorwaarden',
        en: '',
    },
    statusOfflineHeader: {
        fr: 'Hors ligne',
        nl: 'offline',
        en: '',
    },
    // statusOnlineGoodSync: {
    //     fr: 'Online (bonnes conditions)',
    //     nl: '',
    //     en: '',
    // },
    // statusOnlineMediumSync: {
    //     fr: 'Vous êtes en ligne (Conditions moyennes)',
    //     nl: '',
    //     en: '',
    // },
    statusOnlineBadSync: {
        fr: 'La qualité de la connection ne permet pas de synchroniser les données.',
        nl: 'Door de kwaliteit van de verbinding kunnen gegevens niet worden gesynchroniseerd.',
        en: '',
    },
    statusOfflineSync: {
        fr: 'Vous êtes hors ligne, vous ne pouvez pas synchroniser les données.',
        nl: 'U bent offline, u kunt de gegevens niet synchroniseren.',
        en: '',
    },
    ToBeSynchronized: {
        fr: 'À synchroniser ',
        nl: 'Synchroniseren ',
        en: '',
    },
    buildings: {
        fr: 'Bâtiments : ',
        nl: 'Gebouwen :',
        en: '',
    },
    parcels: {
        fr: 'Parcelles :',
        nl: 'Percelen :',
        en: '',
    },
    publicSpaces: {
        fr: 'Espaces publics : ',
        nl: 'Openbare ruimtes :',
        en: '',
    },
    source: {
        fr: 'Source',
        nl: 'Bron',
        en: '',
    },
    sitexID: {
        fr: 'Identifiant Sitex',
        nl: 'Sitex ID',
        en: 'Sitex ID',
    },

    availableInformations: {
        fr: 'Informations disponibles',
        nl: 'Beschikbare informatie',
        en: '',
    },
    insertValue: {
        fr: 'Entrer une valeur',
        nl: 'Voer een waarde in',
        en: '',
    },
    FieldCouldNotBeFoundForThisBuilding: {
        fr: 'Champs introuvable pour ce bâtiment',
        nl: 'Velden niet gevonden voor dit gebouw',
        en: '',
    },
    FieldCouldNotBeFoundForThisParcel: {
        fr: 'Champs introuvable pour ce parcelle',
        nl: 'Velden niet gevonden voor dit pakket',
        en: '',
    },
    pleaseSelectField: {
        fr: 'Veuillez sélectionner un champs',
        nl: 'Selecteer een veld',
        en: '',
    },
    loading: {
        fr: 'Chargement',
        nl: 'Bezig met laden',
        en: '',
    },
    previewBuildingSheetTitle: {
        fr: 'Bâtiment > Visualisation',
        nl: 'Gebouw > Visualisatie',
        en: '',
    },
    previewParcelSheetTitle: {
        fr: 'Parcelle > Visualisation',
        nl: 'Perceel > Visualisatie',
        en: '',
    },
    previewPublicSpaceSheetTitle: {
        fr: 'Espace public > Visualisation',
        nl: 'Publieke ruimte > Visualisatie',
        en: '',
    },
    buildingSheetTitle: {
        fr: 'Bâtiment > Édition des données',
        nl: 'Gebouw > Gegevens bewerken',
        en: '',
    },
    parcelSheetTitle: {
        fr: 'Parcelle > Édition des données',
        nl: 'Perceel > Gegevens bewerken',
        en: '',
    },
    publicSpaceSheetTitle: {
        fr: 'Espace public > Édition des données',
        nl: 'Publieke ruimte > Gegevens bewerken',
        en: '',
    },
    historyBuildingTitle: {
        fr: 'Bâtiment > Historique',
        nl: 'Gebouw > Geschiedenis',
        en: '',
    },
    historyParcelTitle: {
        fr: 'Parcelle > Historique',
        nl: 'Perceel > Geschiedenis',
        en: '',
    },
    historyPublicSpaceTitle: {
        fr: 'Espace public > Historique',
        nl: 'Publieke ruimte > Geschiedenis',
        en: '',
    },
    occupationCode: {
        fr: 'Code occupation',
        nl: 'Bezettingscode',
        en: '',
    },
    occupationLevel: {
        fr: 'Niveau',
        nl: 'Peil',
        en: '',
    },
    occupationArea: {
        fr: 'Superficie (m²)',
        nl: 'Oppervlakte (m²)',
        en: '',
    },
    occupationHousingAbbr: {
        fr: 'Nbr de logements',
        nl: 'Aaantal woningen',
        en: '',
    },
    occupationHousing: {
        fr: 'Nombre de logements',
        nl: 'Aaantal woningen',
        en: '',
    },
    occupationVacant: {
        fr: 'Espace innocupé',
        nl: 'Onbezette ruimte',
        en: '',
    },
    backLevel: {
        fr: 'Niveau en recul',
        nl: 'Dalende niveaus',
        en: '',
    },
    occupationDescription: {
        fr: 'Description',
        nl: 'De omschrijving',
        en: 'Description',
    },
    occupancyInfos: {
        fr: `Occupation sélectionnée`,
        nl: 'Geselecteerde bezetting',
        en: '',
    },
    levelInfos: {
        fr: `Niveau`,
        nl: 'Verdieping',
        en: '',
    },
    syncTitle: {
        fr: 'Synchronisation',
        nl: 'Synchronisatie',
        en: '',
    },
    syncSummary: {
        fr: 'Résumé',
        nl: 'Overzicht',
        en: '',
    },
    owner: {
        fr: 'Propriétaire',
        nl: 'Eigenaar',
        en: '',
    },
    loaded: {
        fr: 'Chargé : ',
        nl: 'Aanval : ',
        en: '',
    },
    buildingsFromUrbis: {
        fr: 'bâtiments depuis Urbis.',
        nl: 'gebouwen van Urbis.',
        en: '',
    },
    parcelsFromUrbis: {
        fr: 'parcelle depuis Urbis.',
        nl: 'perceel van Urbis.',
        en: '',
    },
    localAddress: {
        fr: 'Adresse',
        nl: 'Adres',
        en: 'Address',
    },
    localCadastralplot: {
        fr: 'Capakey',
        nl: 'Capakey',
        en: 'Capakey',
    },
    addressInfos: {
        fr: `Localisation`,
        nl: 'Plaats',
        en: 'Localization',
    },
    history: {
        fr: `Historique`,
        nl: 'historisch',
        en: 'History',
    },
    home: {
        fr: `Accueil`,
        nl: 'Ontvangst',
        en: 'Home',
    },
    yes: {
        fr: `Oui`,
        nl: 'Ja',
        en: 'Yes',
    },
    no: {
        fr: `Non`,
        nl: 'Neen',
        en: 'No',
    },
    'mode-building': {
        en: 'building',
        fr: `Bâtiment`,
        nl: 'Gebouw',
    },
    'mode-parcel': {
        en: 'plot',
        fr: `Parcelle`,
        nl: 'Perceel',
    },
    'mode-public-space': {
        en: 'public space',
        fr: `Espace public`,
        nl: 'Openbare ruimte',
    },
    lookup: {
        en: 'Search',
        fr: `Recherche`,
        nl: 'Zoeken',
    },
    versionsTitle: {
        en: 'Previous versions',
        fr: `Versions précédentes`,
        nl: 'Vorige versies',
    },
    navigatePreview: {
        en: 'Preview',
        fr: `Voir la fiche`,
        nl: 'Zie het bestand',
    },
    sitexCredit: {
        en: `Design and conception :   [VT](https://www.voxteneo.com) & [AC](https://atelier-cartographique.be) - Background map & data © UrbIS - [cartostation](https://cartostation.com) © [AC](https://atelier-cartographique.be)`,
        fr: `Conception et réalisation :    [VT](https://www.voxteneo.com) & [AC](https://atelier-cartographique.be) - Fond de plan & données © UrbIS - [cartostation](https://cartostation.com) © [AC](https://atelier-cartographique.be)`,
        nl: `Ontwerp en productie : [VT](https://www.voxteneo.com) & [AC](https://atelier-cartographique.be) - Achtergrond & data © UrbIS - [cartostation](https://cartostation.com) © [AC](https://atelier-cartographique.be)`,
    },
    btnDeleteFile: {
        en: 'Delete',
        fr: `Supprimer`,
        nl: 'Verwijder',
    },
    btnDeleteOccupation: {
        en: 'Delete',
        fr: `Supprimer`,
        nl: 'Verwijder',
    },
    btnAddGeometryBuilding: {
        en: 'Draw a new building',
        fr: `Tracer un nouveau bâtiment`,
        nl: 'Nieuw gebouw tekenen',
    },
    btnAddGeometryParcel: {
        en: 'Draw a new parcel',
        fr: `Tracer une nouvellle parcelle`,
        nl: 'Nieuw perceel tekenen',
    },
    btnAddGeometryPublicSpace: {
        en: 'Draw a new public space',
        fr: `Tracer un nouvel espace public`,
        nl: 'Nieuw openbare ruimte tekenen',
    },
    buttonConfirmCreateBuilding: {
        en: 'Confirm new building',
        fr: `Confirmer le nouveau bâtiment`,
        nl: 'Bevestiging van het nieuwe gebouw',
    },
    buttonConfirmCreateParcel: {
        en: 'Confirm new parcel',
        fr: `Confirmer la nouvelle parcelle`,
        nl: 'Bevestig het nieuwe perceel',
    },
    buttonConfirmCreatePublicSpace: {
        en: 'Confirm new public space',
        fr: `Confirmer le nouvel espace public`,
        nl: 'Bevestiging van de nieuwe openbare ruimte',
    },
    buttonGroupAddOther: {
        en: 'Create group',
        fr: 'Créer le group',
        nl: 'to do',
    },

    liningPervious: {
        en: 'Pervious',
        fr: 'Dure perméable',
        nl: 'Waterdoorlatend',
    },

    liningImpervious: {
        en: 'Impervious',
        fr: 'Dure imperméable',
        nl: 'Ondoorlatend',
    },

    liningLowVegetation: {
        en: 'Low vegetation',
        fr: 'Végétation basse',
        nl: 'Lage vegetatie',
    },

    liningHighVegetation: {
        en: 'High vegetation',
        fr: 'Végétation haute',
        nl: 'Hoge vegetatie',
    },

    liningBarePlot: {
        en: 'Bare plot',
        fr: 'Parcelle nue',
        nl: 'Kale perceel',
    },

    situationFront: {
        en: 'Front',
        fr: 'Front de rue',
        nl: 'Straatgevel',
    },

    situationInsideBlock: {
        en: 'Inside block',
        fr: 'Intérieur d’Îlot',
        nl: 'Op het eiland',
    },

    'measure/start/line': {
        en: 'Measure distance',
        fr: 'Mesurer une distance',
        nl: 'Een afstand meten',
    },

    'measure/start/polygon': {
        en: 'Measure area',
        fr: 'Mesurer une aire',
        nl: 'Een oppervlakte meten',
    },

    'measure/stop': {
        en: 'Stop',
        fr: 'Arrêter',
        nl: 'Stop',
    },

    'navigate/goups': {
        en: 'Groups Manager',
        fr: 'Gestion des groupes',
        nl: 'Groepsmanagement',
    },

    observation_status: {
        en: 'Observation Status',
        fr: 'Statut de l’observation',
        nl: 'Observatiestatus',
    },

    'groups/clear-selection': {
        en: 'Clear selection',
        fr: 'Réinitialiser la sélection',
        nl: 'Selectie wissen',
    },

    'groups/remove': {
        en: 'Remove from group',
        fr: 'Retirer du groupe',
        nl: 'Uit de groep verwijderen',
    },

    'groups/title': {
        en: 'Groups',
        fr: 'Groupes',
        nl: 'Groepen',
    },
    'groups/create': {
        en: 'create group',
        fr: 'Créer un groupe pour le bâtiment sélectionné',
        nl: 'Een groep maken voor het geselecteerde gebouw',
    },
    'groups/selectedFeature': {
        en: 'Selected feature',
        fr: 'Bâtiment sélectionné',
        nl: 'Geselecteerde gebouw',
    },

    'groups/selectedGroup': {
        en: 'Selected group',
        fr: 'Groupe sélectionné',
        nl: 'Geselcteerde groep',
    },
    'groups/withGroup': {
        en: 'With group',
        fr: 'Bâtiment rattaché à un groupe',
        nl: 'Gebouw toegevoegd aan een groep',
    },
    'groups/inBuildingView': {
        en: 'Groups are managed on their own screen once building have been created.',
        fr: 'Les groupes de bâtiments sont gérés de façon séparée',
        nl: 'Groepen gebouwen worden apart beheerd',
    },

    groupNamePLaceholder: {
        en: 'Group name',
        fr: 'Nom du groupe',
        nl: 'Groepnaam',
    },

    createGroupHelptextMap: {
        en: '',
        fr: 'Sélectionnez sur la carte les batiments à ajouter à ce groupe',
        nl: 'Selecteer op de kaart de gebouwen die aan deze groep moeten worden toegevoegd',
    },

    createGroupStart: {
        en: 'HELP: FIRST SELECT A BUILDING TO INITIATE ACTIONS',
        fr: `
Sélectionnez un bâtiment pour initier les actions possibles

Seuls les bâtiments "Sitex" sont disponibles dans la gestion des groupes. 

Cette étape intervient donc généralement *suite* à l'encodage de bâtiments.
`,
        nl: `Selecteer een gebouw om de mogelijke acties te starten

Alleen "Sitex"-gebouwen zijn beschikbaar in het groepsbeheer. 

Deze stap wordt daarom gewoonlijk uitgevoerd *na* de codering van gebouwen.`,
    },
};

type MDB = typeof messages;
export type SitexMessageKey = keyof MDB;

declare module 'sdi/locale' {
    export interface MessageStore {
        sitex(k: SitexMessageKey): Translated;
    }
}

MessageStore.prototype.sitex = function (k: SitexMessageKey) {
    return this.getEdited('sitex', k, () => formatMessage(messages[k]));
};
