import * as io from 'io-ts';
import { displayException } from 'sdi/app';
import { configure, defaultShape, IShape } from 'sdi/shape';
import { AppConfigIO, getMessage, source } from 'sdi/source';
import { app } from './app';
import './locale';
import { defaultAppShape, defaultMapState } from './shape';

// const excludeKeys: (keyof IShape)[] = [
//     'app/csrf',
//     'app/user',
//     'data/layers',
//     'data/occupations',
//     'data/statecode',
//     'data/typology',
//     'port/map/view',
// ];
const keys: (keyof IShape)[] = [
    'app/lang',
    'form/buildings',
    'form/block/current',
    'form/building/current',
];
const withLocaleStorage = () => {
    const loc = document.location;
    const params = new URL(loc.href).searchParams;
    const storage = params.get('storage');
    return io
        .union([io.literal('on'), io.literal('off')])
        .decode(storage)
        .fold(
            () => true,
            storage => storage === 'on'
        );
};

export const main = (SDI: unknown) =>
    AppConfigIO.decode(SDI).fold(
        errors => {
            const textErrors = errors.map(e => getMessage(e.value, e.context));
            displayException(textErrors.join('\n'));
        },
        config => {
            const initialState: IShape = {
                'app/codename': 'sitex',
                ...defaultShape(config),
                ...defaultAppShape(),
                ...defaultMapState(),
            };
            // const keys = Object.keys(initialState)
            //     .map(k => k as keyof IShape)
            //     .filter(k => excludeKeys.indexOf(k) < 0);

            const stateActions = source<IShape>(keys);
            const store = stateActions(initialState, withLocaleStorage());
            configure(store);
            const start = app(config.args)(store);
            start();
        }
    );
