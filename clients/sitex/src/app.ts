import * as debug from 'debug';
import { loop } from 'sdi/app';
import { DIV, NodeOrOptional } from 'sdi/components/elements';
import header from './components/header';
import footer from './components/footer';

import {
    loadBlockLayer,
    loadOccupations,
    loadRoute,
    loadTypology,
    loadStatecode,
    loadBuildingGroup,
    monitorOnline,
    loadObservation,
} from './events';
import { getLayout } from './queries';
import index from './components/index';
import form from './components/form';
import preview from './components/preview';
import checkout from './components/sync';
import status from './components/status';
import history from './components/history';
import groups from './components/groups';
import { loadAllServices } from 'sdi/geocoder/events';

const logger = debug('sdi:app');

const wrappedMain = (name: string, ...elements: NodeOrOptional[]) =>
    DIV(
        'sitex-inner',
        header('sitex'),
        DIV({ className: `main ${name}` }, ...elements),
        status(),
        footer()
    );

const renderSplash = () => wrappedMain('splash', DIV('', 'splash'));

const renderIndex = () => wrappedMain('index', index());

const renderForm = () => wrappedMain('form', form());

const renderPreview = () => wrappedMain('preview', preview());

const renderCheckout = () => wrappedMain('checkout', checkout());

const renderHistory = () => wrappedMain('history', history());

const renderGroups = () => wrappedMain('groups', groups());

const render = () => {
    const layout = getLayout();
    switch (layout) {
        case 'splash':
            return renderSplash();
        case 'index':
            return renderIndex();
        case 'form':
            return renderForm();
        case 'preview':
            return renderPreview();
        case 'checkout':
            return renderCheckout();
        case 'history':
            return renderHistory();
        case 'groups':
            return renderGroups();
    }
};

const effects = (initialRoute: string[]) => () => {
    loadRoute(initialRoute);
    loadOccupations().then(() => {
        loadBlockLayer();
        loadTypology();
        loadStatecode();
        loadObservation();
        loadBuildingGroup();
        loadAllServices();
        monitorOnline();
    });
};

export const app = (initialRoute: string[]) =>
    loop('sitex', render, effects(initialRoute));

logger('loaded');
