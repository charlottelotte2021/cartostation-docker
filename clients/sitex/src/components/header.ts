/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { DIV, A, HEADER, SPAN } from 'sdi/components/elements';
import langSwitch from 'sdi/components/lang-switch';
import {
    getRoot,
    getAppManifest,
    getRelativeRoot,
    getUserId,
    getAppUrl,
} from 'sdi/app';
import { tr, fromRecord } from 'sdi/locale';
import { getPathElements } from 'sdi/util';
import { appDisplayName } from 'sdi/source';
// import { renderAppSelect } from 'sdi/components/app-select';

import { tryLogout } from '../events/login';
import { enterFullScreen, exitFullScreen, navigateIndex } from '../events';
import { getFullScreen, getOnlineQuality, getSurveyType } from '../queries';
// import { makeLabelAndIcon } from 'sdi/components/button';
import { nameToString } from 'sdi/components/button/names';

const logger = debug('sdi:header');

export const loginURL = () => {
    const path = getPathElements(document.location.pathname);
    const root = getPathElements(getRelativeRoot());
    const next = path
        .filter((p, i) => (i < root.length ? p !== root[i] : true))
        .join('/');
    return `${getRoot()}login/${next}`;
};

const renderNetworkStatus = () => {
    switch (getOnlineQuality()) {
        case 'good':
            return DIV(
                'network-status status-good',
                tr.sitex('statusOnlineGoodHeader')
            );
        case 'medium':
            return DIV(
                'network-status status-medium',
                tr.sitex('statusOnlineMediumHeader')
            );
        case 'bad':
            return DIV(
                'network-status status-bad',
                tr.sitex('statusOnlineBadHeader')
            );
        case 'critical':
            return DIV(
                'network-status status-critical',
                tr.sitex('statusOfflineHeader')
            );
    }
};

const fullscreenButton = () =>
    getFullScreen()
        ? DIV(
              {
                  className: 'fullscreen-app-exit',
                  onClick: exitFullScreen,
              },
              tr.sitex('leave-fullscreen')
          )
        : DIV(
              {
                  className: 'fullscreen-app-enter',
                  onClick: enterFullScreen,
              },
              tr.core('fullscreen')
          );

// const logoutButton = makeLabelAndIcon('logout', 3, 'sign-out-alt', () =>
//     tr.core('logout')
// );

const logoutButton = () =>
    DIV(
        {
            className: 'logout',
            onClick: tryLogout,
        },
        SPAN('icon', nameToString('sign-out-alt')),
        SPAN('label', tr.core('logout'))
    );

const rootButton = () =>
    getUserId().fold(
        DIV(
            { className: 'navigate login' },
            A({ href: loginURL() }, tr.core('login'))
        ),
        // () => renderAppSelect(current)
        // () => logoutButton(tryLogout)
        logoutButton
    );

const helpButton = () =>
    getAppUrl('sitex-vademecum').map(({ url, label }) =>
        DIV(
            'vademecum',
            SPAN('icon vademecum-icon', nameToString('book')),
            A(
                {
                    href: fromRecord(url),
                },
                fromRecord(label)
            )
        )
    );

const renderTitle = (appCodename: string) =>
    getAppManifest(appCodename)
        .chain(appDisplayName)
        .map<string>(fromRecord)
        .getOrElse(appCodename);

// const getUrl = (appCodename: string) =>
//     getAppManifest(appCodename).map(appUrl).getOrElse(getRootUrl('sitex'));

export const header = (appCodename: string) =>
    HEADER(
        { className: 'header' },
        DIV(
            {
                className: 'link',
                'aria-label': renderTitle('sitex'),
                onClick: () => getSurveyType().map(navigateIndex),
            },
            DIV({ className: 'brand-logo' }, DIV({ className: 'brand-name' })),
            DIV({ className: 'app-title' }, renderTitle(appCodename))
        ),
        DIV(
            { className: 'header-toolbar' },
            renderNetworkStatus(),
            fullscreenButton(),
            helpButton(),
            rootButton(),
            langSwitch(appCodename)
        )
    );

export default header;

logger('loaded');
