import * as debug from 'debug';
import { fromNullable, none, Option, some } from 'fp-ts/lib/Option';
import { nameToString } from 'sdi/components/button/names';
import { DIV, H1, IMG, SPAN, A, H2 } from 'sdi/components/elements';
import { iife } from 'sdi/lib';
import tr, { formatNumber, fromRecord } from 'sdi/locale';
import { miniMap2 as minimap } from 'sdi/map/mini';
import { MultiPolygon } from 'sdi/source';
import {
    clearBuildingProp,
    commitBuildingForm,
    navigateHistory,
    navigateIndex,
    navigatePreview,
    navigateProp,
    resetForm,
} from 'sitex/src/events';
import {
    BuildingFormData,
    findUrbisAddress,
    findTypology,
    findStatecode,
    findGroup,
    getCurrentBuildingForm,
    getSurveyProp,
    buildingFormIsDiverging,
    CompressesedAddress,
    CompressesedAddressComp,
    buildingFormValidation,
    findObservation,
} from 'sitex/src/queries';
import {
    BuildingProp,
    BuildingFile,
    DataState,
    GeoState,
} from 'sitex/src/remote';
import {
    BuildingPropGroup,
    findBuildingPropGroup,
    propToMessageKey,
} from './common';
import {
    btnValidate,
    buttonHome,
    buttonRevert,
    buttonPreview,
    buttonHistory,
} from '../../buttons';
import { FieldValueDescriptor, fromRawField } from '../../field';
import {
    renderBuildingOccupation,
    sortBuildingFilesByContentType,
} from 'sitex/src/components/preview';

const logger = debug('sdi:building/preview');

const wrapperClassName = (
    prop: BuildingProp,
    { touched, value }: FieldValueDescriptor,
    isValid: boolean
) =>
    `kv__wrapper ${touched ? 'touched' : ''} ${isValid ? '' : 'error'}  ${value
        .map(() => 'present')
        .getOrElse('missing')} ${getSurveyProp()
        .map(p => (p === prop ? 'active' : ''))
        .getOrElse('')}`;

const valueStatus = (prop: BuildingProp, { touched }: FieldValueDescriptor) =>
    DIV(
        'status__wrapper',
        touched ? buttonRevert(() => clearBuildingProp(prop)) : '',
        getSurveyProp()
            .map(p =>
                p === prop
                    ? DIV('status active icon', nameToString('chevron-right'))
                    : ''
            )
            .getOrElse('')
    );

const renderString = (s: string) => DIV('value value--string', s);

const renderNumber = (n: number) => DIV('value value--number', formatNumber(n));

const renderDataState = (s: DataState) => {
    const label = iife(() => {
        switch (s) {
            case 'D':
                return tr.sitex('datastateInitial');
            case 'O':
                return tr.sitex('datastateOngoing');
            case 'V':
                return tr.sitex('datastateValidated');
        }
    });
    return DIV('value value--data-state', label);
};

const renderMiniMap = minimap(
    'https://geoservices-urbis.irisnet.be/geoserver/ows',
    {
        LAYERS: 'urbisFRGray',
    }
);

const renderGeometry = (geom: MultiPolygon) => {
    const step = renderMiniMap(geom);
    switch (step.step) {
        case 'failed':
            return none;
        case 'loading':
            return some(DIV('minimap-loading', tr.sitex('loading')));
        case 'loaded':
            return some(DIV('value minimap-loaded', IMG({ src: step.data })));
    }
};

const renderGeoState = (s: GeoState) => {
    const label = iife(() => {
        switch (s) {
            case 'D':
                return tr.sitex('geostateDraft');
            case 'O':
                return tr.sitex('geostateOngoing');
            case 'V':
                return tr.sitex('geostateValidated');
        }
    });
    return DIV('value value--geo-state', label);
};

const renderTypology = (i: number) =>
    findTypology(i).map(({ name }) =>
        DIV('value value--string', fromRecord(name))
    );

const renderStatecode = (i: number) =>
    findStatecode(i).map(({ name }) =>
        DIV('value value--string', fromRecord(name))
    );

const renderObservation = (i: number) =>
    findObservation(i).map(({ name }) =>
        DIV('value value--string', fromRecord(name))
    );

const renderImage = ({ file }: BuildingFile) => {
    const image_url = file.images?.small ? file.images.small : file.url;
    return DIV(
        'field--image',
        A(
            {
                href: file.url || '#',
                title: file.name || '#',
                target: '_blank',
            },
            IMG({
                src: image_url || '#',
                alt: file.name || '#',
                onError: () => {
                    logger('error loading image', file);
                },
            })
        )
    );
};

const renderDocument = ({ file }: BuildingFile) => {
    const filename = fromNullable(file.name).getOrElse('');
    const filetype = fromNullable(filename.split('.').pop()).getOrElse('');
    const trimmedName =
        filename.length > 25 + filetype.length
            ? `${filename.slice(0, 20)}(...).${filetype}`
            : filename;

    return DIV(
        'field--doc',
        A(
            {
                href: file.url || '#',
                target: '_blank',
                title: file.name || '#',
            },
            SPAN('icon', nameToString('file')),
            SPAN({}, trimmedName)
        )
    );
};

const renderFileThumbnail = (file: BuildingFile) => {
    const content_type = file.file.type ? file.file.type.split('/')[0] : '';
    switch (content_type) {
        case 'image':
            return renderImage(file);
        case 'application':
            return renderDocument(file);
        default:
            return DIV({ className: 'field--image__thumbnail' });
    }
};

const renderFiles = (files: BuildingFile[]) => {
    const sorted_files = sortBuildingFilesByContentType(files);
    return DIV(
        'files-summary__wrapper',
        ...sorted_files.map(renderFileThumbnail)
    );
};

const renderBuildingGroup = (group_id: string) =>
    findGroup(group_id).map(({ name }) => DIV('value value--string', name));

const emptyValue = DIV('value value--empty');

const renderValue = (d: FieldValueDescriptor): Option<React.ReactNode> => {
    switch (d.tag) {
        case 'string':
            return d.value.map(renderString);
        case 'number':
            return d.value.map(renderNumber);
        case 'datastate':
            return d.value.map(renderDataState);
        case 'geometry':
            return d.value.map(renderGeometry);
        case 'buildgroup':
            return d.value.map(renderBuildingGroup);
        case 'geostate':
            return d.value.map(renderGeoState);
        case 'dict-typo':
            return d.value.map(renderTypology);
        case 'dict-statecode':
            return d.value.map(renderStatecode);
        case 'dict-observation':
            return d.value.map(renderObservation);
        case 'occupation-building':
            return d.value.map(renderBuildingOccupation);
        case 'files-building':
            return d.value.map(renderFiles);
        case 'recordstate':
        case 'dict-update':
        default:
            return some(DIV('value', `~TODO(${d.tag})`));
    }
};

const infoItem = (
    prop: BuildingProp,
    data: BuildingFormData,
    isValid: boolean
) => {
    const field = fromRawField(data[prop]);
    return DIV(
        {
            className: wrapperClassName(prop, field, isValid),
            onClick: () => navigateProp(prop),
        },
        DIV('key', tr.sitex(propToMessageKey(prop))),
        renderValue(field).getOrElse(emptyValue),
        valueStatus(prop, field)
    );
};

const renderGoup =
    (form: BuildingFormData, propIsValid: (prop: BuildingProp) => boolean) =>
    ({ props }: BuildingPropGroup) =>
        DIV(
            'infos__group',
            ...props.map(prop => infoItem(prop, form, propIsValid(prop)))
        );

const infosWrapper = (form: BuildingFormData) => {
    const validation = buildingFormValidation();
    const propIsValid = (prop: BuildingProp) =>
        validation.fold(
            s => !s.has(prop),
            () => true
        );
    return DIV(
        'infos building',
        findBuildingPropGroup('general').map(renderGoup(form, propIsValid)),
        findBuildingPropGroup('secondary').map(renderGoup(form, propIsValid)),
        findBuildingPropGroup('details').map(renderGoup(form, propIsValid))
    );
};

const renderCommpressedAddressComp = (addr: CompressesedAddressComp) =>
    DIV(
        'compressed-address',
        addr.comps
            .map(
                ({ streetName, numbers }) =>
                    `${streetName} ${numbers.join(' - ')}`
            )
            .join('; '),
        `, ${addr.municipality}`
    );

const renderUrbisAddress = (address: CompressesedAddress) =>
    DIV(
        `address__container  compress-${address.comps.length}`,
        H1('', ...address.comps.map(renderCommpressedAddressComp)),
        DIV(
            'capakey',
            `${tr.sitex('localCadastralplot')} : ${address.capakeys.join('; ')}`
        )
    );

const actionValidate = (keys: BuildingProp[]) =>
    buildingFormValidation().fold(
        () =>
            DIV(
                { className: 'form-actions', title: keys.join(', ') },
                btnValidate(() => undefined, 'disabled error'),
                buttonPreview(() => undefined, 'disabled'),
                buttonHistory(() => undefined, 'disabled')
            ),
        () =>
            DIV(
                { className: 'form-actions', title: keys.join(', ') },
                btnValidate(() => {
                    commitBuildingForm();
                    resetForm();
                    navigateIndex('building');
                }),
                buttonPreview(() => undefined, 'disabled'),
                buttonHistory(() => undefined, 'disabled')
            )
    );
const actionNavigate = (id: string) => () =>
    DIV(
        'form-actions',
        btnValidate(() => undefined, 'disabled'),
        buttonPreview(() => navigatePreview('building', id)),
        buttonHistory(() => navigateHistory('building', id))
    );

const formActions = (id: string) =>
    buildingFormIsDiverging().foldL(actionNavigate(id), actionValidate);

const formHeader = () =>
    DIV(
        'sidebar__header form__header',
        buttonHome(() => navigateIndex('building')),
        H2('subtitle', tr.sitex('buildingSheetTitle'))
    );

const infosHeader = (form: BuildingFormData) =>
    DIV(
        'infos__header',
        DIV(
            'address__container',

            fromNullable(form.u2building.value)
                .chain(findUrbisAddress)
                .map(renderUrbisAddress),

            DIV('sitex-id', `${tr.sitex('sitexID')} : ${form.idbuild}`)
        ),
        formActions(form.idbuild)
    );

export const renderFormBuildingInfo = () =>
    getCurrentBuildingForm().map(form =>
        DIV(
            'sidebar infos__wrapper building',
            formHeader(),
            infosHeader(form),
            infosWrapper(form)
        )
    );
