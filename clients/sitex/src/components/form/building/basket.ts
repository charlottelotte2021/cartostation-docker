// import { DIV, H2 } from "sdi/components/elements";
// import { iife } from "sdi/lib";
// import tr, { formatNumber } from "sdi/locale";
// import { clearBuildingProp, commitBuildingForm, navigateIndex, resetForm } from "sitex/src/events";
// import { BuildingFormData, buildingFormDataIsTouched, buildingFormTouched, getCurrentBuildingForm, getSurveyProp } from "sitex/src/queries";
// import { BuildingProp, DataState } from "sitex/src/remote";
// import { propToMessageKey } from ".";
// import { FieldValueDescriptor } from "../../field";
// import { btnValidate, buttonDelete } from "../../buttons";

// const renderString = (
//     s: string
// ) => DIV('value', s)

// const renderNumber = (
//     n: number
// ) => DIV('value', formatNumber(n))

// const renderDataState = (
//     s: DataState
// ) => {
//     const label = iife(() => {
//         switch (s) {
//             case 'D': return tr.sitex('draft');
//             case 'O': return tr.sitex('to-be-confirmed');
//             case 'V': return tr.sitex('validated');
//         }
//     })
//     return DIV('value', label)
// }

// const renderValue = (
//     desc: FieldValueDescriptor,
// ) => {
//     switch (desc.tag) {
//         case 'string': return desc.value.map(renderString);
//         case 'number': return desc.value.map(renderNumber);
//         case 'datastate': return desc.value.map(renderDataState);
//         case 'geostate':
//         case 'recordstate':
//         case 'dict-occup':
//         case 'dict-typo':
//         case 'dict-update':
//         default: return DIV('value', `~TODO(${desc.tag})`);
//     }
// }

// const basketItem = (
//     [prop, desc]: [BuildingProp, FieldValueDescriptor]
// ) => DIV(`basket__kv ${getSurveyProp().map(p => p === prop ? 'active' : '').getOrElse('')} `,
//     DIV('kv__wrapper',
//         DIV('key', tr.sitex(propToMessageKey(prop))),
//         DIV('value', renderValue(desc)),
//     ),
//     buttonDelete(() => clearBuildingProp(prop))
// )

// const basketContent = (
//     form: BuildingFormData,
// ) => DIV('basket__content',
//     ...buildingFormTouched(form).map(basketItem)
// )

// const basketActions = () => DIV('basket__actions',
//     btnValidate(() => {
//         commitBuildingForm()
//         resetForm()
//         navigateIndex()
//     }),
// )

// const renderMinimap = DIV('minimap__wrapper', 'building on this mini-map');

// export const renderFormBuildingChangeset = () => getCurrentBuildingForm()
//     .map(form => DIV(
//         `basket__wrapper building ${buildingFormDataIsTouched(form) ? 'touched' : ''}`,
//         H2('', '~Check and validation'),
//         renderMinimap,
//         basketContent(form),
//         basketActions()
//     ));
