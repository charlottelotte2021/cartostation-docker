import { index } from 'fp-ts/lib/Array';
import { fromNullable, Option, fromEither } from 'fp-ts/lib/Option';
import { DIV, H2, LABEL, SPAN, TEXTAREA } from 'sdi/components/elements';
import { tryNumber, withM2, tryString } from 'sdi/util';

import {
    clearBuildingOccupationLookup,
    clearBuildingOccupationPrefix,
    insertCloneBuildingOccupation,
    insertNewBuildingOccupation,
    removeBuildingOccupation,
    updateIndexBuildingOccupationEdited,
    navigateProp,
    prefixToCode,
    setBuildingFormFieldValue,
    setBuildingOccupationIndex,
    setBuildingOccupationPrefix,
    insertNewBuildingOccupationAtCurrentLevel,
} from 'sitex/src/events';
import {
    proceedBuildingOccupationToUpdate,
    findOccupationByCode,
    getOccupationIndex,
    getOccupationPrefix,
    getCurrentBuildingForm,
} from 'sitex/src/queries';
import {
    BuildingOccupation,
    BuildingProp,
    newBuildingOccupation,
    BuildingOccupationArrayIO,
} from 'sitex/src/remote';
import {
    CodePrefix,
    renderOccupationBreadcrumb,
    renderOccupationLookup,
    renderOccupationOptions,
    renderOccupationVacant,
    updateBuildingOccupation,
} from '../../field';
import {
    btnNextInput,
    buttonAddOccup,
    buttonAddOccupOnLevel,
    buttonCloneOccup,
    buttonDeleteOccupation,
} from '../../buttons';
import tr, { fromRecord } from 'sdi/locale';
import { nextProp } from './common';
import { sortOccupationByLevel } from 'sitex/src/components/preview';
import { inputNumber, options, renderCheckbox } from 'sdi/components/input';

const updateFilteredBuildingOccupation = (
    occupations: BuildingOccupation[] | null
) => {
    const occups = proceedBuildingOccupationToUpdate(occupations);
    return updateBuildingOccupation(occups);
};

type OccupUpdate = (o: BuildingOccupation) => void;

const initial = (prop: BuildingProp) => {
    const set = setBuildingFormFieldValue(prop);
    return DIV(
        'empty occupation',
        H2('', tr.sitex('occupancys')),
        DIV(
            'input',
            buttonAddOccup(() => {
                const occup = newBuildingOccupation(0);
                getCurrentBuildingForm()
                    .chain(form => fromNullable(form.groundarea.value))
                    .map(area => (occup.area = area));
                set(updateFilteredBuildingOccupation([occup]));
                setBuildingOccupationIndex(0);
                clearBuildingOccupationPrefix();
            })
        )
    );
};

const occupationCodeBreadcrumb = (
    occup: BuildingOccupation,
    update: OccupUpdate
) =>
    renderOccupationBreadcrumb(
        occup.occupcode,
        getOccupationPrefix('building'),
        (n, parts) => () => {
            const newCode = prefixToCode(parts.slice(0, n) as CodePrefix);
            if (newCode === null) {
                clearBuildingOccupationPrefix();
                clearBuildingOccupationLookup();
            } else {
                setBuildingOccupationPrefix(newCode);
                clearBuildingOccupationLookup();
                update({ ...occup, occupcode: newCode });
            }
        }
    );

const occupationOptions = (occup: BuildingOccupation, update: OccupUpdate) =>
    renderOccupationOptions('building', occup, code => {
        update({ ...occup, occupcode: code });
        setBuildingOccupationPrefix(code);
        clearBuildingOccupationLookup();
    });

const occupationLookup = (occup: BuildingOccupation, update: OccupUpdate) =>
    renderOccupationLookup('building', (code: string) => {
        update({ ...occup, occupcode: code });
        setBuildingOccupationPrefix(code);
        clearBuildingOccupationLookup();
    });

const renderOccupationCode = (occup: BuildingOccupation, update: OccupUpdate) =>
    DIV(
        'step-occupation-code__wrapper',
        DIV(
            'occup-code__wrapper',
            H2('subtitle', tr.sitex('occupationCode')),
            occupationCodeBreadcrumb(occup, update)
        ),
        occupationOptions(occup, update),
        occupationLookup(occup, update),
        renderVacant(occup, update)
    );

const renderLevel = (
    occup: BuildingOccupation,
    occupIndex: number,
    update: OccupUpdate
) =>
    LABEL(
        '',
        inputNumber(
            options(
                `occup-level-${occupIndex}`,
                () => occup.level,
                level => update({ ...occup, level })
            )
        ),
        SPAN('', tr.sitex('occupationLevel'))
    );

const renderArea = (
    occup: BuildingOccupation,
    occupIndex: number,
    update: OccupUpdate
) =>
    LABEL(
        '',
        inputNumber(
            options(
                `occup-area-${occupIndex}`,
                () => occup.area,
                area => update({ ...occup, area })
            )
        ),
        SPAN('', tr.sitex('occupationArea'))
    );

const renderNbHousing = (
    occup: BuildingOccupation,
    occupIndex: number,
    update: OccupUpdate
) =>
    LABEL(
        '',
        inputNumber(
            options<number>(
                `occup-nbhousing-${occupIndex}`,
                () => fromNullable(occup.nbhousing),
                nbhousing => update({ ...occup, nbhousing })
            )
        ),
        SPAN('', tr.sitex('occupationHousingAbbr'))
    );

const renderVacant = (occup: BuildingOccupation, update: OccupUpdate) =>
    renderOccupationVacant(occup, () =>
        update({
            ...occup,
            vacant: !fromNullable(occup.vacant).getOrElse(false),
        })
    );

const renderBackLevel = (occup: BuildingOccupation, update: OccupUpdate) =>
    LABEL(
        'input--checkbox',
        renderCheckbox(
            'back-level',
            () => tr.sitex('backLevel'),
            back_level =>
                update({
                    ...occup,
                    back_level,
                })
        )(fromNullable(occup.back_level).getOrElse(false))
    );

const renderDescription = (
    occup: BuildingOccupation,
    occupIndex: number,
    update: OccupUpdate
) =>
    LABEL(
        'occupation-description',
        TEXTAREA({
            rows: 3,
            key: `occup-description-${occupIndex}`,
            placeholder: tr.sitex('occupationDescription'),
            value: fromNullable(occup.description).toUndefined(),
            onChange: e =>
                tryString(e.currentTarget.value).map(description =>
                    update({ ...occup, description })
                ),
        })
        // SPAN('', tr.sitex('occupationDescription'))
    );

const renderLevelInfos = (
    occup: BuildingOccupation,
    update: OccupUpdate,
    occupIndex: number
) =>
    DIV(
        'level-infos',
        H2('subtitle', tr.sitex('levelInfos')),
        DIV(
            'occup-input-row',
            renderLevel(occup, occupIndex, update),
            renderArea(occup, occupIndex, update)
        ),
        DIV(
            'occup-input-row',
            renderNbHousing(occup, occupIndex, update),
            renderBackLevel(occup, update)
        ),
        renderDescription(occup, occupIndex, update)
    );

const renderlevelActions = (prop: BuildingProp, occup: BuildingOccupation) =>
    DIV(
        'level-actions',

        buttonCloneOccup(() => {
            insertCloneBuildingOccupation(prop, occup);
        }),
        buttonAddOccupOnLevel(() => {
            insertNewBuildingOccupationAtCurrentLevel(prop);
            clearBuildingOccupationPrefix();
        }),
        buttonAddOccup(() => {
            insertNewBuildingOccupation(prop);
            clearBuildingOccupationPrefix();
        })
        // nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
    );

const renderMainInput = (
    prop: BuildingProp,
    occup: BuildingOccupation,
    occupIndex: number,
    update: OccupUpdate
) =>
    DIV(
        'input__wrapper--occupation',
        DIV(
            'level-content',
            renderLevelInfos(occup, update, occupIndex),
            renderOccupationCode(occup, update)
        ),
        renderlevelActions(prop, occup),
        nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
    );

const renderHeader = (
    prop: BuildingProp,
    occup: BuildingOccupation,
    i: number,
    isSelected: string
) =>
    DIV(
        {
            className: `occup-summary ${isSelected}`,
            onClick: () => {
                setBuildingOccupationIndex(i);
                fromNullable(occup.occupcode).foldL(
                    clearBuildingOccupationPrefix,
                    setBuildingOccupationPrefix
                );
            },
        },
        DIV('level', occup.level),
        DIV(
            'code',
            fromNullable(occup.occupcode)
                .chain(findOccupationByCode)
                .map<string | null>(({ name }) => fromRecord(name))
                .getOrElse('')
        ),
        tryNumber(occup.area).map(area => DIV('area', withM2(area))),
        buttonDeleteOccupation(() => removeBuildingOccupation(prop, i))
    );

export const renderSurfaceAreaAndTotalOccupiedInfo = (prop: BuildingProp) => {
    return getCurrentBuildingForm().map(form => {
        const totalArea =
            tryNumber(form.groundarea.value).getOrElse(0) *
            tryNumber(form.nblevel.value).getOrElse(0);
        const occupation = form[prop].value;
        const totalOccupiedArea = fromEither(
            BuildingOccupationArrayIO.decode(occupation)
        )
            .map(occup =>
                occup.reduce(
                    (acc, cur) => acc + tryNumber(cur.area).getOrElse(0),
                    0
                )
            )
            .getOrElse(0);

        let totalAreaElement = null;
        if (totalArea > 0) {
            totalAreaElement = DIV(
                '',
                tr.sitex('superficiePlancher'),
                withM2(totalArea)
            );
        }

        let totalOccupiedAreaElement = null;
        if (totalOccupiedArea > 0) {
            totalOccupiedAreaElement = DIV(
                '',
                tr.sitex('occupationTotalArea'),
                withM2(totalOccupiedArea)
            );
        }

        return DIV('', totalAreaElement, totalOccupiedAreaElement);
    });
};

export const renderOccupationInput = (
    prop: BuildingProp,
    opt: Option<BuildingOccupation[]>
) => {
    const occups = sortOccupationByLevel(opt.getOrElse([]));
    if (occups.length === 0) {
        return initial(prop);
    }
    const occupIndex = getOccupationIndex('building');
    return index(occupIndex, occups).map(() => {
        const update = (o: BuildingOccupation) => {
            const set = setBuildingFormFieldValue(prop);
            const newValue = occups.map((occup, i) => {
                if (i === occupIndex) {
                    return o;
                }
                return occup;
            });
            set(updateFilteredBuildingOccupation(newValue));

            // Update occupation index being edited
            // FIXME
            setTimeout(() => {
                updateIndexBuildingOccupationEdited(prop, o);
            }, 100);
        };

        const renderedInputWidget = renderMainInput(
            prop,
            occups[occupIndex],
            occupIndex,
            update
        );

        const elements = occups
            // .filter((_o, i) => i!=occupIndex)
            .map((occup, i) => {
                const isSelected = i === occupIndex ? 'selected' : '';
                return renderHeader(prop, occup, i, isSelected);
            });

        return DIV(
            'occups',
            DIV(
                'occup-summary__wrapper',
                H2('', tr.sitex('occupancys')),
                renderSurfaceAreaAndTotalOccupiedInfo(prop),
                DIV('occup-levels', ...elements.reverse())
            ),
            renderedInputWidget
        );
    });
};

export default renderOccupationInput;
