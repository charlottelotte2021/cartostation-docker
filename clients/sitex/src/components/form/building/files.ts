import * as debug from 'debug';
import { flatten, index } from 'fp-ts/lib/Array';
import { fromNullable, Option, some } from 'fp-ts/lib/Option';
import {
    DIV,
    INPUT,
    NodeOrOptional,
    IMG,
    SPAN,
    A,
} from 'sdi/components/elements';
import { iife } from 'sdi/lib';
import tr from 'sdi/locale';
import { foldRemote, uuid } from 'sdi/source';
import {
    setBuildingFormFieldValue,
    processUploadBuildingFile,
    clearUploadBuildingFile,
    navigateProp,
} from 'sitex/src/events';
import {
    getCurrentBuildingForm,
    getUploadBuildingFile,
    getCurrentBuildingFormFieldValue,
} from 'sitex/src/queries';
import { BuildingProp, BuildingFile, SitexFile } from 'sitex/src/remote';
import {
    btnNullifyInput,
    btnUploadFile,
    btnNextInput,
    buttonDeleteFile,
} from '../../buttons';
import { updateBuildingFile } from '../../field';
import { buildingPropGroups } from './common';
import { nameToString } from 'sdi/components/button/names';
import { sortBuildingFilesByContentType } from 'sitex/src/components/preview';

const logger = debug('sdi:building/files');

// const renderFileContent = (file: BuildingFile) => {
//     logger('renderFileContent', file);
//     switch (file.filetype) {
//         case 'jpeg':
//             return DIV({className: 'building-file'}, IMG(file.fileurl))
//         default:
//             return DIV({className: 'building-file'}, SPAN('', 'Not image'));
//     };
// };

const nextProp = (prop: BuildingProp) => {
    const props = flatten(buildingPropGroups.map(({ props }) => props));
    const propIndex = props.indexOf(prop);

    return index(propIndex + 1, props);
};

const fromSitex = (file: SitexFile): BuildingFile => ({
    file,
    idbuildversion: null,
    creator: null,
    date: null,
    id: null,
    online: null,
});

const { setFile, getFile, clearFile } = iife(() => {
    type Record = { idbuild: uuid; file: File };
    let files: Record[] = [];

    const setFile = (id: uuid) => (file: File) => {
        files = files
            .filter(({ idbuild }) => idbuild !== id)
            .concat({ idbuild: id, file });
    };

    const getFile = (id: uuid) =>
        fromNullable(files.find(({ idbuild }) => idbuild === id)).map(
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            ({ file }) => file
        );

    const clearFile = (id: uuid) => {
        files = files.filter(({ idbuild }) => idbuild !== id);
    };

    return { setFile, getFile, clearFile };
});

const actionDeleteFile = (prop: BuildingProp, file: SitexFile) => {
    const new_files = (
        getCurrentBuildingFormFieldValue(prop) as BuildingFile[]
    ).filter(({ file: { id } }) => id !== file.id);
    const set = setBuildingFormFieldValue(prop);
    return set(updateBuildingFile(new_files));
};

const renderImage = (prop: BuildingProp, { file }: BuildingFile) => {
    const image_url = file.images?.small ? file.images.small : file.url;
    return DIV(
        'field--image',
        A(
            {
                href: file.url || '#',
                title: file.name || '#',
                target: '_blank',
            },
            IMG({
                src: image_url || '#',
                alt: file.name || '#',
                onError: () => {
                    logger('error loading image', file);
                },
            })
        ),
        buttonDeleteFile(() => actionDeleteFile(prop, file))
    );
};

const renderDocument = (prop: BuildingProp, { file }: BuildingFile) => {
    const filename = fromNullable(file.name).getOrElse('');
    const filetype = fromNullable(filename.split('.').pop()).getOrElse('');
    const trimmedName =
        filename.length > 25 + filetype.length
            ? `${filename.slice(0, 20)}(...).${filetype}`
            : filename;

    return DIV(
        'field--doc',
        A(
            {
                href: file.url || '#',
                target: '_blank',
                title: file.name || '#',
            },
            SPAN('icon', nameToString('file')),
            SPAN({}, trimmedName)
        ),
        buttonDeleteFile(() => actionDeleteFile(prop, file))
    );
};

const renderFileThumbnail = (prop: BuildingProp, file: BuildingFile) => {
    const content_type = file.file.type ? file.file.type.split('/')[0] : '';
    switch (content_type) {
        case 'image':
            return renderImage(prop, file);
        case 'application':
            return renderDocument(prop, file);
        default:
            return DIV({ className: 'field--image__thumbnail' });
    }
};

const renderPreview = (prop: BuildingProp, buildingFiles: BuildingFile[]) => {
    const sorted_files = sortBuildingFilesByContentType(buildingFiles);
    return DIV(
        'preview--doc',
        ...sorted_files.map(file => renderFileThumbnail(prop, file))
    );
};

const renderFileInput = (prop: BuildingProp, idbuild: uuid) => {
    const setHTMLFile = setFile(idbuild);
    return DIV(
        'input input--file',
        INPUT({
            key: `building-form-${prop}`,
            type: 'file',
            multiple: false,
            accept: 'image/*,.doc,.docx,.pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.oasis.opendocument.text',
            onChange: e => {
                fromNullable(e.currentTarget.files)
                    .chain(files => fromNullable(files.item(0)))
                    .map(setHTMLFile);
                clearUploadBuildingFile();
            },
            value: undefined,
        })
    );
};

const renderFilesInput = (prop: BuildingProp, opt: Option<BuildingFile[]>) =>
    getCurrentBuildingForm().map(({ idbuild }) => {
        const set = setBuildingFormFieldValue(prop);

        const renderActionButton = foldRemote<
            SitexFile,
            string,
            NodeOrOptional
        >(
            () =>
                getFile(idbuild)
                    .map(file =>
                        btnUploadFile(() =>
                            processUploadBuildingFile(file).then(data =>
                                fromNullable(data).map(data => {
                                    set(
                                        updateBuildingFile(
                                            opt
                                                .getOrElse([])
                                                .concat(fromSitex(data))
                                        )
                                    );
                                    clearUploadBuildingFile();
                                    clearFile(idbuild);
                                })
                            )
                        )
                    )
                    .getOrElse(btnUploadFile(() => void 0, 'disabled')),
            () => some(DIV('loading', tr.core('loadingData'))),
            () =>
                some(
                    DIV(
                        'error',
                        '~Error uploading',
                        btnNullifyInput(clearUploadBuildingFile)
                    )
                ),
            data => some(DIV('success', data.name))
        );

        return DIV(
            'input input--file',
            DIV(
                'input--files__content',
                DIV(
                    'upload__wrapper',
                    renderFileInput(prop, idbuild),
                    renderActionButton(getUploadBuildingFile())
                ),
                DIV(
                    'preview--doc__wrapper',
                    opt.map(files => renderPreview(prop, files))
                )
            ),
            DIV(
                'input__actions',
                nextProp(prop).map(next =>
                    btnNextInput(() => navigateProp(next))
                )
            )
        );
    });
export default renderFilesInput;

logger('loaded...');
