import { Option } from 'fp-ts/lib/Option';
// import { iife } from 'sdi/lib';
// import { uuid } from 'sdi/source';
import { DIV } from 'sdi/components/elements';
import tr from 'sdi/locale';

// import tr from 'sdi/locale';
// import {
//     getBuildingGroupLookup,
//     filterGroup,
//     findGroup,
//     getCurrentBuildingForm,
//     getOnlineQuality,
//     getBuildingGroups,
//     setBuildingGroups,
//     getGroupSuggestionInCurrentBlock,
// } from 'sitex/src/queries';
import {
    // navigateProp,
    // nullifyBuildingProp,
    // setBuildingFormFieldValue,
    // setBuildingGroupLookup,
    // clearBuildingGroupLookup,
    navigateGroups,
    navigateProp,
} from 'sitex/src/events';
import {
    BuildingProp,
    //     BuildingGroup,
    //     postBuildingGroup,
} from 'sitex/src/remote';
import { btnNextInput, buttonNavigateGroups } from '../../buttons';
import { nextProp } from './common';
// import { nextProp } from './common';
// import { updateBuildGroup } from '../../field';
// import {
//     btnNextInput,
//     btnNullifyInput,
//     buttonGroupAddOther,
// } from '../../buttons';

export type BuildingGroupWidgetState = {
    lookup: string | null;
};

// type GroupUpdate = (group: BuildingGroup) => void;

export const defaultBuildingGroupWidgetState =
    (): BuildingGroupWidgetState => ({
        lookup: null,
    });

// const renderGroupLookupInput = () => {
//     return INPUT({
//         key: 'renderGroupLookupInput',
//         type: 'text',
//         placeholder: tr.sitex('lookup'),
//         defaultValue: getBuildingGroupLookup().toUndefined(),
//         onChange: e => setBuildingGroupLookup(e.currentTarget.value),
//     });
// };

// const renderGroupLookupResult = (update: GroupUpdate) => {
//     return getBuildingGroupLookup().chain(lookup =>
//         filterGroup(lookup).map(groups => {
//             if (groups.length === 0) {
//                 return null;
//             }
//             return DIV(
//                 'result-list',
//                 ...groups.map(g =>
//                     DIV(
//                         {
//                             key: `result-list-group-${g.id}`,
//                             className: 'result-item interactive',
//                             onClick: () => update(g),
//                         },
//                         g.name
//                     )
//                 )
//             );
//         })
//     );
// };

// const { setOtherGroup, getOtherGroup, clearInputOtherGroup } = iife(() => {
//     type Record = { idbuild: uuid; name: string };
//     let groups: Record[] = [];

//     const setOtherGroup = (id: uuid) => (name: string) => {
//         groups = groups
//             .filter(({ idbuild }) => idbuild !== id)
//             .concat({ idbuild: id, name });
//     };

//     const getOtherGroup = (id: uuid) =>
//         fromNullable(groups.find(({ idbuild }) => idbuild === id)).map(
//             ({ name }) => name
//         );

//     const clearInputOtherGroup = (id: uuid) => {
//         groups = groups.filter(({ idbuild }) => idbuild !== id);
//     };

//     return { setOtherGroup, getOtherGroup, clearInputOtherGroup };
// });

// const updateExistingBuildingGroup = (group: BuildingGroup) => {
//     const newGroups = fromNullable(getBuildingGroups())
//         .getOrElse([])
//         .concat(group);
//     setBuildingGroups(newGroups);
// };

// const renderGroupInputOther = (prop: BuildingProp) => {
//     const onlineStatus = getOnlineQuality();
//     if (onlineStatus === 'bad' || onlineStatus === 'critical') {
//         return null;
//     }
//     return getCurrentBuildingForm().map(({ idbuild }) => {
//         const setNewGroup = setOtherGroup(idbuild);
//         return DIV(
//             'other-value',

//             LABEL(
//                 '',
//                 INPUT({
//                     key: `building-form-${prop}-other`,
//                     type: 'text',
//                     placeholder: tr.sitex('otherGroup'),
//                     onBlur: e => {
//                         fromNullable(e.currentTarget.value).map(setNewGroup);
//                         e.currentTarget.value = '';
//                     },
//                 }),
//                 buttonGroupAddOther(() => {
//                     const set = setBuildingFormFieldValue(prop);
//                     getOtherGroup(idbuild).map(val => {
//                         const newValue = val.trim();
//                         if (newValue.length > 0) {
//                             postBuildingGroup(newValue)
//                                 .then(group => {
//                                     set(updateBuildGroup(group.id));
//                                     updateExistingBuildingGroup(group);
//                                 })
//                                 .catch(() =>
//                                     some(
//                                         DIV(
//                                             'error',
//                                             tr.sitex('status/error/add-group')
//                                         )
//                                     )
//                                 );
//                         }
//                     });
//                     clearInputOtherGroup(idbuild);
//                 })
//             )
//         );
//     });
// };

// const renderGroupLookup = (update: GroupUpdate) =>
//     DIV('lookup', renderGroupLookupInput(), renderGroupLookupResult(update));

// const renderBuildingGroupSuggestion = (prop: BuildingProp) => {
//     const set = setBuildingFormFieldValue(prop);
//     const blockInfo = getGroupSuggestionInCurrentBlock();
//     const groups = fromNullable(blockInfo?.groups).getOrElse([]);

//     if (groups.length > 0) {
//         return DIV(
//             { className: 'group-suggestion' },
//             H2('subtitle', tr.sitex('groupSuggestion')),
//             DIV(
//                 'tag__list',
//                 ...groups.map(group =>
//                     DIV(
//                         {
//                             key: `building-form-group-suggestion-${group.id}`,
//                             className: 'tag form-group-suggestion',
//                             onClick: () => {
//                                 set(updateBuildGroup(group.id));
//                             },
//                         },
//                         SPAN('tag__value', group.name)
//                     )
//                 )
//             )
//         );
//     } else {
//         return null;
//     }
// };

// export const renderGroupInput = (prop: BuildingProp, opt: Option<string>) => {
//     const set = setBuildingFormFieldValue(prop);
//     const renderSelectedGroup = findGroup(opt.getOrElse('')).map(({ name }) =>
//         DIV('value value--group', name)
//     );

//     const update = (group: BuildingGroup) => {
//         set(updateBuildGroup(group.id));
//         clearBuildingGroupLookup();
//     };
//     return DIV(
//         'input input__wrapper--group',
//         renderSelectedGroup,
//         DIV(
//             '',
//             renderGroupLookup(update),

//             renderGroupInputOther(prop),
//             renderBuildingGroupSuggestion(prop)
//         ),
//         DIV(
//             'input__actions',
//             opt.map(() => btnNullifyInput(() => nullifyBuildingProp(prop))),
//             nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
//         )
//     );
// };

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const renderGroupInput = (prop: BuildingProp, _opt: Option<string>) => {
    return DIV(
        'input input__wrapper--group',
        DIV(
            'info',
            tr.sitex('groups/inBuildingView'),
            buttonNavigateGroups(navigateGroups)
        ),
        DIV(
            'input__actions',
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};
