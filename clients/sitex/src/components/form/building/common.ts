import { flatten, index } from 'fp-ts/lib/Array';
import { BuildingProp } from 'sitex/src/remote';
import { SitexMessageKey } from 'sitex/src/locale';
import { fromNullable } from 'fp-ts/lib/Option';

export type GroupName = 'general' | 'secondary' | 'details';

export type BuildingPropGroup = {
    name: GroupName;
    props: BuildingProp[];
};

export const buildingPropGroups: BuildingPropGroup[] = [
    {
        name: 'general',
        props: [
            'groundarea',
            'nblevel',
            'nblevelrec',
            'statecode',
            'typologycode',
            'nbparkcar',
            'nbparkbike',
            'geostate',
            'buildgroup',
        ],
    },
    {
        name: 'secondary',
        props: ['occupancys', 'geom', 'files'],
    },
    {
        name: 'details',
        props: [
            'description',
            'internaldesc',
            'versiondesc',
            'datastate',
            'observation_status',
        ],
    },
];

export const nextProp = (prop: BuildingProp) => {
    const props = flatten(buildingPropGroups.map(({ props }) => props));
    const propIndex = props.indexOf(prop);

    return index(propIndex + 1, props);
};

export const findBuildingPropGroup = (groupName: GroupName) =>
    fromNullable(buildingPropGroups.find(({ name }) => name === groupName));

export const propToMessageKey = (prop: BuildingProp): SitexMessageKey => {
    // TODO
    return prop as SitexMessageKey;
};
