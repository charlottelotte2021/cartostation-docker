import * as debug from 'debug';
import { Option } from 'fp-ts/lib/Option';
import { DIV, H2 } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import {
    getBuildingFormFieldValue,
    getSurveyProp,
    getTypologyDict,
    getStatecodeDict,
    getObservationDict,
} from 'sitex/src/queries';
import {
    navigateProp,
    nullifyBuildingProp,
    setBuildingFormFieldValue,
} from 'sitex/src/events';
import { BuildingProp, DataState, DictTerm, GeoState } from 'sitex/src/remote';
import { nextProp, propToMessageKey } from './common';
import {
    updateDictStateCode,
    FieldValueDescriptor,
    updateDataState,
    updateNumber,
    updateString,
    updateGeoState,
    updateDictTypo,
    updateDictObservation,
} from '../../field';
import { btnNextInput, btnNullifyInput } from '../../buttons';
import renderOccupationInput from './occupation';
import renderFilesInput from './files';
import renderGeometryInput from './geometry';
import {
    attrOptions,
    inputLongText2,
    inputNumber,
    inputText,
    radioRenderer,
} from 'sdi/components/input';
import { renderGroupInput } from './group';

const logger = debug('sdi:building/editor');

// const inputZone = () => DIV(
//     'input__zone',
//     // renderFieldInput(inputNumber('building', 'nblevel', tr.sitex('uploadFile'))),
//     // stepLevel(),
//     stepOccupationCode(),
//     // btnNextInput(),
// );

const renderStringInput = (prop: BuildingProp, opt: Option<string>) => {
    const set = setBuildingFormFieldValue(prop);
    const isLong = prop === 'description' || prop === 'internaldesc';
    const options = {
        key: `building-form-${prop}`,
        get: () => opt,
        set: (s: string) => set(updateString(s)),
        attrs: {
            placeholder: tr.sitex('insertValue'),
        },
    };
    const inpuTextFn = isLong ? inputLongText2 : inputText;

    return DIV(
        'input input--string',
        inpuTextFn(options),
        DIV(
            'input__actions',
            opt.map(() => btnNullifyInput(() => nullifyBuildingProp(prop))),
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};

const renderNumberInput = (prop: BuildingProp, opt: Option<number>) => {
    const set = setBuildingFormFieldValue(prop);
    return DIV(
        'input input--number',
        inputNumber(
            attrOptions<number>(
                `building-form-${prop}`,
                () => opt,
                value => set(updateNumber(value)),
                {
                    placeholder: tr.sitex('insertValue'),
                }
            )
        ),

        DIV(
            'input__actions',
            opt.map(() => btnNullifyInput(() => nullifyBuildingProp(prop))),
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};

// const withChecked = fromPredicate<HTMLInputElement>(el => el.checked);

const renderDataStateInput = (prop: BuildingProp, opt: Option<DataState>) => {
    const set = setBuildingFormFieldValue(prop);
    const renderItem = (ds: DataState) => {
        switch (ds) {
            case 'D':
                return tr.sitex('datastateInitial');
            case 'O':
                return tr.sitex('datastateOngoing');
            case 'V':
                return tr.sitex('datastateValidated');
        }
    };

    const select = (ds: DataState) => set(updateDataState(ds));
    const isSelected = (ds: DataState) =>
        opt.map(v => v === ds).getOrElse(false);

    const renderRadio = radioRenderer(
        'DataStateInput',
        renderItem,
        select,
        isSelected
    );

    return DIV(
        'input radio-block',
        renderRadio(['D', 'O', 'V']),
        DIV(
            'input__actions',
            opt.map(() => btnNullifyInput(() => nullifyBuildingProp(prop))),
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};

const renderTypologyInput = (prop: BuildingProp, opt: Option<number>) => {
    const set = setBuildingFormFieldValue(prop);

    const renderItem = (t: DictTerm) => fromRecord(t.name);
    const select = (t: DictTerm) => set(updateDictTypo(t.id));
    const isSelected = (t: DictTerm) =>
        opt.map(v => v === t.id).getOrElse(false);
    const renderRadio = radioRenderer(
        'TypologyInput',
        renderItem,
        select,
        isSelected
    );

    const typologyCodes = getTypologyDict();

    return DIV(
        'input input--term',
        renderRadio(typologyCodes),
        DIV(
            'input__actions',
            opt.map(() => btnNullifyInput(() => nullifyBuildingProp(prop))),
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};

const renderStatecodeInput = (prop: BuildingProp, opt: Option<number>) => {
    const set = setBuildingFormFieldValue(prop);
    const renderItem = (t: DictTerm) => fromRecord(t.name);
    const select = (t: DictTerm) => set(updateDictStateCode(t.id));
    const isSelected = (t: DictTerm) =>
        opt.map(v => v === t.id).getOrElse(false);
    const renderRadio = radioRenderer(
        'StatecodeInput',
        renderItem,
        select,
        isSelected
    );
    const stateCodes = getStatecodeDict();

    return DIV(
        'input input--statecode',
        renderRadio(stateCodes),
        DIV(
            'input__actions',
            opt.map(() => btnNullifyInput(() => nullifyBuildingProp(prop))),
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};

const renderObservationInput = (prop: BuildingProp, opt: Option<number>) => {
    const set = setBuildingFormFieldValue(prop);
    const renderItem = (t: DictTerm) => fromRecord(t.name);
    const select = (t: DictTerm) => set(updateDictObservation(t.id));
    const isSelected = (t: DictTerm) =>
        opt.map(v => v === t.id).getOrElse(false);
    const renderRadio = radioRenderer(
        'ObservationInput',
        renderItem,
        select,
        isSelected
    );
    const codes = getObservationDict();

    return DIV(
        'input input--observation',
        renderRadio(codes),
        DIV(
            'input__actions',
            opt.map(() => btnNullifyInput(() => nullifyBuildingProp(prop))),
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};

const renderGeoStateInput = (prop: BuildingProp, opt: Option<GeoState>) => {
    const set = setBuildingFormFieldValue(prop);
    const renderItem = (gs: GeoState) => {
        switch (gs) {
            case 'D':
                return tr.sitex('geostateDraft');
            case 'O':
                return tr.sitex('geostateOngoing');
            case 'V':
                return tr.sitex('geostateValidated');
        }
    };
    const select = (gs: GeoState) => set(updateGeoState(gs));
    const isSelected = (gs: GeoState) =>
        opt.map(v => v === gs).getOrElse(false);
    const renderRadio = radioRenderer(
        'GeoStateInput',
        renderItem,
        select,
        isSelected
    );

    return DIV(
        'input radio-block',
        renderRadio(['D', 'O', 'V']),
        DIV(
            'input__actions',
            opt.map(() => btnNullifyInput(() => nullifyBuildingProp(prop))),
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};

const renderInput = (prop: BuildingProp, d: FieldValueDescriptor) => {
    switch (d.tag) {
        case 'string':
            return renderStringInput(prop, d.value);
        case 'number':
            return renderNumberInput(prop, d.value);
        case 'datastate':
            return renderDataStateInput(prop, d.value);
        case 'occupation-building':
            return renderOccupationInput(prop, d.value);
        case 'dict-typo':
            return renderTypologyInput(prop, d.value);
        case 'dict-statecode':
            return renderStatecodeInput(prop, d.value);
        case 'dict-observation':
            return renderObservationInput(prop, d.value);
        case 'files-building':
            return renderFilesInput(prop, d.value);
        case 'geostate':
            return renderGeoStateInput(prop, d.value);
        case 'geometry':
            return renderGeometryInput(prop, d.value);
        case 'buildgroup':
            return renderGroupInput(prop, d.value);
        case 'recordstate':
        case 'dict-update':
        default:
            return DIV('value', `~TODO(${d.tag})`);
    }
};

const renderWithField = (prop: BuildingProp, desc: FieldValueDescriptor) =>
    DIV(
        'main-content editor__wrapper',
        DIV(
            'input__zone',
            H2(`${prop}`, tr.sitex(propToMessageKey(prop))),
            renderInput(prop, desc)
        )
    );

const renderWithoutField = () =>
    DIV('prop-not-found', tr.sitex('FieldCouldNotBeFoundForThisBuilding'));

const renderWithProp = (prop: BuildingProp) =>
    getBuildingFormFieldValue(prop).fold(renderWithoutField, desc =>
        renderWithField(prop, desc)
    );

const renderWithoutProp = () =>
    DIV(
        'main-content editor__wrapper building empty',
        H2('', tr.sitex('pleaseSelectField'))
    );

export const renderFormBuildingInput = () =>
    getSurveyProp().foldL(renderWithoutProp, renderWithProp);

logger('loaded');
