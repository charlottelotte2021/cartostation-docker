import { Option, none, some } from 'fp-ts/lib/Option';
import { DIV } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { create } from 'sdi/map';
import { MultiPolygon } from 'sdi/source';
import { tryMultiPolygon } from 'sdi/util';
import {
    navigateProp,
    setParcelFormFieldValue,
    updateEditorView,
} from 'sitex/src/events';
import editor from 'sitex/src/queries/map/geometry';
import { ParcelProp } from 'sitex/src/remote';
import { btnNextInput } from '../../buttons';
import { geometryValue } from '../../field';
import { nextProp } from './common';

let mapUpdate: Option<() => void> = none;
let mapSetTarget: Option<(e: HTMLElement) => void> = none;

const attachMap = (prop: ParcelProp) => (element: HTMLElement | null) => {
    const set = setParcelFormFieldValue(prop);
    mapUpdate = mapUpdate.foldL(
        () => {
            const { update, setTarget, editable } = create(editor.name, {
                getBaseLayer: editor.getBaseLayer,
                getView: editor.getView,
                getMapInfo: editor.getMapInfo,

                updateView: updateEditorView,
                setScaleLine: () => void 0,

                element,
            });

            editable(
                {
                    getCurrentLayerId: () => editor.name,
                    getGeometryType: () => 'MultiPolygon',
                    addFeature: () => void 0, // create
                    setGeometry: geom =>
                        tryMultiPolygon(geom).map(mp => set(geometryValue(mp))),
                },
                editor.getInteraction
            );

            mapSetTarget = some(setTarget);
            update();
            return some(update);
        },
        update => some(update)
    );

    if (element) {
        mapSetTarget.map(f => f(element));
    }
};

const renderMap = (prop: ParcelProp) => {
    mapUpdate.map(f => f());
    return DIV(
        {
            className: 'map-wrapper',
            'aria-label': `${tr.core('map')}`,
        },
        DIV({
            id: editor.name,
            key: editor.name,
            className: 'map',
            ref: attachMap(prop),
        })
    );
};

export const renderInput = (
    prop: ParcelProp,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    _opt: Option<MultiPolygon>
) => {
    return DIV(
        'input input--geometry',
        DIV('geometry', renderMap(prop)),
        DIV(
            'input__actions',
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};

export default renderInput;
