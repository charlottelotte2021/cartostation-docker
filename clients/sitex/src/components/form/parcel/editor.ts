import { fromNullable, Option } from 'fp-ts/lib/Option';
import { DIV, H2, INPUT, TEXTAREA } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { getParcelFormFieldValue, getSurveyProp } from 'sitex/src/queries';
import {
    navigateProp,
    nullifyParcelProp,
    setParcelFormFieldValue,
} from 'sitex/src/events';
import { ParcelProp, DataState, GeoState } from 'sitex/src/remote';
import { propParcelToMessageKey } from '.';
import {
    FieldValueDescriptor,
    updateDataState,
    updateNumber,
    updateString,
    updateGeoState,
    updateBoolean,
} from '../../field';
import { btnNextInput, btnNullifyInput } from '../../buttons';
import renderOccupationInput from './occupation';
import renderFilesInput from './files';
import * as debug from 'debug';
import {
    attrOptions,
    inputNumber,
    radioRenderer,
    renderCheckbox,
} from 'sdi/components/input';
import { SitexMessageKey } from 'sitex/src/locale';
import { nextProp } from './common';
import renderGeometryInput from './geometry';

const logger = debug('sdi:parcel/editor');

// const inputZone = () => DIV(
//     'input__zone',
//     // renderFieldInput(inputNumber('building', 'nblevel', tr.sitex('uploadFile'))),
//     // stepLevel(),
//     stepOccupationCode(),
//     // btnNextInput(),
// );

const LININGS: { [k: string]: SitexMessageKey } = {
    pervious: 'liningPervious',
    impervious: 'liningImpervious',
    lowveg: 'liningLowVegetation',
    highveg: 'liningHighVegetation',
    bare: 'liningBarePlot',
};

export const findLiningName = (k: string) =>
    fromNullable(LININGS[k]).map<string>(trk => tr.sitex(trk));

// this is a temporary workaround, in need of proper backend implementation
const renderLiningInput = (opt: Option<string>) => {
    const prop: ParcelProp = 'lining';
    const set = setParcelFormFieldValue(prop);
    const renderItem = (k: string) => findLiningName(k).getOrElse(k);

    const select = (s: string) => set(updateString(s));
    const isSelected = (s: string) => opt.map(v => v === s).getOrElse(false);

    const renderRadio = radioRenderer(
        'LiningInput',
        renderItem,
        select,
        isSelected
    );

    return DIV(
        'input radio-block',
        renderRadio(Object.keys(LININGS)),
        DIV(
            'input__actions',
            opt.map(() => btnNullifyInput(() => nullifyParcelProp(prop))),
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};

const SITUATIONS: { [k: string]: SitexMessageKey } = {
    front: 'situationFront',
    inside: 'situationInsideBlock',
};

export const findSituationName = (k: string) =>
    fromNullable(SITUATIONS[k]).map<string>(trk => tr.sitex(trk));

// this is a temporary workaround, in need of proper backend implementation
const renderSituationInput = (opt: Option<string>) => {
    const prop: ParcelProp = 'situation';
    const set = setParcelFormFieldValue(prop);
    const renderItem = (k: string) => findSituationName(k).getOrElse(k);

    const select = (s: string) => set(updateString(s));
    const isSelected = (s: string) => opt.map(v => v === s).getOrElse(false);

    const renderRadio = radioRenderer(
        'SituationInput',
        renderItem,
        select,
        isSelected
    );

    return DIV(
        'input radio-block',
        renderRadio(Object.keys(SITUATIONS)),
        DIV(
            'input__actions',
            opt.map(() => btnNullifyInput(() => nullifyParcelProp(prop))),
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};

const renderStringInput = (prop: ParcelProp, opt: Option<string>) => {
    if (prop === 'lining') {
        return renderLiningInput(opt);
    } else if (prop === 'situation') {
        return renderSituationInput(opt);
    }
    const set = setParcelFormFieldValue(prop);

    if (prop === 'description' || prop === 'internaldesc') {
        return DIV(
            'input input--string',
            TEXTAREA({
                key: `parcel-form-${prop}`,
                placeholder: tr.sitex('insertValue'),
                onChange: e => set(updateString(e.currentTarget.value)),
                value: opt.toUndefined(),
            }),
            DIV(
                'input__actions',
                opt.map(() => btnNullifyInput(() => nullifyParcelProp(prop))),
                nextProp(prop).map(next =>
                    btnNextInput(() => navigateProp(next))
                )
            )
        );
    }

    return DIV(
        'input input--string',
        INPUT({
            key: `parcel-form-${prop}`,
            type: 'text',
            placeholder: tr.sitex('insertValue'),
            onChange: e => set(updateString(e.currentTarget.value)),
            value: opt.toUndefined(),
        }),
        DIV(
            'input__actions',
            opt.map(() => btnNullifyInput(() => nullifyParcelProp(prop))),
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};

const renderNumberInput = (prop: ParcelProp, opt: Option<number>) => {
    const set = setParcelFormFieldValue(prop);
    return DIV(
        'input input--number',
        inputNumber(
            attrOptions<number>(
                `parcel-form-${prop}`,
                () => opt,
                value => set(updateNumber(value)),
                {
                    placeholder: tr.sitex('insertValue'),
                }
            )
        ),
        DIV(
            'input__actions',
            opt.map(() => btnNullifyInput(() => nullifyParcelProp(prop))),
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};

const renderBooleanInput = (prop: ParcelProp, opt: Option<boolean>) => {
    const set = setParcelFormFieldValue(prop);
    const checkbox = renderCheckbox(
        `parcel-boolean-${prop}`,
        () => tr.sitex('yes'),
        b => set(updateBoolean(b))
    );
    return DIV(
        `input input--boolean ${prop}`,
        checkbox(opt.getOrElse(false)),
        DIV(
            'input__actions',
            opt.map(() => btnNullifyInput(() => nullifyParcelProp(prop))),
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};

const renderDataStateInput = (prop: ParcelProp, opt: Option<DataState>) => {
    const set = setParcelFormFieldValue(prop);
    const renderItem = (ds: DataState) => {
        switch (ds) {
            case 'D':
                return tr.sitex('datastateInitial');
            case 'O':
                return tr.sitex('datastateOngoing');
            case 'V':
                return tr.sitex('datastateValidated');
        }
    };

    const select = (ds: DataState) => set(updateDataState(ds));
    const isSelected = (ds: DataState) =>
        opt.map(v => v === ds).getOrElse(false);

    const renderRadio = radioRenderer(
        'DataStateInput',
        renderItem,
        select,
        isSelected
    );

    return DIV(
        'input radio-block',
        renderRadio(['D', 'O', 'V']),
        DIV(
            'input__actions',
            opt.map(() => btnNullifyInput(() => nullifyParcelProp(prop))),
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};

const renderGeoStateInput = (prop: ParcelProp, opt: Option<GeoState>) => {
    const set = setParcelFormFieldValue(prop);
    const renderItem = (gs: GeoState) => {
        switch (gs) {
            case 'D':
                return tr.sitex('geostateDraft');
            case 'O':
                return tr.sitex('geostateOngoing');
            case 'V':
                return tr.sitex('geostateValidated');
        }
    };
    const select = (gs: GeoState) => set(updateGeoState(gs));
    const isSelected = (gs: GeoState) =>
        opt.map(v => v === gs).getOrElse(false);
    const renderRadio = radioRenderer(
        'GeoStateInput',
        renderItem,
        select,
        isSelected
    );

    return DIV(
        'input radio-block',
        renderRadio(['D', 'O', 'V']),
        DIV(
            'input__actions',
            opt.map(() => btnNullifyInput(() => nullifyParcelProp(prop))),
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};

const renderInput = (prop: ParcelProp, d: FieldValueDescriptor) => {
    switch (d.tag) {
        case 'string':
            return renderStringInput(prop, d.value);
        case 'number':
            return renderNumberInput(prop, d.value);
        case 'boolean':
            return renderBooleanInput(prop, d.value);
        case 'datastate':
            return renderDataStateInput(prop, d.value);
        case 'occupation-parcel':
            return renderOccupationInput(prop, d.value);
        case 'files-parcel':
            return renderFilesInput(prop, d.value);
        case 'geostate':
            return renderGeoStateInput(prop, d.value);
        case 'geometry':
            return renderGeometryInput(prop, d.value);
        case 'recordstate':
        case 'dict-update':
        default:
            return DIV('value', `~TODO(${d.tag})`);
    }
};

const renderWithField = (prop: ParcelProp, desc: FieldValueDescriptor) =>
    DIV(
        'main-content editor__wrapper',
        DIV(
            'input__zone',
            H2(`${prop}`, tr.sitex(propParcelToMessageKey(prop))),
            renderInput(prop, desc)
        )
    );

const renderWithoutField = () =>
    DIV('prop-not-found', tr.sitex('FieldCouldNotBeFoundForThisParcel'));

const renderWithProp = (prop: ParcelProp) =>
    getParcelFormFieldValue(prop).fold(renderWithoutField, desc =>
        renderWithField(prop, desc)
    );

const renderWithoutProp = () =>
    DIV(
        'main-content editor__wrapper parcel empty',
        H2('', tr.sitex('pleaseSelectField'))
    );

export const renderFormParcelInput = () =>
    getSurveyProp().foldL(renderWithoutProp, renderWithProp);

logger('loaded');
