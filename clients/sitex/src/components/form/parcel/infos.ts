import * as debug from 'debug';
import { fromNullable, none, Option, some } from 'fp-ts/lib/Option';
import { nameToString } from 'sdi/components/button/names';
import { DIV, H1, IMG, SPAN, A, H2 } from 'sdi/components/elements';
import { iife } from 'sdi/lib';
import tr, { formatNumber } from 'sdi/locale';
import { miniMap2 as minimap } from 'sdi/map/mini';
import { MultiPolygon } from 'sdi/source';
import {
    clearParcelProp,
    commitParcelForm,
    navigateHistory,
    navigateIndex,
    navigatePreview,
    navigateProp,
    resetForm,
} from 'sitex/src/events';
import {
    ParcelFormData,
    getCurrentParcelForm,
    getSurveyProp,
    findUrbisAddressParcel,
    CompressesedAddress,
    CompressesedAddressComp,
    parcelFormIsDiverging,
    parcelFormValidation,
} from 'sitex/src/queries';
import { ParcelProp, ParcelFile, DataState, GeoState } from 'sitex/src/remote';
import {
    ParcelPropGroup,
    findParcelPropGroup,
    propParcelToMessageKey,
} from '.';
import {
    btnValidate,
    buttonHistory,
    buttonHome,
    buttonPreview,
    buttonRevert,
} from '../../buttons';
import { FieldValueDescriptor, fromRawField } from '../../field';
import {
    renderParcelOccupation,
    sortParcelFilesByContentType,
} from 'sitex/src/components/preview';
import { findLiningName, findSituationName } from './editor';

const logger = debug('sdi:parcel/preview');

const wrapperClassName = (
    prop: ParcelProp,
    { touched, value }: FieldValueDescriptor
) =>
    `kv__wrapper ${touched ? 'touched' : ''}  ${value
        .map(() => 'present')
        .getOrElse('missing')} ${getSurveyProp()
        .map(p => (p === prop ? 'active' : ''))
        .getOrElse('')}`;

const valueStatus = (prop: ParcelProp, { touched }: FieldValueDescriptor) =>
    DIV(
        'status__wrapper',
        touched ? buttonRevert(() => clearParcelProp(prop)) : '',
        getSurveyProp()
            .map(p =>
                p === prop
                    ? DIV('status active icon', nameToString('chevron-right'))
                    : ''
            )
            .getOrElse('')
    );

const renderString = (val: string) => {
    const name = findLiningName(val).alt(findSituationName(val)).getOrElse(val);
    return DIV('value value--string', name);
};

const renderNumber = (n: number) => DIV('value value--number', formatNumber(n));

const renderBoolean = (b: boolean) =>
    DIV('value value--boolean', b ? tr.sitex('yes') : tr.sitex('no'));

const renderDataState = (s: DataState) => {
    const label = iife(() => {
        switch (s) {
            case 'D':
                return tr.sitex('datastateInitial');
            case 'O':
                return tr.sitex('datastateOngoing');
            case 'V':
                return tr.sitex('datastateValidated');
        }
    });
    return DIV('value value--data-state', label);
};

const renderMiniMap = minimap(
    'https://geoservices-urbis.irisnet.be/geoserver/ows',
    {
        LAYERS: 'urbisFRGray',
    }
);

const renderGeometry = (geom: MultiPolygon) => {
    const step = renderMiniMap(geom);
    switch (step.step) {
        case 'failed':
            return none;
        case 'loading':
            return some(DIV('minimap-loading', tr.sitex('loading')));
        case 'loaded':
            return some(DIV('value minimap-loaded', IMG({ src: step.data })));
    }
};

const renderGeoState = (s: GeoState) => {
    const label = iife(() => {
        switch (s) {
            case 'D':
                return tr.sitex('geostateDraft');
            case 'O':
                return tr.sitex('geostateOngoing');
            case 'V':
                return tr.sitex('geostateValidated');
        }
    });
    return DIV('value value--geo-state', label);
};

const renderImage = ({ file }: ParcelFile) => {
    const image_url = file.images?.small ? file.images.small : file.url;
    return DIV(
        'field--image',
        IMG({
            src: image_url || '#',
            alt: file.name || '#',

            onError: () => {
                logger('error loading image', file);
            },
        })
    );
};

const renderDocument = ({ file }: ParcelFile) => {
    const filename = fromNullable(file.name).getOrElse('');
    const filetype = fromNullable(filename.split('.').pop()).getOrElse('');
    const trimmedName =
        filename.length > 25 + filetype.length
            ? `${filename.slice(0, 20)}(...).${filetype}`
            : filename;

    return DIV(
        'field--doc',
        A(
            {
                href: file.url || '#',
                target: '_blank',
                title: file.name || '#',
            },
            SPAN('icon', nameToString('file')),
            SPAN({}, trimmedName)
        )
    );
};

const renderFileThumbnail = (file: ParcelFile) => {
    const content_type = file.file.type ? file.file.type.split('/')[0] : '';
    switch (content_type) {
        case 'image':
            return renderImage(file);
        case 'application':
            return renderDocument(file);
        default:
            return DIV({ className: 'field--image__thumbnail' });
    }
};

const renderFiles = (files: ParcelFile[]) => {
    const sorted_files = sortParcelFilesByContentType(files);
    return DIV(
        'files-summary__wrapper',
        ...sorted_files.map(renderFileThumbnail)
    );
};

const emptyValue = DIV('value value--empty');

const renderValue = (d: FieldValueDescriptor): Option<React.ReactNode> => {
    switch (d.tag) {
        case 'string':
            return d.value.map(renderString);
        case 'number':
            return d.value.map(renderNumber);
        case 'boolean':
            return d.value.map(renderBoolean);
        case 'datastate':
            return d.value.map(renderDataState);
        case 'geometry':
            return d.value.map(renderGeometry);
        case 'geostate':
            return d.value.map(renderGeoState);
        case 'occupation-parcel':
            return d.value.map(renderParcelOccupation);
        case 'files-parcel':
            return d.value.map(renderFiles);
        case 'recordstate':
        case 'dict-update':
        default:
            return some(DIV('value', `~TODO(${d.tag})`));
    }
};

const infoItem = (prop: ParcelProp, data: ParcelFormData) => {
    const field = fromRawField(data[prop]);
    return DIV(
        {
            className: wrapperClassName(prop, field),
            onClick: () => navigateProp(prop),
        },
        DIV('key', tr.sitex(propParcelToMessageKey(prop))),
        renderValue(field).getOrElse(emptyValue),
        valueStatus(prop, field)
    );
};

const renderGoup =
    (form: ParcelFormData) =>
    ({ props }: ParcelPropGroup) =>
        DIV('infos__group', ...props.map(prop => infoItem(prop, form)));

const infosWrapper = (form: ParcelFormData) =>
    DIV(
        'infos parcel',
        findParcelPropGroup('general').map(renderGoup(form)),
        findParcelPropGroup('secondary').map(renderGoup(form)),
        findParcelPropGroup('details').map(renderGoup(form))
    );

const renderCommpressedAddressComp = (addr: CompressesedAddressComp) =>
    DIV(
        'compressed-address',
        addr.comps
            .map(
                ({ streetName, numbers }) =>
                    `${streetName} ${numbers.join(' - ')}`
            )
            .join('; '),
        `, ${addr.municipality}`
    );

const renderUrbisAddress = (address: CompressesedAddress) =>
    DIV(
        `address__container  compress-${address.comps.length}`,
        H1('', ...address.comps.map(renderCommpressedAddressComp)),
        DIV(
            'capakey',
            `${tr.sitex('localCadastralplot')} : ${address.capakeys.join('; ')}`
        )
    );

const actionValidate = (keys: ParcelProp[]) =>
    parcelFormValidation().fold(
        () =>
            DIV(
                { className: 'form-actions', title: keys.join(', ') },
                btnValidate(() => undefined, 'disabled error'),
                buttonPreview(() => undefined, 'disabled'),
                buttonHistory(() => undefined, 'disabled')
            ),
        () =>
            DIV(
                { className: 'form-actions', title: keys.join(', ') },
                btnValidate(() => {
                    commitParcelForm();
                    resetForm();
                    navigateIndex('parcel');
                }),
                buttonPreview(() => undefined, 'disabled'),
                buttonHistory(() => undefined, 'disabled')
            )
    );

const actionNavigate = (id: string) => () =>
    DIV(
        'form-actions',
        btnValidate(() => undefined, 'disabled'),
        buttonPreview(() => navigatePreview('parcel', id)),
        buttonHistory(() => navigateHistory('parcel', id))
    );

const formActions = (id: string) =>
    parcelFormIsDiverging().foldL(actionNavigate(id), actionValidate);

const formHeader = () =>
    DIV(
        'sidebar__header form__header',
        buttonHome(() => navigateIndex('parcel')),
        H2('subtitle', tr.sitex('parcelSheetTitle'))
    );

const infosHeader = (form: ParcelFormData) =>
    DIV(
        'infos__header',
        DIV(
            'address__container',

            fromNullable(form.u2parcel.value)
                .chain(findUrbisAddressParcel)
                .map(renderUrbisAddress),

            DIV('sitex-id', `${tr.sitex('sitexID')} : ${form.idparcel}`)
        ),
        formActions(form.idparcel)
    );

export const renderFormParcelInfo = () =>
    getCurrentParcelForm().map(form =>
        DIV(
            'sidebar infos__wrapper parcel',
            formHeader(),
            infosHeader(form),
            infosWrapper(form)
        )
    );
