import * as debug from 'debug';
import { flatten, index } from 'fp-ts/lib/Array';
import { fromNullable, Option, some } from 'fp-ts/lib/Option';
import {
    DIV,
    INPUT,
    NodeOrOptional,
    IMG,
    SPAN,
    A,
} from 'sdi/components/elements';
import { iife } from 'sdi/lib';
import tr from 'sdi/locale';
import { foldRemote, uuid } from 'sdi/source';
import {
    setParcelFormFieldValue,
    processUploadParcelFile,
    clearUploadParcelFile,
    navigateProp,
} from 'sitex/src/events';
import {
    getCurrentParcelForm,
    getUploadParcelFile,
    getCurrentParcelFormFieldValue,
} from 'sitex/src/queries';
import { ParcelProp, ParcelFile, SitexFile } from 'sitex/src/remote';
import {
    btnNullifyInput,
    btnUploadFile,
    btnValidate,
    btnNextInput,
    buttonDeleteFile,
} from '../../buttons';
import { updateParcelFile } from '../../field';
import { parcelPropGroups } from '.';
import { nameToString } from 'sdi/components/button/names';
import { sortParcelFilesByContentType } from 'sitex/src/components/preview';

const logger = debug('sdi:building/files');

// const renderFileContent = (file: ParcelFile) => {
//     logger('renderFileContent', file);
//     switch (file.filetype) {
//         case 'jpeg':
//             return DIV({className: 'building-file'}, IMG(file.fileurl))
//         default:
//             return DIV({className: 'building-file'}, SPAN('', 'Not image'));
//     };
// };

const nextProp = (prop: ParcelProp) => {
    const props = flatten(parcelPropGroups.map(({ props }) => props));
    const propIndex = props.indexOf(prop);

    return index(propIndex + 1, props);
};

const fromSitex = (file: SitexFile): ParcelFile => ({
    file,
    idparcelversion: null,
    creator: null,
    date: null,
    id: null,
    online: null,
});

const { setFile, getFile, clearFile } = iife(() => {
    type Record = { idparcel: uuid; file: File };
    let files: Record[] = [];

    const setFile = (id: uuid) => (file: File) => {
        files = files
            .filter(({ idparcel }) => idparcel !== id)
            .concat({ idparcel: id, file });
    };

    const getFile = (id: uuid) =>
        fromNullable(files.find(({ idparcel }) => idparcel === id)).map(
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            ({ file }) => file
        );

    const clearFile = (id: uuid) => {
        files = files.filter(({ idparcel }) => idparcel !== id);
    };

    return { setFile, getFile, clearFile };
});

const actionDeleteFile = (prop: ParcelProp, file: SitexFile) => {
    const new_files = (
        getCurrentParcelFormFieldValue(prop) as ParcelFile[]
    ).filter(({ file: { id } }) => id !== file.id);
    const set = setParcelFormFieldValue(prop);
    return set(updateParcelFile(new_files));
};

// const renderFilePreview = ({ file }: ParcelFile) => {
//     const image_url = file.images?.small ? file.images.small : file.url;
//     return SPAN(
//         { className: 'field--image__thumbnail' },
//         IMG({
//             className: 'field--image__thumbnail__img',
//             src: image_url || '#',
//             alt: file.name || '#',
//             style: {
//                 width: '64px',
//                 height: '64px',
//             },
//             onError: () => {
//                 logger('error loading image', file);
//             },
//         })
//     );
// };

// const renderDocumentIcon = ({ file }: ParcelFile) => {
//     const filetype = file.name ? file.name.split('.').pop() : '';
//     return DIV(
//         {
//             className: 'field--doc__thumbnail',
//             style: {
//                 minWidth: '64px',
//                 minHeight: '64px',
//                 textAlign: 'center',
//                 position: 'relative',
//             },
//         },
//         SPAN(
//             {
//                 className: 'fa fa-file',
//                 style: { position: 'inherit', top: '30%' },
//             },
//             A(
//                 {
//                     href: file.url || '#',
//                     target: '_blank',
//                     title: file.name || '#',
//                 },
//                 P({ style: { paddingTop: '2px' } }, filetype)
//             )
//         )
//     );
// };

const renderImage = (prop: ParcelProp, { file }: ParcelFile) => {
    const image_url = file.images?.small ? file.images.small : file.url;
    return DIV(
        'field--image',
        IMG({
            src: image_url || '#',
            alt: file.name || '#',
            // style: {
            //     width: '64px',
            //     height: '64px',
            // },
            onError: () => {
                logger('error loading image', file);
            },
        }),
        buttonDeleteFile(() => actionDeleteFile(prop, file))
    );
};

const renderDocument = (prop: ParcelProp, { file }: ParcelFile) => {
    const filename = fromNullable(file.name).getOrElse('');
    const filetype = fromNullable(filename.split('.').pop()).getOrElse('');
    const trimmedName =
        filename.length > 25 + filetype.length
            ? `${filename.slice(0, 20)}(...).${filetype}`
            : filename;

    return DIV(
        'field--doc',
        A(
            {
                href: file.url || '#',
                target: '_blank',
                title: file.name || '#',
            },
            SPAN('icon', nameToString('file')),
            SPAN({}, trimmedName)
        ),
        buttonDeleteFile(() => actionDeleteFile(prop, file))
    );
};

const renderFileThumbnail = (prop: ParcelProp, file: ParcelFile) => {
    const content_type = file.file.type ? file.file.type.split('/')[0] : '';
    switch (content_type) {
        case 'image':
            return renderImage(prop, file);
        case 'application':
            return renderDocument(prop, file);
        default:
            return DIV({ className: 'field--image__thumbnail' });
    }
};

const renderPreview = (prop: ParcelProp, parcelFiles: ParcelFile[]) => {
    const sorted_files = sortParcelFilesByContentType(parcelFiles);
    return DIV(
        'preview',
        ...sorted_files.map(file => renderFileThumbnail(prop, file))
    );
};

const renderFileInput = (prop: ParcelProp, idparcel: uuid) => {
    const setHTMLFile = setFile(idparcel);
    return DIV(
        'input input--file',
        INPUT({
            key: `parcel-form-${prop}`,
            type: 'file',
            placeholder: tr.sitex('insertValue'),
            multiple: false,
            accept: 'image/*,.doc,.docx,.pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.oasis.opendocument.text',
            onChange: e => {
                fromNullable(e.currentTarget.files)
                    .chain(files => fromNullable(files.item(0)))
                    .map(setHTMLFile);
                clearUploadParcelFile();
            },
        })
    );
};

const renderFilesInput = (prop: ParcelProp, opt: Option<ParcelFile[]>) =>
    getCurrentParcelForm().map(({ idparcel }) => {
        const set = setParcelFormFieldValue(prop);

        const renderActionButton = foldRemote<
            SitexFile,
            string,
            NodeOrOptional
        >(
            () =>
                getFile(idparcel).map(file =>
                    btnUploadFile(() => processUploadParcelFile(file))
                ),
            () => some(DIV('loading', tr.core('loadingData'))),
            () =>
                some(
                    DIV(
                        'error',
                        '~Error uploading',
                        btnNullifyInput(clearUploadParcelFile)
                    )
                ),
            data =>
                some(
                    DIV(
                        'success',
                        btnValidate(() => {
                            set(
                                updateParcelFile(
                                    opt.getOrElse([]).concat(fromSitex(data))
                                )
                            );
                            clearUploadParcelFile();
                            clearFile(idparcel);
                        })
                    )
                )
        );

        return DIV(
            'input input--file',
            opt.map(files => renderPreview(prop, files)),
            renderFileInput(prop, idparcel),
            DIV(
                'input__actions',
                renderActionButton(getUploadParcelFile()),
                nextProp(prop).map(next =>
                    btnNextInput(() => navigateProp(next))
                )
            )
        );
    });

export default renderFilesInput;

logger('loaded...');
