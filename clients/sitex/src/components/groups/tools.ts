import { fromNullable, none, some } from 'fp-ts/lib/Option';
import { DIV, H2 } from 'sdi/components/elements';
import { attrOptions, inputText } from 'sdi/components/input';
import tr from 'sdi/locale';
import { markdown } from 'sdi/ports/marked';
import {
    assignGroupToBuilding,
    clearBuildingGroup,
    clearBuildingGroupSelection,
    clearGroupNameInput,
    createGroup,
    navigateIndex,
    selectGroupCurrentBuilding,
    setGroupNameInput,
} from 'sitex/src/events';
import {
    BuildingFormData,
    CompressesedAddress,
    CompressesedAddressComp,
    findUrbisAddress,
    getBuildFormsForGroup,
    getCurrentGroup,
    getCurrentGroupBuildingId,
    getGroupNameInput,
} from 'sitex/src/queries';
import { BuildingGroup } from 'sitex/src/remote';
import {
    buttonClearSelectionGroups,
    buttonGroupAddOther,
    buttonHome,
    buttonRemoveBuildingFromGroup,
} from '../buttons';

const actionCreate = () =>
    DIV(
        'create-group',

        H2('', tr.sitex('groups/create')),
        inputText(
            attrOptions<string>(
                'group-name',
                getGroupNameInput,
                setGroupNameInput,
                {
                    placeholder: tr.sitex('groupNamePLaceholder'),
                }
            )
        ),
        buttonGroupAddOther(() => {
            getGroupNameInput()
                .map(createGroup)
                .map(pr =>
                    pr.then(group =>
                        getCurrentGroupBuildingId().map(id =>
                            assignGroupToBuilding(id, group.id)
                        )
                    )
                );
            clearGroupNameInput();
        })
    );

const renderCommpressedAddressComp = (addr: CompressesedAddressComp) =>
    DIV(
        'compressed-address',
        addr.comps
            .map(
                ({ streetName, numbers }) =>
                    `${streetName} ${numbers.join(' - ')}`
            )
            .join('; '),
        `, ${addr.municipality}`
    );

const renderUrbisAddress = (address: CompressesedAddress) =>
    DIV(
        `address__container  compress-${address.comps.length}`,
        DIV('', ...address.comps.map(renderCommpressedAddressComp))
    );

const renderBulding =
    (selected: string) =>
    ({ idbuild, u2building }: BuildingFormData) =>
        DIV(
            {
                key: idbuild,
                className: `building-item ${
                    selected === idbuild ? 'selected' : ''
                }`,
            },
            DIV(
                {
                    className: 'address__container',
                    onClick: () => selectGroupCurrentBuilding(idbuild),
                },
                fromNullable(u2building.value)
                    .chain(findUrbisAddress)
                    .map(renderUrbisAddress)
                    .getOrElse(DIV('sitex-id', `${idbuild}`))
            ),
            selected === idbuild
                ? some(
                      buttonRemoveBuildingFromGroup(() =>
                          clearBuildingGroup(idbuild)
                      )
                  )
                : none
        );

const renderBuildingWithGroup = (bid: string) => (group: BuildingGroup) =>
    DIV(
        'actions',
        buttonClearSelectionGroups(clearBuildingGroupSelection),
        DIV('helptext', tr.sitex('createGroupHelptextMap')),
        H2('group name', group.name),
        getBuildFormsForGroup(group.id).map(renderBulding(bid))
    );

const renderBuildingWithoutGroup = () =>
    DIV(
        'actions',
        buttonClearSelectionGroups(clearBuildingGroupSelection),
        actionCreate()
    );

const renderWithoutBuilding = () =>
    DIV('actions', markdown(tr.sitex('createGroupStart')));

const render = () => {
    const buildingId = getCurrentGroupBuildingId().toNullable();
    const group = getCurrentGroup();
    if (buildingId === null) {
        return renderWithoutBuilding();
    }

    return group.foldL(
        renderBuildingWithoutGroup,
        renderBuildingWithGroup(buildingId)
    );
};

const renderHeader = () =>
    DIV(
        'sidebar__header form__header',
        buttonHome(() => navigateIndex('building')),
        H2('subtitle', tr.sitex('groups/title'))
    );

const renderLegend = () =>
    DIV(
        'legend__wrapper',
        H2('', tr.core('legend')),
        DIV(
            'legend-body legend-groups',

            DIV(
                'legend-item polygon',
                DIV('item-style selected-feature', ''),
                DIV('item-label'),
                tr.sitex('groups/selectedFeature')
            ),
            DIV(
                'legend-item polygon',
                DIV('item-style selected-group', ''),
                DIV('item-label'),
                tr.sitex('groups/selectedGroup')
            ),
            DIV(
                'legend-item polygon',
                DIV('item-style some', ''),
                DIV('item-label'),
                tr.sitex('groups/withGroup')
            )
            // DIV(
            //     'legend-item polygon',
            //     DIV('item-style none', ''),
            //     DIV('item-label'),
            //     'WITHOUT GROUP'
            // )
        )
    );

const renderTools = () =>
    DIV('sidebar', renderHeader(), render(), renderLegend());

export default renderTools;
