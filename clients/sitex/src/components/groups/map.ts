import { Option, none, some } from 'fp-ts/lib/Option';
import { DIV } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { create, nullFeaturePath, singleSelectOptions } from 'sdi/map';

import {
    clearBuildingGroupSelection,
    selectBuildingGroup,
    updateGroupsView,
} from 'sitex/src/events';
import groups from 'sitex/src/queries/map/groups';

let mapUpdate: Option<() => void> = none;
let mapSetTarget: Option<(e: HTMLElement) => void> = none;

const attachMap = (element: HTMLElement | null) => {
    const selectOptions = singleSelectOptions({
        clearSelection: clearBuildingGroupSelection,
        getSelected: nullFeaturePath,
        selectFeature: selectBuildingGroup,
    });

    mapUpdate = mapUpdate.foldL(
        () => {
            const { update, setTarget, selectable } = create(groups.name, {
                getBaseLayer: groups.getBaseLayer,
                getView: groups.getView,
                getMapInfo: groups.getMapInfo,

                updateView: updateGroupsView,
                setScaleLine: () => void 0,

                element,
            });

            selectable(selectOptions, groups.getInteraction);

            mapSetTarget = some(setTarget);
            update();
            return some(update);
        },
        update => some(update)
    );

    if (element) {
        mapSetTarget.map(f => f(element));
    }
};

const renderMap = () => {
    mapUpdate.map(f => f());
    return DIV(
        {
            className: 'map-wrapper',
            'aria-label': `${tr.core('map')}`,
        },
        DIV({
            id: groups.name,
            key: groups.name,
            className: 'map',
            ref: attachMap,
        })
    );
};

export default renderMap;
