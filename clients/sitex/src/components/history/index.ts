import * as debug from 'debug';
import { Option, none, fromNullable, some } from 'fp-ts/lib/Option';
import { nameToString } from 'sdi/components/button/names';
import {
    DIV,
    H1,
    IMG,
    NodeOrOptional,
    SPAN,
    A,
    H2,
} from 'sdi/components/elements';
import { iife } from 'sdi/lib';
import { miniMap2 as minimap } from 'sdi/map/mini';
import tr, { formatNumber, fromRecord } from 'sdi/locale';
import { MultiPolygon } from 'sdi/source';
import { tryNumber, withM2 } from 'sdi/util';
import { navigateIndex, navigateProp } from 'sitex/src/events';
import {
    BuildingFormData,
    findOccupationByCode,
    findTypology,
    findStatecode,
    findGroup,
    getCurrentBuildingForm,
    getSurveyProp,
    getSurveyType,
    findUrbisAddress,
    ParcelFormData,
    getCurrentParcelForm,
    findUrbisAddressParcel,
    getBuildingHistory,
    getParcelHistory,
    buildingFormData,
    parcelFormData,
    CompressesedAddress,
    CompressesedAddressComp,
    findObservation,
} from 'sitex/src/queries';
import {
    DataState,
    GeoState,
    BuildingOccupation,
    BuildingProp,
    BuildingFile,
    ParcelProp,
    ParcelFile,
    ParcelOccupation,
    Building,
    Parcel,
    SurveyType,
} from 'sitex/src/remote';
import { buttonHome } from '../buttons';
import { FieldValueDescriptor, fromRawField } from '../field';
import {
    propToMessageKey,
    BuildingPropGroup,
    findBuildingPropGroup,
} from '../form/building/common';
import {
    propParcelToMessageKey,
    findParcelPropGroup,
    ParcelPropGroup,
} from '../form/parcel';

const logger = debug('sdi:building/preview');

const wrapperClassName = (
    prop: BuildingProp,
    { touched, value }: FieldValueDescriptor
) =>
    `kv__wrapper ${touched ? 'touched' : ''}  ${value
        .map(() => 'present')
        .getOrElse('missing')} ${getSurveyProp()
        .map(p => (p === prop ? 'active' : ''))
        .getOrElse('')}`;

const wrapperParcelClassName = (
    prop: ParcelProp,
    { touched, value }: FieldValueDescriptor
) =>
    `kv__wrapper ${touched ? 'touched' : ''}  ${value
        .map(() => 'present')
        .getOrElse('missing')} ${getSurveyProp()
        .map(p => (p === prop ? 'active' : ''))
        .getOrElse('')}`;

const renderString = (s: string) => DIV('value value--string', s);

const renderNumber = (n: number) => DIV('value value--number', formatNumber(n));

const renderBoolean = (b: boolean) =>
    DIV('value value--boolean', b ? tr.sitex('yes') : tr.sitex('no'));

const renderDataState = (s: DataState) => {
    const label = iife(() => {
        switch (s) {
            case 'D':
                return tr.sitex('datastateInitial');
            case 'O':
                return tr.sitex('datastateOngoing');
            case 'V':
                return tr.sitex('datastateValidated');
        }
    });
    return DIV('value value--data-state', label);
};

const renderGeoState = (s: GeoState) => {
    const label = iife(() => {
        switch (s) {
            case 'D':
                return tr.sitex('geostateDraft');
            case 'O':
                return tr.sitex('geostateOngoing');
            case 'V':
                return tr.sitex('geostateValidated');
        }
    });
    return DIV('value value--geo-state', label);
};

const renderLevel = (occup: BuildingOccupation) =>
    DIV(
        `occup-summary `,
        DIV('level', occup.level),
        DIV(
            'code',
            fromNullable(occup.occupcode)
                .chain(findOccupationByCode)
                .map<string | null>(({ name }) => fromRecord(name))
                .getOrElse(occup.occupcode)
        ),
        tryNumber(occup.area).map(area => DIV('area', withM2(area)))
    );

const renderOccupation = (occups: BuildingOccupation[]) =>
    DIV('occup-summary__wrapper', ...occups.map(renderLevel).reverse());

const renderParcelLevel = (occup: ParcelOccupation) =>
    DIV(
        `occup-summary `,
        DIV(
            'code',
            fromNullable(occup.occupcode)
                .chain(findOccupationByCode)
                .map<string | null>(({ name }) => fromRecord(name))
                .getOrElse(occup.occupcode)
        ),
        tryNumber(occup.area).map(area => DIV('area', withM2(area)))
    );

const renderParcelOccupation = (occups: ParcelOccupation[]) =>
    DIV('occup-summary__wrapper', ...occups.map(renderParcelLevel).reverse());

const renderTypology = (i: number) =>
    findTypology(i).map(({ name }) =>
        DIV('value value--string', fromRecord(name))
    );

const renderStatecode = (i: number) =>
    findStatecode(i).map(({ name }) =>
        DIV('value value--string', fromRecord(name))
    );

const renderObservation = (i: number) =>
    findObservation(i).map(({ name }) =>
        DIV('value value--string', fromRecord(name))
    );

// const renderFilePreview = ({ file }: BuildingFile) => {
//     const image_url = file.images?.small ? file.images.small : file.url;
//     return SPAN(
//         { className: 'field--image__thumbnail' },
//         IMG({
//             className: 'field--image__thumbnail__img',
//             src: image_url || '#',
//             alt: file.name || '#',
//             style: {
//                 width: '32px',
//                 height: '32px',
//             },
//             onError: () => {
//                 logger('error loading image', file);
//             },
//         })
//     );
// };

// const renderDocumentIcon = ({ file }: BuildingFile) => {
//     const filetype = file?.name ? file.name.split('.').pop() : '';
//     return SPAN(
//         {
//             className: 'field--doc__thumbnail',
//             style: {
//                 minWidth: '32px',
//                 minHeight: '32px',
//                 textAlign: 'center',
//                 position: 'relative',
//             },
//         },
//         SPAN(
//             {
//                 className: 'fa fa-file',
//                 style: { position: 'inherit', top: '30%', padding: '2px 2px' },
//             },
//             A(
//                 {
//                     href: file.url || '#',
//                     target: '_blank',
//                     title: file.name || '#',
//                 },
//                 P({}, filetype)
//             )
//         )
//     );
// };

const renderImage = ({ file }: BuildingFile) => {
    const image_url = file.images?.small ? file.images.small : file.url;
    return DIV(
        'field--image',
        IMG({
            src: image_url || '#',
            alt: file.name || '#',
            // style: {
            //     width: '64px',
            //     height: '64px',
            // },
            onError: () => {
                logger('error loading image', file);
            },
        })
    );
};

const renderDocument = ({ file }: BuildingFile) => {
    const filename = fromNullable(file.name).getOrElse('');
    const filetype = fromNullable(filename.split('.').pop()).getOrElse('');
    const trimmedName =
        filename.length > 25 + filetype.length
            ? `${filename.slice(0, 20)}(...).${filetype}`
            : filename;

    return DIV(
        'field--doc',
        A(
            {
                href: file.url || '#',
                target: '_blank',
                title: file.name || '#',
            },
            SPAN('icon', nameToString('file')),
            SPAN({}, trimmedName)
        )
    );
};

const renderFileThumbnail = (file: BuildingFile) => {
    const content_type = file.file.type ? file.file.type.split('/')[0] : '';
    switch (content_type) {
        case 'image':
            return renderImage(file);
        case 'application':
            return renderDocument(file);
        default:
            return DIV({ className: 'field--image__thumbnail' });
    }
};

const renderFile = (files: BuildingFile[]) =>
    DIV('files-summary__wrapper', ...files.map(renderFileThumbnail).reverse());

const renderMiniMap = minimap(
    'https://geoservices-urbis.irisnet.be/geoserver/ows',
    {
        LAYERS: 'urbisFRGray',
    }
);

const renderGeometry = (geom: MultiPolygon) => {
    const step = renderMiniMap(geom);
    switch (step.step) {
        case 'failed':
            return none;
        case 'loading':
            return some(DIV('minimap-loading', tr.sitex('loading')));
        case 'loaded':
            return some(DIV('value minimap-loaded', IMG({ src: step.data })));
    }
};

const renderParcelImage = ({ file }: ParcelFile) => {
    const image_url = file.images?.small ? file.images.small : file.url;
    return DIV(
        'field--image',
        IMG({
            src: image_url || '#',
            alt: file.name || '#',
            // style: {
            //     width: '64px',
            //     height: '64px',
            // },
            onError: () => {
                logger('error loading image', file);
            },
        })
    );
};

const renderParcelDocument = ({ file }: ParcelFile) => {
    const filename = fromNullable(file.name).getOrElse('');
    const filetype = fromNullable(filename.split('.').pop()).getOrElse('');
    const trimmedName =
        filename.length > 25 + filetype.length
            ? `${filename.slice(0, 20)}(...).${filetype}`
            : filename;

    return DIV(
        'field--doc',
        A(
            {
                href: file.url || '#',
                target: '_blank',
                title: file.name || '#',
            },
            SPAN('icon', nameToString('file')),
            SPAN({}, trimmedName)
        )
    );
};

const renderParcelFileThumbnail = (file: ParcelFile) => {
    const content_type = file.file.type ? file.file.type.split('/')[0] : '';
    switch (content_type) {
        case 'image':
            return renderParcelImage(file);
        case 'application':
            return renderParcelDocument(file);
        default:
            return DIV({ className: 'field--image__thumbnail' });
    }
};

const renderParcelFiles = (files: ParcelFile[]) =>
    DIV(
        'files-summary__wrapper',
        ...files.map(renderParcelFileThumbnail).reverse()
    );

const renderBuildingGroup = (group_id: string) =>
    findGroup(group_id).map(({ name }) => DIV('value value--string', name));

const renderValue = (d: FieldValueDescriptor): Option<React.ReactNode> => {
    switch (d.tag) {
        case 'string':
            return d.value.map(renderString);
        case 'number':
            return d.value.map(renderNumber);
        case 'boolean':
            return d.value.map(renderBoolean);
        case 'datastate':
            return d.value.map(renderDataState);
        case 'geometry':
            return d.value.map(renderGeometry);
        case 'buildgroup':
            return d.value.map(renderBuildingGroup);
        case 'geostate':
            return d.value.map(renderGeoState);
        case 'recordstate':
            return none;
        case 'dict-typo':
            return d.value.map(renderTypology);
        case 'dict-statecode':
            return d.value.map(renderStatecode);
        case 'dict-observation':
            return d.value.map(renderObservation);
        case 'dict-update':
            return none;
        case 'occupation-building':
            return d.value.map(renderOccupation);
        case 'files-building':
            return d.value.map(renderFile);
        case 'occupation-parcel':
            return d.value.map(renderParcelOccupation);
        case 'files-parcel':
            return d.value.map(renderParcelFiles);
    }
};

const infoItem = (prop: BuildingProp, data: BuildingFormData) => {
    const field = fromRawField(data[prop]);
    return DIV(
        {
            className: wrapperClassName(prop, field),
            onClick: () => navigateProp(prop),
        },
        DIV('key', tr.sitex(propToMessageKey(prop))),
        renderValue(field)
    );
};

const infoParcelItem = (prop: ParcelProp, data: ParcelFormData) => {
    const field = fromRawField(data[prop]);
    return DIV(
        {
            className: wrapperParcelClassName(prop, field),
            onClick: () => navigateProp(prop),
        },
        DIV('key', tr.sitex(propParcelToMessageKey(prop))),
        renderValue(field)
    );
};

const renderGoup =
    (form: BuildingFormData) =>
    ({ props }: BuildingPropGroup) =>
        DIV('infos__group', ...props.map(prop => infoItem(prop, form)));

const renderParcelGroup =
    (form: ParcelFormData) =>
    ({ props }: ParcelPropGroup) =>
        DIV('infos__group', ...props.map(prop => infoParcelItem(prop, form)));

const renderCommpressedAddressComp = (addr: CompressesedAddressComp) =>
    DIV(
        'compressed-address',
        addr.comps
            .map(
                ({ streetName, numbers }) =>
                    `${streetName} ${numbers.join(' - ')}`
            )
            .join('; '),
        `, ${addr.municipality}`
    );

const renderUrbisAddress = (address: CompressesedAddress) =>
    DIV(
        `address__container  compress-${address.comps.length}`,
        H1('', ...address.comps.map(renderCommpressedAddressComp)),
        DIV(
            'capakey',
            `${tr.sitex('localCadastralplot')} : ${address.capakeys.join('; ')}`
        )
    );

const infosBuildingHeader = (form: BuildingFormData) =>
    DIV(
        'address__container',

        fromNullable(form.u2building.value)
            .chain(findUrbisAddress)
            .map(renderUrbisAddress),

        DIV('building-id', `${tr.sitex('sitexID')} : ${form.idbuild}`)
    );

const infosBuildingWrapper = (form: BuildingFormData) =>
    DIV(
        'infos building',
        findBuildingPropGroup('general').map(renderGoup(form)),
        findBuildingPropGroup('secondary').map(renderGoup(form)),
        findBuildingPropGroup('details').map(renderGoup(form))
    );

const renderBuildingInfos = () =>
    getCurrentBuildingForm().map(form =>
        DIV('infos__header', infosBuildingHeader(form))
    );

const renderBuilding = () =>
    getCurrentBuildingForm().map(form =>
        DIV('infos__wrapper history building', infosBuildingWrapper(form))
    );

//////////////////////  PARCEL

const infosParcelHeader = (form: ParcelFormData) =>
    DIV(
        'address__container',

        fromNullable(form.u2parcel.value)
            .chain(findUrbisAddressParcel)
            .map(renderUrbisAddress),

        DIV('parcel-id', `id : ${form.idparcel}`)
    );

const infosParcelWrapper = (form: ParcelFormData) =>
    DIV(
        'infos parcel',
        findParcelPropGroup('general').map(renderParcelGroup(form)),
        findParcelPropGroup('secondary').map(renderParcelGroup(form)),
        findParcelPropGroup('details').map(renderParcelGroup(form))
    );

const renderParcelInfos = () =>
    getCurrentParcelForm().map(form =>
        DIV('infos__header', infosParcelHeader(form))
    );

const renderParcel = () =>
    getCurrentParcelForm().map(form =>
        DIV('infos__wrapper history parcel', infosParcelWrapper(form))
    );

// const renderParcel = () => getCurrentParcelForm().map(infosParcelWrapper);

////// public space

const renderPublicSpace = () => DIV('', '~Not Implemented');

//////// General Interface

const surveyTypeHistoryTitle = () =>
    getSurveyType().map(surveyType => {
        switch (surveyType) {
            case 'building':
                return tr.sitex('historyBuildingTitle');
            case 'parcel':
                return tr.sitex('historyParcelTitle');
            case 'public-space':
                return tr.sitex('publicSpaceSheetTitle');
        }
    });

const headerHistory = (surveyType: SurveyType, ...nodes: NodeOrOptional[]) =>
    DIV(
        'history__header',
        DIV(
            'title__wrapper',
            buttonHome(() => navigateIndex(surveyType)),
            H2('subtitle', surveyTypeHistoryTitle())
        ),
        ...nodes
    );

const renderHeaderHistory = () =>
    getSurveyType().map(surveyType => {
        switch (surveyType) {
            case 'building':
                return headerHistory(surveyType, renderBuildingInfos());
            case 'parcel':
                return headerHistory(surveyType, renderParcelInfos());
            case 'public-space':
                return headerHistory(surveyType, renderPublicSpace());
        }
    });

const sidebar = (...nodes: NodeOrOptional[]) =>
    DIV('sidebar infos__wrapper', ...nodes);

const renderSidebar = () =>
    getSurveyType().map(surveyType => {
        switch (surveyType) {
            case 'building':
                return sidebar(renderBuilding());
            case 'parcel':
                return sidebar(renderParcel());
            case 'public-space':
                return sidebar(renderPublicSpace());
        }
    });

const renderBuildingHistory = (buildings: Building[]) =>
    DIV(
        'infos__wrapper history',
        H2('subtitle', tr.sitex('versionsTitle')),
        DIV(
            'versions',
            ...buildings.map(buildingFormData).map(data =>
                DIV(
                    'version',
                    DIV(
                        'version-header',
                        fromRawField(data.startdate).value.map(s =>
                            DIV('start', s)
                        ),
                        fromRawField(data.enddate).value.map(e => DIV('end', e))
                    ),
                    infosBuildingWrapper(data)
                )
            )
        )
    );

const renderParcelHistory = (parcels: Parcel[]) =>
    DIV(
        'infos__wrapper history',
        H2('subtitle', tr.sitex('versionsTitle')),
        DIV(
            'versions',
            ...parcels.map(parcelFormData).map(data =>
                DIV(
                    'version',
                    DIV(
                        'version-header',
                        fromRawField(data.startdate).value.map(s =>
                            DIV('start', s)
                        ),
                        fromRawField(data.enddate).value.map(e => DIV('end', e))
                    ),
                    infosParcelWrapper(data)
                )
            )
        )
    );

const renderHistoryItems = () =>
    getSurveyType().chain(st => {
        switch (st) {
            case 'building':
                return getBuildingHistory().map(renderBuildingHistory);
            case 'parcel':
                return getParcelHistory().map(renderParcelHistory);
            case 'public-space':
                return none;
        }
    });

export const renderHistory = () =>
    DIV(
        'history__content',
        renderHeaderHistory(),
        DIV('history__body', renderSidebar(), renderHistoryItems())
    );

export default renderHistory;
