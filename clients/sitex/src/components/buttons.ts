import { makeIcon, makeLabel, makeLabelAndIcon } from 'sdi/components/button';
import tr from 'sdi/locale';

export const btnValidate = makeLabelAndIcon('validate', 1, 'save', () =>
    tr.sitex('validateObservation')
);

export const buttonRevert = makeIcon('clear', 3, 'undo', {
    text: () => tr.sitex('revert'),
    position: 'left',
});

export const btnUploadFile = makeLabelAndIcon('upload', 1, 'upload', () =>
    tr.sitex('uploadFile')
);

export const btnNextInput = makeLabelAndIcon('next', 1, 'chevron-right', () =>
    tr.sitex('nextInput')
);
export const btnNullifyInput = makeLabelAndIcon('clear', 2, 'times', () =>
    tr.core('clear')
);

export const btnSelectInfo = makeLabelAndIcon('next', 2, 'chevron-up', () =>
    tr.sitex('selectInfo')
);

export const buttonAddOccupParcel = makeLabelAndIcon('add', 2, 'plus', () =>
    tr.sitex('addOccupancy')
);
export const buttonAddOccup = makeLabelAndIcon('add', 2, 'plus', () =>
    tr.sitex('addLevel')
);

export const buttonAddOccupOnLevel = makeLabelAndIcon('add', 2, 'plus', () =>
    tr.sitex('addPartLevel')
);

export const buttonCloneOccup = makeLabelAndIcon('add', 2, 'copy', () =>
    tr.sitex('clone-occup')
);

export const buttonAddLocal = makeLabelAndIcon('add', 2, 'plus', () =>
    tr.core('add')
);
export const buttonCloneLocal = makeLabelAndIcon('add', 2, 'copy', () =>
    tr.sitex('clone-local')
);

export const buttonSyncElements = makeLabelAndIcon('upload', 1, 'sync', () =>
    tr.sitex('syncElements')
);

export const buttonEdit = makeLabelAndIcon('edit', 1, 'pencil-alt', () =>
    tr.core('edit')
);

export const buttonClearStatus = makeLabelAndIcon('clear', 2, 'times', () =>
    tr.sitex('clear-status')
);

export const buttonPrint = makeLabelAndIcon('navigate', 2, 'print', () =>
    tr.core('print')
);
export const buttonHistory = makeLabelAndIcon('navigate', 2, 'history', () =>
    tr.sitex('history')
);

export const buttonHome = makeLabelAndIcon('navigate', 2, 'chevron-left', () =>
    tr.sitex('home')
);

export const buttonPreview = makeLabelAndIcon('navigate', 2, 'eye', () =>
    tr.sitex('navigatePreview')
);

export const buttonRemoveStatus = makeIcon('clear', 3, 'times', {
    text: () => tr.sitex('remove-status'),
    position: 'left',
});

// const btnPreviousInput = () =>
//     makeLabelAndIcon('prev', 2, 'chevron-left', () => tr.sitex('prevInput'))(noop);

// const btnNextInput = () =>
//     makeLabelAndIcon('next', 2, 'chevron-right', () => tr.sitex('nextInput'))(noop);

export const buttonSwitchModeBuilding = makeLabel('switch', 2, () =>
    tr.sitex('mode-building')
);

export const buttonSwitchModeParce = makeLabel('switch', 2, () =>
    tr.sitex('mode-parcel')
);

export const buttonSwitchModePublicSpace = makeLabel('switch', 2, () =>
    tr.sitex('mode-public-space')
);

export const buttonDeleteFile = makeIcon('clear', 3, 'times', {
    text: () => tr.sitex('btnDeleteFile'),
    position: 'top',
});

export const buttonDeleteOccupation = makeIcon('clear', 3, 'times', {
    text: () => tr.sitex('btnDeleteOccupation'),
    position: 'left',
});

export const buttonCreateBuildingOn = makeLabelAndIcon('add', 2, 'plus', () =>
    tr.sitex('btnAddGeometryBuilding')
);

export const buttonCreateParcelOn = makeLabelAndIcon('add', 2, 'plus', () =>
    tr.sitex('btnAddGeometryParcel')
);

export const buttonCreatePublicSpaceOn = makeLabelAndIcon(
    'add',
    2,
    'plus',
    () => tr.sitex('btnAddGeometryPublicSpace')
);

export const buttonCreateGeometryOff = makeLabel('cancel', 2, () =>
    tr.core('cancel')
);

export const buttonConfirmCreateBuilding = makeLabel('validate', 1, () =>
    tr.sitex('buttonConfirmCreateBuilding')
);

export const buttonConfirmCreateParcel = makeLabel('validate', 1, () =>
    tr.sitex('buttonConfirmCreateParcel')
);

export const buttonConfirmCreatePublicSpace = makeLabel('validate', 1, () =>
    tr.sitex('buttonConfirmCreatePublicSpace')
);

export const buttonGroupAddOther = makeLabelAndIcon(
    'validate',
    1,
    'check',
    () => tr.sitex('buttonGroupAddOther')
);

export const buttonStartMeasureLine = makeLabel('start', 2, () =>
    tr.sitex('measure/start/line')
);

export const buttonStartMeasurePolygon = makeLabel('start', 2, () =>
    tr.sitex('measure/start/polygon')
);

export const buttonStopMeasure = makeLabel('cancel', 1, () =>
    tr.sitex('measure/stop')
);

export const buttonNavigateGroups = makeLabel('navigate', 2, () =>
    tr.sitex('navigate/goups')
);

export const buttonClearSelectionGroups = makeLabel('clear', 2, () =>
    tr.sitex('groups/clear-selection')
);

export const buttonRemoveBuildingFromGroup = makeLabelAndIcon(
    'clear',
    1,
    'trash-alt',
    () => tr.sitex('groups/remove')
);

export type ButtonType = ReturnType<typeof makeLabel>;
