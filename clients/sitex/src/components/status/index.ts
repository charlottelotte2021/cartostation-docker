import { fromPredicate } from 'fp-ts/lib/Option';
import { DIV } from 'sdi/components/elements';
import { clearStatus, removeStatus } from 'sitex/src/events/status';
import { getStatus } from 'sitex/src/queries';
import { buttonClearStatus, buttonRemoveStatus } from '../buttons';

export type StatusInfo = {
    tag: 'info';
    content: string;
};

export type StatusError = {
    tag: 'error';
    content: string;
};

export type Status = StatusInfo | StatusError;

export const info = (content: string): StatusInfo => ({
    tag: 'info',
    content,
});

export const error = (content: string): StatusError => ({
    tag: 'error',
    content,
});

const renderInfo = ({ content }: StatusInfo, i: number) =>
    DIV(
        'notification status-info',
        content,
        buttonRemoveStatus(() => removeStatus(i))
    );

const renderError = ({ content }: StatusError, i: number) =>
    DIV(
        'notification status-error',
        content,
        buttonRemoveStatus(() => removeStatus(i))
    );

const renderStatus = (s: Status, i: number) => {
    switch (s.tag) {
        case 'info':
            return renderInfo(s, i);
        case 'error':
            return renderError(s, i);
    }
};

const withStatus = fromPredicate<Status[] | Readonly<Status[]>>(
    ss => ss.length > 0
);

export const renderStatusBar = () =>
    withStatus(getStatus()).map(ss =>
        DIV(
            'status-bar',
            DIV('notifications', ...ss.map(renderStatus)),
            DIV('actions', buttonClearStatus(clearStatus))
        )
    );

export default renderStatusBar;
