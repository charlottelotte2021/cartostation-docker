import { DIV, H1, H2 } from 'sdi/components/elements';
import tr, { formatNumber } from 'sdi/locale';
import { navigateGroups, syncAll } from '../events';
import {
    getOnlineQuality,
    getSitexBuildings,
    getSitexParcels,
    getSurveyType,
    getTouchedBuildings,
    getTouchedParcels,
    getUrbisBuildings,
    getUrbisParcels,
} from '../queries';
import { buttonNavigateGroups, buttonSyncElements } from './buttons';
import mainMap from './main-map';
import { markdown } from 'sdi/ports/marked';

const renderBuildingLegend = () =>
    DIV(
        'legend-item polygon',
        DIV('item-style urbis', ''),
        DIV('item-label'),
        tr.sitex('UrbisBuildings')
    );

const renderParcelLegend = () =>
    DIV(
        'legend-item polygon',
        DIV('item-style urbis-parcel', ''),
        DIV('item-label'),
        tr.sitex('UrbisParcels')
    );

const renderLegend = () =>
    DIV(
        'legend__wrapper',
        H2('', tr.core('legend')),
        DIV(
            'legend-body',
            getSurveyType().map(surveyType => {
                switch (surveyType) {
                    case 'building':
                        return renderBuildingLegend();
                    case 'parcel':
                        return renderParcelLegend();
                    case 'public-space':
                        // TODO
                        return '';
                }
            }),
            DIV(
                'legend-item polygon',
                DIV('item-style synced', ''),
                DIV('item-label'),
                tr.sitex('SyncedElements')
            ),
            DIV(
                'legend-item polygon',
                DIV('item-style not-yet-synced-d', ''),
                DIV('item-label'),
                tr.sitex('not-yet-syncedElements-d')
            ),
            DIV(
                'legend-item polygon',
                DIV('item-style not-yet-synced-o', ''),
                DIV('item-label'),
                tr.sitex('not-yet-syncedElements-o')
            ),
            DIV(
                'legend-item polygon',
                DIV('item-style not-yet-synced-v', ''),
                DIV('item-label'),
                tr.sitex('not-yet-syncedElements-v')
            )
        )
    );

const noBlockSelected = () =>
    DIV('summary summary--empty', markdown(tr.sitex('noBlockSelected')));

const globalBuildingSummary = () =>
    DIV(
        'summary summary--global',
        H2('', tr.sitex('SelectionSummary')),
        // DIV(
        //     'kv__wrapper',
        //     DIV('key', '~Number of buildings to be synced: '),
        //     DIV('value', formatNumber(getTouchedBuildings().length))
        // ),
        DIV(
            'kv__wrapper',
            DIV('key', tr.sitex('NumberOfLoadedSitexBuildings')),
            DIV('value', formatNumber(getSitexBuildings().length))
        ),
        DIV(
            'kv__wrapper',
            DIV('key', tr.sitex('NumberOfLoadedUrbisBuildings')),
            DIV('value', formatNumber(getUrbisBuildings().length))
        )
    );

const globalParcelSummary = () =>
    DIV(
        'summary summary--global',
        H2('', tr.sitex('SelectionSummary')),
        DIV(
            'kv__wrapper',
            DIV('key', tr.sitex('NumberOfLoadedSitexParcels')),
            DIV('value', formatNumber(getSitexParcels().length))
        ),
        DIV(
            'kv__wrapper',
            DIV('key', tr.sitex('NumberOfLoadedUrbisParcels')),
            DIV('value', formatNumber(getUrbisParcels().length))
        )
    );

const globalPublicSpaceSummary = () =>
    DIV(
        'summary summary--global',
        H2('', tr.sitex('SelectionSummary'))
        // TODO
    );

const globalSummary = () =>
    getSurveyType().map(surveyType => {
        switch (surveyType) {
            case 'building':
                return globalBuildingSummary();
            case 'parcel':
                return globalParcelSummary();
            case 'public-space':
                return globalPublicSpaceSummary();
        }
    });

const renderGlobalSummary = () =>
    getUrbisBuildings().length > 0 || getUrbisParcels().length > 0
        ? globalSummary()
        : noBlockSelected();

const renderNetworkStatus = () => {
    switch (getOnlineQuality()) {
        case 'good':
            return DIV(
                'network-status status-good',
                // tr.sitex('statusOnlineGood'),
                buttonSyncElements(syncAll)
            );
        case 'medium':
            return DIV(
                'network-status status-medium',
                // tr.sitex('statusOnlineMedium'),
                buttonSyncElements(syncAll)
            );
        case 'bad':
            return DIV(
                'network-status status-bad',
                tr.sitex('statusOnlineBadSync')
                // buttonSyncElements(syncBuildings)
            );
        case 'critical':
            return DIV(
                'network-status status-critical',
                tr.sitex('statusOfflineSync')
                // buttonSyncElements(syncBuildings)
            );
    }
};

const renderSyncBlock = () =>
    DIV(
        'sync__wrapper',
        H2('', tr.sitex('ToBeSynchronized')),
        // renderNetworkStatus(),
        DIV(
            'sync__content',
            DIV(
                'kv__wrapper sync-item building',
                DIV('key', tr.sitex('buildings')),
                DIV('value', formatNumber(getTouchedBuildings().length))
            ),
            DIV(
                'kv__wrapper sync-item building',
                DIV('key', tr.sitex('parcels')),
                DIV('value', formatNumber(getTouchedParcels().length))
            ),
            DIV(
                'kv__wrapper sync-item building',
                DIV('key', tr.sitex('publicSpaces')),
                DIV('value', '0')
            )
            // buttonSyncElements(syncBuildings)
        ),
        renderNetworkStatus()
    );

const renderSyncWrapper = () =>
    getTouchedBuildings().length > 0 || getTouchedParcels().length > 0
        ? renderSyncBlock()
        : '';

const renderSidebar = () =>
    DIV(
        'sidebar',
        H1('', tr.sitex('sitexName')),
        renderGlobalSummary(),
        renderSyncWrapper(),
        renderLegend(),
        DIV('group-manager', buttonNavigateGroups(navigateGroups))
    );

const render = () => DIV('index', renderSidebar(), mainMap());

export default render;
