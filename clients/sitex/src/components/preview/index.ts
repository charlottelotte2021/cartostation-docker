import * as debug from 'debug';
import { Option, none, fromNullable, some } from 'fp-ts/lib/Option';
import { nameToString } from 'sdi/components/button/names';
import {
    DIV,
    H1,
    IMG,
    NodeOrOptional,
    SPAN,
    A,
    H2,
} from 'sdi/components/elements';
import { iife } from 'sdi/lib';
import tr, { formatNumber, fromRecord } from 'sdi/locale';
import { MultiPolygon } from 'sdi/source';
import { miniMap2 as minimap } from 'sdi/map/mini';
import { tryNumber, withM2 } from 'sdi/util';
import {
    loadBuildingHistory,
    loadParcelHistory,
    navigateForm,
    navigateHistory,
    navigateIndex,
    navigateProp,
} from 'sitex/src/events';
import {
    BuildingFormData,
    findOccupationByCode,
    findTypology,
    findStatecode,
    findGroup,
    getCurrentBuildingForm,
    getFormId,
    getSurveyProp,
    getSurveyType,
    findUrbisAddress,
    ParcelFormData,
    getCurrentParcelForm,
    compressBuilding,
    CompressesedAddressComp,
    findObservation,
} from 'sitex/src/queries';
import {
    DataState,
    GeoState,
    BuildingOccupation,
    BuildingProp,
    SurveyType,
    BuildingFile,
    ParcelProp,
    ParcelFile,
    ParcelOccupation,
} from 'sitex/src/remote';
import { buttonEdit, buttonHistory, buttonHome, buttonPrint } from '../buttons';
import { FieldValueDescriptor, fromRawField } from '../field';
import {
    propToMessageKey,
    BuildingPropGroup,
    findBuildingPropGroup,
} from '../form/building/common';
import {
    propParcelToMessageKey,
    findParcelPropGroup,
    ParcelPropGroup,
} from '../form/parcel';

// import renderMap from './map';
import renderMap from '../main-map';

const logger = debug('sdi:building/preview');

export const sortParcelFilesByContentType = (files: ParcelFile[]) => {
    const images: ParcelFile[] = [];
    const others: ParcelFile[] = [];
    fromNullable(files)
        .getOrElse([])
        .forEach(f => {
            if (fromNullable(f.file.type).getOrElse('').startsWith('image')) {
                images.push(f);
            } else {
                others.push(f);
            }
        });

    return images.concat(others);
};

export const sortBuildingFilesByContentType = (files: BuildingFile[]) => {
    const images: BuildingFile[] = [];
    const others: BuildingFile[] = [];
    fromNullable(files)
        .getOrElse([])
        .forEach(f => {
            if (fromNullable(f.file.type).getOrElse('').startsWith('image')) {
                images.push(f);
            } else {
                others.push(f);
            }
        });
    return images.concat(others);
};

export const sortOccupationByLevel = (occups: BuildingOccupation[]) =>
    occups.sort(
        (a, b) =>
            fromNullable(a.level).getOrElse(0) -
            fromNullable(b.level).getOrElse(0)
    );

const wrapperClassName = (
    prop: BuildingProp,
    { touched, value }: FieldValueDescriptor
) =>
    `kv__wrapper ${touched ? 'touched' : ''}  ${value
        .map(() => 'present')
        .getOrElse('missing')} ${getSurveyProp()
        .map(p => (p === prop ? 'active' : ''))
        .getOrElse('')}`;

const wrapperParcelClassName = (
    prop: ParcelProp,
    { touched, value }: FieldValueDescriptor
) =>
    `kv__wrapper ${touched ? 'touched' : ''}  ${value
        .map(() => 'present')
        .getOrElse('missing')} ${getSurveyProp()
        .map(p => (p === prop ? 'active' : ''))
        .getOrElse('')}`;

const renderString = (s: string) => DIV('value value--string', s);

const renderNumber = (n: number) => DIV('value value--number', formatNumber(n));

const renderBoolean = (b: boolean) =>
    DIV('value value--boolean', b ? tr.sitex('yes') : tr.sitex('no'));

const renderDataState = (s: DataState) => {
    const label = iife(() => {
        switch (s) {
            case 'D':
                return tr.sitex('datastateInitial');
            case 'O':
                return tr.sitex('datastateOngoing');
            case 'V':
                return tr.sitex('datastateValidated');
        }
    });
    return DIV('value value--data-state', label);
};

const renderGeoState = (s: GeoState) => {
    const label = iife(() => {
        switch (s) {
            case 'D':
                return tr.sitex('geostateDraft');
            case 'O':
                return tr.sitex('geostateOngoing');
            case 'V':
                return tr.sitex('geostateValidated');
        }
    });
    return DIV('value value--geo-state', label);
};

const renderBuildingOccupationInfo = (
    classname: string,
    level: string | number | null,
    occupcode: string | null,
    area: number | null
) =>
    DIV(
        classname,
        DIV('level', level),
        DIV(
            'code',
            fromNullable(occupcode)
                .chain(findOccupationByCode)
                .map<string | null>(({ name }) => fromRecord(name))
                .getOrElse(occupcode)
        ),
        tryNumber(area).map(area => DIV('area', withM2(area)))
    );

const renderBuildingOccupationSummary = (compressed: BuildingOccupation[]) => {
    if (compressed.length === 1) {
        return renderBuildingOccupationInfo(
            'occup-summary',
            compressed[0].level,
            compressed[0].occupcode,
            compressed[0].area
        );
    }
    const levelRange = { low: Number.MAX_VALUE, high: Number.MIN_VALUE };
    let area = 0;
    let occupcode = '-';
    for (const occup of compressed) {
        if (occup.occupcode) {
            occupcode = occup.occupcode;
        }
        if (typeof occup.level === 'number') {
            levelRange.low = Math.min(levelRange.low, occup.level);
            levelRange.high = Math.max(levelRange.high, occup.level);
        }
        if (typeof occup.area === 'number') {
            area += occup.area;
        }
    }
    const { low, high } = levelRange;
    const level = low < Number.MAX_VALUE ? `${low}..${high}` : '-';
    return renderBuildingOccupationInfo(
        'occup-summary compressed',
        level,
        occupcode,
        area
    );
};

export const renderBuildingOccupation = (occups: BuildingOccupation[]) =>
    DIV(
        'occup-summary__wrapper',
        ...compressBuilding(sortOccupationByLevel(occups))
            .map(renderBuildingOccupationSummary)
            .reverse()
    );

const renderParcelOccupationSummary = (occup: ParcelOccupation) =>
    DIV(
        `occup-summary `,
        DIV(
            'code',
            fromNullable(occup.occupcode)
                .chain(findOccupationByCode)
                .map<string | null>(({ name }) => fromRecord(name))
                .getOrElse(occup.occupcode)
        ),
        tryNumber(occup.area).map(area => DIV('area', withM2(area)))
    );

export const renderParcelOccupation = (occups: ParcelOccupation[]) =>
    DIV(
        'occup-summary__wrapper',
        ...occups.map(renderParcelOccupationSummary).reverse()
    );

const renderTypology = (i: number) =>
    findTypology(i).map(({ name }) =>
        DIV('value value--string', fromRecord(name))
    );

const renderStatecode = (i: number) =>
    findStatecode(i).map(({ name }) =>
        DIV('value value--string', fromRecord(name))
    );

const renderObservation = (i: number) =>
    findObservation(i).map(({ name }) =>
        DIV('value value--string', fromRecord(name))
    );

// const renderFilePreview = ({ file }: BuildingFile) => {
//     const image_url = file.images?.small ? file.images.small : file.url;
//     return SPAN(
//         { className: 'field--image__thumbnail' },
//         IMG({
//             className: 'field--image__thumbnail__img',
//             src: image_url || '#',
//             alt: file.name || '#',
//             style: {
//                 width: '32px',
//                 height: '32px',
//             },
//             onError: () => {
//                 logger('error loading image', file);
//             },
//         })
//     );
// };

// const renderDocumentIcon = ({ file }: BuildingFile) => {
//     const filetype = file?.name ? file.name.split('.').pop() : '';
//     return SPAN(
//         {
//             className: 'field--doc__thumbnail',
//             style: {
//                 minWidth: '32px',
//                 minHeight: '32px',
//                 textAlign: 'center',
//                 position: 'relative',
//             },
//         },
//         SPAN(
//             {
//                 className: 'fa fa-file',
//                 style: { position: 'inherit', top: '30%', padding: '2px 2px' },
//             },
//             A(
//                 {
//                     href: file.url || '#',
//                     target: '_blank',
//                     title: file.name || '#',
//                 },
//                 P({}, filetype)
//             )
//         )
//     );
// };

const renderBuildingImage = ({ file }: BuildingFile) => {
    const image_url = file.images?.small ? file.images.small : file.url;
    return DIV(
        'field--image',
        IMG({
            src: image_url || '#',
            alt: file.name || '#',
            // style: {
            //     width: '64px',
            //     height: '64px',
            // },
            onError: () => {
                logger('error loading image', file);
            },
        })
    );
};

const renderBuildingDocument = ({ file }: BuildingFile) => {
    const filename = fromNullable(file.name).getOrElse('');
    const filetype = fromNullable(filename.split('.').pop()).getOrElse('');
    const trimmedName =
        filename.length > 25 + filetype.length
            ? `${filename.slice(0, 20)}(...).${filetype}`
            : filename;

    return DIV(
        'field--doc',
        A(
            {
                href: file.url || '#',
                target: '_blank',
                title: file.name || '#',
            },
            SPAN('icon', nameToString('file')),
            SPAN({}, trimmedName)
        )
    );
};

const renderFileThumbnail = (file: BuildingFile) => {
    const content_type = file.file.type ? file.file.type.split('/')[0] : '';
    switch (content_type) {
        case 'image':
            return renderBuildingImage(file);
        case 'application':
            return renderBuildingDocument(file);
        default:
            return DIV({ className: 'field--image__thumbnail' });
    }
};

const renderFile = (files: BuildingFile[]) => {
    const sorted_files = sortBuildingFilesByContentType(files);
    return DIV(
        'files-summary__wrapper',
        ...sorted_files.map(renderFileThumbnail)
    );
};

const renderParcelImage = ({ file }: ParcelFile) => {
    const image_url = file.images?.small ? file.images.small : file.url;
    return DIV(
        'field--image',
        IMG({
            src: image_url || '#',
            alt: file.name || '#',
            // style: {
            //     width: '64px',
            //     height: '64px',
            // },
            onError: () => {
                logger('error loading image', file);
            },
        })
    );
};

const renderParcelDocument = ({ file }: ParcelFile) => {
    const filename = fromNullable(file.name).getOrElse('');
    const filetype = fromNullable(filename.split('.').pop()).getOrElse('');
    const trimmedName =
        filename.length > 25 + filetype.length
            ? `${filename.slice(0, 20)}(...).${filetype}`
            : filename;

    return DIV(
        'field--doc',
        A(
            {
                href: file.url || '#',
                target: '_blank',
                title: file.name || '#',
            },
            SPAN('icon', nameToString('file')),
            SPAN({}, trimmedName)
        )
    );
};

const renderParcelFileThumbnail = (file: ParcelFile) => {
    const content_type = file.file.type ? file.file.type.split('/')[0] : '';
    switch (content_type) {
        case 'image':
            return renderParcelImage(file);
        case 'application':
            return renderParcelDocument(file);
        default:
            return DIV({ className: 'field--image__thumbnail' });
    }
};

const renderParcelFiles = (files: ParcelFile[]) => {
    const sorted_files = sortParcelFilesByContentType(files);
    return DIV(
        'files-summary__wrapper',
        ...sorted_files.map(renderParcelFileThumbnail)
    );
};

const renderMiniMap = minimap(
    'https://geoservices-urbis.irisnet.be/geoserver/ows',
    {
        LAYERS: 'urbisFRGray',
    }
);
const renderGeometry = (geom: MultiPolygon) => {
    const step = renderMiniMap(geom);
    switch (step.step) {
        case 'failed':
            return none;
        case 'loading':
            return some(DIV('minimap-loading', tr.sitex('loading')));
        case 'loaded':
            return some(DIV('value minimap-loaded', IMG({ src: step.data })));
    }
};

const renderGroup = (group_id: string) =>
    findGroup(group_id).map(({ name }) => DIV('value value--string', name));

const renderValue = (d: FieldValueDescriptor): Option<React.ReactNode> => {
    switch (d.tag) {
        case 'string':
            return d.value.map(renderString);
        case 'number':
            return d.value.map(renderNumber);
        case 'boolean':
            return d.value.map(renderBoolean);
        case 'datastate':
            return d.value.map(renderDataState);
        case 'geometry':
            return d.value.map(renderGeometry);
        case 'buildgroup':
            return d.value.map(renderGroup);
        case 'geostate':
            return d.value.map(renderGeoState);
        case 'recordstate':
            return none;
        case 'dict-typo':
            return d.value.map(renderTypology);
        case 'dict-statecode':
            return d.value.map(renderStatecode);
        case 'dict-observation':
            return d.value.map(renderObservation);
        case 'dict-update':
            return none;
        case 'occupation-building':
            return d.value.map(renderBuildingOccupation);
        case 'files-building':
            return d.value.map(renderFile);
        case 'occupation-parcel':
            return d.value.map(renderParcelOccupation);
        case 'files-parcel':
            return d.value.map(renderParcelFiles);
    }
};

const infoItem = (prop: BuildingProp, data: BuildingFormData) => {
    const field = fromRawField(data[prop]);
    return DIV(
        {
            className: wrapperClassName(prop, field),
            onClick: () => navigateProp(prop),
        },
        DIV('key', tr.sitex(propToMessageKey(prop))),
        renderValue(field)
    );
};

const infoParcelItem = (prop: ParcelProp, data: ParcelFormData) => {
    const field = fromRawField(data[prop]);
    return DIV(
        {
            className: wrapperParcelClassName(prop, field),
            onClick: () => navigateProp(prop),
        },
        DIV('key', tr.sitex(propParcelToMessageKey(prop))),
        renderValue(field)
    );
};

const renderGoup =
    (form: BuildingFormData) =>
    ({ props }: BuildingPropGroup) =>
        DIV('infos__group', ...props.map(prop => infoItem(prop, form)));

const renderParcelGroup =
    (form: ParcelFormData) =>
    ({ props }: ParcelPropGroup) =>
        DIV('infos__group', ...props.map(prop => infoParcelItem(prop, form)));

const renderCommpressedAddressComp = (addr: CompressesedAddressComp) =>
    DIV(
        'compressed-address',
        addr.comps
            .map(
                ({ streetName, numbers }) =>
                    `${streetName} ${numbers.join(' - ')}`
            )
            .join('; '),
        `, ${addr.municipality}`
    );

const renderUrbisAddress = () =>
    getCurrentBuildingForm()
        .chain(form => fromNullable(form.u2building.value))
        .chain(findUrbisAddress)
        .map(address =>
            DIV(
                {
                    className: `address__container compress-${address.comps.length}`,
                },
                H1(
                    'address',
                    ...address.comps.map(renderCommpressedAddressComp)
                ),
                DIV(
                    'capakey',
                    `${tr.sitex(
                        'localCadastralplot'
                    )} : ${address.capakeys.join('; ')}`
                )
            )
        );

const renderActions = (surveyType: SurveyType) =>
    getFormId().map(id =>
        DIV(
            'form-actions',
            buttonEdit(() => navigateForm(surveyType, id)),
            renderHistoryButton(surveyType, id),
            buttonPrint(() => window.print())
        )
    );

const infosBuildingWrapper = (form: BuildingFormData) =>
    DIV(
        'infos building',
        findBuildingPropGroup('general').map(renderGoup(form)),
        findBuildingPropGroup('secondary').map(renderGoup(form)),
        findBuildingPropGroup('details').map(renderGoup(form))
    );

const renderBuilding = () => getCurrentBuildingForm().map(infosBuildingWrapper);

const infosParcelWrapper = (form: ParcelFormData) =>
    DIV(
        'infos parcel',
        // fromNullable(form.u2parcel.value)
        //     .chain(findUrbisAddressParcel)
        //     .map(renderUrbisAddress),
        findParcelPropGroup('general').map(renderParcelGroup(form)),
        findParcelPropGroup('secondary').map(renderParcelGroup(form)),
        findParcelPropGroup('details').map(renderParcelGroup(form))
    );
const renderParcel = () => getCurrentParcelForm().map(infosParcelWrapper);

const renderPublicSpace = () => DIV('', '~Not Implemented');

const renderHistoryButton = (surveyType: SurveyType, id: string) => {
    switch (surveyType) {
        case 'public-space':
        case 'parcel':
            return buttonHistory(() => {
                loadParcelHistory(id);
                navigateHistory(surveyType, id);
            });
        case 'building':
            return buttonHistory(() => {
                loadBuildingHistory(id);
                navigateHistory(surveyType, id);
            });
    }
};

const surveyTypeTitle = () =>
    getSurveyType().map(surveyType => {
        switch (surveyType) {
            case 'building':
                return tr.sitex('previewBuildingSheetTitle');
            case 'parcel':
                return tr.sitex('previewParcelSheetTitle');
            case 'public-space':
                return tr.sitex('previewPublicSpaceSheetTitle');
        }
    });

const sidebar = (surveyType: SurveyType, ...nodes: NodeOrOptional[]) =>
    DIV(
        'sidebar infos__wrapper',
        DIV(
            'sidebar__header',
            buttonHome(() => navigateIndex(surveyType)),
            H2('subtitle', surveyTypeTitle())
        ),
        ...nodes
    );

const renderSidebar = () =>
    getSurveyType().map(surveyType => {
        switch (surveyType) {
            case 'building':
                return sidebar(
                    surveyType,
                    DIV(
                        'infos__header',
                        renderUrbisAddress(),
                        renderActions(surveyType)
                    ),
                    renderBuilding()
                );
            case 'parcel':
                return sidebar(
                    surveyType,
                    DIV(
                        'infos__header',
                        renderUrbisAddress(),
                        renderActions(surveyType)
                    ),
                    renderParcel()
                );
            case 'public-space':
                return sidebar(surveyType, renderPublicSpace());
        }
    });

export const renderPreview = () => DIV('preview', renderSidebar(), renderMap());

export default renderPreview;
