import { Option, none, some, fromPredicate } from 'fp-ts/lib/Option';
import { DIV } from 'sdi/components/elements';
import tr from 'sdi/locale';
import {
    create,
    IMapViewData,
    InteractionMeasure,
    singleSelectOptions,
} from 'sdi/map';
import { tryMultiPolygon } from 'sdi/util';
import { renderRadioIn } from 'sdi/components/input';
import geocoder from 'sdi/components/geocoder';
import { iife } from 'sdi/lib';
import {
    updateView,
    selectFeature,
    clearSelection,
    startCreatingGeometry,
    resetMapInteraction,
    createFormFromGeometry,
    setCreateGeometry,
    updateCreateGeometry,
    navigateFormAtProp,
    setBaseLayerCode,
    setView,
    switchSurveyType,
    stopMeasure,
    updateMeasureCoordinates,
    startMeasureLength,
    startMeasureArea,
    updateGroupsView,
} from '../events';
import {
    mainMapName,
    getView,
    getBaseLayer,
    getMapInfo,
    getSurveyType,
    getSelection as getSelected,
    getInteraction,
    getBaseLayerCode,
    BaseLayerCode,
    getMeasured,
} from '../queries';
import geometry from '../queries/map/geometry';
import { SurveyType } from '../remote';
import {
    buttonCreateGeometryOff,
    buttonConfirmCreateBuilding,
    buttonConfirmCreateParcel,
    buttonConfirmCreatePublicSpace,
    buttonCreateBuildingOn,
    buttonCreateParcelOn,
    buttonCreatePublicSpaceOn,
    buttonStopMeasure,
    buttonStartMeasureLine,
    buttonStartMeasurePolygon,
} from './buttons';
import { renderNorthButton } from 'sdi/map/controls';
import urbisBuilding from '../queries/map/urbis-building';
import urbisParcel from '../queries/map/urbis-parcel';

let mapUpdate: Option<() => void> = none;
let mapSetTarget: Option<(e: HTMLElement) => void> = none;

const selectOptions = singleSelectOptions({
    clearSelection,
    getSelected,
    selectFeature,
});

const attachMap = (element: HTMLElement | null) => {
    mapUpdate = mapUpdate.foldL(
        () => {
            const {
                update,
                setTarget,
                selectable,
                editable,
                measurable,
                follow,
            } = create(mainMapName, {
                getBaseLayer,
                getView,
                getMapInfo,

                updateView,
                setScaleLine: () => void 0,

                element,
            });

            selectable(selectOptions, getInteraction);
            editable(
                {
                    getCurrentLayerId: () => geometry.name,
                    getGeometryType: () => 'MultiPolygon',
                    addFeature: feature =>
                        tryMultiPolygon(feature.geometry).map(
                            setCreateGeometry
                        ),
                    setGeometry: updateCreateGeometry,
                    getSnapLayer: () =>
                        getSurveyType()
                            .map(st => {
                                switch (st) {
                                    case 'building':
                                        return urbisBuilding.name;
                                    case 'parcel':
                                        return urbisParcel.name;
                                    default:
                                        return '__not_supported__';
                                }
                            })
                            .getOrElse('__missing_survey_type__'),
                },
                getInteraction
            );

            measurable(
                {
                    updateMeasureCoordinates,
                    stopMeasuring: stopMeasure,
                },
                getInteraction
            );

            follow('groups', updateGroupsView);

            mapSetTarget = some(setTarget);
            update();
            return some(update);
        },
        update => some(update)
    );

    if (element) {
        mapSetTarget.map(f => f(element));
    }
};

const renderSwitchLabel = (s: SurveyType) => {
    switch (s) {
        case 'building':
            return tr.sitex('mode-building');
        case 'parcel':
            return tr.sitex('mode-parcel');
        case 'public-space':
            return tr.sitex('mode-public-space');
    }
};

const renderSwitchSurveyType = () =>
    renderRadioIn(
        'survey-type',
        renderSwitchLabel,
        switchSurveyType,
        'switch'
    )(
        [
            'building',
            'parcel',
            // 'public-space' //no public space for now
        ],
        getSurveyType().getOrElse('building')
    );

const renderBaseLayerSwitch = () => {
    const [other, className] = iife((): [BaseLayerCode, string] => {
        if (getBaseLayerCode() === 'urbis_gray') {
            return ['urbis_ortho', 'switch-background-ortho'];
        }
        return ['urbis_gray', 'switch-background-bn'];
    });
    return DIV(
        { className: 'switcher' },
        DIV({ className, onClick: () => setBaseLayerCode(other) })
    );
};

const renderToolsSelect = () =>
    DIV(
        'map-tools',
        buttonStartMeasurePolygon(startMeasureArea),
        buttonStartMeasureLine(startMeasureLength),

        getSurveyType()
            .map(st => {
                switch (st) {
                    case 'building':
                        return buttonCreateBuildingOn;
                    case 'parcel':
                        return buttonCreateParcelOn;
                    case 'public-space':
                        return buttonCreatePublicSpaceOn;
                }
            })
            .map(button => button(startCreatingGeometry))
    );

const renderToolsCreate = () =>
    DIV(
        'map-tools',
        buttonCreateGeometryOff(resetMapInteraction),
        getSurveyType().map(st => {
            const confirm = () =>
                createFormFromGeometry(st).map(id => {
                    navigateFormAtProp(st, id, 'geom');
                    resetMapInteraction();
                });
            switch (st) {
                case 'building':
                    return buttonConfirmCreateBuilding(confirm);
                case 'parcel':
                    return buttonConfirmCreateParcel(confirm);
                case 'public-space':
                    return buttonConfirmCreatePublicSpace(confirm);
            }
        })
    );

const renderToolsMeasure = ({ state }: InteractionMeasure) =>
    DIV(
        `map-tools measure-box ${state.geometryType}`,
        DIV('measure-value', getMeasured()),
        buttonStopMeasure(stopMeasure)
    );

const renderTools = () => {
    const i = getInteraction();
    switch (i.label) {
        case 'select':
            return renderToolsSelect();
        case 'measure':
            return renderToolsMeasure(i);
        default:
            return renderToolsCreate();
    }
};

const renderLookup = () => geocoder(setView, () => void 0);

const notStraight = fromPredicate<IMapViewData>(
    ({ rotation }) => Math.abs(rotation) > 0.05
);

const northButton = () =>
    notStraight(getView()).map(() => renderNorthButton(setView, getView));

const render = () => {
    mapUpdate.map(f => f());
    return DIV(
        {
            className: 'map-wrapper',
            'aria-label': `${tr.core('map')}`,
        },
        renderTools(),
        renderSwitchSurveyType(),
        renderBaseLayerSwitch(),
        renderLookup(),
        // onIndexPage(getLayout()).map(renderTools),
        northButton(),
        DIV({
            id: mainMapName,
            key: mainMapName,
            className: 'map',
            ref: attachMap,
        })
    );
};

export default render;
