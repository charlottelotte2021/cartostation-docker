import { index } from 'fp-ts/lib/Array';
import { fromNullable, fromPredicate, Option } from 'fp-ts/lib/Option';
import {
    DIV,
    INPUT,
    LABEL,
    OPTION,
    SELECT,
    SPAN,
} from 'sdi/components/elements';
import { iife } from 'sdi/lib';
import tr, { fromRecord } from 'sdi/locale';
import { MultiPolygon } from 'sdi/source';
import { Nullable, zip } from 'sdi/util';
import { setOccupationLookup } from 'sitex/src/events';
import {
    filterOccupations,
    findOccupationByCode,
    getOccupationLookup,
    getOccupationPrefix,
    lookupOccupation,
} from 'sitex/src/queries';
import {
    BuildingOccupation,
    DataState,
    GeoState,
    RecordState,
    BuildingFile,
    ParcelOccupation,
    ParcelFile,
    SurveyType,
    SitexFile,
    BuildingGroup,
    NULL_OCCUPATION_CODE,
    Occupation,
} from 'sitex/src/remote';

export type DictCode = 'typo' | 'update' | 'statecode' | 'observation';

type ValueTag =
    | 'string'
    | 'number'
    | 'boolean'
    | 'recordstate'
    | 'datastate'
    | 'geostate'
    | 'buildgroup'
    | `dict-${DictCode}`
    | 'geometry'
    | `occupation-${SurveyType}`
    | `files-${SurveyType}`;

type ValueType =
    | number
    | string
    | number
    | boolean
    | RecordState
    | DataState
    | GeoState
    | BuildingGroup
    | MultiPolygon
    | BuildingOccupation[]
    | BuildingFile[]
    | ParcelOccupation[]
    | ParcelFile[];

type Value<T extends ValueTag, O extends ValueType> = {
    tag: T;
    value: Nullable<O>;
    touched: boolean;
};

export type DictValue = Value<`dict-${DictCode}`, number>;
export type StringValue = Value<'string', string>;
export type NumberValue = Value<'number', number>;
export type BooleanValue = Value<'boolean', boolean>;
export type RecordStateValue = Value<'recordstate', RecordState>;
export type DataStateValue = Value<'datastate', DataState>;
export type GeoStateValue = Value<'geostate', GeoState>;
export type GeometryValue = Value<'geometry', MultiPolygon>;
export type BuildingOccupationValue = Value<
    'occupation-building',
    BuildingOccupation[]
>;

export type BuildingFileValue = Value<'files-building', BuildingFile[]>;
export type ParcelOccupationValue = Value<
    'occupation-parcel',
    ParcelOccupation[]
>;

export type ParcelFileValue = Value<'files-parcel', ParcelFile[]>;
export type BuildingGroupValue = Value<'buildgroup', string>;

export type FieldValueDescriptorRaw =
    | DictValue
    | StringValue
    | NumberValue
    | BooleanValue
    | RecordStateValue
    | DataStateValue
    | GeoStateValue
    | GeometryValue
    | BuildingGroupValue
    | BuildingOccupationValue
    | BuildingFileValue
    | ParcelOccupationValue
    | ParcelFileValue;

type OptionValue<V extends FieldValueDescriptorRaw> = {
    tag: V['tag'];
    touched: V['touched'];
    value: Option<NonNullable<V['value']>>;
};

export type FieldValueDescriptor =
    | OptionValue<DictValue>
    | OptionValue<StringValue>
    | OptionValue<NumberValue>
    | OptionValue<BooleanValue>
    | OptionValue<RecordStateValue>
    | OptionValue<DataStateValue>
    | OptionValue<GeoStateValue>
    | OptionValue<GeometryValue>
    | OptionValue<BuildingGroupValue>
    | OptionValue<BuildingOccupationValue>
    | OptionValue<BuildingFileValue>
    | OptionValue<ParcelOccupationValue>
    | OptionValue<ParcelFileValue>;

const compareBase = (a: FieldValueDescriptorRaw, b: FieldValueDescriptorRaw) =>
    a.tag === b.tag && a.touched === b.touched;

const compareSimpleValue = (
    a: FieldValueDescriptorRaw,
    b: FieldValueDescriptorRaw
) => compareBase(a, b) && a.value === b.value;

// const compareStringValues = (a: StringValue, b: StringValue) =>
//     compareSimple(a, b)

// const compareNumberValues = (a: NumberValue, b: NumberValue) =>
//     compareSimple(a, b)

// const compareBooleanValues = (a: BooleanValue, b: BooleanValue) =>
//     compareSimple(a, b)

const compareBuildingOccupation = (
    a: BuildingOccupation,
    b: BuildingOccupation
) =>
    a.occupcode === b.occupcode &&
    a.level === b.level &&
    a.area === b.area &&
    a.owner === b.owner &&
    a.vacant === b.vacant &&
    a.description === b.description;

const compareParcelOccupation = (a: ParcelOccupation, b: ParcelOccupation) =>
    a.occupcode === b.occupcode &&
    a.area === b.area &&
    a.nbparkcar == b.nbparkcar &&
    a.nbparkpmr === b.nbparkpmr &&
    a.nbelecplug === b.nbelecplug &&
    a.nbparkbike === b.nbparkbike &&
    a.owner === b.owner &&
    a.vacant === b.vacant &&
    a.description === b.description;

const compareOccupationList = <T extends BuildingOccupation | ParcelOccupation>(
    a: Nullable<T[]>,
    b: Nullable<T[]>,
    comp: (a: T, b: T) => boolean
) => {
    if (a === null && b === null) {
        return true;
    } else if (a !== null && b !== null) {
        if (a.length !== b.length) {
            return false;
        } else {
            return zip(a, b).reduce(
                (acc, [a, b]) => (acc ? comp(a, b) : acc),
                true
            );
        }
    }
    return false;
};

const compareBuildingOccupationValue = (
    a: BuildingOccupationValue,
    b: BuildingOccupationValue
) =>
    compareBase(a, b) &&
    compareOccupationList(a.value, b.value, compareBuildingOccupation);

const compareParcelOccupationValue = (
    a: ParcelOccupationValue,
    b: ParcelOccupationValue
) =>
    compareBase(a, b) &&
    compareOccupationList(a.value, b.value, compareParcelOccupation);

const compareFile = (a: SitexFile, b: SitexFile) => a.id === b.id;
const compareFileList = (a: SitexFile[], b: SitexFile[]) => {
    if (a.length !== b.length) {
        return false;
    } else {
        return zip(a, b).reduce(
            (acc, [a, b]) => (acc ? compareFile(a, b) : acc),
            true
        );
    }
};
const compareFileValue = <T extends BuildingFileValue | ParcelFileValue>(
    a: T,
    b: T
) =>
    compareBase(a, b) && a.value && b.value
        ? compareFileList(
              a.value.map(v => v.file),
              b.value.map(v => v.file)
          )
        : a.value === a.value;

const compareGeometryValue = (a: GeometryValue, b: GeometryValue) => {
    if (a.value !== null && b.value !== null) {
        const acoords = a.value.coordinates;
        const bcoords = b.value.coordinates;
        if (acoords.length !== bcoords.length) {
            return false;
        }
        const polys = zip(acoords, bcoords);
        for (const [apoly, bpoly] of polys) {
            if (apoly.length !== bpoly.length) {
                return false;
            }
            const rings = zip(apoly, bpoly);
            for (const [aring, bring] of rings) {
                if (aring.length !== bring.length) {
                    return false;
                }
                const points = zip(aring, bring);
                for (const [apoint, bpoint] of points) {
                    if (apoint[0] !== bpoint[0] || apoint[1] !== bpoint[1]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    return a.value === b.value;
};

export const compareFieldValue = <T extends FieldValueDescriptorRaw>(
    a: T,
    b: T
) => {
    switch (a.tag) {
        case 'string':
            return compareSimpleValue(a, b);
        case 'number':
            return compareSimpleValue(a, b);
        case 'boolean':
            return compareSimpleValue(a, b);
        case 'recordstate':
            return compareSimpleValue(a, b);
        case 'datastate':
            return compareSimpleValue(a, b);
        case 'geostate':
            return compareSimpleValue(a, b);
        case 'buildgroup':
            return compareSimpleValue(a, b);
        case 'dict-typo':
            return compareSimpleValue(a, b);
        case 'dict-statecode':
            return compareSimpleValue(a, b);
        case 'dict-update':
            return compareSimpleValue(a, b);
        case 'dict-observation':
            return compareSimpleValue(a, b);
        case 'geometry':
            return compareGeometryValue(a, b as GeometryValue);
        case 'occupation-building':
            return compareBuildingOccupationValue(
                a,
                b as BuildingOccupationValue
            ); // silly, but it is the sad state of TS
        case 'files-building':
            return compareFileValue(a, b as BuildingFileValue);
        case 'occupation-parcel':
            return compareParcelOccupationValue(a, b as ParcelOccupationValue);
        case 'files-parcel':
            return compareFileValue(a, b as ParcelFileValue);
    }
};

export const fromRawField = (
    field: FieldValueDescriptorRaw
): FieldValueDescriptor => {
    switch (field.tag) {
        case 'string':
            return { ...field, value: fromNullable(field.value) };
        case 'number':
            return { ...field, value: fromNullable(field.value) };
        case 'boolean':
            return { ...field, value: fromNullable(field.value) };
        case 'recordstate':
            return { ...field, value: fromNullable(field.value) };
        case 'datastate':
            return { ...field, value: fromNullable(field.value) };
        case 'geostate':
            return { ...field, value: fromNullable(field.value) };
        case 'geometry':
            return { ...field, value: fromNullable(field.value) };
        case 'buildgroup':
            return { ...field, value: fromNullable(field.value) };
        case 'dict-typo':
            return { ...field, value: fromNullable(field.value) };
        case 'dict-statecode':
            return { ...field, value: fromNullable(field.value) };
        case 'dict-update':
            return { ...field, value: fromNullable(field.value) };
        case 'dict-observation':
            return { ...field, value: fromNullable(field.value) };
        case 'occupation-building':
            return { ...field, value: fromNullable(field.value) };

        case 'files-building':
            return { ...field, value: fromNullable(field.value) };
        case 'occupation-parcel':
            return { ...field, value: fromNullable(field.value) };

        case 'files-parcel':
            return { ...field, value: fromNullable(field.value) };
    }
};

export const numberValue = (
    value: number | null,
    touched = false
): NumberValue => ({
    tag: 'number',
    value,
    touched,
});

export const booleanValue = (
    value: boolean | null,
    touched = false
): BooleanValue => ({
    tag: 'boolean',
    value,
    touched,
});

export const stringValue = (
    value: string | null,
    touched = false
): StringValue => ({
    tag: 'string',
    value,
    touched,
});

export const dictValue = (
    dict: DictCode,
    value: number | null,
    touched = false
): DictValue => ({
    tag: `dict-${dict}`,
    value,
    touched,
});

export const recordStateValue = (
    value: RecordState | null,
    touched = false
): RecordStateValue => ({
    tag: 'recordstate',
    value,
    touched,
});

export const dataStateValue = (
    value: DataState | null,
    touched = false
): DataStateValue => ({
    tag: 'datastate',
    value,
    touched,
});

export const geoStateValue = (
    value: GeoState | null,
    touched = false
): GeoStateValue => ({
    tag: 'geostate',
    value,
    touched,
});

export const geometryValue = (
    value: MultiPolygon | null,
    touched = false
): GeometryValue => ({
    tag: 'geometry',
    value,
    touched,
});

export const buildingOccupationValue = (
    value: BuildingOccupation[] | null,
    touched = false
): BuildingOccupationValue => ({
    tag: 'occupation-building',
    value,
    touched,
});

export const buildingFileValue = (
    value: BuildingFile[] | null,
    touched = false
): BuildingFileValue => ({
    tag: 'files-building',
    value,
    touched,
});

export const parcelOccupationValue = (
    value: ParcelOccupation[] | null,
    touched = false
): ParcelOccupationValue => ({
    tag: 'occupation-parcel',
    value,
    touched,
});

export const parcelFileValue = (
    value: ParcelFile[] | null,
    touched = false
): ParcelFileValue => ({
    tag: 'files-parcel',
    value,
    touched,
});

export const buildingGroupValue = (
    value: string | null,
    touched = false
): BuildingGroupValue => ({
    tag: 'buildgroup',
    value,
    touched,
});

export const updateString = (v: string | null) => stringValue(v, true);
export const updateNumber = (v: number | null) => numberValue(v, true);
export const updateBoolean = (v: boolean | null) => booleanValue(v, true);
export const updateRecordState = (v: RecordState | null) =>
    recordStateValue(v, true);
export const updateDataState = (v: DataState | null) => dataStateValue(v, true);
export const updateGeoState = (v: GeoState | null) => geoStateValue(v, true);
export const updateDictTypo = (v: number | null) => dictValue('typo', v, true);
export const updateDictStateCode = (v: number | null) =>
    dictValue('statecode', v, true);
export const updateDictObservation = (v: number | null) =>
    dictValue('observation', v, true);
export const updateDictUpdate = (v: number | null) =>
    dictValue('update', v, true);
export const updateGeometry = (v: MultiPolygon | null) =>
    geometryValue(v, true);
export const updateBuildingOccupation = (v: BuildingOccupation[] | null) =>
    buildingOccupationValue(v, true);
export const updateBuildingFile = (v: BuildingFile[] | null) =>
    buildingFileValue(v, true);
export const updateParcelOccupation = (v: ParcelOccupation[] | null) =>
    parcelOccupationValue(v, true);
export const updateParcelFile = (v: ParcelFile[] | null) =>
    parcelFileValue(v, true);
export const updateBuildGroup = (v: string | null) =>
    buildingGroupValue(v, true);

export const cloneValue = <T extends FieldValueDescriptorRaw>(v: T): T => ({
    ...v,
    value: JSON.parse(JSON.stringify(v.value)),
});

export const nullifyValue = <T extends FieldValueDescriptorRaw>(v: T): T => ({
    ...v,
    touched: true,
    value: null,
});

export type CodePrefix =
    | []
    | [string]
    | [string, string]
    | [string, string, string]
    | [string, string, string, string];

export type OccupationWidgetState = {
    index: number;
    prefix: CodePrefix;
    lookup: string | null;
};

export const defaultOccupationWidgetState = (): OccupationWidgetState => ({
    index: -1,
    prefix: [],
    lookup: null,
});

export const renderOccupationBreadcrumb = (
    occupcode: string | null,
    prefix: CodePrefix,
    clearPart: (n: number, parts: string[]) => () => void
) =>
    fromNullable(occupcode)
        .map(code => {
            const parts = code.split('.');
            return DIV(
                'occupation-code__list',
                DIV(
                    {
                        className: `occupation-code__item ${
                            prefix.length > 0 ? 'active' : ''
                        }`,
                        onClick: clearPart(0, parts),
                    },
                    index(0, parts)
                ),
                DIV(
                    {
                        className: `occupation-code__item ${
                            prefix.length > 1 ? 'active' : ''
                        }`,
                        onClick: clearPart(1, parts),
                    },
                    index(1, parts)
                ),
                DIV(
                    {
                        className: `occupation-code__item ${
                            prefix.length > 2 ? 'active' : ''
                        }`,
                        onClick: clearPart(2, parts),
                    },
                    index(2, parts)
                ),
                DIV(
                    {
                        className: `occupation-code__item ${
                            prefix.length > 3 ? 'active' : ''
                        }`,
                        onClick: clearPart(3, parts),
                    },
                    index(3, parts)
                )
            );
        })
        .getOrElse(
            DIV(
                'occupation-code__list',
                DIV('occupation-code__item', '00'),
                DIV('occupation-code__item', '00'),
                DIV('occupation-code__item', '00'),
                DIV('occupation-code__item', '00')
            )
        );

const renderOption = ({ code, name }: Occupation) =>
    OPTION(
        {
            value: code,
        },
        fromRecord(name)
    );

const withOccupations = fromPredicate<Occupation[]>(os => os.length > 0);

export const renderOccupationOptions = (
    st: SurveyType,
    occup: BuildingOccupation | ParcelOccupation,
    update: (code: string) => void
) =>
    withOccupations(
        filterOccupations(st, getOccupationPrefix(st)).filter(
            o => o.code !== occup.occupcode
        )
    ).map(occupations =>
        SELECT(
            {
                className: 'occupation-value__list',
                value: NULL_OCCUPATION_CODE,
                onChange: e =>
                    findOccupationByCode(e.currentTarget.value).map(
                        ({ code }) => update(code)
                    ),
            },
            OPTION(
                {
                    className: 'select-placeholder',
                    value: NULL_OCCUPATION_CODE,
                },
                '---'
            ),
            ...occupations.map(renderOption)
        )
    );

const { lookupRef, clearLookup } = iife(() => {
    let input: null | HTMLInputElement = null;

    const lookupRef = (e: null | HTMLInputElement) => (input = e);

    const clearLookup = () => fromNullable(input).map(i => (i.value = ''));

    return { lookupRef, clearLookup };
});

const renderOccupationLookupInput = (st: SurveyType) =>
    INPUT({
        ref: lookupRef,
        key: 'renderOccupationLookupInput',
        type: 'text',
        placeholder: tr.sitex('lookup'),
        defaultValue: getOccupationLookup(st).toUndefined(),
        onChange: e => setOccupationLookup(st, e.currentTarget.value),
    });

const renderOccupationLookupResult = (
    st: SurveyType,
    update: (code: string) => void
) =>
    getOccupationLookup(st).chain(token =>
        withOccupations(lookupOccupation(st, token)).map(
            occupations =>
                DIV(
                    'tag__list',
                    ...occupations.map(occupation =>
                        DIV(
                            {
                                className: 'tag interactive',
                                onClick: () => {
                                    update(occupation.code);
                                    clearLookup();
                                },
                            },
                            fromRecord(occupation.name)
                        )
                    )
                )
            // SELECT(
            //     {
            //         className: 'result-list',
            //         onChange: e =>
            //             findOccupationByCode(e.currentTarget.value).map(
            //                 ({ code }) => update(code)
            //             ),
            //     },
            //     ...occupations.map(occupation =>
            //         OPTION(
            //             {
            //                 value: occupation.code,
            //             },
            //             fromRecord(occupation.name)
            //         )
            //     )
            // )
        )
    );

export const renderOccupationLookup = (
    st: SurveyType,
    update: (code: string) => void
) =>
    DIV(
        'lookup',
        renderOccupationLookupInput(st),
        renderOccupationLookupResult(st, update)
    );

export const renderOccupationVacant = (
    occup: BuildingOccupation | ParcelOccupation,
    onChange: () => void
) =>
    LABEL(
        'input--checkbox vacant',
        INPUT({
            key: `checkbox-vacant-${occup.occupcode}`,
            type: `checkbox`,
            onChange,
        }),
        SPAN('', tr.sitex('occupationVacant'))
    );
