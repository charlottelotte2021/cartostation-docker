/**
 * This is a port of
 * https://github.com/sb-js/typescript-algorithms/tree/master/src/algorithms/sets/longest-common-subsequence
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Oleksii Trekhleb
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

type Identity = ['id', string[]];
type Add = ['add', string[]];
type Del = ['del', string[]];

type Op = Identity | Add | Del;

export type Diff = Op[];

/**
 *
 * @param set1
 * @param set2
 */
export const longestSequence = (
    set1: string[],
    set2: string[],
    collator: Intl.Collator
): [string[], Diff] => {
    // Init LCS matrix.
    const lcsMatrix = Array(set2.length + 1)
        .fill(null)
        .map(() => Array(set1.length + 1).fill(null));

    // Fill first row with zeros.
    for (let columnIndex = 0; columnIndex <= set1.length; columnIndex += 1) {
        lcsMatrix[0][columnIndex] = 0;
    }

    // Fill first column with zeros.
    for (let rowIndex = 0; rowIndex <= set2.length; rowIndex += 1) {
        lcsMatrix[rowIndex][0] = 0;
    }

    // Fill rest of the column that correspond to each of two strings.
    for (let rowIndex = 1; rowIndex <= set2.length; rowIndex += 1) {
        for (
            let columnIndex = 1;
            columnIndex <= set1.length;
            columnIndex += 1
        ) {
            if (
                collator.compare(set1[columnIndex - 1], set2[rowIndex - 1]) ===
                0
            ) {
                lcsMatrix[rowIndex][columnIndex] =
                    lcsMatrix[rowIndex - 1][columnIndex - 1] + 1;
            } else {
                lcsMatrix[rowIndex][columnIndex] = Math.max(
                    lcsMatrix[rowIndex - 1][columnIndex],
                    lcsMatrix[rowIndex][columnIndex - 1]
                );
            }
        }
    }

    // Calculate LCS based on LCS matrix.
    if (!lcsMatrix[set2.length][set1.length]) {
        // If the length of largest common string is zero then return empty string.
        return [
            [''],
            [
                ['del', set1],
                ['add', set2],
            ],
        ];
    }

    const longestSequence: string[] = [];
    const diff: Diff = [];
    let columnIndex = set1.length;
    let rowIndex = set2.length;

    while (columnIndex > 0 && rowIndex > 0) {
        if (collator.compare(set1[columnIndex - 1], set2[rowIndex - 1]) === 0) {
            // Move by diagonal left-top.
            longestSequence.unshift(set1[columnIndex - 1]);
            diff.push(['id', [set1[columnIndex - 1]]]);
            columnIndex -= 1;
            rowIndex -= 1;
        } else if (
            lcsMatrix[rowIndex - 1][columnIndex] <
            lcsMatrix[rowIndex][columnIndex - 1]
        ) {
            // Move left.
            diff.push(['del', [set1[columnIndex - 1]]]);
            columnIndex -= 1;
        } else {
            // Move up.
            diff.push(['add', [set2[rowIndex - 1]]]);
            rowIndex -= 1;
        }
    }
    // let columnIndex = set1.length;
    // let rowIndex = set2.length;

    // while (columnIndex > 0 || rowIndex > 0) {
    //     if (set1[columnIndex - 1] === set2[rowIndex - 1]) {
    //         // Move by diagonal left-top.
    //         longestSequence.unshift(set1[columnIndex - 1]);
    //         diff.push(['id', [set1[columnIndex - 1]]])
    //         columnIndex -= 1;
    //         rowIndex -= 1;
    //     } else if (lcsMatrix[rowIndex][columnIndex] === lcsMatrix[rowIndex][columnIndex - 1]) {
    //         // Move left.
    //         diff.push(['del', [set1[columnIndex - 1]]])
    //         columnIndex -= 1;
    //     } else {
    //         // Move up.
    //         diff.push(['add', [set2[rowIndex - 1]]])
    //         rowIndex -= 1;
    //     }
    // }

    return [longestSequence, diff.reverse()];
};

// export const diffByWord = (str1: string, str2: string) => {
//     // (str1: string, str2: string) => {
//     // const str1 = 'a b c d f g h j q z';
//     // const str2 = 'a b c d e f g i j k r x y z';
//     const strTable1 = str1.split(' ');
//     const strTable2 = str2.split(' ');
//     const [, diff] = getDiff(strTable1, strTable2);
//     // diff.forEach(d => console.log(`------------------- ${d[0]}: ${d[1]}`));
//     return diff;
// }
