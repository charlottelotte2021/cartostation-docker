import {
    IMapViewData,
    Interaction,
    defaultInteraction,
    IViewEvent,
    FeaturePath,
    nullFeaturePath,
} from 'sdi/map';
import {
    Inspire,
    ILayerInfo,
    FeatureCollection,
    RemoteResource,
    MultiPolygon,
} from 'sdi/source';
import { Collection, Nullable } from 'sdi/util';
import { BaseLayerCode } from '../queries';

declare module 'sdi/shape' {
    export interface IShape {
        'port/map/view': IMapViewData;
        'port/map/interaction': Interaction;
        'port/map/selection': FeaturePath;
        'port/map/create': Nullable<MultiPolygon>;
        'port/map/baselayer': BaseLayerCode;

        'data/layer-infos': ILayerInfo[];
        'data/metadatas': Inspire[];
        'data/layers': Collection<RemoteResource<FeatureCollection>>;
    }
}

export const defaultMapView = (): IMapViewData => ({
    dirty: 'geo',
    srs: 'EPSG:31370',
    center: [148885, 170690],
    rotation: 0,
    zoom: 8.2,
    feature: null,
    extent: null,
});

export const defaultMapEvent = (): IViewEvent => ({
    dirty: 'geo',
    center: [148885, 170690],
    rotation: 0,
    zoom: 6,
});

export const defaultMapState = () => ({
    'port/map/view': defaultMapView(),
    'port/map/interaction': defaultInteraction(),
    'port/map/selection': nullFeaturePath(),
    'port/map/create': null,
    'port/map/baselayer': 'urbis_gray' as BaseLayerCode,
    'data/layer-infos': [],
    'data/metadatas': [],
    'data/layers': {},
});
