import * as debug from 'debug';
import { index } from 'fp-ts/lib/Array';
import { fromEither, some } from 'fp-ts/lib/Option';
import * as io from 'io-ts';

import { scopeOption } from 'sdi/lib';
import { Router, Path } from 'sdi/router';

import {
    clearSelection,
    mountEditorLayers,
    mountLayers,
    prepareForm,
    resetSurveyProp,
    setFormId,
    setLayout,
    setSurveyType,
    startEditingGeometry,
    updateSitexBuildingLayer,
    updateSitexParcelLayer,
} from '.';
import {
    SurveyProp,
    SurveyPropIO,
    SurveyType,
    SurveyTypeIO,
} from 'sitex/src/remote';
import { uuidIO } from 'sdi/source';
import { getFormId, getSurveyType } from '../queries';
import { setSurveyProp } from './app';
import {
    clearBuildingGroupSelection,
    updateSitexBuildingGroupsLayer,
} from './groups';

const logger = debug('sdi:route');

// tslint:disable-next-line: variable-name
const RouteIO = io.union([
    io.literal('index'),
    io.literal('form'),
    io.literal('preview'),
    io.literal('checkout'),
    io.literal('history'),
    io.literal('groups'),
]);
export type Route = io.TypeOf<typeof RouteIO>;

export const { home, route, navigate } = Router<Route>('sitex');

export type Layout = Route | 'splash';

const surveyTypeRouteParser = (p: Path) =>
    scopeOption().let(
        'surveyType',
        index(0, p).chain(t => fromEither(SurveyTypeIO.decode(t)))
    );
const optSurveyTypeRouteParser = (p: Path) =>
    index(0, p).chain(t => fromEither(SurveyTypeIO.decode(t)));

const formRouteParser = (p: Path) =>
    surveyTypeRouteParser(p)
        .let(
            'id',
            index(1, p).chain(id => fromEither(uuidIO.decode(id)))
        )
        .let(
            'prop',
            some(
                index(2, p).chain(prop => fromEither(SurveyPropIO.decode(prop)))
            )
        );
const previewRouteParser = (p: Path) =>
    surveyTypeRouteParser(p).let(
        'id',
        index(1, p).chain(id => fromEither(uuidIO.decode(id)))
    );

/// << route handlers

home(
    'index',
    optSurveyType => {
        setLayout('index');
        optSurveyType.map(setSurveyType);
        resetSurveyProp();
        clearSelection();
        setTimeout(() => {
            mountLayers();
            optSurveyType.map(st => {
                if (st === 'building') {
                    updateSitexBuildingLayer();
                } else if (st === 'parcel') {
                    updateSitexParcelLayer();
                }
            });
        }, 300);
    },
    optSurveyTypeRouteParser
);

route(
    'form',
    route => {
        route.map(({ surveyType, id, prop }) => {
            setSurveyType(surveyType);
            setFormId(id);
            prop.map(prop => {
                setSurveyProp(prop);
                if (prop === 'geom') {
                    mountEditorLayers().then(startEditingGeometry);
                }
            });
            setLayout('form');
            prepareForm();
        });
        setTimeout(() => {
            mountLayers();
        }, 300);
    },
    formRouteParser
);

route(
    'preview',
    route => {
        route.map(({ surveyType, id }) => {
            setSurveyType(surveyType);
            setFormId(id);
            setLayout('preview');
        });
        setTimeout(() => {
            mountLayers();
        }, 300);
    },
    previewRouteParser
);

route(
    'history',
    route => {
        route.map(({ surveyType, id }) => {
            setSurveyType(surveyType);
            setFormId(id);
            setLayout('history');
        });
    },
    previewRouteParser
);

route('groups', () => {
    setLayout('groups');
    clearBuildingGroupSelection();
    setTimeout(() => {
        updateSitexBuildingGroupsLayer();
    }, 300);
});

route('checkout', () => {
    setLayout('checkout');
});

/// route handlers >>

const defaultRoute = () => navigate('index', []);

export const loadRoute = (initial: string[]) =>
    index(0, initial).foldL(defaultRoute, prefix =>
        RouteIO.decode(prefix).fold(defaultRoute, c =>
            navigate(c, initial.slice(1))
        )
    );

export const navigateIndex = (st: SurveyType) => {
    navigate('index', [st]);
};

export const navigateCheckout = () => {
    navigate('checkout', []);
};

export const navigateForm = (st: SurveyType, id: string) => {
    navigate('form', [st, id]);
};

export const navigateFormAtProp = (
    st: SurveyType,
    id: string,
    prop: SurveyProp
) => {
    navigate('form', [st, id, prop]);
};

export const navigateProp = (prop: SurveyProp) =>
    scopeOption()
        .let('id', getFormId())
        .let('st', getSurveyType())
        .map(({ st, id }) => navigateFormAtProp(st, id, prop));

export const navigatePreview = (st: SurveyType, id: string) => {
    navigate('preview', [st, id]);
};

export const navigateHistory = (st: SurveyType, id: string) => {
    navigate('history', [st, id]);
};

export const navigateGroups = () => {
    navigate('groups', []);
};

logger('loaded');
