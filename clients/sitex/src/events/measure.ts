import { measureEventsFactory } from 'sdi/map';
import { getInteraction } from '../queries';
import { setInteraction } from './map';

export const {
    startMeasureLength,
    startMeasureArea,
    stopMeasure,
    updateMeasureCoordinates,
} = measureEventsFactory(setInteraction, getInteraction);
