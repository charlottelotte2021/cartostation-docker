import * as debug from 'debug';
import { catOptions, index } from 'fp-ts/lib/Array';
import { fromEither, fromNullable, Option } from 'fp-ts/lib/Option';
import turfArea from '@turf/area';
import { iife } from 'sdi/lib';
import tr from 'sdi/locale';
import { addFeaturesToLayer, toLonLat } from 'sdi/map';
import online from 'sdi/online';
import { assign, assignK, dispatch } from 'sdi/shape';
import {
    Feature,
    MultiPolygon,
    MultiPolygonIO,
    onValidationError,
    remoteError,
    remoteLoading,
    remoteSuccess,
    uuid,
} from 'sdi/source';
import {
    updateDictObservation,
    updateGeometry,
    updateGeoState,
    updateNumber,
} from '../components/field';
import {
    buildingFormData,
    findUrbisBuilding,
    mainMapName,
    buildingFormDataToBuilding,
    BuildingFormData,
    getTouchedBuildings,
    findUrbisParcel,
    parcelFormData,
    ParcelFormData,
    getTouchedParcels,
    parcelFormDataToParcel,
    setBuildingGroups,
    getLayout,
    getFormId,
    getObservationDict,
} from '../queries';
import sitexBuilding from '../queries/map/sitex-building';
import urbisBuilding from '../queries/map/urbis-building';
import urbisParcel from '../queries/map/urbis-parcel';
import sitexParcel from '../queries/map/sitex-parcel';
import {
    Building,
    BuildingIO,
    fetchBuildingInBlock,
    fetchParcelInBlock,
    fetchUrbisAddressInBlock,
    fetchOccupations,
    fetchTypology,
    fetchStatecode,
    fetchUrbisBuildingInBlock,
    newBuilding,
    postBuilding,
    fetchUrbisParcelInBlock,
    newParcel,
    Parcel,
    postParcel,
    ParcelIO,
    fetchBuildingHistory,
    fetchBuildingGroup,
    fetchBuildingGroupSuggestion,
    SurveyType,
    fetchParcelHistory,
    fetchObservationStatus,
} from '../remote';
import {
    updateBlockBuildingGroupsState,
    setCurrentFormBuilding,
    navigateIndex,
    navigatePreview,
} from 'sitex/src/events';
import { updateSitexBuildingLayer, updateSitexParcelLayer } from './map';
import type { Layout } from './route';
import { statusError, statusInfo } from './status';

const logger = debug('sdi:events/app');

export const setLayout = (layout: Layout) => assign('app/layout', layout);

export const setSurveyType = assignK('form/survey-type');
export const resetSurveyType = () => setSurveyType(null);

export const setSurveyProp = assignK('form/prop');
export const resetSurveyProp = () => setSurveyProp(null);

export const setFormId = assignK('form/id');
export const resetFormId = () => setFormId(null);

export const resetForm = () => {
    resetSurveyType();
    resetSurveyProp();
    resetFormId();
    assign('form/building/current', null);
    assign('form/parcel/current', null);
    assign('form/block/current', null);
};

export const loadOccupations = () =>
    fetchOccupations().then(occups => assign('data/occupations', occups));

export const loadTypology = () =>
    fetchTypology().then(terms => assign('data/typology', terms));

export const loadStatecode = () =>
    fetchStatecode().then(codes => assign('data/statecode', codes));

export const loadObservation = () =>
    fetchObservationStatus().then(codes => assign('data/observation', codes));

export const loadBuildingGroup = () =>
    fetchBuildingGroup().then(groups => setBuildingGroups(groups));

export const loadBuildingGroupInBlock = (blockId: number) =>
    fetchBuildingGroupSuggestion(blockId).then(groups =>
        updateBlockBuildingGroupsState(blockId, groups)
    );

const onceUrbisBuildings = iife(() => {
    const rec: number[] = [];

    return (blockId: number, f: () => void) => {
        if (rec.indexOf(blockId) < 0) {
            rec.push(blockId);
            f();
        }
    };
});

const onceUrbisParcels = iife(() => {
    const rec: number[] = [];

    return (blockId: number, f: () => void) => {
        if (rec.indexOf(blockId) < 0) {
            rec.push(blockId);
            f();
        }
    };
});

const featureToBuilding = (f: Feature): Option<Building> => {
    const geom = f.geometry;
    const idbuild = f.id;
    return fromNullable(f.properties)
        .map(props => ({
            ...props,
            idbuild,
            geom,
            recordstate: fromNullable(props.recordstate).getOrElse('A'),
        }))
        .chain(props =>
            fromEither(
                BuildingIO.decode(props).mapLeft(
                    onValidationError(BuildingIO, 'featureToBuilding')
                )
            )
        );
};

const featureToParcel = (f: Feature): Option<Parcel> => {
    const geom = f.geometry;
    const idparcel = f.id;
    return fromNullable(f.properties)
        .map(props => ({
            ...props,
            idparcel,
            geom,
            recordstate: fromNullable(props.recordstate).getOrElse('A'),
        }))
        .chain(props =>
            fromEither(
                ParcelIO.decode(props).mapLeft(
                    onValidationError(ParcelIO, 'featureToParcel')
                )
            )
        );
};

export const loadBuildingInBlock = (blockId: number) =>
    fetchBuildingInBlock(blockId)
        .then(({ features }) => {
            addFeaturesToLayer(
                mainMapName,
                sitexBuilding.layer(),
                features.map(f => ({
                    ...f,
                    properties: { appStatus: 'old' },
                }))
            );
            const buildings = catOptions(features.map(featureToBuilding));
            const forms = buildings.map(buildingFormData);
            const ids = buildings.map(b => b.idbuild);
            const buildingFilter = (b: Building | BuildingFormData) =>
                ids.indexOf(b.idbuild) < 0;
            dispatch('data/buildings', bs =>
                bs.filter(buildingFilter).concat(buildings)
            );
            dispatch('form/buildings', fs =>
                fs.filter(buildingFilter).concat(forms)
            );
        })
        .then(() => loadBuildingGroupInBlock(blockId))
        .catch(err => logger('error loading buildings', err));

export const loadUrbisAndSitexBuildingInBlock = (blockId: number) =>
    onceUrbisBuildings(blockId, () =>
        fetchUrbisBuildingInBlock(blockId)
            .then(buildings => {
                addFeaturesToLayer(
                    mainMapName,
                    urbisBuilding.layer(),
                    buildings.features
                );
                dispatch('data/urbis-buildings', features => {
                    const listBuilding = features.concat(buildings.features);
                    updateBlockBuildingGroupsState(blockId, null, listBuilding);
                    return listBuilding;
                });

                statusInfo(
                    `${tr.sitex('loaded')} ${
                        buildings.features.length
                    } ${tr.sitex('buildingsFromUrbis')} `
                );
            })
            .then(() => loadBuildingInBlock(blockId))
            .catch(err => logger('error loading buildings', err))
    );

export const loadParcelInBlock = (blockId: number) =>
    fetchParcelInBlock(blockId)
        .then(({ features }) => {
            addFeaturesToLayer(
                mainMapName,
                sitexParcel.layer(),
                features.map(f => ({
                    ...f,
                    properties: { appStatus: 'old' },
                }))
            );
            const parcels = catOptions(features.map(featureToParcel));
            const forms = parcels.map(parcelFormData);
            const ids = parcels.map(p => p.idparcel);
            const parcelFilter = (p: Parcel | ParcelFormData) =>
                ids.indexOf(p.idparcel) < 0;
            dispatch('data/parcels', ps =>
                ps.filter(parcelFilter).concat(parcels)
            );
            dispatch('form/parcels', ps =>
                ps.filter(parcelFilter).concat(forms)
            );
        })
        .catch(err => logger('error loading parcels', err));

export const loadUrbisAndSitexParcelInBlock = (blockId: number) =>
    onceUrbisParcels(blockId, () =>
        fetchUrbisParcelInBlock(blockId)
            .then(parcels => {
                addFeaturesToLayer(
                    mainMapName,
                    urbisParcel.layer(),
                    parcels.features
                );
                dispatch('data/urbis-parcels', features =>
                    features.concat(parcels.features)
                );

                statusInfo(
                    `${tr.sitex('loaded')} ${
                        parcels.features.length
                    } ${tr.sitex('parcelsFromUrbis')} `
                );
            })
            .then(() => loadParcelInBlock(blockId))
            .catch(err => logger('error loading parcels', err))
    );

export const loadUrbisAddressInBlock = (blockId: number) =>
    fetchUrbisAddressInBlock(blockId)
        .then(urbisAddress => {
            dispatch('data/urbis-address', address =>
                address.concat(urbisAddress)
            );
        })
        .catch(err => logger('error loading address', err));

export const createFormFromUrbisBuilding = (urbisId: number) =>
    findUrbisBuilding(urbisId)
        .chain(({ geometry }) => fromEither(MultiPolygonIO.decode(geometry)))
        .map(mpoly => {
            const form = buildingFormData(newBuilding());
            form.geom = updateGeometry(mpoly);
            form.geostate = updateGeoState('V');
            form.u2building = updateNumber(urbisId);
            form.groundarea = updateNumber(
                Math.round(turfArea(toLonLat(mpoly)))
            );
            index(0, getObservationDict()).map(
                ({ id }) =>
                    (form.observation_status = updateDictObservation(id))
            );
            setCurrentFormBuilding(form);
            dispatch('form/buildings', bs => bs.concat({ ...form }));
            const feature: Feature = {
                id: form.idbuild,
                type: 'Feature',
                properties: {
                    appStatus: 'new-D',
                },
                geometry: mpoly,
            };
            addFeaturesToLayer(mainMapName, sitexBuilding.layer(), [feature]);
            return form.idbuild;
        });

export const createFormFromUrbisParcel = (urbisId: number) =>
    findUrbisParcel(urbisId)
        .chain(({ geometry }) => fromEither(MultiPolygonIO.decode(geometry)))
        .map(mpoly => {
            const form = parcelFormData(newParcel());
            form.geom = updateGeometry(mpoly);
            form.geostate = updateGeoState('V');
            form.u2parcel = updateNumber(urbisId);
            form.groundarea = updateNumber(
                Math.round(turfArea(toLonLat(mpoly)))
            );
            assign('form/parcel/current', form);
            dispatch('form/parcels', bs => bs.concat({ ...form }));
            const feature: Feature = {
                id: form.idparcel,
                type: 'Feature',
                properties: {
                    appStatus: 'new',
                },
                geometry: mpoly,
            };
            addFeaturesToLayer(mainMapName, sitexParcel.layer(), [feature]);
            return form.idparcel;
        });

export const createBuildingFormFromGeometry = (mpoly: MultiPolygon) => {
    const form = buildingFormData(newBuilding());
    form.geom = updateGeometry(mpoly);
    form.geostate = updateGeoState('D');
    form.groundarea = updateNumber(Math.round(turfArea(toLonLat(mpoly))));
    setCurrentFormBuilding(form);
    dispatch('form/buildings', bs => bs.concat({ ...form }));
    // const feature: Feature = {
    //     id: form.idbuild,
    //     type: 'Feature',
    //     properties: {
    //         appStatus: 'new',
    //     },
    //     geometry: mpoly,
    // };
    // addFeaturesToLayer(mainMapName, sitexBuilding.layer(), [feature]);
    updateSitexBuildingLayer();
    return form.idbuild;
};

export const createParcelFormFromGeometry = (mpoly: MultiPolygon) => {
    const form = parcelFormData(newParcel());
    form.geom = updateGeometry(mpoly);
    form.geostate = updateGeoState('D');
    form.groundarea = updateNumber(Math.round(turfArea(toLonLat(mpoly))));
    assign('form/parcel/current', form);
    dispatch('form/parcels', bs => bs.concat({ ...form }));
    // const feature: Feature = {
    //     id: form.idparcel,
    //     type: 'Feature',
    //     properties: {
    //         appStatus: 'new',
    //     },
    //     geometry: mpoly,
    // };
    // addFeaturesToLayer(mainMapName, sitexParcel.layer(), [feature]);
    updateSitexParcelLayer();
    return form.idparcel;
};

export const enterFullScreen = () =>
    document.body
        .requestFullscreen()
        .then(() => assign('app/fullscreen', true))
        .catch(err => logger('failed to get fullscreen', err));

export const exitFullScreen = () => {
    assign('app/fullscreen', false);
    document.exitFullscreen();
};

const saveBuilding = (building: Building) =>
    postBuilding(building)
        .then(feature =>
            featureToBuilding(feature)
                .map(data => {
                    dispatch('form/buildings', bs =>
                        bs
                            .filter(({ idbuild }) => idbuild !== data.idbuild)
                            .concat(buildingFormData(data))
                    );
                    dispatch('data/buildings', bs =>
                        bs
                            .filter(({ idbuild }) => idbuild !== data.idbuild)
                            .concat(data)
                    );
                    updateSitexBuildingLayer();
                })
                .getOrElseL(() => statusError('~failed to parse building'))
        )
        .catch(err => {
            statusError(tr.sitex('status/error/save-building'));
            logger('error saving building', err);
        });

export const syncBuildings = () => {
    const buildings = getTouchedBuildings().map(buildingFormDataToBuilding);
    const next = () => fromNullable(buildings.pop()).map(saveBuilding);
    const process = (): void => {
        next().map(p => p.then(process));
    };
    process();
};

const saveParcel = (parcel: Parcel) =>
    postParcel(parcel)
        .then(feature =>
            featureToParcel(feature)
                .map(data => {
                    dispatch('form/parcels', bs =>
                        bs
                            .filter(
                                ({ idparcel }) => idparcel !== data.idparcel
                            )
                            .concat(parcelFormData(data))
                    );
                    dispatch('data/parcels', bs =>
                        bs
                            .filter(
                                ({ idparcel }) => idparcel !== data.idparcel
                            )
                            .concat(data)
                    );
                    updateSitexParcelLayer();
                })
                .getOrElseL(() => statusError('~failed to parse parcel'))
        )
        .catch(err => {
            statusError(tr.sitex('status/error/save-parcel'));
            logger('error saving parcel', err);
        });

export const syncParcels = () => {
    const parcels = getTouchedParcels().map(parcelFormDataToParcel);
    const next = () => fromNullable(parcels.pop()).map(saveParcel);
    const process = (): void => {
        next().map(p => p.then(process));
    };
    process();
};

export const syncAll = () => {
    const buildings = getTouchedBuildings().map(buildingFormDataToBuilding);
    const nextBuilding = () => fromNullable(buildings.pop()).map(saveBuilding);

    const parcels = getTouchedParcels().map(parcelFormDataToParcel);
    const nextParcel = () => fromNullable(parcels.pop()).map(saveParcel);

    const process = (): void => {
        nextBuilding().map(p => p.then(process));
        nextParcel().map(p => p.then(process));
    };
    process();
};

export const monitorOnline = () =>
    online(quality => {
        assign('app/online', quality);
    });

export const loadBuildingHistory = (id: uuid) => {
    assign('history/building', remoteLoading);
    fetchBuildingHistory(id)
        .then(h => assign('history/building', remoteSuccess(h)))
        .catch(err => assign('history/building', remoteError(`${err}`)));
};

export const loadParcelHistory = (id: uuid) => {
    assign('history/parcel', remoteLoading);
    fetchParcelHistory(id)
        .then(h => assign('history/parcel', remoteSuccess(h)))
        .catch(err => assign('history/parcel', remoteError(`${err}`)));
};

export const switchSurveyType = (st: SurveyType) => {
    if (getLayout() === 'index') {
        navigateIndex(st);
    } else if (getLayout() === 'preview') {
        getFormId().foldL(
            () => navigateIndex(st),
            id => navigatePreview(st, id)
        );
    }
};

// TODO: later
// export const clearUntouchedBuildings = () => {
//     dispatch('form/buildings', forms =>
//         forms.filter(buildingFormDataIsTouched)
//     );
//     updateSitexBuildingLayer();
// };

// export const clearUntouchedParcels = () => {
//     dispatch('form/parcels', forms => forms.filter(parcelFormDataIsTouched));
//     updateSitexParcelLayer();
// };

logger('loaded');
