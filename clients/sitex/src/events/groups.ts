import { right } from 'fp-ts/lib/Either';
import { some } from 'fp-ts/lib/Option';
import { DirectGeometryObject, Feature } from 'sdi/source';
import { addLayer, checkMap, removeLayer, viewEventsFactory } from 'sdi/map';
import { getMessageRecord } from 'sdi/source';
import {
    BuildingFormData,
    findBuildingForm,
    getBuildings,
    getCurrentGroup,
    getCurrentGroupBuildingId,
} from '../queries';
import groups from '../queries/map/groups';
import { tryString } from 'sdi/util';
import { assign, dispatch, dispatchK } from 'sdi/shape';
import { postBuildingGroup } from '../remote';
import { updateBuildGroup } from '../components/field';

export const setGroupsView = dispatchK('form/groups/view');

export const updateGroupsView = viewEventsFactory(setGroupsView).updateMapView;

export const updateSitexBuildingGroupsLayer = () =>
    checkMap(groups.name).map(() => {
        removeLayer(groups.name, groups.name);

        const defaultGeometry: DirectGeometryObject = {
            type: 'Point',
            coordinates: [0, 0],
        };

        const currentId = getCurrentGroupBuildingId().toNullable();
        const currentGroupId = getCurrentGroup()
            .map(({ id }) => id)
            .toNullable();

        const className = ({ idbuild, buildgroup }: BuildingFormData) => {
            if (currentId === idbuild) {
                return 'selected-feature';
            } else if (buildgroup.value === null) {
                return 'none';
            } else if (buildgroup.value === currentGroupId) {
                return 'selected-group';
            }
            return 'some';
        };

        const features: Feature[] = getBuildings().map(form => ({
            type: 'Feature',
            id: form.idbuild,
            geometry: form.geom.value ?? defaultGeometry,
            properties: {
                prop: className(form),
            },
        }));

        addLayer(
            groups.name,
            () =>
                some({
                    name: getMessageRecord(groups.metadata.resourceTitle),
                    info: groups.layer(),
                    metadata: groups.metadata,
                }),
            () =>
                right(
                    some({
                        type: 'FeatureCollection',
                        features,
                    })
                )
        );
    });

export const clearBuildingGroupSelection = () => {
    assign('form/groups/current', null);
    updateSitexBuildingGroupsLayer();
};

export const selectBuildingGroup = (
    _layerId: string,
    featureId: string | number
) =>
    tryString(featureId)
        .chain(findBuildingForm)
        // .chain(({ buildgroup }) => fromNullable(buildgroup.value))
        // .chain(findGroup)
        .map(({ idbuild, buildgroup }) => {
            getCurrentGroup()
                .map(({ id }) => {
                    // if (id === buildgroup.value) {
                    //     clearBuildingGroup(idbuild);
                    //     return;
                    // } else
                    if (buildgroup.value === null) {
                        assignGroupToBuilding(idbuild, id);
                    }
                    selectGroupCurrentBuilding(idbuild);
                })
                .getOrElseL(() => selectGroupCurrentBuilding(idbuild));

            updateSitexBuildingGroupsLayer();
        })
        .getOrElseL(clearBuildingGroupSelection);

export const setGroupNameInput = (name: string) =>
    assign('form/groups/name', name);

export const clearGroupNameInput = () => assign('form/groups/name', null);

export const createGroup = (name: string) =>
    postBuildingGroup(name).then(group => {
        dispatch('data/building-group', groups => groups.concat(group));
        return group;
    });
//.catch(noop); // TODO

export const selectGroupCurrentBuilding = (idbuild: string) => {
    assign('form/groups/current', idbuild);
    updateSitexBuildingGroupsLayer();
};

export const assignGroupToBuilding = (bid: string, gid: string) => {
    dispatch('form/buildings', forms =>
        forms.map(form => {
            if (form.idbuild === bid) {
                return {
                    ...form,
                    buildgroup: updateBuildGroup(gid),
                };
            } else {
                return form;
            }
        })
    );
    updateSitexBuildingGroupsLayer();
};
export const clearBuildingGroup = (bid: string) => {
    dispatch('form/buildings', forms =>
        forms.map(form => {
            if (form.idbuild === bid) {
                return {
                    ...form,
                    buildgroup: updateBuildGroup(null),
                };
            } else {
                return form;
            }
        })
    );
    assign('form/groups/current', null);
    updateSitexBuildingGroupsLayer();
};
