import { tryNumber } from 'sdi/util';
import { fromEither, fromNullable } from 'fp-ts/lib/Option';
import { assign, dispatch } from 'sdi/shape';
import { Feature } from 'sdi/source';
import {
    remoteError,
    remoteLoading,
    remoteNone,
    remoteSuccess,
} from 'sdi/source';
import { cloneValue, nullifyValue } from '../components/field';
import { CodePrefix } from '../components/field';
import {
    BuildingFormData,
    findBuildingForm,
    findParcelForm,
    getBuildingGroups,
    getCurrentBuildingForm,
    getCurrentParcelForm,
    getFormId,
    getOccupationIndex,
    getSurveyType,
    ParcelFormData,
} from '../queries';
import {
    BuildingOccupation,
    BuildingOccupationArrayIO,
    BuildingProp,
    BuildingGroup,
    newBuildingOccupation,
    ValidOccupationCode,
    uploadBuildingFile,
    ParcelProp,
    ParcelOccupationArrayIO,
    newParcelOccupation,
    ParcelOccupation,
    uploadParcelFile,
    NULL_OCCUPATION_CODE,
    SurveyType,
} from '../remote';
import { updateSitexBuildingLayer } from '.';

// export const setBuildingFieldValue = <P extends BuildingProp>(
//     prop: P,
//     value: Building[P]
// ) => getFormId()
//     .map(id => dispatch('form/buildings', (buildings) => {
//         const buildingForm = buildings.find(({ data }) => data.idbuild === id)
//         if (buildingForm === undefined) {
//             return buildings
//         }
//         const building = buildingFormDataToBuilding(buildingForm.data)
//         building[prop] = value;
//         buildingForm.touched = true
//         buildingForm.data = buildingFormData(building)
//         return buildings.filter(({ data }) => data.idbuild !== id).concat(buildingForm)
//     }))

export const setBuildingFormFieldValue =
    <P extends BuildingProp>(prop: P) =>
    (value: BuildingFormData[P]) =>
        dispatch('form/building/current', buildingForm =>
            fromNullable(buildingForm)
                .map(form => {
                    form[prop] = value;
                    return form;
                })
                .toNullable()
        );

export const commitBuildingForm = () =>
    getCurrentBuildingForm().map(form => {
        dispatch('form/buildings', bs => {
            const buildGroup = form.buildgroup.value;
            if (buildGroup) {
                const blockId = fromNullable(form.u2block.value).getOrElse(0);
                const groups = [
                    getBuildingGroups().find(g => g.id === buildGroup),
                ] as BuildingGroup[];
                if (blockId && groups.length > 0) {
                    updateBlockBuildingGroupsState(blockId, groups);
                }
            }
            return bs.filter(b => b.idbuild !== form.idbuild).concat(form);
        });
        updateSitexBuildingLayer();
    });

export const clearBuildingProp = (prop: BuildingProp) =>
    getFormId()
        .chain(findBuildingForm)
        .map(baseForm =>
            dispatch('form/building/current', current =>
                fromNullable(current)
                    .map(currentForm => {
                        return {
                            ...currentForm,
                            [prop]: cloneValue(baseForm[prop]),
                        };
                    })
                    .toNullable()
            )
        );

export const nullifyBuildingProp = (prop: BuildingProp) =>
    dispatch('form/building/current', current =>
        fromNullable(current)
            .map(form => {
                return {
                    ...form,
                    [prop]: nullifyValue(form[prop]),
                };
            })
            .toNullable()
    );

export const setBuildingOccupationIndex = (index: number) =>
    dispatch('form/building/occupation', o => ({ ...o, index }));

export const codeToPrefix = (code: ValidOccupationCode): CodePrefix =>
    code
        .split('.')
        .reduce((acc, part) => (part === '00' ? acc : [...acc, part]), [])
        .slice(0, 4) as CodePrefix;

export const prefixToCode = (prefix: CodePrefix) => {
    switch (prefix.length) {
        case 0:
            return NULL_OCCUPATION_CODE;
        case 1:
            return `${prefix[0]}.00.00.00`;
        case 2:
            return `${prefix[0]}.${prefix[1]}.00.00`;
        case 3:
            return `${prefix[0]}.${prefix[1]}.${prefix[2]}.00`;
        case 4:
            return `${prefix[0]}.${prefix[1]}.${prefix[2]}.${prefix[3]}`;
    }
};

export const setBuildingOccupationPrefix = (code: ValidOccupationCode) =>
    dispatch('form/building/occupation', o => ({
        ...o,
        prefix: codeToPrefix(code),
    }));

export const clearBuildingOccupationPrefix = () =>
    dispatch('form/building/occupation', o => ({ ...o, prefix: [] }));

const setBuildingOccupationLookup = (lookup: string) =>
    dispatch('form/building/occupation', o => ({ ...o, lookup }));

export const setOccupationLookup = (st: SurveyType, lookup: string) => {
    switch (st) {
        case 'building':
            return setBuildingOccupationLookup(lookup);
        case 'parcel':
            return setParcelOccupationLookup(lookup);
    }
};

export const clearBuildingOccupationLookup = () =>
    dispatch('form/building/occupation', o => ({ ...o, lookup: null }));

export const setBuildingGroupLookup = (lookup: string) =>
    dispatch('form/building/group', g => ({ ...g, lookup }));

export const clearBuildingGroupLookup = () =>
    dispatch('form/building/group', g => ({ ...g, lookup: null }));

export const getBuildingOccupationHigestLevel = (
    occupation: BuildingOccupation[]
) => Math.max(...occupation.map(o => tryNumber(o.level).getOrElse(0)));

/**
 * add a new occupation to an existing list
 * NOTE! it will **silently** fail on a None value
 * @param prop BuildingProp
 * @param index number
 * @returns void
 */
export const insertNewBuildingOccupation = (prop: BuildingProp) =>
    dispatch('form/building/current', buildingForm =>
        fromNullable(buildingForm)
            .map(form => {
                const value = form[prop].value;
                fromEither(BuildingOccupationArrayIO.decode(value)).map(
                    occups => {
                        const newLevel =
                            getBuildingOccupationHigestLevel(occups) + 1;
                        const occup = newBuildingOccupation(newLevel);
                        getCurrentBuildingForm()
                            .chain(form => fromNullable(form.groundarea.value))
                            .map(area => (occup.area = area));
                        const index = occups.length;
                        occups.splice(index, 0, occup);
                        setBuildingOccupationIndex(index);
                    }
                );
                return form;
            })
            .toNullable()
    );

export const insertNewBuildingOccupationAtCurrentLevel = (prop: BuildingProp) =>
    dispatch('form/building/current', buildingForm =>
        fromNullable(buildingForm)
            .map(form => {
                const value = form[prop].value;
                fromEither(BuildingOccupationArrayIO.decode(value)).map(
                    occups => {
                        fromNullable(
                            occups[getOccupationIndex('building')]
                        ).map(current => {
                            const newLevel = current.level;
                            const occup = newBuildingOccupation(newLevel);
                            getCurrentBuildingForm()
                                .chain(form =>
                                    fromNullable(form.groundarea.value)
                                )
                                .map(area => {
                                    const taken = occups.reduce(
                                        (acc, o) =>
                                            o.level === newLevel &&
                                            o.area !== null
                                                ? acc + o.area
                                                : acc,
                                        0
                                    );
                                    occup.area = area - taken;
                                });
                            const index = getOccupationIndex('building') + 1;
                            occups.splice(index, 0, occup);
                            setBuildingOccupationIndex(index);
                        });
                    }
                );
                return form;
            })
            .toNullable()
    );

export const removeBuildingOccupation = (prop: BuildingProp, index: number) =>
    dispatch('form/building/current', buildingForm =>
        fromNullable(buildingForm)
            .map(form => {
                const value = form[prop].value;
                if (Array.isArray(value)) {
                    value.splice(index, 1);
                    const occupation = fromEither(
                        BuildingOccupationArrayIO.decode(value)
                    );
                    const totalOccupation = occupation.getOrElse([]).length - 1;
                    // a hack to keep user on the occupation input section
                    // without this, any time user delete an occupation, it will reset the occupation and caused the section goes empty
                    setBuildingOccupationIndex(totalOccupation);
                }
                return form;
            })
            .toNullable()
    );

export const updateIndexBuildingOccupationEdited = (
    prop: BuildingProp,
    o: BuildingOccupation
) => {
    const checkOccup = Object.entries(o).toString();
    getCurrentBuildingForm().map(form => {
        const updatedOccup = form[prop].value;
        fromEither(BuildingOccupationArrayIO.decode(updatedOccup)).map(
            occupation => {
                const occupEditedIndex = occupation.findIndex(
                    ocp => Object.entries(ocp).toString() === checkOccup
                );
                if (occupEditedIndex > -1) {
                    setBuildingOccupationIndex(occupEditedIndex);
                }
            }
        );
    });
};

/**
 * insert a clone of an occupation to an existing list
 * NOTE! it will **silently** fail on a None value
 * @param prop BuildingProp
 * @param index number
 * @returns void
 */
export const insertCloneBuildingOccupation = (
    prop: BuildingProp,
    baseOccup: BuildingOccupation
) =>
    dispatch('form/building/current', buildingForm =>
        fromNullable(buildingForm)
            .map(form => {
                const value = form[prop].value;
                fromEither(BuildingOccupationArrayIO.decode(value)).map(
                    occups => {
                        const newLevel =
                            getBuildingOccupationHigestLevel(occups) + 1;
                        const occup = newBuildingOccupation(newLevel);
                        occup.area = baseOccup.area;
                        occup.description = baseOccup.description;
                        occup.nbhousing = baseOccup.nbhousing;
                        occup.occupcode = baseOccup.occupcode;
                        occup.owner = baseOccup.owner;
                        occup.vacant = baseOccup.vacant;
                        const index = occups.length;
                        occups.splice(index, 0, occup);
                        setBuildingOccupationIndex(index);
                    }
                );
                return form;
            })
            .toNullable()
    );

export const processUploadBuildingFile = (file: File) => {
    assign('form/building/upload-file', remoteLoading);
    return uploadBuildingFile(file)
        .then(data => {
            assign('form/building/upload-file', remoteSuccess(data));
            return data;
        })
        .catch(() => {
            assign(
                'form/building/upload-file',
                remoteError(`failed to upload ${file.name}`)
            );
            return null;
        });
};

export const clearUploadBuildingFile = () =>
    assign('form/building/upload-file', remoteNone);

// TODO parcel

export const setParcelFormFieldValue =
    <P extends ParcelProp>(prop: P) =>
    (value: ParcelFormData[P]) =>
        dispatch('form/parcel/current', parcelForm =>
            fromNullable(parcelForm)
                .map(form => {
                    form[prop] = value;
                    return form;
                })
                .toNullable()
        );

export const commitParcelForm = () =>
    getCurrentParcelForm().map(form =>
        dispatch('form/parcels', bs =>
            bs.filter(b => b.idparcel !== form.idparcel).concat(form)
        )
    );

export const clearParcelProp = (prop: ParcelProp) =>
    getFormId()
        .chain(findParcelForm)
        .map(baseForm =>
            dispatch('form/parcel/current', current =>
                fromNullable(current)
                    .map(currentForm => {
                        return {
                            ...currentForm,
                            [prop]: cloneValue(baseForm[prop]),
                        };
                    })
                    .toNullable()
            )
        );

export const nullifyParcelProp = (prop: ParcelProp) =>
    dispatch('form/parcel/current', current =>
        fromNullable(current)
            .map(form => {
                return {
                    ...form,
                    [prop]: nullifyValue(form[prop]),
                };
            })
            .toNullable()
    );

export const setParcelOccupationIndex = (index: number) =>
    dispatch('form/parcel/occupation', o => ({ ...o, index }));

export const setParcelOccupationPrefix = (code: ValidOccupationCode) =>
    dispatch('form/parcel/occupation', o => ({
        ...o,
        prefix: codeToPrefix(code),
    }));

export const clearParcelOccupationPrefix = () =>
    dispatch('form/parcel/occupation', o => ({ ...o, prefix: [] }));

const setParcelOccupationLookup = (lookup: string) =>
    dispatch('form/parcel/occupation', o => ({ ...o, lookup }));

export const clearParcelOccupationLookup = () =>
    dispatch('form/parcel/occupation', o => ({ ...o, lookup: null }));

/**
 * add a new occupation to an existing list
 * NOTE! it will **silently** fail on a None value
 * @param prop BuildingProp
 * @param index number
 * @returns void
 */
export const insertNewParcelOccupation = (prop: ParcelProp, index: number) =>
    dispatch('form/parcel/current', parcelForm =>
        fromNullable(parcelForm)
            .map(form => {
                const value = form[prop].value;
                fromEither(ParcelOccupationArrayIO.decode(value)).map(occups =>
                    occups.splice(index, 0, newParcelOccupation())
                );
                return form;
            })
            .toNullable()
    );

/**
 * insert a clone of an occupation to an existing list
 * NOTE! it will **silently** fail on a None value
 * @param prop BuildingProp
 * @param index number
 * @returns void
 */
export const insertCloneParcelOccupation = (
    prop: ParcelProp,
    index: number,
    baseOccup: ParcelOccupation
) =>
    dispatch('form/parcel/current', parcelForm =>
        fromNullable(parcelForm)
            .map(form => {
                const value = form[prop].value;
                fromEither(ParcelOccupationArrayIO.decode(value)).map(
                    occups => {
                        const occup = newParcelOccupation();
                        occup.area = baseOccup.area;
                        occup.description = baseOccup.description;
                        occup.occupcode = baseOccup.occupcode;
                        occup.owner = baseOccup.owner;
                        occup.vacant = baseOccup.vacant;
                        occups.splice(index, 0, occup);
                    }
                );
                return form;
            })
            .toNullable()
    );

export const processUploadParcelFile = (file: File) => {
    assign('form/parcel/upload-file', remoteLoading);
    uploadParcelFile(file)
        .then(data => assign('form/parcel/upload-file', remoteSuccess(data)))
        .catch(() =>
            assign(
                'form/parcel/upload-file',
                remoteError(`failed to upload ${file.name}`)
            )
        );
};

export const clearUploadParcelFile = () =>
    assign('form/parcel/upload-file', remoteNone);

export const prepareForm = () =>
    getSurveyType().map(surveyType => {
        switch (surveyType) {
            case 'building':
                return prepareFormBuilding();
            case 'parcel':
                return prepareFormParcel();
            case 'public-space':
                return;
        }
    });
const prepareFormBuilding = () =>
    getCurrentBuildingForm().map(form => {
        fromNullable(form.occupancys.value).map(occups =>
            setBuildingOccupationIndex(occups.length - 1)
        );
    });
const prepareFormParcel = () =>
    getCurrentParcelForm().map(form => {
        fromNullable(form.occupancys.value).map(occups =>
            setParcelOccupationIndex(occups.length - 1)
        );
    });

const filterGroupUnique = (groups: BuildingGroup[]) =>
    groups.filter(
        (group, index, self) => index === self.findIndex(g => g.id === group.id)
    );

const filteBuildingUnique = (buildings: Feature[]) =>
    buildings.filter(
        (building, index, self) =>
            index === self.findIndex(b => b.id === building.id)
    );

export const updateBlockBuildingGroupsState = (
    blockId: number,
    groups: BuildingGroup[] | null = null,
    buildings: Feature[] | null = null
) => {
    dispatch('data/building-groups-suggestion', blockInfo => {
        const block = blockInfo.find(info => info.blockId === blockId);
        if (block) {
            if (groups && buildings) {
                const newGroups = filterGroupUnique(
                    fromNullable(block.groups).getOrElse([]).concat(groups)
                );
                const newBuildings = filteBuildingUnique(
                    fromNullable(block.urbisBuildings)
                        .getOrElse([])
                        .concat(buildings)
                );
                return blockInfo
                    .filter(b => b.blockId !== blockId)
                    .concat([
                        {
                            blockId,
                            groups: newGroups,
                            urbisBuildings: newBuildings,
                        },
                    ]);
            } else if (groups && !buildings) {
                const newGroups = filterGroupUnique(
                    fromNullable(block.groups).getOrElse([]).concat(groups)
                );
                return blockInfo
                    .filter(b => b.blockId !== blockId)
                    .concat([
                        {
                            blockId,
                            groups: newGroups,
                            urbisBuildings: block.urbisBuildings,
                        },
                    ]);
            } else if (!groups && buildings) {
                const newBuildings = filteBuildingUnique(
                    fromNullable(block.urbisBuildings)
                        .getOrElse([])
                        .concat(buildings)
                );
                return blockInfo
                    .filter(b => b.blockId !== blockId)
                    .concat([
                        {
                            blockId,
                            groups: block.groups,
                            urbisBuildings: newBuildings,
                        },
                    ]);
            }
        }
        return blockInfo.concat([
            {
                blockId,
                groups,
                urbisBuildings: buildings,
            },
        ]);
    });
};
