import { navigateProp } from 'sitex/src/events';
import { fromNullable, none, some } from 'fp-ts/lib/Option';
import { right } from 'fp-ts/lib/Either';

import {
    addLayer,
    checkMap,
    FetchData,
    removeLayer,
    viewEventsFactory,
    nullFeaturePath,
    defaultInteraction,
    removeLayerAll,
} from 'sdi/map';
import * as debug from 'debug';
import { assign, assignK, dispatchK, query } from 'sdi/shape';
import {
    FeatureCollection,
    remoteError,
    remoteLoading,
    remoteSuccess,
    getMessageRecord,
    Feature,
    DirectGeometryObject,
    MultiPolygon,
} from 'sdi/source';
import { condOnce, tryNumber, tryString, updateCollection } from 'sdi/util';
import {
    findBuildingForm,
    getLayerData,
    getTouchedBuildings,
    getUntouchedBuildings,
    mainMapName,
    getSurveyType,
    getUntouchedParcels,
    getTouchedParcels,
    findParcelForm,
    getCurrentBuildingForm,
    getCreateGeometry,
    BuildingFormData,
    getGroupSuggestionByBuilding,
    getCurrentParcelForm,
} from '../queries';
import { updateNumber } from 'sitex/src/components/field';
import { fetchAllBlock, SurveyType } from '../remote';
import {
    createFormFromUrbisBuilding,
    loadUrbisAndSitexBuildingInBlock,
    loadUrbisAddressInBlock,
    loadUrbisAndSitexParcelInBlock,
    createFormFromUrbisParcel,
    createBuildingFormFromGeometry,
} from './app';
import { navigateForm, navigatePreview } from './route';
import urbisBlock from '../queries/map/urbis-block';
import urbisBuilding from '../queries/map/urbis-building';
import urbisParcel from '../queries/map/urbis-parcel';
import geometryMap from '../queries/map/geometry';
import sitexBuilding from '../queries/map/sitex-building';
import sitexParcel from '../queries/map/sitex-parcel';
import { createParcelFormFromGeometry } from '.';
import { iife } from 'sdi/lib';

export const logger = debug('sdi:events/map');

export const setView = dispatchK('port/map/view');

export const setInteraction = dispatchK('port/map/interaction');

export const updateView = viewEventsFactory(setView).updateMapView;

export const setEditorView = dispatchK('form/geometry/view');

export const updateEditorView = viewEventsFactory(setEditorView).updateMapView;

const dispatchLayers = dispatchK('data/layers');

export const setBaseLayerCode = assignK('port/map/baselayer');

const loadRemoteBlocks = (
    name: string,
    fetchFunction: () => Promise<FeatureCollection>
) => {
    const layers = query('data/layers');
    if (name in layers) {
        return layers[name].tag;
    }
    dispatchLayers(coll =>
        updateCollection(coll, urbisBlock.name, remoteLoading)
    );
    fetchFunction()
        .then(fc => {
            // const dbg = fc.features.filter(({ properties }) =>
            //     tryNumber(properties!['sitex_building_count'])
            //         .map(n => n > 0)
            //         .getOrElse(false)
            // );
            // logger('DBG', dbg);
            dispatchLayers(coll =>
                updateCollection(coll, urbisBlock.name, remoteSuccess(fc))
            );
            mountBlockLayer();
        })
        .catch(err =>
            dispatchLayers(coll =>
                updateCollection(coll, urbisBlock.name, remoteError(err))
            )
        );
    return 'loading';
};

export const loadBlockLayer = () =>
    loadRemoteBlocks(urbisBlock.name, fetchAllBlock);

const mountBlockLayer = () => {
    removeLayer(mainMapName, urbisBlock.name);
    addLayer(
        mainMapName,
        () =>
            some({
                name: getMessageRecord(urbisBlock.metadata.resourceTitle),
                info: urbisBlock.layer(),
                metadata: urbisBlock.metadata,
            }),
        getLayerData(urbisBlock.name)
    );
};

const baseMountLayer = () =>
    checkMap(mainMapName)
        .map(() => {
            mountBlockLayer();

            const emptyFetcher: FetchData = () =>
                right(some({ type: 'FeatureCollection', features: [] }));
            addLayer(
                mainMapName,
                () =>
                    some({
                        name: getMessageRecord(
                            urbisBuilding.metadata.resourceTitle
                        ),
                        info: urbisBuilding.layer(),
                        metadata: urbisBuilding.metadata,
                    }),
                emptyFetcher
            );
            addLayer(
                mainMapName,
                () =>
                    some({
                        name: getMessageRecord(
                            urbisParcel.metadata.resourceTitle
                        ),
                        info: urbisParcel.layer(),
                        metadata: urbisParcel.metadata,
                    }),
                emptyFetcher
            );
            addLayer(
                mainMapName,
                () =>
                    some({
                        name: getMessageRecord(
                            sitexBuilding.metadata.resourceTitle
                        ),
                        info: sitexBuilding.layer(),
                        metadata: sitexBuilding.metadata,
                    }),
                emptyFetcher
            );
            addLayer(
                mainMapName,
                () =>
                    some({
                        name: getMessageRecord(
                            sitexParcel.metadata.resourceTitle
                        ),
                        info: sitexParcel.layer(),
                        metadata: sitexParcel.metadata,
                    }),
                emptyFetcher
            );
            addLayer(
                mainMapName,
                () =>
                    some({
                        name: getMessageRecord(
                            geometryMap.metadata.resourceTitle
                        ),
                        info: geometryMap.layer(),
                        metadata: geometryMap.metadata,
                    }),
                emptyFetcher
            );
            return true;
        })
        .getOrElse(false);

export const updateSitexBuildingLayer = () =>
    checkMap(mainMapName).map(() => {
        removeLayer(mainMapName, sitexBuilding.name);

        const defaultGeometry: DirectGeometryObject = {
            type: 'Point',
            coordinates: [0, 0],
        };
        const syncedFeatures: Feature[] = getUntouchedBuildings().map(
            ({ idbuild, geom }) => ({
                type: 'Feature',
                id: idbuild,
                geometry: geom.value ?? defaultGeometry,
                properties: {
                    appStatus: 'old',
                },
            })
        );
        const editedFeatures: Feature[] = getTouchedBuildings().map(
            ({ idbuild, geom, datastate }) => ({
                type: 'Feature',
                id: idbuild,
                geometry: geom.value ?? defaultGeometry,
                properties: {
                    appStatus: fromNullable(datastate.value)
                        .map(s => `new-${s}`)
                        .getOrElse('new'),
                },
            })
        );
        addLayer(
            mainMapName,
            () =>
                some({
                    name: getMessageRecord(
                        sitexBuilding.metadata.resourceTitle
                    ),
                    info: sitexBuilding.layer(),
                    metadata: sitexBuilding.metadata,
                }),
            () =>
                right(
                    some({
                        type: 'FeatureCollection',
                        features: syncedFeatures.concat(editedFeatures),
                    })
                )
        );
    });

export const updateSitexParcelLayer = () =>
    checkMap(mainMapName).map(() => {
        removeLayer(mainMapName, sitexParcel.name);

        const defaultGeometry: DirectGeometryObject = {
            type: 'Point',
            coordinates: [0, 0],
        };
        const syncedFeatures: Feature[] = getUntouchedParcels().map(
            ({ idparcel, geom }) => ({
                type: 'Feature',
                id: idparcel,
                geometry: geom.value ?? defaultGeometry,
                properties: {
                    appStatus: 'old',
                },
            })
        );
        const editedFeatures: Feature[] = getTouchedParcels().map(
            ({ idparcel, geom }) => ({
                type: 'Feature',
                id: idparcel,
                geometry: geom.value ?? defaultGeometry,
                properties: {
                    appStatus: 'new',
                },
            })
        );

        addLayer(
            mainMapName,
            () =>
                some({
                    name: getMessageRecord(sitexParcel.metadata.resourceTitle),
                    info: sitexParcel.layer(),
                    metadata: sitexParcel.metadata,
                }),
            () =>
                right(
                    some({
                        type: 'FeatureCollection',
                        features: syncedFeatures.concat(editedFeatures),
                    })
                )
        );
    });

export const mountLayers = condOnce(baseMountLayer);

// const findBlockFromFeature = (
//     id: number
// ) => {
//     const layers = query('data/layers')
//     const resource = layers[urbisBlock.name]
//     return remoteToOption(resource).chain(fc => fromNullable(fc.features.find(f => f.id === id)))
// }

export const setCurrentFormBuilding = (form: BuildingFormData) => {
    if (form.u2building.value && !form.u2block.value) {
        const blockInfo = getGroupSuggestionByBuilding(form.u2building.value);
        if (blockInfo) {
            form.u2block = updateNumber(blockInfo.blockId);
        }
    }
    assign('form/building/current', form);
    assign('form/block/current', form.u2block.value);
};

export const setSelection = (layerId: string, featureId: string | number) =>
    assign('port/map/selection', { layerId, featureId });

export const clearSelection = () =>
    assign('port/map/selection', nullFeaturePath());

export const selectFeature = (layerId: string, featureId: string | number) => {
    if (layerId === urbisBlock.name) {
        tryNumber(featureId).map(fid => {
            assign('form/block/current', fid);
            loadUrbisAndSitexBuildingInBlock(fid);
            loadUrbisAndSitexParcelInBlock(fid);
            loadUrbisAddressInBlock(fid);
        });
    } else if (layerId === urbisBuilding.name) {
        tryNumber(featureId)
            .chain(createFormFromUrbisBuilding)
            .map(id => {
                navigateForm('building', id);
                // focus on building nblevel input
                setTimeout(() => {
                    navigateProp('nblevel');
                }, 500);
            });
    } else if (layerId === urbisParcel.name) {
        tryNumber(featureId)
            .chain(createFormFromUrbisParcel)
            .map(id => navigateForm('parcel', id));
    } else if (layerId === sitexBuilding.name) {
        tryString(featureId)
            .chain(findBuildingForm)
            .map(form => {
                setCurrentFormBuilding({ ...form });
                navigatePreview('building', form.idbuild);
                setSelection(layerId, featureId);
            });
    } else if (layerId === sitexParcel.name) {
        tryString(featureId)
            .chain(findParcelForm)
            .map(form => {
                assign('form/parcel/current', { ...form });
                navigatePreview('parcel', form.idparcel);
                setSelection(layerId, featureId);
            });
    }
};

export const mountEditorLayers = (): Promise<unknown> => {
    const inner = (resolve: (value: unknown) => void): unknown =>
        checkMap(geometryMap.name)
            .map(() => {
                removeLayerAll(geometryMap.name);
                getSurveyType()
                    .chain(st =>
                        st === 'public-space'
                            ? none
                            : st === 'building'
                            ? some(urbisBuilding)
                            : some(urbisParcel)
                    )
                    .map(ns => {
                        const features = iife(() => {
                            if (ns.name === urbisBuilding.name) {
                                return query(
                                    'data/urbis-buildings'
                                ) as Feature[];
                            }
                            return query('data/urbis-parcels') as Feature[];
                        });
                        const urbisFetcher: FetchData = () =>
                            right(
                                some({ type: 'FeatureCollection', features })
                            );
                        addLayer(
                            geometryMap.name,
                            () =>
                                some({
                                    name: getMessageRecord(
                                        ns.metadata.resourceTitle
                                    ),
                                    info: ns.layer(),
                                    metadata: ns.metadata,
                                }),
                            urbisFetcher
                        );
                    });

                geometryMap.incrFeatureId();
                const emptyFetcher: FetchData = () =>
                    right(some({ type: 'FeatureCollection', features: [] }));
                const fetcher: FetchData = getSurveyType()
                    .map(surveyType => {
                        switch (surveyType) {
                            case 'building': {
                                const featureCollectionOpt =
                                    getCurrentBuildingForm()
                                        .chain(form =>
                                            fromNullable(form.geom.value)
                                        )
                                        .map<FeatureCollection>(geometry => ({
                                            type: 'FeatureCollection',
                                            features: [
                                                {
                                                    type: 'Feature',
                                                    id: geometryMap.getFeatureId(),
                                                    geometry,
                                                    properties: {},
                                                },
                                            ],
                                        }));
                                return (() =>
                                    right(
                                        featureCollectionOpt
                                    )) as unknown as FetchData;
                            }
                            case 'parcel': {
                                const featureCollectionOpt =
                                    getCurrentParcelForm()
                                        .chain(form =>
                                            fromNullable(form.geom.value)
                                        )
                                        .map<FeatureCollection>(geometry => ({
                                            type: 'FeatureCollection',
                                            features: [
                                                {
                                                    type: 'Feature',
                                                    id: geometryMap.getFeatureId(),
                                                    geometry,
                                                    properties: {},
                                                },
                                            ],
                                        }));
                                return (() =>
                                    right(
                                        featureCollectionOpt
                                    )) as unknown as FetchData;
                            }
                            case 'public-space':
                                return emptyFetcher;
                        }
                    })
                    .getOrElse(emptyFetcher);
                addLayer(
                    geometryMap.name,
                    () =>
                        some({
                            name: getMessageRecord(
                                geometryMap.metadata.resourceTitle
                            ),
                            info: geometryMap.layer(),
                            metadata: geometryMap.metadata,
                        }),
                    fetcher
                );

                resolve(0);
            })
            .getOrElseL(() => setTimeout(() => inner(resolve), 60));
    return new Promise(inner);
};

export const startEditingGeometry = () =>
    assign('form/geometry/interaction', {
        label: 'modify',
        state: {
            geometryType: 'MultiPolygon',
            selected: geometryMap.getFeatureId(),
            centerOnSelected: true,
        },
    });

export const startCreatingGeometry = () =>
    assign('port/map/interaction', {
        label: 'create',
        state: {
            geometryType: 'MultiPolygon',
        },
    });

export const resetMapInteraction = () =>
    assign('port/map/interaction', defaultInteraction());

export const setCreateGeometry = (geometry: MultiPolygon) => {
    assign('port/map/create', geometry);
    const createFeatureId = 1;
    removeLayer(mainMapName, geometryMap.name);
    const fetcher: FetchData = () =>
        right(
            some({
                type: 'FeatureCollection',
                features: [
                    {
                        type: 'Feature',
                        id: createFeatureId,
                        geometry,
                        properties: {},
                    },
                ],
            })
        );
    const info = () =>
        some({
            name: getMessageRecord(geometryMap.metadata.resourceTitle),
            info: geometryMap.layer(),
            metadata: geometryMap.metadata,
        });
    addLayer(mainMapName, info, fetcher);
    assign('port/map/interaction', {
        label: 'modify',
        state: {
            geometryType: 'MultiPolygon',
            selected: createFeatureId,
            centerOnSelected: true,
        },
    });
};

export const updateCreateGeometry = (geom: MultiPolygon) => {
    assign('port/map/create', geom);
};

export const clearCreateGeometry = () => assign('port/map/create', null);

export const createFormFromGeometry = (surveyType: SurveyType) =>
    getCreateGeometry().chain(geom => {
        clearGeometryLayer();
        switch (surveyType) {
            case 'building':
                return some(createBuildingFormFromGeometry(geom));
            case 'parcel':
                return some(createParcelFormFromGeometry(geom));
            case 'public-space':
                return none;
        }
    });

export const clearGeometryLayer = () => {
    removeLayer(mainMapName, geometryMap.name);
};

logger('loaded');
