# Introduction and generalities

-   [What is _cartostation_?](#what-is-cartostation)
-   [Project philosophy](#project-philosophy)
-   [Applications and modularity](#applications-and-modularity)
-   [Open-source development](#open-source-development)
-   [User guide](#user-guide)

## What is _cartostation_?

<!-- WEB GIS -->

_cartostation_ is a web application platform offering tools for managing, visualizing and publishing spatial data.

These functionalities are available online, through carefully designed interfaces, with a view to making this tool available to the greatest number of people.

The basic applications of _cartostation_ allow, in particular, to easily publish maps with legends on the web and to integrate them into third party websites.

cartostation\_ is a software under free license (AGPL v3), whose code is available at [https://gitlab.com/atelier-cartographique/carto-station](https://gitlab.com/atelier-cartographique/carto-station)

_cartostation_ is currently deployed in administrations of the Brussels-Capital Region (Bruxelles Environnement and Perspective.Brussels) under the name _Geodata_.

## Project philosophy

_cartostation_ is developed with a strong attention to the context of its use, both at the individual level and at the level of the organizations that employ it.

At the user level, the aim is to facilitate access to tools for manipulating spatial data. Geomatics and cartography are complex fields, at the crossroads of many fields (scientific, visual, political, technological, etc.). One of the major ambitions of _cartostation_ is to lower the technical level required to apprehend and act in these fields.

At the level of an organization, _cartostation_, as an information system, plays an important role because it participates in the structuring, manipulation, and publication of information internally or externally.

This position of the information system, which is often particularly sensitive and influential in organizations, invites us to propose delicate and adapted approaches.

Two approaches are combined to address these issues.

Firstly, _cartostation_ favours an architecture that separates the major functionalities into dedicated applications, which are lighter and less complex to apprehend than a vast generalist application.

Then, an important place is given to interface design, and more broadly to the design of the (co)creation process of the tools. This dimension comes into play from the beginning of the application production period, in close relationship with the clients in order to produce a tool that remains in contact with the needs and is adapted to the skills of the end users.

This mode of operation makes particular sense when it comes to creating innovative tools, dedicated to numerous operators within large-scale organisations: the future users of the platform - technophiles and less technophiles - can thus grasp the project from its conception, and take it over conceptually in a global way.

As a spatial information system, _cartostation_ is positioned as a benevolent solution, anxious to place its users in a position of capacity with regard to the technological tool. By combining this attention to the technical quality of its development, _cartostation_ can thus accompany more structural transitions within the organizations that use it through the prism of the data and the technological and organizational tool that accompanies them.

## Applications and modularity

_cartostation_ is structured in two parts: a simple kernel, connected to a spatial database, and a series of applications assuming various functionalities.

In addition to the benefits of use mentioned above, this modular architecture allows great development agility, and promotes the creation of tools that are easy to access in view of the complexity inherent in the processing and visualization of spatial data.

The software kernel is therefore intended to be light, in order to facilitate its maintenance and evolution; while the applications linked to it each follow this logic of compactness, offering users simple and clearly identified tools, and developers a readable and compartmentalized platform.

Some of the applications are generic, responding to common needs (creation of maps, visualization etc...), while other applications are developed to measure, to meet more specific needs.

Applications are activated according to deployment.

## Open-source development

_cartostation_ is distributed under the free AFFERO GPL 3 license: https://www.gnu.

## User guide

The complete user guide is available here : https://cartostation.com/documentation
