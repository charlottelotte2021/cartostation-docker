#########################################################################
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#########################################################################

import uuid
from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
import os
from sorl.thumbnail import get_thumbnail


def get_file_extension(instance):
    try:
        ext = os.path.splitext(str(instance.document))[1]
    except AttributeError:
        try:
            ext = os.path.splitext(str(instance.image))[1]
        except AttributeError:
            ext = ""
    return ext


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return "user_{0}/{1}{2}".format(
        instance.user.get_username(), instance.id, get_file_extension(instance)
    )


class Document(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    document = models.FileField(upload_to=user_directory_path)
    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="documents",
    )


class Image(models.Model):
    SIZE_SMALL = "small"
    SIZE_MEDIUM = "medium"
    SIZE_LARGE = "large"
    SIZES = {SIZE_SMALL: "x250", SIZE_MEDIUM: "300x300", SIZE_LARGE: "1200x1200"}

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    image = models.ImageField(upload_to=user_directory_path)
    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="images",
    )

    def thumbnail(self, size):
        if size in Image.SIZES:
            thumbnail = get_thumbnail(
                self.image, Image.SIZES[size], crop="center", quality=50
            )
            return open(thumbnail.storage.path(thumbnail.name), "rb")
        raise Exception("thumbnail size not supporter. Should be one of: ")
