#
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.urls import path, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf import settings
from .views import get_admin_logs

admin.site.site_header = "geodata"
admin.site.site_title = "geodata"
admin.site.index_title = "geodata administration"


admin.site.site_header = getattr(settings, "ADMIN_SITE_HEADER", "geodata")
admin.site.site_title = getattr(settings, "ADMIN_SITE_TITLE", "geodata")
admin.site.index_title = getattr(
    settings, "ADMIN_INDEX_TITLE", "geodata administration"
)

ROOT_WEB_URLS = getattr(settings, "ROOT_WEB_URLS", "web.urls")

urlpatterns = [
    path("", include(ROOT_WEB_URLS)),
    path("timeserie/", include("timeserie.urls")),
    path("api/", include("api.urls")),
    path("admin/", admin.site.urls),
    path("client/", include("clients.urls")),
    path("documents/", include("documents.urls")),
    path(
        "login/",
        auth_views.LoginView.as_view(template_name="main/login.html"),
        name="login",
    ),
    path("catalog/", include("catalog.urls")),
    path("webservice/", include("webservice.urls")),
    path("remote-manage/", include("remote_manage.urls")),
    # path(r'^render/', include('render.urls')),
    path("basic-wfs/", include("basic_wfs.urls")),
    path("activity/", include("activity.urls")),
    path("lookup/", include("lookup.urls")),
    path("online/", include("online.urls")),
    path("log-admin/", get_admin_logs, name="main.get_admin_log"),
    path("log-admin/<int:days>/", get_admin_logs, name="main.get_admin_log_days"),
]

if getattr(settings, "MEDIA_URL_DEV", None) is not None:
    from django.views.static import serve
    from django.urls import re_path

    MEDIA_ROOT = getattr(settings, "MEDIA_ROOT")
    MEDIA_URL = getattr(settings, "MEDIA_URL_DEV")
    if MEDIA_URL.endswith("/") is False:
        MEDIA_URL = MEDIA_URL + "/"
    if MEDIA_URL.startswith("/"):
        MEDIA_URL = MEDIA_URL[1:]
    urlpatterns += [
        re_path(
            f"^{MEDIA_URL}" + r"(?P<path>.*)$",
            serve,
            {
                "document_root": MEDIA_ROOT,
            },
        ),
    ]
