# Generated by Django 2.1.7 on 2019-02-28 13:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webservice', '0007_auto_20190228_1108'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wmslayer',
            name='display_name',
        ),
        migrations.RemoveField(
            model_name='wmslayer',
            name='layers',
        ),
    ]
