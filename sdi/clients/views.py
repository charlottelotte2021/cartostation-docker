#
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import mimetypes
import os
import posixpath
import stat
from collections import namedtuple, OrderedDict
import json
import logging

from django.shortcuts import render, redirect
from django.urls import reverse
from django.template.loader import render_to_string
from django.conf import settings
from django.http import (
    FileResponse,
    Http404,
    HttpResponse,
)
from django.middleware.csrf import get_token
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views import static
from django.utils._os import safe_join
from django.contrib.auth.models import Group
from django.contrib.staticfiles.finders import find as find_static

logger = logging.getLogger(__name__)
Resource = namedtuple('Resource', ['mtime', 'data'])
Client = namedtuple('Client', ['module', 'display_name', 'route', 'url'])

DEBUG = getattr(settings, 'DEBUG', False)
CLIENTS = getattr(settings, 'CLIENTS')
CLIENTS_DEFAULT = getattr(settings, 'CLIENTS_DEFAULT')
CLIENTS_LOGIN = getattr(settings, 'CLIENTS_LOGIN', 'login')
CLIENTS_URLS = getattr(settings, 'CLIENTS_URLS', [])
ADMIN_QUERY_NAME = getattr(settings, 'ADMIN_QUERY_GROUP', None)
ADMIN_QUERY = Group.objects.get(name=ADMIN_QUERY_NAME) if ADMIN_QUERY_NAME is not None else None


def get_version(name):
    version_path = find_static('clients/apps/{}.version'.format(name))
    try:
        with open(version_path) as version_file:
            return json.load(version_file)
    except Exception as ex:
        logger.error('Could not get a version for {}:\n\t{}'.format(name, ex))
        return None


class ClientConfig:
    configured_clients = None  # configure_clients()

    @classmethod
    def get_items(cls):
        if cls.configured_clients is not None:
            return cls.configured_clients.items()

        cls.configured_clients = OrderedDict()
        for client in CLIENTS:
            info = dict()
            route = client['route']
            module = client.get('module', route)
            name = client.get('name', None)
            login = client.get('login', False)
            groups = client.get('groups', None)
            version = get_version(module)

            if groups is not None:
                info['groups'] = [Group.objects.get(name=gn) for gn in groups]

            if name is not None:
                info['name'] = name

            info['version'] = version
            info['login'] = login
            info['route'] = route
            info['module'] = module

            cls.configured_clients[route] = info

            logger.info('configured client - {} =============='.format(route))
            logger.info('name:\t{}'.format(name))
            logger.info('groups:\t{}'.format(groups))
            logger.info('login:\t{}'.format(login))
            logger.info('version:\t{}'.format(version))

        return cls.configured_clients.items()

    @classmethod
    def get(cls, app_name):
        if cls.configured_clients is None:
            cls.get_items()

        return cls.configured_clients[app_name]


def in_group(user, group_list):
    groups = set([g.id for g in group_list])
    user_groups = set([g.id for g in user.groups.all()])
    diff = groups.difference(user_groups)

    return len(diff) < len(groups)


def filter_clients(request):
    clients = []

    for route, info in ClientConfig.get_items():
        groups = info.get('groups', None)
        display_name = info.get('name', None)
        module = info.get('module', route)
        url = reverse('clients.root', args=(route, ''))
        if groups is None or in_group(request.user, groups):
            clients.append(Client(module, display_name, route, url))

    return clients


@xframe_options_exempt
def render_index(request, app_name, path):
    user_id = None
    require_login = ClientConfig.get(app_name)['login']
    # print('render_index ', app_name, request.user.is_authenticated,
    #       require_login)
    if request.user.is_authenticated:
        user_id = request.user.id
    elif require_login:
        next = '{}/{}'.format(app_name, path)
        login_url = reverse('clients.root', args=(CLIENTS_LOGIN, next))
        return redirect(login_url)

    client_config = ClientConfig.get(app_name)
    if DEBUG:
        version = get_version(client_config['module'])
    else:
        version = client_config['version']

    if version is None:
        raise Http404('There\'s no version for "{}"'.format(app_name))

    root_url = request.build_absolute_uri(reverse('clients.index'))
    api_url = request.build_absolute_uri(reverse('api-root'))
    return render(
        request,
        'clients/app_index.html',
        context=dict(
            user_id=user_id,
            path=path,
            root=root_url,
            api=api_url,
            apps=json.dumps(filter_clients(request)),
            csrf_token=get_token(request),
            app_name=app_name,
            bundle_url='clients/apps/' + version['bundle'],
            style_url='clients/apps/' + version['style'],
            urls=CLIENTS_URLS,
            admin_query=ADMIN_QUERY,
        ))


def app_index(request, app_name, path):

    for client in filter_clients(request):
        if client.route == app_name:
            return render_index(request, app_name, path)

    raise Http404('{} not configured'.format(app_name))


def index(request):

    for client in filter_clients(request):
        if client.route == CLIENTS_DEFAULT:
            return render_index(request, CLIENTS_DEFAULT, '')

    raise Http404('Missing default application')
