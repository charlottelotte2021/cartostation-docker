from importlib import import_module
from json import loads

from django.utils.text import slugify
from django.shortcuts import render
from django.conf import settings
from django.http import JsonResponse, Http404
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_protect
import urllib
from django.apps import apps as django_apps
from django.db import connections, DEFAULT_DB_ALIAS

import logging

logger = logging.getLogger(__name__)

PROXY_TIMEOUT = getattr(settings, "PROXY_TIMEOUT", 12)
LANGUAGES = getattr(settings, "LANGUAGES", [])


def make_service_code(type, name):
    sname = "-".join([slugify(n) for _, n in name.items()])
    return "{}-{}".format(type, sname)


def filter_service(app_name):
    def inner(service):
        if service.apps is None:
            return True
        return app_name in service.apps

    return inner


class ServiceProxy:
    def __init__(self, name, placeholder, description, apps, config):
        self.code = make_service_code("proxy", name)
        self.name = name
        self.placeholder = placeholder
        self.description = description
        self.apps = apps
        mod_name = config["module"]
        mod = import_module(mod_name)
        self.get_url = getattr(mod, "get_url")
        self.format_response = getattr(mod, "format_response")
        self.get_error = getattr(mod, "get_error")

    def manifest(self):
        return {
            "code": self.code,
            "name": self.name,
            "placeholder": self.placeholder,
            "description": self.description,
        }

    def get_response(self, url, body=None, headers={}):
        request = urllib.request.Request(url, body, headers)
        response = urllib.request.urlopen(request, timeout=PROXY_TIMEOUT)
        try:
            response_body = response.read()
            status = response.getcode()
            # logger.debug(self._msg % response_body)
        except urllib.error.HTTPError as e:
            reponse_body = e.read()
            status = e.code
            # logger.debug(self._msg % response_body)
        response_body = loads(response_body.decode("utf-8"))
        service_error = self.get_error(response_body)
        if service_error is not None:
            return service_error
        elif status >= 400:
            return "Upstream server error {}".format(status), []

        return None, response_body["result"]

    def lookup(self, term, lang):
        url = self.get_url(lang, term)
        error, response = self.get_response(url)
        if error is not None:
            return {"results": [], "error": error}

        result = self.format_response(response)

        # for l in LANGUAGES:
        #     if (len(result)==0 and l[0] != lang):
        #         url = self.get_url(l[0])
        #         error, response = self.get_response(url)
        #         if error is not None:
        #             return {
        #                 'results': [],
        #                 'error': error
        #         }
        #         result = self.format_response(response)

        return {"results": result, "error": None}


class ServiceModel:
    def __init__(self, name, placeholder, description, apps, config):
        self.code = make_service_code("model", name)
        self.name = name
        self.placeholder = placeholder
        self.description = description
        self.apps = apps
        self.model = django_apps.get_model(config["app"], config["model"])
        self.con = config.get("connection", DEFAULT_DB_ALIAS)
        self.field = config["field"]
        self.geo_field = config["geo_field"]
        lt = config["lookup_type"]
        if lt == "begin_with":
            self.key = "{}__istartswith".format(self.field)
        elif lt == "end_with":
            self.key = "{}__iendswith".format(self.field)
        elif lt == "contains":
            self.key = "{}__icontains".format(self.field)
        if lt == "equal":
            self.key = self.field

    def manifest(self):
        return {
            "code": self.code,
            "name": self.name,
            "placeholder": self.placeholder,
            "description": self.description,
        }

    def lookup(self, term, _lang):
        kwargs = {}
        kwargs[self.key] = term
        results = self.model.objects.using(self.con).filter(**kwargs)
        output = []
        error = None
        for r in results:
            try:
                name = str(getattr(r, self.field))
            except Exception as ex:
                error = 'field is missing: "{}" due to {}'.format(self.field, ex)
                break
            if self.geo_field is not None:
                try:
                    geofield = getattr(r, self.geo_field)
                except Exception as ex:
                    error = 'geo_field is missing: "{}" due to {}'.format(
                        self.geo_field, ex
                    )
                    break

                if geofield is not None:
                    try:
                        point = geofield.centroid
                    except Exception as ex:
                        error = "geofield is not a geometry due to {}".format(ex)
                        break
                    output.append(
                        {
                            "coord": {"x": point.x, "y": point.y},
                            "message": name,
                        }
                    )
            else:
                output.append({"coord": {"x": 0, "y": 0}, "message": name})
        return {"results": output, "error": error}


class ServicePostgis(ServiceModel):
    def __init__(self, name, placeholder, description, apps, config):
        from postgis_loader.models import get_layer

        schema = config["schema"]
        table = config["table"]
        model, geometry_field, geometry_field_type = get_layer(schema, table)
        self.code = make_service_code("postgis", name)
        self.name = name
        self.placeholder = placeholder
        self.description = description
        self.apps = apps
        self.model = model
        self.con = schema
        self.field = config["field"]
        self.geo_field = geometry_field
        lt = config["lookup_type"]
        if lt == "begin_with":
            self.key = "{}__istartswith".format(self.field)
        elif lt == "end_with":
            self.key = "{}__iendswith".format(self.field)
        elif lt == "contains":
            self.key = "{}__icontains".format(self.field)
        if lt == "equal":
            self.key = self.field

    def manifest(self):
        return {
            "code": self.code,
            "name": self.name,
            "placeholder": self.placeholder,
            "description": self.description,
        }


def make_service(conf):
    service_name = conf["name"]
    service_type = conf["type"]
    config = conf["config"]
    placeholder = conf.get("placeholder", None)
    description = conf.get("description", None)
    apps = conf.get("apps", None)
    if apps is not None and isinstance(apps, (list, tuple)) is False:
        apps = [apps]

    if service_type == "proxy":
        return ServiceProxy(service_name, placeholder, description, apps, config)

    elif service_type == "model":
        return ServiceModel(service_name, placeholder, description, apps, config)

    elif service_type == "postgis":
        return ServicePostgis(service_name, placeholder, description, apps, config)


services = [make_service(conf) for conf in getattr(settings, "LOOKUP_SERVICES", [])]


def find_service(code):
    for service in services:
        if service.code == code:
            return service
    raise Http404("Service not found: {}".format(code))


@require_http_methods(["POST"])
@csrf_protect
def lookup(request):
    data_string = request.body.decode("utf-8")
    data = loads(data_string)
    service = find_service(data["service"])
    term = data["input"]
    lang = data["lang"]
    results = service.lookup(term, lang)
    return JsonResponse(results, safe=False)


def lookup_services(self, app):
    formated_services = []
    for s in filter(filter_service(app), services):
        formated_services.append(s.manifest())
    return JsonResponse(formated_services, safe=False)
