Three search types are available for the lookup module:

-   proxy: to access an external service
-   model: to search a Django model
-   postgis: to search a [postgis-loader](https://gitlab.com/atelier-cartographique/postgis-loader) layer

To set up one or more of them, add a "LOOKUP_SERVICES" key in the Django settings. This take the form of a list of dictionnaries.

## general

In all service's types, a `type` key gives the choosen search type (proxy, model or postgis).

The `name`, `description` and `placeholder` keys are multilingual records used by the client to display the geocoder choices and the input placeholder.  
`placeholder` and `description` are optional keys.

The optional `apps` key is either a application name or a list of application names for which this lookup service is targeted.

The `config` key contains specific keys, described below, depending of the service type.

## proxy

In the proxy `config`, the `module` key refers to a python file which contains the following functions:

-   `get_url(lang, term)` that returns the complete request url
-   `format_response(response)` that format the response to a list of dictionnaries with the following keys:

```
    {
    coord: {x:number, y: number},
    message: string
    }
```

-   `get_error(response_body)` that return a string formatted error if there is one in the response body, or None.

This file already exists for the proxy service "Urbis".

## postgis

The postgis `config` contains the following keys:

-   `schema`: DB schema
-   `table`: table name
-   `field`: name of the field (column name) to search in

In postgis and model `config`, the `lookup_type` key refers to the way the terms are compared to the input. There are 4 possibilities:

-   'equal'
-   'begin_with'
-   'end_with'
-   'contains'

## model

The model `config` contains the following keys:

-   `connection`: DB connection name,
-   `app`: Django application name in which the model is,
-   `model`: Django model name,
-   `field`: name of the field (column name) to search in,
-   `geo_field`: name of the field (column name) where to find the related geometry,
-   `lookup_type`: refers to the way the terms are compared to the input. There are 4 possibilities:
    -   'equal'
    -   'begin_with'
    -   'end_with'
    -   'contains'

## example

```python
LOOKUP_SERVICES = [
    # a proxy search example:
    {
        'type': 'proxy',
        'name': {
            'fr':'urbis',
            'nl':'urbis_nl'
            },
        'placeholder': {
            'fr':'Encodez l\'adresse',
            'nl':'Schrijf de adres in'
            },
        'description':{
            'fr':'',
            'nl':''
        },
        'config': {
            'module': 'lookup.urbis'
        },
    },
    # a postgis search example:
    {
        'type': 'postgis',
        'name': {
            'fr':'capakey',
            'nl':'capakey'
            },
        'placeholder': {
            'fr':'Encodez la capakey',
            'nl':'Schrijf de capakey in'
            },
        'config': {
            'schema': 'urbpab',
            'table': 'capa_poly_20',
            'field': 'capakey',
            'lookup_type': 'contains'
        },
        'apps': ['my-app'],
    },
    # a Django model search example:
    {
        'type': 'model',
        'name': {
            'fr':'test_fr',
            'nl':'test_nl'
            },
        'placeholder': None,
        'config': {
            'connection': 'default',
            'app': 'angled',
            'model': 'term',
            'field': 'name__fr',
            'geo_field': None,
            'lookup_type': 'begin_with'
        },
    },
]
```
