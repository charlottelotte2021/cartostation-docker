from django.shortcuts import render


def index(request):
    return render(request, 'web/index.html', context=dict())


def root_file(request, filename):
    return render(request, 'web/{}'.format(filename), context=dict())
