from django.urls import path
from .views import index, root_file

urlpatterns = [
    path('', index, name='web.index'),
    path('web/<filename>', root_file, name='web.root_file'),
]