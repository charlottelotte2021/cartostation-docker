from .models.record import MessageRecord, LANGUAGES

from rest_framework import serializers


def make_serializer():
    fields = dict()

    class Meta:
        model = MessageRecord
        fields = LANGUAGES

    fields['Meta'] = Meta

    for code in LANGUAGES:
        fields[code] = serializers.CharField(
            allow_blank=True, read_only=False, required=False)

    return type(
        'MessageRecordSerializer',
        (serializers.ModelSerializer, ),
        fields,
    )


MessageRecordSerializer = make_serializer()


class LinguaRecordSerializer(serializers.JSONField):
    def to_representation(self, value):
        value = super().to_representation(value)
        result = dict()
        for k, v in value.items():
            if v is not None:
                result[k] = v

        return result
