from django.forms import MultiValueField,  MultiWidget,  Textarea,TextInput, CharField
import json


class LinguaWidgetBase(MultiWidget):
    template_name = 'lingua/widgets/lingua.html'

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        subwidgets = context['widget']['subwidgets']
        context['widget']['subwidgets'] = zip(['fr', 'nl', 'en'], subwidgets)

        return context

    def decompress(self, value):
        print('decompress', value)
        from .fields import LinguaRecord
        if value is None:
            return []
        getter = getattr if isinstance(value, LinguaRecord) else lambda o, k, d: o.get(k, d)

        return [getter(value, k, "-") for k in ['fr', 'nl', 'en']]


class LinguaWidgetText(LinguaWidgetBase):
    def __init__(self, attrs=None):
        widgets = [
            Textarea(),
            Textarea(),
            Textarea(),
        ]
        super().__init__(widgets, attrs)


class LinguaWidgetLabel(LinguaWidgetBase):
    def __init__(self, attrs=None):
        widgets = [
            TextInput(),
            TextInput(),
            TextInput(),
        ]
        super().__init__(widgets, attrs)


class LinguaFormFieldBase(MultiValueField):

    def __init__(self, *, require_all_fields=True, **kwargs):
        fields = [
            CharField(),
            CharField(),
            CharField(),
        ]
        super().__init__(fields, **kwargs)

    def compress(self, value):
        if value is None:
            return dict()

        return json.dumps(dict(zip(['fr', 'nl', 'en'], value)))


class LinguaFormFieldText(LinguaFormFieldBase):
    widget = LinguaWidgetText


class LinguaFormFieldLabel(LinguaFormFieldBase):
    widget = LinguaWidgetLabel

