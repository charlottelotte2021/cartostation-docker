#  Copyright (C) 2019 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django.db.models import JSONField
from django.conf import settings
from json import loads

from .forms import LinguaFormFieldText, LinguaFormFieldLabel


def code_to_propname(code):
    return code.replace("-", "_")


LANGUAGE_CODE = code_to_propname(getattr(settings, "LANGUAGE_CODE"))
LANGUAGES = tuple(
    code_to_propname(code) for code, _name in getattr(settings, "LANGUAGES")
)


class LinguaRecord:
    def __init__(self, **kwargs):
        self.update_record(**kwargs)

    def __str__(self):
        label = getattr(self, LANGUAGE_CODE, None)

        if label is None:
            for l in LANGUAGES:
                inner_label = getattr(self, l, None)
                if inner_label is not None:
                    label = "[{}] {}".format(l, inner_label)
                    break

        if label is None:
            label = "We tried every language we knew of, no hope"

        end = ""
        if len(label) > 64:
            end = "…"

        return "{}{}".format(label[:64], end)

    def tr(self, lang):
        return getattr(self, lang, str(self))

    def get(self, lang, default=None):
        return getattr(self, lang, default) if lang in LANGUAGES else default

    def items(self):
        return self.to_dict().items()

    def update_record(self, **kwargs):
        # print('update_record({})'.format(kwargs))
        for k, v in kwargs.items():
            if k in LANGUAGES:
                setattr(self, k, v)

    def to_dict(self):
        ret = dict()
        for code in LANGUAGES:
            content = getattr(self, code, None)
            if content is not None:
                ret[code] = content

        return ret

    def clone(self):
        record = LinguaRecord()
        for code in LANGUAGES:
            try:
                setattr(record, code, getattr(self, code))
            except Exception:
                pass
        return record

    def contains(self, s):
        for code in LANGUAGES:
            if s == getattr(self, code, None):
                return True

        return False


class LinguaField(JSONField):
    description = "A Postgres JSON field to store multilingual text"

    def __init__(self, *args, **kwargs):
        self.text_size = kwargs.pop("text_size", "large")
        super().__init__(*args, **kwargs)

    def from_db_value(self, value, expression, connection):
        if value is None:
            return value
        if isinstance(value, (str,)):
            return LinguaRecord(**loads(value))
        return LinguaRecord(**value)

    def validate(self, value, model_instance):
        v = value
        if isinstance(value, LinguaRecord):
            v = value.to_dict()
        super().validate(v, model_instance)

    def to_python(self, value):
        if value is None:
            return value
        elif isinstance(value, LinguaRecord):
            return value
        elif isinstance(value, str):
            loaded = loads(value)
            return LinguaRecord(**loaded)

        return LinguaRecord(**value)

    def get_prep_value(self, value):
        if isinstance(value, LinguaRecord):
            return super().get_prep_value(value.to_dict())

        return super().get_prep_value(value)

    def formfield(self, **kwargs):
        if self.text_size == "small":
            return LinguaFormFieldLabel(**kwargs)
        return LinguaFormFieldText(**kwargs)


def lingua_string(value):
    record = LinguaField(text_size="small").to_python(value)
    if record is None:
        return "None"
    return str(record)


def label_field(message_type, app_name=None, **kwargs):
    return LinguaField(verbose_name=message_type, text_size="small", **kwargs)


def text_field(message_type, app_name=None, **kwargs):
    return LinguaField(verbose_name=message_type, text_size="large", **kwargs)


def nullable_label_field(message_type, app_name=None, text_size="small", **kwargs):
    if kwargs is not None:
        kwargs.pop("null", None)
        kwargs.pop("blank", None)
    return LinguaField(verbose_name=message_type, null=True, blank=True, **kwargs)


def nullable_text_field(message_type, app_name=None, **kwargs):
    if kwargs is not None:
        kwargs.pop("null", None)
        kwargs.pop("blank", None)
    return LinguaField(
        verbose_name=message_type, text_size="large", null=True, blank=True, **kwargs
    )


def message(message_type, **kwargs):
    return LinguaRecord(**kwargs)


def simple_message(message_type, msg):
    kwargs = dict()
    for code in LANGUAGES:
        kwargs[code] = msg
    return message(message_type, **kwargs)
