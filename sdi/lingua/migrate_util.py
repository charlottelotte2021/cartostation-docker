
from .models.record import LANGUAGES
from django.db.models.deletion import ProtectedError
from json import loads

def deprecate_message_record_migration_ops(PKG, FIELDS):

    def forward(apps, schema_editor):
        for lingua_model, mod_name, field_name in FIELDS:
            new_field_name = '{}_lf'.format(field_name)
            Model = apps.get_model(PKG, mod_name)
            for instance in Model.objects.all():
                try:
                    record = getattr(instance, field_name, None)
                except Exception:
                    record = None
                if record is not None:
                    data = dict()
                    for code in LANGUAGES:
                        content = record.content if type(record.content) == dict else loads(record.content)
                        s = content.get(code)
                        if s is not None:
                            data[code] = s
                    setattr(instance, new_field_name, data)
                    setattr(instance, field_name, None)
                    instance.save()
                    try:
                        record.delete()
                    except ProtectedError:
                        print('A record is in use elsewhere, might get deleted later')

    def reverse(apps, schema_editor):
        for lingua_model, mod_name, field_name in FIELDS:
            new_field_name = '{}_lf'.format(field_name)
            Record = apps.get_model('lingua', lingua_model)
            Model = apps.get_model(PKG, mod_name)
            for instance in Model.objects.all():
                data = getattr(instance, new_field_name)
                if data is not None:
                    setattr(instance, field_name, Record.objects.create(data))
                    setattr(instance, new_field_name, None)
                    instance.save()

    return forward, reverse
