from django.urls import reverse

from api.models.metadata import MetaData

from .xml import (
    create,
    create_tree,
    append,
    tree_to_string,
)


def ensure_list(x):
    if isinstance(x, (
            list,
            tuple,
    )):
        return x
    return [x]


"""
<?xml version="1.0" encoding="UTF-8"?><xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:Urbis="http://www.cirb.brussels/urbis" xmlns:gml="http://www.opengis.net/gml" elementFormDefault="qualified" targetNamespace="http://www.cirb.brussels/urbis">
  <xsd:import namespace="http://www.opengis.net/gml" schemaLocation="http://geoservices-others.irisnet.be:80/geoserver/schemas/gml/2.1.2/feature.xsd"/>
  <xsd:complexType name="AdptOnSiType">
    <xsd:complexContent>
      <xsd:extension base="gml:AbstractFeatureType">
        <xsd:sequence>
          <xsd:element maxOccurs="1" minOccurs="0" name="ADRN" nillable="true" type="xsd:string"/>
          <xsd:element maxOccurs="1" minOccurs="0" name="SI_ID" nillable="true" type="xsd:decimal"/>
          <xsd:element maxOccurs="1" minOccurs="0" name="GEOM" nillable="true" type="gml:GeometryPropertyType"/>
          <xsd:element maxOccurs="1" minOccurs="1" name="ADPT_ID" nillable="false" type="xsd:decimal"/>
          <xsd:element maxOccurs="1" minOccurs="0" name="ADNCS" nillable="true" type="xsd:string"/>
        </xsd:sequence>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>
  <xsd:element name="AdptOnSi" substitutionGroup="gml:_Feature" type="Urbis:AdptOnSiType"/>
</xsd:schema>
"""


def describe_feature_type(request, typenames):
    names = ensure_list(typenames)
    attributes = {
        'targetNamespace': "http://www.carto-station.com/cs",
        'xmlns:cs': "http://www.carto-station.com/cs",
        'xmlns:xsd': "http://www.w3.org/2001/XMLSchema",
        'xmlns': "http://www.w3.org/2001/XMLSchema",
        'xmlns:gml': "http://www.opengis.net/gml",
        'elementFormDefault': "qualified",
        'version': "0.1",
    }
    schema = create('schema', attributes)

    append(
        schema, 'xsd:import', {
            'namespace': 'http://www.opengis.net/gml',
            'schemaLocation':
            'http://schemas.opengis.net/gml/3.1.1/feature.xsd',
        })

    for name in names:
        ct = append(schema, 'xsd:complexType', {
            'name': name,
        })
        cc = append(ct, 'xsd:complexContent')
        aft = append(cc, 'xsd:extension', {'base': 'gml:AbstractFeatureType'})
        append(aft, 'xsd:sequence')

    return tree_to_string(create_tree(schema))