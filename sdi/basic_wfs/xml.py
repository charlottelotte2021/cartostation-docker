import xml.etree.ElementTree as ET
from xml.dom import minidom
from io import StringIO

create_tree = ET.ElementTree
create = ET.Element
append = ET.SubElement


def tree_to_string(t):
    s = StringIO()
    t.write(s, encoding='unicode', xml_declaration=True)
    val = s.getvalue()
    return minidom.parseString(val).toprettyxml(indent="  ")


def strip_ns(name):
    return ':'.join(name.split(':')[1:])


def path_to_typename(p):
    return p[2:].replace('/', '__')


def rid_to_typename_ns(rid):
    sep_index = rid.find(':')
    ns = rid[0:sep_index] 
    path = rid[sep_index + 1:]
    return '{}:{}'.format(ns, path_to_typename(path))


def typename_ns_to_rid(rid):
    sep_index = rid.find(':')
    ns = rid[0:sep_index] 
    path = rid[sep_index + 1:]
    return '{}://{}'.format(ns, path.replace('__', '/'))
