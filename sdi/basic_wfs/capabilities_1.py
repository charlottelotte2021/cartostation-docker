import re

from django.urls import reverse

from api.models.metadata import MetaData

from .xml import (
    create,
    create_tree,
    append,
    tree_to_string,
)



def capabilities():
    attributes = {
        "version": "1.0.0",
        'xmlns:ows': 'http://www.opengis.net/ows',
        'xmlns:ogc': 'http://www.opengis.net/ogc',
        'xmlns:wfs': 'http://www.opengis.net/wfs',
        'xmlns:gml': 'http://www.opengis.net/gml',
        'xmlns:xlink': 'http://www.w3.org/1999/xlink',
        'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:schemaLocation': 'http://www.opengis.net/wfs',
    }
    return create('WFS_Capabilities', attributes)


def service_identification(caps, request):
    ident = append(caps, 'ows:ServiceIdentification')
    append(ident, 'ows:Title').text = 'carto-station'
    append(ident, 'ows:Abstract').text = 'carto-station'
    append(ident, 'ows:ServiceType').text = 'WFS'
    append(ident, 'ows:ServiceTypeVersion').text = '1.0.0'
    append(ident, 'ows:Fees').text = 'NONE'
    append(ident, 'ows:AccessConstraints').text = 'NONE'


def service_provider(caps, request):
    ident = append(caps, 'ows:ServiceProvider')
    append(ident, 'ows:ProviderName').text = 'carto-station'
    append(ident, 'ows:ProviderSite').text = 'bruxelles'


def make_op_metadata(md, name, request):
    op = append(md, 'ows:Operation', dict(name=name))
    dcp = append(op, 'ows:DCP')

    http = append(dcp, 'ows:HTTP')
    append(http, 'ows:Get', {'xlink:href': request.get_raw_uri()})


def make_op_parameter(md, name, values):
    param = append(md, 'ows:Parameter', dict(name=name))
    for value in values:
        append(param, 'ows:Value').text = value


CONSTRAINTS = [
    'ImplementsBasicWFS',
    'ImplementsTransactionalWFS',
    'ImplementsLockingWFS',
    'KVPEncoding',
    'XMLEncoding',
    'SOAPEncoding',
    'ImplementsInheritance',
    'ImplementsRemoteResolve',
    'ImplementsResultPaging',
    'ImplementsStandardJoins',
    'ImplementsSpatialJoins',
    'ImplementsTemporalJoins',
    'ImplementsFeatureVersioning',
    'ManageStoredQueries',
]
BASIC_CONFORMANCE = ['ImplementsBasicWFS', 'KVPEncoding']


def make_constraint(md, name, default):
    constraint = append(md, 'ows:Constraint', dict(name=name))
    append(constraint, 'ows:NoValues')
    append(constraint, 'ows:DefaultValue').text = default


def operations_metadata(caps, request):
    md = append(caps, 'ows:OperationsMetadata')
    make_op_metadata(md, 'GetCapabilities', request)
    make_op_metadata(md, 'DescribeFeatureType', request)
    make_op_metadata(md, 'GetFeature', request)
    make_op_parameter(md, 'srsName',
                      ['EPSG:http://www.opengis/net/def/crs/epsg/0/31370'])


def md_to_feature(ftl, md, request):
    ft = append(ftl, 'wfs:FeatureType')
    append(ft, 'wfs:Name').text = md.resource_identifier
    append(ft, 'wfs:Title').text = md.title.fr
    append(ft, 'wfs:Abstract').text = md.abstract.fr
    append(
        ft,
        'wfs:DefaultCRS').text = 'http://www.opengis.net/def/crs/epsg/0/31370'
    append(ft, 'wfs:MetadataURL').text = request.build_absolute_uri(
        reverse('metadata-detail', args=(md.id, )))
    append(append(ft, 'wfs:OutputFormats'),
           'wfs:Format').text = 'text/xml; subtype=gml/3.1.1'


def feature_type_list(caps, request):
    ftl = append(caps, 'wfs:FeatureTypeList')
    for md in MetaData.objects.all():
        md_to_feature(ftl, md, request)


# def gml_type_list(caps, request):
#     gml = append(caps, 'wfs:ServesGMLObjectTypeList')

FILTER_CONSTRAINTS = [
    'ImplementsQuery',
    'ImplementsAdHocQuery',
    'ImplementsFunctions',
    'ImplementsMinStandardFilter',
    'ImplementsStandardFilter',
    'ImplementsMinSpatialFilter',
    'ImplementsSpatialFilter',
    'ImplementsMinTemporalFilter',
    'ImplementsTemporalFilter',
    'ImplementsVersionNav',
    'ImplementsSorting',
    'ImplementsExtendedOperators',
]

IMPLEMENTED_FILTER_CONSTRAINTS = ['ImplementsQuery']


def filter_capabilities(caps, request):
    append(caps, 'ogc:Filter_Capabilities')


def get_capabilities(request):
    caps = capabilities()
    service_identification(caps, request)
    service_provider(caps, request)
    operations_metadata(caps, request)
    feature_type_list(caps, request)
    filter_capabilities(caps, request)

    return tree_to_string(create_tree(caps))