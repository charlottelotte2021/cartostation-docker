from importlib import import_module
import logging
from urllib.parse import urlparse

from django.apps import apps

logger = logging.getLogger(__name__)


class AppConfigNotFound(Exception):
    pass


def find_app_config(name):
    for label in apps.app_configs:
        if name == label:
            return apps.app_configs[label]
    raise AppConfigNotFound('Config {} Not Found'.format(name))


def schema_noop(request, rid_url, lang):
    return ''


def feature_noop(request, rid_url, lang):
    return ''


GET_FEATURE_TYPE = 'get_feature_type'
GET_FEATURE = 'get_feature'


class SchemaLoader:
    def __init__(self):
        self._cache = {}

    def get_loader(self, app_name):
        if app_name not in self._cache:
            config = find_app_config(app_name)
            try:
                mod = import_module('.wfs', config.name)
                self._cache[app_name] = getattr(mod, GET_FEATURE_TYPE,
                                                schema_noop)
            except Exception as ex:
                print('Could not find {} in {}: {}'.format(
                    GET_FEATURE_TYPE, app_name, ex))
                self._cache[app_name] = schema_noop

        return self._cache[app_name]

    def get_schema(self, request, lang, rid):
        u = urlparse(rid)
        f = self.get_loader(u.scheme)

        return f(request, lang, u)


class FeatureLoader:
    def __init__(self):
        self._cache = {}

    def get_loader(self, app_name):
        if app_name not in self._cache:
            config = find_app_config(app_name)
            try:
                mod = import_module('.wfs', config.name)
                self._cache[app_name] = getattr(mod, GET_FEATURE, feature_noop)
            except Exception as ex:
                print('Could not find {} in {}: {}'.format(
                    GET_FEATURE, app_name, ex))
                self._cache[app_name] = feature_noop

        return self._cache[app_name]

    def get_feature(self, request, lang, rid):
        u = urlparse(rid)
        f = self.get_loader(u.scheme)

        return f(request, lang, u)


schema_loader = SchemaLoader()
feature_loader = FeatureLoader()
