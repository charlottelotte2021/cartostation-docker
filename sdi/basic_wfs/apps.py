from django.apps import AppConfig


class BasicWFSConfig(AppConfig):
    name = 'basic_wfs'
