"""
example from http://docs.opengeospatial.org/is/09-025r2/09-025r2.html

<?xml version="1.0"?>
<WFS_Capabilities
   version="2.0.02.0.0"
   xmlns="http://www.opengis.net/wfs/2.0"
   xmlns:gml="http://www.opengis.net/gml/3.2"
   xmlns:fes="http://www.opengis.net/fes/2.0"
   xmlns:xlink="http://www.w3.org/1999/xlink"
   xmlns:ows="http://www.opengis.net/ows/1.1"
   xmlns:xsd="http://www.w3.org/2001/XMLSchema"
   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation="http://www.opengis.net/wfs/2.0
                       http://schemas.opengis.net/wfs/2.0.02.0/wfs.xsd
                       http://www.opengis.net/ows/1.1
                       http://schemas.opengis.net/ows/1.1.0/owsAll.xsd">
   <ows:ServiceIdentification>
      <ows:Title>OGC Member WFS</ows:Title>
      <ows:Abstract>Web Feature Service maintained by NSDI data provider, serving FGDC framework layer XXX; contact Paul.Bunyon@BlueOx.org</ows:Abstract>
      <ows:Keywords>
         <ows:Keyword>FGDC</ows:Keyword>
         <ows:Keyword>NSDI</ows:Keyword>
         <ows:Keyword>Framework Data Layer</ows:Keyword>
         <ows:Keyword>BlueOx</ows:Keyword>
         <ows:Type>String</ows:Type>
      </ows:Keywords>
      <ows:ServiceType>WFS</ows:ServiceType>
      <ows:ServiceTypeVersion>2.0.0</ows:ServiceTypeVersion>
      <ows:ServiceTypeVersion>2.0.0</ows:ServiceTypeVersion>
      <ows:ServiceTypeVersion>1.1.0</ows:ServiceTypeVersion>
      <ows:ServiceTypeVersion>1.0.0</ows:ServiceTypeVersion>
      <ows:Fees>NONE</ows:Fees>
      <ows:AccessConstraints>NONE</ows:AccessConstraints>
   </ows:ServiceIdentification>
   <ows:ServiceProvider>
      <ows:ProviderName>BlueOx Inc.</ows:ProviderName>
      <ows:ProviderSite xlink:href="http://www.cubewerx.com"/>
      <ows:ServiceContact>
         <ows:IndividualName>Paul Bunyon</ows:IndividualName>
         <ows:PositionName>Mythology Manager</ows:PositionName>
         <ows:ContactInfo>
            <ows:Phone>
               <ows:Voice>1.800.BIG.WOOD</ows:Voice>
               <ows:Facsimile>1.800.FAX.WOOD</ows:Facsimile>
            </ows:Phone>
            <ows:Address>
               <ows:DeliveryPoint>North Country</ows:DeliveryPoint>
               <ows:City>Small Town</ows:City>
               <ows:AdministrativeArea>Rural County</ows:AdministrativeArea>
               <ows:PostalCode>12345</ows:PostalCode>
               <ows:Country>USA</ows:Country>
               <ows:ElectronicMailAddress>Paul.Bunyon@BlueOx.org</ows:ElectronicMailAddress>
            </ows:Address>
            <ows:OnlineResource xlink:href="http://www.BlueOx.org/contactUs"/>
            <ows:HoursOfService>24x7</ows:HoursOfService>
            <ows:ContactInstructions>
               eMail Paul with normal requsts; Phone Paul for emergency
               requests; if you get voice mail and your request can’t wait,
               contact another mythological figure listed on the contactUs
               page of our web site.
            </ows:ContactInstructions>
         </ows:ContactInfo>
         <ows:Role>PointOfContact</ows:Role>
      </ows:ServiceContact>
   </ows:ServiceProvider>
   <ows:OperationsMetadata>
      <ows:Operation name="GetCapabilities">
         <ows:DCP>
            <ows:HTTP>
               <ows:Get xlink:href="http://www.BlueOx.org/wfs/wfs.cgi?"/>
               <ows:Post xlink:href="http://www.BlueOx.org/wfs/wfs.cgi"/>
            </ows:HTTP>
         </ows:DCP>
         <ows:Parameter name="AcceptVersions">
            <ows:AllowedValues>
            <ows:Value>2.0.0</ows:Value>
            <ows:Value>2.0.0</ows:Value>
            </ows:AllowedValues>
         </ows:Parameter>
      </ows:Operation>
      <ows:Operation name="DescribeFeatureType">
         <ows:DCP>
            <ows:HTTP>
               <ows:Get xlink:href="http://www.BlueOx.org/wfs/wfs.cgi?"/>
               <ows:Post xlink:href="http://www.BlueOx.org/wfs/wfs.cgi"/>
            </ows:HTTP>
         </ows:DCP>
      </ows:Operation>
      <ows:Operation name="ListStoredQueries">
         <ows:DCP>
            <ows:HTTP>
               <ows:Get xlink:href="http://www.BlueOx.org/wfs/wfs.cgi?"/>
               <ows:Post xlink:href="http://www.BlueOx.org/wfs/wfs.cgi"/>
            </ows:HTTP>
         </ows:DCP>
      </ows:Operation>
      <ows:Operation name="DescribeStoredQueries">
         <ows:DCP>
            <ows:HTTP>
               <ows:Get xlink:href="http://www.BlueOx.org/wfs/wfs.cgi?"/>
               <ows:Post xlink:href="http://www.BlueOx.org/wfs/wfs.cgi"/>
            </ows:HTTP>
         </ows:DCP>
      </ows:Operation>
      <ows:Operation name="GetFeature">
         <ows:DCP>
            <ows:HTTP>
               <ows:Get xlink:href="http://www.BlueOx.org/wfs/wfs.cgi?"/>
               <ows:Post xlink:href="http://www.BlueOx.org/wfs/wfs.cgi"/>
            </ows:HTTP>
         </ows:DCP>
      </ows:Operation>
      <ows:Parameter name="version">
         <ows:AllowedValues>
         <ows:Value>2.0.0</ows:Value>
         <ows:Value>2.0.0</ows:Value>
         </ows:AllowedValues>
      </ows:Parameter>
      <!– ***************************************************** –>
      <!– *   CONFORMANCE DECLARATION                         * –>
      <!– ***************************************************** –>
      <ows:Constraint name="ImplementsBasicWFS">
         <ows:NoValues/>
         <ows:DefaultValue>FALSE</ows:DefaultValue>
      </ows:Constraint>
      <ows:Constraint name="ImplementsTransactionalWFS">
         <ows:NoValues/>
         <ows:DefaultValue>FALSE</ows:DefaultValue>
      </ows:Constraint>
      <ows:Constraint name="ImplementsLockingWFS">
         <ows:NoValues/>
         <ows:DefaultValue>FALSE</ows:DefaultValue>
      </ows:Constraint>
      <ows:Constraint name="KVPEncoding">
         <ows:NoValues/>
         <ows:DefaultValue>TRUE</ows:DefaultValue>
      </ows:Constraint>
      <ows:Constraint name="XMLEncoding">
         <ows:NoValues/>
         <ows:DefaultValue>FALSE</ows:DefaultValue>
      </ows:Constraint>
      <ows:Constraint name="SOAPEncoding">
         <ows:NoValues/>
         <ows:DefaultValue>FALSE</ows:DefaultValue>
      </ows:Constraint>
      <ows:Constraint name="ImplementsInheritance">
         <ows:NoValues/>
         <ows:DefaultValue>FALSE</ows:DefaultValue>
      </ows:Constraint>
      <ows:Constraint name="ImplementsRemoteResolve">
         <ows:NoValues/>
         <ows:DefaultValue>FALSE</ows:DefaultValue>
      </ows:Constraint>
      <ows:Constraint name="ImplementsResultPaging">
         <ows:NoValues/>
         <ows:DefaultValue>FALSE</ows:DefaultValue>
      </ows:Constraint>
      <ows:Constraint name="ImplementsStandardJoins">
         <ows:NoValues/>
         <ows:DefaultValue>FALSE</ows:DefaultValue>
      </ows:Constraint>
      <ows:Constraint name="ImplementsSpatialJoins">
         <ows:NoValues/>
         <ows:DefaultValue>FALSE</ows:DefaultValue>
      </ows:Constraint>
      <ows:Constraint name="ImplementsTemporalJoins">
         <ows:NoValues/>
         <ows:DefaultValue>FALSE</ows:DefaultValue>
      </ows:Constraint>
      <ows:Constraint name="ImplementsFeatureVersioning">
         <ows:NoValues/>
         <ows:DefaultValue>FALSE</ows:DefaultValue>
      </ows:Constraint>
      <ows:Constraint name="ManageStoredQueries">
         <ows:NoValues/>
         <ows:DefaultValue>FALSE</ows:DefaultValue>
      </ows:Constraint>
      <!– ***************************************************** –>
      <!– *   CAPACITY CONSTRAINTS                            * –>
      <!– ***************************************************** –>
      <ows:Constraint name="CountDefault">
         <ows:NoValues/>
         <ows:DefaultValue>1000</ows:DefaultValue>
      </ows:Constraint>
      <ows:Constraint name="QueryExpressions">
         <ows:AllowedValues>
            <ows:Value>wfs:StoredQuery</ows:Value>
         </ows:AllowedValues>
      </ows:Constraint>
      <!– ***************************************************** –>
   </ows:OperationsMetadata>
   <FeatureTypeList>
      <FeatureType xmlns:bo="http://www.BlueOx.org/BlueOx">
         <Name>bo:Woods</Name>
         <Title>The Great Northern Forest</Title>
         <Abstract>
            Describes the arborial diversity of the Great
            Northern Forest.
         </Abstract>
         <ows:Keywords>
            <ows:Keyword>forest</ows:Keyword>
            <ows:Keyword>north</ows:Keyword>
            <ows:Keyword>woods</ows:Keyword>
            <ows:Keyword>arborial</ows:Keyword>
            <ows:Keyword>diversity</ows:Keyword>
         </ows:Keywords>
         <DefaultCRS>urn:ogc:def:crs:EPSG::http://www.opengis.net/def/crs/epsg/0/6269</DefaultCRS>
         <ows:WGS84BoundingBox>
            <ows:LowerCorner>-180 -90</ows:LowerCorner>
            <ows:UpperCorner>180 90</ows:UpperCorner>
         </ows:WGS84BoundingBox>
         <MetadataURL xlink:href="http://www.ogccatservice.com/csw.cgi?service=CSW&amp;version=2.0.02.0.0&amp;request=GetRecords&amp;constraintlanguage=CQL&amp;recordid=urn:uuid:4ee8b2d3-9409-4a1d-b26b-6782e4fa3d59"/>
         <ExtendedDescription>
            <Element name="TemporalExtent" type="xsd:date">
               <ows:Metadata xlink:href="http://www.someserver.example.com/AboutTemporalExtent.html"/>
               <ValueList>
                  <Value>2000-01-01</Value>
                  <Value>2006-01-31</Value>
               </ValueList>
            </Element>
            <Element name="Classifications" type="xsd:anyURI">
               <ows:Metadata xlink:href="http://www.someserver.example.com/AboutClassifications.html"/>
               <ValueList>
                  <Value>urn:x-ogc:specification:csw-ebrim:ClassificationScheme:ISO-19115:biota</Value>
               </ValueList>
            </Element>
            <Element name="ArborialDiversityIndex" type="xsd:integer">
               <ows:Metadata xlink:href="http://www.someserver.example.com/ArborialDiversity.html"/>
               <ValueList>
                  <Value>14</Value>
               </ValueList>
            </Element>
         </ExtendedDescription>
      </FeatureType>
   </FeatureTypeList>
   <fes:Filter_Capabilities>
      <fes:Conformance>
         <fes:Constraint name="ImplementsQuery">
            <ows:NoValues/>
            <ows:DefaultValue>TRUE</ows:DefaultValue>
         </fes:Constraint>
         <fes:Constraint name="ImplementsAdHocQuery">
            <ows:NoValues/>
            <ows:DefaultValue>FALSE</ows:DefaultValue>
         </fes:Constraint>
         <fes:Constraint name="ImplementsFunctions">
            <ows:NoValues/>
            <ows:DefaultValue>FALSE</ows:DefaultValue>
         </fes:Constraint>
         <fes:Constraint name="ImplementsMinStandardFilter">
            <ows:NoValues/>
            <ows:DefaultValue>FALSE</ows:DefaultValue>
         </fes:Constraint>
         <fes:Constraint name="ImplementsStandardFilter">
            <ows:NoValues/>
            <ows:DefaultValue>FALSE</ows:DefaultValue>
         </fes:Constraint>
         <fes:Constraint name="ImplementsMinSpatialFilter">
            <ows:NoValues/>
            <ows:DefaultValue>FALSE</ows:DefaultValue>
         </fes:Constraint>
         <fes:Constraint name="ImplementsSpatialFilter">
            <ows:NoValues/>
            <ows:DefaultValue>FALSE</ows:DefaultValue>
         </fes:Constraint>
         <fes:Constraint name="ImplementsMinTemporalFilter">
            <ows:NoValues/>
            <ows:DefaultValue>FALSE</ows:DefaultValue>
         </fes:Constraint>
         <fes:Constraint name="ImplementsTemporalFilter">
            <ows:NoValues/>
            <ows:DefaultValue>FALSE</ows:DefaultValue>
         </fes:Constraint>
         <fes:Constraint name="ImplementsVersionNav">
            <ows:NoValues/>
            <ows:DefaultValue>FALSE</ows:DefaultValue>
         </fes:Constraint>
         <fes:Constraint name="ImplementsSorting">
            <ows:NoValues/>
            <ows:DefaultValue>FALSE</ows:DefaultValue>
         </fes:Constraint>
         <fes:Constraint name="ImplementsExtendedOperators">
            <ows:NoValues/>
            <ows:DefaultValue>FALSE</ows:DefaultValue>
         </fes:Constraint>
     </fes:Conformance>
   </fes:Filter_Capabilities>
</WFS_Capabilities>

"""

from django.urls import reverse
from api.models.metadata import MetaData
from .xml import (
    create,
    create_tree,
    append,
    tree_to_string,
    rid_to_typename_ns,
)
from .geodata import feature_loader, schema_loader
from urllib.parse import urlparse


def capabilities():
    attributes = {
        "version":
        "2.0.0",
        "xmlns":
        "http://www.opengis.net/wfs/2.0",
        "xmlns:gml":
        "http://www.opengis.net/gml/3.2",
        "xmlns:fes":
        "http://www.opengis.net/fes/2.0",
        "xmlns:xlink":
        "http://www.w3.org/1999/xlink",
        "xmlns:ows":
        "http://www.opengis.net/ows/1.1",
        "xmlns:xsd":
        "http://www.w3.org/2001/XMLSchema",
        "xmlns:xsi":
        "http://www.w3.org/2001/XMLSchema-instance",
        "xsi:schemaLocation":
        "http://www.opengis.net/wfs/2.0 http://schemas.opengis.net/wfs/2.0.02.0/wfs.xsd http://www.opengis.net/ows/1.1 http://schemas.opengis.net/ows/1.1.0/owsAll.xsd",
    }
    return create('WFS_Capabilities', attributes)


def service_identification(caps, request):
    ident = append(caps, 'ows:ServiceIdentification')
    append(ident, 'ows:Title').text = 'carto-station'
    append(ident, 'ows:Abstract').text = 'carto-station'
    append(ident, 'ows:ServiceType').text = 'WFS'
    append(ident, 'ows:ServiceTypeVersion').text = '2.0.0'
    append(ident, 'ows:ServiceTypeVersion').text = '2.0.0'
    append(ident, 'ows:Fees').text = 'NONE'
    append(ident, 'ows:AccessConstraints').text = 'NONE'


def service_provider(caps, request):
    ident = append(caps, 'ows:ServiceProvider')
    append(ident, 'ows:ProviderName').text = 'carto-station'
    append(ident, 'ows:ProviderSite').text = 'bruxelles'


def make_op_metadata(md, name, request):
    op = append(md, 'ows:Operation', dict(name=name))
    dcp = append(op, 'ows:DCP')

    http = append(dcp, 'ows:HTTP')
    append(http, 'ows:Get', {'xlink:href': request.get_raw_uri()})

    # param = append(dcp, 'ows:Parameter', dict(name='AcceptVersions'))
    # version = append(param, 'ows:AllowedValues')
    # append(version, 'ows:Value').text = '2.0.0'
    # append(version, 'ows:Value').text = '2.0.0'


def make_op_parameter(md, name, values):
    param = append(md, 'ows:Parameter', dict(name=name))
    val = append(param, 'ows:AllowedValues')
    for value in values:
        append(val, 'ows:Value').text = value


CONSTRAINTS = [
    'ImplementsBasicWFS',
    'ImplementsTransactionalWFS',
    'ImplementsLockingWFS',
    'KVPEncoding',
    'XMLEncoding',
    'SOAPEncoding',
    'ImplementsInheritance',
    'ImplementsRemoteResolve',
    'ImplementsResultPaging',
    'ImplementsStandardJoins',
    'ImplementsSpatialJoins',
    'ImplementsTemporalJoins',
    'ImplementsFeatureVersioning',
    'ManageStoredQueries',
]
BASIC_CONFORMANCE = ['ImplementsBasicWFS', 'KVPEncoding']


def make_constraint(md, name, default):
    constraint = append(md, 'ows:Constraint', dict(name=name))
    append(constraint, 'ows:NoValues')
    append(constraint, 'ows:DefaultValue').text = default


def operations_metadata(caps, request):
    md = append(caps, 'ows:OperationsMetadata')
    make_op_metadata(md, 'GetCapabilities', request)
    make_op_metadata(md, 'DescribeFeatureType', request)
    make_op_metadata(md, 'ListStoredQueries', request)
    make_op_metadata(md, 'DescribeStoredQueries', request)
    make_op_metadata(md, 'GetFeature', request)
    make_op_parameter(md, 'version', ['2.0.0', '2.0.0'])
    for con in CONSTRAINTS:
        val = 'TRUE' if con in BASIC_CONFORMANCE else 'FALSE'
        make_constraint(md, con, val)


def md_to_feature(ftl, md, request, lang):
    rid = md.resource_identifier
    try:
        schema_loader.get_loader(urlparse(rid).scheme)
        feature_loader.get_loader(urlparse(rid).scheme)
        ft = append(ftl, 'FeatureType')
        append(ft, 'Name').text = rid_to_typename_ns(rid)
        append(ft, 'Title').text = getattr(md.title, lang)
        append(ft, 'Abstract').text = getattr(md.abstract, lang)
        append(ft,
               'DefaultCRS').text = 'http://www.opengis.net/def/crs/epsg/0/31370'
        append(ft, 'MetadataURL').text = request.build_absolute_uri(
            reverse('metadata-detail', args=(md.id, )))
    except Exception:
        pass


def feature_type_list(caps, request, lang):
    ftl = append(caps, 'FeatureTypeList')
    for md in MetaData.objects.all():
        md_to_feature(ftl, md, request, lang)


FILTER_CONSTRAINTS = [
    'ImplementsQuery',
    'ImplementsAdHocQuery',
    'ImplementsFunctions',
    'ImplementsMinStandardFilter',
    'ImplementsStandardFilter',
    'ImplementsMinSpatialFilter',
    'ImplementsSpatialFilter',
    'ImplementsMinTemporalFilter',
    'ImplementsTemporalFilter',
    'ImplementsVersionNav',
    'ImplementsSorting',
    'ImplementsExtendedOperators',
]

IMPLEMENTED_FILTER_CONSTRAINTS = ['ImplementsQuery']


def filter_capabilities(caps, request):
    fes = append(caps, 'fes:Filter_Capabilities')
    conf = append(fes, 'fes:Conformance')

    for con in FILTER_CONSTRAINTS:
        val = 'TRUE' if con in IMPLEMENTED_FILTER_CONSTRAINTS else 'FALSE'
        make_constraint(conf, con, val)


def get_capabilities(request, lang):
    caps = capabilities()
    service_identification(caps, request)
    service_provider(caps, request)
    operations_metadata(caps, request)
    feature_type_list(caps, request, lang)
    filter_capabilities(caps, request)

    return tree_to_string(create_tree(caps))
