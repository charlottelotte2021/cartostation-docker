from django.forms import (
    Form,
    ModelForm,
    DateField,
    TextInput,
    IntegerField,
    NumberInput,
)
from api.models.metadata import MetaData


class MetadataForm(ModelForm):
    class Meta:
        model = MetaData
        fields = "__all__"


class MetadataSearchForm(Form):
    begin = DateField(
        required=False,
        widget=TextInput(
            attrs={
                "filter_field": "creation",
                "filter_method": "__gte",
                "data-mask": "00/00/0000",
                "placeholder": "MM/DD/YYYY",
            }
        ),
    )
    end = DateField(
        required=False,
        widget=TextInput(
            attrs={
                "filter_field": "creation",
                "filter_method": "__lte",
                "data-mask": "00/00/0000",
                "placeholder": "MM/DD/YYYY",
            }
        ),
    )
    count = IntegerField(
        required=False,
        widget=NumberInput(attrs={"filter_field": "layer_count"}),
    )
