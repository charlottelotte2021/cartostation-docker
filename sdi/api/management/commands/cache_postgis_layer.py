#
#  Copyright (C) 2019 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import io
import codecs
from datetime import datetime
from json import dump
from pathlib import PosixPath
from django.core.management.base import BaseCommand, CommandError
from django.db import connections
from django.core.cache import caches, InvalidCacheBackendError

from postgis_loader.serializer import get_geojson


class Command(BaseCommand):
    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('config', nargs=1)

    def handle(self, *args, **options):
        config = PosixPath(options['config'][0])
        cache = caches['layers']

        with config.open() as lines:
            for ckey in lines.read().split('\n'):
                # print('[{}]'.format(ckey))
                try:
                    schema, table = ckey.split('.')
                    start = datetime.now()
                    # the following comes from postgis_loader.views
                    # which consumes this cache item, so check there if changing here
                    # or here if changes happened there
                    stream = io.BytesIO()
                    writer = codecs.getwriter("utf-8")(stream)
                    data = get_geojson(schema, table)
                    dump(data, writer)
                    stream.seek(0)
                    cache.set(ckey, stream, read=True)
                    self.stdout.write(
                        self.style.SUCCESS(
                            'Got "{}" in cache in {} seconds'.format(
                                ckey,
                                (datetime.now() - start).total_seconds())))
                except Exception as ex:
                    self.stdout.write(
                        self.style.ERROR('{} failed: {}'.format(ckey, ex)))
