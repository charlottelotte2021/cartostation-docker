#
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from urllib.parse import urlparse
from importlib import import_module

from rest_framework import serializers
from django.apps import apps
from django.conf import settings


from ..models import (
    Thesaurus,
    MetaData,
    Topic,
    Keyword,
    BoundingBox,
    ResponsibleOrganisation,
    PointOfContact,
    Role,
    Organisation,
)
from lingua.serializers import LinguaRecordSerializer
from lingua.fields import message, simple_message


class DuplicateURIError(Exception):
    def __init__(self, uri):
        super().__init__('Duplicate Resource Identifier: "{}"'.format(uri))


class ThesaurusSerializer(serializers.ModelSerializer):
    name = LinguaRecordSerializer()

    class Meta:
        model = Thesaurus
        fields = ("id", "name", "uri", "admin_only")


class TopicSerializer(serializers.ModelSerializer):
    name = LinguaRecordSerializer()

    class Meta:
        model = Topic
        fields = ("id", "code", "name", "thesaurus")


class KeywordSerializer(serializers.ModelSerializer):
    name = LinguaRecordSerializer()
    thesaurus = ThesaurusSerializer()

    class Meta:
        model = Keyword
        fields = ("id", "code", "name", "thesaurus")


class BoundingBoxSerializer(serializers.ModelSerializer):
    class Meta:
        model = BoundingBox
        fields = ("west", "north", "east", "south")


class OrgSerializer(serializers.ModelSerializer):
    name = LinguaRecordSerializer(read_only=True)
    email = serializers.EmailField(read_only=True)
    contactName = serializers.SerializerMethodField(method_name="get_contact_name")

    def get_contact_name(self, instance):
        return instance.contact_name

    class Meta:
        model = Organisation
        fields = ("id", "name", "email", "contactName")


class ResponsibleOrgSerializer(serializers.ModelSerializer):
    # organisationName = serializers.SerializerMethodField(method_name="get_org_name")
    # contactName = serializers.SerializerMethodField(method_name="get_org_contact")
    # email = serializers.SerializerMethodField(method_name="get_org_email")
    organisation = OrgSerializer()
    roleCode = serializers.SerializerMethodField(method_name="get_role")

    class Meta:
        model = ResponsibleOrganisation
        fields = ("id", "organisation", "roleCode")

    def get_role(self, instance):
        return instance.role.code


class PointOfContactSerializer(serializers.ModelSerializer):
    # organisationName = serializers.SerializerMethodField(method_name="get_org_name")
    contactName = serializers.SerializerMethodField(method_name="get_name")
    email = serializers.SerializerMethodField(method_name="get_email")
    organisation = OrgSerializer()

    class Meta:
        model = PointOfContact
        fields = ("id", "organisation", "email", "contactName")

    def get_name(self, instance):
        full_name = instance.user.get_full_name()
        if full_name:
            return full_name
        return instance.user.get_username()

    def get_email(self, instance):
        user = instance.user
        return getattr(user, user.get_email_field_name())


class AppConfigNotFound(Exception):
    pass


def find_app_config(name):
    for label in apps.app_configs:
        if name == label:
            return apps.app_configs[label]
    raise AppConfigNotFound("Config {} Not Found".format(name))


class MetaDataSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)
    resourceIdentifier = serializers.CharField(source="resource_identifier")
    resourceTitle = LinguaRecordSerializer(source="title")
    resourceAbstract = LinguaRecordSerializer(source="abstract")

    uniqueResourceIdentifier = serializers.SerializerMethodField(
        method_name="get_resource_identifier"
    )

    dataStreamUrl = serializers.SerializerMethodField(method_name="get_data_stream_url")

    geometryType = serializers.CharField(source="geometry_type")
    topicCategory = serializers.PrimaryKeyRelatedField(
        source="topics",
        required=False,
        many=True,
        default=[],
        pk_field=serializers.UUIDField(format="hex_verbose"),
        queryset=Topic.objects,
    )
    keywords = serializers.PrimaryKeyRelatedField(
        required=False,
        many=True,
        default=[],
        pk_field=serializers.UUIDField(format="hex_verbose"),
        queryset=Keyword.objects,
    )
    geographicBoundingBox = BoundingBoxSerializer(source="bounding_box", required=False)
    temporalReference = serializers.SerializerMethodField(
        method_name="get_temporal_ref"
    )
    responsibleOrganisation = serializers.PrimaryKeyRelatedField(
        many=True,
        source="responsible_organisation",
        read_only=True,
    )
    metadataPointOfContact = serializers.PrimaryKeyRelatedField(
        many=True,
        source="point_of_contact",
        queryset=PointOfContact.objects,
        required=False,
    )

    metadataDate = serializers.DateTimeField(source="revision", read_only=True)

    maintenanceFrequency = serializers.CharField(source="maintenance_frequency")

    class Meta:
        model = MetaData
        fields = (
            "id",
            "resourceIdentifier",
            "geometryType",
            "resourceTitle",
            "resourceAbstract",
            "uniqueResourceIdentifier",
            "dataStreamUrl",
            "topicCategory",
            "keywords",
            "geographicBoundingBox",
            "temporalReference",
            "responsibleOrganisation",
            "metadataPointOfContact",
            "metadataDate",
            "published",
            "maintenanceFrequency",
        )

    def get_id(self, instance):
        return str(instance.id)

    def get_temporal_ref(self, instance):
        return dict(
            creation=instance.creation,
            revision=instance.revision,
        )

    def get_resource_identifier(self, instance):
        """
        Build a valid URL to pass to clients to access the resource
        An 'internal' resource identifier is an URL-like. We first
        extract the scheme to identify a geodata loader, then from
        its urls module try to import a function `from_identifier`
        that takes an resource_identifier and a request and returns
        an URL. The request is useful to let the function build an
        absolute URL.
        If the whole thing fails, we just return the resource identifier as is.
        """
        rid = instance.resource_identifier
        u = urlparse(rid)

        try:
            config = find_app_config(u.scheme)
            module_url = import_module(".urls", config.name)
            return module_url.from_identifier(u, self.context["request"])
        except Exception:
            return rid

    def get_data_stream_url(self, instance):
        """
        .
        """
        rid = instance.resource_identifier
        u = urlparse(rid)

        try:
            config = find_app_config(u.scheme)
            module_url = import_module(".urls", config.name)
            return module_url.data_stream_url(u, self.context["request"])
        except Exception:
            return None

    def update(self, instance, validated_data):
        title_data = validated_data.get("title")
        abstract_data = validated_data.get("abstract")
        keywords_data = validated_data.get("keywords", [])
        topics_data = validated_data.get("topics", [])
        maintenance_frequency_data = validated_data.get("maintenance_frequency")
        published_data = validated_data.get("published", False)
        poc_data = validated_data.get("point_of_contact", [])

        instance.update_title(title_data)
        instance.update_abstract(abstract_data)
        instance.update_keywords(keywords_data)
        instance.update_topics(topics_data)
        instance.update_maintenance_frequency(maintenance_frequency_data)
        instance.update_publication_state(published_data)
        instance.update_point_of_contact(poc_data)
        instance.save()

        return instance

    def get_posted_resource_identifier(self):
        """Because `uniqueResourceIdentifier` is read_only field,
        it doesn't show  up in validated data, so we're going
        to pick it up at the source.
        """
        data = self.initial_data
        if isinstance(data, dict) and "uniqueResourceIdentifier" in data:
            postable_rid = serializers.CharField(max_length=1024, allow_blank=False)
            return postable_rid.run_validation(data["uniqueResourceIdentifier"])

        raise Exception("Missing resource identifier")

    def create(self, validated_data):
        request = self.context.get("request")
        user = request.user

        uri = self.get_posted_resource_identifier()
        title_data = validated_data.get("title")
        abstract_data = validated_data.get("abstract")
        geometry_type_data = validated_data.get("geometry_type")

        if MetaData.objects.filter(resource_identifier=uri).count() > 0:
            raise DuplicateURIError(uri)

        poc = user.pointofcontact_set.first()
        org = poc.organisation
        try:
            role = Role.objects.get(code="author")
        except Role.DoesNotExist:
            role_name = simple_message("role_name", "author")
            role = Role.objects.create(code="author", name=role_name)

        instance = MetaData.objects.create(
            title=message("md_title", **title_data),
            abstract=message("md_abstract", **abstract_data),
            resource_identifier=uri,
            bounding_box=BoundingBox.objects.create(),
            geometry_type=geometry_type_data,
        )
        instance.point_of_contact.add(poc)
        ResponsibleOrganisation.objects.create(
            organisation=org,
            md=instance,
            role=role,
        )

        return instance
