# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-09-26 10:56
from __future__ import unicode_literals

from django.db import migrations, models
try:
    from jsonfield.fields import JSONField
except Exception:
    from django.db.models import JSONField


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0011_patch_layerinfo_en'),
    ]

    operations = [
        migrations.AlterField(
            model_name='baselayer',
            name='params',
            field=JSONField(default=dict),
        ),
        migrations.AlterField(
            model_name='layerinfo',
            name='feature_view_options',
            field=JSONField(default=dict),
        ),
        migrations.AlterField(
            model_name='layerinfo',
            name='style',
            field=JSONField(default=dict),
        ),
        migrations.AlterField(
            model_name='messagerecord',
            name='parameters',
            field=JSONField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='metadata',
            name='keywords',
            field=models.ManyToManyField(
                blank=True, related_name='md_keyword', to='api.Keyword'),
        ),
        migrations.AlterField(
            model_name='metadata',
            name='topics',
            field=models.ManyToManyField(
                blank=True, related_name='md_topic', to='api.Topic'),
        ),
    ]
