from django.contrib.admin import ModelAdmin, SimpleListFilter
from django_admin_search.admin import AdvancedSearchAdmin
from django.conf import settings
from api.form.metadata import MetadataSearchForm, MetadataForm
from django.db.models import Count, F
from django.contrib import messages


class SchemaListFilter(SimpleListFilter):
    title = "Schema"
    parameter_name = "schema"

    def lookups(self, request, model_admin):
        return [(s, s) for s in settings.LAYERS_SCHEMAS]

    def queryset(self, request, queryset):
        value = self.value()
        if value in settings.LAYERS_SCHEMAS:
            prefix = f"{value}/"
            return queryset.filter(resource_identifier__contains=prefix)


class MetadataAdmin(AdvancedSearchAdmin):
    form = MetadataForm
    search_form = MetadataSearchForm
    search_fields = ("resource_identifier",)
    list_display = (
        "title",
        "resource_identifier",
        "published",
        "creation",
        "revision",
        "layers_count",
        # "status",
    )
    ordering = ("revision",)
    list_filter = (
        SchemaListFilter,
        "published",
    )
    list_per_page = 500

    def get_queryset(self, request):
        """
        override django admin 'get_queryset'
        """
        queryset = ModelAdmin.get_queryset(self, request).annotate(
            layer_count=Count("layer_info_md")
        )
        try:
            base = self.advanced_search_query(request)
            return queryset.filter(base)
        except Exception as ex:  # pylint: disable=broad-except
            messages.add_message(
                request, messages.ERROR, f"Filter not applied, error has occurred: {ex}"
            )
            return queryset.none()

    def delete_queryset(self, request, queryset):
        for obj in queryset:
            obj.status = self.model.DELETED
            obj.save()

    def delete_model(self, request, obj):
        obj.status = self.model.DELETED
        obj.save()


class KeywordAdmin(ModelAdmin):
    list_display = ("name", "code", "thesaurus")
    search_fields = ("name__fr", "name__nl")
