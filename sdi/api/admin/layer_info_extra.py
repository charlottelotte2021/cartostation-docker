from django.contrib.admin import ModelAdmin


class LayerInfoExtraAdmin(ModelAdmin):
    list_display = ('layer_info', 'exportable')

    def layer_info(self, layer_info_extra):
        return layer_info_extra.layer_info
