from django.contrib.admin import ModelAdmin


class EditedAdmin(ModelAdmin):
    list_display = (
        'key',
        'record',
        'updated_at',
    )
