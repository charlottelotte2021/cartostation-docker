from functools import partial
from rest_framework import viewsets
from rest_framework.permissions import DjangoObjectPermissions, BasePermission
from rest_framework.filters import BaseFilterBackend
from django.core.exceptions import ValidationError

import rules


class ObjectPermissions(DjangoObjectPermissions):
    """
    Similar to `DjangoObjectPermissions`, but adding 'view' permissions.
    """
    perms_map = {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': ['%(app_label)s.view_%(model_name)s'],
        'HEAD': ['%(app_label)s.view_%(model_name)s'],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }

    def get_permission(self, method, model):
        kwargs = {
            'app_label': model._meta.app_label,
            'model_name': model._meta.model_name
        }
        return self.perms_map[method][0] % kwargs

    def has_object_permission(self, request, view, obj):
        """
        Object-level permission to only allow correct user of an object to use it.
        """
        return request.user.has_perm(self.get_permission(request.method, obj), obj)

    def has_permission(self, request, view):
        """
        Viewset calls it whatever, so we need to neutralized otherwise
        we nevr reach object level
        """
        method = request.method
        if method == 'POST':
            try:
                serializer_class = view.get_serializer_class()
                model = serializer_class.Meta.model
                return request.user.has_perm(self.get_permission(method, model))
            except Exception:
                pass # revert to old behaviour for the moment
        return True


class RulesPermissionsFilter(BaseFilterBackend):
    """
    A filter backend that limits results to those where the requesting user
    has read object level permissions.
    """

    perm_format = '%(app_label)s.view_%(model_name)s'

    def filter_queryset(self, request, queryset, view):
        queryset = view.get_queryset()  # Q: what is the usecase for this? - pm

        # If no queryset or exception, do not check permission
        # Q: waht is the semantic of `not` here ? - pm
        try:
            if not queryset:
                return None
        except Exception:
            return None

        # Case of viewing a map : filter on pk
        if 'pk' in view.kwargs:
            try:
                return queryset.filter(id=view.kwargs['pk'])
            except Exception:
                return None

        # Case of viewing list of maps : filter on rules
        user = request.user
        model_cls = queryset.model
        kwargs = {
            'app_label': model_cls._meta.app_label,
            'model_name': model_cls._meta.model_name
        }
        permission = self.perm_format % kwargs

        def perm_checker(o):
            return user.has_perm(permission, o)

        ids = [o.pk for o in filter(perm_checker, queryset)]
        # Before you start debugging this again, remember that superuser
        # has all and every permissions, thus short-circuiting
        # permissions check - pm
        # print('IDS[{}] {}'.format(permission, ids))

        return queryset.filter(pk__in=ids)


class ViewSetWithPermissions(viewsets.ModelViewSet):
    permission_classes = (ObjectPermissions, )


class ViewSetWithPermissionsAndFilter(ViewSetWithPermissions):
    filter_backends = (RulesPermissionsFilter, )
