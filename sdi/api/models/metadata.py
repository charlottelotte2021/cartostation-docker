#
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import uuid
from django.utils import timezone

from django.db import models
from django.contrib.auth.models import User


from lingua.fields import nullable_label_field, nullable_text_field, simple_message


class Thesaurus(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = nullable_label_field("thesaurus_name")
    uri = models.URLField()
    admin_only = models.BooleanField(default=False)

    def __str__(self):
        return str(self.uri)


class Topic(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    code = models.CharField(max_length=124)
    name = nullable_label_field("topic_name")
    thesaurus = models.ForeignKey(Thesaurus, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.code)


class Keyword(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    code = models.CharField(max_length=124)
    name = nullable_label_field("keyword_name")
    thesaurus = models.ForeignKey(Thesaurus, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.code)


class BoundingBox(models.Model):
    id = models.AutoField(primary_key=True)
    west = models.FloatField(default=0)
    north = models.FloatField(default=0)
    east = models.FloatField(default=0)
    south = models.FloatField(default=0)

    def to_polygon_wkt(self):
        return "POLYGON(({west} {south}, {west} {north}, {east} {north}, {east} {south}, {west} {south}))".format(
            west=self.west, north=self.north, east=self.east, south=self.south
        )

    def __str__(self):
        return "{}, {}, {}, {}".format(self.west, self.south, self.east, self.north)


class Organisation(models.Model):
    id = models.AutoField(primary_key=True)
    name = nullable_label_field("org_name")
    email = models.EmailField()
    contact_name = models.TextField()

    def __str__(self):
        return "{}/{}".format(self.name.fr, self.name.nl)


class PointOfContact(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    organisation = models.ForeignKey(
        Organisation,
        on_delete=models.CASCADE,
        related_name="point_of_contact",
    )

    def __str__(self):
        return "{} @ {}".format(self.user.get_username(), self.organisation)


class Role(models.Model):
    id = models.AutoField(primary_key=True)
    code = models.CharField(max_length=124)
    name = nullable_label_field("role_name")

    def __str__(self):
        return str(self.code)


class MetaDataManager(models.Manager):
    def create_draft(self, table_name, geometry_type, user):
        poc = user.pointofcontact_set.first()
        org = poc.organisation
        bb = BoundingBox.objects.create()
        title_rec = simple_message("md_title", table_name)
        abstract_rec = simple_message(
            "md_abstract", "An abstract for {}".format(table_name)
        )
        try:
            role = Role.objects.get(code="author")
        except Role.DoesNotExist:
            role_name = simple_message("role_name", "author")
            role = Role.objects.create(code="author", name=role_name)

        md = self.create(
            title=title_rec,
            abstract=abstract_rec,
            resource_identifier=table_name,
            bounding_box=bb,
            geometry_type=geometry_type,
        )

        md.point_of_contact.add(poc)
        ResponsibleOrganisation.objects.create(
            organisation=org,
            md=md,
            role=role,
        )

        return md

    def fetch_bulk(self):
        select = ("bounding_box",)
        prefetch = (
            "responsible_organisation",
            "topics",
            "keywords",
            "point_of_contact",
        )
        return self.select_related(*select).prefetch_related(*prefetch)

    def get_queryset(self):
        return super().get_queryset().filter(status=self.model.VISIBLE)


class MetaData(models.Model):
    POINT = "Point"
    POLYGON = "Polygon"
    LINESTRING = "LineString"
    MULTIPOINT = "MultiPoint"
    MULTIPOLYGON = "MultiPolygon"
    MULTILINESTRING = "MultiLineString"

    GEOMETRY = (
        (POINT, POINT),
        (LINESTRING, LINESTRING),
        (POLYGON, POLYGON),
        (MULTIPOINT, MULTIPOINT),
        (MULTILINESTRING, MULTILINESTRING),
        (MULTIPOLYGON, MULTIPOLYGON),
    )

    FREQUENCY_CODE = (
        ("continual", "data is repeatedly and frequently updated"),
        ("daily", "data is updated each day"),
        ("weekly", "data is updated on a weekly basis"),
        ("fortnightly", "data is updated every two weeks"),
        ("monthly", "data is updated each month"),
        ("quarterly", "data is updated every three months"),
        ("biannually", "data is updated twice each year"),
        ("annually", "data is updated every year"),
        ("asNeeded", "data is updated as deemed necessary"),
        ("irregular", "data is updated in intervals that are uneven in duration"),
        ("notPlanned", "there are no plans to update the data"),
        ("unknown", "frequency of maintenance for the data is not known"),
    )

    VISIBLE = "vis"
    DELETED = "del"

    STATUS_KEYS = (
        (VISIBLE, "visible"),
        (DELETED, "deleted"),
    )
    status = models.CharField(max_length=20, choices=STATUS_KEYS, default=VISIBLE)

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = nullable_label_field("md_title")
    abstract = nullable_text_field("md_abstract")
    resource_identifier = models.CharField(max_length=1024)
    geometry_type = models.CharField(max_length=16, choices=GEOMETRY)
    published = models.BooleanField(default=False)

    topics = models.ManyToManyField(Topic, related_name="md_topic", blank=True)
    keywords = models.ManyToManyField(Keyword, related_name="md_keyword", blank=True)

    bounding_box = models.ForeignKey(BoundingBox, on_delete=models.CASCADE)

    creation = models.DateTimeField(auto_now_add=True, editable=False)
    revision = models.DateTimeField(default=timezone.now, editable=False)

    responsible_organisation = models.ManyToManyField(
        Organisation,
        through="ResponsibleOrganisation",
        through_fields=("md", "organisation"),
        related_name="md_responsible_org",
    )

    point_of_contact = models.ManyToManyField(
        PointOfContact,
        related_name="md_point_of_contact",
    )

    # https://www.ngdc.noaa.gov/wiki/index.php/ISO_19115_and_19115-2_CodeList_Dictionaries#MD_MaintenanceFrequencyCode
    maintenance_frequency = models.CharField(
        max_length=12, choices=FREQUENCY_CODE, default="unknown"
    )

    objects = MetaDataManager()

    def __str__(self):
        return "[{}], {}".format(self.resource_identifier, self.title)

    @property
    def layers_count(self):
        return self.layer_info_md.count()

    def update_title(self, data):
        self.title.update_record(**data)

    def update_abstract(self, data):
        self.abstract.update_record(**data)

    def update_keywords(self, data):
        self.keywords.clear()
        for k in data:
            self.keywords.add(k)

    def update_topics(self, data):
        self.topics.clear()
        for k in data:
            self.topics.add(k)

    def update_maintenance_frequency(self, data):
        self.maintenance_frequency = data

    def update_publication_state(self, data):
        self.published = data

    def update_point_of_contact(self, data):
        self.keywords.clear()
        for p in data:
            self.point_of_contact.add(p)


class ResponsibleOrganisation(models.Model):
    id = models.AutoField(primary_key=True)
    organisation = models.ForeignKey(Organisation, on_delete=models.CASCADE)
    md = models.ForeignKey(MetaData, null=True, on_delete=models.SET_NULL)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
