import json

from rest_framework import serializers
from activity.models.profile import ActivityProfile
from activity.profile_parser import parse_profile


# class JSONField(serializers.RelatedField):
#     def to_representation(self, value):
#         return json.loads(value)


# class ActivityProfileTextField(serializers.Field):
#     def to_representation(self, value):
#         return parse_profile(value)


class ActivityProfileSerializer(serializers.ModelSerializer):
    # text = ProfileTextField(read_only=True)
    layout = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = ActivityProfile
        fields = ["id", "layout", "name"]

    def get_layout(self, instance):
        return parse_profile(instance.text)
