"""The profile parser

It parses lines of the form:
<indentation><object_type><whitespace+><object_name>([<whitespace+><option>=<value>]*)

Indentation is the means of creating scopes

"""
from parsy import (
    regex,
    seq,
    string,
    whitespace,
    generate,
    alt,
    letter,
    char_from,
    ParseError,
)
from json import dumps, loads, JSONEncoder

WIDGET_TAGS = {"list", "reversedlist", "applist", "value", "graph"}
WIDGET_OPTIONS = [
    {"tag": "list", "argument": {"key": "nb"}},
    {"tag": "reversedlist", "argument": {"key": "nb"}},
    {
        "tag": "applist",
        "argument": {
            "key": "aggregation",
            "options": ["count", "sum", "min", "max", "avg"],
        },
    },
    {
        "tag": "value",
        "argument": {
            "key": "aggregation",
            "options": ["count", "sum", "min", "max", "avg"],
        },
    },
    {
        "tag": "graph",
        "argument": {
            "key": "graphtype",
            "options": ["barchart", "piechart", "time-barchart"],
        },
    },
]

# WIDGET_NS = {
#     'view',
#     'compose',
#     'angled-query',
#     'angled-project',
#     'solar',
#     'mdq',
# }


class WithData:
    def set_data(self, d):
        self.data = d


class Page(WithData):
    def __init__(self):
        self.boxes = []


class Box(WithData):
    def __init__(self, direction):
        self.direction = direction
        self.children = []


class Widget(WithData):
    def __init__(self, tag):
        self.tag = tag


class MultiWidget(WithData):
    def __init__(self, tag):
        self.tag = tag


class UnknowWidget(WithData):
    def __init__(self, tag):
        self.tag = tag


class Data:
    def __init__(self, key, value):
        self.key = key
        self.value = value


def letter_or_chars(*args):
    char_parsers = char_from("".join(args))
    return alt(letter, char_parsers)


lexeme = lambda p: p << regex(r"\s*")
UNKNOWN_WIDGET = "__unknown_widget__"
widget_tag_parser = [lexeme(string(w)) for w in WIDGET_TAGS] + [
    lexeme(letter.at_least(1).concat()).tag(UNKNOWN_WIDGET)
]

EQUALS = string("=")
INDENT = whitespace.times(0, 124)
PAGE_TYPE = lexeme(string("page"))
BOX_TYPE = lexeme(string("box"))
WIDGET_TYPE = lexeme(string("widget"))
MULTI_WIDGET_TYPE = lexeme(string("multiwidget"))

PAGE_NAME = lexeme(string("new")).desc("Page name")
BOX_NAME = alt(lexeme(string("vertical")), lexeme(string("horizontal"))).desc(
    "Box name"
)
WIDGET_TAG = alt(*widget_tag_parser).desc("Widget tag")
MULTI_WIDGET_TAG = alt(*widget_tag_parser).desc("Multi widget tag")


def make_widget(x):
    if (
        isinstance(
            x,
            (
                list,
                tuple,
            ),
        )
        and x[0] == UNKNOWN_WIDGET
    ):
        return UnknowWidget(x[1])
    return Widget(x)


def make_multiwidget(x):
    if (
        isinstance(
            x,
            (
                list,
                tuple,
            ),
        )
        and x[0] == UNKNOWN_WIDGET
    ):
        return UnknowWidget(x[1])
    return MultiWidget(x)


PAGE_OBJECT = seq(PAGE_TYPE, PAGE_NAME).map(lambda x: Page()).desc("Page object")
BOX_OBJECT = seq(BOX_TYPE, BOX_NAME).map(lambda x: Box(x[1])).desc("Box object")
WIDGET_OBJECT = (
    seq(WIDGET_TYPE, WIDGET_TAG).map(lambda x: make_widget(x[1])).desc("Widget object")
)
MULTI_WIDGET_OBJECT = (
    seq(MULTI_WIDGET_TYPE, WIDGET_TAG)
    .map(lambda x: make_multiwidget(x[1]))
    .desc("Widget object")
)

OBJECT = alt(PAGE_OBJECT, BOX_OBJECT, WIDGET_OBJECT, MULTI_WIDGET_OBJECT).desc("Object")

DATA_KEY = seq(letter_or_chars("_", "-").at_least(1).concat(), EQUALS).map(
    lambda ok: ok[0]
)
number = lexeme(regex(r"-?(0|[1-9][0-9]*)([.][0-9]+)?([eE][+-]?[0-9]+)?")).map(float)
string_part = regex(r'[^"\\]+')
string_esc = string("\\") >> (
    string("\\")
    | string("/")
    | string('"')
    | string("b").result("\b")
    | string("f").result("\f")
    | string("n").result("\n")
    | string("r").result("\r")
    | string("t").result("\t")
    | regex(r"u[0-9a-fA-F]{4}").map(lambda s: chr(int(s[1:], 16)))
)
quoted = lexeme(
    string('"') >> (string_part | string_esc).many().concat() << string('"')
)

# quoted_array = lexeme(string('[') >> quoted.sep_by(string(',')) << string(']'))
ns = letter_or_chars("_", "-").at_least(2).concat()
ns_array = lexeme(ns.sep_by(string(","), min=1))

DATA_VALUE = number | quoted | ns_array


def make_data(d):
    return Data(d[0], d[1])


def make_data_dict(os):
    ret = dict()
    for o in os:
        ret[o.key] = o.value
    return ret


DATA = seq(DATA_KEY, DATA_VALUE).map(make_data).many().map(make_data_dict)


@generate
def LINE():
    indent = yield INDENT
    object = yield OBJECT
    data = yield DATA
    object.set_data(data)
    if len(indent) > 0:
        return (len(indent[0]) // 2, object, data)

    return (0, object, data)


class ProfileValidationError(Exception):
    pass


def parse_profile(data, fail_on_warning=False):
    current_indent = 0
    pages = []
    boxes = {}
    for i, l in enumerate(data.split("\n")):
        line_number = i + 1
        stripped = l.lstrip()
        if len(stripped) > 0 and stripped[0] != "%":
            # print('Parsing "{}"'.format(l))
            try:
                parsed = LINE.parse(l)
            except ParseError as pex:
                message = "Parse error on line {}\n>> {}".format(line_number, pex)
                raise ProfileValidationError(message)
            indent, obj, data = parsed
            if isinstance(obj, Page):
                pages.append(obj)
                boxes = {}
                current_indent = 0
            else:
                page = pages[len(pages) - 1]
                if len(boxes) == 0:
                    if not isinstance(obj, Box):
                        raise ProfileValidationError(
                            "Line {}: Page does not contains any Box to which attach this object".format(
                                line_number
                            )
                        )
                    boxes[indent] = obj
                    page.boxes.append(obj)

                elif isinstance(obj, UnknowWidget):
                    message = 'Line {}: Unknown widget "{}"'.format(
                        line_number, obj.tag
                    )
                    if fail_on_warning:
                        raise ProfileValidationError(message)
                    else:
                        print("[WARNING] {}".format(message))
                    continue

                else:
                    try:
                        parent_box = boxes[indent - 1]
                    except KeyError:
                        expected = (indent - 1) * 2
                        got = len(l) - len(stripped)
                        raise IndentationError(
                            "\nLine {}: expected an indent of {} whitespaces,  got {}.\n\n{}".format(
                                i, expected, got, l
                            )
                        )

                    parent_box.children.append(obj)
                    if isinstance(obj, Box):
                        boxes[indent] = obj

    return loads(dumps(pages, cls=Encoder, indent=2))


class Encoder(JSONEncoder):
    def default(self, o):

        if isinstance(o, Page):
            return dict(
                type="Page",
                boxes=o.boxes,
                data=o.data,
            )
        elif isinstance(o, Box):
            return dict(
                type="DirectedContainer",
                direction=o.direction.title(),
                children=o.children,
                data=o.data,
            )
        elif isinstance(o, Widget):
            return dict(
                type="Widget",
                tag=o.tag,
                data=o.data,
            )
        elif isinstance(o, MultiWidget):
            return dict(
                type="MultiWidget",
                tag=o.tag,
                data=o.data,
            )

        raise TypeError(repr(o) + " is not a valid profile node")
