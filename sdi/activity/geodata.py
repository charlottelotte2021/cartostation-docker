from importlib import import_module

from django.apps import apps


ACTIVITY_MODULE = 'activity'

class Geodata:

    def __init__(self):
        self.activities = {}
        self.load_activities()

    def load_activities(self):
        activities = {}

        for label in apps.app_configs:
            app = apps.app_configs[label]
            geodata = getattr(app, 'geodata', None)
            if geodata is not None:
                try:
                    activities[label] = import_module('.' + ACTIVITY_MODULE, app.name)
                    print('Activity Handler "{}"'.format(label))
                except ImportError:
                    pass

        self.activities = activities

    def update_activity(self, ns, data):
        if ns in self.activities:
            self.activities[ns].update_activity(data)
            

