from django.db import models
from django.core.exceptions import ValidationError
from activity.profile_parser import ProfileValidationError, parse_profile


def validate_activity_profile(value):
    try:
        parse_profile(value, True)
    except ProfileValidationError as e:
        raise ValidationError("Error parsing profile: {}".format(e))
    except IndentationError as e:
        raise ValidationError("Indentation error: {}".format(e))
    except Exception as e:
        raise ValidationError("Unknown error: {}".format(e))


class ActivityProfileTextField(models.TextField):
    default_validators = [validate_activity_profile]
    description = "A profile description"


class ActivityProfile(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256, unique=True)
    text = ActivityProfileTextField("Profile Layout")

    def __str__(self):
        return "Profile #{}".format(self.name)
