from django.contrib import admin
from django import forms
from .models import profile
from .profile_parser import WIDGET_OPTIONS


# Register your models here.

class ActivityProfileTextWidget(forms.Textarea):
    template_name = 'activity/textarea.html'

class ActivityProfileForm(forms.ModelForm):
    class Meta:
        model = profile.ActivityProfile
        fields = (
            'name',
            'text',
        )
        widgets = {
            'text':
            ActivityProfileTextWidget(attrs={
                'widgets': WIDGET_OPTIONS,
            }),
        }




class ActivityProfileAdmin(admin.ModelAdmin):
    form = ActivityProfileForm

admin.site.register(profile.ActivityProfile, ActivityProfileAdmin)
